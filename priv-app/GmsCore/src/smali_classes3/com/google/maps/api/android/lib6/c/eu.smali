.class public final Lcom/google/maps/api/android/lib6/c/eu;
.super Lcom/google/android/gms/maps/model/internal/k;

# interfaces
.implements Lcom/google/maps/api/android/lib6/c/as;


# static fields
.field private static final a:Lcom/google/android/gms/maps/model/GroundOverlayOptions;

.field private static w:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/maps/api/android/lib6/c/ar;

.field private final d:Lcom/google/maps/api/android/lib6/c/cd;

.field private e:Lcom/google/android/gms/maps/model/LatLngBounds;

.field private f:F

.field private g:F

.field private h:Lcom/google/android/gms/maps/model/LatLng;

.field private i:F

.field private j:I

.field private k:I

.field private l:Lcom/google/maps/api/android/lib6/c/ct;

.field private m:F

.field private n:Lcom/google/maps/api/android/lib6/c/ev;

.field private o:F

.field private p:F

.field private final q:Lcom/google/maps/api/android/lib6/c/db;

.field private r:Lcom/google/maps/api/android/lib6/c/cs;

.field private s:F

.field private t:Z

.field private u:Z

.field private v:Lcom/google/maps/api/android/lib6/c/bz;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;-><init>()V

    sput-object v0, Lcom/google/maps/api/android/lib6/c/eu;->a:Lcom/google/android/gms/maps/model/GroundOverlayOptions;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/google/maps/api/android/lib6/c/eu;->w:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/maps/model/GroundOverlayOptions;Lcom/google/maps/api/android/lib6/c/ar;Lcom/google/maps/api/android/lib6/c/db;Lcom/google/maps/api/android/lib6/c/cs;Lcom/google/maps/api/android/lib6/c/cd;Lcom/google/maps/api/android/lib6/c/bz;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/maps/model/internal/k;-><init>()V

    invoke-static {p2}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/ar;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->c:Lcom/google/maps/api/android/lib6/c/ar;

    invoke-static {p5}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/cd;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->d:Lcom/google/maps/api/android/lib6/c/cd;

    const-string v0, "go%d"

    new-array v3, v1, [Ljava/lang/Object;

    sget-object v4, Lcom/google/maps/api/android/lib6/c/eu;->w:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/maps/api/android/lib6/c/eu;->q:Lcom/google/maps/api/android/lib6/c/db;

    iput-object p4, p0, Lcom/google/maps/api/android/lib6/c/eu;->r:Lcom/google/maps/api/android/lib6/c/cs;

    iput-object p6, p0, Lcom/google/maps/api/android/lib6/c/eu;->v:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->f()F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_6

    move v0, v1

    :goto_0
    const-string v3, "line width is negative"

    invoke-static {v0, v3}, Lcom/google/k/a/cj;->a(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->d()Lcom/google/android/gms/maps/model/a;

    move-result-object v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_1
    const-string v3, "Options doesn\'t specify an image"

    invoke-static {v0, v3}, Lcom/google/k/a/cj;->a(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->l()F

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->o:F

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->m()F

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->p:F

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->n()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->t:Z

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->j()F

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->s:F

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->k()F

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->m:F

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->d()Lcom/google/android/gms/maps/model/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/ct;->a(Lcom/google/android/gms/maps/model/a;)Lcom/google/maps/api/android/lib6/c/ct;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->l:Lcom/google/maps/api/android/lib6/c/ct;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->q:Lcom/google/maps/api/android/lib6/c/db;

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/c/eu;->l:Lcom/google/maps/api/android/lib6/c/ct;

    invoke-virtual {v0, v3}, Lcom/google/maps/api/android/lib6/c/db;->a(Lcom/google/maps/api/android/lib6/c/ct;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->q:Lcom/google/maps/api/android/lib6/c/db;

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/c/eu;->l:Lcom/google/maps/api/android/lib6/c/ct;

    invoke-virtual {v0, v3}, Lcom/google/maps/api/android/lib6/c/db;->b(Lcom/google/maps/api/android/lib6/c/ct;)Lcom/google/maps/api/android/lib6/c/dc;

    move-result-object v0

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/c/dc;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->j:I

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->q:Lcom/google/maps/api/android/lib6/c/db;

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/c/eu;->l:Lcom/google/maps/api/android/lib6/c/ct;

    invoke-virtual {v0, v3}, Lcom/google/maps/api/android/lib6/c/db;->b(Lcom/google/maps/api/android/lib6/c/ct;)Lcom/google/maps/api/android/lib6/c/dc;

    move-result-object v0

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/c/dc;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->k:I

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->e()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->h()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    const-string v0, "Options doesn\'t specify a position"

    invoke-static {v2, v0}, Lcom/google/k/a/cj;->a(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->h()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->h()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->e:Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/eu;->u()V

    :goto_2
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->i()F

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->i:F

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->i()F

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/eu;->a:Lcom/google/android/gms/maps/model/GroundOverlayOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->i()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->d:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->aa:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->k()F

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/eu;->a:Lcom/google/android/gms/maps/model/GroundOverlayOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->k()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->d:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->af:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->n()Z

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/eu;->a:Lcom/google/android/gms/maps/model/GroundOverlayOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->n()Z

    move-result v1

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->d:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->ae:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->j()F

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/eu;->a:Lcom/google/android/gms/maps/model/GroundOverlayOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->j()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->d:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->ad:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_5
    return-void

    :cond_6
    move v0, v2

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto/16 :goto_1

    :cond_8
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->e()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->h:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->f()F

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->f:F

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->g()F

    move-result v0

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->g()F

    move-result v0

    :goto_3
    iput v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->g:F

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/eu;->v()V

    goto :goto_2

    :cond_9
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->j:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/maps/api/android/lib6/c/eu;->k:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lcom/google/maps/api/android/lib6/c/eu;->f:F

    mul-float/2addr v0, v1

    goto :goto_3
.end method

.method private a(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->u:Z

    if-eqz v0, :cond_1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    return-void

    :cond_1
    monitor-exit p0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->n:Lcom/google/maps/api/android/lib6/c/ev;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->n:Lcom/google/maps/api/android/lib6/c/ev;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/ev;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private u()V
    .locals 14

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/eu;->e:Lcom/google/android/gms/maps/model/LatLngBounds;

    iget-object v0, v2, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-object v0, v2, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v6, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-object v0, v2, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-object v2, v2, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->b:D

    cmpg-double v8, v0, v2

    if-gez v8, :cond_0

    const-wide v8, 0x4076800000000000L    # 360.0

    add-double/2addr v0, v8

    :cond_0
    iget v8, p0, Lcom/google/maps/api/android/lib6/c/eu;->o:F

    iget v9, p0, Lcom/google/maps/api/android/lib6/c/eu;->p:F

    new-instance v10, Lcom/google/android/gms/maps/model/LatLng;

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float/2addr v11, v9

    float-to-double v12, v11

    mul-double/2addr v4, v12

    float-to-double v12, v9

    mul-double/2addr v6, v12

    add-double/2addr v4, v6

    const/high16 v6, 0x3f800000    # 1.0f

    sub-float/2addr v6, v8

    float-to-double v6, v6

    mul-double/2addr v2, v6

    float-to-double v6, v8

    mul-double/2addr v0, v6

    add-double/2addr v0, v2

    invoke-direct {v10, v4, v5, v0, v1}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iput-object v10, p0, Lcom/google/maps/api/android/lib6/c/eu;->h:Lcom/google/android/gms/maps/model/LatLng;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->e:Lcom/google/android/gms/maps/model/LatLngBounds;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/eu;->h:Lcom/google/android/gms/maps/model/LatLng;

    iget-object v2, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-object v0, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-static {v2, v3, v4, v5}, Lcom/google/maps/api/android/lib6/c/ej;->a(DD)D

    move-result-wide v2

    iget-wide v0, v1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    const-wide v4, 0x41584db040000000L    # 6371009.0

    mul-double/2addr v2, v4

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->f:F

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->e:Lcom/google/android/gms/maps/model/LatLngBounds;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/eu;->h:Lcom/google/android/gms/maps/model/LatLng;

    iget-object v1, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-object v0, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    sub-double v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    const-wide v2, 0x41584db040000000L    # 6371009.0

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->g:F

    return-void
.end method

.method private v()V
    .locals 12

    const/high16 v10, 0x3f800000    # 1.0f

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->h:Lcom/google/android/gms/maps/model/LatLng;

    iget v1, p0, Lcom/google/maps/api/android/lib6/c/eu;->f:F

    float-to-double v2, v1

    invoke-static {v0, v2, v3}, Lcom/google/maps/api/android/lib6/c/ej;->a(Lcom/google/android/gms/maps/model/LatLng;D)D

    move-result-wide v0

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/eu;->h:Lcom/google/android/gms/maps/model/LatLng;

    iget v2, p0, Lcom/google/maps/api/android/lib6/c/eu;->g:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Lcom/google/maps/api/android/lib6/c/ej;->a(D)D

    move-result-wide v2

    iget-object v4, p0, Lcom/google/maps/api/android/lib6/c/eu;->h:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, v4, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget v6, p0, Lcom/google/maps/api/android/lib6/c/eu;->p:F

    float-to-double v6, v6

    mul-double/2addr v6, v2

    add-double/2addr v4, v6

    iget-object v6, p0, Lcom/google/maps/api/android/lib6/c/eu;->h:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v6, v6, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget v8, p0, Lcom/google/maps/api/android/lib6/c/eu;->p:F

    sub-float v8, v10, v8

    float-to-double v8, v8

    mul-double/2addr v2, v8

    sub-double v2, v6, v2

    iget-object v6, p0, Lcom/google/maps/api/android/lib6/c/eu;->h:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v6, v6, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget v8, p0, Lcom/google/maps/api/android/lib6/c/eu;->o:F

    sub-float v8, v10, v8

    float-to-double v8, v8

    mul-double/2addr v8, v0

    add-double/2addr v6, v8

    iget-object v8, p0, Lcom/google/maps/api/android/lib6/c/eu;->h:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v8, v8, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget v10, p0, Lcom/google/maps/api/android/lib6/c/eu;->o:F

    float-to-double v10, v10

    mul-double/2addr v0, v10

    sub-double v0, v8, v0

    new-instance v8, Lcom/google/android/gms/maps/model/LatLngBounds;

    new-instance v9, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v9, v2, v3, v0, v1}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-direct {v8, v9, v0}, Lcom/google/android/gms/maps/model/LatLngBounds;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)V

    iput-object v8, p0, Lcom/google/maps/api/android/lib6/c/eu;->e:Lcom/google/android/gms/maps/model/LatLngBounds;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->v:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->d:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->Z:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/eu;->l()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->c:Lcom/google/maps/api/android/lib6/c/ar;

    invoke-virtual {v0, p0}, Lcom/google/maps/api/android/lib6/c/ar;->b(Lcom/google/maps/api/android/lib6/c/as;)V

    return-void
.end method

.method public final a(F)V
    .locals 1

    const/high16 v0, -0x40800000    # -1.0f

    invoke-virtual {p0, p1, v0}, Lcom/google/maps/api/android/lib6/c/eu;->a(FF)V

    return-void
.end method

.method public final a(FF)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->v:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->d:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->ab:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/maps/api/android/lib6/c/eu;->f:F

    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p2, v0

    if-eqz v0, :cond_0

    :goto_0
    iput p2, p0, Lcom/google/maps/api/android/lib6/c/eu;->g:F

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/eu;->v()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/eu;->a(I)V

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->j:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/maps/api/android/lib6/c/eu;->k:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    int-to-float v1, v1

    div-float/2addr v0, v1

    mul-float p2, v0, p1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/b/l;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->v:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->d:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->ag:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->q:Lcom/google/maps/api/android/lib6/c/db;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/eu;->l:Lcom/google/maps/api/android/lib6/c/ct;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/db;->c(Lcom/google/maps/api/android/lib6/c/ct;)V

    invoke-static {p1}, Lcom/google/maps/api/android/lib6/c/ct;->a(Lcom/google/android/gms/b/l;)Lcom/google/maps/api/android/lib6/c/ct;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->l:Lcom/google/maps/api/android/lib6/c/ct;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->q:Lcom/google/maps/api/android/lib6/c/db;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/eu;->l:Lcom/google/maps/api/android/lib6/c/ct;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/db;->a(Lcom/google/maps/api/android/lib6/c/ct;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->q:Lcom/google/maps/api/android/lib6/c/db;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/eu;->l:Lcom/google/maps/api/android/lib6/c/ct;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/db;->b(Lcom/google/maps/api/android/lib6/c/ct;)Lcom/google/maps/api/android/lib6/c/dc;

    move-result-object v0

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/c/dc;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->j:I

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->q:Lcom/google/maps/api/android/lib6/c/db;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/eu;->l:Lcom/google/maps/api/android/lib6/c/ct;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/db;->b(Lcom/google/maps/api/android/lib6/c/ct;)Lcom/google/maps/api/android/lib6/c/dc;

    move-result-object v0

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/c/dc;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->k:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/eu;->a(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->v:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->d:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->ac:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/eu;->h:Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/eu;->v()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/eu;->a(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLngBounds;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->v:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/eu;->e:Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/eu;->u()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/eu;->a(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/internal/BitmapDescriptorParcelable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->v:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->r:Lcom/google/maps/api/android/lib6/c/cs;

    invoke-virtual {v0, p1}, Lcom/google/maps/api/android/lib6/c/cs;->a(Lcom/google/android/gms/maps/model/internal/BitmapDescriptorParcelable;)Lcom/google/android/gms/b/l;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/eu;->a(Lcom/google/android/gms/b/l;)V

    return-void
.end method

.method public final a(Lcom/google/maps/api/android/lib6/c/ev;)V
    .locals 0

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/eu;->n:Lcom/google/maps/api/android/lib6/c/ev;

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->v:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->d:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->ae:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/maps/api/android/lib6/c/eu;->t:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/eu;->a(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/internal/j;)Z
    .locals 1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b(F)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->v:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->d:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->aa:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/maps/api/android/lib6/c/eu;->i:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/eu;->a(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->v:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->h:Lcom/google/android/gms/maps/model/LatLng;

    return-object v0
.end method

.method public final c(F)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->v:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->d:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->ad:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iput p1, p0, Lcom/google/maps/api/android/lib6/c/eu;->s:F

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/eu;->a(I)V

    return-void
.end method

.method public final d()F
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->v:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->f:F

    return v0
.end method

.method public final d(F)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->v:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->d:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->af:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Transparency must be in the range [0..1]"

    invoke-static {v0, v1}, Lcom/google/k/a/cj;->a(ZLjava/lang/Object;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/maps/api/android/lib6/c/eu;->m:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x40

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/eu;->a(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()F
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->v:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->g:F

    return v0
.end method

.method public final declared-synchronized f()Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->v:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->e:Lcom/google/android/gms/maps/model/LatLngBounds;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()F
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->v:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/eu;->m()F

    move-result v0

    return v0
.end method

.method public final h()F
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->v:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->s:F

    return v0
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->v:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/eu;->s()Z

    move-result v0

    return v0
.end method

.method public final j()F
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->v:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/eu;->q()F

    move-result v0

    return v0
.end method

.method public final k()I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final l()V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->n:Lcom/google/maps/api/android/lib6/c/ev;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->n:Lcom/google/maps/api/android/lib6/c/ev;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ev;->a()V

    :cond_0
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->u:Z

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->q:Lcom/google/maps/api/android/lib6/c/db;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/eu;->l:Lcom/google/maps/api/android/lib6/c/ct;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/db;->c(Lcom/google/maps/api/android/lib6/c/ct;)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized m()F
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->i:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final n()Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->e:Lcom/google/android/gms/maps/model/LatLngBounds;

    return-object v0
.end method

.method public final declared-synchronized o()F
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->o:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized p()F
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->p:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized q()F
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->m:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final r()F
    .locals 1

    iget v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->s:F

    return v0
.end method

.method public final declared-synchronized s()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->t:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized t()Lcom/google/maps/api/android/lib6/c/dc;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/eu;->q:Lcom/google/maps/api/android/lib6/c/db;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/eu;->l:Lcom/google/maps/api/android/lib6/c/ct;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/db;->b(Lcom/google/maps/api/android/lib6/c/ct;)Lcom/google/maps/api/android/lib6/c/dc;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
