.class public Lcom/google/maps/api/android/lib6/c/al;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field private final b:Ljava/util/Map;

.field private final c:Lcom/google/maps/api/android/lib6/c/db;

.field private final d:Lcom/google/maps/api/android/lib6/c/cs;

.field private final e:Lcom/google/maps/api/android/lib6/c/cd;

.field private final f:Lcom/google/maps/api/android/lib6/c/am;

.field private final g:Lcom/google/maps/api/android/lib6/c/bz;

.field private h:Lcom/google/android/gms/maps/internal/bb;

.field private i:Lcom/google/android/gms/maps/internal/be;

.field private j:Lcom/google/android/gms/maps/internal/aj;

.field private k:Lcom/google/android/gms/maps/internal/q;

.field private l:Lcom/google/maps/api/android/lib6/c/af;

.field private m:Lcom/google/maps/api/android/lib6/c/ae;

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/maps/api/android/lib6/c/al;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/maps/api/android/lib6/c/am;Lcom/google/android/gms/maps/internal/q;Lcom/google/maps/api/android/lib6/c/db;Lcom/google/maps/api/android/lib6/c/cs;Lcom/google/maps/api/android/lib6/c/bz;Lcom/google/maps/api/android/lib6/c/cd;Lcom/google/maps/api/android/lib6/c/af;Lcom/google/maps/api/android/lib6/c/ae;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/al;->a:I

    invoke-static {}, Lcom/google/k/c/hd;->c()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->b:Ljava/util/Map;

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/al;->f:Lcom/google/maps/api/android/lib6/c/am;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/c/al;->k:Lcom/google/android/gms/maps/internal/q;

    iput-object p3, p0, Lcom/google/maps/api/android/lib6/c/al;->c:Lcom/google/maps/api/android/lib6/c/db;

    iput-object p4, p0, Lcom/google/maps/api/android/lib6/c/al;->d:Lcom/google/maps/api/android/lib6/c/cs;

    iput-object p5, p0, Lcom/google/maps/api/android/lib6/c/al;->g:Lcom/google/maps/api/android/lib6/c/bz;

    iput-object p6, p0, Lcom/google/maps/api/android/lib6/c/al;->e:Lcom/google/maps/api/android/lib6/c/cd;

    iput-object p7, p0, Lcom/google/maps/api/android/lib6/c/al;->l:Lcom/google/maps/api/android/lib6/c/af;

    iput-object p8, p0, Lcom/google/maps/api/android/lib6/c/al;->m:Lcom/google/maps/api/android/lib6/c/ae;

    iput-boolean p9, p0, Lcom/google/maps/api/android/lib6/c/al;->n:Z

    return-void
.end method

.method private k(Lcom/google/maps/api/android/lib6/c/aj;)Lcom/google/maps/api/android/lib6/c/ak;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/ak;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/maps/api/android/lib6/c/aj;
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->d()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "no position in marker options"

    invoke-static {v0, v3}, Lcom/google/k/a/cj;->a(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/maps/api/android/lib6/c/aj;

    const-string v3, "m%d"

    new-array v1, v1, [Ljava/lang/Object;

    iget v4, p0, Lcom/google/maps/api/android/lib6/c/al;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/maps/api/android/lib6/c/al;->c:Lcom/google/maps/api/android/lib6/c/db;

    iget-object v5, p0, Lcom/google/maps/api/android/lib6/c/al;->d:Lcom/google/maps/api/android/lib6/c/cs;

    iget-object v6, p0, Lcom/google/maps/api/android/lib6/c/al;->g:Lcom/google/maps/api/android/lib6/c/bz;

    iget-object v7, p0, Lcom/google/maps/api/android/lib6/c/al;->e:Lcom/google/maps/api/android/lib6/c/cd;

    move-object v2, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/maps/api/android/lib6/c/aj;-><init>(Ljava/lang/String;Lcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/maps/api/android/lib6/c/al;Lcom/google/maps/api/android/lib6/c/db;Lcom/google/maps/api/android/lib6/c/cs;Lcom/google/maps/api/android/lib6/c/bz;Lcom/google/maps/api/android/lib6/c/cd;)V

    iget v1, p0, Lcom/google/maps/api/android/lib6/c/al;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/api/android/lib6/c/al;->a:I

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/al;->f:Lcom/google/maps/api/android/lib6/c/am;

    invoke-interface {v1, v0}, Lcom/google/maps/api/android/lib6/c/am;->a(Lcom/google/maps/api/android/lib6/c/aj;)Lcom/google/maps/api/android/lib6/c/ak;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/aj;->a(Lcom/google/maps/api/android/lib6/c/ak;)V

    invoke-interface {v1}, Lcom/google/maps/api/android/lib6/c/ak;->a()V

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/al;->b:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->o()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/internal/aj;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/al;->j:Lcom/google/android/gms/maps/internal/aj;

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/internal/bb;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/al;->h:Lcom/google/android/gms/maps/internal/bb;

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/internal/be;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/al;->i:Lcom/google/android/gms/maps/internal/be;

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/internal/q;)V
    .locals 1

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/internal/q;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->k:Lcom/google/android/gms/maps/internal/q;

    return-void
.end method

.method public final a(Lcom/google/maps/api/android/lib6/c/aj;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->l:Lcom/google/maps/api/android/lib6/c/af;

    invoke-virtual {v0, p1}, Lcom/google/maps/api/android/lib6/c/af;->a(Lcom/google/maps/api/android/lib6/c/aj;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/ak;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ak;->b()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Lcom/google/maps/api/android/lib6/c/aj;I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/maps/api/android/lib6/c/al;->k(Lcom/google/maps/api/android/lib6/c/aj;)Lcom/google/maps/api/android/lib6/c/ak;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Lcom/google/maps/api/android/lib6/c/ak;->a(I)V

    :cond_0
    return-void
.end method

.method public final b()Lcom/google/android/gms/maps/internal/q;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->k:Lcom/google/android/gms/maps/internal/q;

    return-object v0
.end method

.method public final b(Lcom/google/maps/api/android/lib6/c/aj;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/ak;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ak;->f()Z

    move-result v0

    return v0
.end method

.method public final c(Lcom/google/maps/api/android/lib6/c/aj;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/maps/api/android/lib6/c/al;->k(Lcom/google/maps/api/android/lib6/c/aj;)Lcom/google/maps/api/android/lib6/c/ak;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ak;->c()V

    :cond_0
    return-void
.end method

.method public final d(Lcom/google/maps/api/android/lib6/c/aj;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/maps/api/android/lib6/c/al;->k(Lcom/google/maps/api/android/lib6/c/aj;)Lcom/google/maps/api/android/lib6/c/ak;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ak;->d()V

    :cond_0
    return-void
.end method

.method public final e(Lcom/google/maps/api/android/lib6/c/aj;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->i:Lcom/google/android/gms/maps/internal/be;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/ak;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ak;->e()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->i:Lcom/google/android/gms/maps/internal/be;

    invoke-interface {v0, p1}, Lcom/google/android/gms/maps/internal/be;->a(Lcom/google/android/gms/maps/model/internal/s;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final f(Lcom/google/maps/api/android/lib6/c/aj;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->i:Lcom/google/android/gms/maps/internal/be;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/ak;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ak;->e()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->i:Lcom/google/android/gms/maps/internal/be;

    invoke-interface {v0, p1}, Lcom/google/android/gms/maps/internal/be;->b(Lcom/google/android/gms/maps/model/internal/s;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final g(Lcom/google/maps/api/android/lib6/c/aj;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->i:Lcom/google/android/gms/maps/internal/be;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/ak;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ak;->e()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->i:Lcom/google/android/gms/maps/internal/be;

    invoke-interface {v0, p1}, Lcom/google/android/gms/maps/internal/be;->c(Lcom/google/android/gms/maps/model/internal/s;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final h(Lcom/google/maps/api/android/lib6/c/aj;)Z
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->h:Lcom/google/android/gms/maps/internal/bb;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->h:Lcom/google/android/gms/maps/internal/bb;

    invoke-interface {v0, p1}, Lcom/google/android/gms/maps/internal/bb;->a(Lcom/google/android/gms/maps/model/internal/s;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v2, Lcom/google/maps/api/android/lib6/c/cf;->v:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v2}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :goto_0
    return v1

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v3, Lcom/google/maps/api/android/lib6/c/cf;->u:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v3}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-virtual {p1}, Lcom/google/maps/api/android/lib6/c/aj;->g()V

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/al;->n:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->f:Lcom/google/maps/api/android/lib6/c/am;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/am;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_3

    move v0, v1

    :goto_2
    iget-object v3, p0, Lcom/google/maps/api/android/lib6/c/al;->l:Lcom/google/maps/api/android/lib6/c/af;

    invoke-virtual {v3, v1, p1, v0}, Lcom/google/maps/api/android/lib6/c/af;->a(ZLcom/google/maps/api/android/lib6/c/aj;Z)V

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v3, Lcom/google/maps/api/android/lib6/c/cf;->w:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v3}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method public final i(Lcom/google/maps/api/android/lib6/c/aj;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->j:Lcom/google/android/gms/maps/internal/aj;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->t:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->s:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->j:Lcom/google/android/gms/maps/internal/aj;

    invoke-interface {v0, p1}, Lcom/google/android/gms/maps/internal/aj;->a(Lcom/google/android/gms/maps/model/internal/s;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final j(Lcom/google/maps/api/android/lib6/c/aj;)Z
    .locals 4

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/al;->m:Lcom/google/maps/api/android/lib6/c/ae;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ae;->r()Lcom/google/maps/api/android/lib6/c/ba;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/maps/api/android/lib6/c/aj;->c()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/ba;->a(Lcom/google/android/gms/maps/model/LatLng;)Landroid/graphics/Point;

    move-result-object v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/al;->m:Lcom/google/maps/api/android/lib6/c/ae;

    invoke-interface {v1}, Lcom/google/maps/api/android/lib6/c/ae;->f()Landroid/view/View;

    move-result-object v1

    iget v2, v0, Landroid/graphics/Point;->x:I

    if-ltz v2, :cond_0

    iget v2, v0, Landroid/graphics/Point;->x:I

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v3

    if-ge v2, v3, :cond_0

    iget v2, v0, Landroid/graphics/Point;->y:I

    if-ltz v2, :cond_0

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
