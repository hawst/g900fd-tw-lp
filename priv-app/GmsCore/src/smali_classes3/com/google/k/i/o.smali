.class public final Lcom/google/k/i/o;
.super Lcom/google/k/c/js;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private transient a:Lcom/google/k/c/gd;

.field private synthetic b:Lcom/google/k/i/f;


# direct methods
.method constructor <init>(Lcom/google/k/i/f;)V
    .locals 0

    iput-object p1, p0, Lcom/google/k/i/o;->b:Lcom/google/k/i/f;

    invoke-direct {p0}, Lcom/google/k/c/js;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a()Ljava/util/Collection;
    .locals 1

    invoke-virtual {p0}, Lcom/google/k/i/o;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic b()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/k/i/o;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected final c()Ljava/util/Set;
    .locals 2

    iget-object v0, p0, Lcom/google/k/i/o;->a:Lcom/google/k/c/gd;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/k/i/h;->a:Lcom/google/k/i/h;

    iget-object v1, p0, Lcom/google/k/i/o;->b:Lcom/google/k/i/f;

    invoke-virtual {v0, v1}, Lcom/google/k/i/h;->a(Ljava/lang/Object;)Lcom/google/k/c/fp;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/c/jo;->a(Ljava/lang/Iterable;)Lcom/google/k/c/jo;

    move-result-object v0

    sget-object v1, Lcom/google/k/i/l;->a:Lcom/google/k/i/l;

    iget-object v0, v0, Lcom/google/k/c/jo;->a:Ljava/lang/Iterable;

    invoke-static {v0, v1}, Lcom/google/k/c/gn;->a(Ljava/lang/Iterable;Lcom/google/k/a/ck;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/c/jo;->a(Ljava/lang/Iterable;)Lcom/google/k/c/jo;

    move-result-object v0

    iget-object v0, v0, Lcom/google/k/c/jo;->a:Ljava/lang/Iterable;

    invoke-static {v0}, Lcom/google/k/c/gd;->a(Ljava/lang/Iterable;)Lcom/google/k/c/gd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/i/o;->a:Lcom/google/k/c/gd;

    :cond_0
    return-object v0
.end method

.method public final d()Ljava/util/Set;
    .locals 2

    sget-object v0, Lcom/google/k/i/h;->b:Lcom/google/k/i/h;

    iget-object v1, p0, Lcom/google/k/i/o;->b:Lcom/google/k/i/f;

    iget-object v1, v1, Lcom/google/k/i/f;->a:Ljava/lang/reflect/Type;

    invoke-static {v1}, Lcom/google/k/i/f;->b(Ljava/lang/reflect/Type;)Lcom/google/k/c/gd;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/k/i/h;->a(Ljava/lang/Iterable;)Lcom/google/k/c/fp;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/c/gd;->a(Ljava/util/Collection;)Lcom/google/k/c/gd;

    move-result-object v0

    return-object v0
.end method
