.class public final Lcom/google/k/g/a;
.super Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    const/16 v0, 0xb

    new-array v0, v0, [D

    const/4 v1, 0x0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    aput-wide v2, v0, v1

    const/4 v1, 0x1

    const-wide v2, 0x42b3077775800000L    # 2.0922789888E13

    aput-wide v2, v0, v1

    const/4 v1, 0x2

    const-wide v2, 0x474956ad0aae33a4L    # 2.631308369336935E35

    aput-wide v2, v0, v1

    const/4 v1, 0x3

    const-wide v2, 0x4c9ee69a78d72cb6L    # 1.2413915592536073E61

    aput-wide v2, v0, v1

    const/4 v1, 0x4

    const-wide v2, 0x526fe478ee34844aL    # 1.2688693218588417E89

    aput-wide v2, v0, v1

    const/4 v1, 0x5

    const-wide v2, 0x589c619094edabffL    # 7.156945704626381E118

    aput-wide v2, v0, v1

    const/4 v1, 0x6

    const-wide v2, 0x5f13638dd7bd6347L    # 9.916779348709496E149

    aput-wide v2, v0, v1

    const/4 v1, 0x7

    const-wide v2, 0x65c7cac197cfe503L    # 1.974506857221074E182

    aput-wide v2, v0, v1

    const/16 v1, 0x8

    const-wide v2, 0x6cb1e5dfc140e1e5L    # 3.856204823625804E215

    aput-wide v2, v0, v1

    const/16 v1, 0x9

    const-wide v2, 0x73c8ce85fadb707eL    # 5.5502938327393044E249

    aput-wide v2, v0, v1

    const/16 v1, 0xa

    const-wide v2, 0x7b095d5f3d928edeL    # 4.7147236359920616E284

    aput-wide v2, v0, v1

    return-void
.end method

.method public static a(D)Z
    .locals 2

    invoke-static {p0, p1}, Lcom/google/k/g/b;->c(D)Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x0

    cmpl-double v0, p0, v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lcom/google/k/g/b;->b(D)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->numberOfTrailingZeros(J)I

    move-result v0

    rsub-int/lit8 v0, v0, 0x34

    invoke-static {p0, p1}, Lcom/google/k/g/b;->a(D)I

    move-result v1

    if-gt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
