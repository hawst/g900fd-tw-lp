.class final Lcom/google/k/c/iz;
.super Lcom/google/k/c/iu;

# interfaces
.implements Ljava/util/SortedSet;


# instance fields
.field private synthetic e:Lcom/google/k/c/il;


# direct methods
.method constructor <init>(Lcom/google/k/c/il;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/google/k/c/iu;)V
    .locals 0

    iput-object p1, p0, Lcom/google/k/c/iz;->e:Lcom/google/k/c/il;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/k/c/iu;-><init>(Lcom/google/k/c/il;Ljava/lang/Object;Ljava/util/Collection;Lcom/google/k/c/iu;)V

    return-void
.end method


# virtual methods
.method public final comparator()Ljava/util/Comparator;
    .locals 1

    iget-object v0, p0, Lcom/google/k/c/iu;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final first()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/k/c/iz;->a()V

    iget-object v0, p0, Lcom/google/k/c/iu;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5

    invoke-virtual {p0}, Lcom/google/k/c/iz;->a()V

    new-instance v1, Lcom/google/k/c/iz;

    iget-object v2, p0, Lcom/google/k/c/iz;->e:Lcom/google/k/c/il;

    iget-object v3, p0, Lcom/google/k/c/iu;->a:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/k/c/iu;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v4, p0, Lcom/google/k/c/iu;->c:Lcom/google/k/c/iu;

    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v1, v2, v3, v0, p0}, Lcom/google/k/c/iz;-><init>(Lcom/google/k/c/il;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/google/k/c/iu;)V

    return-object v1

    :cond_0
    iget-object p0, p0, Lcom/google/k/c/iu;->c:Lcom/google/k/c/iu;

    goto :goto_0
.end method

.method public final last()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/k/c/iz;->a()V

    iget-object v0, p0, Lcom/google/k/c/iu;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5

    invoke-virtual {p0}, Lcom/google/k/c/iz;->a()V

    new-instance v1, Lcom/google/k/c/iz;

    iget-object v2, p0, Lcom/google/k/c/iz;->e:Lcom/google/k/c/il;

    iget-object v3, p0, Lcom/google/k/c/iu;->a:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/k/c/iu;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0, p1, p2}, Ljava/util/SortedSet;->subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v4, p0, Lcom/google/k/c/iu;->c:Lcom/google/k/c/iu;

    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v1, v2, v3, v0, p0}, Lcom/google/k/c/iz;-><init>(Lcom/google/k/c/il;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/google/k/c/iu;)V

    return-object v1

    :cond_0
    iget-object p0, p0, Lcom/google/k/c/iu;->c:Lcom/google/k/c/iu;

    goto :goto_0
.end method

.method public final tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5

    invoke-virtual {p0}, Lcom/google/k/c/iz;->a()V

    new-instance v1, Lcom/google/k/c/iz;

    iget-object v2, p0, Lcom/google/k/c/iz;->e:Lcom/google/k/c/il;

    iget-object v3, p0, Lcom/google/k/c/iu;->a:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/k/c/iu;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v4, p0, Lcom/google/k/c/iu;->c:Lcom/google/k/c/iu;

    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v1, v2, v3, v0, p0}, Lcom/google/k/c/iz;-><init>(Lcom/google/k/c/il;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/google/k/c/iu;)V

    return-object v1

    :cond_0
    iget-object p0, p0, Lcom/google/k/c/iu;->c:Lcom/google/k/c/iu;

    goto :goto_0
.end method
