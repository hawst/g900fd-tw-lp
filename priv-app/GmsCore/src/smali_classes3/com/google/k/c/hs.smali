.class final enum Lcom/google/k/c/hs;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/google/k/c/hs;

.field public static final enum b:Lcom/google/k/c/hs;

.field public static final enum c:Lcom/google/k/c/hs;

.field public static final enum d:Lcom/google/k/c/hs;

.field private static final synthetic e:[Lcom/google/k/c/hs;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/k/c/hs;

    const-string v1, "READY"

    invoke-direct {v0, v1, v2}, Lcom/google/k/c/hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/k/c/hs;->a:Lcom/google/k/c/hs;

    new-instance v0, Lcom/google/k/c/hs;

    const-string v1, "NOT_READY"

    invoke-direct {v0, v1, v3}, Lcom/google/k/c/hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/k/c/hs;->b:Lcom/google/k/c/hs;

    new-instance v0, Lcom/google/k/c/hs;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v4}, Lcom/google/k/c/hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/k/c/hs;->c:Lcom/google/k/c/hs;

    new-instance v0, Lcom/google/k/c/hs;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, Lcom/google/k/c/hs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/k/c/hs;->d:Lcom/google/k/c/hs;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/k/c/hs;

    sget-object v1, Lcom/google/k/c/hs;->a:Lcom/google/k/c/hs;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/k/c/hs;->b:Lcom/google/k/c/hs;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/k/c/hs;->c:Lcom/google/k/c/hs;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/k/c/hs;->d:Lcom/google/k/c/hs;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/k/c/hs;->e:[Lcom/google/k/c/hs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/k/c/hs;
    .locals 1

    const-class v0, Lcom/google/k/c/hs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/k/c/hs;

    return-object v0
.end method

.method public static values()[Lcom/google/k/c/hs;
    .locals 1

    sget-object v0, Lcom/google/k/c/hs;->e:[Lcom/google/k/c/hs;

    invoke-virtual {v0}, [Lcom/google/k/c/hs;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/k/c/hs;

    return-object v0
.end method
