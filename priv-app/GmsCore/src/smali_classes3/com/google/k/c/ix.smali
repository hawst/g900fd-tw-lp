.class final Lcom/google/k/c/ix;
.super Lcom/google/k/c/iv;

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field private synthetic b:Lcom/google/k/c/iw;


# direct methods
.method constructor <init>(Lcom/google/k/c/iw;)V
    .locals 0

    iput-object p1, p0, Lcom/google/k/c/ix;->b:Lcom/google/k/c/iw;

    invoke-direct {p0, p1}, Lcom/google/k/c/iv;-><init>(Lcom/google/k/c/iu;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/k/c/iw;I)V
    .locals 1

    iput-object p1, p0, Lcom/google/k/c/ix;->b:Lcom/google/k/c/iw;

    invoke-virtual {p1}, Lcom/google/k/c/iw;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/k/c/iv;-><init>(Lcom/google/k/c/iu;Ljava/util/Iterator;)V

    return-void
.end method

.method private b()Ljava/util/ListIterator;
    .locals 1

    invoke-virtual {p0}, Lcom/google/k/c/iv;->a()V

    iget-object v0, p0, Lcom/google/k/c/iv;->a:Ljava/util/Iterator;

    check-cast v0, Ljava/util/ListIterator;

    return-object v0
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/k/c/ix;->b:Lcom/google/k/c/iw;

    invoke-virtual {v0}, Lcom/google/k/c/iw;->isEmpty()Z

    move-result v0

    invoke-direct {p0}, Lcom/google/k/c/ix;->b()Ljava/util/ListIterator;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/k/c/ix;->b:Lcom/google/k/c/iw;

    iget-object v1, v1, Lcom/google/k/c/iw;->e:Lcom/google/k/c/il;

    invoke-static {v1}, Lcom/google/k/c/il;->c(Lcom/google/k/c/il;)I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/k/c/ix;->b:Lcom/google/k/c/iw;

    invoke-virtual {v0}, Lcom/google/k/c/iw;->c()V

    :cond_0
    return-void
.end method

.method public final hasPrevious()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/k/c/ix;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public final nextIndex()I
    .locals 1

    invoke-direct {p0}, Lcom/google/k/c/ix;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->nextIndex()I

    move-result v0

    return v0
.end method

.method public final previous()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/k/c/ix;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final previousIndex()I
    .locals 1

    invoke-direct {p0}, Lcom/google/k/c/ix;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->previousIndex()I

    move-result v0

    return v0
.end method

.method public final set(Ljava/lang/Object;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/k/c/ix;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    return-void
.end method
