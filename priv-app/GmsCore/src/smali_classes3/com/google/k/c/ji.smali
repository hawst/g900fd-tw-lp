.class final Lcom/google/k/c/ji;
.super Lcom/google/k/c/jw;


# static fields
.field static final a:Lcom/google/k/c/ji;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/k/c/ji;

    invoke-direct {v0}, Lcom/google/k/c/ji;-><init>()V

    sput-object v0, Lcom/google/k/c/ji;->a:Lcom/google/k/c/ji;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/k/c/jw;-><init>()V

    return-void
.end method


# virtual methods
.method public final c()Lcom/google/k/c/gd;
    .locals 1

    invoke-static {}, Lcom/google/k/c/gd;->d()Lcom/google/k/c/gd;

    move-result-object v0

    return-object v0
.end method

.method final d()Lcom/google/k/c/gd;
    .locals 2

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final e()Lcom/google/k/c/gd;
    .locals 1

    invoke-static {}, Lcom/google/k/c/gd;->d()Lcom/google/k/c/gd;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic entrySet()Ljava/util/Set;
    .locals 1

    invoke-static {}, Lcom/google/k/c/gd;->d()Lcom/google/k/c/gd;

    move-result-object v0

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method final h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final i()Lcom/google/k/c/jw;
    .locals 0

    return-object p0
.end method

.method public final isEmpty()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic keySet()Ljava/util/Set;
    .locals 1

    invoke-static {}, Lcom/google/k/c/gd;->d()Lcom/google/k/c/gd;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
