.class final Lcom/google/k/c/ho;
.super Lcom/google/k/c/jv;


# instance fields
.field private final a:Lcom/google/k/c/jx;

.field private final b:Lcom/google/k/c/fp;


# direct methods
.method private constructor <init>(Lcom/google/k/c/jx;Lcom/google/k/c/fp;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/k/c/jv;-><init>()V

    iput-object p1, p0, Lcom/google/k/c/ho;->a:Lcom/google/k/c/jx;

    iput-object p2, p0, Lcom/google/k/c/ho;->b:Lcom/google/k/c/fp;

    return-void
.end method

.method constructor <init>(Lcom/google/k/c/jx;[Ljava/lang/Object;)V
    .locals 1

    invoke-static {p2}, Lcom/google/k/c/fp;->b([Ljava/lang/Object;)Lcom/google/k/c/fp;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/k/c/ho;-><init>(Lcom/google/k/c/jx;Lcom/google/k/c/fp;)V

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/k/c/ik;
    .locals 1

    iget-object v0, p0, Lcom/google/k/c/ho;->b:Lcom/google/k/c/fp;

    invoke-virtual {v0, p1}, Lcom/google/k/c/fp;->a(I)Lcom/google/k/c/ik;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/k/c/ho;->b:Lcom/google/k/c/fp;

    invoke-virtual {v0, p1}, Lcom/google/k/c/fp;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final g()Lcom/google/k/c/jx;
    .locals 1

    iget-object v0, p0, Lcom/google/k/c/ho;->a:Lcom/google/k/c/jx;

    return-object v0
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/k/c/ho;->b:Lcom/google/k/c/fp;

    invoke-virtual {v0, p1}, Lcom/google/k/c/fp;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/k/c/ho;->b:Lcom/google/k/c/fp;

    invoke-virtual {v0}, Lcom/google/k/c/fp;->hashCode()I

    move-result v0

    return v0
.end method

.method public final indexOf(Ljava/lang/Object;)I
    .locals 1

    iget-object v0, p0, Lcom/google/k/c/ho;->b:Lcom/google/k/c/fp;

    invoke-virtual {v0, p1}, Lcom/google/k/c/fp;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    iget-object v0, p0, Lcom/google/k/c/ho;->b:Lcom/google/k/c/fp;

    invoke-virtual {v0, p1}, Lcom/google/k/c/fp;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/k/c/ho;->a(I)Lcom/google/k/c/ik;

    move-result-object v0

    return-object v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/k/c/ho;->b:Lcom/google/k/c/fp;

    invoke-virtual {v0}, Lcom/google/k/c/fp;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/k/c/ho;->b:Lcom/google/k/c/fp;

    invoke-virtual {v0, p1}, Lcom/google/k/c/fp;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
