.class final Lcom/google/k/c/jm;
.super Lcom/google/k/c/gj;


# instance fields
.field private final transient a:Lcom/google/k/c/gl;


# direct methods
.method constructor <init>(Ljava/util/Comparator;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/k/c/gj;-><init>()V

    invoke-static {p1}, Lcom/google/k/c/gl;->a(Ljava/util/Comparator;)Lcom/google/k/c/gl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/jm;->a:Lcom/google/k/c/gl;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/k/c/gj;
    .locals 0

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final b(Ljava/lang/Object;)Lcom/google/k/c/gj;
    .locals 0

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final c()Lcom/google/k/c/gd;
    .locals 1

    invoke-static {}, Lcom/google/k/c/gd;->d()Lcom/google/k/c/gd;

    move-result-object v0

    return-object v0
.end method

.method final d()Lcom/google/k/c/gd;
    .locals 2

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final bridge synthetic e()Lcom/google/k/c/gd;
    .locals 1

    iget-object v0, p0, Lcom/google/k/c/jm;->a:Lcom/google/k/c/gl;

    return-object v0
.end method

.method public final synthetic entrySet()Ljava/util/Set;
    .locals 1

    invoke-static {}, Lcom/google/k/c/gd;->d()Lcom/google/k/c/gd;

    move-result-object v0

    return-object v0
.end method

.method public final g()Lcom/google/k/c/jx;
    .locals 1

    invoke-static {}, Lcom/google/k/c/fp;->aB_()Lcom/google/k/c/fp;

    move-result-object v0

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method final h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final i()Lcom/google/k/c/gl;
    .locals 1

    iget-object v0, p0, Lcom/google/k/c/jm;->a:Lcom/google/k/c/gl;

    return-object v0
.end method

.method public final isEmpty()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final bridge synthetic keySet()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/k/c/jm;->a:Lcom/google/k/c/gl;

    return-object v0
.end method

.method public final size()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    const-string v0, "{}"

    return-object v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .locals 1

    invoke-static {}, Lcom/google/k/c/fp;->aB_()Lcom/google/k/c/fp;

    move-result-object v0

    return-object v0
.end method
