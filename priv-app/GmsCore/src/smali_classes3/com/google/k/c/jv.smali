.class abstract Lcom/google/k/c/jv;
.super Lcom/google/k/c/fp;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/k/c/fp;-><init>()V

    return-void
.end method


# virtual methods
.method final a()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/k/c/jv;->g()Lcom/google/k/c/jx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/jx;->a()Z

    move-result v0

    return v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/k/c/jv;->g()Lcom/google/k/c/jx;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/k/c/jx;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method abstract g()Lcom/google/k/c/jx;
.end method

.method public isEmpty()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/k/c/jv;->g()Lcom/google/k/c/jx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/jx;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public size()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/k/c/jv;->g()Lcom/google/k/c/jx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/jx;->size()I

    move-result v0

    return v0
.end method
