.class public Lcom/google/k/c/gg;
.super Lcom/google/k/c/ga;

# interfaces
.implements Lcom/google/k/c/hj;


# instance fields
.field private final transient b:Lcom/google/k/c/gl;


# direct methods
.method constructor <init>(Lcom/google/k/c/ft;ILjava/util/Comparator;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/k/c/ga;-><init>(Lcom/google/k/c/ft;I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/k/c/gg;->b:Lcom/google/k/c/gl;

    return-void
.end method

.method public static synthetic a(Lcom/google/k/c/hj;)Lcom/google/k/c/gg;
    .locals 6

    invoke-static {p0}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0}, Lcom/google/k/c/hj;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/k/c/jl;->b:Lcom/google/k/c/jl;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    instance-of v0, p0, Lcom/google/k/c/gg;

    if-eqz v0, :cond_2

    move-object v0, p0

    check-cast v0, Lcom/google/k/c/gg;

    iget-object v1, v0, Lcom/google/k/c/ga;->a:Lcom/google/k/c/ft;

    invoke-virtual {v1}, Lcom/google/k/c/ft;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    invoke-static {}, Lcom/google/k/c/ft;->b()Lcom/google/k/c/fu;

    move-result-object v2

    const/4 v0, 0x0

    invoke-interface {p0}, Lcom/google/k/c/hj;->c()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lcom/google/k/c/gd;->a(Ljava/util/Collection;)Lcom/google/k/c/gd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/gd;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    invoke-virtual {v2, v4, v0}, Lcom/google/k/c/fu;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/fu;

    invoke-virtual {v0}, Lcom/google/k/c/gd;->size()I

    move-result v0

    add-int/2addr v0, v1

    :goto_2
    move v1, v0

    goto :goto_1

    :cond_3
    new-instance v0, Lcom/google/k/c/gg;

    invoke-virtual {v2}, Lcom/google/k/c/fu;->a()Lcom/google/k/c/ft;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v2, v1, v3}, Lcom/google/k/c/gg;-><init>(Lcom/google/k/c/ft;ILjava/util/Comparator;)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method private c(Ljava/lang/Object;)Lcom/google/k/c/gd;
    .locals 1

    iget-object v0, p0, Lcom/google/k/c/gg;->a:Lcom/google/k/c/ft;

    invoke-virtual {v0, p1}, Lcom/google/k/c/ft;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/c/gd;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/k/c/gg;->b:Lcom/google/k/c/gl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/k/c/gg;->b:Lcom/google/k/c/gl;

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/k/c/gd;->d()Lcom/google/k/c/gd;

    move-result-object v0

    goto :goto_0
.end method

.method public static f()Lcom/google/k/c/gg;
    .locals 1

    sget-object v0, Lcom/google/k/c/jl;->b:Lcom/google/k/c/jl;

    return-object v0
.end method

.method public static g()Lcom/google/k/c/gh;
    .locals 1

    new-instance v0, Lcom/google/k/c/gh;

    invoke-direct {v0}, Lcom/google/k/c/gh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Lcom/google/k/c/jx;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/k/c/gg;->c(Ljava/lang/Object;)Lcom/google/k/c/gd;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/k/c/gg;->c(Ljava/lang/Object;)Lcom/google/k/c/gd;

    move-result-object v0

    return-object v0
.end method
