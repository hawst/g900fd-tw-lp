.class public abstract Lcom/google/k/c/gl;
.super Lcom/google/k/c/gm;

# interfaces
.implements Lcom/google/k/c/ih;
.implements Ljava/util/SortedSet;


# static fields
.field private static final b:Ljava/util/Comparator;

.field private static final c:Lcom/google/k/c/gl;


# instance fields
.field final transient a:Ljava/util/Comparator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/google/k/c/hn;->b()Lcom/google/k/c/hn;

    move-result-object v0

    sput-object v0, Lcom/google/k/c/gl;->b:Ljava/util/Comparator;

    new-instance v0, Lcom/google/k/c/jn;

    sget-object v1, Lcom/google/k/c/gl;->b:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Lcom/google/k/c/jn;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lcom/google/k/c/gl;->c:Lcom/google/k/c/gl;

    return-void
.end method

.method constructor <init>(Ljava/util/Comparator;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/k/c/gm;-><init>()V

    iput-object p1, p0, Lcom/google/k/c/gl;->a:Ljava/util/Comparator;

    return-void
.end method

.method static a(Ljava/util/Comparator;)Lcom/google/k/c/gl;
    .locals 1

    sget-object v0, Lcom/google/k/c/gl;->b:Ljava/util/Comparator;

    invoke-interface {v0, p0}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/k/c/gl;->c:Lcom/google/k/c/gl;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/k/c/jn;

    invoke-direct {v0, p0}, Lcom/google/k/c/jn;-><init>(Ljava/util/Comparator;)V

    goto :goto_0
.end method


# virtual methods
.method public abstract b()Lcom/google/k/c/ij;
.end method

.method public comparator()Ljava/util/Comparator;
    .locals 1

    iget-object v0, p0, Lcom/google/k/c/gl;->a:Ljava/util/Comparator;

    return-object v0
.end method

.method public first()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/k/c/gl;->b()Lcom/google/k/c/ij;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/ij;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method abstract h()Lcom/google/k/c/gl;
.end method

.method public synthetic headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/k/c/gl;->h()Lcom/google/k/c/gl;

    move-result-object v0

    return-object v0
.end method

.method abstract i()Lcom/google/k/c/gl;
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lcom/google/k/c/gl;->b()Lcom/google/k/c/ij;

    move-result-object v0

    return-object v0
.end method

.method abstract j()Lcom/google/k/c/gl;
.end method

.method public abstract k()Lcom/google/k/c/ij;
.end method

.method public last()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/k/c/gl;->k()Lcom/google/k/c/ij;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/ij;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public synthetic subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/k/c/gl;->a:Ljava/util/Comparator;

    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/k/a/cj;->a(Z)V

    invoke-virtual {p0}, Lcom/google/k/c/gl;->i()Lcom/google/k/c/gl;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/k/c/gl;->j()Lcom/google/k/c/gl;

    move-result-object v0

    return-object v0
.end method
