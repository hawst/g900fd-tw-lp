.class final Lcom/google/k/c/fy;
.super Lcom/google/k/c/jx;


# instance fields
.field private final a:Lcom/google/k/c/ft;


# direct methods
.method constructor <init>(Lcom/google/k/c/ft;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/k/c/jx;-><init>()V

    iput-object p1, p0, Lcom/google/k/c/fy;->a:Lcom/google/k/c/ft;

    return-void
.end method


# virtual methods
.method final a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method final aC_()Lcom/google/k/c/fp;
    .locals 2

    iget-object v0, p0, Lcom/google/k/c/fy;->a:Lcom/google/k/c/ft;

    invoke-virtual {v0}, Lcom/google/k/c/ft;->c()Lcom/google/k/c/gd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/gd;->e()Lcom/google/k/c/fp;

    move-result-object v0

    new-instance v1, Lcom/google/k/c/fz;

    invoke-direct {v1, p0, v0}, Lcom/google/k/c/fz;-><init>(Lcom/google/k/c/fy;Lcom/google/k/c/fp;)V

    return-object v1
.end method

.method public final b()Lcom/google/k/c/ij;
    .locals 1

    iget-object v0, p0, Lcom/google/k/c/fy;->a:Lcom/google/k/c/ft;

    invoke-virtual {v0}, Lcom/google/k/c/ft;->c()Lcom/google/k/c/gd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/gd;->b()Lcom/google/k/c/ij;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/c/hd;->a(Lcom/google/k/c/ij;)Lcom/google/k/c/ij;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/k/c/fy;->a:Lcom/google/k/c/ft;

    invoke-virtual {v0, p1}, Lcom/google/k/c/ft;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lcom/google/k/c/fy;->b()Lcom/google/k/c/ij;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    iget-object v0, p0, Lcom/google/k/c/fy;->a:Lcom/google/k/c/ft;

    invoke-virtual {v0}, Lcom/google/k/c/ft;->size()I

    move-result v0

    return v0
.end method
