.class Lcom/google/k/c/iv;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final a:Ljava/util/Iterator;

.field private b:Ljava/util/Collection;

.field private synthetic c:Lcom/google/k/c/iu;


# direct methods
.method constructor <init>(Lcom/google/k/c/iu;)V
    .locals 1

    iput-object p1, p0, Lcom/google/k/c/iv;->c:Lcom/google/k/c/iu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p0, Lcom/google/k/c/iv;->c:Lcom/google/k/c/iu;

    iget-object v0, v0, Lcom/google/k/c/iu;->b:Ljava/util/Collection;

    iput-object v0, p0, Lcom/google/k/c/iv;->b:Ljava/util/Collection;

    iget-object v0, p1, Lcom/google/k/c/iu;->d:Lcom/google/k/c/il;

    iget-object v0, p1, Lcom/google/k/c/iu;->b:Ljava/util/Collection;

    invoke-static {v0}, Lcom/google/k/c/il;->a(Ljava/util/Collection;)Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/iv;->a:Ljava/util/Iterator;

    return-void
.end method

.method constructor <init>(Lcom/google/k/c/iu;Ljava/util/Iterator;)V
    .locals 1

    iput-object p1, p0, Lcom/google/k/c/iv;->c:Lcom/google/k/c/iu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p0, Lcom/google/k/c/iv;->c:Lcom/google/k/c/iu;

    iget-object v0, v0, Lcom/google/k/c/iu;->b:Ljava/util/Collection;

    iput-object v0, p0, Lcom/google/k/c/iv;->b:Ljava/util/Collection;

    iput-object p2, p0, Lcom/google/k/c/iv;->a:Ljava/util/Iterator;

    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/k/c/iv;->c:Lcom/google/k/c/iu;

    invoke-virtual {v0}, Lcom/google/k/c/iu;->a()V

    iget-object v0, p0, Lcom/google/k/c/iv;->c:Lcom/google/k/c/iu;

    iget-object v0, v0, Lcom/google/k/c/iu;->b:Ljava/util/Collection;

    iget-object v1, p0, Lcom/google/k/c/iv;->b:Ljava/util/Collection;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    :cond_0
    return-void
.end method

.method public hasNext()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/k/c/iv;->a()V

    iget-object v0, p0, Lcom/google/k/c/iv;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/k/c/iv;->a()V

    iget-object v0, p0, Lcom/google/k/c/iv;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    iget-object v0, p0, Lcom/google/k/c/iv;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    iget-object v0, p0, Lcom/google/k/c/iv;->c:Lcom/google/k/c/iu;

    iget-object v0, v0, Lcom/google/k/c/iu;->d:Lcom/google/k/c/il;

    invoke-static {v0}, Lcom/google/k/c/il;->b(Lcom/google/k/c/il;)I

    iget-object v0, p0, Lcom/google/k/c/iv;->c:Lcom/google/k/c/iu;

    invoke-virtual {v0}, Lcom/google/k/c/iu;->b()V

    return-void
.end method
