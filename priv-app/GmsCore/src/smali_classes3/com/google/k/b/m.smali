.class Lcom/google/k/b/m;
.super Ljava/util/AbstractMap;

# interfaces
.implements Ljava/util/concurrent/ConcurrentMap;


# static fields
.field static final a:Ljava/util/logging/Logger;

.field static final b:Lcom/google/k/k/a/bf;

.field static final m:Lcom/google/k/b/bj;

.field static final n:Ljava/util/Queue;


# instance fields
.field final c:[Lcom/google/k/b/aw;

.field final d:Lcom/google/k/a/bw;

.field final e:Lcom/google/k/a/bw;

.field final f:Lcom/google/k/b/az;

.field final g:Lcom/google/k/b/cg;

.field final h:J

.field final i:Ljava/util/Queue;

.field final j:Lcom/google/k/a/df;

.field final k:Lcom/google/k/b/ad;

.field final l:Lcom/google/k/b/j;

.field private o:I

.field private p:I

.field private q:I

.field private r:Lcom/google/k/b/az;

.field private s:J

.field private t:J

.field private u:J

.field private v:Lcom/google/k/b/by;

.field private w:Ljava/util/Set;

.field private x:Ljava/util/Collection;

.field private y:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/k/b/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/k/b/m;->a:Ljava/util/logging/Logger;

    invoke-static {}, Lcom/google/k/k/a/bg;->a()Lcom/google/k/k/a/bf;

    move-result-object v0

    sput-object v0, Lcom/google/k/b/m;->b:Lcom/google/k/k/a/bf;

    new-instance v0, Lcom/google/k/b/n;

    invoke-direct {v0}, Lcom/google/k/b/n;-><init>()V

    sput-object v0, Lcom/google/k/b/m;->m:Lcom/google/k/b/bj;

    new-instance v0, Lcom/google/k/b/o;

    invoke-direct {v0}, Lcom/google/k/b/o;-><init>()V

    sput-object v0, Lcom/google/k/b/m;->n:Ljava/util/Queue;

    return-void
.end method

.method constructor <init>(Lcom/google/k/b/d;Lcom/google/k/b/j;)V
    .locals 8

    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    invoke-virtual {p1}, Lcom/google/k/b/d;->e()I

    move-result v0

    const/high16 v1, 0x10000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/k/b/m;->q:I

    invoke-virtual {p1}, Lcom/google/k/b/d;->i()Lcom/google/k/b/az;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/b/m;->r:Lcom/google/k/b/az;

    invoke-static {}, Lcom/google/k/b/d;->j()Lcom/google/k/b/az;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/b/m;->f:Lcom/google/k/b/az;

    invoke-virtual {p1}, Lcom/google/k/b/d;->b()Lcom/google/k/a/bw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/b/m;->d:Lcom/google/k/a/bw;

    invoke-static {}, Lcom/google/k/b/d;->c()Lcom/google/k/a/bw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/b/m;->e:Lcom/google/k/a/bw;

    invoke-virtual {p1}, Lcom/google/k/b/d;->f()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/k/b/m;->s:J

    invoke-static {}, Lcom/google/k/b/d;->g()Lcom/google/k/b/cg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/b/m;->g:Lcom/google/k/b/cg;

    invoke-virtual {p1}, Lcom/google/k/b/d;->l()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/k/b/m;->t:J

    invoke-virtual {p1}, Lcom/google/k/b/d;->k()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/k/b/m;->u:J

    invoke-virtual {p1}, Lcom/google/k/b/d;->m()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/k/b/m;->h:J

    invoke-static {}, Lcom/google/k/b/d;->n()Lcom/google/k/b/by;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/b/m;->v:Lcom/google/k/b/by;

    iget-object v0, p0, Lcom/google/k/b/m;->v:Lcom/google/k/b/by;

    sget-object v1, Lcom/google/k/b/h;->a:Lcom/google/k/b/h;

    if-ne v0, v1, :cond_5

    sget-object v0, Lcom/google/k/b/m;->n:Ljava/util/Queue;

    :goto_0
    iput-object v0, p0, Lcom/google/k/b/m;->i:Ljava/util/Queue;

    invoke-virtual {p0}, Lcom/google/k/b/m;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/k/b/m;->o()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_0
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/google/k/b/d;->a(Z)Lcom/google/k/a/df;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/b/m;->j:Lcom/google/k/a/df;

    iget-object v2, p0, Lcom/google/k/b/m;->r:Lcom/google/k/b/az;

    invoke-virtual {p0}, Lcom/google/k/b/m;->d()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/k/b/m;->o()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_1
    const/4 v0, 0x1

    :goto_2
    invoke-direct {p0}, Lcom/google/k/b/m;->n()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/google/k/b/m;->f()Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_2
    const/4 v1, 0x1

    :goto_3
    invoke-static {v2, v0, v1}, Lcom/google/k/b/ad;->a(Lcom/google/k/b/az;ZZ)Lcom/google/k/b/ad;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/b/m;->k:Lcom/google/k/b/ad;

    invoke-virtual {p1}, Lcom/google/k/b/d;->o()Lcom/google/k/a/dc;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/k/a/dc;->a()Ljava/lang/Object;

    iput-object p2, p0, Lcom/google/k/b/m;->l:Lcom/google/k/b/j;

    invoke-virtual {p1}, Lcom/google/k/b/d;->d()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/k/b/m;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/k/b/m;->b()Z

    move-result v1

    if-nez v1, :cond_3

    iget-wide v2, p0, Lcom/google/k/b/m;->s:J

    long-to-int v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_3
    const/4 v2, 0x0

    const/4 v1, 0x1

    :goto_4
    iget v3, p0, Lcom/google/k/b/m;->q:I

    if-ge v1, v3, :cond_9

    invoke-virtual {p0}, Lcom/google/k/b/m;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    mul-int/lit8 v3, v1, 0x14

    int-to-long v4, v3

    iget-wide v6, p0, Lcom/google/k/b/m;->s:J

    cmp-long v3, v4, v6

    if-gtz v3, :cond_9

    :cond_4
    add-int/lit8 v2, v2, 0x1

    shl-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_5
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    :cond_8
    const/4 v1, 0x0

    goto :goto_3

    :cond_9
    rsub-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/k/b/m;->p:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/google/k/b/m;->o:I

    new-array v2, v1, [Lcom/google/k/b/aw;

    iput-object v2, p0, Lcom/google/k/b/m;->c:[Lcom/google/k/b/aw;

    div-int v2, v0, v1

    mul-int v3, v2, v1

    if-ge v3, v0, :cond_e

    add-int/lit8 v0, v2, 0x1

    :goto_5
    const/4 v2, 0x1

    move v5, v2

    :goto_6
    if-ge v5, v0, :cond_a

    shl-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_6

    :cond_a
    invoke-virtual {p0}, Lcom/google/k/b/m;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-wide v2, p0, Lcom/google/k/b/m;->s:J

    int-to-long v6, v1

    div-long/2addr v2, v6

    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    iget-wide v6, p0, Lcom/google/k/b/m;->s:J

    int-to-long v0, v1

    rem-long/2addr v6, v0

    const/4 v0, 0x0

    move v4, v0

    move-wide v0, v2

    :goto_7
    iget-object v2, p0, Lcom/google/k/b/m;->c:[Lcom/google/k/b/aw;

    array-length v2, v2

    if-ge v4, v2, :cond_c

    int-to-long v2, v4

    cmp-long v2, v2, v6

    if-nez v2, :cond_d

    const-wide/16 v2, 0x1

    sub-long v2, v0, v2

    :goto_8
    iget-object v1, p0, Lcom/google/k/b/m;->c:[Lcom/google/k/b/aw;

    invoke-virtual {p1}, Lcom/google/k/b/d;->o()Lcom/google/k/a/dc;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/k/a/dc;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/b/c;

    invoke-direct {p0, v5, v2, v3, v0}, Lcom/google/k/b/m;->a(IJLcom/google/k/b/c;)Lcom/google/k/b/aw;

    move-result-object v0

    aput-object v0, v1, v4

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move-wide v0, v2

    goto :goto_7

    :cond_b
    const/4 v0, 0x0

    move v1, v0

    :goto_9
    iget-object v0, p0, Lcom/google/k/b/m;->c:[Lcom/google/k/b/aw;

    array-length v0, v0

    if-ge v1, v0, :cond_c

    iget-object v2, p0, Lcom/google/k/b/m;->c:[Lcom/google/k/b/aw;

    const-wide/16 v6, -0x1

    invoke-virtual {p1}, Lcom/google/k/b/d;->o()Lcom/google/k/a/dc;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/k/a/dc;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/b/c;

    invoke-direct {p0, v5, v6, v7, v0}, Lcom/google/k/b/m;->a(IJLcom/google/k/b/c;)Lcom/google/k/b/aw;

    move-result-object v0

    aput-object v0, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    :cond_c
    return-void

    :cond_d
    move-wide v2, v0

    goto :goto_8

    :cond_e
    move v0, v2

    goto :goto_5
.end method

.method private a(I)Lcom/google/k/b/aw;
    .locals 3

    iget-object v0, p0, Lcom/google/k/b/m;->c:[Lcom/google/k/b/aw;

    iget v1, p0, Lcom/google/k/b/m;->p:I

    ushr-int v1, p1, v1

    iget v2, p0, Lcom/google/k/b/m;->o:I

    and-int/2addr v1, v2

    aget-object v0, v0, v1

    return-object v0
.end method

.method private a(IJLcom/google/k/b/c;)Lcom/google/k/b/aw;
    .locals 8

    new-instance v1, Lcom/google/k/b/aw;

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/k/b/aw;-><init>(Lcom/google/k/b/m;IJLcom/google/k/b/c;)V

    return-object v1
.end method

.method static a(Lcom/google/k/b/av;Lcom/google/k/b/av;)V
    .locals 0

    invoke-interface {p0, p1}, Lcom/google/k/b/av;->a(Lcom/google/k/b/av;)V

    invoke-interface {p1, p0}, Lcom/google/k/b/av;->b(Lcom/google/k/b/av;)V

    return-void
.end method

.method private b(Ljava/lang/Object;)I
    .locals 3

    iget-object v0, p0, Lcom/google/k/b/m;->d:Lcom/google/k/a/bw;

    invoke-virtual {v0, p1}, Lcom/google/k/a/bw;->a(Ljava/lang/Object;)I

    move-result v0

    shl-int/lit8 v1, v0, 0xf

    xor-int/lit16 v1, v1, -0x3283

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0xa

    xor-int/2addr v0, v1

    shl-int/lit8 v1, v0, 0x3

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0x6

    xor-int/2addr v0, v1

    shl-int/lit8 v1, v0, 0x2

    shl-int/lit8 v2, v0, 0xe

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    return v0
.end method

.method static b(Lcom/google/k/b/av;)V
    .locals 1

    sget-object v0, Lcom/google/k/b/au;->a:Lcom/google/k/b/au;

    invoke-interface {p0, v0}, Lcom/google/k/b/av;->a(Lcom/google/k/b/av;)V

    invoke-interface {p0, v0}, Lcom/google/k/b/av;->b(Lcom/google/k/b/av;)V

    return-void
.end method

.method static b(Lcom/google/k/b/av;Lcom/google/k/b/av;)V
    .locals 0

    invoke-interface {p0, p1}, Lcom/google/k/b/av;->c(Lcom/google/k/b/av;)V

    invoke-interface {p1, p0}, Lcom/google/k/b/av;->d(Lcom/google/k/b/av;)V

    return-void
.end method

.method static c(Lcom/google/k/b/av;)V
    .locals 1

    sget-object v0, Lcom/google/k/b/au;->a:Lcom/google/k/b/au;

    invoke-interface {p0, v0}, Lcom/google/k/b/av;->c(Lcom/google/k/b/av;)V

    invoke-interface {p0, v0}, Lcom/google/k/b/av;->d(Lcom/google/k/b/av;)V

    return-void
.end method

.method static j()Lcom/google/k/b/bj;
    .locals 1

    sget-object v0, Lcom/google/k/b/m;->m:Lcom/google/k/b/bj;

    return-object v0
.end method

.method static k()Lcom/google/k/b/av;
    .locals 1

    sget-object v0, Lcom/google/k/b/au;->a:Lcom/google/k/b/au;

    return-object v0
.end method

.method static l()Ljava/util/Queue;
    .locals 1

    sget-object v0, Lcom/google/k/b/m;->n:Ljava/util/Queue;

    return-object v0
.end method

.method private n()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/k/b/m;->u:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/k/b/m;->t:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a(Lcom/google/k/b/av;J)Ljava/lang/Object;
    .locals 4

    const/4 v0, 0x0

    invoke-interface {p1}, Lcom/google/k/b/av;->g()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p1}, Lcom/google/k/b/av;->h()Lcom/google/k/b/bj;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/k/b/bj;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/k/b/m;->b(Lcom/google/k/b/av;J)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lcom/google/k/b/m;->l:Lcom/google/k/b/j;

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/k/b/m;->b(Ljava/lang/Object;)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/k/b/m;->a(I)Lcom/google/k/b/aw;

    move-result-object v2

    invoke-virtual {v2, p1, v1, v0}, Lcom/google/k/b/aw;->a(Ljava/lang/Object;ILcom/google/k/b/j;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final a(Lcom/google/k/b/av;)V
    .locals 2

    invoke-interface {p1}, Lcom/google/k/b/av;->i()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/k/b/m;->a(I)Lcom/google/k/b/aw;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/google/k/b/aw;->a(Lcom/google/k/b/av;I)Z

    return-void
.end method

.method final a(Lcom/google/k/b/bj;)V
    .locals 3

    invoke-interface {p1}, Lcom/google/k/b/bj;->b()Lcom/google/k/b/av;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/k/b/av;->i()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/k/b/m;->a(I)Lcom/google/k/b/aw;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/k/b/av;->g()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0, v1, p1}, Lcom/google/k/b/aw;->a(Ljava/lang/Object;ILcom/google/k/b/bj;)Z

    return-void
.end method

.method final a()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/k/b/m;->s:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b()Z
    .locals 2

    iget-object v0, p0, Lcom/google/k/b/m;->g:Lcom/google/k/b/cg;

    sget-object v1, Lcom/google/k/b/i;->a:Lcom/google/k/b/i;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b(Lcom/google/k/b/av;J)Z
    .locals 6

    const/4 v0, 0x1

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/k/b/m;->o()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Lcom/google/k/b/av;->a()J

    move-result-wide v2

    sub-long v2, p2, v2

    iget-wide v4, p0, Lcom/google/k/b/m;->t:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/google/k/b/m;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Lcom/google/k/b/av;->d()J

    move-result-wide v2

    sub-long v2, p2, v2

    iget-wide v4, p0, Lcom/google/k/b/m;->u:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final c()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/k/b/m;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clear()V
    .locals 4

    iget-object v1, p0, Lcom/google/k/b/m;->c:[Lcom/google/k/b/aw;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lcom/google/k/b/aw;->a()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/k/b/m;->b(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/k/b/m;->a(I)Lcom/google/k/b/aw;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/google/k/b/aw;->b(Ljava/lang/Object;I)Z

    move-result v0

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 20

    if-nez p1, :cond_0

    const/4 v4, 0x0

    :goto_0
    return v4

    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/k/b/m;->j:Lcom/google/k/a/df;

    invoke-virtual {v4}, Lcom/google/k/a/df;->a()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/k/b/m;->c:[Lcom/google/k/b/aw;

    const-wide/16 v8, -0x1

    const/4 v4, 0x0

    move v10, v4

    move-wide v12, v8

    :goto_1
    const/4 v4, 0x3

    if-ge v10, v4, :cond_5

    const-wide/16 v6, 0x0

    array-length v0, v11

    move/from16 v16, v0

    const/4 v4, 0x0

    move-wide v8, v6

    move v6, v4

    :goto_2
    move/from16 v0, v16

    if-ge v6, v0, :cond_4

    aget-object v7, v11, v6

    iget v4, v7, Lcom/google/k/b/aw;->a:I

    iget-object v0, v7, Lcom/google/k/b/aw;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-object/from16 v17, v0

    const/4 v4, 0x0

    move v5, v4

    :goto_3
    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v4

    if-ge v5, v4, :cond_3

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/k/b/av;

    :goto_4
    if-eqz v4, :cond_2

    invoke-virtual {v7, v4, v14, v15}, Lcom/google/k/b/aw;->a(Lcom/google/k/b/av;J)Ljava/lang/Object;

    move-result-object v18

    if-eqz v18, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/k/b/m;->e:Lcom/google/k/a/bw;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/google/k/a/bw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    const/4 v4, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {v4}, Lcom/google/k/b/av;->j()Lcom/google/k/b/av;

    move-result-object v4

    goto :goto_4

    :cond_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_3

    :cond_3
    iget v4, v7, Lcom/google/k/b/aw;->b:I

    int-to-long v4, v4

    add-long/2addr v8, v4

    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_2

    :cond_4
    cmp-long v4, v8, v12

    if-eqz v4, :cond_5

    add-int/lit8 v4, v10, 0x1

    move v10, v4

    move-wide v12, v8

    goto :goto_1

    :cond_5
    const/4 v4, 0x0

    goto :goto_0
.end method

.method final d()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/k/b/m;->o()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/k/b/m;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final e()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/k/b/m;->n()Z

    move-result v0

    return v0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/k/b/m;->y:Ljava/util/Set;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/k/b/an;

    invoke-direct {v0, p0, p0}, Lcom/google/k/b/an;-><init>(Lcom/google/k/b/m;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, Lcom/google/k/b/m;->y:Ljava/util/Set;

    goto :goto_0
.end method

.method final f()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/k/b/m;->n()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/k/b/m;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final g()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/k/b/m;->o()Z

    move-result v0

    return v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/k/b/m;->b(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/k/b/m;->a(I)Lcom/google/k/b/aw;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/google/k/b/aw;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method final h()Z
    .locals 2

    iget-object v0, p0, Lcom/google/k/b/m;->r:Lcom/google/k/b/az;

    sget-object v1, Lcom/google/k/b/az;->a:Lcom/google/k/b/az;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final i()Z
    .locals 2

    iget-object v0, p0, Lcom/google/k/b/m;->f:Lcom/google/k/b/az;

    sget-object v1, Lcom/google/k/b/az;->a:Lcom/google/k/b/az;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 10

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    iget-object v6, p0, Lcom/google/k/b/m;->c:[Lcom/google/k/b/aw;

    move v0, v1

    move-wide v2, v4

    :goto_0
    array-length v7, v6

    if-ge v0, v7, :cond_2

    aget-object v7, v6, v0

    iget v7, v7, Lcom/google/k/b/aw;->a:I

    if-eqz v7, :cond_1

    :cond_0
    :goto_1
    return v1

    :cond_1
    aget-object v7, v6, v0

    iget v7, v7, Lcom/google/k/b/aw;->b:I

    int-to-long v8, v7

    add-long/2addr v2, v8

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    array-length v7, v6

    if-ge v0, v7, :cond_3

    aget-object v7, v6, v0

    iget v7, v7, Lcom/google/k/b/aw;->a:I

    if-nez v7, :cond_0

    aget-object v7, v6, v0

    iget v7, v7, Lcom/google/k/b/aw;->b:I

    int-to-long v8, v7

    sub-long/2addr v2, v8

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    :cond_4
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public keySet()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/k/b/m;->w:Ljava/util/Set;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/k/b/aq;

    invoke-direct {v0, p0, p0}, Lcom/google/k/b/aq;-><init>(Lcom/google/k/b/m;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, Lcom/google/k/b/m;->w:Ljava/util/Set;

    goto :goto_0
.end method

.method final m()V
    .locals 4

    :goto_0
    iget-object v0, p0, Lcom/google/k/b/m;->i:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/b/bz;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/k/b/m;->v:Lcom/google/k/b/by;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/k/b/m;->a:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Exception thrown by removal listener"

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/k/b/m;->b(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/k/b/m;->a(I)Lcom/google/k/b/aw;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, p2, v2}, Lcom/google/k/b/aw;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 3

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/k/b/m;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/k/b/m;->b(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/k/b/m;->a(I)Lcom/google/k/b/aw;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, p2, v2}, Lcom/google/k/b/aw;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/k/b/m;->b(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/k/b/m;->a(I)Lcom/google/k/b/aw;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/google/k/b/aw;->c(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/k/b/m;->b(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/k/b/m;->a(I)Lcom/google/k/b/aw;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lcom/google/k/b/aw;->b(Ljava/lang/Object;ILjava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/k/b/m;->b(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/k/b/m;->a(I)Lcom/google/k/b/aw;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lcom/google/k/b/aw;->a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/k/b/m;->b(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/k/b/m;->a(I)Lcom/google/k/b/aw;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2, p3}, Lcom/google/k/b/aw;->a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public size()I
    .locals 6

    iget-object v3, p0, Lcom/google/k/b/m;->c:[Lcom/google/k/b/aw;

    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v4, v3

    if-ge v2, v4, :cond_0

    aget-object v4, v3, v2

    iget v4, v4, Lcom/google/k/b/aw;->a:I

    int-to-long v4, v4

    add-long/2addr v0, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v0, v1}, Lcom/google/k/h/c;->a(J)I

    move-result v0

    return v0
.end method

.method public values()Ljava/util/Collection;
    .locals 1

    iget-object v0, p0, Lcom/google/k/b/m;->x:Ljava/util/Collection;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/k/b/bk;

    invoke-direct {v0, p0, p0}, Lcom/google/k/b/bk;-><init>(Lcom/google/k/b/m;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, Lcom/google/k/b/m;->x:Ljava/util/Collection;

    goto :goto_0
.end method
