.class abstract enum Lcom/google/k/b/az;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/google/k/b/az;

.field public static final enum b:Lcom/google/k/b/az;

.field private static enum c:Lcom/google/k/b/az;

.field private static final synthetic d:[Lcom/google/k/b/az;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/k/b/ba;

    const-string v1, "STRONG"

    invoke-direct {v0, v1}, Lcom/google/k/b/ba;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/k/b/az;->a:Lcom/google/k/b/az;

    new-instance v0, Lcom/google/k/b/bb;

    const-string v1, "SOFT"

    invoke-direct {v0, v1}, Lcom/google/k/b/bb;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/k/b/az;->c:Lcom/google/k/b/az;

    new-instance v0, Lcom/google/k/b/bc;

    const-string v1, "WEAK"

    invoke-direct {v0, v1}, Lcom/google/k/b/bc;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/k/b/az;->b:Lcom/google/k/b/az;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/k/b/az;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/k/b/az;->a:Lcom/google/k/b/az;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/k/b/az;->c:Lcom/google/k/b/az;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/k/b/az;->b:Lcom/google/k/b/az;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/k/b/az;->d:[Lcom/google/k/b/az;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/k/b/az;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/k/b/az;
    .locals 1

    const-class v0, Lcom/google/k/b/az;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/k/b/az;

    return-object v0
.end method

.method public static values()[Lcom/google/k/b/az;
    .locals 1

    sget-object v0, Lcom/google/k/b/az;->d:[Lcom/google/k/b/az;

    invoke-virtual {v0}, [Lcom/google/k/b/az;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/k/b/az;

    return-object v0
.end method


# virtual methods
.method abstract a()Lcom/google/k/a/bw;
.end method

.method abstract a(Lcom/google/k/b/aw;Lcom/google/k/b/av;Ljava/lang/Object;)Lcom/google/k/b/bj;
.end method
