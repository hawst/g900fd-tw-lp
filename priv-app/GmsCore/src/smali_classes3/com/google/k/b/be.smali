.class final Lcom/google/k/b/be;
.super Lcom/google/k/b/bf;

# interfaces
.implements Lcom/google/k/b/av;


# instance fields
.field private volatile a:J

.field private b:Lcom/google/k/b/av;

.field private c:Lcom/google/k/b/av;

.field private volatile d:J

.field private e:Lcom/google/k/b/av;

.field private f:Lcom/google/k/b/av;


# direct methods
.method constructor <init>(Ljava/lang/Object;ILcom/google/k/b/av;)V
    .locals 4

    const-wide v2, 0x7fffffffffffffffL

    invoke-direct {p0, p1, p2, p3}, Lcom/google/k/b/bf;-><init>(Ljava/lang/Object;ILcom/google/k/b/av;)V

    iput-wide v2, p0, Lcom/google/k/b/be;->a:J

    invoke-static {}, Lcom/google/k/b/m;->k()Lcom/google/k/b/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/b/be;->b:Lcom/google/k/b/av;

    invoke-static {}, Lcom/google/k/b/m;->k()Lcom/google/k/b/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/b/be;->c:Lcom/google/k/b/av;

    iput-wide v2, p0, Lcom/google/k/b/be;->d:J

    invoke-static {}, Lcom/google/k/b/m;->k()Lcom/google/k/b/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/b/be;->e:Lcom/google/k/b/av;

    invoke-static {}, Lcom/google/k/b/m;->k()Lcom/google/k/b/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/b/be;->f:Lcom/google/k/b/av;

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    iget-wide v0, p0, Lcom/google/k/b/be;->a:J

    return-wide v0
.end method

.method public final a(J)V
    .locals 1

    iput-wide p1, p0, Lcom/google/k/b/be;->a:J

    return-void
.end method

.method public final a(Lcom/google/k/b/av;)V
    .locals 0

    iput-object p1, p0, Lcom/google/k/b/be;->b:Lcom/google/k/b/av;

    return-void
.end method

.method public final b()Lcom/google/k/b/av;
    .locals 1

    iget-object v0, p0, Lcom/google/k/b/be;->b:Lcom/google/k/b/av;

    return-object v0
.end method

.method public final b(J)V
    .locals 1

    iput-wide p1, p0, Lcom/google/k/b/be;->d:J

    return-void
.end method

.method public final b(Lcom/google/k/b/av;)V
    .locals 0

    iput-object p1, p0, Lcom/google/k/b/be;->c:Lcom/google/k/b/av;

    return-void
.end method

.method public final c()Lcom/google/k/b/av;
    .locals 1

    iget-object v0, p0, Lcom/google/k/b/be;->c:Lcom/google/k/b/av;

    return-object v0
.end method

.method public final c(Lcom/google/k/b/av;)V
    .locals 0

    iput-object p1, p0, Lcom/google/k/b/be;->e:Lcom/google/k/b/av;

    return-void
.end method

.method public final d()J
    .locals 2

    iget-wide v0, p0, Lcom/google/k/b/be;->d:J

    return-wide v0
.end method

.method public final d(Lcom/google/k/b/av;)V
    .locals 0

    iput-object p1, p0, Lcom/google/k/b/be;->f:Lcom/google/k/b/av;

    return-void
.end method

.method public final e()Lcom/google/k/b/av;
    .locals 1

    iget-object v0, p0, Lcom/google/k/b/be;->e:Lcom/google/k/b/av;

    return-object v0
.end method

.method public final f()Lcom/google/k/b/av;
    .locals 1

    iget-object v0, p0, Lcom/google/k/b/be;->f:Lcom/google/k/b/av;

    return-object v0
.end method
