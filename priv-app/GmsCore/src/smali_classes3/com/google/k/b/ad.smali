.class abstract enum Lcom/google/k/b/ad;
.super Ljava/lang/Enum;


# static fields
.field private static enum a:Lcom/google/k/b/ad;

.field private static enum b:Lcom/google/k/b/ad;

.field private static enum c:Lcom/google/k/b/ad;

.field private static enum d:Lcom/google/k/b/ad;

.field private static enum e:Lcom/google/k/b/ad;

.field private static enum f:Lcom/google/k/b/ad;

.field private static enum g:Lcom/google/k/b/ad;

.field private static enum h:Lcom/google/k/b/ad;

.field private static i:[Lcom/google/k/b/ad;

.field private static final synthetic j:[Lcom/google/k/b/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/k/b/ae;

    const-string v1, "STRONG"

    invoke-direct {v0, v1}, Lcom/google/k/b/ae;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/k/b/ad;->a:Lcom/google/k/b/ad;

    new-instance v0, Lcom/google/k/b/af;

    const-string v1, "STRONG_ACCESS"

    invoke-direct {v0, v1}, Lcom/google/k/b/af;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/k/b/ad;->b:Lcom/google/k/b/ad;

    new-instance v0, Lcom/google/k/b/ag;

    const-string v1, "STRONG_WRITE"

    invoke-direct {v0, v1}, Lcom/google/k/b/ag;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/k/b/ad;->c:Lcom/google/k/b/ad;

    new-instance v0, Lcom/google/k/b/ah;

    const-string v1, "STRONG_ACCESS_WRITE"

    invoke-direct {v0, v1}, Lcom/google/k/b/ah;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/k/b/ad;->d:Lcom/google/k/b/ad;

    new-instance v0, Lcom/google/k/b/ai;

    const-string v1, "WEAK"

    invoke-direct {v0, v1}, Lcom/google/k/b/ai;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/k/b/ad;->e:Lcom/google/k/b/ad;

    new-instance v0, Lcom/google/k/b/aj;

    const-string v1, "WEAK_ACCESS"

    invoke-direct {v0, v1}, Lcom/google/k/b/aj;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/k/b/ad;->f:Lcom/google/k/b/ad;

    new-instance v0, Lcom/google/k/b/ak;

    const-string v1, "WEAK_WRITE"

    invoke-direct {v0, v1}, Lcom/google/k/b/ak;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/k/b/ad;->g:Lcom/google/k/b/ad;

    new-instance v0, Lcom/google/k/b/al;

    const-string v1, "WEAK_ACCESS_WRITE"

    invoke-direct {v0, v1}, Lcom/google/k/b/al;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/k/b/ad;->h:Lcom/google/k/b/ad;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/k/b/ad;

    sget-object v1, Lcom/google/k/b/ad;->a:Lcom/google/k/b/ad;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/k/b/ad;->b:Lcom/google/k/b/ad;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/k/b/ad;->c:Lcom/google/k/b/ad;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/k/b/ad;->d:Lcom/google/k/b/ad;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/k/b/ad;->e:Lcom/google/k/b/ad;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/k/b/ad;->f:Lcom/google/k/b/ad;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/k/b/ad;->g:Lcom/google/k/b/ad;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/k/b/ad;->h:Lcom/google/k/b/ad;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/k/b/ad;->j:[Lcom/google/k/b/ad;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/k/b/ad;

    sget-object v1, Lcom/google/k/b/ad;->a:Lcom/google/k/b/ad;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/k/b/ad;->b:Lcom/google/k/b/ad;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/k/b/ad;->c:Lcom/google/k/b/ad;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/k/b/ad;->d:Lcom/google/k/b/ad;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/k/b/ad;->e:Lcom/google/k/b/ad;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/k/b/ad;->f:Lcom/google/k/b/ad;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/k/b/ad;->g:Lcom/google/k/b/ad;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/k/b/ad;->h:Lcom/google/k/b/ad;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/k/b/ad;->i:[Lcom/google/k/b/ad;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/k/b/ad;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(Lcom/google/k/b/az;ZZ)Lcom/google/k/b/ad;
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lcom/google/k/b/az;->b:Lcom/google/k/b/az;

    if-ne p0, v1, :cond_1

    const/4 v1, 0x4

    move v2, v1

    :goto_0
    if-eqz p1, :cond_2

    const/4 v1, 0x1

    :goto_1
    or-int/2addr v1, v2

    if-eqz p2, :cond_0

    const/4 v0, 0x2

    :cond_0
    or-int/2addr v0, v1

    sget-object v1, Lcom/google/k/b/ad;->i:[Lcom/google/k/b/ad;

    aget-object v0, v1, v0

    return-object v0

    :cond_1
    move v2, v0

    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_1
.end method

.method static a(Lcom/google/k/b/av;Lcom/google/k/b/av;)V
    .locals 2

    invoke-interface {p0}, Lcom/google/k/b/av;->a()J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, Lcom/google/k/b/av;->a(J)V

    invoke-interface {p0}, Lcom/google/k/b/av;->c()Lcom/google/k/b/av;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/k/b/m;->a(Lcom/google/k/b/av;Lcom/google/k/b/av;)V

    invoke-interface {p0}, Lcom/google/k/b/av;->b()Lcom/google/k/b/av;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/k/b/m;->a(Lcom/google/k/b/av;Lcom/google/k/b/av;)V

    invoke-static {p0}, Lcom/google/k/b/m;->b(Lcom/google/k/b/av;)V

    return-void
.end method

.method static b(Lcom/google/k/b/av;Lcom/google/k/b/av;)V
    .locals 2

    invoke-interface {p0}, Lcom/google/k/b/av;->d()J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, Lcom/google/k/b/av;->b(J)V

    invoke-interface {p0}, Lcom/google/k/b/av;->f()Lcom/google/k/b/av;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/k/b/m;->b(Lcom/google/k/b/av;Lcom/google/k/b/av;)V

    invoke-interface {p0}, Lcom/google/k/b/av;->e()Lcom/google/k/b/av;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/k/b/m;->b(Lcom/google/k/b/av;Lcom/google/k/b/av;)V

    invoke-static {p0}, Lcom/google/k/b/m;->c(Lcom/google/k/b/av;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/k/b/ad;
    .locals 1

    const-class v0, Lcom/google/k/b/ad;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/k/b/ad;

    return-object v0
.end method

.method public static values()[Lcom/google/k/b/ad;
    .locals 1

    sget-object v0, Lcom/google/k/b/ad;->j:[Lcom/google/k/b/ad;

    invoke-virtual {v0}, [Lcom/google/k/b/ad;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/k/b/ad;

    return-object v0
.end method


# virtual methods
.method a(Lcom/google/k/b/aw;Lcom/google/k/b/av;Lcom/google/k/b/av;)Lcom/google/k/b/av;
    .locals 2

    invoke-interface {p2}, Lcom/google/k/b/av;->g()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, Lcom/google/k/b/av;->i()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, Lcom/google/k/b/ad;->a(Lcom/google/k/b/aw;Ljava/lang/Object;ILcom/google/k/b/av;)Lcom/google/k/b/av;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Lcom/google/k/b/aw;Ljava/lang/Object;ILcom/google/k/b/av;)Lcom/google/k/b/av;
.end method
