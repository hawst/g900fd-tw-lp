.class abstract Lcom/google/k/b/ao;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private a:I

.field private b:I

.field private c:Lcom/google/k/b/aw;

.field private d:Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field private e:Lcom/google/k/b/av;

.field private f:Lcom/google/k/b/y;

.field private g:Lcom/google/k/b/y;

.field private synthetic h:Lcom/google/k/b/m;


# direct methods
.method constructor <init>(Lcom/google/k/b/m;)V
    .locals 1

    iput-object p1, p0, Lcom/google/k/b/ao;->h:Lcom/google/k/b/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/google/k/b/m;->c:[Lcom/google/k/b/aw;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/k/b/ao;->a:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/b/ao;->b:I

    invoke-direct {p0}, Lcom/google/k/b/ao;->b()V

    return-void
.end method

.method private a(Lcom/google/k/b/av;)Z
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/google/k/b/ao;->h:Lcom/google/k/b/m;

    iget-object v0, v0, Lcom/google/k/b/m;->j:Lcom/google/k/a/df;

    invoke-virtual {v0}, Lcom/google/k/a/df;->a()J

    move-result-wide v0

    invoke-interface {p1}, Lcom/google/k/b/av;->g()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lcom/google/k/b/ao;->h:Lcom/google/k/b/m;

    invoke-virtual {v3, p1, v0, v1}, Lcom/google/k/b/m;->a(Lcom/google/k/b/av;J)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/k/b/y;

    iget-object v3, p0, Lcom/google/k/b/ao;->h:Lcom/google/k/b/m;

    invoke-direct {v1, v2, v0}, Lcom/google/k/b/y;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/google/k/b/ao;->f:Lcom/google/k/b/y;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/k/b/ao;->c:Lcom/google/k/b/aw;

    invoke-virtual {v0}, Lcom/google/k/b/aw;->b()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/k/b/ao;->c:Lcom/google/k/b/aw;

    invoke-virtual {v0}, Lcom/google/k/b/aw;->b()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/k/b/ao;->c:Lcom/google/k/b/aw;

    invoke-virtual {v1}, Lcom/google/k/b/aw;->b()V

    throw v0
.end method

.method private b()V
    .locals 3

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/k/b/ao;->f:Lcom/google/k/b/y;

    invoke-direct {p0}, Lcom/google/k/b/ao;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/k/b/ao;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    iget v0, p0, Lcom/google/k/b/ao;->a:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/k/b/ao;->h:Lcom/google/k/b/m;

    iget-object v0, v0, Lcom/google/k/b/m;->c:[Lcom/google/k/b/aw;

    iget v1, p0, Lcom/google/k/b/ao;->a:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/google/k/b/ao;->a:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/k/b/ao;->c:Lcom/google/k/b/aw;

    iget-object v0, p0, Lcom/google/k/b/ao;->c:Lcom/google/k/b/aw;

    iget v0, v0, Lcom/google/k/b/aw;->a:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/k/b/ao;->c:Lcom/google/k/b/aw;

    iget-object v0, v0, Lcom/google/k/b/aw;->c:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput-object v0, p0, Lcom/google/k/b/ao;->d:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget-object v0, p0, Lcom/google/k/b/ao;->d:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/k/b/ao;->b:I

    invoke-direct {p0}, Lcom/google/k/b/ao;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0
.end method

.method private c()Z
    .locals 1

    iget-object v0, p0, Lcom/google/k/b/ao;->e:Lcom/google/k/b/av;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/k/b/ao;->e:Lcom/google/k/b/av;

    invoke-interface {v0}, Lcom/google/k/b/av;->j()Lcom/google/k/b/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/b/ao;->e:Lcom/google/k/b/av;

    :goto_0
    iget-object v0, p0, Lcom/google/k/b/ao;->e:Lcom/google/k/b/av;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/k/b/ao;->e:Lcom/google/k/b/av;

    invoke-direct {p0, v0}, Lcom/google/k/b/ao;->a(Lcom/google/k/b/av;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/k/b/ao;->e:Lcom/google/k/b/av;

    invoke-interface {v0}, Lcom/google/k/b/av;->j()Lcom/google/k/b/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/b/ao;->e:Lcom/google/k/b/av;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private d()Z
    .locals 3

    :cond_0
    iget v0, p0, Lcom/google/k/b/ao;->b:I

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/google/k/b/ao;->d:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget v1, p0, Lcom/google/k/b/ao;->b:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/google/k/b/ao;->b:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/b/av;

    iput-object v0, p0, Lcom/google/k/b/ao;->e:Lcom/google/k/b/av;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/k/b/ao;->e:Lcom/google/k/b/av;

    invoke-direct {p0, v0}, Lcom/google/k/b/ao;->a(Lcom/google/k/b/av;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/k/b/ao;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a()Lcom/google/k/b/y;
    .locals 1

    iget-object v0, p0, Lcom/google/k/b/ao;->f:Lcom/google/k/b/y;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/k/b/ao;->f:Lcom/google/k/b/y;

    iput-object v0, p0, Lcom/google/k/b/ao;->g:Lcom/google/k/b/y;

    invoke-direct {p0}, Lcom/google/k/b/ao;->b()V

    iget-object v0, p0, Lcom/google/k/b/ao;->g:Lcom/google/k/b/y;

    return-object v0
.end method

.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lcom/google/k/b/ao;->f:Lcom/google/k/b/y;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public remove()V
    .locals 2

    iget-object v0, p0, Lcom/google/k/b/ao;->g:Lcom/google/k/b/y;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/k/a/cj;->b(Z)V

    iget-object v0, p0, Lcom/google/k/b/ao;->h:Lcom/google/k/b/m;

    iget-object v1, p0, Lcom/google/k/b/ao;->g:Lcom/google/k/b/y;

    invoke-virtual {v1}, Lcom/google/k/b/y;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/k/b/m;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/k/b/ao;->g:Lcom/google/k/b/y;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
