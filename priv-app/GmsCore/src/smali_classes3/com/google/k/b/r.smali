.class Lcom/google/k/b/r;
.super Ljava/lang/ref/WeakReference;

# interfaces
.implements Lcom/google/k/b/av;


# instance fields
.field private a:I

.field private b:Lcom/google/k/b/av;

.field private volatile c:Lcom/google/k/b/bj;


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILcom/google/k/b/av;)V
    .locals 1

    invoke-direct {p0, p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    invoke-static {}, Lcom/google/k/b/m;->j()Lcom/google/k/b/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/b/r;->c:Lcom/google/k/b/bj;

    iput p3, p0, Lcom/google/k/b/r;->a:I

    iput-object p4, p0, Lcom/google/k/b/r;->b:Lcom/google/k/b/av;

    return-void
.end method


# virtual methods
.method public a()J
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(J)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lcom/google/k/b/av;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/google/k/b/bj;)V
    .locals 0

    iput-object p1, p0, Lcom/google/k/b/r;->c:Lcom/google/k/b/bj;

    return-void
.end method

.method public b()Lcom/google/k/b/av;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(J)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(Lcom/google/k/b/av;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c()Lcom/google/k/b/av;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c(Lcom/google/k/b/av;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d()J
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d(Lcom/google/k/b/av;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()Lcom/google/k/b/av;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public f()Lcom/google/k/b/av;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final g()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/k/b/r;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/google/k/b/bj;
    .locals 1

    iget-object v0, p0, Lcom/google/k/b/r;->c:Lcom/google/k/b/bj;

    return-object v0
.end method

.method public final i()I
    .locals 1

    iget v0, p0, Lcom/google/k/b/r;->a:I

    return v0
.end method

.method public final j()Lcom/google/k/b/av;
    .locals 1

    iget-object v0, p0, Lcom/google/k/b/r;->b:Lcom/google/k/b/av;

    return-object v0
.end method
