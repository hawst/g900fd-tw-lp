.class final Lcom/google/k/e/o;
.super Lcom/google/k/a/bi;


# instance fields
.field final c:I

.field final d:I

.field final e:I

.field final f:I

.field private final g:Ljava/lang/String;

.field private final h:[C

.field private final i:[B


# direct methods
.method constructor <init>(Ljava/lang/String;[C)V
    .locals 10

    const/16 v4, 0x8

    const/4 v9, -0x1

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/k/a/bi;-><init>()V

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/k/e/o;->g:Ljava/lang/String;

    invoke-static {p2}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [C

    iput-object v0, p0, Lcom/google/k/e/o;->h:[C

    :try_start_0
    array-length v0, p2

    sget-object v2, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-static {v0, v2}, Lcom/google/k/g/c;->a(ILjava/math/RoundingMode;)I

    move-result v0

    iput v0, p0, Lcom/google/k/e/o;->d:I
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    iget v0, p0, Lcom/google/k/e/o;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->lowestOneBit(I)I

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    div-int v2, v4, v0

    iput v2, p0, Lcom/google/k/e/o;->e:I

    iget v2, p0, Lcom/google/k/e/o;->d:I

    div-int v0, v2, v0

    iput v0, p0, Lcom/google/k/e/o;->f:I

    array-length v0, p2

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/k/e/o;->c:I

    const/16 v0, 0x80

    new-array v4, v0, [B

    invoke-static {v4, v9}, Ljava/util/Arrays;->fill([BB)V

    move v0, v1

    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_1

    aget-char v5, p2, v0

    sget-object v2, Lcom/google/k/a/bi;->a:Lcom/google/k/a/bi;

    invoke-virtual {v2, v5}, Lcom/google/k/a/bi;->a(C)Z

    move-result v2

    const-string v6, "Non-ASCII character: %s"

    new-array v7, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-static {v2, v6, v7}, Lcom/google/k/a/cj;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    aget-byte v2, v4, v5

    if-ne v2, v9, :cond_0

    move v2, v3

    :goto_1
    const-string v6, "Duplicate character: %s"

    new-array v7, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-static {v2, v6, v7}, Lcom/google/k/a/cj;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    int-to-byte v2, v0

    aput-byte v2, v4, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Illegal alphabet length "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, p2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    move v2, v1

    goto :goto_1

    :cond_1
    iput-object v4, p0, Lcom/google/k/e/o;->i:[B

    iget v0, p0, Lcom/google/k/e/o;->e:I

    new-array v0, v0, [Z

    :goto_2
    iget v2, p0, Lcom/google/k/e/o;->f:I

    if-ge v1, v2, :cond_2

    mul-int/lit8 v2, v1, 0x8

    iget v4, p0, Lcom/google/k/e/o;->d:I

    sget-object v5, Ljava/math/RoundingMode;->CEILING:Ljava/math/RoundingMode;

    invoke-static {v2, v4, v5}, Lcom/google/k/g/c;->a(IILjava/math/RoundingMode;)I

    move-result v2

    aput-boolean v3, v0, v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    return-void
.end method


# virtual methods
.method final a(I)C
    .locals 1

    iget-object v0, p0, Lcom/google/k/e/o;->h:[C

    aget-char v0, v0, p1

    return v0
.end method

.method public final a(C)Z
    .locals 2

    sget-object v0, Lcom/google/k/a/bi;->a:Lcom/google/k/a/bi;

    invoke-virtual {v0, p1}, Lcom/google/k/a/bi;->a(C)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/k/e/o;->i:[B

    aget-byte v0, v0, p1

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/k/e/o;->g:Ljava/lang/String;

    return-object v0
.end method
