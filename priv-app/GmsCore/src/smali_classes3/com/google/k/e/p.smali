.class final Lcom/google/k/e/p;
.super Lcom/google/k/e/n;


# instance fields
.field private final a:Lcom/google/k/e/o;

.field private final b:Ljava/lang/Character;


# direct methods
.method private constructor <init>(Lcom/google/k/e/o;Ljava/lang/Character;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/k/e/n;-><init>()V

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/e/o;

    iput-object v0, p0, Lcom/google/k/e/p;->a:Lcom/google/k/e/o;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/k/e/o;->a(C)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Padding character %s was already in alphabet"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v1

    invoke-static {v0, v3, v2}, Lcom/google/k/a/cj;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/google/k/e/p;->b:Ljava/lang/Character;

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V
    .locals 2

    new-instance v0, Lcom/google/k/e/o;

    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/k/e/o;-><init>(Ljava/lang/String;[C)V

    invoke-direct {p0, v0, p3}, Lcom/google/k/e/p;-><init>(Lcom/google/k/e/o;Ljava/lang/Character;)V

    return-void
.end method

.method static synthetic a(Lcom/google/k/e/p;)Lcom/google/k/e/o;
    .locals 1

    iget-object v0, p0, Lcom/google/k/e/p;->a:Lcom/google/k/e/o;

    return-object v0
.end method

.method static synthetic b(Lcom/google/k/e/p;)Ljava/lang/Character;
    .locals 1

    iget-object v0, p0, Lcom/google/k/e/p;->b:Ljava/lang/Character;

    return-object v0
.end method


# virtual methods
.method final a(I)I
    .locals 3

    iget-object v0, p0, Lcom/google/k/e/p;->a:Lcom/google/k/e/o;

    iget v0, v0, Lcom/google/k/e/o;->e:I

    iget-object v1, p0, Lcom/google/k/e/p;->a:Lcom/google/k/e/o;

    iget v1, v1, Lcom/google/k/e/o;->f:I

    sget-object v2, Ljava/math/RoundingMode;->CEILING:Ljava/math/RoundingMode;

    invoke-static {p1, v1, v2}, Lcom/google/k/g/c;->a(IILjava/math/RoundingMode;)I

    move-result v1

    mul-int/2addr v0, v1

    return v0
.end method

.method final a(Lcom/google/k/e/u;)Lcom/google/k/e/t;
    .locals 1

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/k/e/q;

    invoke-direct {v0, p0, p1}, Lcom/google/k/e/q;-><init>(Lcom/google/k/e/p;Lcom/google/k/e/u;)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BaseEncoding."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/k/e/p;->a:Lcom/google/k/e/o;

    invoke-virtual {v1}, Lcom/google/k/e/o;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/k/e/p;->a:Lcom/google/k/e/o;

    iget v2, v2, Lcom/google/k/e/o;->d:I

    rem-int/2addr v1, v2

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/k/e/p;->b:Ljava/lang/Character;

    if-nez v1, :cond_1

    const-string v1, ".omitPadding()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const-string v1, ".withPadChar("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/k/e/p;->b:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x29

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
