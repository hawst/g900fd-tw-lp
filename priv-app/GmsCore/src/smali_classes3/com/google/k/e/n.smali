.class public abstract Lcom/google/k/e/n;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lcom/google/k/e/n;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v4, 0x3d

    new-instance v0, Lcom/google/k/e/p;

    const-string v1, "base64()"

    const-string v2, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/k/e/p;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V

    new-instance v0, Lcom/google/k/e/p;

    const-string v1, "base64Url()"

    const-string v2, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/k/e/p;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V

    sput-object v0, Lcom/google/k/e/n;->a:Lcom/google/k/e/n;

    new-instance v0, Lcom/google/k/e/p;

    const-string v1, "base32()"

    const-string v2, "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/k/e/p;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V

    new-instance v0, Lcom/google/k/e/p;

    const-string v1, "base32Hex()"

    const-string v2, "0123456789ABCDEFGHIJKLMNOPQRSTUV"

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/k/e/p;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V

    new-instance v0, Lcom/google/k/e/p;

    const-string v1, "base16()"

    const-string v2, "0123456789ABCDEF"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/k/e/p;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/google/k/e/n;
    .locals 1

    sget-object v0, Lcom/google/k/e/n;->a:Lcom/google/k/e/n;

    return-object v0
.end method

.method private a([BI)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, p2, 0x0

    array-length v2, p1

    invoke-static {v0, v1, v2}, Lcom/google/k/a/cj;->a(III)V

    invoke-virtual {p0, p2}, Lcom/google/k/e/n;->a(I)I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    new-instance v1, Lcom/google/k/e/s;

    invoke-direct {v1, v2}, Lcom/google/k/e/s;-><init>(Ljava/lang/StringBuilder;)V

    invoke-virtual {p0, v1}, Lcom/google/k/e/n;->a(Lcom/google/k/e/u;)Lcom/google/k/e/t;

    move-result-object v2

    :goto_0
    if-ge v0, p2, :cond_0

    add-int/lit8 v3, v0, 0x0

    :try_start_0
    aget-byte v3, p1, v3

    invoke-interface {v2, v3}, Lcom/google/k/e/t;->a(B)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Lcom/google/k/e/t;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "impossible"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method


# virtual methods
.method abstract a(I)I
.end method

.method abstract a(Lcom/google/k/e/u;)Lcom/google/k/e/t;
.end method

.method public final a([B)Ljava/lang/String;
    .locals 2

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    array-length v1, p1

    invoke-direct {p0, v0, v1}, Lcom/google/k/e/n;->a([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
