.class final Lcom/google/k/e/q;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/k/e/t;


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private synthetic d:Lcom/google/k/e/u;

.field private synthetic e:Lcom/google/k/e/p;


# direct methods
.method constructor <init>(Lcom/google/k/e/p;Lcom/google/k/e/u;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/google/k/e/q;->e:Lcom/google/k/e/p;

    iput-object p2, p0, Lcom/google/k/e/q;->d:Lcom/google/k/e/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/k/e/q;->a:I

    iput v0, p0, Lcom/google/k/e/q;->b:I

    iput v0, p0, Lcom/google/k/e/q;->c:I

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget v0, p0, Lcom/google/k/e/q;->b:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/k/e/q;->a:I

    iget-object v1, p0, Lcom/google/k/e/q;->e:Lcom/google/k/e/p;

    invoke-static {v1}, Lcom/google/k/e/p;->a(Lcom/google/k/e/p;)Lcom/google/k/e/o;

    move-result-object v1

    iget v1, v1, Lcom/google/k/e/o;->d:I

    iget v2, p0, Lcom/google/k/e/q;->b:I

    sub-int/2addr v1, v2

    shl-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/k/e/q;->e:Lcom/google/k/e/p;

    invoke-static {v1}, Lcom/google/k/e/p;->a(Lcom/google/k/e/p;)Lcom/google/k/e/o;

    move-result-object v1

    iget v1, v1, Lcom/google/k/e/o;->c:I

    and-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/k/e/q;->d:Lcom/google/k/e/u;

    iget-object v2, p0, Lcom/google/k/e/q;->e:Lcom/google/k/e/p;

    invoke-static {v2}, Lcom/google/k/e/p;->a(Lcom/google/k/e/p;)Lcom/google/k/e/o;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/k/e/o;->a(I)C

    move-result v0

    invoke-interface {v1, v0}, Lcom/google/k/e/u;->a(C)V

    iget v0, p0, Lcom/google/k/e/q;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/k/e/q;->c:I

    iget-object v0, p0, Lcom/google/k/e/q;->e:Lcom/google/k/e/p;

    invoke-static {v0}, Lcom/google/k/e/p;->b(Lcom/google/k/e/p;)Ljava/lang/Character;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    iget v0, p0, Lcom/google/k/e/q;->c:I

    iget-object v1, p0, Lcom/google/k/e/q;->e:Lcom/google/k/e/p;

    invoke-static {v1}, Lcom/google/k/e/p;->a(Lcom/google/k/e/p;)Lcom/google/k/e/o;

    move-result-object v1

    iget v1, v1, Lcom/google/k/e/o;->e:I

    rem-int/2addr v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/k/e/q;->d:Lcom/google/k/e/u;

    iget-object v1, p0, Lcom/google/k/e/q;->e:Lcom/google/k/e/p;

    invoke-static {v1}, Lcom/google/k/e/p;->b(Lcom/google/k/e/p;)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Character;->charValue()C

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/k/e/u;->a(C)V

    iget v0, p0, Lcom/google/k/e/q;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/k/e/q;->c:I

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/k/e/q;->d:Lcom/google/k/e/u;

    return-void
.end method

.method public final a(B)V
    .locals 3

    iget v0, p0, Lcom/google/k/e/q;->a:I

    shl-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/k/e/q;->a:I

    iget v0, p0, Lcom/google/k/e/q;->a:I

    and-int/lit16 v1, p1, 0xff

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/k/e/q;->a:I

    iget v0, p0, Lcom/google/k/e/q;->b:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/k/e/q;->b:I

    :goto_0
    iget v0, p0, Lcom/google/k/e/q;->b:I

    iget-object v1, p0, Lcom/google/k/e/q;->e:Lcom/google/k/e/p;

    invoke-static {v1}, Lcom/google/k/e/p;->a(Lcom/google/k/e/p;)Lcom/google/k/e/o;

    move-result-object v1

    iget v1, v1, Lcom/google/k/e/o;->d:I

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/google/k/e/q;->a:I

    iget v1, p0, Lcom/google/k/e/q;->b:I

    iget-object v2, p0, Lcom/google/k/e/q;->e:Lcom/google/k/e/p;

    invoke-static {v2}, Lcom/google/k/e/p;->a(Lcom/google/k/e/p;)Lcom/google/k/e/o;

    move-result-object v2

    iget v2, v2, Lcom/google/k/e/o;->d:I

    sub-int/2addr v1, v2

    shr-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/k/e/q;->e:Lcom/google/k/e/p;

    invoke-static {v1}, Lcom/google/k/e/p;->a(Lcom/google/k/e/p;)Lcom/google/k/e/o;

    move-result-object v1

    iget v1, v1, Lcom/google/k/e/o;->c:I

    and-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/k/e/q;->d:Lcom/google/k/e/u;

    iget-object v2, p0, Lcom/google/k/e/q;->e:Lcom/google/k/e/p;

    invoke-static {v2}, Lcom/google/k/e/p;->a(Lcom/google/k/e/p;)Lcom/google/k/e/o;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/k/e/o;->a(I)C

    move-result v0

    invoke-interface {v1, v0}, Lcom/google/k/e/u;->a(C)V

    iget v0, p0, Lcom/google/k/e/q;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/k/e/q;->c:I

    iget v0, p0, Lcom/google/k/e/q;->b:I

    iget-object v1, p0, Lcom/google/k/e/q;->e:Lcom/google/k/e/p;

    invoke-static {v1}, Lcom/google/k/e/p;->a(Lcom/google/k/e/p;)Lcom/google/k/e/o;

    move-result-object v1

    iget v1, v1, Lcom/google/k/e/o;->d:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/k/e/q;->b:I

    goto :goto_0

    :cond_0
    return-void
.end method
