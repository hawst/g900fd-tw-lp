.class public final Lf/a/a/b;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Long;

.field public e:Lcom/google/ac/d/a/b;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/Boolean;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Lcom/google/ac/a/a/b;

.field public o:Ljava/lang/String;

.field public p:Lf/a/a/f;

.field public q:Lf/a/a/c;

.field public r:Lf/a/a/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 464
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 465
    iput-object v0, p0, Lf/a/a/b;->a:Ljava/lang/String;

    iput-object v0, p0, Lf/a/a/b;->b:Ljava/lang/String;

    iput-object v0, p0, Lf/a/a/b;->c:Ljava/lang/String;

    iput-object v0, p0, Lf/a/a/b;->d:Ljava/lang/Long;

    iput-object v0, p0, Lf/a/a/b;->e:Lcom/google/ac/d/a/b;

    iput-object v0, p0, Lf/a/a/b;->f:Ljava/lang/String;

    iput-object v0, p0, Lf/a/a/b;->g:Ljava/lang/String;

    iput-object v0, p0, Lf/a/a/b;->h:Ljava/lang/String;

    iput-object v0, p0, Lf/a/a/b;->i:Ljava/lang/String;

    iput-object v0, p0, Lf/a/a/b;->j:Ljava/lang/Integer;

    iput-object v0, p0, Lf/a/a/b;->k:Ljava/lang/Boolean;

    iput-object v0, p0, Lf/a/a/b;->l:Ljava/lang/String;

    iput-object v0, p0, Lf/a/a/b;->m:Ljava/lang/String;

    iput-object v0, p0, Lf/a/a/b;->n:Lcom/google/ac/a/a/b;

    iput-object v0, p0, Lf/a/a/b;->o:Ljava/lang/String;

    iput-object v0, p0, Lf/a/a/b;->p:Lf/a/a/f;

    iput-object v0, p0, Lf/a/a/b;->q:Lf/a/a/c;

    iput-object v0, p0, Lf/a/a/b;->r:Lf/a/a/d;

    iput-object v0, p0, Lf/a/a/b;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lf/a/a/b;->cachedSize:I

    .line 466
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 554
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 555
    iget-object v1, p0, Lf/a/a/b;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 556
    const/4 v1, 0x1

    iget-object v2, p0, Lf/a/a/b;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 559
    :cond_0
    iget-object v1, p0, Lf/a/a/b;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 560
    const/4 v1, 0x2

    iget-object v2, p0, Lf/a/a/b;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 563
    :cond_1
    iget-object v1, p0, Lf/a/a/b;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 564
    const/4 v1, 0x3

    iget-object v2, p0, Lf/a/a/b;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 567
    :cond_2
    iget-object v1, p0, Lf/a/a/b;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 568
    const/4 v1, 0x4

    iget-object v2, p0, Lf/a/a/b;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 571
    :cond_3
    iget-object v1, p0, Lf/a/a/b;->e:Lcom/google/ac/d/a/b;

    if-eqz v1, :cond_4

    .line 572
    const/4 v1, 0x5

    iget-object v2, p0, Lf/a/a/b;->e:Lcom/google/ac/d/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 575
    :cond_4
    iget-object v1, p0, Lf/a/a/b;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 576
    const/4 v1, 0x6

    iget-object v2, p0, Lf/a/a/b;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 579
    :cond_5
    iget-object v1, p0, Lf/a/a/b;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 580
    const/4 v1, 0x7

    iget-object v2, p0, Lf/a/a/b;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 583
    :cond_6
    iget-object v1, p0, Lf/a/a/b;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 584
    const/16 v1, 0x8

    iget-object v2, p0, Lf/a/a/b;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 587
    :cond_7
    iget-object v1, p0, Lf/a/a/b;->i:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 588
    const/16 v1, 0x9

    iget-object v2, p0, Lf/a/a/b;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 591
    :cond_8
    iget-object v1, p0, Lf/a/a/b;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 592
    const/16 v1, 0xa

    iget-object v2, p0, Lf/a/a/b;->j:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 595
    :cond_9
    iget-object v1, p0, Lf/a/a/b;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 596
    const/16 v1, 0xb

    iget-object v2, p0, Lf/a/a/b;->k:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 599
    :cond_a
    iget-object v1, p0, Lf/a/a/b;->l:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 600
    const/16 v1, 0xc

    iget-object v2, p0, Lf/a/a/b;->l:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 603
    :cond_b
    iget-object v1, p0, Lf/a/a/b;->m:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 604
    const/16 v1, 0xd

    iget-object v2, p0, Lf/a/a/b;->m:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 607
    :cond_c
    iget-object v1, p0, Lf/a/a/b;->n:Lcom/google/ac/a/a/b;

    if-eqz v1, :cond_d

    .line 608
    const/16 v1, 0xe

    iget-object v2, p0, Lf/a/a/b;->n:Lcom/google/ac/a/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 611
    :cond_d
    iget-object v1, p0, Lf/a/a/b;->o:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 612
    const/16 v1, 0xf

    iget-object v2, p0, Lf/a/a/b;->o:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 615
    :cond_e
    iget-object v1, p0, Lf/a/a/b;->p:Lf/a/a/f;

    if-eqz v1, :cond_f

    .line 616
    const/16 v1, 0x10

    iget-object v2, p0, Lf/a/a/b;->p:Lf/a/a/f;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 619
    :cond_f
    iget-object v1, p0, Lf/a/a/b;->q:Lf/a/a/c;

    if-eqz v1, :cond_10

    .line 620
    const/16 v1, 0x11

    iget-object v2, p0, Lf/a/a/b;->q:Lf/a/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 623
    :cond_10
    iget-object v1, p0, Lf/a/a/b;->r:Lf/a/a/d;

    if-eqz v1, :cond_11

    .line 624
    const/16 v1, 0x12

    iget-object v2, p0, Lf/a/a/b;->r:Lf/a/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 627
    :cond_11
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 393
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lf/a/a/b;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/b;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/b;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/b;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/b;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lf/a/a/b;->e:Lcom/google/ac/d/a/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/d/a/b;

    invoke-direct {v0}, Lcom/google/ac/d/a/b;-><init>()V

    iput-object v0, p0, Lf/a/a/b;->e:Lcom/google/ac/d/a/b;

    :cond_1
    iget-object v0, p0, Lf/a/a/b;->e:Lcom/google/ac/d/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/b;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/b;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/b;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/b;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/b;->j:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/b;->k:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/b;->l:Ljava/lang/String;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/b;->m:Ljava/lang/String;

    goto :goto_0

    :sswitch_e
    iget-object v0, p0, Lf/a/a/b;->n:Lcom/google/ac/a/a/b;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/a/a/b;

    invoke-direct {v0}, Lcom/google/ac/a/a/b;-><init>()V

    iput-object v0, p0, Lf/a/a/b;->n:Lcom/google/ac/a/a/b;

    :cond_2
    iget-object v0, p0, Lf/a/a/b;->n:Lcom/google/ac/a/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/b;->o:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Lf/a/a/b;->p:Lf/a/a/f;

    if-nez v0, :cond_3

    new-instance v0, Lf/a/a/f;

    invoke-direct {v0}, Lf/a/a/f;-><init>()V

    iput-object v0, p0, Lf/a/a/b;->p:Lf/a/a/f;

    :cond_3
    iget-object v0, p0, Lf/a/a/b;->p:Lf/a/a/f;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_11
    iget-object v0, p0, Lf/a/a/b;->q:Lf/a/a/c;

    if-nez v0, :cond_4

    new-instance v0, Lf/a/a/c;

    invoke-direct {v0}, Lf/a/a/c;-><init>()V

    iput-object v0, p0, Lf/a/a/b;->q:Lf/a/a/c;

    :cond_4
    iget-object v0, p0, Lf/a/a/b;->q:Lf/a/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_12
    iget-object v0, p0, Lf/a/a/b;->r:Lf/a/a/d;

    if-nez v0, :cond_5

    new-instance v0, Lf/a/a/d;

    invoke-direct {v0}, Lf/a/a/d;-><init>()V

    iput-object v0, p0, Lf/a/a/b;->r:Lf/a/a/d;

    :cond_5
    iget-object v0, p0, Lf/a/a/b;->r:Lf/a/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 495
    iget-object v0, p0, Lf/a/a/b;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 496
    const/4 v0, 0x1

    iget-object v1, p0, Lf/a/a/b;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 498
    :cond_0
    iget-object v0, p0, Lf/a/a/b;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 499
    const/4 v0, 0x2

    iget-object v1, p0, Lf/a/a/b;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 501
    :cond_1
    iget-object v0, p0, Lf/a/a/b;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 502
    const/4 v0, 0x3

    iget-object v1, p0, Lf/a/a/b;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 504
    :cond_2
    iget-object v0, p0, Lf/a/a/b;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 505
    const/4 v0, 0x4

    iget-object v1, p0, Lf/a/a/b;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 507
    :cond_3
    iget-object v0, p0, Lf/a/a/b;->e:Lcom/google/ac/d/a/b;

    if-eqz v0, :cond_4

    .line 508
    const/4 v0, 0x5

    iget-object v1, p0, Lf/a/a/b;->e:Lcom/google/ac/d/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 510
    :cond_4
    iget-object v0, p0, Lf/a/a/b;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 511
    const/4 v0, 0x6

    iget-object v1, p0, Lf/a/a/b;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 513
    :cond_5
    iget-object v0, p0, Lf/a/a/b;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 514
    const/4 v0, 0x7

    iget-object v1, p0, Lf/a/a/b;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 516
    :cond_6
    iget-object v0, p0, Lf/a/a/b;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 517
    const/16 v0, 0x8

    iget-object v1, p0, Lf/a/a/b;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 519
    :cond_7
    iget-object v0, p0, Lf/a/a/b;->i:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 520
    const/16 v0, 0x9

    iget-object v1, p0, Lf/a/a/b;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 522
    :cond_8
    iget-object v0, p0, Lf/a/a/b;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 523
    const/16 v0, 0xa

    iget-object v1, p0, Lf/a/a/b;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 525
    :cond_9
    iget-object v0, p0, Lf/a/a/b;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 526
    const/16 v0, 0xb

    iget-object v1, p0, Lf/a/a/b;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 528
    :cond_a
    iget-object v0, p0, Lf/a/a/b;->l:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 529
    const/16 v0, 0xc

    iget-object v1, p0, Lf/a/a/b;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 531
    :cond_b
    iget-object v0, p0, Lf/a/a/b;->m:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 532
    const/16 v0, 0xd

    iget-object v1, p0, Lf/a/a/b;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 534
    :cond_c
    iget-object v0, p0, Lf/a/a/b;->n:Lcom/google/ac/a/a/b;

    if-eqz v0, :cond_d

    .line 535
    const/16 v0, 0xe

    iget-object v1, p0, Lf/a/a/b;->n:Lcom/google/ac/a/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 537
    :cond_d
    iget-object v0, p0, Lf/a/a/b;->o:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 538
    const/16 v0, 0xf

    iget-object v1, p0, Lf/a/a/b;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 540
    :cond_e
    iget-object v0, p0, Lf/a/a/b;->p:Lf/a/a/f;

    if-eqz v0, :cond_f

    .line 541
    const/16 v0, 0x10

    iget-object v1, p0, Lf/a/a/b;->p:Lf/a/a/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 543
    :cond_f
    iget-object v0, p0, Lf/a/a/b;->q:Lf/a/a/c;

    if-eqz v0, :cond_10

    .line 544
    const/16 v0, 0x11

    iget-object v1, p0, Lf/a/a/b;->q:Lf/a/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 546
    :cond_10
    iget-object v0, p0, Lf/a/a/b;->r:Lf/a/a/d;

    if-eqz v0, :cond_11

    .line 547
    const/16 v0, 0x12

    iget-object v1, p0, Lf/a/a/b;->r:Lf/a/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 549
    :cond_11
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 550
    return-void
.end method
