.class public final Lc/a;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile h:[Lc/a;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:[Ljava/lang/String;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 49
    iput-object v1, p0, Lc/a;->a:Ljava/lang/String;

    iput-object v1, p0, Lc/a;->b:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lc/a;->c:[Ljava/lang/String;

    iput-object v1, p0, Lc/a;->d:Ljava/lang/Integer;

    iput-object v1, p0, Lc/a;->e:Ljava/lang/String;

    iput-object v1, p0, Lc/a;->f:Ljava/lang/String;

    iput-object v1, p0, Lc/a;->g:Ljava/lang/String;

    iput-object v1, p0, Lc/a;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lc/a;->cachedSize:I

    .line 50
    return-void
.end method

.method public static a()[Lc/a;
    .locals 2

    .prologue
    .line 16
    sget-object v0, Lc/a;->h:[Lc/a;

    if-nez v0, :cond_1

    .line 17
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 19
    :try_start_0
    sget-object v0, Lc/a;->h:[Lc/a;

    if-nez v0, :cond_0

    .line 20
    const/4 v0, 0x0

    new-array v0, v0, [Lc/a;

    sput-object v0, Lc/a;->h:[Lc/a;

    .line 22
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 24
    :cond_1
    sget-object v0, Lc/a;->h:[Lc/a;

    return-object v0

    .line 22
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 176
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 177
    iget-object v2, p0, Lc/a;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 178
    const/4 v2, 0x1

    iget-object v3, p0, Lc/a;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 181
    :cond_0
    iget-object v2, p0, Lc/a;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 182
    const/4 v2, 0x2

    iget-object v3, p0, Lc/a;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 185
    :cond_1
    iget-object v2, p0, Lc/a;->c:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lc/a;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    move v3, v1

    .line 188
    :goto_0
    iget-object v4, p0, Lc/a;->c:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 189
    iget-object v4, p0, Lc/a;->c:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 190
    if-eqz v4, :cond_2

    .line 191
    add-int/lit8 v3, v3, 0x1

    .line 192
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 188
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 196
    :cond_3
    add-int/2addr v0, v2

    .line 197
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 199
    :cond_4
    iget-object v1, p0, Lc/a;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 200
    const/4 v1, 0x4

    iget-object v2, p0, Lc/a;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 203
    :cond_5
    iget-object v1, p0, Lc/a;->e:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 204
    const/4 v1, 0x5

    iget-object v2, p0, Lc/a;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 207
    :cond_6
    iget-object v1, p0, Lc/a;->f:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 208
    const/4 v1, 0x6

    iget-object v2, p0, Lc/a;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 211
    :cond_7
    iget-object v1, p0, Lc/a;->g:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 212
    const/4 v1, 0x7

    iget-object v2, p0, Lc/a;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 215
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 67
    if-ne p1, p0, :cond_1

    .line 68
    const/4 v0, 0x1

    .line 119
    :cond_0
    :goto_0
    return v0

    .line 70
    :cond_1
    instance-of v1, p1, Lc/a;

    if-eqz v1, :cond_0

    .line 73
    check-cast p1, Lc/a;

    .line 74
    iget-object v1, p0, Lc/a;->a:Ljava/lang/String;

    if-nez v1, :cond_8

    .line 75
    iget-object v1, p1, Lc/a;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 81
    :cond_2
    iget-object v1, p0, Lc/a;->b:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 82
    iget-object v1, p1, Lc/a;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 88
    :cond_3
    iget-object v1, p0, Lc/a;->c:[Ljava/lang/String;

    iget-object v2, p1, Lc/a;->c:[Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 92
    iget-object v1, p0, Lc/a;->d:Ljava/lang/Integer;

    if-nez v1, :cond_a

    .line 93
    iget-object v1, p1, Lc/a;->d:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 98
    :cond_4
    iget-object v1, p0, Lc/a;->e:Ljava/lang/String;

    if-nez v1, :cond_b

    .line 99
    iget-object v1, p1, Lc/a;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 105
    :cond_5
    iget-object v1, p0, Lc/a;->f:Ljava/lang/String;

    if-nez v1, :cond_c

    .line 106
    iget-object v1, p1, Lc/a;->f:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 112
    :cond_6
    iget-object v1, p0, Lc/a;->g:Ljava/lang/String;

    if-nez v1, :cond_d

    .line 113
    iget-object v1, p1, Lc/a;->g:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 119
    :cond_7
    invoke-virtual {p0, p1}, Lc/a;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 78
    :cond_8
    iget-object v1, p0, Lc/a;->a:Ljava/lang/String;

    iget-object v2, p1, Lc/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 85
    :cond_9
    iget-object v1, p0, Lc/a;->b:Ljava/lang/String;

    iget-object v2, p1, Lc/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 96
    :cond_a
    iget-object v1, p0, Lc/a;->d:Ljava/lang/Integer;

    iget-object v2, p1, Lc/a;->d:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 102
    :cond_b
    iget-object v1, p0, Lc/a;->e:Ljava/lang/String;

    iget-object v2, p1, Lc/a;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 109
    :cond_c
    iget-object v1, p0, Lc/a;->f:Ljava/lang/String;

    iget-object v2, p1, Lc/a;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0

    .line 116
    :cond_d
    iget-object v1, p0, Lc/a;->g:Ljava/lang/String;

    iget-object v2, p1, Lc/a;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 124
    iget-object v0, p0, Lc/a;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 127
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lc/a;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 129
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lc/a;->c:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 131
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lc/a;->d:Ljava/lang/Integer;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 132
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lc/a;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 134
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lc/a;->f:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 136
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lc/a;->g:Ljava/lang/String;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 138
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lc/a;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    return v0

    .line 124
    :cond_0
    iget-object v0, p0, Lc/a;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 127
    :cond_1
    iget-object v0, p0, Lc/a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 131
    :cond_2
    iget-object v0, p0, Lc/a;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_2

    .line 132
    :cond_3
    iget-object v0, p0, Lc/a;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 134
    :cond_4
    iget-object v0, p0, Lc/a;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 136
    :cond_5
    iget-object v1, p0, Lc/a;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lc/a;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lc/a;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lc/a;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lc/a;->c:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lc/a;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lc/a;->c:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lc/a;->c:[Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lc/a;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lc/a;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lc/a;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lc/a;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 145
    iget-object v0, p0, Lc/a;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 146
    const/4 v0, 0x1

    iget-object v1, p0, Lc/a;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 148
    :cond_0
    iget-object v0, p0, Lc/a;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 149
    const/4 v0, 0x2

    iget-object v1, p0, Lc/a;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 151
    :cond_1
    iget-object v0, p0, Lc/a;->c:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lc/a;->c:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 152
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lc/a;->c:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 153
    iget-object v1, p0, Lc/a;->c:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 154
    if-eqz v1, :cond_2

    .line 155
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 152
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 159
    :cond_3
    iget-object v0, p0, Lc/a;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 160
    const/4 v0, 0x4

    iget-object v1, p0, Lc/a;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 162
    :cond_4
    iget-object v0, p0, Lc/a;->e:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 163
    const/4 v0, 0x5

    iget-object v1, p0, Lc/a;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 165
    :cond_5
    iget-object v0, p0, Lc/a;->f:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 166
    const/4 v0, 0x6

    iget-object v1, p0, Lc/a;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 168
    :cond_6
    iget-object v0, p0, Lc/a;->g:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 169
    const/4 v0, 0x7

    iget-object v1, p0, Lc/a;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 171
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 172
    return-void
.end method
