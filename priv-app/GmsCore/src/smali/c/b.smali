.class public final Lc/b;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/protobuf/nano/e;

.field private static final e:[Lc/b;


# instance fields
.field public b:Ljava/lang/String;

.field public c:[Lc/a;

.field public d:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    const-class v0, Lc/b;

    const v1, 0x15ec49c2

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/e;->a(Ljava/lang/Class;I)Lcom/google/protobuf/nano/e;

    move-result-object v0

    sput-object v0, Lc/b;->a:Lcom/google/protobuf/nano/e;

    .line 29
    const/4 v0, 0x0

    new-array v0, v0, [Lc/b;

    sput-object v0, Lc/b;->e:[Lc/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 44
    iput-object v1, p0, Lc/b;->b:Ljava/lang/String;

    invoke-static {}, Lc/a;->a()[Lc/a;

    move-result-object v0

    iput-object v0, p0, Lc/b;->c:[Lc/a;

    iput-object v1, p0, Lc/b;->d:Ljava/lang/Integer;

    iput-object v1, p0, Lc/b;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lc/b;->cachedSize:I

    .line 45
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 119
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 120
    iget-object v1, p0, Lc/b;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 121
    const/4 v1, 0x1

    iget-object v2, p0, Lc/b;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    :cond_0
    iget-object v1, p0, Lc/b;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 125
    const/4 v1, 0x2

    iget-object v2, p0, Lc/b;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 128
    :cond_1
    iget-object v1, p0, Lc/b;->c:[Lc/a;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lc/b;->c:[Lc/a;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 129
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lc/b;->c:[Lc/a;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 130
    iget-object v2, p0, Lc/b;->c:[Lc/a;

    aget-object v2, v2, v0

    .line 131
    if-eqz v2, :cond_2

    .line 132
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 129
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 137
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 58
    if-ne p1, p0, :cond_1

    .line 59
    const/4 v0, 0x1

    .line 82
    :cond_0
    :goto_0
    return v0

    .line 61
    :cond_1
    instance-of v1, p1, Lc/b;

    if-eqz v1, :cond_0

    .line 64
    check-cast p1, Lc/b;

    .line 65
    iget-object v1, p0, Lc/b;->b:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 66
    iget-object v1, p1, Lc/b;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 72
    :cond_2
    iget-object v1, p0, Lc/b;->c:[Lc/a;

    iget-object v2, p1, Lc/b;->c:[Lc/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    iget-object v1, p0, Lc/b;->d:Ljava/lang/Integer;

    if-nez v1, :cond_5

    .line 77
    iget-object v1, p1, Lc/b;->d:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 82
    :cond_3
    invoke-virtual {p0, p1}, Lc/b;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 69
    :cond_4
    iget-object v1, p0, Lc/b;->b:Ljava/lang/String;

    iget-object v2, p1, Lc/b;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 80
    :cond_5
    iget-object v1, p0, Lc/b;->d:Ljava/lang/Integer;

    iget-object v2, p1, Lc/b;->d:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 87
    iget-object v0, p0, Lc/b;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 90
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lc/b;->c:[Lc/a;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 92
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lc/b;->d:Ljava/lang/Integer;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 93
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lc/b;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 94
    return v0

    .line 87
    :cond_0
    iget-object v0, p0, Lc/b;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 92
    :cond_1
    iget-object v1, p0, Lc/b;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lc/b;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lc/b;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lc/b;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lc/b;->c:[Lc/a;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lc/a;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lc/b;->c:[Lc/a;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lc/a;

    invoke-direct {v3}, Lc/a;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lc/b;->c:[Lc/a;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lc/a;

    invoke-direct {v3}, Lc/a;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lc/b;->c:[Lc/a;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x190 -> :sswitch_2
        0x193 -> :sswitch_2
        0x194 -> :sswitch_2
        0x199 -> :sswitch_2
        0x19a -> :sswitch_2
        0x19c -> :sswitch_2
        0x1f4 -> :sswitch_2
        0x1f7 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 100
    iget-object v0, p0, Lc/b;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 101
    const/4 v0, 0x1

    iget-object v1, p0, Lc/b;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 103
    :cond_0
    iget-object v0, p0, Lc/b;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 104
    const/4 v0, 0x2

    iget-object v1, p0, Lc/b;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 106
    :cond_1
    iget-object v0, p0, Lc/b;->c:[Lc/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lc/b;->c:[Lc/a;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 107
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lc/b;->c:[Lc/a;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 108
    iget-object v1, p0, Lc/b;->c:[Lc/a;

    aget-object v1, v1, v0

    .line 109
    if-eqz v1, :cond_2

    .line 110
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 107
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 114
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 115
    return-void
.end method
