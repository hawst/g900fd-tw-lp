.class public final Lcom/google/s/a/h;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public apiHeader:Lcom/google/s/a/d;

.field public b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3652
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3653
    iput-object v0, p0, Lcom/google/s/a/h;->apiHeader:Lcom/google/s/a/d;

    iput-object v0, p0, Lcom/google/s/a/h;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/s/a/h;->b:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/s/a/h;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/s/a/h;->cachedSize:I

    .line 3654
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3682
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3683
    iget-object v1, p0, Lcom/google/s/a/h;->apiHeader:Lcom/google/s/a/d;

    if-eqz v1, :cond_0

    .line 3684
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/s/a/h;->apiHeader:Lcom/google/s/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3687
    :cond_0
    iget-object v1, p0, Lcom/google/s/a/h;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3688
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/s/a/h;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3691
    :cond_1
    iget-object v1, p0, Lcom/google/s/a/h;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 3692
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/s/a/h;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3695
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3626
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/s/a/h;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/s/a/h;->apiHeader:Lcom/google/s/a/d;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/s/a/d;

    invoke-direct {v0}, Lcom/google/s/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/s/a/h;->apiHeader:Lcom/google/s/a/d;

    :cond_1
    iget-object v0, p0, Lcom/google/s/a/h;->apiHeader:Lcom/google/s/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/h;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/h;->b:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3668
    iget-object v0, p0, Lcom/google/s/a/h;->apiHeader:Lcom/google/s/a/d;

    if-eqz v0, :cond_0

    .line 3669
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/s/a/h;->apiHeader:Lcom/google/s/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3671
    :cond_0
    iget-object v0, p0, Lcom/google/s/a/h;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3672
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/s/a/h;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3674
    :cond_1
    iget-object v0, p0, Lcom/google/s/a/h;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 3675
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/s/a/h;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 3677
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3678
    return-void
.end method
