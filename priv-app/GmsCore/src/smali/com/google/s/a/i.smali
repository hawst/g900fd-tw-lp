.class public final Lcom/google/s/a/i;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/s/a/z;

.field public apiHeader:Lcom/google/s/a/e;

.field public b:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3770
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3771
    iput-object v1, p0, Lcom/google/s/a/i;->apiHeader:Lcom/google/s/a/e;

    invoke-static {}, Lcom/google/s/a/z;->a()[Lcom/google/s/a/z;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/i;->a:[Lcom/google/s/a/z;

    iput-object v1, p0, Lcom/google/s/a/i;->b:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/s/a/i;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/s/a/i;->cachedSize:I

    .line 3772
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 3805
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3806
    iget-object v1, p0, Lcom/google/s/a/i;->apiHeader:Lcom/google/s/a/e;

    if-eqz v1, :cond_0

    .line 3807
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/s/a/i;->apiHeader:Lcom/google/s/a/e;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3810
    :cond_0
    iget-object v1, p0, Lcom/google/s/a/i;->a:[Lcom/google/s/a/z;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/s/a/i;->a:[Lcom/google/s/a/z;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 3811
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/s/a/i;->a:[Lcom/google/s/a/z;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 3812
    iget-object v2, p0, Lcom/google/s/a/i;->a:[Lcom/google/s/a/z;

    aget-object v2, v2, v0

    .line 3813
    if-eqz v2, :cond_1

    .line 3814
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3811
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 3819
    :cond_3
    iget-object v1, p0, Lcom/google/s/a/i;->b:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 3820
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/s/a/i;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3823
    :cond_4
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3744
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/s/a/i;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/s/a/i;->apiHeader:Lcom/google/s/a/e;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/s/a/e;

    invoke-direct {v0}, Lcom/google/s/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/s/a/i;->apiHeader:Lcom/google/s/a/e;

    :cond_1
    iget-object v0, p0, Lcom/google/s/a/i;->apiHeader:Lcom/google/s/a/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/s/a/i;->a:[Lcom/google/s/a/z;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/s/a/z;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/s/a/i;->a:[Lcom/google/s/a/z;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/s/a/z;

    invoke-direct {v3}, Lcom/google/s/a/z;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/s/a/i;->a:[Lcom/google/s/a/z;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/s/a/z;

    invoke-direct {v3}, Lcom/google/s/a/z;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/s/a/i;->a:[Lcom/google/s/a/z;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/i;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 3786
    iget-object v0, p0, Lcom/google/s/a/i;->apiHeader:Lcom/google/s/a/e;

    if-eqz v0, :cond_0

    .line 3787
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/s/a/i;->apiHeader:Lcom/google/s/a/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3789
    :cond_0
    iget-object v0, p0, Lcom/google/s/a/i;->a:[Lcom/google/s/a/z;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/s/a/i;->a:[Lcom/google/s/a/z;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 3790
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/s/a/i;->a:[Lcom/google/s/a/z;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 3791
    iget-object v1, p0, Lcom/google/s/a/i;->a:[Lcom/google/s/a/z;

    aget-object v1, v1, v0

    .line 3792
    if-eqz v1, :cond_1

    .line 3793
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3790
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3797
    :cond_2
    iget-object v0, p0, Lcom/google/s/a/i;->b:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 3798
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/s/a/i;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 3800
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3801
    return-void
.end method
