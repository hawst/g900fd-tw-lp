.class public final Lcom/google/s/a/r;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/s/a/r;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Lcom/google/s/a/n;

.field public c:[Lcom/google/s/a/q;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1328
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1329
    iput-object v1, p0, Lcom/google/s/a/r;->a:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/s/a/r;->b:Lcom/google/s/a/n;

    invoke-static {}, Lcom/google/s/a/q;->a()[Lcom/google/s/a/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/r;->c:[Lcom/google/s/a/q;

    iput-object v1, p0, Lcom/google/s/a/r;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/s/a/r;->cachedSize:I

    .line 1330
    return-void
.end method

.method public static a()[Lcom/google/s/a/r;
    .locals 2

    .prologue
    .line 1308
    sget-object v0, Lcom/google/s/a/r;->d:[Lcom/google/s/a/r;

    if-nez v0, :cond_1

    .line 1309
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1311
    :try_start_0
    sget-object v0, Lcom/google/s/a/r;->d:[Lcom/google/s/a/r;

    if-nez v0, :cond_0

    .line 1312
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/s/a/r;

    sput-object v0, Lcom/google/s/a/r;->d:[Lcom/google/s/a/r;

    .line 1314
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1316
    :cond_1
    sget-object v0, Lcom/google/s/a/r;->d:[Lcom/google/s/a/r;

    return-object v0

    .line 1314
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 1363
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1364
    iget-object v1, p0, Lcom/google/s/a/r;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1365
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/s/a/r;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1368
    :cond_0
    iget-object v1, p0, Lcom/google/s/a/r;->b:Lcom/google/s/a/n;

    if-eqz v1, :cond_1

    .line 1369
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/s/a/r;->b:Lcom/google/s/a/n;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1372
    :cond_1
    iget-object v1, p0, Lcom/google/s/a/r;->c:[Lcom/google/s/a/q;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/s/a/r;->c:[Lcom/google/s/a/q;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 1373
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/s/a/r;->c:[Lcom/google/s/a/q;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 1374
    iget-object v2, p0, Lcom/google/s/a/r;->c:[Lcom/google/s/a/q;

    aget-object v2, v2, v0

    .line 1375
    if-eqz v2, :cond_2

    .line 1376
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1373
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1381
    :cond_4
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1302
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/s/a/r;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/r;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/s/a/r;->b:Lcom/google/s/a/n;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/s/a/n;

    invoke-direct {v0}, Lcom/google/s/a/n;-><init>()V

    iput-object v0, p0, Lcom/google/s/a/r;->b:Lcom/google/s/a/n;

    :cond_1
    iget-object v0, p0, Lcom/google/s/a/r;->b:Lcom/google/s/a/n;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/s/a/r;->c:[Lcom/google/s/a/q;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/s/a/q;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/s/a/r;->c:[Lcom/google/s/a/q;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/s/a/q;

    invoke-direct {v3}, Lcom/google/s/a/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/s/a/r;->c:[Lcom/google/s/a/q;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/s/a/q;

    invoke-direct {v3}, Lcom/google/s/a/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/s/a/r;->c:[Lcom/google/s/a/q;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1344
    iget-object v0, p0, Lcom/google/s/a/r;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1345
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/s/a/r;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1347
    :cond_0
    iget-object v0, p0, Lcom/google/s/a/r;->b:Lcom/google/s/a/n;

    if-eqz v0, :cond_1

    .line 1348
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/s/a/r;->b:Lcom/google/s/a/n;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1350
    :cond_1
    iget-object v0, p0, Lcom/google/s/a/r;->c:[Lcom/google/s/a/q;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/s/a/r;->c:[Lcom/google/s/a/q;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 1351
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/s/a/r;->c:[Lcom/google/s/a/q;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 1352
    iget-object v1, p0, Lcom/google/s/a/r;->c:[Lcom/google/s/a/q;

    aget-object v1, v1, v0

    .line 1353
    if-eqz v1, :cond_2

    .line 1354
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1351
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1358
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1359
    return-void
.end method
