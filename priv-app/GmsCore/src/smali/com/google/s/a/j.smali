.class public final Lcom/google/s/a/j;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public apiHeader:Lcom/google/s/a/d;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/s/a/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3431
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3432
    iput-object v0, p0, Lcom/google/s/a/j;->apiHeader:Lcom/google/s/a/d;

    iput-object v0, p0, Lcom/google/s/a/j;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/s/a/j;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/s/a/j;->c:Lcom/google/s/a/b;

    iput-object v0, p0, Lcom/google/s/a/j;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/s/a/j;->cachedSize:I

    .line 3433
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3465
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3466
    iget-object v1, p0, Lcom/google/s/a/j;->apiHeader:Lcom/google/s/a/d;

    if-eqz v1, :cond_0

    .line 3467
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/s/a/j;->apiHeader:Lcom/google/s/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3470
    :cond_0
    iget-object v1, p0, Lcom/google/s/a/j;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3471
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/s/a/j;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3474
    :cond_1
    iget-object v1, p0, Lcom/google/s/a/j;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3475
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/s/a/j;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3478
    :cond_2
    iget-object v1, p0, Lcom/google/s/a/j;->c:Lcom/google/s/a/b;

    if-eqz v1, :cond_3

    .line 3479
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/s/a/j;->c:Lcom/google/s/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3482
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3402
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/s/a/j;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/s/a/j;->apiHeader:Lcom/google/s/a/d;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/s/a/d;

    invoke-direct {v0}, Lcom/google/s/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/s/a/j;->apiHeader:Lcom/google/s/a/d;

    :cond_1
    iget-object v0, p0, Lcom/google/s/a/j;->apiHeader:Lcom/google/s/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/j;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/j;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/s/a/j;->c:Lcom/google/s/a/b;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/s/a/b;

    invoke-direct {v0}, Lcom/google/s/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/s/a/j;->c:Lcom/google/s/a/b;

    :cond_2
    iget-object v0, p0, Lcom/google/s/a/j;->c:Lcom/google/s/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3448
    iget-object v0, p0, Lcom/google/s/a/j;->apiHeader:Lcom/google/s/a/d;

    if-eqz v0, :cond_0

    .line 3449
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/s/a/j;->apiHeader:Lcom/google/s/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3451
    :cond_0
    iget-object v0, p0, Lcom/google/s/a/j;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3452
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/s/a/j;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3454
    :cond_1
    iget-object v0, p0, Lcom/google/s/a/j;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3455
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/s/a/j;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3457
    :cond_2
    iget-object v0, p0, Lcom/google/s/a/j;->c:Lcom/google/s/a/b;

    if-eqz v0, :cond_3

    .line 3458
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/s/a/j;->c:Lcom/google/s/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3460
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3461
    return-void
.end method
