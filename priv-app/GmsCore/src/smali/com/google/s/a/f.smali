.class public final Lcom/google/s/a/f;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public apiHeader:Lcom/google/s/a/d;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2202
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2203
    iput-object v0, p0, Lcom/google/s/a/f;->apiHeader:Lcom/google/s/a/d;

    iput-object v0, p0, Lcom/google/s/a/f;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/s/a/f;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/s/a/f;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/s/a/f;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/s/a/f;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/s/a/f;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/s/a/f;->cachedSize:I

    .line 2204
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2244
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2245
    iget-object v1, p0, Lcom/google/s/a/f;->apiHeader:Lcom/google/s/a/d;

    if-eqz v1, :cond_0

    .line 2246
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/s/a/f;->apiHeader:Lcom/google/s/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2249
    :cond_0
    iget-object v1, p0, Lcom/google/s/a/f;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2250
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/s/a/f;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2253
    :cond_1
    iget-object v1, p0, Lcom/google/s/a/f;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 2254
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/s/a/f;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2257
    :cond_2
    iget-object v1, p0, Lcom/google/s/a/f;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2258
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/s/a/f;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2261
    :cond_3
    iget-object v1, p0, Lcom/google/s/a/f;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 2262
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/s/a/f;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2265
    :cond_4
    iget-object v1, p0, Lcom/google/s/a/f;->e:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 2266
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/s/a/f;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2269
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2167
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/s/a/f;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/s/a/f;->apiHeader:Lcom/google/s/a/d;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/s/a/d;

    invoke-direct {v0}, Lcom/google/s/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/s/a/f;->apiHeader:Lcom/google/s/a/d;

    :cond_1
    iget-object v0, p0, Lcom/google/s/a/f;->apiHeader:Lcom/google/s/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/f;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/f;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/f;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/f;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/f;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2221
    iget-object v0, p0, Lcom/google/s/a/f;->apiHeader:Lcom/google/s/a/d;

    if-eqz v0, :cond_0

    .line 2222
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/s/a/f;->apiHeader:Lcom/google/s/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2224
    :cond_0
    iget-object v0, p0, Lcom/google/s/a/f;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2225
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/s/a/f;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2227
    :cond_1
    iget-object v0, p0, Lcom/google/s/a/f;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2228
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/s/a/f;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2230
    :cond_2
    iget-object v0, p0, Lcom/google/s/a/f;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2231
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/s/a/f;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2233
    :cond_3
    iget-object v0, p0, Lcom/google/s/a/f;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 2234
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/s/a/f;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2236
    :cond_4
    iget-object v0, p0, Lcom/google/s/a/f;->e:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 2237
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/s/a/f;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2239
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2240
    return-void
.end method
