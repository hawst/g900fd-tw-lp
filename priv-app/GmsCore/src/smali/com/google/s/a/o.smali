.class public final Lcom/google/s/a/o;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/s/a/o;


# instance fields
.field public a:Ljava/lang/String;

.field public b:[[B


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1067
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1068
    iput-object v1, p0, Lcom/google/s/a/o;->a:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->g:[[B

    iput-object v0, p0, Lcom/google/s/a/o;->b:[[B

    iput-object v1, p0, Lcom/google/s/a/o;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/s/a/o;->cachedSize:I

    .line 1069
    return-void
.end method

.method public static a()[Lcom/google/s/a/o;
    .locals 2

    .prologue
    .line 1050
    sget-object v0, Lcom/google/s/a/o;->c:[Lcom/google/s/a/o;

    if-nez v0, :cond_1

    .line 1051
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1053
    :try_start_0
    sget-object v0, Lcom/google/s/a/o;->c:[Lcom/google/s/a/o;

    if-nez v0, :cond_0

    .line 1054
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/s/a/o;

    sput-object v0, Lcom/google/s/a/o;->c:[Lcom/google/s/a/o;

    .line 1056
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1058
    :cond_1
    sget-object v0, Lcom/google/s/a/o;->c:[Lcom/google/s/a/o;

    return-object v0

    .line 1056
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1098
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1099
    iget-object v2, p0, Lcom/google/s/a/o;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1100
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/s/a/o;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1103
    :cond_0
    iget-object v2, p0, Lcom/google/s/a/o;->b:[[B

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/s/a/o;->b:[[B

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    move v3, v1

    .line 1106
    :goto_0
    iget-object v4, p0, Lcom/google/s/a/o;->b:[[B

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 1107
    iget-object v4, p0, Lcom/google/s/a/o;->b:[[B

    aget-object v4, v4, v1

    .line 1108
    if-eqz v4, :cond_1

    .line 1109
    add-int/lit8 v3, v3, 0x1

    .line 1110
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->b([B)I

    move-result v4

    add-int/2addr v2, v4

    .line 1106
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1114
    :cond_2
    add-int/2addr v0, v2

    .line 1115
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 1117
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1044
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/s/a/o;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/o;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/s/a/o;->b:[[B

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [[B

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/s/a/o;->b:[[B

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/s/a/o;->b:[[B

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/s/a/o;->b:[[B

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1082
    iget-object v0, p0, Lcom/google/s/a/o;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1083
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/s/a/o;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1085
    :cond_0
    iget-object v0, p0, Lcom/google/s/a/o;->b:[[B

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/s/a/o;->b:[[B

    array-length v0, v0

    if-lez v0, :cond_2

    .line 1086
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/s/a/o;->b:[[B

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 1087
    iget-object v1, p0, Lcom/google/s/a/o;->b:[[B

    aget-object v1, v1, v0

    .line 1088
    if-eqz v1, :cond_1

    .line 1089
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 1086
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1093
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1094
    return-void
.end method
