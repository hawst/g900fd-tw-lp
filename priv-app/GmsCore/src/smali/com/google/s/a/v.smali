.class public final Lcom/google/s/a/v;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/s/a/w;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 952
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 953
    iput-object v0, p0, Lcom/google/s/a/v;->a:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/s/a/v;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/s/a/v;->c:Lcom/google/s/a/w;

    iput-object v0, p0, Lcom/google/s/a/v;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/s/a/v;->cachedSize:I

    .line 954
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 982
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 983
    iget-object v1, p0, Lcom/google/s/a/v;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 984
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/s/a/v;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 987
    :cond_0
    iget-object v1, p0, Lcom/google/s/a/v;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 988
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/s/a/v;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 991
    :cond_1
    iget-object v1, p0, Lcom/google/s/a/v;->c:Lcom/google/s/a/w;

    if-eqz v1, :cond_2

    .line 992
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/s/a/v;->c:Lcom/google/s/a/w;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 995
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 813
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/s/a/v;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/v;->a:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/v;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/s/a/v;->c:Lcom/google/s/a/w;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/s/a/w;

    invoke-direct {v0}, Lcom/google/s/a/w;-><init>()V

    iput-object v0, p0, Lcom/google/s/a/v;->c:Lcom/google/s/a/w;

    :cond_1
    iget-object v0, p0, Lcom/google/s/a/v;->c:Lcom/google/s/a/w;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 968
    iget-object v0, p0, Lcom/google/s/a/v;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 969
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/s/a/v;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 971
    :cond_0
    iget-object v0, p0, Lcom/google/s/a/v;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 972
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/s/a/v;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 974
    :cond_1
    iget-object v0, p0, Lcom/google/s/a/v;->c:Lcom/google/s/a/w;

    if-eqz v0, :cond_2

    .line 975
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/s/a/v;->c:Lcom/google/s/a/w;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 977
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 978
    return-void
.end method
