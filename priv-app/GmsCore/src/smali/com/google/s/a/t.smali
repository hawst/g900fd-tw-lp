.class public final Lcom/google/s/a/t;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Lcom/google/s/a/v;

.field public e:[Lcom/google/s/a/u;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 663
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 664
    iput-object v1, p0, Lcom/google/s/a/t;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/s/a/t;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/s/a/t;->c:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/s/a/t;->d:Lcom/google/s/a/v;

    invoke-static {}, Lcom/google/s/a/u;->a()[Lcom/google/s/a/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/t;->e:[Lcom/google/s/a/u;

    iput-object v1, p0, Lcom/google/s/a/t;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/s/a/t;->cachedSize:I

    .line 665
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 706
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 707
    iget-object v1, p0, Lcom/google/s/a/t;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 708
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/s/a/t;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 711
    :cond_0
    iget-object v1, p0, Lcom/google/s/a/t;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 712
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/s/a/t;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 715
    :cond_1
    iget-object v1, p0, Lcom/google/s/a/t;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 716
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/s/a/t;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 719
    :cond_2
    iget-object v1, p0, Lcom/google/s/a/t;->d:Lcom/google/s/a/v;

    if-eqz v1, :cond_3

    .line 720
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/s/a/t;->d:Lcom/google/s/a/v;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 723
    :cond_3
    iget-object v1, p0, Lcom/google/s/a/t;->e:[Lcom/google/s/a/u;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/s/a/t;->e:[Lcom/google/s/a/u;

    array-length v1, v1

    if-lez v1, :cond_6

    .line 724
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/s/a/t;->e:[Lcom/google/s/a/u;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 725
    iget-object v2, p0, Lcom/google/s/a/t;->e:[Lcom/google/s/a/u;

    aget-object v2, v2, v0

    .line 726
    if-eqz v2, :cond_4

    .line 727
    const/4 v3, 0x5

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 724
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 732
    :cond_6
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 525
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/s/a/t;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/t;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/t;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/t;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/s/a/t;->d:Lcom/google/s/a/v;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/s/a/v;

    invoke-direct {v0}, Lcom/google/s/a/v;-><init>()V

    iput-object v0, p0, Lcom/google/s/a/t;->d:Lcom/google/s/a/v;

    :cond_1
    iget-object v0, p0, Lcom/google/s/a/t;->d:Lcom/google/s/a/v;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/s/a/t;->e:[Lcom/google/s/a/u;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/s/a/u;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/s/a/t;->e:[Lcom/google/s/a/u;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/s/a/u;

    invoke-direct {v3}, Lcom/google/s/a/u;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/s/a/t;->e:[Lcom/google/s/a/u;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/s/a/u;

    invoke-direct {v3}, Lcom/google/s/a/u;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/s/a/t;->e:[Lcom/google/s/a/u;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 681
    iget-object v0, p0, Lcom/google/s/a/t;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 682
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/s/a/t;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 684
    :cond_0
    iget-object v0, p0, Lcom/google/s/a/t;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 685
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/s/a/t;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 687
    :cond_1
    iget-object v0, p0, Lcom/google/s/a/t;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 688
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/s/a/t;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 690
    :cond_2
    iget-object v0, p0, Lcom/google/s/a/t;->d:Lcom/google/s/a/v;

    if-eqz v0, :cond_3

    .line 691
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/s/a/t;->d:Lcom/google/s/a/v;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 693
    :cond_3
    iget-object v0, p0, Lcom/google/s/a/t;->e:[Lcom/google/s/a/u;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/s/a/t;->e:[Lcom/google/s/a/u;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 694
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/s/a/t;->e:[Lcom/google/s/a/u;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 695
    iget-object v1, p0, Lcom/google/s/a/t;->e:[Lcom/google/s/a/u;

    aget-object v1, v1, v0

    .line 696
    if-eqz v1, :cond_4

    .line 697
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 694
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 701
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 702
    return-void
.end method
