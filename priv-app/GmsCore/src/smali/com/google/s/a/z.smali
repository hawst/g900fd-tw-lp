.class public final Lcom/google/s/a/z;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile i:[Lcom/google/s/a/z;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Lcom/google/s/a/y;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/Long;

.field public g:[I

.field public h:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 171
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 172
    iput-object v1, p0, Lcom/google/s/a/z;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/s/a/z;->b:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/s/a/z;->c:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/s/a/z;->d:Lcom/google/s/a/y;

    iput-object v1, p0, Lcom/google/s/a/z;->e:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/s/a/z;->f:Ljava/lang/Long;

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/s/a/z;->g:[I

    iput-object v1, p0, Lcom/google/s/a/z;->h:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/s/a/z;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/s/a/z;->cachedSize:I

    .line 173
    return-void
.end method

.method public static a()[Lcom/google/s/a/z;
    .locals 2

    .prologue
    .line 136
    sget-object v0, Lcom/google/s/a/z;->i:[Lcom/google/s/a/z;

    if-nez v0, :cond_1

    .line 137
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 139
    :try_start_0
    sget-object v0, Lcom/google/s/a/z;->i:[Lcom/google/s/a/z;

    if-nez v0, :cond_0

    .line 140
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/s/a/z;

    sput-object v0, Lcom/google/s/a/z;->i:[Lcom/google/s/a/z;

    .line 142
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    :cond_1
    sget-object v0, Lcom/google/s/a/z;->i:[Lcom/google/s/a/z;

    return-object v0

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 223
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 224
    iget-object v2, p0, Lcom/google/s/a/z;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 225
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/s/a/z;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 228
    :cond_0
    iget-object v2, p0, Lcom/google/s/a/z;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 229
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/s/a/z;->b:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 232
    :cond_1
    iget-object v2, p0, Lcom/google/s/a/z;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 233
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/s/a/z;->c:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 236
    :cond_2
    iget-object v2, p0, Lcom/google/s/a/z;->d:Lcom/google/s/a/y;

    if-eqz v2, :cond_3

    .line 237
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/s/a/z;->d:Lcom/google/s/a/y;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 240
    :cond_3
    iget-object v2, p0, Lcom/google/s/a/z;->e:Ljava/lang/Long;

    if-eqz v2, :cond_4

    .line 241
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/s/a/z;->e:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 244
    :cond_4
    iget-object v2, p0, Lcom/google/s/a/z;->f:Ljava/lang/Long;

    if-eqz v2, :cond_5

    .line 245
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/s/a/z;->f:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 248
    :cond_5
    iget-object v2, p0, Lcom/google/s/a/z;->g:[I

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/s/a/z;->g:[I

    array-length v2, v2

    if-lez v2, :cond_7

    move v2, v1

    .line 250
    :goto_0
    iget-object v3, p0, Lcom/google/s/a/z;->g:[I

    array-length v3, v3

    if-ge v1, v3, :cond_6

    .line 251
    iget-object v3, p0, Lcom/google/s/a/z;->g:[I

    aget v3, v3, v1

    .line 252
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->e(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 250
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 255
    :cond_6
    add-int/2addr v0, v2

    .line 256
    iget-object v1, p0, Lcom/google/s/a/z;->g:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 258
    :cond_7
    iget-object v1, p0, Lcom/google/s/a/z;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 259
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/s/a/z;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 262
    :cond_8
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 130
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/s/a/z;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/z;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/z;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/z;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/s/a/z;->d:Lcom/google/s/a/y;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/s/a/y;

    invoke-direct {v0}, Lcom/google/s/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/s/a/z;->d:Lcom/google/s/a/y;

    :cond_1
    iget-object v0, p0, Lcom/google/s/a/z;->d:Lcom/google/s/a/y;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/z;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/z;->f:Ljava/lang/Long;

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x38

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/s/a/z;->g:[I

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/s/a/z;->g:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/s/a/z;->g:[I

    array-length v0, v0

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lcom/google/s/a/z;->g:[I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_5

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/s/a/z;->g:[I

    if-nez v2, :cond_7

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_6

    iget-object v4, p0, Lcom/google/s/a/z;->g:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_8

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_7
    iget-object v2, p0, Lcom/google/s/a/z;->g:[I

    array-length v2, v2

    goto :goto_4

    :cond_8
    iput-object v0, p0, Lcom/google/s/a/z;->g:[I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/z;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x3a -> :sswitch_8
        0x40 -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/s/a/z;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 193
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/s/a/z;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/google/s/a/z;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 196
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/s/a/z;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 198
    :cond_1
    iget-object v0, p0, Lcom/google/s/a/z;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 199
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/s/a/z;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 201
    :cond_2
    iget-object v0, p0, Lcom/google/s/a/z;->d:Lcom/google/s/a/y;

    if-eqz v0, :cond_3

    .line 202
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/s/a/z;->d:Lcom/google/s/a/y;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 204
    :cond_3
    iget-object v0, p0, Lcom/google/s/a/z;->e:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 205
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/s/a/z;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 207
    :cond_4
    iget-object v0, p0, Lcom/google/s/a/z;->f:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 208
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/s/a/z;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 210
    :cond_5
    iget-object v0, p0, Lcom/google/s/a/z;->g:[I

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/s/a/z;->g:[I

    array-length v0, v0

    if-lez v0, :cond_6

    .line 211
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/s/a/z;->g:[I

    array-length v1, v1

    if-ge v0, v1, :cond_6

    .line 212
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/s/a/z;->g:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 211
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 215
    :cond_6
    iget-object v0, p0, Lcom/google/s/a/z;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 216
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/s/a/z;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 218
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 219
    return-void
.end method
