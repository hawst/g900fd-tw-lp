.class public final Lcom/google/s/a/s;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Lcom/google/s/a/m;

.field public c:Lcom/google/s/a/p;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 193
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 194
    iput-object v0, p0, Lcom/google/s/a/s;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/s/a/s;->b:Lcom/google/s/a/m;

    iput-object v0, p0, Lcom/google/s/a/s;->c:Lcom/google/s/a/p;

    iput-object v0, p0, Lcom/google/s/a/s;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/s/a/s;->cachedSize:I

    .line 195
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 223
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 224
    iget-object v1, p0, Lcom/google/s/a/s;->b:Lcom/google/s/a/m;

    if-eqz v1, :cond_0

    .line 225
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/s/a/s;->b:Lcom/google/s/a/m;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 228
    :cond_0
    iget-object v1, p0, Lcom/google/s/a/s;->c:Lcom/google/s/a/p;

    if-eqz v1, :cond_1

    .line 229
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/s/a/s;->c:Lcom/google/s/a/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 232
    :cond_1
    iget-object v1, p0, Lcom/google/s/a/s;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 233
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/s/a/s;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 236
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 167
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/s/a/s;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/s/a/s;->b:Lcom/google/s/a/m;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/s/a/m;

    invoke-direct {v0}, Lcom/google/s/a/m;-><init>()V

    iput-object v0, p0, Lcom/google/s/a/s;->b:Lcom/google/s/a/m;

    :cond_1
    iget-object v0, p0, Lcom/google/s/a/s;->b:Lcom/google/s/a/m;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/s/a/s;->c:Lcom/google/s/a/p;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/s/a/p;

    invoke-direct {v0}, Lcom/google/s/a/p;-><init>()V

    iput-object v0, p0, Lcom/google/s/a/s;->c:Lcom/google/s/a/p;

    :cond_2
    iget-object v0, p0, Lcom/google/s/a/s;->c:Lcom/google/s/a/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/s;->a:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1a -> :sswitch_1
        0x2a -> :sswitch_2
        0x30 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/s/a/s;->b:Lcom/google/s/a/m;

    if-eqz v0, :cond_0

    .line 210
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/s/a/s;->b:Lcom/google/s/a/m;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/google/s/a/s;->c:Lcom/google/s/a/p;

    if-eqz v0, :cond_1

    .line 213
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/s/a/s;->c:Lcom/google/s/a/p;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 215
    :cond_1
    iget-object v0, p0, Lcom/google/s/a/s;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 216
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/s/a/s;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 218
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 219
    return-void
.end method
