.class public final Lcom/google/s/a/n;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1201
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1202
    iput-object v0, p0, Lcom/google/s/a/n;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/s/a/n;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/s/a/n;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/s/a/n;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/s/a/n;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/s/a/n;->cachedSize:I

    .line 1203
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1235
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1236
    iget-object v1, p0, Lcom/google/s/a/n;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1237
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/s/a/n;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1240
    :cond_0
    iget-object v1, p0, Lcom/google/s/a/n;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1241
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/s/a/n;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1244
    :cond_1
    iget-object v1, p0, Lcom/google/s/a/n;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1245
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/s/a/n;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1248
    :cond_2
    iget-object v1, p0, Lcom/google/s/a/n;->a:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1249
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/s/a/n;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1252
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1172
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/s/a/n;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/n;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/n;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/n;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/n;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1218
    iget-object v0, p0, Lcom/google/s/a/n;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1219
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/s/a/n;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1221
    :cond_0
    iget-object v0, p0, Lcom/google/s/a/n;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1222
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/s/a/n;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1224
    :cond_1
    iget-object v0, p0, Lcom/google/s/a/n;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1225
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/s/a/n;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1227
    :cond_2
    iget-object v0, p0, Lcom/google/s/a/n;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1228
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/s/a/n;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1230
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1231
    return-void
.end method
