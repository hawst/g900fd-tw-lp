.class public final Lcom/google/s/a/b;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[I

.field public b:[Lcom/google/s/a/o;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 435
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 436
    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/s/a/b;->a:[I

    invoke-static {}, Lcom/google/s/a/o;->a()[Lcom/google/s/a/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/b;->b:[Lcom/google/s/a/o;

    iput-object v1, p0, Lcom/google/s/a/b;->c:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/s/a/b;->d:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/s/a/b;->e:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/s/a/b;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/s/a/b;->cachedSize:I

    .line 437
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 480
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v3

    .line 481
    iget-object v0, p0, Lcom/google/s/a/b;->a:[I

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/s/a/b;->a:[I

    array-length v0, v0

    if-lez v0, :cond_6

    move v0, v1

    move v2, v1

    .line 483
    :goto_0
    iget-object v4, p0, Lcom/google/s/a/b;->a:[I

    array-length v4, v4

    if-ge v0, v4, :cond_0

    .line 484
    iget-object v4, p0, Lcom/google/s/a/b;->a:[I

    aget v4, v4, v0

    .line 485
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v4

    add-int/2addr v2, v4

    .line 483
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 488
    :cond_0
    add-int v0, v3, v2

    .line 489
    iget-object v2, p0, Lcom/google/s/a/b;->a:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 491
    :goto_1
    iget-object v2, p0, Lcom/google/s/a/b;->b:[Lcom/google/s/a/o;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/s/a/b;->b:[Lcom/google/s/a/o;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 492
    :goto_2
    iget-object v2, p0, Lcom/google/s/a/b;->b:[Lcom/google/s/a/o;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 493
    iget-object v2, p0, Lcom/google/s/a/b;->b:[Lcom/google/s/a/o;

    aget-object v2, v2, v1

    .line 494
    if-eqz v2, :cond_1

    .line 495
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 492
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 500
    :cond_2
    iget-object v1, p0, Lcom/google/s/a/b;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 501
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/s/a/b;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 504
    :cond_3
    iget-object v1, p0, Lcom/google/s/a/b;->d:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 505
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/s/a/b;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 508
    :cond_4
    iget-object v1, p0, Lcom/google/s/a/b;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 509
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/s/a/b;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 512
    :cond_5
    return v0

    :cond_6
    move v0, v3

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 403
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/s/a/b;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/s/a/b;->a:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Lcom/google/s/a/b;->a:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/s/a/b;->a:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Lcom/google/s/a/b;->a:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/s/a/b;->a:[I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/s/a/b;->a:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/google/s/a/b;->a:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Lcom/google/s/a/b;->a:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Lcom/google/s/a/b;->a:[I

    :cond_a
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_3
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v1

    iget-object v0, p0, Lcom/google/s/a/b;->b:[Lcom/google/s/a/o;

    if-nez v0, :cond_c

    move v0, v2

    :goto_7
    add-int/2addr v1, v0

    new-array v1, v1, [Lcom/google/s/a/o;

    if-eqz v0, :cond_b

    iget-object v3, p0, Lcom/google/s/a/b;->b:[Lcom/google/s/a/o;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_8
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    new-instance v3, Lcom/google/s/a/o;

    invoke-direct {v3}, Lcom/google/s/a/o;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_c
    iget-object v0, p0, Lcom/google/s/a/b;->b:[Lcom/google/s/a/o;

    array-length v0, v0

    goto :goto_7

    :cond_d
    new-instance v3, Lcom/google/s/a/o;

    invoke-direct {v3}, Lcom/google/s/a/o;-><init>()V

    aput-object v3, v1, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v1, p0, Lcom/google/s/a/b;->b:[Lcom/google/s/a/o;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/b;->c:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/b;->d:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/b;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_3
        0x18 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 453
    iget-object v0, p0, Lcom/google/s/a/b;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/s/a/b;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    move v0, v1

    .line 454
    :goto_0
    iget-object v2, p0, Lcom/google/s/a/b;->a:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 455
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/s/a/b;->a:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 454
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 458
    :cond_0
    iget-object v0, p0, Lcom/google/s/a/b;->b:[Lcom/google/s/a/o;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/s/a/b;->b:[Lcom/google/s/a/o;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 459
    :goto_1
    iget-object v0, p0, Lcom/google/s/a/b;->b:[Lcom/google/s/a/o;

    array-length v0, v0

    if-ge v1, v0, :cond_2

    .line 460
    iget-object v0, p0, Lcom/google/s/a/b;->b:[Lcom/google/s/a/o;

    aget-object v0, v0, v1

    .line 461
    if-eqz v0, :cond_1

    .line 462
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 459
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 466
    :cond_2
    iget-object v0, p0, Lcom/google/s/a/b;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 467
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/s/a/b;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 469
    :cond_3
    iget-object v0, p0, Lcom/google/s/a/b;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 470
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/s/a/b;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 472
    :cond_4
    iget-object v0, p0, Lcom/google/s/a/b;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 473
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/s/a/b;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 475
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 476
    return-void
.end method
