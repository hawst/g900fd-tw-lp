.class public final Lcom/google/s/a/q;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/s/a/q;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/google/s/a/s;

.field public c:Lcom/google/s/a/t;

.field public d:Lcom/google/s/a/v;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 58
    iput-object v0, p0, Lcom/google/s/a/q;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/s/a/q;->b:Lcom/google/s/a/s;

    iput-object v0, p0, Lcom/google/s/a/q;->c:Lcom/google/s/a/t;

    iput-object v0, p0, Lcom/google/s/a/q;->d:Lcom/google/s/a/v;

    iput-object v0, p0, Lcom/google/s/a/q;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/s/a/q;->cachedSize:I

    .line 59
    return-void
.end method

.method public static a()[Lcom/google/s/a/q;
    .locals 2

    .prologue
    .line 34
    sget-object v0, Lcom/google/s/a/q;->e:[Lcom/google/s/a/q;

    if-nez v0, :cond_1

    .line 35
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 37
    :try_start_0
    sget-object v0, Lcom/google/s/a/q;->e:[Lcom/google/s/a/q;

    if-nez v0, :cond_0

    .line 38
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/s/a/q;

    sput-object v0, Lcom/google/s/a/q;->e:[Lcom/google/s/a/q;

    .line 40
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    :cond_1
    sget-object v0, Lcom/google/s/a/q;->e:[Lcom/google/s/a/q;

    return-object v0

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 91
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 92
    iget-object v1, p0, Lcom/google/s/a/q;->b:Lcom/google/s/a/s;

    if-eqz v1, :cond_0

    .line 93
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/s/a/q;->b:Lcom/google/s/a/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_0
    iget-object v1, p0, Lcom/google/s/a/q;->c:Lcom/google/s/a/t;

    if-eqz v1, :cond_1

    .line 97
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/s/a/q;->c:Lcom/google/s/a/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    :cond_1
    iget-object v1, p0, Lcom/google/s/a/q;->d:Lcom/google/s/a/v;

    if-eqz v1, :cond_2

    .line 101
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/s/a/q;->d:Lcom/google/s/a/v;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    :cond_2
    iget-object v1, p0, Lcom/google/s/a/q;->a:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 105
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/s/a/q;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 28
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/s/a/q;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/s/a/q;->b:Lcom/google/s/a/s;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/s/a/s;

    invoke-direct {v0}, Lcom/google/s/a/s;-><init>()V

    iput-object v0, p0, Lcom/google/s/a/q;->b:Lcom/google/s/a/s;

    :cond_1
    iget-object v0, p0, Lcom/google/s/a/q;->b:Lcom/google/s/a/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/s/a/q;->c:Lcom/google/s/a/t;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/s/a/t;

    invoke-direct {v0}, Lcom/google/s/a/t;-><init>()V

    iput-object v0, p0, Lcom/google/s/a/q;->c:Lcom/google/s/a/t;

    :cond_2
    iget-object v0, p0, Lcom/google/s/a/q;->c:Lcom/google/s/a/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/s/a/q;->d:Lcom/google/s/a/v;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/s/a/v;

    invoke-direct {v0}, Lcom/google/s/a/v;-><init>()V

    iput-object v0, p0, Lcom/google/s/a/q;->d:Lcom/google/s/a/v;

    :cond_3
    iget-object v0, p0, Lcom/google/s/a/q;->d:Lcom/google/s/a/v;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/s/a/q;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/s/a/q;->b:Lcom/google/s/a/s;

    if-eqz v0, :cond_0

    .line 75
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/s/a/q;->b:Lcom/google/s/a/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/google/s/a/q;->c:Lcom/google/s/a/t;

    if-eqz v0, :cond_1

    .line 78
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/s/a/q;->c:Lcom/google/s/a/t;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/google/s/a/q;->d:Lcom/google/s/a/v;

    if-eqz v0, :cond_2

    .line 81
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/s/a/q;->d:Lcom/google/s/a/v;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 83
    :cond_2
    iget-object v0, p0, Lcom/google/s/a/q;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 84
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/s/a/q;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 86
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 87
    return-void
.end method
