.class Lcom/google/k/c/s;
.super Lcom/google/k/c/q;
.source "SourceFile"

# interfaces
.implements Ljava/util/List;


# instance fields
.field final synthetic g:Lcom/google/k/c/e;


# direct methods
.method constructor <init>(Lcom/google/k/c/e;Ljava/lang/Object;Ljava/util/List;Lcom/google/k/c/q;)V
    .locals 0

    .prologue
    .line 760
    iput-object p1, p0, Lcom/google/k/c/s;->g:Lcom/google/k/c/e;

    .line 761
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/k/c/q;-><init>(Lcom/google/k/c/e;Ljava/lang/Object;Ljava/util/Collection;Lcom/google/k/c/q;)V

    .line 762
    return-void
.end method


# virtual methods
.method public add(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 799
    invoke-virtual {p0}, Lcom/google/k/c/s;->a()V

    .line 800
    iget-object v0, p0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    .line 801
    iget-object v0, p0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 802
    iget-object v0, p0, Lcom/google/k/c/s;->g:Lcom/google/k/c/e;

    invoke-static {v0}, Lcom/google/k/c/e;->c(Lcom/google/k/c/e;)I

    .line 803
    if-eqz v1, :cond_0

    .line 804
    invoke-virtual {p0}, Lcom/google/k/c/s;->c()V

    .line 806
    :cond_0
    return-void
.end method

.method public addAll(ILjava/util/Collection;)Z
    .locals 4

    .prologue
    .line 770
    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 771
    const/4 v0, 0x0

    .line 782
    :cond_0
    :goto_0
    return v0

    .line 773
    :cond_1
    invoke-virtual {p0}, Lcom/google/k/c/s;->size()I

    move-result v1

    .line 774
    iget-object v0, p0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    move-result v0

    .line 775
    if-eqz v0, :cond_0

    .line 776
    iget-object v2, p0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    .line 777
    iget-object v3, p0, Lcom/google/k/c/s;->g:Lcom/google/k/c/e;

    sub-int/2addr v2, v1

    invoke-static {v3, v2}, Lcom/google/k/c/e;->a(Lcom/google/k/c/e;I)I

    .line 778
    if-nez v1, :cond_0

    .line 779
    invoke-virtual {p0}, Lcom/google/k/c/s;->c()V

    goto :goto_0
.end method

.method final d()Ljava/util/List;
    .locals 1

    .prologue
    .line 765
    iget-object v0, p0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 787
    invoke-virtual {p0}, Lcom/google/k/c/s;->a()V

    .line 788
    iget-object v0, p0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 819
    invoke-virtual {p0}, Lcom/google/k/c/s;->a()V

    .line 820
    iget-object v0, p0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 825
    invoke-virtual {p0}, Lcom/google/k/c/s;->a()V

    .line 826
    iget-object v0, p0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public listIterator()Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 831
    invoke-virtual {p0}, Lcom/google/k/c/s;->a()V

    .line 832
    new-instance v0, Lcom/google/k/c/t;

    invoke-direct {v0, p0}, Lcom/google/k/c/t;-><init>(Lcom/google/k/c/s;)V

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 837
    invoke-virtual {p0}, Lcom/google/k/c/s;->a()V

    .line 838
    new-instance v0, Lcom/google/k/c/t;

    invoke-direct {v0, p0, p1}, Lcom/google/k/c/t;-><init>(Lcom/google/k/c/s;I)V

    return-object v0
.end method

.method public remove(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 810
    invoke-virtual {p0}, Lcom/google/k/c/s;->a()V

    .line 811
    iget-object v0, p0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    .line 812
    iget-object v1, p0, Lcom/google/k/c/s;->g:Lcom/google/k/c/e;

    invoke-static {v1}, Lcom/google/k/c/e;->b(Lcom/google/k/c/e;)I

    .line 813
    invoke-virtual {p0}, Lcom/google/k/c/s;->b()V

    .line 814
    return-object v0
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 793
    invoke-virtual {p0}, Lcom/google/k/c/s;->a()V

    .line 794
    iget-object v0, p0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subList(II)Ljava/util/List;
    .locals 4

    .prologue
    .line 843
    invoke-virtual {p0}, Lcom/google/k/c/s;->a()V

    .line 844
    iget-object v1, p0, Lcom/google/k/c/s;->g:Lcom/google/k/c/e;

    iget-object v2, p0, Lcom/google/k/c/q;->b:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    iget-object v3, p0, Lcom/google/k/c/q;->d:Lcom/google/k/c/q;

    if-nez v3, :cond_0

    :goto_0
    invoke-static {v1, v2, v0, p0}, Lcom/google/k/c/e;->a(Lcom/google/k/c/e;Ljava/lang/Object;Ljava/util/List;Lcom/google/k/c/q;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object p0, p0, Lcom/google/k/c/q;->d:Lcom/google/k/c/q;

    goto :goto_0
.end method
