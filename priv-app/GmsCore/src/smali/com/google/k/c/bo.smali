.class public abstract Lcom/google/k/c/bo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Map;


# static fields
.field private static final a:[Ljava/util/Map$Entry;


# instance fields
.field private transient b:Lcom/google/k/c/cd;

.field private transient c:Lcom/google/k/c/cd;

.field private transient d:Lcom/google/k/c/bf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 313
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/util/Map$Entry;

    sput-object v0, Lcom/google/k/c/bo;->a:[Ljava/util/Map$Entry;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bo;
    .locals 4

    .prologue
    .line 89
    new-instance v0, Lcom/google/k/c/ef;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/k/c/br;

    const/4 v2, 0x0

    invoke-static {p0, p1}, Lcom/google/k/c/bo;->c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/br;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2, p3}, Lcom/google/k/c/bo;->c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/br;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/google/k/c/ef;-><init>([Lcom/google/k/c/br;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bo;
    .locals 4

    .prologue
    .line 99
    new-instance v0, Lcom/google/k/c/ef;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/google/k/c/br;

    const/4 v2, 0x0

    invoke-static {p0, p1}, Lcom/google/k/c/bo;->c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/br;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2, p3}, Lcom/google/k/c/bo;->c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/br;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p4, p5}, Lcom/google/k/c/bo;->c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/br;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/google/k/c/ef;-><init>([Lcom/google/k/c/br;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bo;
    .locals 1

    .prologue
    .line 80
    invoke-static {p0, p1}, Lcom/google/k/c/be;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/be;

    move-result-object v0

    return-object v0
.end method

.method static c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/br;
    .locals 1

    .prologue
    .line 135
    invoke-static {p0, p1}, Lcom/google/k/c/ao;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 136
    new-instance v0, Lcom/google/k/c/br;

    invoke-direct {v0, p0, p1}, Lcom/google/k/c/br;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static g()Lcom/google/k/c/bo;
    .locals 1

    .prologue
    .line 70
    invoke-static {}, Lcom/google/k/c/be;->e()Lcom/google/k/c/be;

    move-result-object v0

    return-object v0
.end method

.method public static h()Lcom/google/k/c/bp;
    .locals 1

    .prologue
    .line 144
    new-instance v0, Lcom/google/k/c/bp;

    invoke-direct {v0}, Lcom/google/k/c/bp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public b()Lcom/google/k/c/cd;
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/k/c/bo;->b:Lcom/google/k/c/cd;

    .line 393
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/k/c/bo;->c()Lcom/google/k/c/cd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/bo;->b:Lcom/google/k/c/cd;

    :cond_0
    return-object v0
.end method

.method abstract c()Lcom/google/k/c/cd;
.end method

.method public final clear()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 362
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 372
    invoke-virtual {p0, p1}, Lcom/google/k/c/bo;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 377
    invoke-virtual {p0}, Lcom/google/k/c/bo;->f()Lcom/google/k/c/bf;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/k/c/bf;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public d()Lcom/google/k/c/cd;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/google/k/c/bo;->c:Lcom/google/k/c/cd;

    .line 407
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/k/c/bo;->i()Lcom/google/k/c/cd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/bo;->c:Lcom/google/k/c/cd;

    :cond_0
    return-object v0
.end method

.method public synthetic entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/k/c/bo;->b()Lcom/google/k/c/cd;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 507
    invoke-static {p0, p1}, Lcom/google/k/c/cv;->d(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public f()Lcom/google/k/c/bf;
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/google/k/c/bo;->d:Lcom/google/k/c/bf;

    .line 423
    if-nez v0, :cond_0

    new-instance v0, Lcom/google/k/c/bv;

    invoke-direct {v0, p0}, Lcom/google/k/c/bv;-><init>(Lcom/google/k/c/bo;)V

    iput-object v0, p0, Lcom/google/k/c/bo;->d:Lcom/google/k/c/bf;

    :cond_0
    return-object v0
.end method

.method public abstract get(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 515
    invoke-virtual {p0}, Lcom/google/k/c/bo;->b()Lcom/google/k/c/cd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/cd;->hashCode()I

    move-result v0

    return v0
.end method

.method i()Lcom/google/k/c/cd;
    .locals 1

    .prologue
    .line 411
    new-instance v0, Lcom/google/k/c/bt;

    invoke-direct {v0, p0}, Lcom/google/k/c/bt;-><init>(Lcom/google/k/c/bo;)V

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 367
    invoke-virtual {p0}, Lcom/google/k/c/bo;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/k/c/bo;->d()Lcom/google/k/c/cd;

    move-result-object v0

    return-object v0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 326
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final putAll(Ljava/util/Map;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 350
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 338
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 519
    invoke-static {p0}, Lcom/google/k/c/cv;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/k/c/bo;->f()Lcom/google/k/c/bf;

    move-result-object v0

    return-object v0
.end method
