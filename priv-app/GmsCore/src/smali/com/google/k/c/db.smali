.class abstract enum Lcom/google/k/c/db;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/k/a/u;


# static fields
.field public static final enum a:Lcom/google/k/c/db;

.field public static final enum b:Lcom/google/k/c/db;

.field private static final synthetic c:[Lcom/google/k/c/db;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 86
    new-instance v0, Lcom/google/k/c/dc;

    const-string v1, "KEY"

    invoke-direct {v0, v1}, Lcom/google/k/c/dc;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/k/c/db;->a:Lcom/google/k/c/db;

    .line 93
    new-instance v0, Lcom/google/k/c/dd;

    const-string v1, "VALUE"

    invoke-direct {v0, v1}, Lcom/google/k/c/dd;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/k/c/db;->b:Lcom/google/k/c/db;

    .line 85
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/k/c/db;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/k/c/db;->a:Lcom/google/k/c/db;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/k/c/db;->b:Lcom/google/k/c/db;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/k/c/db;->c:[Lcom/google/k/c/db;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Lcom/google/k/c/db;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/k/c/db;
    .locals 1

    .prologue
    .line 85
    const-class v0, Lcom/google/k/c/db;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/k/c/db;

    return-object v0
.end method

.method public static values()[Lcom/google/k/c/db;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/google/k/c/db;->c:[Lcom/google/k/c/db;

    invoke-virtual {v0}, [Lcom/google/k/c/db;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/k/c/db;

    return-object v0
.end method
