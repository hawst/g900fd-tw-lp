.class Lcom/google/k/c/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final a:Ljava/util/Iterator;

.field final b:Ljava/util/Collection;

.field final synthetic c:Lcom/google/k/c/q;


# direct methods
.method constructor <init>(Lcom/google/k/c/q;)V
    .locals 1

    .prologue
    .line 458
    iput-object p1, p0, Lcom/google/k/c/r;->c:Lcom/google/k/c/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456
    iget-object v0, p0, Lcom/google/k/c/r;->c:Lcom/google/k/c/q;

    iget-object v0, v0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    iput-object v0, p0, Lcom/google/k/c/r;->b:Ljava/util/Collection;

    .line 459
    iget-object v0, p1, Lcom/google/k/c/q;->f:Lcom/google/k/c/e;

    iget-object v0, p1, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    invoke-static {v0}, Lcom/google/k/c/e;->a(Ljava/util/Collection;)Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/r;->a:Ljava/util/Iterator;

    .line 460
    return-void
.end method

.method constructor <init>(Lcom/google/k/c/q;Ljava/util/Iterator;)V
    .locals 1

    .prologue
    .line 462
    iput-object p1, p0, Lcom/google/k/c/r;->c:Lcom/google/k/c/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456
    iget-object v0, p0, Lcom/google/k/c/r;->c:Lcom/google/k/c/q;

    iget-object v0, v0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    iput-object v0, p0, Lcom/google/k/c/r;->b:Ljava/util/Collection;

    .line 463
    iput-object p2, p0, Lcom/google/k/c/r;->a:Ljava/util/Iterator;

    .line 464
    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/k/c/r;->c:Lcom/google/k/c/q;

    invoke-virtual {v0}, Lcom/google/k/c/q;->a()V

    .line 472
    iget-object v0, p0, Lcom/google/k/c/r;->c:Lcom/google/k/c/q;

    iget-object v0, v0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    iget-object v1, p0, Lcom/google/k/c/r;->b:Ljava/util/Collection;

    if-eq v0, v1, :cond_0

    .line 473
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 475
    :cond_0
    return-void
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 479
    invoke-virtual {p0}, Lcom/google/k/c/r;->a()V

    .line 480
    iget-object v0, p0, Lcom/google/k/c/r;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 485
    invoke-virtual {p0}, Lcom/google/k/c/r;->a()V

    .line 486
    iget-object v0, p0, Lcom/google/k/c/r;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lcom/google/k/c/r;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 492
    iget-object v0, p0, Lcom/google/k/c/r;->c:Lcom/google/k/c/q;

    iget-object v0, v0, Lcom/google/k/c/q;->f:Lcom/google/k/c/e;

    invoke-static {v0}, Lcom/google/k/c/e;->b(Lcom/google/k/c/e;)I

    .line 493
    iget-object v0, p0, Lcom/google/k/c/r;->c:Lcom/google/k/c/q;

    invoke-virtual {v0}, Lcom/google/k/c/q;->b()V

    .line 494
    return-void
.end method
