.class final Lcom/google/k/c/v;
.super Lcom/google/k/c/q;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedSet;


# instance fields
.field final synthetic a:Lcom/google/k/c/e;


# direct methods
.method constructor <init>(Lcom/google/k/c/e;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/google/k/c/q;)V
    .locals 0

    .prologue
    .line 632
    iput-object p1, p0, Lcom/google/k/c/v;->a:Lcom/google/k/c/e;

    .line 633
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/k/c/q;-><init>(Lcom/google/k/c/e;Ljava/lang/Object;Ljava/util/Collection;Lcom/google/k/c/q;)V

    .line 634
    return-void
.end method


# virtual methods
.method public final comparator()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 642
    iget-object v0, p0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final first()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 647
    invoke-virtual {p0}, Lcom/google/k/c/v;->a()V

    .line 648
    iget-object v0, p0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5

    .prologue
    .line 659
    invoke-virtual {p0}, Lcom/google/k/c/v;->a()V

    .line 660
    new-instance v1, Lcom/google/k/c/v;

    iget-object v2, p0, Lcom/google/k/c/v;->a:Lcom/google/k/c/e;

    iget-object v3, p0, Lcom/google/k/c/q;->b:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v4, p0, Lcom/google/k/c/q;->d:Lcom/google/k/c/q;

    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v1, v2, v3, v0, p0}, Lcom/google/k/c/v;-><init>(Lcom/google/k/c/e;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/google/k/c/q;)V

    return-object v1

    :cond_0
    iget-object p0, p0, Lcom/google/k/c/q;->d:Lcom/google/k/c/q;

    goto :goto_0
.end method

.method public final last()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 653
    invoke-virtual {p0}, Lcom/google/k/c/v;->a()V

    .line 654
    iget-object v0, p0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5

    .prologue
    .line 667
    invoke-virtual {p0}, Lcom/google/k/c/v;->a()V

    .line 668
    new-instance v1, Lcom/google/k/c/v;

    iget-object v2, p0, Lcom/google/k/c/v;->a:Lcom/google/k/c/e;

    iget-object v3, p0, Lcom/google/k/c/q;->b:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0, p1, p2}, Ljava/util/SortedSet;->subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v4, p0, Lcom/google/k/c/q;->d:Lcom/google/k/c/q;

    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v1, v2, v3, v0, p0}, Lcom/google/k/c/v;-><init>(Lcom/google/k/c/e;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/google/k/c/q;)V

    return-object v1

    :cond_0
    iget-object p0, p0, Lcom/google/k/c/q;->d:Lcom/google/k/c/q;

    goto :goto_0
.end method

.method public final tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5

    .prologue
    .line 675
    invoke-virtual {p0}, Lcom/google/k/c/v;->a()V

    .line 676
    new-instance v1, Lcom/google/k/c/v;

    iget-object v2, p0, Lcom/google/k/c/v;->a:Lcom/google/k/c/e;

    iget-object v3, p0, Lcom/google/k/c/q;->b:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/k/c/q;->c:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v4, p0, Lcom/google/k/c/q;->d:Lcom/google/k/c/q;

    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v1, v2, v3, v0, p0}, Lcom/google/k/c/v;-><init>(Lcom/google/k/c/e;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/google/k/c/q;)V

    return-object v1

    :cond_0
    iget-object p0, p0, Lcom/google/k/c/q;->d:Lcom/google/k/c/q;

    goto :goto_0
.end method
