.class abstract Lcom/google/k/c/dx;
.super Lcom/google/k/c/eo;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 964
    invoke-direct {p0}, Lcom/google/k/c/eo;-><init>()V

    return-void
.end method


# virtual methods
.method abstract a()Lcom/google/k/c/dq;
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 1002
    invoke-virtual {p0}, Lcom/google/k/c/dx;->a()Lcom/google/k/c/dq;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/k/c/dq;->clear()V

    .line 1003
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 968
    instance-of v1, p1, Lcom/google/k/c/dr;

    if-eqz v1, :cond_0

    .line 973
    check-cast p1, Lcom/google/k/c/dr;

    .line 974
    invoke-interface {p1}, Lcom/google/k/c/dr;->b()I

    move-result v1

    if-gtz v1, :cond_1

    .line 981
    :cond_0
    :goto_0
    return v0

    .line 977
    :cond_1
    invoke-virtual {p0}, Lcom/google/k/c/dx;->a()Lcom/google/k/c/dq;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/k/c/dr;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/k/c/dq;->a(Ljava/lang/Object;)I

    move-result v1

    .line 978
    invoke-interface {p1}, Lcom/google/k/c/dr;->b()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 987
    instance-of v1, p1, Lcom/google/k/c/dr;

    if-eqz v1, :cond_0

    .line 988
    check-cast p1, Lcom/google/k/c/dr;

    .line 989
    invoke-interface {p1}, Lcom/google/k/c/dr;->a()Ljava/lang/Object;

    move-result-object v1

    .line 990
    invoke-interface {p1}, Lcom/google/k/c/dr;->b()I

    move-result v2

    .line 991
    if-eqz v2, :cond_0

    .line 994
    invoke-virtual {p0}, Lcom/google/k/c/dx;->a()Lcom/google/k/c/dq;

    move-result-object v3

    .line 995
    invoke-interface {v3, v1, v2, v0}, Lcom/google/k/c/dq;->a(Ljava/lang/Object;II)Z

    move-result v0

    .line 998
    :cond_0
    return v0
.end method
