.class public abstract enum Lcom/google/k/c/ak;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/k/c/ak;

.field public static final enum b:Lcom/google/k/c/ak;

.field private static final synthetic c:[Lcom/google/k/c/ak;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    new-instance v0, Lcom/google/k/c/al;

    const-string v1, "OPEN"

    invoke-direct {v0, v1}, Lcom/google/k/c/al;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/k/c/ak;->a:Lcom/google/k/c/ak;

    .line 40
    new-instance v0, Lcom/google/k/c/am;

    const-string v1, "CLOSED"

    invoke-direct {v0, v1}, Lcom/google/k/c/am;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/k/c/ak;->b:Lcom/google/k/c/ak;

    .line 26
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/k/c/ak;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/k/c/ak;->a:Lcom/google/k/c/ak;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/k/c/ak;->b:Lcom/google/k/c/ak;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/k/c/ak;->c:[Lcom/google/k/c/ak;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/k/c/ak;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/k/c/ak;
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/k/c/ak;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/k/c/ak;

    return-object v0
.end method

.method public static values()[Lcom/google/k/c/ak;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/k/c/ak;->c:[Lcom/google/k/c/ak;

    invoke-virtual {v0}, [Lcom/google/k/c/ak;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/k/c/ak;

    return-object v0
.end method
