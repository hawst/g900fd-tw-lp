.class Lcom/google/k/c/dl;
.super Lcom/google/k/c/di;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedMap;


# direct methods
.method constructor <init>(Ljava/util/SortedMap;Lcom/google/k/c/df;)V
    .locals 0

    .prologue
    .line 1945
    invoke-direct {p0, p1, p2}, Lcom/google/k/c/di;-><init>(Ljava/util/Map;Lcom/google/k/c/df;)V

    .line 1946
    return-void
.end method


# virtual methods
.method protected c()Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 1940
    iget-object v0, p0, Lcom/google/k/c/dl;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 1949
    invoke-virtual {p0}, Lcom/google/k/c/dl;->c()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public firstKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1953
    invoke-virtual {p0}, Lcom/google/k/c/dl;->c()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 2

    .prologue
    .line 1957
    invoke-virtual {p0}, Lcom/google/k/c/dl;->c()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/k/c/dl;->b:Lcom/google/k/c/df;

    invoke-static {v0, v1}, Lcom/google/k/c/cv;->a(Ljava/util/SortedMap;Lcom/google/k/c/df;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method public lastKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1961
    invoke-virtual {p0}, Lcom/google/k/c/dl;->c()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 2

    .prologue
    .line 1965
    invoke-virtual {p0}, Lcom/google/k/c/dl;->c()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/k/c/dl;->b:Lcom/google/k/c/df;

    invoke-static {v0, v1}, Lcom/google/k/c/cv;->a(Ljava/util/SortedMap;Lcom/google/k/c/df;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method public tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 2

    .prologue
    .line 1970
    invoke-virtual {p0}, Lcom/google/k/c/dl;->c()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/k/c/dl;->b:Lcom/google/k/c/df;

    invoke-static {v0, v1}, Lcom/google/k/c/cv;->a(Ljava/util/SortedMap;Lcom/google/k/c/df;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method
