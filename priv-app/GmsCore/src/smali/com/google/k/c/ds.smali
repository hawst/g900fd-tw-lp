.class public final Lcom/google/k/c/ds;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/k/c/eb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1080
    new-instance v0, Lcom/google/k/c/dt;

    invoke-direct {v0}, Lcom/google/k/c/dt;-><init>()V

    sput-object v0, Lcom/google/k/c/ds;->a:Lcom/google/k/c/eb;

    return-void
.end method

.method static a(Lcom/google/k/c/dq;Ljava/lang/Object;I)I
    .locals 2

    .prologue
    .line 891
    const-string v0, "count"

    invoke-static {p2, v0}, Lcom/google/k/c/ao;->a(ILjava/lang/String;)I

    .line 893
    invoke-interface {p0, p1}, Lcom/google/k/c/dq;->a(Ljava/lang/Object;)I

    move-result v0

    .line 895
    sub-int v1, p2, v0

    .line 896
    if-lez v1, :cond_1

    .line 897
    invoke-interface {p0, p1, v1}, Lcom/google/k/c/dq;->a(Ljava/lang/Object;I)I

    .line 902
    :cond_0
    :goto_0
    return v0

    .line 898
    :cond_1
    if-gez v1, :cond_0

    .line 899
    neg-int v1, v1

    invoke-interface {p0, p1, v1}, Lcom/google/k/c/dq;->b(Ljava/lang/Object;I)I

    goto :goto_0
.end method

.method static a(Lcom/google/k/c/dq;)Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 1010
    new-instance v0, Lcom/google/k/c/dy;

    invoke-interface {p0}, Lcom/google/k/c/dq;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/k/c/dy;-><init>(Lcom/google/k/c/dq;Ljava/util/Iterator;)V

    return-object v0
.end method

.method static a(Lcom/google/k/c/dq;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 820
    if-ne p1, p0, :cond_0

    move v0, v1

    .line 842
    :goto_0
    return v0

    .line 823
    :cond_0
    instance-of v0, p1, Lcom/google/k/c/dq;

    if-eqz v0, :cond_5

    .line 824
    check-cast p1, Lcom/google/k/c/dq;

    .line 831
    invoke-interface {p0}, Lcom/google/k/c/dq;->size()I

    move-result v0

    invoke-interface {p1}, Lcom/google/k/c/dq;->size()I

    move-result v3

    if-ne v0, v3, :cond_1

    invoke-interface {p0}, Lcom/google/k/c/dq;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-interface {p1}, Lcom/google/k/c/dq;->a()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-eq v0, v3, :cond_2

    :cond_1
    move v0, v2

    .line 833
    goto :goto_0

    .line 835
    :cond_2
    invoke-interface {p1}, Lcom/google/k/c/dq;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/c/dr;

    .line 836
    invoke-interface {v0}, Lcom/google/k/c/dr;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p0, v4}, Lcom/google/k/c/dq;->a(Ljava/lang/Object;)I

    move-result v4

    invoke-interface {v0}, Lcom/google/k/c/dr;->b()I

    move-result v0

    if-eq v4, v0, :cond_3

    move v0, v2

    .line 837
    goto :goto_0

    :cond_4
    move v0, v1

    .line 840
    goto :goto_0

    :cond_5
    move v0, v2

    .line 842
    goto :goto_0
.end method

.method static a(Lcom/google/k/c/dq;Ljava/lang/Object;II)Z
    .locals 1

    .prologue
    .line 910
    const-string v0, "oldCount"

    invoke-static {p2, v0}, Lcom/google/k/c/ao;->a(ILjava/lang/String;)I

    .line 911
    const-string v0, "newCount"

    invoke-static {p3, v0}, Lcom/google/k/c/ao;->a(ILjava/lang/String;)I

    .line 913
    invoke-interface {p0, p1}, Lcom/google/k/c/dq;->a(Ljava/lang/Object;)I

    move-result v0

    if-ne v0, p2, :cond_0

    .line 914
    invoke-interface {p0, p1, p3}, Lcom/google/k/c/dq;->c(Ljava/lang/Object;I)I

    .line 915
    const/4 v0, 0x1

    .line 917
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Lcom/google/k/c/dq;Ljava/util/Collection;)Z
    .locals 3

    .prologue
    .line 850
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 851
    const/4 v0, 0x0

    .line 861
    :goto_0
    return v0

    .line 853
    :cond_0
    instance-of v0, p1, Lcom/google/k/c/dq;

    if-eqz v0, :cond_1

    .line 854
    check-cast p1, Lcom/google/k/c/dq;

    .line 855
    invoke-interface {p1}, Lcom/google/k/c/dq;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/c/dr;

    .line 856
    invoke-interface {v0}, Lcom/google/k/c/dr;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/k/c/dr;->b()I

    move-result v0

    invoke-interface {p0, v2, v0}, Lcom/google/k/c/dq;->a(Ljava/lang/Object;I)I

    goto :goto_1

    .line 859
    :cond_1
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/k/c/cj;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    .line 861
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static b(Lcom/google/k/c/dq;)I
    .locals 5

    .prologue
    .line 1066
    const-wide/16 v0, 0x0

    .line 1067
    invoke-interface {p0}, Lcom/google/k/c/dq;->a()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/c/dr;

    .line 1068
    invoke-interface {v0}, Lcom/google/k/c/dr;->b()I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 1069
    goto :goto_0

    .line 1070
    :cond_0
    invoke-static {v2, v3}, Lcom/google/k/h/a;->a(J)I

    move-result v0

    return v0
.end method

.method static b(Lcom/google/k/c/dq;Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 869
    instance-of v0, p1, Lcom/google/k/c/dq;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/k/c/dq;

    invoke-interface {p1}, Lcom/google/k/c/dq;->d()Ljava/util/Set;

    move-result-object p1

    .line 872
    :cond_0
    invoke-interface {p0}, Lcom/google/k/c/dq;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method static c(Lcom/google/k/c/dq;Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 880
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 881
    instance-of v0, p1, Lcom/google/k/c/dq;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/k/c/dq;

    invoke-interface {p1}, Lcom/google/k/c/dq;->d()Ljava/util/Set;

    move-result-object p1

    .line 884
    :cond_0
    invoke-interface {p0}, Lcom/google/k/c/dq;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method
