.class final Lcom/google/k/c/o;
.super Lcom/google/k/c/h;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedMap;


# instance fields
.field c:Ljava/util/SortedSet;

.field final synthetic d:Lcom/google/k/c/e;


# direct methods
.method constructor <init>(Lcom/google/k/c/e;Ljava/util/SortedMap;)V
    .locals 0

    .prologue
    .line 1365
    iput-object p1, p0, Lcom/google/k/c/o;->d:Lcom/google/k/c/e;

    .line 1366
    invoke-direct {p0, p1, p2}, Lcom/google/k/c/h;-><init>(Lcom/google/k/c/e;Ljava/util/Map;)V

    .line 1367
    return-void
.end method

.method private c()Ljava/util/SortedSet;
    .locals 3

    .prologue
    .line 1414
    new-instance v1, Lcom/google/k/c/p;

    iget-object v2, p0, Lcom/google/k/c/o;->d:Lcom/google/k/c/e;

    iget-object v0, p0, Lcom/google/k/c/o;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-direct {v1, v2, v0}, Lcom/google/k/c/p;-><init>(Lcom/google/k/c/e;Ljava/util/SortedMap;)V

    return-object v1
.end method


# virtual methods
.method final synthetic b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 1363
    invoke-direct {p0}, Lcom/google/k/c/o;->c()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final comparator()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 1375
    iget-object v0, p0, Lcom/google/k/c/o;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final firstKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1380
    iget-object v0, p0, Lcom/google/k/c/o;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 3

    .prologue
    .line 1390
    new-instance v1, Lcom/google/k/c/o;

    iget-object v2, p0, Lcom/google/k/c/o;->d:Lcom/google/k/c/e;

    iget-object v0, p0, Lcom/google/k/c/o;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/k/c/o;-><init>(Lcom/google/k/c/e;Ljava/util/SortedMap;)V

    return-object v1
.end method

.method public final synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 1363
    iget-object v0, p0, Lcom/google/k/c/o;->c:Ljava/util/SortedSet;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/k/c/o;->c()Ljava/util/SortedSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/o;->c:Ljava/util/SortedSet;

    :cond_0
    return-object v0
.end method

.method public final lastKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1385
    iget-object v0, p0, Lcom/google/k/c/o;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 3

    .prologue
    .line 1395
    new-instance v1, Lcom/google/k/c/o;

    iget-object v2, p0, Lcom/google/k/c/o;->d:Lcom/google/k/c/e;

    iget-object v0, p0, Lcom/google/k/c/o;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1, p2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/k/c/o;-><init>(Lcom/google/k/c/e;Ljava/util/SortedMap;)V

    return-object v1
.end method

.method public final tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 3

    .prologue
    .line 1400
    new-instance v1, Lcom/google/k/c/o;

    iget-object v2, p0, Lcom/google/k/c/o;->d:Lcom/google/k/c/e;

    iget-object v0, p0, Lcom/google/k/c/o;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/k/c/o;-><init>(Lcom/google/k/c/e;Ljava/util/SortedMap;)V

    return-object v1
.end method
