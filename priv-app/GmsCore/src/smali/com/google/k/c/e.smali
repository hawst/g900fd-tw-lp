.class abstract Lcom/google/k/c/e;
.super Lcom/google/k/c/ab;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private transient a:Ljava/util/Map;

.field private transient b:I


# direct methods
.method protected constructor <init>(Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/k/c/ab;-><init>()V

    .line 123
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    invoke-static {v0}, Lcom/google/k/a/ah;->a(Z)V

    .line 124
    iput-object p1, p0, Lcom/google/k/c/e;->a:Ljava/util/Map;

    .line 125
    return-void
.end method

.method static synthetic a(Lcom/google/k/c/e;I)I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/google/k/c/e;->b:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/k/c/e;->b:I

    return v0
.end method

.method static synthetic a(Lcom/google/k/c/e;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/k/c/e;->a:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/k/c/cv;->c(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    iget v0, p0, Lcom/google/k/c/e;->b:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/k/c/e;->b:I

    :cond_0
    move v0, v1

    return v0
.end method

.method static synthetic a(Ljava/util/Collection;)Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 91
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/k/c/e;Ljava/lang/Object;Ljava/util/List;Lcom/google/k/c/q;)Ljava/util/List;
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0, p1, p2, p3}, Lcom/google/k/c/e;->a(Ljava/lang/Object;Ljava/util/List;Lcom/google/k/c/q;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;Ljava/util/List;Lcom/google/k/c/q;)Ljava/util/List;
    .locals 1

    .prologue
    .line 332
    instance-of v0, p2, Ljava/util/RandomAccess;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/k/c/n;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/k/c/n;-><init>(Lcom/google/k/c/e;Ljava/lang/Object;Ljava/util/List;Lcom/google/k/c/q;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/k/c/s;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/k/c/s;-><init>(Lcom/google/k/c/e;Ljava/lang/Object;Ljava/util/List;Lcom/google/k/c/q;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/k/c/e;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/k/c/e;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lcom/google/k/c/e;)I
    .locals 2

    .prologue
    .line 91
    iget v0, p0, Lcom/google/k/c/e;->b:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/k/c/e;->b:I

    return v0
.end method

.method static synthetic b(Lcom/google/k/c/e;I)I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/google/k/c/e;->b:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/google/k/c/e;->b:I

    return v0
.end method

.method private static b(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 274
    instance-of v0, p0, Ljava/util/SortedSet;

    if-eqz v0, :cond_0

    .line 275
    check-cast p0, Ljava/util/SortedSet;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSortedSet(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v0

    .line 281
    :goto_0
    return-object v0

    .line 276
    :cond_0
    instance-of v0, p0, Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 277
    check-cast p0, Ljava/util/Set;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0

    .line 278
    :cond_1
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_2

    .line 279
    check-cast p0, Ljava/util/List;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 281
    :cond_2
    invoke-static {p0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/k/c/e;)I
    .locals 2

    .prologue
    .line 91
    iget v0, p0, Lcom/google/k/c/e;->b:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/k/c/e;->b:I

    return v0
.end method


# virtual methods
.method a()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/google/k/c/e;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/c/e;->b(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 319
    instance-of v0, p2, Ljava/util/SortedSet;

    if-eqz v0, :cond_0

    .line 320
    new-instance v0, Lcom/google/k/c/v;

    check-cast p2, Ljava/util/SortedSet;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/k/c/v;-><init>(Lcom/google/k/c/e;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/google/k/c/q;)V

    .line 326
    :goto_0
    return-object v0

    .line 321
    :cond_0
    instance-of v0, p2, Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 322
    new-instance v0, Lcom/google/k/c/u;

    check-cast p2, Ljava/util/Set;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/k/c/u;-><init>(Lcom/google/k/c/e;Ljava/lang/Object;Ljava/util/Set;)V

    goto :goto_0

    .line 323
    :cond_1
    instance-of v0, p2, Ljava/util/List;

    if-eqz v0, :cond_2

    .line 324
    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p1, p2, v1}, Lcom/google/k/c/e;->a(Ljava/lang/Object;Ljava/util/List;Lcom/google/k/c/q;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 326
    :cond_2
    new-instance v0, Lcom/google/k/c/q;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/k/c/q;-><init>(Lcom/google/k/c/e;Ljava/lang/Object;Ljava/util/Collection;Lcom/google/k/c/q;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/k/c/e;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 192
    iget-object v0, p0, Lcom/google/k/c/e;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 193
    if-nez v0, :cond_1

    .line 194
    invoke-virtual {p0}, Lcom/google/k/c/e;->b()Ljava/util/Collection;

    move-result-object v0

    .line 195
    invoke-interface {v0, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 196
    iget v2, p0, Lcom/google/k/c/e;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/k/c/e;->b:I

    .line 197
    iget-object v2, p0, Lcom/google/k/c/e;->a:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 206
    :goto_0
    return v0

    .line 200
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "New Collection violated the Collection spec"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 202
    :cond_1
    invoke-interface {v0, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 203
    iget v0, p0, Lcom/google/k/c/e;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/k/c/e;->b:I

    move v0, v1

    .line 204
    goto :goto_0

    .line 206
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method abstract b()Ljava/util/Collection;
.end method

.method public b(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 4

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/k/c/e;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 259
    if-nez v0, :cond_0

    .line 260
    invoke-virtual {p0}, Lcom/google/k/c/e;->a()Ljava/util/Collection;

    move-result-object v0

    .line 268
    :goto_0
    return-object v0

    .line 263
    :cond_0
    invoke-virtual {p0}, Lcom/google/k/c/e;->b()Ljava/util/Collection;

    move-result-object v1

    .line 264
    invoke-interface {v1, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 265
    iget v2, p0, Lcom/google/k/c/e;->b:I

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/google/k/c/e;->b:I

    .line 266
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 268
    invoke-static {v1}, Lcom/google/k/c/e;->b(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/google/k/c/e;->b:I

    return v0
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/k/c/e;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 305
    if-nez v0, :cond_0

    .line 306
    invoke-virtual {p0}, Lcom/google/k/c/e;->b()Ljava/util/Collection;

    move-result-object v0

    .line 308
    :cond_0
    invoke-virtual {p0, p1, v0}, Lcom/google/k/c/e;->a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/k/c/e;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 289
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    goto :goto_0

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/google/k/c/e;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 292
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/k/c/e;->b:I

    .line 293
    return-void
.end method

.method final e()Ljava/util/Set;
    .locals 2

    .prologue
    .line 915
    iget-object v0, p0, Lcom/google/k/c/e;->a:Ljava/util/Map;

    instance-of v0, v0, Ljava/util/SortedMap;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/k/c/p;

    iget-object v0, p0, Lcom/google/k/c/e;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-direct {v1, p0, v0}, Lcom/google/k/c/p;-><init>(Lcom/google/k/c/e;Ljava/util/SortedMap;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/k/c/l;

    iget-object v1, p0, Lcom/google/k/c/e;->a:Ljava/util/Map;

    invoke-direct {v0, p0, v1}, Lcom/google/k/c/l;-><init>(Lcom/google/k/c/e;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public f()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 1170
    invoke-super {p0}, Lcom/google/k/c/ab;->f()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method final g()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 1175
    new-instance v0, Lcom/google/k/c/f;

    invoke-direct {v0, p0}, Lcom/google/k/c/f;-><init>(Lcom/google/k/c/e;)V

    return-object v0
.end method

.method public h()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 1201
    invoke-super {p0}, Lcom/google/k/c/ab;->h()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method final i()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 1214
    new-instance v0, Lcom/google/k/c/g;

    invoke-direct {v0, p0}, Lcom/google/k/c/g;-><init>(Lcom/google/k/c/e;)V

    return-object v0
.end method

.method final j()Ljava/util/Map;
    .locals 2

    .prologue
    .line 1226
    iget-object v0, p0, Lcom/google/k/c/e;->a:Ljava/util/Map;

    instance-of v0, v0, Ljava/util/SortedMap;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/k/c/o;

    iget-object v0, p0, Lcom/google/k/c/e;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-direct {v1, p0, v0}, Lcom/google/k/c/o;-><init>(Lcom/google/k/c/e;Ljava/util/SortedMap;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/k/c/h;

    iget-object v1, p0, Lcom/google/k/c/e;->a:Ljava/util/Map;

    invoke-direct {v0, p0, v1}, Lcom/google/k/c/h;-><init>(Lcom/google/k/c/e;Ljava/util/Map;)V

    goto :goto_0
.end method
