.class final Lcom/google/k/c/cu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field a:Z

.field final synthetic b:Ljava/util/ListIterator;

.field final synthetic c:Lcom/google/k/c/ct;


# direct methods
.method constructor <init>(Lcom/google/k/c/ct;Ljava/util/ListIterator;)V
    .locals 0

    .prologue
    .line 862
    iput-object p1, p0, Lcom/google/k/c/cu;->c:Lcom/google/k/c/ct;

    iput-object p2, p0, Lcom/google/k/c/cu;->b:Ljava/util/ListIterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 867
    iget-object v0, p0, Lcom/google/k/c/cu;->b:Ljava/util/ListIterator;

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 868
    iget-object v0, p0, Lcom/google/k/c/cu;->b:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    .line 869
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/k/c/cu;->a:Z

    .line 870
    return-void
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 873
    iget-object v0, p0, Lcom/google/k/c/cu;->b:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public final hasPrevious()Z
    .locals 1

    .prologue
    .line 877
    iget-object v0, p0, Lcom/google/k/c/cu;->b:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 881
    invoke-virtual {p0}, Lcom/google/k/c/cu;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 882
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 884
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/k/c/cu;->a:Z

    .line 885
    iget-object v0, p0, Lcom/google/k/c/cu;->b:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final nextIndex()I
    .locals 2

    .prologue
    .line 889
    iget-object v0, p0, Lcom/google/k/c/cu;->c:Lcom/google/k/c/ct;

    iget-object v1, p0, Lcom/google/k/c/cu;->b:Ljava/util/ListIterator;

    invoke-interface {v1}, Ljava/util/ListIterator;->nextIndex()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/k/c/ct;->a(Lcom/google/k/c/ct;I)I

    move-result v0

    return v0
.end method

.method public final previous()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 893
    invoke-virtual {p0}, Lcom/google/k/c/cu;->hasPrevious()Z

    move-result v0

    if-nez v0, :cond_0

    .line 894
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 896
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/k/c/cu;->a:Z

    .line 897
    iget-object v0, p0, Lcom/google/k/c/cu;->b:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final previousIndex()I
    .locals 1

    .prologue
    .line 901
    invoke-virtual {p0}, Lcom/google/k/c/cu;->nextIndex()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 905
    iget-boolean v0, p0, Lcom/google/k/c/cu;->a:Z

    const-string v1, "no calls to next() since the last call to remove()"

    invoke-static {v0, v1}, Lcom/google/k/a/ah;->b(ZLjava/lang/Object;)V

    .line 906
    iget-object v0, p0, Lcom/google/k/c/cu;->b:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->remove()V

    .line 907
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/k/c/cu;->a:Z

    .line 908
    return-void
.end method

.method public final set(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 911
    iget-boolean v0, p0, Lcom/google/k/c/cu;->a:Z

    invoke-static {v0}, Lcom/google/k/a/ah;->b(Z)V

    .line 912
    iget-object v0, p0, Lcom/google/k/c/cu;->b:Ljava/util/ListIterator;

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 913
    return-void
.end method
