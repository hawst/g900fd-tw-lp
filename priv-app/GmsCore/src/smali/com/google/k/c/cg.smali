.class final Lcom/google/k/c/cg;
.super Lcom/google/k/c/cd;
.source "SourceFile"


# instance fields
.field private final transient a:Lcom/google/k/c/cf;


# direct methods
.method constructor <init>(Lcom/google/k/c/cf;)V
    .locals 0

    .prologue
    .line 428
    invoke-direct {p0}, Lcom/google/k/c/cd;-><init>()V

    .line 429
    iput-object p1, p0, Lcom/google/k/c/cg;->a:Lcom/google/k/c/cf;

    .line 430
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/k/c/fl;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/google/k/c/cg;->a:Lcom/google/k/c/cf;

    invoke-virtual {v0}, Lcom/google/k/c/cf;->p()Lcom/google/k/c/fl;

    move-result-object v0

    return-object v0
.end method

.method final b()Z
    .locals 1

    .prologue
    .line 453
    const/4 v0, 0x0

    return v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 434
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-eqz v0, :cond_0

    .line 435
    check-cast p1, Ljava/util/Map$Entry;

    .line 436
    iget-object v0, p0, Lcom/google/k/c/cg;->a:Lcom/google/k/c/cf;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/cf;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 438
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcom/google/k/c/cg;->a:Lcom/google/k/c/cf;

    invoke-virtual {v0}, Lcom/google/k/c/cf;->p()Lcom/google/k/c/fl;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/google/k/c/cg;->a:Lcom/google/k/c/cf;

    invoke-virtual {v0}, Lcom/google/k/c/cf;->c()I

    move-result v0

    return v0
.end method
