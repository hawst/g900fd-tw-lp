.class public final Lcom/google/k/c/bp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:[Lcom/google/k/c/br;

.field b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/k/c/bp;-><init>(B)V

    .line 185
    return-void
.end method

.method private constructor <init>(B)V
    .locals 1

    .prologue
    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 189
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/k/c/br;

    iput-object v0, p0, Lcom/google/k/c/bp;->a:[Lcom/google/k/c/br;

    .line 190
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/k/c/bp;->b:I

    .line 191
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/k/c/bo;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 248
    iget v0, p0, Lcom/google/k/c/bp;->b:I

    packed-switch v0, :pswitch_data_0

    .line 254
    new-instance v0, Lcom/google/k/c/ef;

    iget v1, p0, Lcom/google/k/c/bp;->b:I

    iget-object v2, p0, Lcom/google/k/c/bp;->a:[Lcom/google/k/c/br;

    invoke-direct {v0, v1, v2}, Lcom/google/k/c/ef;-><init>(I[Lcom/google/k/c/br;)V

    :goto_0
    return-object v0

    .line 250
    :pswitch_0
    invoke-static {}, Lcom/google/k/c/bo;->g()Lcom/google/k/c/bo;

    move-result-object v0

    goto :goto_0

    .line 252
    :pswitch_1
    iget-object v0, p0, Lcom/google/k/c/bp;->a:[Lcom/google/k/c/br;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/google/k/c/br;->getKey()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/k/c/bp;->a:[Lcom/google/k/c/br;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/google/k/c/br;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/k/c/bo;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bo;

    move-result-object v0

    goto :goto_0

    .line 248
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;
    .locals 4

    .prologue
    .line 205
    iget v0, p0, Lcom/google/k/c/bp;->b:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/k/c/bp;->a:[Lcom/google/k/c/br;

    array-length v1, v1

    if-le v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/k/c/bp;->a:[Lcom/google/k/c/br;

    iget-object v2, p0, Lcom/google/k/c/bp;->a:[Lcom/google/k/c/br;

    array-length v2, v2

    invoke-static {v2, v0}, Lcom/google/k/c/bh;->a(II)I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/k/c/ea;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/k/c/br;

    iput-object v0, p0, Lcom/google/k/c/bp;->a:[Lcom/google/k/c/br;

    .line 206
    :cond_0
    invoke-static {p1, p2}, Lcom/google/k/c/bo;->c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/br;

    move-result-object v0

    .line 208
    iget-object v1, p0, Lcom/google/k/c/bp;->a:[Lcom/google/k/c/br;

    iget v2, p0, Lcom/google/k/c/bp;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/k/c/bp;->b:I

    aput-object v0, v1, v2

    .line 209
    return-object p0
.end method
