.class final Lcom/google/k/c/ev;
.super Lcom/google/k/c/fg;
.source "SourceFile"


# direct methods
.method constructor <init>(Ljava/util/Set;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 843
    invoke-direct {p0, p1, p2}, Lcom/google/k/c/fg;-><init>(Ljava/util/Set;Ljava/lang/Object;)V

    .line 844
    return-void
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 881
    iget-object v1, p0, Lcom/google/k/c/ev;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 882
    :try_start_0
    invoke-virtual {p0}, Lcom/google/k/c/ev;->c()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/k/c/cv;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 883
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .locals 2

    .prologue
    .line 886
    iget-object v1, p0, Lcom/google/k/c/ev;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 887
    :try_start_0
    invoke-virtual {p0}, Lcom/google/k/c/ev;->c()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/k/c/ap;->a(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 888
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 891
    if-ne p1, p0, :cond_0

    .line 892
    const/4 v0, 0x1

    .line 895
    :goto_0
    return v0

    .line 894
    :cond_0
    iget-object v1, p0, Lcom/google/k/c/ev;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 895
    :try_start_0
    invoke-virtual {p0}, Lcom/google/k/c/ev;->c()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/k/c/em;->a(Ljava/util/Set;Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 896
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 848
    invoke-super {p0}, Lcom/google/k/c/fg;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 849
    new-instance v1, Lcom/google/k/c/ew;

    invoke-direct {v1, p0, v0}, Lcom/google/k/c/ew;-><init>(Lcom/google/k/c/ev;Ljava/util/Iterator;)V

    return-object v1
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 899
    iget-object v1, p0, Lcom/google/k/c/ev;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 900
    :try_start_0
    invoke-virtual {p0}, Lcom/google/k/c/ev;->c()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/k/c/cv;->b(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 901
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .locals 2

    .prologue
    .line 904
    iget-object v1, p0, Lcom/google/k/c/ev;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 905
    :try_start_0
    invoke-virtual {p0}, Lcom/google/k/c/ev;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/k/c/cj;->a(Ljava/util/Iterator;Ljava/util/Collection;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 906
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 2

    .prologue
    .line 909
    iget-object v1, p0, Lcom/google/k/c/ev;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 910
    :try_start_0
    invoke-virtual {p0}, Lcom/google/k/c/ev;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/k/c/cj;->b(Ljava/util/Iterator;Ljava/util/Collection;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 911
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .locals 2

    .prologue
    .line 871
    iget-object v1, p0, Lcom/google/k/c/ev;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 872
    :try_start_0
    invoke-virtual {p0}, Lcom/google/k/c/ev;->c()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/c/ea;->a(Ljava/util/Collection;)[Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 873
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 2

    .prologue
    .line 876
    iget-object v1, p0, Lcom/google/k/c/ev;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 877
    :try_start_0
    invoke-virtual {p0}, Lcom/google/k/c/ev;->c()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/k/c/ea;->a(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 878
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
