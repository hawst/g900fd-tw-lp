.class final Lcom/google/k/c/p;
.super Lcom/google/k/c/l;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedSet;


# instance fields
.field final synthetic b:Lcom/google/k/c/e;


# direct methods
.method constructor <init>(Lcom/google/k/c/e;Ljava/util/SortedMap;)V
    .locals 0

    .prologue
    .line 983
    iput-object p1, p0, Lcom/google/k/c/p;->b:Lcom/google/k/c/e;

    .line 984
    invoke-direct {p0, p1, p2}, Lcom/google/k/c/l;-><init>(Lcom/google/k/c/e;Ljava/util/Map;)V

    .line 985
    return-void
.end method


# virtual methods
.method public final comparator()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 993
    iget-object v0, p0, Lcom/google/k/c/dh;->c:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final first()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 998
    iget-object v0, p0, Lcom/google/k/c/dh;->c:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 3

    .prologue
    .line 1003
    new-instance v1, Lcom/google/k/c/p;

    iget-object v2, p0, Lcom/google/k/c/p;->b:Lcom/google/k/c/e;

    iget-object v0, p0, Lcom/google/k/c/dh;->c:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/k/c/p;-><init>(Lcom/google/k/c/e;Ljava/util/SortedMap;)V

    return-object v1
.end method

.method public final last()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1008
    iget-object v0, p0, Lcom/google/k/c/dh;->c:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 3

    .prologue
    .line 1013
    new-instance v1, Lcom/google/k/c/p;

    iget-object v2, p0, Lcom/google/k/c/p;->b:Lcom/google/k/c/e;

    iget-object v0, p0, Lcom/google/k/c/dh;->c:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1, p2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/k/c/p;-><init>(Lcom/google/k/c/e;Ljava/util/SortedMap;)V

    return-object v1
.end method

.method public final tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 3

    .prologue
    .line 1018
    new-instance v1, Lcom/google/k/c/p;

    iget-object v2, p0, Lcom/google/k/c/p;->b:Lcom/google/k/c/e;

    iget-object v0, p0, Lcom/google/k/c/dh;->c:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/k/c/p;-><init>(Lcom/google/k/c/e;Ljava/util/SortedMap;)V

    return-object v1
.end method
