.class final Lcom/google/k/c/t;
.super Lcom/google/k/c/r;
.source "SourceFile"

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field final synthetic d:Lcom/google/k/c/s;


# direct methods
.method constructor <init>(Lcom/google/k/c/s;)V
    .locals 0

    .prologue
    .line 852
    iput-object p1, p0, Lcom/google/k/c/t;->d:Lcom/google/k/c/s;

    invoke-direct {p0, p1}, Lcom/google/k/c/r;-><init>(Lcom/google/k/c/q;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/k/c/s;I)V
    .locals 1

    .prologue
    .line 854
    iput-object p1, p0, Lcom/google/k/c/t;->d:Lcom/google/k/c/s;

    .line 855
    invoke-virtual {p1}, Lcom/google/k/c/s;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/k/c/r;-><init>(Lcom/google/k/c/q;Ljava/util/Iterator;)V

    .line 856
    return-void
.end method

.method private b()Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 859
    invoke-virtual {p0}, Lcom/google/k/c/r;->a()V

    iget-object v0, p0, Lcom/google/k/c/r;->a:Ljava/util/Iterator;

    check-cast v0, Ljava/util/ListIterator;

    return-object v0
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 889
    iget-object v0, p0, Lcom/google/k/c/t;->d:Lcom/google/k/c/s;

    invoke-virtual {v0}, Lcom/google/k/c/s;->isEmpty()Z

    move-result v0

    .line 890
    invoke-direct {p0}, Lcom/google/k/c/t;->b()Ljava/util/ListIterator;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 891
    iget-object v1, p0, Lcom/google/k/c/t;->d:Lcom/google/k/c/s;

    iget-object v1, v1, Lcom/google/k/c/s;->g:Lcom/google/k/c/e;

    invoke-static {v1}, Lcom/google/k/c/e;->c(Lcom/google/k/c/e;)I

    .line 892
    if-eqz v0, :cond_0

    .line 893
    iget-object v0, p0, Lcom/google/k/c/t;->d:Lcom/google/k/c/s;

    invoke-virtual {v0}, Lcom/google/k/c/s;->c()V

    .line 895
    :cond_0
    return-void
.end method

.method public final hasPrevious()Z
    .locals 1

    .prologue
    .line 864
    invoke-direct {p0}, Lcom/google/k/c/t;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public final nextIndex()I
    .locals 1

    .prologue
    .line 874
    invoke-direct {p0}, Lcom/google/k/c/t;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->nextIndex()I

    move-result v0

    return v0
.end method

.method public final previous()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 869
    invoke-direct {p0}, Lcom/google/k/c/t;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final previousIndex()I
    .locals 1

    .prologue
    .line 879
    invoke-direct {p0}, Lcom/google/k/c/t;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->previousIndex()I

    move-result v0

    return v0
.end method

.method public final set(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 884
    invoke-direct {p0}, Lcom/google/k/c/t;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 885
    return-void
.end method
