.class public abstract Lcom/google/k/c/bx;
.super Lcom/google/k/c/ab;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field final transient a:Lcom/google/k/c/bo;

.field final transient b:I


# virtual methods
.method public final a()Lcom/google/k/c/bo;
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/google/k/c/bx;->a:Lcom/google/k/c/bo;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/google/k/c/bx;->a:Lcom/google/k/c/bo;

    invoke-virtual {v0, p1}, Lcom/google/k/c/bo;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 392
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()Lcom/google/k/c/bf;
    .locals 1

    .prologue
    .line 492
    invoke-super {p0}, Lcom/google/k/c/ab;->h()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/google/k/c/bf;

    return-object v0
.end method

.method public abstract b(Ljava/lang/Object;)Lcom/google/k/c/bf;
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 64
    invoke-super {p0, p1, p2}, Lcom/google/k/c/ab;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 455
    iget v0, p0, Lcom/google/k/c/bx;->b:I

    return v0
.end method

.method public synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0, p1}, Lcom/google/k/c/bx;->b(Ljava/lang/Object;)Lcom/google/k/c/bf;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 428
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 362
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 450
    if-eqz p1, :cond_0

    invoke-super {p0, p1}, Lcom/google/k/c/ab;->d(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/google/k/c/ab;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic f()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 64
    invoke-super {p0}, Lcom/google/k/c/ab;->f()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/google/k/c/bf;

    return-object v0
.end method

.method final synthetic g()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/k/c/bx;->r()Lcom/google/k/c/fl;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/k/c/bx;->b()Lcom/google/k/c/bf;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic hashCode()I
    .locals 1

    .prologue
    .line 64
    invoke-super {p0}, Lcom/google/k/c/ab;->hashCode()I

    move-result v0

    return v0
.end method

.method final synthetic i()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/k/c/bx;->p()Lcom/google/k/c/fl;

    move-result-object v0

    return-object v0
.end method

.method final j()Ljava/util/Map;
    .locals 2

    .prologue
    .line 482
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final bridge synthetic k()Z
    .locals 1

    .prologue
    .line 64
    invoke-super {p0}, Lcom/google/k/c/ab;->k()Z

    move-result v0

    return v0
.end method

.method final synthetic l()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lcom/google/k/c/ca;

    invoke-direct {v0, p0}, Lcom/google/k/c/ca;-><init>(Lcom/google/k/c/bx;)V

    return-object v0
.end method

.method public final synthetic m()Ljava/util/Set;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/k/c/bx;->a:Lcom/google/k/c/bo;

    invoke-virtual {v0}, Lcom/google/k/c/bo;->d()Lcom/google/k/c/cd;

    move-result-object v0

    return-object v0
.end method

.method final synthetic n()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lcom/google/k/c/cc;

    invoke-direct {v0, p0}, Lcom/google/k/c/cc;-><init>(Lcom/google/k/c/bx;)V

    return-object v0
.end method

.method public final bridge synthetic o()Ljava/util/Map;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/k/c/bx;->a:Lcom/google/k/c/bo;

    return-object v0
.end method

.method final p()Lcom/google/k/c/fl;
    .locals 1

    .prologue
    .line 557
    new-instance v0, Lcom/google/k/c/by;

    invoke-direct {v0, p0}, Lcom/google/k/c/by;-><init>(Lcom/google/k/c/bx;)V

    return-object v0
.end method

.method final r()Lcom/google/k/c/fl;
    .locals 1

    .prologue
    .line 633
    new-instance v0, Lcom/google/k/c/bz;

    invoke-direct {v0, p0}, Lcom/google/k/c/bz;-><init>(Lcom/google/k/c/bx;)V

    return-object v0
.end method

.method public bridge synthetic toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    invoke-super {p0}, Lcom/google/k/c/ab;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
