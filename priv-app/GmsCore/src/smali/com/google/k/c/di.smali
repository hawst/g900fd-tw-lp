.class Lcom/google/k/c/di;
.super Lcom/google/k/c/dg;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Map;

.field final b:Lcom/google/k/c/df;


# direct methods
.method constructor <init>(Ljava/util/Map;Lcom/google/k/c/df;)V
    .locals 1

    .prologue
    .line 1883
    invoke-direct {p0}, Lcom/google/k/c/dg;-><init>()V

    .line 1884
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/google/k/c/di;->a:Ljava/util/Map;

    .line 1885
    invoke-static {p2}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/c/df;

    iput-object v0, p0, Lcom/google/k/c/di;->b:Lcom/google/k/c/df;

    .line 1886
    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/Set;
    .locals 1

    .prologue
    .line 1923
    new-instance v0, Lcom/google/k/c/dj;

    invoke-direct {v0, p0}, Lcom/google/k/c/dj;-><init>(Lcom/google/k/c/di;)V

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 1914
    iget-object v0, p0, Lcom/google/k/c/di;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1915
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1893
    iget-object v0, p0, Lcom/google/k/c/di;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1899
    iget-object v0, p0, Lcom/google/k/c/di;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1900
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/k/c/di;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/k/c/di;->b:Lcom/google/k/c/df;

    invoke-interface {v1, v0}, Lcom/google/k/c/df;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 1918
    iget-object v0, p0, Lcom/google/k/c/di;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1908
    iget-object v0, p0, Lcom/google/k/c/di;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/k/c/di;->b:Lcom/google/k/c/df;

    iget-object v1, p0, Lcom/google/k/c/di;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/k/c/df;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 1889
    iget-object v0, p0, Lcom/google/k/c/di;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method
