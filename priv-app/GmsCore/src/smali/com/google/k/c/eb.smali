.class public abstract Lcom/google/k/c/eb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/Comparator;)Lcom/google/k/c/eb;
    .locals 1

    .prologue
    .line 124
    instance-of v0, p0, Lcom/google/k/c/eb;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/google/k/c/eb;

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/google/k/c/aq;

    invoke-direct {v0, p0}, Lcom/google/k/c/aq;-><init>(Ljava/util/Comparator;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static b()Lcom/google/k/c/eb;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lcom/google/k/c/dz;->a:Lcom/google/k/c/dz;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/k/c/eb;
    .locals 1

    .prologue
    .line 333
    new-instance v0, Lcom/google/k/c/ek;

    invoke-direct {v0, p0}, Lcom/google/k/c/ek;-><init>(Lcom/google/k/c/eb;)V

    return-object v0
.end method

.method public final a(Lcom/google/k/a/u;)Lcom/google/k/c/eb;
    .locals 1

    .prologue
    .line 369
    new-instance v0, Lcom/google/k/c/an;

    invoke-direct {v0, p1, p0}, Lcom/google/k/c/an;-><init>(Lcom/google/k/a/u;Lcom/google/k/c/eb;)V

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 499
    invoke-virtual {p0, p1, p2}, Lcom/google/k/c/eb;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    move-object p1, p2

    goto :goto_0
.end method

.method public final a(Ljava/lang/Iterable;I)Ljava/util/List;
    .locals 11

    .prologue
    const/4 v5, 0x0

    .line 615
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 616
    check-cast v0, Ljava/util/Collection;

    .line 617
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    int-to-long v2, v1

    const-wide/16 v6, 0x2

    int-to-long v8, p2

    mul-long/2addr v6, v8

    cmp-long v1, v2, v6

    if-gtz v1, :cond_1

    .line 623
    invoke-interface {v0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 624
    invoke-static {v0, p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 625
    array-length v1, v0

    if-le v1, p2, :cond_0

    .line 626
    invoke-static {v0, p2}, Lcom/google/k/c/ea;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    .line 628
    :cond_0
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 631
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    invoke-static {v7}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "k"

    invoke-static {p2, v0}, Lcom/google/k/c/ao;->a(ILjava/lang/String;)I

    if-eqz p2, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    invoke-static {}, Lcom/google/k/c/bj;->f()Lcom/google/k/c/bj;

    move-result-object v0

    goto :goto_0

    :cond_3
    const v0, 0x3fffffff    # 1.9999999f

    if-lt p2, v0, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v7}, Lcom/google/k/c/cj;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    invoke-static {v0, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, p2, :cond_4

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, p2, v1}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    :cond_4
    invoke-virtual {v0}, Ljava/util/ArrayList;->trimToSize()V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_5
    mul-int/lit8 v8, p2, 0x2

    new-array v0, v8, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v0, v5

    const/4 v1, 0x1

    :goto_1
    if-ge v1, p2, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    add-int/lit8 v3, v1, 0x1

    aput-object v4, v0, v1

    invoke-virtual {p0, v2, v4}, Lcom/google/k/c/eb;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move v1, v3

    goto :goto_1

    :cond_6
    move v1, p2

    :cond_7
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v4, v2}, Lcom/google/k/c/eb;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    if-gez v3, :cond_7

    add-int/lit8 v3, v1, 0x1

    aput-object v4, v0, v1

    if-ne v3, v8, :cond_d

    add-int/lit8 v1, v8, -0x1

    move v4, v5

    move v6, v1

    move v3, v5

    :goto_3
    if-ge v3, v6, :cond_b

    add-int v1, v3, v6

    add-int/lit8 v1, v1, 0x1

    ushr-int/lit8 v1, v1, 0x1

    aget-object v9, v0, v1

    aget-object v2, v0, v6

    aput-object v2, v0, v1

    aput-object v9, v0, v6

    move v2, v3

    move v1, v3

    :goto_4
    if-ge v2, v6, :cond_9

    aget-object v10, v0, v2

    invoke-virtual {p0, v10, v9}, Lcom/google/k/c/eb;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v10

    if-gez v10, :cond_8

    invoke-static {v0, v1, v2}, Lcom/google/k/c/ea;->a([Ljava/lang/Object;II)V

    add-int/lit8 v1, v1, 0x1

    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_9
    invoke-static {v0, v6, v1}, Lcom/google/k/c/ea;->a([Ljava/lang/Object;II)V

    if-le v1, p2, :cond_a

    add-int/lit8 v1, v1, -0x1

    move v6, v1

    goto :goto_3

    :cond_a
    if-ge v1, p2, :cond_b

    add-int/lit8 v2, v3, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    move v4, v1

    goto :goto_3

    :cond_b
    aget-object v2, v0, v4

    add-int/lit8 v1, v4, 0x1

    :goto_5
    if-ge v1, p2, :cond_6

    aget-object v3, v0, v1

    invoke-virtual {p0, v2, v3}, Lcom/google/k/c/eb;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_c
    invoke-static {v0, v5, v1, p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/k/c/ea;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    :cond_d
    move v1, v3

    goto :goto_2
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 575
    invoke-virtual {p0, p1, p2}, Lcom/google/k/c/eb;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    move-object p1, p2

    goto :goto_0
.end method

.method public abstract compare(Ljava/lang/Object;Ljava/lang/Object;)I
.end method
