.class final Lcom/google/k/c/u;
.super Lcom/google/k/c/q;
.source "SourceFile"

# interfaces
.implements Ljava/util/Set;


# instance fields
.field final synthetic a:Lcom/google/k/c/e;


# direct methods
.method constructor <init>(Lcom/google/k/c/e;Ljava/lang/Object;Ljava/util/Set;)V
    .locals 1

    .prologue
    .line 602
    iput-object p1, p0, Lcom/google/k/c/u;->a:Lcom/google/k/c/e;

    .line 603
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/k/c/q;-><init>(Lcom/google/k/c/e;Ljava/lang/Object;Ljava/util/Collection;Lcom/google/k/c/q;)V

    .line 604
    return-void
.end method


# virtual methods
.method public final removeAll(Ljava/util/Collection;)Z
    .locals 4

    .prologue
    .line 608
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 609
    const/4 v0, 0x0

    .line 622
    :cond_0
    :goto_0
    return v0

    .line 611
    :cond_1
    invoke-virtual {p0}, Lcom/google/k/c/u;->size()I

    move-result v1

    .line 616
    iget-object v0, p0, Lcom/google/k/c/u;->c:Ljava/util/Collection;

    check-cast v0, Ljava/util/Set;

    invoke-static {v0, p1}, Lcom/google/k/c/em;->a(Ljava/util/Set;Ljava/util/Collection;)Z

    move-result v0

    .line 617
    if-eqz v0, :cond_0

    .line 618
    iget-object v2, p0, Lcom/google/k/c/u;->c:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    .line 619
    iget-object v3, p0, Lcom/google/k/c/u;->a:Lcom/google/k/c/e;

    sub-int v1, v2, v1

    invoke-static {v3, v1}, Lcom/google/k/c/e;->a(Lcom/google/k/c/e;I)I

    .line 620
    invoke-virtual {p0}, Lcom/google/k/c/u;->b()V

    goto :goto_0
.end method
