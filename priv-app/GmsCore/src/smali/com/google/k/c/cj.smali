.class public final Lcom/google/k/c/cj;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lcom/google/k/c/fm;

.field private static final b:Ljava/util/Iterator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/google/k/c/ck;

    invoke-direct {v0}, Lcom/google/k/c/ck;-><init>()V

    sput-object v0, Lcom/google/k/c/cj;->a:Lcom/google/k/c/fm;

    .line 127
    new-instance v0, Lcom/google/k/c/cn;

    invoke-direct {v0}, Lcom/google/k/c/cn;-><init>()V

    sput-object v0, Lcom/google/k/c/cj;->b:Ljava/util/Iterator;

    return-void
.end method

.method public static a(Ljava/util/Iterator;)I
    .locals 2

    .prologue
    .line 190
    const/4 v0, 0x0

    .line 191
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 192
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 193
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 195
    :cond_0
    return v0
.end method

.method public static a()Lcom/google/k/c/fl;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 112
    sget-object v0, Lcom/google/k/c/cj;->a:Lcom/google/k/c/fm;

    return-object v0
.end method

.method public static a(Ljava/lang/Object;)Lcom/google/k/c/fl;
    .locals 1

    .prologue
    .line 1073
    new-instance v0, Lcom/google/k/c/cm;

    invoke-direct {v0, p0}, Lcom/google/k/c/cm;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/util/Iterator;Lcom/google/k/a/ai;)Lcom/google/k/c/fl;
    .locals 1

    .prologue
    .line 646
    invoke-static {p0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 647
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 648
    new-instance v0, Lcom/google/k/c/co;

    invoke-direct {v0, p0, p1}, Lcom/google/k/c/co;-><init>(Ljava/util/Iterator;Lcom/google/k/a/ai;)V

    return-object v0
.end method

.method public static varargs a([Ljava/lang/Object;)Lcom/google/k/c/fl;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1031
    array-length v0, p0

    invoke-static {p0, v1, v0, v1}, Lcom/google/k/c/cj;->a([Ljava/lang/Object;III)Lcom/google/k/c/fm;

    move-result-object v0

    return-object v0
.end method

.method static a([Ljava/lang/Object;III)Lcom/google/k/c/fm;
    .locals 2

    .prologue
    .line 1043
    if-ltz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/k/a/ah;->a(Z)V

    .line 1044
    add-int v0, p1, p2

    .line 1047
    array-length v1, p0

    invoke-static {p1, v0, v1}, Lcom/google/k/a/ah;->a(III)V

    .line 1048
    invoke-static {p3, p2}, Lcom/google/k/a/ah;->b(II)I

    .line 1049
    if-nez p2, :cond_1

    .line 1050
    sget-object v0, Lcom/google/k/c/cj;->a:Lcom/google/k/c/fm;

    .line 1058
    :goto_1
    return-object v0

    .line 1043
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1058
    :cond_1
    new-instance v0, Lcom/google/k/c/cl;

    invoke-direct {v0, p2, p3, p0, p1}, Lcom/google/k/c/cl;-><init>(II[Ljava/lang/Object;I)V

    goto :goto_1
.end method

.method public static a(Ljava/util/Iterator;Lcom/google/k/a/u;)Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 795
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 796
    new-instance v0, Lcom/google/k/c/cp;

    invoke-direct {v0, p0, p1}, Lcom/google/k/c/cp;-><init>(Ljava/util/Iterator;Lcom/google/k/a/u;)V

    return-object v0
.end method

.method public static a(Ljava/util/Collection;Ljava/util/Iterator;)Z
    .locals 2

    .prologue
    .line 358
    invoke-static {p0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    const/4 v0, 0x0

    .line 361
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 362
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v1

    or-int/2addr v0, v1

    goto :goto_0

    .line 364
    :cond_0
    return v0
.end method

.method public static a(Ljava/util/Iterator;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 202
    invoke-static {p1}, Lcom/google/k/a/aj;->a(Ljava/lang/Object;)Lcom/google/k/a/ai;

    move-result-object v3

    const-string v0, "predicate"

    invoke-static {v3, v0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/k/a/ai;->a(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_1
    if-eq v0, v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public static a(Ljava/util/Iterator;Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 216
    invoke-static {p1}, Lcom/google/k/a/aj;->a(Ljava/util/Collection;)Lcom/google/k/a/ai;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/k/c/cj;->c(Ljava/util/Iterator;Lcom/google/k/a/ai;)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/util/Iterator;Ljava/util/Iterator;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 269
    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 270
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 279
    :cond_1
    :goto_0
    return v0

    .line 273
    :cond_2
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 274
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 275
    invoke-static {v1, v2}, Lcom/google/k/a/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 279
    :cond_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Ljava/util/Iterator;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 288
    sget-object v0, Lcom/google/k/c/ap;->a:Lcom/google/k/a/x;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, p0}, Lcom/google/k/a/x;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static b()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 151
    sget-object v0, Lcom/google/k/c/cj;->b:Ljava/util/Iterator;

    return-object v0
.end method

.method public static b(Ljava/util/Iterator;Lcom/google/k/a/ai;)Z
    .locals 1

    .prologue
    .line 694
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 695
    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 696
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 697
    invoke-interface {p1, v0}, Lcom/google/k/a/ai;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 698
    const/4 v0, 0x0

    .line 701
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Ljava/util/Iterator;Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 254
    invoke-static {p1}, Lcom/google/k/a/aj;->a(Ljava/util/Collection;)Lcom/google/k/a/ai;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/a/aj;->a(Lcom/google/k/a/ai;)Lcom/google/k/a/ai;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/k/c/cj;->c(Ljava/util/Iterator;Lcom/google/k/a/ai;)Z

    move-result v0

    return v0
.end method

.method public static c(Ljava/util/Iterator;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 302
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 303
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 304
    return-object v0

    .line 307
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 308
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "expected one element but was: <"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 310
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 312
    :cond_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 313
    const-string v0, ", ..."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    :cond_2
    const/16 v0, 0x3e

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 317
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static c(Ljava/util/Iterator;Lcom/google/k/a/ai;)Z
    .locals 2

    .prologue
    .line 232
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    const/4 v0, 0x0

    .line 234
    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 235
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/google/k/a/ai;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 236
    invoke-interface {p0}, Ljava/util/Iterator;->remove()V

    .line 237
    const/4 v0, 0x1

    goto :goto_0

    .line 240
    :cond_1
    return v0
.end method

.method static d(Ljava/util/Iterator;)V
    .locals 1

    .prologue
    .line 1010
    invoke-static {p0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1012
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 1013
    invoke-interface {p0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 1015
    :cond_0
    return-void
.end method

.method public static e(Ljava/util/Iterator;)Lcom/google/k/c/ec;
    .locals 1

    .prologue
    .line 1219
    instance-of v0, p0, Lcom/google/k/c/cq;

    if-eqz v0, :cond_0

    .line 1223
    check-cast p0, Lcom/google/k/c/cq;

    .line 1226
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/google/k/c/cq;

    invoke-direct {v0, p0}, Lcom/google/k/c/cq;-><init>(Ljava/util/Iterator;)V

    move-object p0, v0

    goto :goto_0
.end method
