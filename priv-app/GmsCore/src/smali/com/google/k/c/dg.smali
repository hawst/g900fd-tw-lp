.class abstract Lcom/google/k/c/dg;
.super Ljava/util/AbstractMap;
.source "SourceFile"


# instance fields
.field private transient a:Ljava/util/Set;

.field private transient b:Ljava/util/Set;

.field private transient c:Ljava/util/Collection;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 3308
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    return-void
.end method


# virtual methods
.method abstract a()Ljava/util/Set;
.end method

.method b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 3331
    new-instance v0, Lcom/google/k/c/dh;

    invoke-direct {v0, p0}, Lcom/google/k/c/dh;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 3319
    iget-object v0, p0, Lcom/google/k/c/dg;->a:Ljava/util/Set;

    .line 3320
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/k/c/dg;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/dg;->a:Ljava/util/Set;

    :cond_0
    return-object v0
.end method

.method public keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 3326
    iget-object v0, p0, Lcom/google/k/c/dg;->b:Ljava/util/Set;

    .line 3327
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/k/c/dg;->b()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/dg;->b:Ljava/util/Set;

    :cond_0
    return-object v0
.end method

.method public values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 3337
    iget-object v0, p0, Lcom/google/k/c/dg;->c:Ljava/util/Collection;

    .line 3338
    if-nez v0, :cond_0

    new-instance v0, Lcom/google/k/c/dm;

    invoke-direct {v0, p0}, Lcom/google/k/c/dm;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/google/k/c/dg;->c:Ljava/util/Collection;

    :cond_0
    return-object v0
.end method
