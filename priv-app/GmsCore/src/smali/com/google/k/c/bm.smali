.class final Lcom/google/k/c/bm;
.super Lcom/google/k/c/bj;
.source "SourceFile"


# instance fields
.field private final transient b:Lcom/google/k/c/bj;


# direct methods
.method constructor <init>(Lcom/google/k/c/bj;)V
    .locals 0

    .prologue
    .line 516
    invoke-direct {p0}, Lcom/google/k/c/bj;-><init>()V

    .line 517
    iput-object p1, p0, Lcom/google/k/c/bm;->b:Lcom/google/k/c/bj;

    .line 518
    return-void
.end method

.method private b(I)I
    .locals 1

    .prologue
    .line 521
    invoke-virtual {p0}, Lcom/google/k/c/bm;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p1

    return v0
.end method


# virtual methods
.method public final a(II)Lcom/google/k/c/bj;
    .locals 3

    .prologue
    .line 547
    invoke-virtual {p0}, Lcom/google/k/c/bm;->size()I

    move-result v0

    invoke-static {p1, p2, v0}, Lcom/google/k/a/ah;->a(III)V

    .line 548
    iget-object v0, p0, Lcom/google/k/c/bm;->b:Lcom/google/k/c/bj;

    invoke-virtual {p0}, Lcom/google/k/c/bm;->size()I

    move-result v1

    sub-int/2addr v1, p2

    invoke-virtual {p0}, Lcom/google/k/c/bm;->size()I

    move-result v2

    sub-int/2addr v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bj;->a(II)Lcom/google/k/c/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/bj;->g()Lcom/google/k/c/bj;

    move-result-object v0

    return-object v0
.end method

.method final b()Z
    .locals 1

    .prologue
    .line 562
    iget-object v0, p0, Lcom/google/k/c/bm;->b:Lcom/google/k/c/bj;

    invoke-virtual {v0}, Lcom/google/k/c/bj;->b()Z

    move-result v0

    return v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 533
    iget-object v0, p0, Lcom/google/k/c/bm;->b:Lcom/google/k/c/bj;

    invoke-virtual {v0, p1}, Lcom/google/k/c/bj;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final g()Lcom/google/k/c/bj;
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/google/k/c/bm;->b:Lcom/google/k/c/bj;

    return-object v0
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 553
    invoke-virtual {p0}, Lcom/google/k/c/bm;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/google/k/a/ah;->a(II)I

    .line 554
    iget-object v0, p0, Lcom/google/k/c/bm;->b:Lcom/google/k/c/bj;

    invoke-direct {p0, p1}, Lcom/google/k/c/bm;->b(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/k/c/bj;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final indexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 537
    iget-object v0, p0, Lcom/google/k/c/bm;->b:Lcom/google/k/c/bj;

    invoke-virtual {v0, p1}, Lcom/google/k/c/bj;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    .line 538
    if-ltz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/k/c/bm;->b(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 513
    invoke-super {p0}, Lcom/google/k/c/bj;->a()Lcom/google/k/c/fl;

    move-result-object v0

    return-object v0
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 542
    iget-object v0, p0, Lcom/google/k/c/bm;->b:Lcom/google/k/c/bj;

    invoke-virtual {v0, p1}, Lcom/google/k/c/bj;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 543
    if-ltz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/k/c/bm;->b(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final synthetic listIterator()Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 513
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/k/c/bj;->a(I)Lcom/google/k/c/fm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 513
    invoke-super {p0, p1}, Lcom/google/k/c/bj;->a(I)Lcom/google/k/c/fm;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Lcom/google/k/c/bm;->b:Lcom/google/k/c/bj;

    invoke-virtual {v0}, Lcom/google/k/c/bj;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic subList(II)Ljava/util/List;
    .locals 1

    .prologue
    .line 513
    invoke-virtual {p0, p1, p2}, Lcom/google/k/c/bm;->a(II)Lcom/google/k/c/bj;

    move-result-object v0

    return-object v0
.end method
