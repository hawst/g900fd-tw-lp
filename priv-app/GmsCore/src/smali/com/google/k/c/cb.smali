.class abstract Lcom/google/k/c/cb;
.super Lcom/google/k/c/fl;
.source "SourceFile"


# instance fields
.field final b:Ljava/util/Iterator;

.field c:Ljava/lang/Object;

.field d:Ljava/util/Iterator;

.field final synthetic e:Lcom/google/k/c/bx;


# direct methods
.method private constructor <init>(Lcom/google/k/c/bx;)V
    .locals 1

    .prologue
    .line 532
    iput-object p1, p0, Lcom/google/k/c/cb;->e:Lcom/google/k/c/bx;

    invoke-direct {p0}, Lcom/google/k/c/fl;-><init>()V

    .line 533
    iget-object v0, p0, Lcom/google/k/c/cb;->e:Lcom/google/k/c/bx;

    invoke-virtual {v0}, Lcom/google/k/c/bx;->a()Lcom/google/k/c/bo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/bo;->b()Lcom/google/k/c/cd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/cd;->a()Lcom/google/k/c/fl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/cb;->b:Ljava/util/Iterator;

    .line 534
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/k/c/cb;->c:Ljava/lang/Object;

    .line 535
    invoke-static {}, Lcom/google/k/c/cj;->a()Lcom/google/k/c/fl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/cb;->d:Ljava/util/Iterator;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/k/c/bx;B)V
    .locals 0

    .prologue
    .line 532
    invoke-direct {p0, p1}, Lcom/google/k/c/cb;-><init>(Lcom/google/k/c/bx;)V

    return-void
.end method


# virtual methods
.method abstract a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Lcom/google/k/c/cb;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/k/c/cb;->d:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 546
    iget-object v0, p0, Lcom/google/k/c/cb;->d:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 547
    iget-object v0, p0, Lcom/google/k/c/cb;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 548
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lcom/google/k/c/cb;->c:Ljava/lang/Object;

    .line 549
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/cb;->d:Ljava/util/Iterator;

    .line 551
    :cond_0
    iget-object v0, p0, Lcom/google/k/c/cb;->c:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/k/c/cb;->d:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/k/c/cb;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
