.class final Lcom/google/k/c/dy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private final a:Lcom/google/k/c/dq;

.field private final b:Ljava/util/Iterator;

.field private c:Lcom/google/k/c/dr;

.field private d:I

.field private e:I

.field private f:Z


# direct methods
.method constructor <init>(Lcom/google/k/c/dq;Ljava/util/Iterator;)V
    .locals 0

    .prologue
    .line 1025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1026
    iput-object p1, p0, Lcom/google/k/c/dy;->a:Lcom/google/k/c/dq;

    .line 1027
    iput-object p2, p0, Lcom/google/k/c/dy;->b:Ljava/util/Iterator;

    .line 1028
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 1032
    iget v0, p0, Lcom/google/k/c/dy;->d:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/k/c/dy;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1037
    invoke-virtual {p0}, Lcom/google/k/c/dy;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1038
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 1040
    :cond_0
    iget v0, p0, Lcom/google/k/c/dy;->d:I

    if-nez v0, :cond_1

    .line 1041
    iget-object v0, p0, Lcom/google/k/c/dy;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/c/dr;

    iput-object v0, p0, Lcom/google/k/c/dy;->c:Lcom/google/k/c/dr;

    .line 1042
    iget-object v0, p0, Lcom/google/k/c/dy;->c:Lcom/google/k/c/dr;

    invoke-interface {v0}, Lcom/google/k/c/dr;->b()I

    move-result v0

    iput v0, p0, Lcom/google/k/c/dy;->d:I

    iput v0, p0, Lcom/google/k/c/dy;->e:I

    .line 1044
    :cond_1
    iget v0, p0, Lcom/google/k/c/dy;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/k/c/dy;->d:I

    .line 1045
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/k/c/dy;->f:Z

    .line 1046
    iget-object v0, p0, Lcom/google/k/c/dy;->c:Lcom/google/k/c/dr;

    invoke-interface {v0}, Lcom/google/k/c/dr;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 1051
    iget-boolean v0, p0, Lcom/google/k/c/dy;->f:Z

    const-string v1, "no calls to next() since the last call to remove()"

    invoke-static {v0, v1}, Lcom/google/k/a/ah;->b(ZLjava/lang/Object;)V

    .line 1052
    iget v0, p0, Lcom/google/k/c/dy;->e:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1053
    iget-object v0, p0, Lcom/google/k/c/dy;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 1057
    :goto_0
    iget v0, p0, Lcom/google/k/c/dy;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/k/c/dy;->e:I

    .line 1058
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/k/c/dy;->f:Z

    .line 1059
    return-void

    .line 1055
    :cond_0
    iget-object v0, p0, Lcom/google/k/c/dy;->a:Lcom/google/k/c/dq;

    iget-object v1, p0, Lcom/google/k/c/dy;->c:Lcom/google/k/c/dr;

    invoke-interface {v1}, Lcom/google/k/c/dr;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/k/c/dq;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
