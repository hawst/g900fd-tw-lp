.class final Lcom/google/k/c/bt;
.super Lcom/google/k/c/cd;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/k/c/bo;


# direct methods
.method constructor <init>(Lcom/google/k/c/bo;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/k/c/cd;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/k/c/bt;->a:Lcom/google/k/c/bo;

    .line 39
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/k/c/fl;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/k/c/bt;->c()Lcom/google/k/c/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/bj;->a()Lcom/google/k/c/fl;

    move-result-object v0

    return-object v0
.end method

.method final b()Z
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x1

    return v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/k/c/bt;->a:Lcom/google/k/c/bo;

    invoke-virtual {v0, p1}, Lcom/google/k/c/bo;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final e()Lcom/google/k/c/bj;
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/k/c/bt;->a:Lcom/google/k/c/bo;

    invoke-virtual {v0}, Lcom/google/k/c/bo;->b()Lcom/google/k/c/cd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/cd;->c()Lcom/google/k/c/bj;

    move-result-object v0

    .line 59
    new-instance v1, Lcom/google/k/c/bu;

    invoke-direct {v1, p0, v0}, Lcom/google/k/c/bu;-><init>(Lcom/google/k/c/bt;Lcom/google/k/c/bj;)V

    return-object v1
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/k/c/bt;->c()Lcom/google/k/c/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/bj;->a()Lcom/google/k/c/fl;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/k/c/bt;->a:Lcom/google/k/c/bo;

    invoke-virtual {v0}, Lcom/google/k/c/bo;->size()I

    move-result v0

    return v0
.end method
