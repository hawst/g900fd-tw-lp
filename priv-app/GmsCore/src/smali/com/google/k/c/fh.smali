.class final Lcom/google/k/c/fh;
.super Lcom/google/k/c/fd;
.source "SourceFile"

# interfaces
.implements Lcom/google/k/c/el;


# instance fields
.field transient e:Ljava/util/Set;


# direct methods
.method constructor <init>(Lcom/google/k/c/el;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 742
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/k/c/fd;-><init>(Lcom/google/k/c/dn;Ljava/lang/Object;)V

    .line 743
    return-void
.end method


# virtual methods
.method final bridge synthetic a()Lcom/google/k/c/dn;
    .locals 1

    .prologue
    .line 736
    invoke-super {p0}, Lcom/google/k/c/fd;->a()Lcom/google/k/c/dn;

    move-result-object v0

    check-cast v0, Lcom/google/k/c/el;

    return-object v0
.end method

.method final synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 736
    invoke-super {p0}, Lcom/google/k/c/fd;->a()Lcom/google/k/c/dn;

    move-result-object v0

    check-cast v0, Lcom/google/k/c/el;

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 736
    invoke-virtual {p0, p1}, Lcom/google/k/c/fh;->e(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/Object;)Ljava/util/Set;
    .locals 3

    .prologue
    .line 748
    iget-object v1, p0, Lcom/google/k/c/fh;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 749
    :try_start_0
    invoke-super {p0}, Lcom/google/k/c/fd;->a()Lcom/google/k/c/dn;

    move-result-object v0

    check-cast v0, Lcom/google/k/c/el;

    invoke-interface {v0, p1}, Lcom/google/k/c/el;->e(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lcom/google/k/c/fh;->g:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/k/c/et;->a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 750
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final synthetic h()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 736
    invoke-virtual {p0}, Lcom/google/k/c/fh;->q()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/Set;
    .locals 3

    .prologue
    .line 764
    iget-object v1, p0, Lcom/google/k/c/fh;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 765
    :try_start_0
    iget-object v0, p0, Lcom/google/k/c/fh;->e:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 766
    invoke-super {p0}, Lcom/google/k/c/fd;->a()Lcom/google/k/c/dn;

    move-result-object v0

    check-cast v0, Lcom/google/k/c/el;

    invoke-interface {v0}, Lcom/google/k/c/el;->q()Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lcom/google/k/c/fh;->g:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/k/c/et;->a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/fh;->e:Ljava/util/Set;

    .line 768
    :cond_0
    iget-object v0, p0, Lcom/google/k/c/fh;->e:Ljava/util/Set;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 769
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
