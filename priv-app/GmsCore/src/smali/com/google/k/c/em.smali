.class public final Lcom/google/k/c/em;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(Ljava/util/Set;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1340
    .line 1341
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v0, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1342
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :goto_1
    add-int/2addr v0, v2

    .line 1344
    xor-int/lit8 v0, v0, -0x1

    xor-int/lit8 v0, v0, -0x1

    .line 1346
    goto :goto_0

    :cond_0
    move v2, v1

    .line 1342
    goto :goto_1

    .line 1347
    :cond_1
    return v0
.end method

.method public static a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/k/c/ep;
    .locals 2

    .prologue
    .line 677
    const-string v0, "set1"

    invoke-static {p0, v0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 678
    const-string v0, "set2"

    invoke-static {p1, v0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 680
    invoke-static {p1}, Lcom/google/k/a/aj;->a(Ljava/util/Collection;)Lcom/google/k/a/ai;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/a/aj;->a(Lcom/google/k/a/ai;)Lcom/google/k/a/ai;

    move-result-object v0

    .line 681
    new-instance v1, Lcom/google/k/c/en;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/k/c/en;-><init>(Ljava/util/Set;Lcom/google/k/a/ai;Ljava/util/Set;)V

    return-object v1
.end method

.method public static a(I)Ljava/util/HashSet;
    .locals 2

    .prologue
    .line 201
    new-instance v0, Ljava/util/HashSet;

    invoke-static {p0}, Lcom/google/k/c/cv;->a(I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    return-object v0
.end method

.method public static varargs a([Ljava/lang/Object;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 182
    array-length v0, p0

    invoke-static {v0}, Lcom/google/k/c/em;->a(I)Ljava/util/HashSet;

    move-result-object v0

    .line 183
    invoke-static {v0, p0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 184
    return-object v0
.end method

.method static a(Ljava/util/Set;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1354
    if-ne p0, p1, :cond_1

    .line 1368
    :cond_0
    :goto_0
    return v0

    .line 1357
    :cond_1
    instance-of v2, p1, Ljava/util/Set;

    if-eqz v2, :cond_3

    .line 1358
    check-cast p1, Ljava/util/Set;

    .line 1361
    :try_start_0
    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v2

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    if-ne v2, v3, :cond_2

    invoke-interface {p0, p1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1363
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 1365
    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1368
    goto :goto_0
.end method

.method static a(Ljava/util/Set;Ljava/util/Collection;)Z
    .locals 2

    .prologue
    .line 1547
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1548
    instance-of v0, p1, Lcom/google/k/c/dq;

    if-eqz v0, :cond_0

    .line 1549
    check-cast p1, Lcom/google/k/c/dq;

    invoke-interface {p1}, Lcom/google/k/c/dq;->d()Ljava/util/Set;

    move-result-object p1

    .line 1558
    :cond_0
    instance-of v0, p1, Ljava/util/Set;

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 1559
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/k/c/cj;->a(Ljava/util/Iterator;Ljava/util/Collection;)Z

    move-result v0

    .line 1561
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/k/c/em;->a(Ljava/util/Set;Ljava/util/Iterator;)Z

    move-result v0

    goto :goto_0
.end method

.method static a(Ljava/util/Set;Ljava/util/Iterator;)Z
    .locals 2

    .prologue
    .line 1539
    const/4 v0, 0x0

    .line 1540
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1541
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    or-int/2addr v0, v1

    goto :goto_0

    .line 1543
    :cond_0
    return v0
.end method
