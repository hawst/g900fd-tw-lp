.class Lcom/google/k/c/h;
.super Lcom/google/k/c/dg;
.source "SourceFile"


# instance fields
.field final transient a:Ljava/util/Map;

.field final synthetic b:Lcom/google/k/c/e;


# direct methods
.method constructor <init>(Lcom/google/k/c/e;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 1237
    iput-object p1, p0, Lcom/google/k/c/h;->b:Lcom/google/k/c/e;

    invoke-direct {p0}, Lcom/google/k/c/dg;-><init>()V

    .line 1238
    iput-object p2, p0, Lcom/google/k/c/h;->a:Ljava/util/Map;

    .line 1239
    return-void
.end method


# virtual methods
.method final a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;
    .locals 3

    .prologue
    .line 1306
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 1307
    iget-object v2, p0, Lcom/google/k/c/h;->b:Lcom/google/k/c/e;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {v2, v1, v0}, Lcom/google/k/c/e;->a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/k/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method protected final a()Ljava/util/Set;
    .locals 1

    .prologue
    .line 1243
    new-instance v0, Lcom/google/k/c/i;

    invoke-direct {v0, p0}, Lcom/google/k/c/i;-><init>(Lcom/google/k/c/h;)V

    return-object v0
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 1298
    iget-object v0, p0, Lcom/google/k/c/h;->a:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/k/c/h;->b:Lcom/google/k/c/e;

    invoke-static {v1}, Lcom/google/k/c/e;->a(Lcom/google/k/c/e;)Ljava/util/Map;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 1299
    iget-object v0, p0, Lcom/google/k/c/h;->b:Lcom/google/k/c/e;

    invoke-virtual {v0}, Lcom/google/k/c/e;->d()V

    .line 1303
    :goto_0
    return-void

    .line 1301
    :cond_0
    new-instance v0, Lcom/google/k/c/j;

    invoke-direct {v0, p0}, Lcom/google/k/c/j;-><init>(Lcom/google/k/c/h;)V

    invoke-static {v0}, Lcom/google/k/c/cj;->d(Ljava/util/Iterator;)V

    goto :goto_0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1249
    iget-object v0, p0, Lcom/google/k/c/h;->a:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/k/c/cv;->b(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1285
    if-eq p0, p1, :cond_0

    iget-object v0, p0, Lcom/google/k/c/h;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1230
    iget-object v0, p0, Lcom/google/k/c/h;->a:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/k/c/cv;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/k/c/h;->b:Lcom/google/k/c/e;

    invoke-virtual {v1, p1, v0}, Lcom/google/k/c/e;->a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1289
    iget-object v0, p0, Lcom/google/k/c/h;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    return v0
.end method

.method public keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 1263
    iget-object v0, p0, Lcom/google/k/c/h;->b:Lcom/google/k/c/e;

    invoke-virtual {v0}, Lcom/google/k/c/e;->m()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1230
    iget-object v0, p0, Lcom/google/k/c/h;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/k/c/h;->b:Lcom/google/k/c/e;

    invoke-virtual {v1}, Lcom/google/k/c/e;->b()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    iget-object v2, p0, Lcom/google/k/c/h;->b:Lcom/google/k/c/e;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/k/c/e;->b(Lcom/google/k/c/e;I)I

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    move-object v0, v1

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 1268
    iget-object v0, p0, Lcom/google/k/c/h;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1293
    iget-object v0, p0, Lcom/google/k/c/h;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
