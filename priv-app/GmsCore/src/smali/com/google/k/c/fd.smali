.class Lcom/google/k/c/fd;
.super Lcom/google/k/c/fe;
.source "SourceFile"

# interfaces
.implements Lcom/google/k/c/dn;


# instance fields
.field transient a:Ljava/util/Set;

.field transient b:Ljava/util/Collection;

.field transient c:Ljava/util/Collection;

.field transient d:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/google/k/c/dn;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 528
    invoke-direct {p0, p1, p2}, Lcom/google/k/c/fe;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 529
    return-void
.end method


# virtual methods
.method a()Lcom/google/k/c/dn;
    .locals 1

    .prologue
    .line 524
    invoke-super {p0}, Lcom/google/k/c/fe;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/c/dn;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 547
    iget-object v1, p0, Lcom/google/k/c/fd;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 548
    :try_start_0
    invoke-virtual {p0}, Lcom/google/k/c/fd;->a()Lcom/google/k/c/dn;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/k/c/dn;->a(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 549
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 575
    iget-object v1, p0, Lcom/google/k/c/fd;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 576
    :try_start_0
    invoke-virtual {p0}, Lcom/google/k/c/fd;->a()Lcom/google/k/c/dn;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/k/c/dn;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 577
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 514
    invoke-virtual {p0}, Lcom/google/k/c/fd;->a()Lcom/google/k/c/dn;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 561
    iget-object v1, p0, Lcom/google/k/c/fd;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 562
    :try_start_0
    invoke-virtual {p0}, Lcom/google/k/c/fd;->a()Lcom/google/k/c/dn;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/k/c/dn;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 563
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 533
    iget-object v1, p0, Lcom/google/k/c/fd;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 534
    :try_start_0
    invoke-virtual {p0}, Lcom/google/k/c/fd;->a()Lcom/google/k/c/dn;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/k/c/dn;->c()I

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 535
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 3

    .prologue
    .line 568
    iget-object v1, p0, Lcom/google/k/c/fd;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 569
    :try_start_0
    invoke-virtual {p0}, Lcom/google/k/c/fd;->a()Lcom/google/k/c/dn;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/k/c/dn;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, Lcom/google/k/c/fd;->g:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/k/c/et;->b(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 570
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 603
    iget-object v1, p0, Lcom/google/k/c/fd;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 604
    :try_start_0
    invoke-virtual {p0}, Lcom/google/k/c/fd;->a()Lcom/google/k/c/dn;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/k/c/dn;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 605
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 617
    iget-object v1, p0, Lcom/google/k/c/fd;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 618
    :try_start_0
    invoke-virtual {p0}, Lcom/google/k/c/fd;->a()Lcom/google/k/c/dn;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/k/c/dn;->d()V

    .line 619
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 673
    if-ne p1, p0, :cond_0

    .line 674
    const/4 v0, 0x1

    .line 677
    :goto_0
    return v0

    .line 676
    :cond_0
    iget-object v1, p0, Lcom/google/k/c/fd;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 677
    :try_start_0
    invoke-virtual {p0}, Lcom/google/k/c/fd;->a()Lcom/google/k/c/dn;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/k/c/dn;->equals(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 678
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f()Ljava/util/Collection;
    .locals 3

    .prologue
    .line 634
    iget-object v1, p0, Lcom/google/k/c/fd;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 635
    :try_start_0
    iget-object v0, p0, Lcom/google/k/c/fd;->b:Ljava/util/Collection;

    if-nez v0, :cond_0

    .line 636
    invoke-virtual {p0}, Lcom/google/k/c/fd;->a()Lcom/google/k/c/dn;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/k/c/dn;->f()Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, Lcom/google/k/c/fd;->g:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/k/c/et;->a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/fd;->b:Ljava/util/Collection;

    .line 638
    :cond_0
    iget-object v0, p0, Lcom/google/k/c/fd;->b:Ljava/util/Collection;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 639
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public h()Ljava/util/Collection;
    .locals 3

    .prologue
    .line 644
    iget-object v1, p0, Lcom/google/k/c/fd;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 645
    :try_start_0
    iget-object v0, p0, Lcom/google/k/c/fd;->c:Ljava/util/Collection;

    if-nez v0, :cond_0

    .line 646
    invoke-virtual {p0}, Lcom/google/k/c/fd;->a()Lcom/google/k/c/dn;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/k/c/dn;->h()Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, Lcom/google/k/c/fd;->g:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/k/c/et;->b(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/fd;->c:Ljava/util/Collection;

    .line 648
    :cond_0
    iget-object v0, p0, Lcom/google/k/c/fd;->c:Ljava/util/Collection;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 649
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 682
    iget-object v1, p0, Lcom/google/k/c/fd;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 683
    :try_start_0
    invoke-virtual {p0}, Lcom/google/k/c/fd;->a()Lcom/google/k/c/dn;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/k/c/dn;->hashCode()I

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 684
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 540
    iget-object v1, p0, Lcom/google/k/c/fd;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 541
    :try_start_0
    invoke-virtual {p0}, Lcom/google/k/c/fd;->a()Lcom/google/k/c/dn;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/k/c/dn;->k()Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 542
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final m()Ljava/util/Set;
    .locals 4

    .prologue
    .line 624
    iget-object v1, p0, Lcom/google/k/c/fd;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 625
    :try_start_0
    iget-object v0, p0, Lcom/google/k/c/fd;->a:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 626
    invoke-virtual {p0}, Lcom/google/k/c/fd;->a()Lcom/google/k/c/dn;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/k/c/dn;->m()Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lcom/google/k/c/fd;->g:Ljava/lang/Object;

    instance-of v3, v0, Ljava/util/SortedSet;

    if-eqz v3, :cond_1

    check-cast v0, Ljava/util/SortedSet;

    invoke-static {v0, v2}, Lcom/google/k/c/et;->a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/k/c/fd;->a:Ljava/util/Set;

    .line 628
    :cond_0
    iget-object v0, p0, Lcom/google/k/c/fd;->a:Ljava/util/Set;

    monitor-exit v1

    return-object v0

    .line 626
    :cond_1
    invoke-static {v0, v2}, Lcom/google/k/c/et;->a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 629
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final o()Ljava/util/Map;
    .locals 4

    .prologue
    .line 654
    iget-object v1, p0, Lcom/google/k/c/fd;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 655
    :try_start_0
    iget-object v0, p0, Lcom/google/k/c/fd;->d:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 656
    new-instance v0, Lcom/google/k/c/eu;

    invoke-virtual {p0}, Lcom/google/k/c/fd;->a()Lcom/google/k/c/dn;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/k/c/dn;->o()Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/google/k/c/fd;->g:Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/google/k/c/eu;-><init>(Ljava/util/Map;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/k/c/fd;->d:Ljava/util/Map;

    .line 658
    :cond_0
    iget-object v0, p0, Lcom/google/k/c/fd;->d:Ljava/util/Map;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 659
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
