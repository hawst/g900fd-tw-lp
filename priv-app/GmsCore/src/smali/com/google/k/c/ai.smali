.class abstract Lcom/google/k/c/ai;
.super Lcom/google/k/c/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/k/c/el;


# direct methods
.method protected constructor <init>(Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/k/c/e;-><init>(Ljava/util/Map;)V

    .line 45
    return-void
.end method


# virtual methods
.method final synthetic a()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Lcom/google/k/c/cd;->g()Lcom/google/k/c/cd;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 121
    invoke-super {p0, p1, p2}, Lcom/google/k/c/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method synthetic b()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/google/k/c/ai;->p()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/google/k/c/ai;->f(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/google/k/c/ai;->e(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/Object;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/google/k/c/e;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 132
    invoke-super {p0, p1}, Lcom/google/k/c/e;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public f(Ljava/lang/Object;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/google/k/c/e;->b(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public final synthetic h()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/google/k/c/ai;->q()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public o()Ljava/util/Map;
    .locals 1

    .prologue
    .line 109
    invoke-super {p0}, Lcom/google/k/c/e;->o()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method abstract p()Ljava/util/Set;
.end method

.method public q()Ljava/util/Set;
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Lcom/google/k/c/e;->h()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
