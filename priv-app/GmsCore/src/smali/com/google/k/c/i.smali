.class final Lcom/google/k/c/i;
.super Lcom/google/k/c/de;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/k/c/h;


# direct methods
.method constructor <init>(Lcom/google/k/c/h;)V
    .locals 0

    .prologue
    .line 1310
    iput-object p1, p0, Lcom/google/k/c/i;->a:Lcom/google/k/c/h;

    invoke-direct {p0}, Lcom/google/k/c/de;-><init>()V

    return-void
.end method


# virtual methods
.method final a()Ljava/util/Map;
    .locals 1

    .prologue
    .line 1313
    iget-object v0, p0, Lcom/google/k/c/i;->a:Lcom/google/k/c/h;

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1323
    iget-object v0, p0, Lcom/google/k/c/i;->a:Lcom/google/k/c/h;

    iget-object v0, v0, Lcom/google/k/c/h;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/k/c/ap;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 1317
    new-instance v0, Lcom/google/k/c/j;

    iget-object v1, p0, Lcom/google/k/c/i;->a:Lcom/google/k/c/h;

    invoke-direct {v0, v1}, Lcom/google/k/c/j;-><init>(Lcom/google/k/c/h;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1327
    invoke-virtual {p0, p1}, Lcom/google/k/c/i;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1328
    const/4 v0, 0x0

    .line 1332
    :goto_0
    return v0

    .line 1330
    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    .line 1331
    iget-object v0, p0, Lcom/google/k/c/i;->a:Lcom/google/k/c/h;

    iget-object v0, v0, Lcom/google/k/c/h;->b:Lcom/google/k/c/e;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/k/c/e;->a(Lcom/google/k/c/e;Ljava/lang/Object;)I

    .line 1332
    const/4 v0, 0x1

    goto :goto_0
.end method
