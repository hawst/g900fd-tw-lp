.class public final Lcom/google/k/c/ba;
.super Lcom/google/k/c/ai;
.source "SourceFile"


# instance fields
.field transient a:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/k/c/ai;-><init>(Ljava/util/Map;)V

    .line 53
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/k/c/ba;->a:I

    .line 92
    return-void
.end method

.method public static r()Lcom/google/k/c/ba;
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/google/k/c/ba;

    invoke-direct {v0}, Lcom/google/k/c/ba;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/google/k/c/ai;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 49
    invoke-super {p0, p1, p2}, Lcom/google/k/c/ai;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final synthetic b()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/google/k/c/ba;->a:I

    invoke-static {v0}, Lcom/google/k/c/em;->a(I)Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 49
    invoke-super {p0, p1, p2}, Lcom/google/k/c/ai;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic c()I
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Lcom/google/k/c/ai;->c()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 49
    invoke-super {p0, p1, p2}, Lcom/google/k/c/ai;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic d()V
    .locals 0

    .prologue
    .line 49
    invoke-super {p0}, Lcom/google/k/c/ai;->d()V

    return-void
.end method

.method public final bridge synthetic d(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/google/k/c/ai;->d(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic e(Ljava/lang/Object;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/google/k/c/ai;->e(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/google/k/c/ai;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic f()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Lcom/google/k/c/ai;->f()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic f(Ljava/lang/Object;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/google/k/c/ai;->f(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic hashCode()I
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Lcom/google/k/c/ai;->hashCode()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic k()Z
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Lcom/google/k/c/ai;->k()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic m()Ljava/util/Set;
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Lcom/google/k/c/ai;->m()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic o()Ljava/util/Map;
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Lcom/google/k/c/ai;->o()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method final p()Ljava/util/Set;
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/google/k/c/ba;->a:I

    invoke-static {v0}, Lcom/google/k/c/em;->a(I)Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic q()Ljava/util/Set;
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Lcom/google/k/c/ai;->q()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Lcom/google/k/c/ai;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
