.class public final Lcom/google/k/c/cv;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lcom/google/k/a/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 3458
    sget-object v0, Lcom/google/k/c/ap;->a:Lcom/google/k/a/x;

    const-string v1, "="

    invoke-virtual {v0, v1}, Lcom/google/k/a/x;->c(Ljava/lang/String;)Lcom/google/k/a/ab;

    move-result-object v0

    sput-object v0, Lcom/google/k/c/cv;->a:Lcom/google/k/a/ab;

    return-void
.end method

.method static a(I)I
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x3

    if-ge p0, v0, :cond_0

    .line 205
    const-string v0, "expectedSize"

    invoke-static {p0, v0}, Lcom/google/k/c/ao;->a(ILjava/lang/String;)I

    .line 206
    add-int/lit8 v0, p0, 0x1

    .line 211
    :goto_0
    return v0

    .line 208
    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    if-ge p0, v0, :cond_1

    .line 209
    div-int/lit8 v0, p0, 0x3

    add-int/2addr v0, p0

    goto :goto_0

    .line 211
    :cond_1
    const v0, 0x7fffffff

    goto :goto_0
.end method

.method static a(Lcom/google/k/c/df;)Lcom/google/k/a/u;
    .locals 1

    .prologue
    .line 1867
    invoke-static {p0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1868
    new-instance v0, Lcom/google/k/c/cy;

    invoke-direct {v0, p0}, Lcom/google/k/c/cy;-><init>(Lcom/google/k/c/df;)V

    return-object v0
.end method

.method static a(Lcom/google/k/c/fl;)Lcom/google/k/c/fl;
    .locals 1

    .prologue
    .line 122
    new-instance v0, Lcom/google/k/c/cw;

    invoke-direct {v0, p0}, Lcom/google/k/c/cw;-><init>(Lcom/google/k/c/fl;)V

    return-object v0
.end method

.method static a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 3351
    invoke-static {p0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3353
    :try_start_0
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 3357
    :goto_0
    return-object v0

    .line 3355
    :catch_0
    move-exception v1

    goto :goto_0

    .line 3357
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public static a(Ljava/util/Map;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 3465
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/k/c/ap;->a(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 3467
    sget-object v1, Lcom/google/k/c/cv;->a:Lcom/google/k/a/ab;

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/k/a/ab;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    .line 3468
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/util/Iterator;)Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/google/k/c/db;->a:Lcom/google/k/c/db;

    invoke-static {p0, v0}, Lcom/google/k/c/cj;->a(Ljava/util/Iterator;Lcom/google/k/a/u;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public static a()Ljava/util/LinkedHashMap;
    .locals 1

    .prologue
    .line 243
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    return-object v0
.end method

.method static a(Lcom/google/k/c/df;Ljava/util/Map$Entry;)Ljava/util/Map$Entry;
    .locals 1

    .prologue
    .line 1847
    invoke-static {p0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1848
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1849
    new-instance v0, Lcom/google/k/c/cx;

    invoke-direct {v0, p1, p0}, Lcom/google/k/c/cx;-><init>(Ljava/util/Map$Entry;Lcom/google/k/c/df;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;
    .locals 1

    .prologue
    .line 1207
    new-instance v0, Lcom/google/k/c/bi;

    invoke-direct {v0, p0, p1}, Lcom/google/k/c/bi;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method private static a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;
    .locals 1

    .prologue
    .line 1234
    invoke-static {p0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1235
    new-instance v0, Lcom/google/k/c/cz;

    invoke-direct {v0, p0}, Lcom/google/k/c/cz;-><init>(Ljava/util/Map$Entry;)V

    return-object v0
.end method

.method public static a(Ljava/util/Map;Lcom/google/k/a/u;)Ljava/util/Map;
    .locals 2

    .prologue
    .line 1494
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/k/c/da;

    invoke-direct {v1, p1}, Lcom/google/k/c/da;-><init>(Lcom/google/k/a/u;)V

    instance-of v0, p0, Ljava/util/SortedMap;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/util/SortedMap;

    invoke-static {p0, v1}, Lcom/google/k/c/cv;->a(Ljava/util/SortedMap;Lcom/google/k/c/df;)Ljava/util/SortedMap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/k/c/di;

    invoke-direct {v0, p0, v1}, Lcom/google/k/c/di;-><init>(Ljava/util/Map;Lcom/google/k/c/df;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/NavigableMap;Lcom/google/k/c/df;)Ljava/util/NavigableMap;
    .locals 1

    .prologue
    .line 1763
    new-instance v0, Lcom/google/k/c/dk;

    invoke-direct {v0, p0, p1}, Lcom/google/k/c/dk;-><init>(Ljava/util/NavigableMap;Lcom/google/k/c/df;)V

    return-object v0
.end method

.method public static a(Ljava/util/SortedMap;Lcom/google/k/c/df;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 1703
    instance-of v0, p0, Ljava/util/NavigableMap;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/util/NavigableMap;

    invoke-static {p0, p1}, Lcom/google/k/c/cv;->a(Ljava/util/NavigableMap;Lcom/google/k/c/df;)Ljava/util/NavigableMap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/k/c/dl;

    invoke-direct {v0, p0, p1}, Lcom/google/k/c/dl;-><init>(Ljava/util/SortedMap;Lcom/google/k/c/df;)V

    goto :goto_0
.end method

.method static a(Ljava/util/Collection;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 3419
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_0

    .line 3420
    const/4 v0, 0x0

    .line 3422
    :goto_0
    return v0

    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    invoke-static {p1}, Lcom/google/k/c/cv;->a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method static b(Ljava/util/Iterator;)Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/google/k/c/db;->b:Lcom/google/k/c/db;

    invoke-static {p0, v0}, Lcom/google/k/c/cj;->a(Ljava/util/Iterator;Lcom/google/k/a/u;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method static b(Ljava/util/Collection;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 3439
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_0

    .line 3440
    const/4 v0, 0x0

    .line 3442
    :goto_0
    return v0

    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    invoke-static {p1}, Lcom/google/k/c/cv;->a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method static b(Ljava/util/Map;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 3366
    invoke-static {p0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3368
    :try_start_0
    invoke-interface {p0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 3372
    :goto_0
    return v0

    .line 3370
    :catch_0
    move-exception v1

    goto :goto_0

    .line 3372
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method static c(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 3381
    invoke-static {p0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3383
    :try_start_0
    invoke-interface {p0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 3387
    :goto_0
    return-object v0

    .line 3385
    :catch_0
    move-exception v1

    goto :goto_0

    .line 3387
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method static d(Ljava/util/Map;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3449
    if-ne p0, p1, :cond_0

    .line 3450
    const/4 v0, 0x1

    .line 3455
    :goto_0
    return v0

    .line 3451
    :cond_0
    instance-of v0, p1, Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 3452
    check-cast p1, Ljava/util/Map;

    .line 3453
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 3455
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
