.class abstract Lcom/google/k/c/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final b:Ljava/util/Iterator;

.field c:Ljava/lang/Object;

.field d:Ljava/util/Collection;

.field e:Ljava/util/Iterator;

.field final synthetic f:Lcom/google/k/c/e;


# direct methods
.method constructor <init>(Lcom/google/k/c/e;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1128
    iput-object p1, p0, Lcom/google/k/c/k;->f:Lcom/google/k/c/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129
    invoke-static {p1}, Lcom/google/k/c/e;->a(Lcom/google/k/c/e;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/k;->b:Ljava/util/Iterator;

    .line 1130
    iput-object v1, p0, Lcom/google/k/c/k;->c:Ljava/lang/Object;

    .line 1131
    iput-object v1, p0, Lcom/google/k/c/k;->d:Ljava/util/Collection;

    .line 1132
    invoke-static {}, Lcom/google/k/c/cj;->b()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/k;->e:Ljava/util/Iterator;

    .line 1133
    return-void
.end method


# virtual methods
.method abstract a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 1139
    iget-object v0, p0, Lcom/google/k/c/k;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/k/c/k;->e:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1144
    iget-object v0, p0, Lcom/google/k/c/k;->e:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1145
    iget-object v0, p0, Lcom/google/k/c/k;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1146
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lcom/google/k/c/k;->c:Ljava/lang/Object;

    .line 1147
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    iput-object v0, p0, Lcom/google/k/c/k;->d:Ljava/util/Collection;

    .line 1148
    iget-object v0, p0, Lcom/google/k/c/k;->d:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/k;->e:Ljava/util/Iterator;

    .line 1150
    :cond_0
    iget-object v0, p0, Lcom/google/k/c/k;->c:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/k/c/k;->e:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/k/c/k;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 1155
    iget-object v0, p0, Lcom/google/k/c/k;->e:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 1156
    iget-object v0, p0, Lcom/google/k/c/k;->d:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1157
    iget-object v0, p0, Lcom/google/k/c/k;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 1159
    :cond_0
    iget-object v0, p0, Lcom/google/k/c/k;->f:Lcom/google/k/c/e;

    invoke-static {v0}, Lcom/google/k/c/e;->b(Lcom/google/k/c/e;)I

    .line 1160
    return-void
.end method
