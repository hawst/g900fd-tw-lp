.class abstract Lcom/google/k/c/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/k/c/dn;


# instance fields
.field private transient a:Ljava/util/Collection;

.field private transient b:Ljava/util/Set;

.field private transient c:Ljava/util/Collection;

.field private transient d:Ljava/util/Map;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0, p1}, Lcom/google/k/c/ab;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/google/k/c/ab;->o()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 58
    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/google/k/c/ab;->o()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 64
    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/k/c/ab;->o()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 47
    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    const/4 v0, 0x1

    .line 52
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e()Ljava/util/Set;
    .locals 2

    .prologue
    .line 154
    new-instance v0, Lcom/google/k/c/dh;

    invoke-virtual {p0}, Lcom/google/k/c/ab;->o()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/k/c/dh;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 216
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lcom/google/k/c/dn;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/k/c/dn;

    invoke-interface {p0}, Lcom/google/k/c/dn;->o()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/k/c/dn;->o()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/k/c/ab;->c:Ljava/util/Collection;

    .line 174
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/k/c/ab;->n()Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/ab;->c:Ljava/util/Collection;

    :cond_0
    return-object v0
.end method

.method g()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/google/k/c/ab;->h()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/c/cv;->b(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/k/c/ab;->a:Ljava/util/Collection;

    .line 108
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/k/c/ab;->l()Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/ab;->a:Ljava/util/Collection;

    :cond_0
    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/google/k/c/ab;->o()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    return v0
.end method

.method abstract i()Ljava/util/Iterator;
.end method

.method abstract j()Ljava/util/Map;
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/google/k/c/ab;->c()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method l()Ljava/util/Collection;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 112
    instance-of v0, p0, Lcom/google/k/c/el;

    if-eqz v0, :cond_0

    .line 113
    new-instance v0, Lcom/google/k/c/ad;

    invoke-direct {v0, p0, v1}, Lcom/google/k/c/ad;-><init>(Lcom/google/k/c/ab;B)V

    .line 115
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/k/c/ac;

    invoke-direct {v0, p0, v1}, Lcom/google/k/c/ac;-><init>(Lcom/google/k/c/ab;B)V

    goto :goto_0
.end method

.method public m()Ljava/util/Set;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/k/c/ab;->b:Ljava/util/Set;

    .line 150
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/k/c/ab;->e()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/ab;->b:Ljava/util/Set;

    :cond_0
    return-object v0
.end method

.method n()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 178
    new-instance v0, Lcom/google/k/c/ae;

    invoke-direct {v0, p0}, Lcom/google/k/c/ae;-><init>(Lcom/google/k/c/ab;)V

    return-object v0
.end method

.method public o()Ljava/util/Map;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/k/c/ab;->d:Ljava/util/Map;

    .line 208
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/k/c/ab;->j()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/c/ab;->d:Ljava/util/Map;

    :cond_0
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/google/k/c/ab;->o()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
