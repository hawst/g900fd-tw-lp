.class public final Lcom/google/k/f/g;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 739
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 740
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/k/f/g;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/g;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/k/f/g;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/g;->cachedSize:I

    .line 741
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 797
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 798
    iget v1, p0, Lcom/google/k/f/g;->a:I

    if-eqz v1, :cond_0

    .line 799
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/k/f/g;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 802
    :cond_0
    iget-object v1, p0, Lcom/google/k/f/g;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 803
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/k/f/g;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 806
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 753
    if-ne p1, p0, :cond_1

    .line 754
    const/4 v0, 0x1

    .line 770
    :cond_0
    :goto_0
    return v0

    .line 756
    :cond_1
    instance-of v1, p1, Lcom/google/k/f/g;

    if-eqz v1, :cond_0

    .line 759
    check-cast p1, Lcom/google/k/f/g;

    .line 760
    iget v1, p0, Lcom/google/k/f/g;->a:I

    iget v2, p1, Lcom/google/k/f/g;->a:I

    if-ne v1, v2, :cond_0

    .line 763
    iget-object v1, p0, Lcom/google/k/f/g;->b:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 764
    iget-object v1, p1, Lcom/google/k/f/g;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 770
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/k/f/g;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 767
    :cond_3
    iget-object v1, p0, Lcom/google/k/f/g;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/k/f/g;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 775
    iget v0, p0, Lcom/google/k/f/g;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 777
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/g;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 779
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/k/f/g;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 780
    return v0

    .line 777
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/g;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 706
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/k/f/g;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/k/f/g;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/g;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 786
    iget v0, p0, Lcom/google/k/f/g;->a:I

    if-eqz v0, :cond_0

    .line 787
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/k/f/g;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 789
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/g;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 790
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/k/f/g;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 792
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 793
    return-void
.end method
