.class public final Lcom/google/k/f/ap;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 414
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 415
    iput v0, p0, Lcom/google/k/f/ap;->a:I

    iput v0, p0, Lcom/google/k/f/ap;->b:I

    iput v0, p0, Lcom/google/k/f/ap;->c:I

    iput v0, p0, Lcom/google/k/f/ap;->d:I

    iput v0, p0, Lcom/google/k/f/ap;->e:I

    iput v0, p0, Lcom/google/k/f/ap;->f:I

    iput v0, p0, Lcom/google/k/f/ap;->g:I

    iput v0, p0, Lcom/google/k/f/ap;->h:I

    iput v0, p0, Lcom/google/k/f/ap;->i:I

    iput v0, p0, Lcom/google/k/f/ap;->j:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/k/f/ap;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/ap;->cachedSize:I

    .line 416
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 531
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 532
    iget v1, p0, Lcom/google/k/f/ap;->a:I

    if-eqz v1, :cond_0

    .line 533
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/k/f/ap;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 536
    :cond_0
    iget v1, p0, Lcom/google/k/f/ap;->d:I

    if-eqz v1, :cond_1

    .line 537
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/k/f/ap;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 540
    :cond_1
    iget v1, p0, Lcom/google/k/f/ap;->e:I

    if-eqz v1, :cond_2

    .line 541
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/k/f/ap;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 544
    :cond_2
    iget v1, p0, Lcom/google/k/f/ap;->f:I

    if-eqz v1, :cond_3

    .line 545
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/k/f/ap;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 548
    :cond_3
    iget v1, p0, Lcom/google/k/f/ap;->g:I

    if-eqz v1, :cond_4

    .line 549
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/k/f/ap;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 552
    :cond_4
    iget v1, p0, Lcom/google/k/f/ap;->h:I

    if-eqz v1, :cond_5

    .line 553
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/k/f/ap;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 556
    :cond_5
    iget v1, p0, Lcom/google/k/f/ap;->i:I

    if-eqz v1, :cond_6

    .line 557
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/k/f/ap;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 560
    :cond_6
    iget v1, p0, Lcom/google/k/f/ap;->j:I

    if-eqz v1, :cond_7

    .line 561
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/k/f/ap;->j:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 564
    :cond_7
    iget v1, p0, Lcom/google/k/f/ap;->b:I

    if-eqz v1, :cond_8

    .line 565
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/k/f/ap;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 568
    :cond_8
    iget v1, p0, Lcom/google/k/f/ap;->c:I

    if-eqz v1, :cond_9

    .line 569
    const/16 v1, 0xa

    iget v2, p0, Lcom/google/k/f/ap;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 572
    :cond_9
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 436
    if-ne p1, p0, :cond_1

    .line 437
    const/4 v0, 0x1

    .line 473
    :cond_0
    :goto_0
    return v0

    .line 439
    :cond_1
    instance-of v1, p1, Lcom/google/k/f/ap;

    if-eqz v1, :cond_0

    .line 442
    check-cast p1, Lcom/google/k/f/ap;

    .line 443
    iget v1, p0, Lcom/google/k/f/ap;->a:I

    iget v2, p1, Lcom/google/k/f/ap;->a:I

    if-ne v1, v2, :cond_0

    .line 446
    iget v1, p0, Lcom/google/k/f/ap;->b:I

    iget v2, p1, Lcom/google/k/f/ap;->b:I

    if-ne v1, v2, :cond_0

    .line 449
    iget v1, p0, Lcom/google/k/f/ap;->c:I

    iget v2, p1, Lcom/google/k/f/ap;->c:I

    if-ne v1, v2, :cond_0

    .line 452
    iget v1, p0, Lcom/google/k/f/ap;->d:I

    iget v2, p1, Lcom/google/k/f/ap;->d:I

    if-ne v1, v2, :cond_0

    .line 455
    iget v1, p0, Lcom/google/k/f/ap;->e:I

    iget v2, p1, Lcom/google/k/f/ap;->e:I

    if-ne v1, v2, :cond_0

    .line 458
    iget v1, p0, Lcom/google/k/f/ap;->f:I

    iget v2, p1, Lcom/google/k/f/ap;->f:I

    if-ne v1, v2, :cond_0

    .line 461
    iget v1, p0, Lcom/google/k/f/ap;->g:I

    iget v2, p1, Lcom/google/k/f/ap;->g:I

    if-ne v1, v2, :cond_0

    .line 464
    iget v1, p0, Lcom/google/k/f/ap;->h:I

    iget v2, p1, Lcom/google/k/f/ap;->h:I

    if-ne v1, v2, :cond_0

    .line 467
    iget v1, p0, Lcom/google/k/f/ap;->i:I

    iget v2, p1, Lcom/google/k/f/ap;->i:I

    if-ne v1, v2, :cond_0

    .line 470
    iget v1, p0, Lcom/google/k/f/ap;->j:I

    iget v2, p1, Lcom/google/k/f/ap;->j:I

    if-ne v1, v2, :cond_0

    .line 473
    invoke-virtual {p0, p1}, Lcom/google/k/f/ap;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 478
    iget v0, p0, Lcom/google/k/f/ap;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 480
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/ap;->b:I

    add-int/2addr v0, v1

    .line 481
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/ap;->c:I

    add-int/2addr v0, v1

    .line 482
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/ap;->d:I

    add-int/2addr v0, v1

    .line 483
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/ap;->e:I

    add-int/2addr v0, v1

    .line 484
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/ap;->f:I

    add-int/2addr v0, v1

    .line 485
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/ap;->g:I

    add-int/2addr v0, v1

    .line 486
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/ap;->h:I

    add-int/2addr v0, v1

    .line 487
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/ap;->i:I

    add-int/2addr v0, v1

    .line 488
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/ap;->j:I

    add-int/2addr v0, v1

    .line 489
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/k/f/ap;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 490
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 343
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/k/f/ap;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/k/f/ap;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/ap;->d:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/ap;->e:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/ap;->f:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/ap;->g:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/ap;->h:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/ap;->i:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/ap;->j:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/k/f/ap;->b:I

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    iput v0, p0, Lcom/google/k/f/ap;->c:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 496
    iget v0, p0, Lcom/google/k/f/ap;->a:I

    if-eqz v0, :cond_0

    .line 497
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/k/f/ap;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 499
    :cond_0
    iget v0, p0, Lcom/google/k/f/ap;->d:I

    if-eqz v0, :cond_1

    .line 500
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/k/f/ap;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 502
    :cond_1
    iget v0, p0, Lcom/google/k/f/ap;->e:I

    if-eqz v0, :cond_2

    .line 503
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/k/f/ap;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 505
    :cond_2
    iget v0, p0, Lcom/google/k/f/ap;->f:I

    if-eqz v0, :cond_3

    .line 506
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/k/f/ap;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 508
    :cond_3
    iget v0, p0, Lcom/google/k/f/ap;->g:I

    if-eqz v0, :cond_4

    .line 509
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/k/f/ap;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 511
    :cond_4
    iget v0, p0, Lcom/google/k/f/ap;->h:I

    if-eqz v0, :cond_5

    .line 512
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/k/f/ap;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 514
    :cond_5
    iget v0, p0, Lcom/google/k/f/ap;->i:I

    if-eqz v0, :cond_6

    .line 515
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/k/f/ap;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 517
    :cond_6
    iget v0, p0, Lcom/google/k/f/ap;->j:I

    if-eqz v0, :cond_7

    .line 518
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/k/f/ap;->j:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 520
    :cond_7
    iget v0, p0, Lcom/google/k/f/ap;->b:I

    if-eqz v0, :cond_8

    .line 521
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/k/f/ap;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 523
    :cond_8
    iget v0, p0, Lcom/google/k/f/ap;->c:I

    if-eqz v0, :cond_9

    .line 524
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/k/f/ap;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 526
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 527
    return-void
.end method
