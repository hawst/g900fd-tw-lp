.class public final Lcom/google/k/f/an;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/k/f/ao;

.field public b:Lcom/google/k/f/aq;

.field public c:Lcom/google/k/f/ap;

.field public d:Lcom/google/k/f/ar;

.field public e:[Lcom/google/k/f/ak;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1459
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1460
    iput-object v1, p0, Lcom/google/k/f/an;->a:Lcom/google/k/f/ao;

    iput-object v1, p0, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    iput-object v1, p0, Lcom/google/k/f/an;->c:Lcom/google/k/f/ap;

    iput-object v1, p0, Lcom/google/k/f/an;->d:Lcom/google/k/f/ar;

    invoke-static {}, Lcom/google/k/f/ak;->a()[Lcom/google/k/f/ak;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/an;->e:[Lcom/google/k/f/ak;

    iput-object v1, p0, Lcom/google/k/f/an;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/an;->cachedSize:I

    .line 1461
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 1571
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1572
    iget-object v1, p0, Lcom/google/k/f/an;->a:Lcom/google/k/f/ao;

    if-eqz v1, :cond_0

    .line 1573
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/k/f/an;->a:Lcom/google/k/f/ao;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1576
    :cond_0
    iget-object v1, p0, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    if-eqz v1, :cond_1

    .line 1577
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1580
    :cond_1
    iget-object v1, p0, Lcom/google/k/f/an;->c:Lcom/google/k/f/ap;

    if-eqz v1, :cond_2

    .line 1581
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/k/f/an;->c:Lcom/google/k/f/ap;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1584
    :cond_2
    iget-object v1, p0, Lcom/google/k/f/an;->d:Lcom/google/k/f/ar;

    if-eqz v1, :cond_3

    .line 1585
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/k/f/an;->d:Lcom/google/k/f/ar;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1588
    :cond_3
    iget-object v1, p0, Lcom/google/k/f/an;->e:[Lcom/google/k/f/ak;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/k/f/an;->e:[Lcom/google/k/f/ak;

    array-length v1, v1

    if-lez v1, :cond_6

    .line 1589
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/k/f/an;->e:[Lcom/google/k/f/ak;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 1590
    iget-object v2, p0, Lcom/google/k/f/an;->e:[Lcom/google/k/f/ak;

    aget-object v2, v2, v0

    .line 1591
    if-eqz v2, :cond_4

    .line 1592
    const/4 v3, 0x5

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1589
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 1597
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1476
    if-ne p1, p0, :cond_1

    .line 1477
    const/4 v0, 0x1

    .line 1523
    :cond_0
    :goto_0
    return v0

    .line 1479
    :cond_1
    instance-of v1, p1, Lcom/google/k/f/an;

    if-eqz v1, :cond_0

    .line 1482
    check-cast p1, Lcom/google/k/f/an;

    .line 1483
    iget-object v1, p0, Lcom/google/k/f/an;->a:Lcom/google/k/f/ao;

    if-nez v1, :cond_6

    .line 1484
    iget-object v1, p1, Lcom/google/k/f/an;->a:Lcom/google/k/f/ao;

    if-nez v1, :cond_0

    .line 1492
    :cond_2
    iget-object v1, p0, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    if-nez v1, :cond_7

    .line 1493
    iget-object v1, p1, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    if-nez v1, :cond_0

    .line 1501
    :cond_3
    iget-object v1, p0, Lcom/google/k/f/an;->c:Lcom/google/k/f/ap;

    if-nez v1, :cond_8

    .line 1502
    iget-object v1, p1, Lcom/google/k/f/an;->c:Lcom/google/k/f/ap;

    if-nez v1, :cond_0

    .line 1510
    :cond_4
    iget-object v1, p0, Lcom/google/k/f/an;->d:Lcom/google/k/f/ar;

    if-nez v1, :cond_9

    .line 1511
    iget-object v1, p1, Lcom/google/k/f/an;->d:Lcom/google/k/f/ar;

    if-nez v1, :cond_0

    .line 1519
    :cond_5
    iget-object v1, p0, Lcom/google/k/f/an;->e:[Lcom/google/k/f/ak;

    iget-object v2, p1, Lcom/google/k/f/an;->e:[Lcom/google/k/f/ak;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1523
    invoke-virtual {p0, p1}, Lcom/google/k/f/an;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1488
    :cond_6
    iget-object v1, p0, Lcom/google/k/f/an;->a:Lcom/google/k/f/ao;

    iget-object v2, p1, Lcom/google/k/f/an;->a:Lcom/google/k/f/ao;

    invoke-virtual {v1, v2}, Lcom/google/k/f/ao;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 1497
    :cond_7
    iget-object v1, p0, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    iget-object v2, p1, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    invoke-virtual {v1, v2}, Lcom/google/k/f/aq;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 1506
    :cond_8
    iget-object v1, p0, Lcom/google/k/f/an;->c:Lcom/google/k/f/ap;

    iget-object v2, p1, Lcom/google/k/f/an;->c:Lcom/google/k/f/ap;

    invoke-virtual {v1, v2}, Lcom/google/k/f/ap;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 1515
    :cond_9
    iget-object v1, p0, Lcom/google/k/f/an;->d:Lcom/google/k/f/ar;

    iget-object v2, p1, Lcom/google/k/f/an;->d:Lcom/google/k/f/ar;

    invoke-virtual {v1, v2}, Lcom/google/k/f/ar;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1528
    iget-object v0, p0, Lcom/google/k/f/an;->a:Lcom/google/k/f/ao;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1531
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1533
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/an;->c:Lcom/google/k/f/ap;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1535
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/k/f/an;->d:Lcom/google/k/f/ar;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 1537
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/k/f/an;->e:[Lcom/google/k/f/ak;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1539
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/k/f/an;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1540
    return v0

    .line 1528
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/an;->a:Lcom/google/k/f/ao;

    invoke-virtual {v0}, Lcom/google/k/f/ao;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1531
    :cond_1
    iget-object v0, p0, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    invoke-virtual {v0}, Lcom/google/k/f/aq;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1533
    :cond_2
    iget-object v0, p0, Lcom/google/k/f/an;->c:Lcom/google/k/f/ap;

    invoke-virtual {v0}, Lcom/google/k/f/ap;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1535
    :cond_3
    iget-object v1, p0, Lcom/google/k/f/an;->d:Lcom/google/k/f/ar;

    invoke-virtual {v1}, Lcom/google/k/f/ar;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1427
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/k/f/an;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/k/f/an;->a:Lcom/google/k/f/ao;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/k/f/ao;

    invoke-direct {v0}, Lcom/google/k/f/ao;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/an;->a:Lcom/google/k/f/ao;

    :cond_1
    iget-object v0, p0, Lcom/google/k/f/an;->a:Lcom/google/k/f/ao;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/k/f/aq;

    invoke-direct {v0}, Lcom/google/k/f/aq;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    :cond_2
    iget-object v0, p0, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/k/f/an;->c:Lcom/google/k/f/ap;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/k/f/ap;

    invoke-direct {v0}, Lcom/google/k/f/ap;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/an;->c:Lcom/google/k/f/ap;

    :cond_3
    iget-object v0, p0, Lcom/google/k/f/an;->c:Lcom/google/k/f/ap;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/k/f/an;->d:Lcom/google/k/f/ar;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/k/f/ar;

    invoke-direct {v0}, Lcom/google/k/f/ar;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/an;->d:Lcom/google/k/f/ar;

    :cond_4
    iget-object v0, p0, Lcom/google/k/f/an;->d:Lcom/google/k/f/ar;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/k/f/an;->e:[Lcom/google/k/f/ak;

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/k/f/ak;

    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/google/k/f/an;->e:[Lcom/google/k/f/ak;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    new-instance v3, Lcom/google/k/f/ak;

    invoke-direct {v3}, Lcom/google/k/f/ak;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/google/k/f/an;->e:[Lcom/google/k/f/ak;

    array-length v0, v0

    goto :goto_1

    :cond_7
    new-instance v3, Lcom/google/k/f/ak;

    invoke-direct {v3}, Lcom/google/k/f/ak;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/k/f/an;->e:[Lcom/google/k/f/ak;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1546
    iget-object v0, p0, Lcom/google/k/f/an;->a:Lcom/google/k/f/ao;

    if-eqz v0, :cond_0

    .line 1547
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/k/f/an;->a:Lcom/google/k/f/ao;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1549
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    if-eqz v0, :cond_1

    .line 1550
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1552
    :cond_1
    iget-object v0, p0, Lcom/google/k/f/an;->c:Lcom/google/k/f/ap;

    if-eqz v0, :cond_2

    .line 1553
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/k/f/an;->c:Lcom/google/k/f/ap;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1555
    :cond_2
    iget-object v0, p0, Lcom/google/k/f/an;->d:Lcom/google/k/f/ar;

    if-eqz v0, :cond_3

    .line 1556
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/k/f/an;->d:Lcom/google/k/f/ar;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1558
    :cond_3
    iget-object v0, p0, Lcom/google/k/f/an;->e:[Lcom/google/k/f/ak;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/k/f/an;->e:[Lcom/google/k/f/ak;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 1559
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/k/f/an;->e:[Lcom/google/k/f/ak;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 1560
    iget-object v1, p0, Lcom/google/k/f/an;->e:[Lcom/google/k/f/ak;

    aget-object v1, v1, v0

    .line 1561
    if-eqz v1, :cond_4

    .line 1562
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1559
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1566
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1567
    return-void
.end method
