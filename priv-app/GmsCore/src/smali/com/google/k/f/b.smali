.class public final Lcom/google/k/f/b;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/k/f/c;

.field public b:Lcom/google/k/f/f;

.field public c:Lcom/google/k/f/h;

.field public d:Lcom/google/k/f/i;

.field public e:I

.field public f:Z

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1221
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1222
    iput-object v1, p0, Lcom/google/k/f/b;->a:Lcom/google/k/f/c;

    iput-object v1, p0, Lcom/google/k/f/b;->b:Lcom/google/k/f/f;

    iput-object v1, p0, Lcom/google/k/f/b;->c:Lcom/google/k/f/h;

    iput-object v1, p0, Lcom/google/k/f/b;->d:Lcom/google/k/f/i;

    iput v0, p0, Lcom/google/k/f/b;->e:I

    iput-boolean v0, p0, Lcom/google/k/f/b;->f:Z

    iput v0, p0, Lcom/google/k/f/b;->g:I

    iput v2, p0, Lcom/google/k/f/b;->h:I

    iput v0, p0, Lcom/google/k/f/b;->i:I

    iput v0, p0, Lcom/google/k/f/b;->j:I

    iput-boolean v0, p0, Lcom/google/k/f/b;->k:Z

    iput-object v1, p0, Lcom/google/k/f/b;->unknownFieldData:Lcom/google/protobuf/nano/f;

    iput v2, p0, Lcom/google/k/f/b;->cachedSize:I

    .line 1223
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1374
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1375
    iget-object v1, p0, Lcom/google/k/f/b;->a:Lcom/google/k/f/c;

    if-eqz v1, :cond_0

    .line 1376
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/k/f/b;->a:Lcom/google/k/f/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1379
    :cond_0
    iget-object v1, p0, Lcom/google/k/f/b;->b:Lcom/google/k/f/f;

    if-eqz v1, :cond_1

    .line 1380
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/k/f/b;->b:Lcom/google/k/f/f;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1383
    :cond_1
    iget-object v1, p0, Lcom/google/k/f/b;->c:Lcom/google/k/f/h;

    if-eqz v1, :cond_2

    .line 1384
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/k/f/b;->c:Lcom/google/k/f/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1387
    :cond_2
    iget-object v1, p0, Lcom/google/k/f/b;->d:Lcom/google/k/f/i;

    if-eqz v1, :cond_3

    .line 1388
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/k/f/b;->d:Lcom/google/k/f/i;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1391
    :cond_3
    iget v1, p0, Lcom/google/k/f/b;->e:I

    if-eqz v1, :cond_4

    .line 1392
    const/16 v1, 0x20

    iget v2, p0, Lcom/google/k/f/b;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1395
    :cond_4
    iget-boolean v1, p0, Lcom/google/k/f/b;->f:Z

    if-eqz v1, :cond_5

    .line 1396
    const/16 v1, 0x21

    iget-boolean v2, p0, Lcom/google/k/f/b;->f:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1399
    :cond_5
    iget v1, p0, Lcom/google/k/f/b;->g:I

    if-eqz v1, :cond_6

    .line 1400
    const/16 v1, 0x22

    iget v2, p0, Lcom/google/k/f/b;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1403
    :cond_6
    iget v1, p0, Lcom/google/k/f/b;->h:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_7

    .line 1404
    const/16 v1, 0x23

    iget v2, p0, Lcom/google/k/f/b;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1407
    :cond_7
    iget v1, p0, Lcom/google/k/f/b;->i:I

    if-eqz v1, :cond_8

    .line 1408
    const/16 v1, 0x24

    iget v2, p0, Lcom/google/k/f/b;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1411
    :cond_8
    iget v1, p0, Lcom/google/k/f/b;->j:I

    if-eqz v1, :cond_9

    .line 1412
    const/16 v1, 0x25

    iget v2, p0, Lcom/google/k/f/b;->j:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1415
    :cond_9
    iget-boolean v1, p0, Lcom/google/k/f/b;->k:Z

    if-eqz v1, :cond_a

    .line 1416
    const/16 v1, 0x26

    iget-boolean v2, p0, Lcom/google/k/f/b;->k:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1419
    :cond_a
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1244
    if-ne p1, p0, :cond_1

    .line 1245
    const/4 v0, 0x1

    .line 1308
    :cond_0
    :goto_0
    return v0

    .line 1247
    :cond_1
    instance-of v1, p1, Lcom/google/k/f/b;

    if-eqz v1, :cond_0

    .line 1250
    check-cast p1, Lcom/google/k/f/b;

    .line 1251
    iget-object v1, p0, Lcom/google/k/f/b;->a:Lcom/google/k/f/c;

    if-nez v1, :cond_6

    .line 1252
    iget-object v1, p1, Lcom/google/k/f/b;->a:Lcom/google/k/f/c;

    if-nez v1, :cond_0

    .line 1260
    :cond_2
    iget-object v1, p0, Lcom/google/k/f/b;->b:Lcom/google/k/f/f;

    if-nez v1, :cond_7

    .line 1261
    iget-object v1, p1, Lcom/google/k/f/b;->b:Lcom/google/k/f/f;

    if-nez v1, :cond_0

    .line 1269
    :cond_3
    iget-object v1, p0, Lcom/google/k/f/b;->c:Lcom/google/k/f/h;

    if-nez v1, :cond_8

    .line 1270
    iget-object v1, p1, Lcom/google/k/f/b;->c:Lcom/google/k/f/h;

    if-nez v1, :cond_0

    .line 1278
    :cond_4
    iget-object v1, p0, Lcom/google/k/f/b;->d:Lcom/google/k/f/i;

    if-nez v1, :cond_9

    .line 1279
    iget-object v1, p1, Lcom/google/k/f/b;->d:Lcom/google/k/f/i;

    if-nez v1, :cond_0

    .line 1287
    :cond_5
    iget v1, p0, Lcom/google/k/f/b;->e:I

    iget v2, p1, Lcom/google/k/f/b;->e:I

    if-ne v1, v2, :cond_0

    .line 1290
    iget-boolean v1, p0, Lcom/google/k/f/b;->f:Z

    iget-boolean v2, p1, Lcom/google/k/f/b;->f:Z

    if-ne v1, v2, :cond_0

    .line 1293
    iget v1, p0, Lcom/google/k/f/b;->g:I

    iget v2, p1, Lcom/google/k/f/b;->g:I

    if-ne v1, v2, :cond_0

    .line 1296
    iget v1, p0, Lcom/google/k/f/b;->h:I

    iget v2, p1, Lcom/google/k/f/b;->h:I

    if-ne v1, v2, :cond_0

    .line 1299
    iget v1, p0, Lcom/google/k/f/b;->i:I

    iget v2, p1, Lcom/google/k/f/b;->i:I

    if-ne v1, v2, :cond_0

    .line 1302
    iget v1, p0, Lcom/google/k/f/b;->j:I

    iget v2, p1, Lcom/google/k/f/b;->j:I

    if-ne v1, v2, :cond_0

    .line 1305
    iget-boolean v1, p0, Lcom/google/k/f/b;->k:Z

    iget-boolean v2, p1, Lcom/google/k/f/b;->k:Z

    if-ne v1, v2, :cond_0

    .line 1308
    invoke-virtual {p0, p1}, Lcom/google/k/f/b;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1256
    :cond_6
    iget-object v1, p0, Lcom/google/k/f/b;->a:Lcom/google/k/f/c;

    iget-object v2, p1, Lcom/google/k/f/b;->a:Lcom/google/k/f/c;

    invoke-virtual {v1, v2}, Lcom/google/k/f/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 1265
    :cond_7
    iget-object v1, p0, Lcom/google/k/f/b;->b:Lcom/google/k/f/f;

    iget-object v2, p1, Lcom/google/k/f/b;->b:Lcom/google/k/f/f;

    invoke-virtual {v1, v2}, Lcom/google/k/f/f;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 1274
    :cond_8
    iget-object v1, p0, Lcom/google/k/f/b;->c:Lcom/google/k/f/h;

    iget-object v2, p1, Lcom/google/k/f/b;->c:Lcom/google/k/f/h;

    invoke-virtual {v1, v2}, Lcom/google/k/f/h;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 1283
    :cond_9
    iget-object v1, p0, Lcom/google/k/f/b;->d:Lcom/google/k/f/i;

    iget-object v2, p1, Lcom/google/k/f/b;->d:Lcom/google/k/f/i;

    invoke-virtual {v1, v2}, Lcom/google/k/f/i;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 1313
    iget-object v0, p0, Lcom/google/k/f/b;->a:Lcom/google/k/f/c;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1316
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/b;->b:Lcom/google/k/f/f;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 1318
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/b;->c:Lcom/google/k/f/h;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    .line 1320
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/k/f/b;->d:Lcom/google/k/f/i;

    if-nez v4, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 1322
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/b;->e:I

    add-int/2addr v0, v1

    .line 1323
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/b;->f:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_4
    add-int/2addr v0, v1

    .line 1324
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/b;->g:I

    add-int/2addr v0, v1

    .line 1325
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/b;->h:I

    add-int/2addr v0, v1

    .line 1326
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/b;->i:I

    add-int/2addr v0, v1

    .line 1327
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/b;->j:I

    add-int/2addr v0, v1

    .line 1328
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/k/f/b;->k:Z

    if-eqz v1, :cond_5

    :goto_5
    add-int/2addr v0, v2

    .line 1329
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/k/f/b;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1330
    return v0

    .line 1313
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/b;->a:Lcom/google/k/f/c;

    invoke-virtual {v0}, Lcom/google/k/f/c;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1316
    :cond_1
    iget-object v0, p0, Lcom/google/k/f/b;->b:Lcom/google/k/f/f;

    invoke-virtual {v0}, Lcom/google/k/f/f;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1318
    :cond_2
    iget-object v0, p0, Lcom/google/k/f/b;->c:Lcom/google/k/f/h;

    invoke-virtual {v0}, Lcom/google/k/f/h;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1320
    :cond_3
    iget-object v1, p0, Lcom/google/k/f/b;->d:Lcom/google/k/f/i;

    invoke-virtual {v1}, Lcom/google/k/f/i;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_4
    move v0, v3

    .line 1323
    goto :goto_4

    :cond_5
    move v2, v3

    .line 1328
    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1147
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/k/f/b;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/k/f/b;->a:Lcom/google/k/f/c;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/k/f/c;

    invoke-direct {v0}, Lcom/google/k/f/c;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/b;->a:Lcom/google/k/f/c;

    :cond_1
    iget-object v0, p0, Lcom/google/k/f/b;->a:Lcom/google/k/f/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/k/f/b;->b:Lcom/google/k/f/f;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/k/f/f;

    invoke-direct {v0}, Lcom/google/k/f/f;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/b;->b:Lcom/google/k/f/f;

    :cond_2
    iget-object v0, p0, Lcom/google/k/f/b;->b:Lcom/google/k/f/f;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/k/f/b;->c:Lcom/google/k/f/h;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/k/f/h;

    invoke-direct {v0}, Lcom/google/k/f/h;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/b;->c:Lcom/google/k/f/h;

    :cond_3
    iget-object v0, p0, Lcom/google/k/f/b;->c:Lcom/google/k/f/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/k/f/b;->d:Lcom/google/k/f/i;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/k/f/i;

    invoke-direct {v0}, Lcom/google/k/f/i;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/b;->d:Lcom/google/k/f/i;

    :cond_4
    iget-object v0, p0, Lcom/google/k/f/b;->d:Lcom/google/k/f/i;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/b;->e:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/b;->f:Z

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/b;->g:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/k/f/b;->h:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/b;->i:I

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/b;->j:I

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/b;->k:Z

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x100 -> :sswitch_5
        0x108 -> :sswitch_6
        0x110 -> :sswitch_7
        0x118 -> :sswitch_8
        0x120 -> :sswitch_9
        0x128 -> :sswitch_a
        0x130 -> :sswitch_b
    .end sparse-switch

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1336
    iget-object v0, p0, Lcom/google/k/f/b;->a:Lcom/google/k/f/c;

    if-eqz v0, :cond_0

    .line 1337
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/k/f/b;->a:Lcom/google/k/f/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1339
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/b;->b:Lcom/google/k/f/f;

    if-eqz v0, :cond_1

    .line 1340
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/k/f/b;->b:Lcom/google/k/f/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1342
    :cond_1
    iget-object v0, p0, Lcom/google/k/f/b;->c:Lcom/google/k/f/h;

    if-eqz v0, :cond_2

    .line 1343
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/k/f/b;->c:Lcom/google/k/f/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1345
    :cond_2
    iget-object v0, p0, Lcom/google/k/f/b;->d:Lcom/google/k/f/i;

    if-eqz v0, :cond_3

    .line 1346
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/k/f/b;->d:Lcom/google/k/f/i;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1348
    :cond_3
    iget v0, p0, Lcom/google/k/f/b;->e:I

    if-eqz v0, :cond_4

    .line 1349
    const/16 v0, 0x20

    iget v1, p0, Lcom/google/k/f/b;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1351
    :cond_4
    iget-boolean v0, p0, Lcom/google/k/f/b;->f:Z

    if-eqz v0, :cond_5

    .line 1352
    const/16 v0, 0x21

    iget-boolean v1, p0, Lcom/google/k/f/b;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1354
    :cond_5
    iget v0, p0, Lcom/google/k/f/b;->g:I

    if-eqz v0, :cond_6

    .line 1355
    const/16 v0, 0x22

    iget v1, p0, Lcom/google/k/f/b;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1357
    :cond_6
    iget v0, p0, Lcom/google/k/f/b;->h:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    .line 1358
    const/16 v0, 0x23

    iget v1, p0, Lcom/google/k/f/b;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1360
    :cond_7
    iget v0, p0, Lcom/google/k/f/b;->i:I

    if-eqz v0, :cond_8

    .line 1361
    const/16 v0, 0x24

    iget v1, p0, Lcom/google/k/f/b;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1363
    :cond_8
    iget v0, p0, Lcom/google/k/f/b;->j:I

    if-eqz v0, :cond_9

    .line 1364
    const/16 v0, 0x25

    iget v1, p0, Lcom/google/k/f/b;->j:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1366
    :cond_9
    iget-boolean v0, p0, Lcom/google/k/f/b;->k:Z

    if-eqz v0, :cond_a

    .line 1367
    const/16 v0, 0x26

    iget-boolean v1, p0, Lcom/google/k/f/b;->k:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1369
    :cond_a
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1370
    return-void
.end method
