.class public final Lcom/google/k/f/ak;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile j:[Lcom/google/k/f/ak;


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:I

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Lcom/google/k/f/al;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1167
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1168
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/k/f/ak;->a:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/ak;->b:Ljava/lang/String;

    iput v2, p0, Lcom/google/k/f/ak;->c:I

    iput v2, p0, Lcom/google/k/f/ak;->d:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/ak;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/ak;->f:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/ak;->g:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/ak;->h:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/k/f/ak;->i:Lcom/google/k/f/al;

    iput-object v3, p0, Lcom/google/k/f/ak;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/ak;->cachedSize:I

    .line 1169
    return-void
.end method

.method public static a()[Lcom/google/k/f/ak;
    .locals 2

    .prologue
    .line 1129
    sget-object v0, Lcom/google/k/f/ak;->j:[Lcom/google/k/f/ak;

    if-nez v0, :cond_1

    .line 1130
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1132
    :try_start_0
    sget-object v0, Lcom/google/k/f/ak;->j:[Lcom/google/k/f/ak;

    if-nez v0, :cond_0

    .line 1133
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/k/f/ak;

    sput-object v0, Lcom/google/k/f/ak;->j:[Lcom/google/k/f/ak;

    .line 1135
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1137
    :cond_1
    sget-object v0, Lcom/google/k/f/ak;->j:[Lcom/google/k/f/ak;

    return-object v0

    .line 1135
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 1309
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1310
    iget-wide v2, p0, Lcom/google/k/f/ak;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 1311
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/k/f/ak;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1314
    :cond_0
    iget-object v1, p0, Lcom/google/k/f/ak;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1315
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/k/f/ak;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1318
    :cond_1
    iget v1, p0, Lcom/google/k/f/ak;->c:I

    if-eqz v1, :cond_2

    .line 1319
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/k/f/ak;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1322
    :cond_2
    iget v1, p0, Lcom/google/k/f/ak;->d:I

    if-eqz v1, :cond_3

    .line 1323
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/k/f/ak;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1326
    :cond_3
    iget-object v1, p0, Lcom/google/k/f/ak;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1327
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/k/f/ak;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1330
    :cond_4
    iget-object v1, p0, Lcom/google/k/f/ak;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1331
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/k/f/ak;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1334
    :cond_5
    iget-object v1, p0, Lcom/google/k/f/ak;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1335
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/k/f/ak;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1338
    :cond_6
    iget-object v1, p0, Lcom/google/k/f/ak;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1339
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/k/f/ak;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1342
    :cond_7
    iget-object v1, p0, Lcom/google/k/f/ak;->i:Lcom/google/k/f/al;

    if-eqz v1, :cond_8

    .line 1343
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/k/f/ak;->i:Lcom/google/k/f/al;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1346
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1188
    if-ne p1, p0, :cond_1

    .line 1189
    const/4 v0, 0x1

    .line 1248
    :cond_0
    :goto_0
    return v0

    .line 1191
    :cond_1
    instance-of v1, p1, Lcom/google/k/f/ak;

    if-eqz v1, :cond_0

    .line 1194
    check-cast p1, Lcom/google/k/f/ak;

    .line 1195
    iget-wide v2, p0, Lcom/google/k/f/ak;->a:J

    iget-wide v4, p1, Lcom/google/k/f/ak;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 1198
    iget-object v1, p0, Lcom/google/k/f/ak;->b:Ljava/lang/String;

    if-nez v1, :cond_8

    .line 1199
    iget-object v1, p1, Lcom/google/k/f/ak;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1205
    :cond_2
    iget v1, p0, Lcom/google/k/f/ak;->c:I

    iget v2, p1, Lcom/google/k/f/ak;->c:I

    if-ne v1, v2, :cond_0

    .line 1208
    iget v1, p0, Lcom/google/k/f/ak;->d:I

    iget v2, p1, Lcom/google/k/f/ak;->d:I

    if-ne v1, v2, :cond_0

    .line 1211
    iget-object v1, p0, Lcom/google/k/f/ak;->e:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 1212
    iget-object v1, p1, Lcom/google/k/f/ak;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1218
    :cond_3
    iget-object v1, p0, Lcom/google/k/f/ak;->f:Ljava/lang/String;

    if-nez v1, :cond_a

    .line 1219
    iget-object v1, p1, Lcom/google/k/f/ak;->f:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1225
    :cond_4
    iget-object v1, p0, Lcom/google/k/f/ak;->g:Ljava/lang/String;

    if-nez v1, :cond_b

    .line 1226
    iget-object v1, p1, Lcom/google/k/f/ak;->g:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1232
    :cond_5
    iget-object v1, p0, Lcom/google/k/f/ak;->h:Ljava/lang/String;

    if-nez v1, :cond_c

    .line 1233
    iget-object v1, p1, Lcom/google/k/f/ak;->h:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1239
    :cond_6
    iget-object v1, p0, Lcom/google/k/f/ak;->i:Lcom/google/k/f/al;

    if-nez v1, :cond_d

    .line 1240
    iget-object v1, p1, Lcom/google/k/f/ak;->i:Lcom/google/k/f/al;

    if-nez v1, :cond_0

    .line 1248
    :cond_7
    invoke-virtual {p0, p1}, Lcom/google/k/f/ak;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1202
    :cond_8
    iget-object v1, p0, Lcom/google/k/f/ak;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/k/f/ak;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 1215
    :cond_9
    iget-object v1, p0, Lcom/google/k/f/ak;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/k/f/ak;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 1222
    :cond_a
    iget-object v1, p0, Lcom/google/k/f/ak;->f:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/k/f/ak;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 1229
    :cond_b
    iget-object v1, p0, Lcom/google/k/f/ak;->g:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/k/f/ak;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 1236
    :cond_c
    iget-object v1, p0, Lcom/google/k/f/ak;->h:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/k/f/ak;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 1244
    :cond_d
    iget-object v1, p0, Lcom/google/k/f/ak;->i:Lcom/google/k/f/al;

    iget-object v2, p1, Lcom/google/k/f/ak;->i:Lcom/google/k/f/al;

    invoke-virtual {v1, v2}, Lcom/google/k/f/al;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1253
    iget-wide v2, p0, Lcom/google/k/f/ak;->a:J

    iget-wide v4, p0, Lcom/google/k/f/ak;->a:J

    const/16 v0, 0x20

    ushr-long/2addr v4, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/lit16 v0, v0, 0x20f

    .line 1256
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/ak;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 1258
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/k/f/ak;->c:I

    add-int/2addr v0, v2

    .line 1259
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/k/f/ak;->d:I

    add-int/2addr v0, v2

    .line 1260
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/ak;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1262
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/ak;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1264
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/ak;->g:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 1266
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/ak;->h:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 1268
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/k/f/ak;->i:Lcom/google/k/f/al;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 1270
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/k/f/ak;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1271
    return v0

    .line 1256
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/ak;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1260
    :cond_1
    iget-object v0, p0, Lcom/google/k/f/ak;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1262
    :cond_2
    iget-object v0, p0, Lcom/google/k/f/ak;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1264
    :cond_3
    iget-object v0, p0, Lcom/google/k/f/ak;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 1266
    :cond_4
    iget-object v0, p0, Lcom/google/k/f/ak;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 1268
    :cond_5
    iget-object v1, p0, Lcom/google/k/f/ak;->i:Lcom/google/k/f/al;

    invoke-virtual {v1}, Lcom/google/k/f/al;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 1115
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/k/f/ak;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/k/f/ak;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/ak;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/ak;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/k/f/ak;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/ak;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/ak;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/ak;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/ak;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/k/f/ak;->i:Lcom/google/k/f/al;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/k/f/al;

    invoke-direct {v0}, Lcom/google/k/f/al;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/ak;->i:Lcom/google/k/f/al;

    :cond_1
    iget-object v0, p0, Lcom/google/k/f/ak;->i:Lcom/google/k/f/al;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 1277
    iget-wide v0, p0, Lcom/google/k/f/ak;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1278
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/k/f/ak;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 1280
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/ak;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1281
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/k/f/ak;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1283
    :cond_1
    iget v0, p0, Lcom/google/k/f/ak;->c:I

    if-eqz v0, :cond_2

    .line 1284
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/k/f/ak;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1286
    :cond_2
    iget v0, p0, Lcom/google/k/f/ak;->d:I

    if-eqz v0, :cond_3

    .line 1287
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/k/f/ak;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1289
    :cond_3
    iget-object v0, p0, Lcom/google/k/f/ak;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1290
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/k/f/ak;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1292
    :cond_4
    iget-object v0, p0, Lcom/google/k/f/ak;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1293
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/k/f/ak;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1295
    :cond_5
    iget-object v0, p0, Lcom/google/k/f/ak;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1296
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/k/f/ak;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1298
    :cond_6
    iget-object v0, p0, Lcom/google/k/f/ak;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1299
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/k/f/ak;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1301
    :cond_7
    iget-object v0, p0, Lcom/google/k/f/ak;->i:Lcom/google/k/f/al;

    if-eqz v0, :cond_8

    .line 1302
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/k/f/ak;->i:Lcom/google/k/f/al;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1304
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1305
    return-void
.end method
