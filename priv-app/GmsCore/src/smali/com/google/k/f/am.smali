.class public final Lcom/google/k/f/am;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/k/f/am;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 852
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 853
    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/am;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/am;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/k/f/am;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/k/f/am;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/am;->cachedSize:I

    .line 854
    return-void
.end method

.method public static a()[Lcom/google/k/f/am;
    .locals 2

    .prologue
    .line 832
    sget-object v0, Lcom/google/k/f/am;->d:[Lcom/google/k/f/am;

    if-nez v0, :cond_1

    .line 833
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 835
    :try_start_0
    sget-object v0, Lcom/google/k/f/am;->d:[Lcom/google/k/f/am;

    if-nez v0, :cond_0

    .line 836
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/k/f/am;

    sput-object v0, Lcom/google/k/f/am;->d:[Lcom/google/k/f/am;

    .line 838
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 840
    :cond_1
    sget-object v0, Lcom/google/k/f/am;->d:[Lcom/google/k/f/am;

    return-object v0

    .line 838
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 923
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 924
    iget-object v1, p0, Lcom/google/k/f/am;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 925
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/k/f/am;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 928
    :cond_0
    iget-object v1, p0, Lcom/google/k/f/am;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 929
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/k/f/am;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 932
    :cond_1
    iget v1, p0, Lcom/google/k/f/am;->c:I

    if-eqz v1, :cond_2

    .line 933
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/k/f/am;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 936
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 867
    if-ne p1, p0, :cond_1

    .line 868
    const/4 v0, 0x1

    .line 891
    :cond_0
    :goto_0
    return v0

    .line 870
    :cond_1
    instance-of v1, p1, Lcom/google/k/f/am;

    if-eqz v1, :cond_0

    .line 873
    check-cast p1, Lcom/google/k/f/am;

    .line 874
    iget-object v1, p0, Lcom/google/k/f/am;->a:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 875
    iget-object v1, p1, Lcom/google/k/f/am;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 881
    :cond_2
    iget-object v1, p0, Lcom/google/k/f/am;->b:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 882
    iget-object v1, p1, Lcom/google/k/f/am;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 888
    :cond_3
    iget v1, p0, Lcom/google/k/f/am;->c:I

    iget v2, p1, Lcom/google/k/f/am;->c:I

    if-ne v1, v2, :cond_0

    .line 891
    invoke-virtual {p0, p1}, Lcom/google/k/f/am;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 878
    :cond_4
    iget-object v1, p0, Lcom/google/k/f/am;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/k/f/am;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 885
    :cond_5
    iget-object v1, p0, Lcom/google/k/f/am;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/k/f/am;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 896
    iget-object v0, p0, Lcom/google/k/f/am;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 899
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/k/f/am;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 901
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/am;->c:I

    add-int/2addr v0, v1

    .line 902
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/k/f/am;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 903
    return v0

    .line 896
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/am;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 899
    :cond_1
    iget-object v1, p0, Lcom/google/k/f/am;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 826
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/k/f/am;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/am;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/am;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/am;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 909
    iget-object v0, p0, Lcom/google/k/f/am;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 910
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/k/f/am;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 912
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/am;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 913
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/k/f/am;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 915
    :cond_1
    iget v0, p0, Lcom/google/k/f/am;->c:I

    if-eqz v0, :cond_2

    .line 916
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/k/f/am;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 918
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 919
    return-void
.end method
