.class public final Lcom/google/k/f/ar;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 706
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 707
    iput v0, p0, Lcom/google/k/f/ar;->a:I

    iput v0, p0, Lcom/google/k/f/ar;->b:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/k/f/ar;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/ar;->cachedSize:I

    .line 708
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 759
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 760
    iget v1, p0, Lcom/google/k/f/ar;->a:I

    if-eqz v1, :cond_0

    .line 761
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/k/f/ar;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 764
    :cond_0
    iget v1, p0, Lcom/google/k/f/ar;->b:I

    if-eqz v1, :cond_1

    .line 765
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/k/f/ar;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 768
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 720
    if-ne p1, p0, :cond_1

    .line 721
    const/4 v0, 0x1

    .line 733
    :cond_0
    :goto_0
    return v0

    .line 723
    :cond_1
    instance-of v1, p1, Lcom/google/k/f/ar;

    if-eqz v1, :cond_0

    .line 726
    check-cast p1, Lcom/google/k/f/ar;

    .line 727
    iget v1, p0, Lcom/google/k/f/ar;->a:I

    iget v2, p1, Lcom/google/k/f/ar;->a:I

    if-ne v1, v2, :cond_0

    .line 730
    iget v1, p0, Lcom/google/k/f/ar;->b:I

    iget v2, p1, Lcom/google/k/f/ar;->b:I

    if-ne v1, v2, :cond_0

    .line 733
    invoke-virtual {p0, p1}, Lcom/google/k/f/ar;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 738
    iget v0, p0, Lcom/google/k/f/ar;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 740
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/ar;->b:I

    add-int/2addr v0, v1

    .line 741
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/k/f/ar;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 742
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 670
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/k/f/ar;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/k/f/ar;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/ar;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 748
    iget v0, p0, Lcom/google/k/f/ar;->a:I

    if-eqz v0, :cond_0

    .line 749
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/k/f/ar;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 751
    :cond_0
    iget v0, p0, Lcom/google/k/f/ar;->b:I

    if-eqz v0, :cond_1

    .line 752
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/k/f/ar;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 754
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 755
    return-void
.end method
