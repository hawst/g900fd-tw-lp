.class public final Lcom/google/k/f/c;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/k/f/e;

.field public b:Lcom/google/k/f/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 473
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 474
    iput-object v0, p0, Lcom/google/k/f/c;->a:Lcom/google/k/f/e;

    iput-object v0, p0, Lcom/google/k/f/c;->b:Lcom/google/k/f/d;

    iput-object v0, p0, Lcom/google/k/f/c;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/c;->cachedSize:I

    .line 475
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 540
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 541
    iget-object v1, p0, Lcom/google/k/f/c;->a:Lcom/google/k/f/e;

    if-eqz v1, :cond_0

    .line 542
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/k/f/c;->a:Lcom/google/k/f/e;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 545
    :cond_0
    iget-object v1, p0, Lcom/google/k/f/c;->b:Lcom/google/k/f/d;

    if-eqz v1, :cond_1

    .line 546
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/k/f/c;->b:Lcom/google/k/f/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 549
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 487
    if-ne p1, p0, :cond_1

    .line 488
    const/4 v0, 0x1

    .line 512
    :cond_0
    :goto_0
    return v0

    .line 490
    :cond_1
    instance-of v1, p1, Lcom/google/k/f/c;

    if-eqz v1, :cond_0

    .line 493
    check-cast p1, Lcom/google/k/f/c;

    .line 494
    iget-object v1, p0, Lcom/google/k/f/c;->a:Lcom/google/k/f/e;

    if-nez v1, :cond_4

    .line 495
    iget-object v1, p1, Lcom/google/k/f/c;->a:Lcom/google/k/f/e;

    if-nez v1, :cond_0

    .line 503
    :cond_2
    iget-object v1, p0, Lcom/google/k/f/c;->b:Lcom/google/k/f/d;

    if-nez v1, :cond_5

    .line 504
    iget-object v1, p1, Lcom/google/k/f/c;->b:Lcom/google/k/f/d;

    if-nez v1, :cond_0

    .line 512
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/k/f/c;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 499
    :cond_4
    iget-object v1, p0, Lcom/google/k/f/c;->a:Lcom/google/k/f/e;

    iget-object v2, p1, Lcom/google/k/f/c;->a:Lcom/google/k/f/e;

    invoke-virtual {v1, v2}, Lcom/google/k/f/e;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 508
    :cond_5
    iget-object v1, p0, Lcom/google/k/f/c;->b:Lcom/google/k/f/d;

    iget-object v2, p1, Lcom/google/k/f/c;->b:Lcom/google/k/f/d;

    invoke-virtual {v1, v2}, Lcom/google/k/f/d;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 517
    iget-object v0, p0, Lcom/google/k/f/c;->a:Lcom/google/k/f/e;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 520
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/k/f/c;->b:Lcom/google/k/f/d;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 522
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/k/f/c;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 523
    return v0

    .line 517
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/c;->a:Lcom/google/k/f/e;

    invoke-virtual {v0}, Lcom/google/k/f/e;->hashCode()I

    move-result v0

    goto :goto_0

    .line 520
    :cond_1
    iget-object v1, p0, Lcom/google/k/f/c;->b:Lcom/google/k/f/d;

    invoke-virtual {v1}, Lcom/google/k/f/d;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/k/f/c;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/k/f/c;->a:Lcom/google/k/f/e;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/k/f/e;

    invoke-direct {v0}, Lcom/google/k/f/e;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/c;->a:Lcom/google/k/f/e;

    :cond_1
    iget-object v0, p0, Lcom/google/k/f/c;->a:Lcom/google/k/f/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/k/f/c;->b:Lcom/google/k/f/d;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/k/f/d;

    invoke-direct {v0}, Lcom/google/k/f/d;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/c;->b:Lcom/google/k/f/d;

    :cond_2
    iget-object v0, p0, Lcom/google/k/f/c;->b:Lcom/google/k/f/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 529
    iget-object v0, p0, Lcom/google/k/f/c;->a:Lcom/google/k/f/e;

    if-eqz v0, :cond_0

    .line 530
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/k/f/c;->a:Lcom/google/k/f/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 532
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/c;->b:Lcom/google/k/f/d;

    if-eqz v0, :cond_1

    .line 533
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/k/f/c;->b:Lcom/google/k/f/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 535
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 536
    return-void
.end method
