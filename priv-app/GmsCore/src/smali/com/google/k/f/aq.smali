.class public final Lcom/google/k/f/aq;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:J

.field public c:J

.field public d:J

.field public e:Z

.field public f:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 164
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 165
    iput v0, p0, Lcom/google/k/f/aq;->a:I

    iput-wide v2, p0, Lcom/google/k/f/aq;->b:J

    iput-wide v2, p0, Lcom/google/k/f/aq;->c:J

    iput-wide v2, p0, Lcom/google/k/f/aq;->d:J

    iput-boolean v0, p0, Lcom/google/k/f/aq;->e:Z

    iput v0, p0, Lcom/google/k/f/aq;->f:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/k/f/aq;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/aq;->cachedSize:I

    .line 166
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 252
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 253
    iget v1, p0, Lcom/google/k/f/aq;->a:I

    if-eqz v1, :cond_0

    .line 254
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/k/f/aq;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 257
    :cond_0
    iget-wide v2, p0, Lcom/google/k/f/aq;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 258
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/k/f/aq;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 261
    :cond_1
    iget-wide v2, p0, Lcom/google/k/f/aq;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 262
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/k/f/aq;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 265
    :cond_2
    iget-wide v2, p0, Lcom/google/k/f/aq;->d:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 266
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/k/f/aq;->d:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 269
    :cond_3
    iget-boolean v1, p0, Lcom/google/k/f/aq;->e:Z

    if-eqz v1, :cond_4

    .line 270
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/k/f/aq;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 273
    :cond_4
    iget v1, p0, Lcom/google/k/f/aq;->f:I

    if-eqz v1, :cond_5

    .line 274
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/k/f/aq;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 277
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 182
    if-ne p1, p0, :cond_1

    .line 183
    const/4 v0, 0x1

    .line 207
    :cond_0
    :goto_0
    return v0

    .line 185
    :cond_1
    instance-of v1, p1, Lcom/google/k/f/aq;

    if-eqz v1, :cond_0

    .line 188
    check-cast p1, Lcom/google/k/f/aq;

    .line 189
    iget v1, p0, Lcom/google/k/f/aq;->a:I

    iget v2, p1, Lcom/google/k/f/aq;->a:I

    if-ne v1, v2, :cond_0

    .line 192
    iget-wide v2, p0, Lcom/google/k/f/aq;->b:J

    iget-wide v4, p1, Lcom/google/k/f/aq;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 195
    iget-wide v2, p0, Lcom/google/k/f/aq;->c:J

    iget-wide v4, p1, Lcom/google/k/f/aq;->c:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 198
    iget-wide v2, p0, Lcom/google/k/f/aq;->d:J

    iget-wide v4, p1, Lcom/google/k/f/aq;->d:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 201
    iget-boolean v1, p0, Lcom/google/k/f/aq;->e:Z

    iget-boolean v2, p1, Lcom/google/k/f/aq;->e:Z

    if-ne v1, v2, :cond_0

    .line 204
    iget v1, p0, Lcom/google/k/f/aq;->f:I

    iget v2, p1, Lcom/google/k/f/aq;->f:I

    if-ne v1, v2, :cond_0

    .line 207
    invoke-virtual {p0, p1}, Lcom/google/k/f/aq;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 212
    iget v0, p0, Lcom/google/k/f/aq;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 214
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/k/f/aq;->b:J

    iget-wide v4, p0, Lcom/google/k/f/aq;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 216
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/k/f/aq;->c:J

    iget-wide v4, p0, Lcom/google/k/f/aq;->c:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 218
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/k/f/aq;->d:J

    iget-wide v4, p0, Lcom/google/k/f/aq;->d:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 220
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/aq;->e:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    .line 221
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/aq;->f:I

    add-int/2addr v0, v1

    .line 222
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/k/f/aq;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 223
    return v0

    .line 220
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 121
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/k/f/aq;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/k/f/aq;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/k/f/aq;->b:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/k/f/aq;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/k/f/aq;->d:J

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/aq;->e:Z

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/aq;->f:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 229
    iget v0, p0, Lcom/google/k/f/aq;->a:I

    if-eqz v0, :cond_0

    .line 230
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/k/f/aq;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 232
    :cond_0
    iget-wide v0, p0, Lcom/google/k/f/aq;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 233
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/k/f/aq;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 235
    :cond_1
    iget-wide v0, p0, Lcom/google/k/f/aq;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 236
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/k/f/aq;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 238
    :cond_2
    iget-wide v0, p0, Lcom/google/k/f/aq;->d:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 239
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/k/f/aq;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 241
    :cond_3
    iget-boolean v0, p0, Lcom/google/k/f/aq;->e:Z

    if-eqz v0, :cond_4

    .line 242
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/k/f/aq;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 244
    :cond_4
    iget v0, p0, Lcom/google/k/f/aq;->f:I

    if-eqz v0, :cond_5

    .line 245
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/k/f/aq;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 247
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 248
    return-void
.end method
