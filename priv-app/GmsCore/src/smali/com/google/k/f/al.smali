.class public final Lcom/google/k/f/al;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/k/f/am;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 999
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1000
    invoke-static {}, Lcom/google/k/f/am;->a()[Lcom/google/k/f/am;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/al;->a:[Lcom/google/k/f/am;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/k/f/al;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/al;->cachedSize:I

    .line 1001
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 1051
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v1

    .line 1052
    iget-object v0, p0, Lcom/google/k/f/al;->a:[Lcom/google/k/f/am;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/k/f/al;->a:[Lcom/google/k/f/am;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1053
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/k/f/al;->a:[Lcom/google/k/f/am;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1054
    iget-object v2, p0, Lcom/google/k/f/al;->a:[Lcom/google/k/f/am;

    aget-object v2, v2, v0

    .line 1055
    if-eqz v2, :cond_0

    .line 1056
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1053
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1061
    :cond_1
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1012
    if-ne p1, p0, :cond_1

    .line 1013
    const/4 v0, 0x1

    .line 1023
    :cond_0
    :goto_0
    return v0

    .line 1015
    :cond_1
    instance-of v1, p1, Lcom/google/k/f/al;

    if-eqz v1, :cond_0

    .line 1018
    check-cast p1, Lcom/google/k/f/al;

    .line 1019
    iget-object v1, p0, Lcom/google/k/f/al;->a:[Lcom/google/k/f/am;

    iget-object v2, p1, Lcom/google/k/f/al;->a:[Lcom/google/k/f/am;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1023
    invoke-virtual {p0, p1}, Lcom/google/k/f/al;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1028
    iget-object v0, p0, Lcom/google/k/f/al;->a:[Lcom/google/k/f/am;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1031
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/k/f/al;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1032
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 823
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/k/f/al;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/k/f/al;->a:[Lcom/google/k/f/am;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/k/f/am;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/k/f/al;->a:[Lcom/google/k/f/am;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/k/f/am;

    invoke-direct {v3}, Lcom/google/k/f/am;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/k/f/al;->a:[Lcom/google/k/f/am;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/k/f/am;

    invoke-direct {v3}, Lcom/google/k/f/am;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/k/f/al;->a:[Lcom/google/k/f/am;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1038
    iget-object v0, p0, Lcom/google/k/f/al;->a:[Lcom/google/k/f/am;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/k/f/al;->a:[Lcom/google/k/f/am;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1039
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/k/f/al;->a:[Lcom/google/k/f/am;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1040
    iget-object v1, p0, Lcom/google/k/f/al;->a:[Lcom/google/k/f/am;

    aget-object v1, v1, v0

    .line 1041
    if-eqz v1, :cond_0

    .line 1042
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1039
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1046
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1047
    return-void
.end method
