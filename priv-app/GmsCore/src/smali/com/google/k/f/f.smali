.class public final Lcom/google/k/f/f;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 617
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 618
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/k/f/f;->a:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/k/f/f;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/f;->cachedSize:I

    .line 619
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 663
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 664
    iget-wide v2, p0, Lcom/google/k/f/f;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 665
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/k/f/f;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 668
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 630
    if-ne p1, p0, :cond_1

    .line 631
    const/4 v0, 0x1

    .line 640
    :cond_0
    :goto_0
    return v0

    .line 633
    :cond_1
    instance-of v1, p1, Lcom/google/k/f/f;

    if-eqz v1, :cond_0

    .line 636
    check-cast p1, Lcom/google/k/f/f;

    .line 637
    iget-wide v2, p0, Lcom/google/k/f/f;->a:J

    iget-wide v4, p1, Lcom/google/k/f/f;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 640
    invoke-virtual {p0, p1}, Lcom/google/k/f/f;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    .line 645
    iget-wide v0, p0, Lcom/google/k/f/f;->a:J

    iget-wide v2, p0, Lcom/google/k/f/f;->a:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 648
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/k/f/f;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 649
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 597
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/k/f/f;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/k/f/f;->a:J

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 655
    iget-wide v0, p0, Lcom/google/k/f/f;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 656
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/k/f/f;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 658
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 659
    return-void
.end method
