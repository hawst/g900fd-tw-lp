.class public final Lcom/google/k/f/h;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/k/f/g;

.field public b:Lcom/google/k/f/g;

.field public c:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 884
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 885
    iput-object v2, p0, Lcom/google/k/f/h;->a:Lcom/google/k/f/g;

    iput-object v2, p0, Lcom/google/k/f/h;->b:Lcom/google/k/f/g;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/k/f/h;->c:J

    iput-object v2, p0, Lcom/google/k/f/h;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/h;->cachedSize:I

    .line 886
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 960
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 961
    iget-object v1, p0, Lcom/google/k/f/h;->a:Lcom/google/k/f/g;

    if-eqz v1, :cond_0

    .line 962
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/k/f/h;->a:Lcom/google/k/f/g;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 965
    :cond_0
    iget-object v1, p0, Lcom/google/k/f/h;->b:Lcom/google/k/f/g;

    if-eqz v1, :cond_1

    .line 966
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/k/f/h;->b:Lcom/google/k/f/g;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 969
    :cond_1
    iget-wide v2, p0, Lcom/google/k/f/h;->c:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 970
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/k/f/h;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 973
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 899
    if-ne p1, p0, :cond_1

    .line 900
    const/4 v0, 0x1

    .line 927
    :cond_0
    :goto_0
    return v0

    .line 902
    :cond_1
    instance-of v1, p1, Lcom/google/k/f/h;

    if-eqz v1, :cond_0

    .line 905
    check-cast p1, Lcom/google/k/f/h;

    .line 906
    iget-object v1, p0, Lcom/google/k/f/h;->a:Lcom/google/k/f/g;

    if-nez v1, :cond_4

    .line 907
    iget-object v1, p1, Lcom/google/k/f/h;->a:Lcom/google/k/f/g;

    if-nez v1, :cond_0

    .line 915
    :cond_2
    iget-object v1, p0, Lcom/google/k/f/h;->b:Lcom/google/k/f/g;

    if-nez v1, :cond_5

    .line 916
    iget-object v1, p1, Lcom/google/k/f/h;->b:Lcom/google/k/f/g;

    if-nez v1, :cond_0

    .line 924
    :cond_3
    iget-wide v2, p0, Lcom/google/k/f/h;->c:J

    iget-wide v4, p1, Lcom/google/k/f/h;->c:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 927
    invoke-virtual {p0, p1}, Lcom/google/k/f/h;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 911
    :cond_4
    iget-object v1, p0, Lcom/google/k/f/h;->a:Lcom/google/k/f/g;

    iget-object v2, p1, Lcom/google/k/f/h;->a:Lcom/google/k/f/g;

    invoke-virtual {v1, v2}, Lcom/google/k/f/g;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 920
    :cond_5
    iget-object v1, p0, Lcom/google/k/f/h;->b:Lcom/google/k/f/g;

    iget-object v2, p1, Lcom/google/k/f/h;->b:Lcom/google/k/f/g;

    invoke-virtual {v1, v2}, Lcom/google/k/f/g;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 932
    iget-object v0, p0, Lcom/google/k/f/h;->a:Lcom/google/k/f/g;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 935
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/k/f/h;->b:Lcom/google/k/f/g;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 937
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/k/f/h;->c:J

    iget-wide v4, p0, Lcom/google/k/f/h;->c:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 939
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/k/f/h;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 940
    return v0

    .line 932
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/h;->a:Lcom/google/k/f/g;

    invoke-virtual {v0}, Lcom/google/k/f/g;->hashCode()I

    move-result v0

    goto :goto_0

    .line 935
    :cond_1
    iget-object v1, p0, Lcom/google/k/f/h;->b:Lcom/google/k/f/g;

    invoke-virtual {v1}, Lcom/google/k/f/g;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 858
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/k/f/h;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/k/f/h;->a:Lcom/google/k/f/g;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/k/f/g;

    invoke-direct {v0}, Lcom/google/k/f/g;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/h;->a:Lcom/google/k/f/g;

    :cond_1
    iget-object v0, p0, Lcom/google/k/f/h;->a:Lcom/google/k/f/g;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/k/f/h;->b:Lcom/google/k/f/g;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/k/f/g;

    invoke-direct {v0}, Lcom/google/k/f/g;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/h;->b:Lcom/google/k/f/g;

    :cond_2
    iget-object v0, p0, Lcom/google/k/f/h;->b:Lcom/google/k/f/g;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/k/f/h;->c:J

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 946
    iget-object v0, p0, Lcom/google/k/f/h;->a:Lcom/google/k/f/g;

    if-eqz v0, :cond_0

    .line 947
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/k/f/h;->a:Lcom/google/k/f/g;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 949
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/h;->b:Lcom/google/k/f/g;

    if-eqz v0, :cond_1

    .line 950
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/k/f/h;->b:Lcom/google/k/f/g;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 952
    :cond_1
    iget-wide v0, p0, Lcom/google/k/f/h;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 953
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/k/f/h;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 955
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 956
    return-void
.end method
