.class public final Lcom/google/k/a/ae;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lcom/google/k/a/af;

.field private c:Lcom/google/k/a/af;

.field private d:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 200
    new-instance v0, Lcom/google/k/a/af;

    invoke-direct {v0, v1}, Lcom/google/k/a/af;-><init>(B)V

    iput-object v0, p0, Lcom/google/k/a/ae;->b:Lcom/google/k/a/af;

    .line 201
    iget-object v0, p0, Lcom/google/k/a/ae;->b:Lcom/google/k/a/af;

    iput-object v0, p0, Lcom/google/k/a/ae;->c:Lcom/google/k/a/af;

    .line 202
    iput-boolean v1, p0, Lcom/google/k/a/ae;->d:Z

    .line 208
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/k/a/ae;->a:Ljava/lang/String;

    .line 209
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 198
    invoke-direct {p0, p1}, Lcom/google/k/a/ae;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/k/a/ae;
    .locals 2

    .prologue
    .line 230
    new-instance v1, Lcom/google/k/a/af;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lcom/google/k/a/af;-><init>(B)V

    iget-object v0, p0, Lcom/google/k/a/ae;->c:Lcom/google/k/a/af;

    iput-object v1, v0, Lcom/google/k/a/af;->c:Lcom/google/k/a/af;

    iput-object v1, p0, Lcom/google/k/a/ae;->c:Lcom/google/k/a/af;

    iput-object p2, v1, Lcom/google/k/a/af;->b:Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/google/k/a/af;->a:Ljava/lang/String;

    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 387
    iget-boolean v2, p0, Lcom/google/k/a/ae;->d:Z

    .line 388
    const-string v1, ""

    .line 389
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    iget-object v3, p0, Lcom/google/k/a/ae;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0x7b

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 391
    iget-object v0, p0, Lcom/google/k/a/ae;->b:Lcom/google/k/a/af;

    iget-object v0, v0, Lcom/google/k/a/af;->c:Lcom/google/k/a/af;

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_3

    .line 393
    if-eqz v2, :cond_0

    iget-object v4, v1, Lcom/google/k/a/af;->b:Ljava/lang/Object;

    if-eqz v4, :cond_2

    .line 394
    :cond_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395
    const-string v0, ", "

    .line 397
    iget-object v4, v1, Lcom/google/k/a/af;->a:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 398
    iget-object v4, v1, Lcom/google/k/a/af;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x3d

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 400
    :cond_1
    iget-object v4, v1, Lcom/google/k/a/af;->b:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 392
    :cond_2
    iget-object v1, v1, Lcom/google/k/a/af;->c:Lcom/google/k/a/af;

    goto :goto_0

    .line 403
    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
