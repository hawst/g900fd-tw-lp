.class abstract enum Lcom/google/k/a/an;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/k/a/ai;


# static fields
.field public static final enum a:Lcom/google/k/a/an;

.field public static final enum b:Lcom/google/k/a/an;

.field public static final enum c:Lcom/google/k/a/an;

.field public static final enum d:Lcom/google/k/a/an;

.field private static final synthetic e:[Lcom/google/k/a/an;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 277
    new-instance v0, Lcom/google/k/a/ao;

    const-string v1, "ALWAYS_TRUE"

    invoke-direct {v0, v1}, Lcom/google/k/a/ao;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/k/a/an;->a:Lcom/google/k/a/an;

    .line 286
    new-instance v0, Lcom/google/k/a/ap;

    const-string v1, "ALWAYS_FALSE"

    invoke-direct {v0, v1}, Lcom/google/k/a/ap;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/k/a/an;->b:Lcom/google/k/a/an;

    .line 295
    new-instance v0, Lcom/google/k/a/aq;

    const-string v1, "IS_NULL"

    invoke-direct {v0, v1}, Lcom/google/k/a/aq;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/k/a/an;->c:Lcom/google/k/a/an;

    .line 304
    new-instance v0, Lcom/google/k/a/ar;

    const-string v1, "NOT_NULL"

    invoke-direct {v0, v1}, Lcom/google/k/a/ar;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/k/a/an;->d:Lcom/google/k/a/an;

    .line 275
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/k/a/an;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/k/a/an;->a:Lcom/google/k/a/an;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/k/a/an;->b:Lcom/google/k/a/an;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/k/a/an;->c:Lcom/google/k/a/an;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/k/a/an;->d:Lcom/google/k/a/an;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/k/a/an;->e:[Lcom/google/k/a/an;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 275
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 275
    invoke-direct {p0, p1, p2}, Lcom/google/k/a/an;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/k/a/an;
    .locals 1

    .prologue
    .line 275
    const-class v0, Lcom/google/k/a/an;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/k/a/an;

    return-object v0
.end method

.method public static values()[Lcom/google/k/a/an;
    .locals 1

    .prologue
    .line 275
    sget-object v0, Lcom/google/k/a/an;->e:[Lcom/google/k/a/an;

    invoke-virtual {v0}, [Lcom/google/k/a/an;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/k/a/an;

    return-object v0
.end method


# virtual methods
.method final a()Lcom/google/k/a/ai;
    .locals 0

    .prologue
    .line 315
    return-object p0
.end method
