.class final Lcom/google/k/a/r;
.super Lcom/google/k/a/e;
.source "SourceFile"


# instance fields
.field final q:Lcom/google/k/a/e;

.field final r:Lcom/google/k/a/e;


# direct methods
.method constructor <init>(Lcom/google/k/a/e;Lcom/google/k/a/e;)V
    .locals 2

    .prologue
    .line 734
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CharMatcher.or("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/k/a/r;-><init>(Lcom/google/k/a/e;Lcom/google/k/a/e;Ljava/lang/String;)V

    .line 735
    return-void
.end method

.method private constructor <init>(Lcom/google/k/a/e;Lcom/google/k/a/e;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 728
    invoke-direct {p0, p3}, Lcom/google/k/a/e;-><init>(Ljava/lang/String;)V

    .line 729
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/a/e;

    iput-object v0, p0, Lcom/google/k/a/r;->q:Lcom/google/k/a/e;

    .line 730
    invoke-static {p2}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/a/e;

    iput-object v0, p0, Lcom/google/k/a/r;->r:Lcom/google/k/a/e;

    .line 731
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;)Lcom/google/k/a/e;
    .locals 3

    .prologue
    .line 751
    new-instance v0, Lcom/google/k/a/r;

    iget-object v1, p0, Lcom/google/k/a/r;->q:Lcom/google/k/a/e;

    iget-object v2, p0, Lcom/google/k/a/r;->r:Lcom/google/k/a/e;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/k/a/r;-><init>(Lcom/google/k/a/e;Lcom/google/k/a/e;Ljava/lang/String;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 723
    check-cast p1, Ljava/lang/Character;

    invoke-super {p0, p1}, Lcom/google/k/a/e;->a(Ljava/lang/Character;)Z

    move-result v0

    return v0
.end method

.method public final b(C)Z
    .locals 1

    .prologue
    .line 746
    iget-object v0, p0, Lcom/google/k/a/r;->q:Lcom/google/k/a/e;

    invoke-virtual {v0, p1}, Lcom/google/k/a/e;->b(C)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/k/a/r;->r:Lcom/google/k/a/e;

    invoke-virtual {v0, p1}, Lcom/google/k/a/e;->b(C)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
