.class public final Lcom/google/k/a/at;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/k/a/e;

.field public final b:Z

.field public final c:Lcom/google/k/a/bb;

.field public final d:I


# direct methods
.method private constructor <init>(Lcom/google/k/a/bb;)V
    .locals 3

    .prologue
    .line 110
    const/4 v0, 0x0

    sget-object v1, Lcom/google/k/a/e;->m:Lcom/google/k/a/e;

    const v2, 0x7fffffff

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/k/a/at;-><init>(Lcom/google/k/a/bb;ZLcom/google/k/a/e;I)V

    .line 111
    return-void
.end method

.method public constructor <init>(Lcom/google/k/a/bb;ZLcom/google/k/a/e;I)V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    iput-object p1, p0, Lcom/google/k/a/at;->c:Lcom/google/k/a/bb;

    .line 116
    iput-boolean p2, p0, Lcom/google/k/a/at;->b:Z

    .line 117
    iput-object p3, p0, Lcom/google/k/a/at;->a:Lcom/google/k/a/e;

    .line 118
    iput p4, p0, Lcom/google/k/a/at;->d:I

    .line 119
    return-void
.end method

.method public static a(C)Lcom/google/k/a/at;
    .locals 3

    .prologue
    .line 130
    invoke-static {p0}, Lcom/google/k/a/e;->a(C)Lcom/google/k/a/e;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/k/a/at;

    new-instance v2, Lcom/google/k/a/au;

    invoke-direct {v2, v0}, Lcom/google/k/a/au;-><init>(Lcom/google/k/a/e;)V

    invoke-direct {v1, v2}, Lcom/google/k/a/at;-><init>(Lcom/google/k/a/bb;)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;)Lcom/google/k/a/at;
    .locals 2

    .prologue
    .line 171
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The separator may not be the empty string."

    invoke-static {v0, v1}, Lcom/google/k/a/ah;->a(ZLjava/lang/Object;)V

    .line 174
    new-instance v0, Lcom/google/k/a/at;

    new-instance v1, Lcom/google/k/a/aw;

    invoke-direct {v1, p0}, Lcom/google/k/a/aw;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/google/k/a/at;-><init>(Lcom/google/k/a/bb;)V

    return-object v0

    .line 171
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/k/a/at;Ljava/lang/CharSequence;)Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/k/a/at;->c:Lcom/google/k/a/bb;

    invoke-interface {v0, p0, p1}, Lcom/google/k/a/bb;->a(Lcom/google/k/a/at;Ljava/lang/CharSequence;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/k/a/at;
    .locals 5

    .prologue
    .line 316
    new-instance v0, Lcom/google/k/a/at;

    iget-object v1, p0, Lcom/google/k/a/at;->c:Lcom/google/k/a/bb;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/k/a/at;->a:Lcom/google/k/a/e;

    iget v4, p0, Lcom/google/k/a/at;->d:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/k/a/at;-><init>(Lcom/google/k/a/bb;ZLcom/google/k/a/e;I)V

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;
    .locals 1

    .prologue
    .line 386
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    new-instance v0, Lcom/google/k/a/ay;

    invoke-direct {v0, p0, p1}, Lcom/google/k/a/ay;-><init>(Lcom/google/k/a/at;Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/k/a/az;
    .locals 3

    .prologue
    .line 437
    invoke-static {p1}, Lcom/google/k/a/at;->a(Ljava/lang/String;)Lcom/google/k/a/at;

    move-result-object v0

    new-instance v1, Lcom/google/k/a/az;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v0, v2}, Lcom/google/k/a/az;-><init>(Lcom/google/k/a/at;Lcom/google/k/a/at;B)V

    return-object v1
.end method
