.class final Lcom/google/k/k/a/v;
.super Lcom/google/k/k/a/b;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private b:Lcom/google/k/k/a/e;

.field private c:Lcom/google/k/k/a/af;

.field private volatile d:Lcom/google/k/k/a/af;


# direct methods
.method private constructor <init>(Lcom/google/k/k/a/e;Lcom/google/k/k/a/af;)V
    .locals 1

    .prologue
    .line 860
    invoke-direct {p0}, Lcom/google/k/k/a/b;-><init>()V

    .line 861
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/k/a/e;

    iput-object v0, p0, Lcom/google/k/k/a/v;->b:Lcom/google/k/k/a/e;

    .line 862
    invoke-static {p2}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/k/a/af;

    iput-object v0, p0, Lcom/google/k/k/a/v;->c:Lcom/google/k/k/a/af;

    .line 863
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/k/k/a/e;Lcom/google/k/k/a/af;B)V
    .locals 0

    .prologue
    .line 851
    invoke-direct {p0, p1, p2}, Lcom/google/k/k/a/v;-><init>(Lcom/google/k/k/a/e;Lcom/google/k/k/a/af;)V

    return-void
.end method

.method static synthetic a(Lcom/google/k/k/a/v;)Lcom/google/k/k/a/af;
    .locals 1

    .prologue
    .line 851
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/k/k/a/v;->d:Lcom/google/k/k/a/af;

    return-object v0
.end method

.method private static a(Ljava/util/concurrent/Future;Z)V
    .locals 0

    .prologue
    .line 883
    if-eqz p0, :cond_0

    .line 884
    invoke-interface {p0, p1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 886
    :cond_0
    return-void
.end method


# virtual methods
.method public final cancel(Z)Z
    .locals 1

    .prologue
    .line 871
    invoke-super {p0, p1}, Lcom/google/k/k/a/b;->cancel(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 874
    iget-object v0, p0, Lcom/google/k/k/a/v;->c:Lcom/google/k/k/a/af;

    invoke-static {v0, p1}, Lcom/google/k/k/a/v;->a(Ljava/util/concurrent/Future;Z)V

    .line 875
    iget-object v0, p0, Lcom/google/k/k/a/v;->d:Lcom/google/k/k/a/af;

    invoke-static {v0, p1}, Lcom/google/k/k/a/v;->a(Ljava/util/concurrent/Future;Z)V

    .line 876
    const/4 v0, 0x1

    .line 878
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 893
    :try_start_0
    iget-object v0, p0, Lcom/google/k/k/a/v;->c:Lcom/google/k/k/a/af;

    invoke-static {v0}, Lcom/google/k/k/a/as;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 906
    :try_start_1
    iget-object v1, p0, Lcom/google/k/k/a/v;->b:Lcom/google/k/k/a/e;

    invoke-interface {v1, v0}, Lcom/google/k/k/a/e;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v0

    const-string v1, "AsyncFunction may not return null."

    invoke-static {v0, v1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/k/a/af;

    iput-object v0, p0, Lcom/google/k/k/a/v;->d:Lcom/google/k/k/a/af;

    .line 909
    invoke-virtual {p0}, Lcom/google/k/k/a/v;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 910
    iget-object v1, p0, Lcom/google/k/k/a/b;->a:Lcom/google/k/k/a/c;

    invoke-virtual {v1}, Lcom/google/k/k/a/c;->d()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/k/k/a/af;->cancel(Z)Z

    .line 911
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/k/k/a/v;->d:Lcom/google/k/k/a/af;
    :try_end_1
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 943
    iput-object v3, p0, Lcom/google/k/k/a/v;->b:Lcom/google/k/k/a/e;

    .line 944
    iput-object v3, p0, Lcom/google/k/k/a/v;->c:Lcom/google/k/k/a/af;

    .line 945
    :goto_0
    return-void

    .line 898
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    :try_start_2
    invoke-virtual {p0, v0}, Lcom/google/k/k/a/v;->cancel(Z)Z
    :try_end_2
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 943
    iput-object v3, p0, Lcom/google/k/k/a/v;->b:Lcom/google/k/k/a/e;

    .line 944
    iput-object v3, p0, Lcom/google/k/k/a/v;->c:Lcom/google/k/k/a/af;

    goto :goto_0

    .line 900
    :catch_1
    move-exception v0

    .line 902
    :try_start_3
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/k/k/a/v;->a(Ljava/lang/Throwable;)Z
    :try_end_3
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 943
    iput-object v3, p0, Lcom/google/k/k/a/v;->b:Lcom/google/k/k/a/e;

    .line 944
    iput-object v3, p0, Lcom/google/k/k/a/v;->c:Lcom/google/k/k/a/af;

    goto :goto_0

    .line 914
    :cond_0
    :try_start_4
    new-instance v1, Lcom/google/k/k/a/w;

    invoke-direct {v1, p0, v0}, Lcom/google/k/k/a/w;-><init>(Lcom/google/k/k/a/v;Lcom/google/k/k/a/af;)V

    sget-object v2, Lcom/google/k/k/a/al;->a:Lcom/google/k/k/a/al;

    invoke-interface {v0, v1, v2}, Lcom/google/k/k/a/af;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    :try_end_4
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 943
    iput-object v3, p0, Lcom/google/k/k/a/v;->b:Lcom/google/k/k/a/e;

    .line 944
    iput-object v3, p0, Lcom/google/k/k/a/v;->c:Lcom/google/k/k/a/af;

    goto :goto_0

    .line 934
    :catch_2
    move-exception v0

    .line 936
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/reflect/UndeclaredThrowableException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/k/k/a/v;->a(Ljava/lang/Throwable;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 943
    iput-object v3, p0, Lcom/google/k/k/a/v;->b:Lcom/google/k/k/a/e;

    .line 944
    iput-object v3, p0, Lcom/google/k/k/a/v;->c:Lcom/google/k/k/a/af;

    goto :goto_0

    .line 937
    :catch_3
    move-exception v0

    .line 940
    :try_start_6
    invoke-virtual {p0, v0}, Lcom/google/k/k/a/v;->a(Ljava/lang/Throwable;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 943
    iput-object v3, p0, Lcom/google/k/k/a/v;->b:Lcom/google/k/k/a/e;

    .line 944
    iput-object v3, p0, Lcom/google/k/k/a/v;->c:Lcom/google/k/k/a/af;

    goto :goto_0

    .line 943
    :catchall_0
    move-exception v0

    iput-object v3, p0, Lcom/google/k/k/a/v;->b:Lcom/google/k/k/a/e;

    .line 944
    iput-object v3, p0, Lcom/google/k/k/a/v;->c:Lcom/google/k/k/a/af;

    throw v0
.end method
