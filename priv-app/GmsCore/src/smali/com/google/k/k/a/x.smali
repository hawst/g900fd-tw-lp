.class Lcom/google/k/k/a/x;
.super Lcom/google/k/k/a/b;
.source "SourceFile"


# static fields
.field private static final i:Ljava/util/logging/Logger;


# instance fields
.field b:Lcom/google/k/c/bf;

.field final c:Z

.field final d:Ljava/util/concurrent/atomic/AtomicInteger;

.field e:Lcom/google/k/k/a/aa;

.field f:Ljava/util/List;

.field final g:Ljava/lang/Object;

.field h:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1609
    const-class v0, Lcom/google/k/k/a/x;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/k/k/a/x;->i:Ljava/util/logging/Logger;

    return-void
.end method

.method constructor <init>(Lcom/google/k/c/bf;ZLjava/util/concurrent/Executor;Lcom/google/k/k/a/aa;)V
    .locals 2

    .prologue
    .line 1623
    invoke-direct {p0}, Lcom/google/k/k/a/b;-><init>()V

    .line 1617
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/k/k/a/x;->g:Ljava/lang/Object;

    .line 1624
    iput-object p1, p0, Lcom/google/k/k/a/x;->b:Lcom/google/k/c/bf;

    .line 1625
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/k/k/a/x;->c:Z

    .line 1626
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, Lcom/google/k/c/bf;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/k/k/a/x;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 1627
    iput-object p4, p0, Lcom/google/k/k/a/x;->e:Lcom/google/k/k/a/aa;

    .line 1628
    invoke-virtual {p1}, Lcom/google/k/c/bf;->size()I

    move-result v0

    const-string v1, "initialArraySize"

    invoke-static {v0, v1}, Lcom/google/k/c/ao;->a(ILjava/lang/String;)I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/k/k/a/x;->f:Ljava/util/List;

    .line 1629
    invoke-direct {p0, p3}, Lcom/google/k/k/a/x;->a(Ljava/util/concurrent/Executor;)V

    .line 1630
    return-void
.end method

.method static synthetic a(Lcom/google/k/k/a/x;ILjava/util/concurrent/Future;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1608
    iget-object v3, p0, Lcom/google/k/k/a/x;->f:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/k/k/a/x;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez v3, :cond_2

    :cond_0
    iget-boolean v0, p0, Lcom/google/k/k/a/x;->c:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/k/k/a/x;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    move v0, v2

    :goto_0
    const-string v4, "Future was done before all dependencies completed"

    invoke-static {v0, v4}, Lcom/google/k/a/ah;->b(ZLjava/lang/Object;)V

    :cond_2
    :try_start_0
    invoke-interface {p2}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    const-string v4, "Tried to set value from future which is not done"

    invoke-static {v0, v4}, Lcom/google/k/a/ah;->b(ZLjava/lang/Object;)V

    invoke-static {p2}, Lcom/google/k/k/a/as;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v3, :cond_3

    invoke-static {v0}, Lcom/google/k/a/ag;->a(Ljava/lang/Object;)Lcom/google/k/a/ag;

    move-result-object v0

    invoke-interface {v3, p1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    iget-object v0, p0, Lcom/google/k/k/a/x;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-ltz v0, :cond_6

    :goto_1
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v2, v1}, Lcom/google/k/a/ah;->b(ZLjava/lang/Object;)V

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/k/k/a/x;->e:Lcom/google/k/k/a/aa;

    if-eqz v0, :cond_7

    if-eqz v3, :cond_7

    invoke-interface {v0, v3}, Lcom/google/k/k/a/aa;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/k/k/a/x;->a(Ljava/lang/Object;)Z

    :cond_4
    :goto_2
    return-void

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    move v2, v1

    goto :goto_1

    :cond_7
    invoke-virtual {p0}, Lcom/google/k/k/a/x;->isDone()Z

    move-result v0

    invoke-static {v0}, Lcom/google/k/a/ah;->b(Z)V

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/k/k/a/x;->c:Z

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/k/k/a/x;->cancel(Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_8
    iget-object v0, p0, Lcom/google/k/k/a/x;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-ltz v0, :cond_9

    :goto_3
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v2, v1}, Lcom/google/k/a/ah;->b(ZLjava/lang/Object;)V

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/k/k/a/x;->e:Lcom/google/k/k/a/aa;

    if-eqz v0, :cond_a

    if-eqz v3, :cond_a

    invoke-interface {v0, v3}, Lcom/google/k/k/a/aa;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/k/k/a/x;->a(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_9
    move v2, v1

    goto :goto_3

    :cond_a
    invoke-virtual {p0}, Lcom/google/k/k/a/x;->isDone()Z

    move-result v0

    invoke-static {v0}, Lcom/google/k/a/ah;->b(Z)V

    goto :goto_2

    :catch_1
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/k/k/a/x;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lcom/google/k/k/a/x;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-ltz v0, :cond_b

    :goto_4
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v2, v1}, Lcom/google/k/a/ah;->b(ZLjava/lang/Object;)V

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/k/k/a/x;->e:Lcom/google/k/k/a/aa;

    if-eqz v0, :cond_c

    if-eqz v3, :cond_c

    invoke-interface {v0, v3}, Lcom/google/k/k/a/aa;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/k/k/a/x;->a(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_b
    move v2, v1

    goto :goto_4

    :cond_c
    invoke-virtual {p0}, Lcom/google/k/k/a/x;->isDone()Z

    move-result v0

    invoke-static {v0}, Lcom/google/k/a/ah;->b(Z)V

    goto :goto_2

    :catch_2
    move-exception v0

    :try_start_3
    invoke-direct {p0, v0}, Lcom/google/k/k/a/x;->b(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v0, p0, Lcom/google/k/k/a/x;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-ltz v0, :cond_d

    :goto_5
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v2, v1}, Lcom/google/k/a/ah;->b(ZLjava/lang/Object;)V

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/k/k/a/x;->e:Lcom/google/k/k/a/aa;

    if-eqz v0, :cond_e

    if-eqz v3, :cond_e

    invoke-interface {v0, v3}, Lcom/google/k/k/a/aa;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/k/k/a/x;->a(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_d
    move v2, v1

    goto :goto_5

    :cond_e
    invoke-virtual {p0}, Lcom/google/k/k/a/x;->isDone()Z

    move-result v0

    invoke-static {v0}, Lcom/google/k/a/ah;->b(Z)V

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    iget-object v4, p0, Lcom/google/k/k/a/x;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v4

    if-ltz v4, :cond_10

    :goto_6
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v2, v1}, Lcom/google/k/a/ah;->b(ZLjava/lang/Object;)V

    if-nez v4, :cond_f

    iget-object v1, p0, Lcom/google/k/k/a/x;->e:Lcom/google/k/k/a/aa;

    if-eqz v1, :cond_11

    if-eqz v3, :cond_11

    invoke-interface {v1, v3}, Lcom/google/k/k/a/aa;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/k/k/a/x;->a(Ljava/lang/Object;)Z

    :cond_f
    :goto_7
    throw v0

    :cond_10
    move v2, v1

    goto :goto_6

    :cond_11
    invoke-virtual {p0}, Lcom/google/k/k/a/x;->isDone()Z

    move-result v1

    invoke-static {v1}, Lcom/google/k/a/ah;->b(Z)V

    goto :goto_7
.end method

.method private a(Ljava/util/concurrent/Executor;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1637
    new-instance v0, Lcom/google/k/k/a/y;

    invoke-direct {v0, p0}, Lcom/google/k/k/a/y;-><init>(Lcom/google/k/k/a/x;)V

    sget-object v2, Lcom/google/k/k/a/al;->a:Lcom/google/k/k/a/al;

    invoke-virtual {p0, v0, v2}, Lcom/google/k/k/a/x;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 1662
    iget-object v0, p0, Lcom/google/k/k/a/x;->b:Lcom/google/k/c/bf;

    invoke-virtual {v0}, Lcom/google/k/c/bf;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1663
    iget-object v0, p0, Lcom/google/k/k/a/x;->e:Lcom/google/k/k/a/aa;

    invoke-static {}, Lcom/google/k/c/bj;->f()Lcom/google/k/c/bj;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/k/k/a/aa;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/k/k/a/x;->a(Ljava/lang/Object;)Z

    .line 1690
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 1668
    :goto_0
    iget-object v2, p0, Lcom/google/k/k/a/x;->b:Lcom/google/k/c/bf;

    invoke-virtual {v2}, Lcom/google/k/c/bf;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 1669
    iget-object v2, p0, Lcom/google/k/k/a/x;->f:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1681
    :cond_2
    iget-object v0, p0, Lcom/google/k/k/a/x;->b:Lcom/google/k/c/bf;

    invoke-virtual {v0}, Lcom/google/k/c/bf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/k/a/af;

    .line 1682
    add-int/lit8 v2, v1, 0x1

    .line 1683
    new-instance v4, Lcom/google/k/k/a/z;

    invoke-direct {v4, p0, v1, v0}, Lcom/google/k/k/a/z;-><init>(Lcom/google/k/k/a/x;ILcom/google/k/k/a/af;)V

    invoke-interface {v0, v4, p1}, Lcom/google/k/k/a/af;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    move v1, v2

    .line 1689
    goto :goto_1
.end method

.method private b(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1699
    const/4 v1, 0x0

    .line 1700
    const/4 v0, 0x1

    .line 1701
    iget-boolean v2, p0, Lcom/google/k/k/a/x;->c:Z

    if-eqz v2, :cond_1

    .line 1704
    invoke-super {p0, p1}, Lcom/google/k/k/a/b;->a(Ljava/lang/Throwable;)Z

    move-result v1

    .line 1706
    iget-object v2, p0, Lcom/google/k/k/a/x;->g:Ljava/lang/Object;

    monitor-enter v2

    .line 1707
    :try_start_0
    iget-object v0, p0, Lcom/google/k/k/a/x;->h:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 1708
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/k/k/a/x;->h:Ljava/util/Set;

    .line 1710
    :cond_0
    iget-object v0, p0, Lcom/google/k/k/a/x;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 1711
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1714
    :cond_1
    instance-of v2, p1, Ljava/lang/Error;

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/k/k/a/x;->c:Z

    if-eqz v2, :cond_3

    if-nez v1, :cond_3

    if-eqz v0, :cond_3

    .line 1716
    :cond_2
    sget-object v0, Lcom/google/k/k/a/x;->i:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "input future failed."

    invoke-virtual {v0, v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1718
    :cond_3
    return-void

    .line 1711
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method
