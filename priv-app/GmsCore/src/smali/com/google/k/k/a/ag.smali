.class public final Lcom/google/k/k/a/ag;
.super Ljava/util/concurrent/FutureTask;
.source "SourceFile"

# interfaces
.implements Lcom/google/k/k/a/af;


# instance fields
.field private final a:Lcom/google/k/k/a/h;


# direct methods
.method private constructor <init>(Ljava/lang/Runnable;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    .line 43
    new-instance v0, Lcom/google/k/k/a/h;

    invoke-direct {v0}, Lcom/google/k/k/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/k/k/a/ag;->a:Lcom/google/k/k/a/h;

    .line 79
    return-void
.end method

.method private constructor <init>(Ljava/util/concurrent/Callable;)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 43
    new-instance v0, Lcom/google/k/k/a/h;

    invoke-direct {v0}, Lcom/google/k/k/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/k/k/a/ag;->a:Lcom/google/k/k/a/h;

    .line 75
    return-void
.end method

.method public static a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/google/k/k/a/ag;
    .locals 1

    .prologue
    .line 70
    new-instance v0, Lcom/google/k/k/a/ag;

    invoke-direct {v0, p0, p1}, Lcom/google/k/k/a/ag;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;)Lcom/google/k/k/a/ag;
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/google/k/k/a/ag;

    invoke-direct {v0, p0}, Lcom/google/k/k/a/ag;-><init>(Ljava/util/concurrent/Callable;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/k/k/a/ag;->a:Lcom/google/k/k/a/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/k/k/a/h;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 84
    return-void
.end method

.method protected final done()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/k/k/a/ag;->a:Lcom/google/k/k/a/h;

    invoke-virtual {v0}, Lcom/google/k/k/a/h;->a()V

    .line 92
    return-void
.end method
