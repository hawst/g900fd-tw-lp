.class public final Lcom/google/k/k/a/n;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/k/k/a/e;

.field private static final b:Lcom/google/k/c/eb;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 979
    new-instance v0, Lcom/google/k/k/a/r;

    invoke-direct {v0}, Lcom/google/k/k/a/r;-><init>()V

    sput-object v0, Lcom/google/k/k/a/n;->a:Lcom/google/k/k/a/e;

    .line 1570
    invoke-static {}, Lcom/google/k/c/eb;->b()Lcom/google/k/c/eb;

    move-result-object v0

    new-instance v1, Lcom/google/k/k/a/t;

    invoke-direct {v1}, Lcom/google/k/k/a/t;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/k/c/eb;->a(Lcom/google/k/a/u;)Lcom/google/k/c/eb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/eb;->a()Lcom/google/k/c/eb;

    move-result-object v0

    sput-object v0, Lcom/google/k/k/a/n;->b:Lcom/google/k/c/eb;

    return-void
.end method

.method public static a(Lcom/google/k/k/a/af;Lcom/google/k/k/a/e;)Lcom/google/k/k/a/af;
    .locals 2

    .prologue
    .line 565
    new-instance v0, Lcom/google/k/k/a/v;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p0, v1}, Lcom/google/k/k/a/v;-><init>(Lcom/google/k/k/a/e;Lcom/google/k/k/a/af;B)V

    .line 567
    sget-object v1, Lcom/google/k/k/a/al;->a:Lcom/google/k/k/a/al;

    invoke-interface {p0, v0, v1}, Lcom/google/k/k/a/af;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 568
    return-object v0
.end method

.method public static a(Lcom/google/k/k/a/af;Lcom/google/k/k/a/e;Ljava/util/concurrent/Executor;)Lcom/google/k/k/a/af;
    .locals 3

    .prologue
    .line 613
    invoke-static {p2}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 614
    new-instance v0, Lcom/google/k/k/a/v;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p0, v1}, Lcom/google/k/k/a/v;-><init>(Lcom/google/k/k/a/e;Lcom/google/k/k/a/af;B)V

    .line 616
    new-instance v1, Lcom/google/k/k/a/o;

    invoke-direct {v1, p2, v0, v0}, Lcom/google/k/k/a/o;-><init>(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;Lcom/google/k/k/a/b;)V

    sget-object v2, Lcom/google/k/k/a/al;->a:Lcom/google/k/k/a/al;

    invoke-interface {p0, v1, v2}, Lcom/google/k/k/a/af;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 617
    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)Lcom/google/k/k/a/af;
    .locals 5

    .prologue
    .line 1027
    invoke-static {p0}, Lcom/google/k/c/bj;->a(Ljava/lang/Iterable;)Lcom/google/k/c/bj;

    move-result-object v0

    sget-object v1, Lcom/google/k/k/a/al;->a:Lcom/google/k/k/a/al;

    new-instance v2, Lcom/google/k/k/a/x;

    const/4 v3, 0x1

    new-instance v4, Lcom/google/k/k/a/u;

    invoke-direct {v4}, Lcom/google/k/k/a/u;-><init>()V

    invoke-direct {v2, v0, v3, v1, v4}, Lcom/google/k/k/a/x;-><init>(Lcom/google/k/c/bf;ZLjava/util/concurrent/Executor;Lcom/google/k/k/a/aa;)V

    return-object v2
.end method

.method public static a(Ljava/lang/Object;)Lcom/google/k/k/a/af;
    .locals 1

    .prologue
    .line 247
    new-instance v0, Lcom/google/k/k/a/ad;

    invoke-direct {v0, p0}, Lcom/google/k/k/a/ad;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Throwable;)Lcom/google/k/k/a/af;
    .locals 1

    .prologue
    .line 274
    invoke-static {p0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    new-instance v0, Lcom/google/k/k/a/ab;

    invoke-direct {v0, p0}, Lcom/google/k/k/a/ab;-><init>(Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static a(Lcom/google/k/k/a/af;Lcom/google/k/a/u;)Lcom/google/k/k/a/f;
    .locals 2

    .prologue
    .line 92
    new-instance v1, Lcom/google/k/k/a/ae;

    invoke-static {p0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/k/a/af;

    invoke-direct {v1, v0, p1}, Lcom/google/k/k/a/ae;-><init>(Lcom/google/k/k/a/af;Lcom/google/k/a/u;)V

    return-object v1
.end method

.method public static a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1507
    invoke-static {p0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1509
    :try_start_0
    invoke-static {p0}, Lcom/google/k/k/a/as;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1510
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v1, v0, Ljava/lang/Error;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/k/k/a/g;

    check-cast v0, Ljava/lang/Error;

    invoke-direct {v1, v0}, Lcom/google/k/k/a/g;-><init>(Ljava/lang/Error;)V

    throw v1

    :cond_0
    new-instance v1, Lcom/google/k/k/a/ar;

    invoke-direct {v1, v0}, Lcom/google/k/k/a/ar;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Lcom/google/k/k/a/af;Lcom/google/k/k/a/m;)V
    .locals 2

    .prologue
    .line 1258
    sget-object v0, Lcom/google/k/k/a/al;->a:Lcom/google/k/k/a/al;

    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/k/k/a/s;

    invoke-direct {v1, p0, p1}, Lcom/google/k/k/a/s;-><init>(Lcom/google/k/k/a/af;Lcom/google/k/k/a/m;)V

    invoke-interface {p0, v1, v0}, Lcom/google/k/k/a/af;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 1259
    return-void
.end method

.method public static b(Lcom/google/k/k/a/af;Lcom/google/k/a/u;)Lcom/google/k/k/a/af;
    .locals 3

    .prologue
    .line 705
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 706
    new-instance v0, Lcom/google/k/k/a/v;

    new-instance v1, Lcom/google/k/k/a/q;

    invoke-direct {v1, p1}, Lcom/google/k/k/a/q;-><init>(Lcom/google/k/a/u;)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/google/k/k/a/v;-><init>(Lcom/google/k/k/a/e;Lcom/google/k/k/a/af;B)V

    .line 708
    sget-object v1, Lcom/google/k/k/a/al;->a:Lcom/google/k/k/a/al;

    invoke-interface {p0, v0, v1}, Lcom/google/k/k/a/af;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 709
    return-object v0
.end method
