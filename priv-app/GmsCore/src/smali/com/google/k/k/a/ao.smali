.class final Lcom/google/k/k/a/ao;
.super Lcom/google/k/k/a/l;
.source "SourceFile"

# interfaces
.implements Lcom/google/k/k/a/ah;


# instance fields
.field private final a:Ljava/util/concurrent/ScheduledFuture;


# direct methods
.method public constructor <init>(Lcom/google/k/k/a/af;Ljava/util/concurrent/ScheduledFuture;)V
    .locals 0

    .prologue
    .line 610
    invoke-direct {p0, p1}, Lcom/google/k/k/a/l;-><init>(Lcom/google/k/k/a/af;)V

    .line 611
    iput-object p2, p0, Lcom/google/k/k/a/ao;->a:Ljava/util/concurrent/ScheduledFuture;

    .line 612
    return-void
.end method


# virtual methods
.method public final cancel(Z)Z
    .locals 2

    .prologue
    .line 616
    invoke-super {p0, p1}, Lcom/google/k/k/a/l;->cancel(Z)Z

    move-result v0

    .line 617
    if-eqz v0, :cond_0

    .line 619
    iget-object v1, p0, Lcom/google/k/k/a/ao;->a:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v1, p1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 623
    :cond_0
    return v0
.end method

.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 601
    check-cast p1, Ljava/util/concurrent/Delayed;

    iget-object v0, p0, Lcom/google/k/k/a/ao;->a:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ScheduledFuture;->compareTo(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final getDelay(Ljava/util/concurrent/TimeUnit;)J
    .locals 2

    .prologue
    .line 628
    iget-object v0, p0, Lcom/google/k/k/a/ao;->a:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ScheduledFuture;->getDelay(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method
