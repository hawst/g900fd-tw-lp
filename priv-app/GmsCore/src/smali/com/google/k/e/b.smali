.class public final Lcom/google/k/e/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/io/OutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 501
    new-instance v0, Lcom/google/k/e/c;

    invoke-direct {v0}, Lcom/google/k/e/c;-><init>()V

    sput-object v0, Lcom/google/k/e/b;->a:Ljava/io/OutputStream;

    return-void
.end method

.method private static a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    .locals 6

    .prologue
    .line 65
    invoke-static {p0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    const/16 v0, 0x1000

    new-array v2, v0, [B

    .line 68
    const-wide/16 v0, 0x0

    .line 70
    :goto_0
    invoke-virtual {p0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .line 71
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 72
    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 75
    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 76
    goto :goto_0

    .line 77
    :cond_0
    return-wide v0
.end method

.method public static a(Ljava/io/InputStream;)[B
    .locals 1

    .prologue
    .line 114
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 115
    invoke-static {p0, v0}, Lcom/google/k/e/b;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 116
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/io/InputStream;I)[B
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 127
    new-array v0, p1, [B

    move v1, p1

    .line 130
    :goto_0
    if-lez v1, :cond_2

    .line 131
    sub-int v2, p1, v1

    .line 132
    invoke-virtual {p0, v0, v2, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    .line 133
    if-ne v3, v5, :cond_1

    .line 136
    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    .line 155
    :cond_0
    :goto_1
    return-object v0

    .line 138
    :cond_1
    sub-int/2addr v1, v3

    .line 139
    goto :goto_0

    .line 142
    :cond_2
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 143
    if-eq v1, v5, :cond_0

    .line 148
    new-instance v2, Lcom/google/k/e/d;

    invoke-direct {v2, v4}, Lcom/google/k/e/d;-><init>(B)V

    .line 149
    invoke-virtual {v2, v1}, Lcom/google/k/e/d;->write(I)V

    .line 150
    invoke-static {p0, v2}, Lcom/google/k/e/b;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 152
    array-length v1, v0

    invoke-virtual {v2}, Lcom/google/k/e/d;->size()I

    move-result v3

    add-int/2addr v1, v3

    new-array v1, v1, [B

    .line 153
    array-length v3, v0

    invoke-static {v0, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 154
    array-length v0, v0

    invoke-virtual {v2, v1, v0}, Lcom/google/k/e/d;->a([BI)V

    move-object v0, v1

    .line 155
    goto :goto_1
.end method
