.class public abstract Lcom/google/k/e/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 196
    const/16 v0, 0x1000

    new-array v0, v0, [B

    sput-object v0, Lcom/google/k/e/a;->a:[B

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Ljava/io/InputStream;
.end method

.method public b()[B
    .locals 2

    .prologue
    .line 255
    invoke-static {}, Lcom/google/k/e/f;->a()Lcom/google/k/e/f;

    move-result-object v1

    .line 257
    :try_start_0
    invoke-virtual {p0}, Lcom/google/k/e/a;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/k/e/f;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 258
    invoke-static {v0}, Lcom/google/k/e/b;->a(Ljava/io/InputStream;)[B
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 262
    invoke-virtual {v1}, Lcom/google/k/e/f;->close()V

    return-object v0

    .line 259
    :catch_0
    move-exception v0

    .line 260
    :try_start_1
    invoke-virtual {v1, v0}, Lcom/google/k/e/f;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 262
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/k/e/f;->close()V

    throw v0
.end method
