.class public final Lcom/google/k/e/k;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/k/c/fk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 807
    new-instance v0, Lcom/google/k/e/l;

    invoke-direct {v0}, Lcom/google/k/e/l;-><init>()V

    sput-object v0, Lcom/google/k/e/k;->a:Lcom/google/k/c/fk;

    return-void
.end method

.method public static a(Ljava/io/File;)[B
    .locals 2

    .prologue
    .line 250
    new-instance v0, Lcom/google/k/e/m;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/k/e/m;-><init>(Ljava/io/File;B)V

    invoke-virtual {v0}, Lcom/google/k/e/a;->b()[B

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/io/InputStream;J)[B
    .locals 3

    .prologue
    .line 163
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 164
    new-instance v0, Ljava/lang/OutOfMemoryError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "file is too large to fit in a byte array: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/google/k/e/b;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    long-to-int v0, p1

    invoke-static {p0, v0}, Lcom/google/k/e/b;->a(Ljava/io/InputStream;I)[B

    move-result-object v0

    goto :goto_0
.end method
