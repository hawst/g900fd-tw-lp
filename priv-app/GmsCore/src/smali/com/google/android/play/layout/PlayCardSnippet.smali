.class public Lcom/google/android/play/layout/PlayCardSnippet;
.super Lcom/google/android/play/layout/o;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/ImageView;

.field private final d:I

.field private final e:I

.field private f:I

.field private final g:I

.field private final h:I

.field private i:I

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayCardSnippet;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/o;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 71
    sget v1, Lcom/google/android/play/e;->o:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardSnippet;->d:I

    .line 72
    sget v1, Lcom/google/android/play/e;->n:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardSnippet;->e:I

    .line 73
    sget v1, Lcom/google/android/play/e;->C:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardSnippet;->g:I

    .line 74
    sget v1, Lcom/google/android/play/e;->B:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->h:I

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->a:I

    .line 78
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 82
    invoke-super {p0}, Lcom/google/android/play/layout/o;->onFinishInflate()V

    .line 84
    sget v0, Lcom/google/android/play/g;->C:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->b:Landroid/widget/TextView;

    .line 85
    sget v0, Lcom/google/android/play/g;->B:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->c:Landroid/widget/ImageView;

    .line 87
    iget v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->a:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->d:I

    :goto_0
    iput v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->f:I

    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardSnippet;->b:Landroid/widget/TextView;

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->a:I

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->g:I

    int-to-float v0, v0

    :goto_1
    invoke-virtual {v1, v2, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 88
    return-void

    .line 87
    :cond_0
    iget v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->e:I

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->h:I

    int-to-float v0, v0

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 11

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardSnippet;->getHeight()I

    move-result v1

    .line 148
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardSnippet;->getPaddingTop()I

    move-result v2

    .line 150
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    .line 151
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    .line 152
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    const/16 v5, 0x8

    if-ne v0, v5, :cond_0

    .line 153
    sub-int v0, v1, v3

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v2

    .line 154
    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardSnippet;->b:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/play/layout/PlayCardSnippet;->i:I

    iget v5, p0, Lcom/google/android/play/layout/PlayCardSnippet;->i:I

    add-int/2addr v4, v5

    add-int/2addr v3, v0

    invoke-virtual {v1, v2, v0, v4, v3}, Landroid/widget/TextView;->layout(IIII)V

    .line 181
    :goto_0
    return-void

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v5

    .line 158
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v6

    .line 159
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 161
    iget v7, p0, Lcom/google/android/play/layout/PlayCardSnippet;->j:I

    add-int/2addr v7, v6

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v7

    .line 163
    if-le v5, v3, :cond_1

    .line 166
    sub-int v7, v1, v5

    sub-int/2addr v7, v2

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v7, v2

    .line 167
    iget-object v8, p0, Lcom/google/android/play/layout/PlayCardSnippet;->c:Landroid/widget/ImageView;

    iget v9, p0, Lcom/google/android/play/layout/PlayCardSnippet;->j:I

    iget v10, p0, Lcom/google/android/play/layout/PlayCardSnippet;->j:I

    add-int/2addr v6, v10

    add-int/2addr v5, v7

    invoke-virtual {v8, v9, v7, v6, v5}, Landroid/widget/ImageView;->layout(IIII)V

    .line 169
    sub-int/2addr v1, v3

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v2

    .line 170
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardSnippet;->b:Landroid/widget/TextView;

    add-int/2addr v4, v0

    add-int/2addr v3, v1

    invoke-virtual {v2, v0, v1, v4, v3}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_0

    .line 174
    :cond_1
    sub-int/2addr v1, v3

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v2

    .line 175
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardSnippet;->c:Landroid/widget/ImageView;

    iget v7, p0, Lcom/google/android/play/layout/PlayCardSnippet;->j:I

    iget v8, p0, Lcom/google/android/play/layout/PlayCardSnippet;->j:I

    add-int/2addr v6, v8

    add-int/2addr v5, v1

    invoke-virtual {v2, v7, v1, v6, v5}, Landroid/widget/ImageView;->layout(IIII)V

    .line 177
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardSnippet;->b:Landroid/widget/TextView;

    add-int/2addr v4, v0

    add-int/2addr v3, v1

    invoke-virtual {v2, v0, v1, v4, v3}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 121
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 122
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardSnippet;->getPaddingLeft()I

    move-result v0

    sub-int v0, v2, v0

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardSnippet;->getPaddingRight()I

    move-result v3

    sub-int v3, v0, v3

    .line 123
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    const/16 v4, 0x8

    if-ne v0, v4, :cond_0

    const/4 v0, 0x1

    .line 125
    :goto_0
    if-eqz v0, :cond_1

    .line 126
    iget v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->i:I

    sub-int v0, v3, v0

    .line 136
    :goto_1
    iget-object v3, p0, Lcom/google/android/play/layout/PlayCardSnippet;->b:Landroid/widget/TextView;

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v3, v0, v1}, Landroid/widget/TextView;->measure(II)V

    .line 139
    iget v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->f:I

    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardSnippet;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 140
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardSnippet;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardSnippet;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    .line 142
    invoke-virtual {p0, v2, v0}, Lcom/google/android/play/layout/PlayCardSnippet;->setMeasuredDimension(II)V

    .line 143
    return-void

    :cond_0
    move v0, v1

    .line 123
    goto :goto_0

    .line 129
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardSnippet;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 131
    iget v4, p0, Lcom/google/android/play/layout/PlayCardSnippet;->f:I

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 132
    iget-object v5, p0, Lcom/google/android/play/layout/PlayCardSnippet;->c:Landroid/widget/ImageView;

    invoke-virtual {v5, v4, v4}, Landroid/widget/ImageView;->measure(II)V

    .line 133
    iget v4, p0, Lcom/google/android/play/layout/PlayCardSnippet;->f:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v4

    iget v4, p0, Lcom/google/android/play/layout/PlayCardSnippet;->j:I

    add-int/2addr v0, v4

    sub-int v0, v3, v0

    goto :goto_1
.end method
