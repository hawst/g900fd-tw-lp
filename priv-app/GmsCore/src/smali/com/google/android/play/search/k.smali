.class public final Lcom/google/android/play/search/k;
.super Landroid/support/v7/widget/LinearLayoutManager;
.source "SourceFile"


# instance fields
.field private final a:F

.field private final b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>()V

    .line 27
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/e;->z:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/play/search/k;->a:F

    .line 29
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 30
    int-to-float v0, v0

    const v1, 0x3ecccccd    # 0.4f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 31
    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/play/search/k;->a:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 32
    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/play/search/k;->a:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/play/search/k;->b:I

    .line 33
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    .prologue
    .line 38
    iget v1, p0, Lcom/google/android/play/search/k;->b:I

    iget-object v0, p0, Landroid/support/v7/widget/ce;->q:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ce;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->b()Landroid/support/v7/widget/bv;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/bv;->a()I

    move-result v0

    :goto_1
    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/play/search/k;->a:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 39
    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/search/k;->b(II)V

    .line 41
    return-void

    .line 38
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final c(Landroid/support/v7/widget/cj;Landroid/support/v7/widget/cp;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/LinearLayoutManager;->c(Landroid/support/v7/widget/cj;Landroid/support/v7/widget/cp;)V

    .line 46
    invoke-virtual {p0, v0, v0}, Lcom/google/android/play/search/k;->a(II)V

    .line 47
    return-void
.end method
