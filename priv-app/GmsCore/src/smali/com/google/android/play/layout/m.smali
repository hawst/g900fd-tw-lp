.class public abstract Lcom/google/android/play/layout/m;
.super Lcom/google/android/play/layout/l;
.source "SourceFile"


# static fields
.field protected static final e:Z


# instance fields
.field private final A:I

.field private B:Landroid/graphics/drawable/Drawable;

.field private final C:Z

.field private final D:I

.field protected f:I

.field protected g:Z

.field protected h:Lcom/google/android/play/layout/PlayCardThumbnail;

.field protected i:Landroid/widget/TextView;

.field protected j:Lcom/google/android/play/layout/PlayTextView;

.field protected k:Lcom/google/android/play/layout/StarRatingBar;

.field protected l:Lcom/google/android/play/layout/PlayTextView;

.field protected m:Lcom/google/android/play/layout/PlayCardLabelView;

.field protected n:Lcom/google/android/play/layout/PlayTextView;

.field protected o:Landroid/widget/ImageView;

.field protected p:Lcom/google/android/play/layout/PlayCardSnippet;

.field protected q:Lcom/google/android/play/layout/PlayCardSnippet;

.field protected r:Landroid/view/View;

.field protected s:F

.field protected t:Z

.field protected final u:I

.field protected final v:I

.field private final w:I

.field private final x:Landroid/graphics/Rect;

.field private final y:Landroid/graphics/Rect;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/layout/m;->e:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 122
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/play/layout/m;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 123
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/layout/m;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 127
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 130
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/layout/l;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 132
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/e;->m:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/m;->w:I

    .line 134
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/layout/m;->x:Landroid/graphics/Rect;

    .line 135
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/layout/m;->y:Landroid/graphics/Rect;

    .line 137
    sget-object v0, Lcom/google/android/play/k;->ac:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 139
    sget v1, Lcom/google/android/play/k;->af:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/play/layout/m;->C:Z

    .line 141
    sget v1, Lcom/google/android/play/k;->ag:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/play/layout/m;->t:Z

    .line 143
    sget v1, Lcom/google/android/play/k;->ah:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/play/e;->p:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/m;->u:I

    .line 147
    sget v1, Lcom/google/android/play/k;->ad:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/m;->v:I

    .line 149
    sget v1, Lcom/google/android/play/k;->ae:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/m;->D:I

    .line 152
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 154
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/e;->i:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/m;->A:I

    .line 155
    iget v0, p0, Lcom/google/android/play/layout/m;->A:I

    iget v1, p0, Lcom/google/android/play/layout/m;->A:I

    iget v2, p0, Lcom/google/android/play/layout/m;->A:I

    iget v3, p0, Lcom/google/android/play/layout/m;->A:I

    iput v0, p0, Lcom/google/android/play/layout/l;->a:I

    iput v1, p0, Lcom/google/android/play/layout/l;->b:I

    iput v2, p0, Lcom/google/android/play/layout/l;->c:I

    iput v3, p0, Lcom/google/android/play/layout/l;->d:I

    invoke-virtual {p0}, Lcom/google/android/play/layout/l;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/play/layout/l;->invalidate()V

    .line 157
    sget-object v0, Lcom/google/android/play/b/c;->a:Lcom/google/android/play/b/b;

    invoke-interface {v0, p0, p1, p2, p3}, Lcom/google/android/play/b/b;->a(Landroid/view/View;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 158
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 3

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/play/layout/m;->o:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/layout/m;->o:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 472
    :cond_0
    :goto_0
    return-void

    .line 458
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/layout/m;->o:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/play/layout/m;->x:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->getHitRect(Landroid/graphics/Rect;)V

    .line 459
    iget-object v0, p0, Lcom/google/android/play/layout/m;->x:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/google/android/play/layout/m;->w:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 460
    iget-object v0, p0, Lcom/google/android/play/layout/m;->x:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, Lcom/google/android/play/layout/m;->w:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 461
    iget-object v0, p0, Lcom/google/android/play/layout/m;->x:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/google/android/play/layout/m;->w:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 462
    iget-object v0, p0, Lcom/google/android/play/layout/m;->x:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/google/android/play/layout/m;->w:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 463
    iget-object v0, p0, Lcom/google/android/play/layout/m;->x:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/google/android/play/layout/m;->y:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/play/layout/m;->x:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/google/android/play/layout/m;->y:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/play/layout/m;->x:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/play/layout/m;->y:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/play/layout/m;->x:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/play/layout/m;->y:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    if-eq v0, v1, :cond_0

    .line 470
    :cond_2
    new-instance v0, Lcom/google/android/play/utils/c;

    iget-object v1, p0, Lcom/google/android/play/layout/m;->x:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/play/layout/m;->o:Landroid/widget/ImageView;

    invoke-direct {v0, v1, v2}, Lcom/google/android/play/utils/c;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/m;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 471
    iget-object v0, p0, Lcom/google/android/play/layout/m;->y:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/play/layout/m;->x:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method protected final a(I)V
    .locals 6

    .prologue
    .line 323
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 325
    invoke-virtual {p0}, Lcom/google/android/play/layout/m;->getPaddingLeft()I

    move-result v2

    .line 326
    invoke-virtual {p0}, Lcom/google/android/play/layout/m;->getPaddingRight()I

    move-result v3

    .line 328
    iget-object v0, p0, Lcom/google/android/play/layout/m;->h:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 330
    iget-object v4, p0, Lcom/google/android/play/layout/m;->h:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayCardThumbnail;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    .line 331
    sub-int/2addr v1, v2

    sub-int/2addr v1, v3

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v1, v2

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v1, v2

    .line 333
    iget v2, p0, Lcom/google/android/play/layout/m;->s:F

    int-to-float v1, v1

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 334
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 338
    :goto_0
    return-void

    .line 336
    :cond_0
    const/4 v1, 0x0

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    goto :goto_0
.end method

.method protected final b(I)V
    .locals 6

    .prologue
    .line 341
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 343
    invoke-virtual {p0}, Lcom/google/android/play/layout/m;->getPaddingTop()I

    move-result v2

    .line 344
    invoke-virtual {p0}, Lcom/google/android/play/layout/m;->getPaddingBottom()I

    move-result v3

    .line 346
    iget-object v0, p0, Lcom/google/android/play/layout/m;->h:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 348
    iget-object v4, p0, Lcom/google/android/play/layout/m;->h:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayCardThumbnail;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    .line 349
    sub-int/2addr v1, v2

    sub-int/2addr v1, v3

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int/2addr v1, v2

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int/2addr v1, v2

    .line 351
    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/play/layout/m;->s:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 352
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 356
    :goto_0
    return-void

    .line 354
    :cond_0
    const/4 v1, 0x0

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    goto :goto_0
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 516
    invoke-super {p0, p1}, Lcom/google/android/play/layout/l;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v1

    .line 520
    iget-boolean v2, p0, Lcom/google/android/play/layout/m;->z:Z

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v2

    const/16 v4, 0x8

    if-ne v2, v4, :cond_0

    move v2, v0

    .line 522
    :goto_0
    if-eqz v2, :cond_1

    .line 523
    invoke-virtual {p1, v3}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    .line 527
    :goto_1
    return v0

    :cond_0
    move v2, v3

    .line 520
    goto :goto_0

    :cond_1
    move v0, v1

    .line 527
    goto :goto_1
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 494
    invoke-super {p0, p1}, Lcom/google/android/play/layout/l;->draw(Landroid/graphics/Canvas;)V

    .line 496
    invoke-virtual {p0}, Lcom/google/android/play/layout/m;->getWidth()I

    move-result v0

    .line 497
    invoke-virtual {p0}, Lcom/google/android/play/layout/m;->getHeight()I

    move-result v1

    .line 499
    iget-boolean v2, p0, Lcom/google/android/play/layout/m;->z:Z

    if-eqz v2, :cond_1

    .line 500
    iget-object v2, p0, Lcom/google/android/play/layout/m;->B:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_0

    .line 501
    new-instance v2, Landroid/graphics/drawable/PaintDrawable;

    invoke-virtual {p0}, Lcom/google/android/play/layout/m;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/play/d;->g:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/PaintDrawable;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/play/layout/m;->B:Landroid/graphics/drawable/Drawable;

    .line 504
    :cond_0
    iget-object v2, p0, Lcom/google/android/play/layout/m;->B:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v5, v5, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 505
    iget-object v0, p0, Lcom/google/android/play/layout/m;->B:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 507
    :cond_1
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 162
    invoke-super {p0}, Lcom/google/android/play/layout/l;->onAttachedToWindow()V

    .line 164
    invoke-static {}, Lcom/google/android/play/layout/n;->a()Lcom/google/android/play/layout/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/layout/n;->b()V

    .line 165
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 169
    invoke-super {p0}, Lcom/google/android/play/layout/l;->onDetachedFromWindow()V

    .line 171
    invoke-static {}, Lcom/google/android/play/layout/n;->a()Lcom/google/android/play/layout/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/layout/n;->c()V

    .line 172
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 297
    invoke-super {p0}, Lcom/google/android/play/layout/l;->onFinishInflate()V

    .line 299
    sget v0, Lcom/google/android/play/g;->F:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/m;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardThumbnail;

    iput-object v0, p0, Lcom/google/android/play/layout/m;->h:Lcom/google/android/play/layout/PlayCardThumbnail;

    .line 300
    sget v0, Lcom/google/android/play/g;->G:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/m;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/layout/m;->i:Landroid/widget/TextView;

    .line 301
    sget v0, Lcom/google/android/play/g;->D:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/m;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayTextView;

    iput-object v0, p0, Lcom/google/android/play/layout/m;->j:Lcom/google/android/play/layout/PlayTextView;

    .line 302
    sget v0, Lcom/google/android/play/g;->y:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/m;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/StarRatingBar;

    iput-object v0, p0, Lcom/google/android/play/layout/m;->k:Lcom/google/android/play/layout/StarRatingBar;

    .line 303
    sget v0, Lcom/google/android/play/g;->u:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/m;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayTextView;

    iput-object v0, p0, Lcom/google/android/play/layout/m;->l:Lcom/google/android/play/layout/PlayTextView;

    .line 304
    sget v0, Lcom/google/android/play/g;->v:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/m;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayTextView;

    iput-object v0, p0, Lcom/google/android/play/layout/m;->n:Lcom/google/android/play/layout/PlayTextView;

    .line 305
    sget v0, Lcom/google/android/play/g;->x:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/m;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/play/layout/m;->o:Landroid/widget/ImageView;

    .line 306
    sget v0, Lcom/google/android/play/g;->w:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/m;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardLabelView;

    iput-object v0, p0, Lcom/google/android/play/layout/m;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    .line 307
    sget v0, Lcom/google/android/play/g;->z:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/m;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardSnippet;

    iput-object v0, p0, Lcom/google/android/play/layout/m;->p:Lcom/google/android/play/layout/PlayCardSnippet;

    .line 308
    sget v0, Lcom/google/android/play/g;->A:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/m;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardSnippet;

    iput-object v0, p0, Lcom/google/android/play/layout/m;->q:Lcom/google/android/play/layout/PlayCardSnippet;

    .line 309
    sget v0, Lcom/google/android/play/g;->H:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/m;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/layout/m;->r:Landroid/view/View;

    .line 313
    sget-boolean v0, Lcom/google/android/play/layout/m;->e:Z

    if-eqz v0, :cond_0

    .line 314
    invoke-virtual {p0, v2}, Lcom/google/android/play/layout/m;->setNextFocusRightId(I)V

    .line 315
    iget-object v0, p0, Lcom/google/android/play/layout/m;->o:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/google/android/play/layout/m;->o:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 317
    iget-object v0, p0, Lcom/google/android/play/layout/m;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setNextFocusLeftId(I)V

    .line 320
    :cond_0
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 533
    invoke-super {p0, p1}, Lcom/google/android/play/layout/l;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 535
    iget-boolean v0, p0, Lcom/google/android/play/layout/m;->z:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setEnabled(Z)V

    .line 536
    return-void

    .line 535
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 476
    iget-boolean v0, p0, Lcom/google/android/play/layout/m;->z:Z

    if-eqz v0, :cond_0

    .line 477
    const/4 v0, 0x1

    .line 480
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/play/layout/l;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 446
    invoke-super/range {p0 .. p5}, Lcom/google/android/play/layout/l;->onLayout(ZIIII)V

    .line 448
    invoke-virtual {p0}, Lcom/google/android/play/layout/m;->a()V

    .line 449
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 420
    invoke-super {p0, p1, p2}, Lcom/google/android/play/layout/l;->onMeasure(II)V

    .line 422
    iget-object v0, p0, Lcom/google/android/play/layout/m;->n:Lcom/google/android/play/layout/PlayTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/layout/m;->n:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/layout/m;->n:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/google/android/play/layout/m;->n:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v2

    .line 427
    iget-object v0, p0, Lcom/google/android/play/layout/m;->n:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayTextView;->getLayout()Landroid/text/Layout;

    move-result-object v3

    .line 428
    if-nez v3, :cond_1

    .line 442
    :cond_0
    :goto_0
    return-void

    :cond_1
    move v0, v1

    .line 431
    :goto_1
    invoke-virtual {v3}, Landroid/text/Layout;->getLineCount()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 432
    invoke-virtual {v3, v0}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v4

    .line 433
    if-le v4, v2, :cond_3

    .line 437
    iget-object v2, p0, Lcom/google/android/play/layout/m;->n:Lcom/google/android/play/layout/PlayTextView;

    const/4 v3, 0x2

    if-lt v0, v3, :cond_2

    :goto_2
    invoke-virtual {v2, v1}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x4

    goto :goto_2

    .line 431
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 485
    iget-boolean v0, p0, Lcom/google/android/play/layout/m;->z:Z

    if-eqz v0, :cond_0

    .line 486
    const/4 v0, 0x1

    .line 489
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/play/layout/l;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setBackgroundColor(I)V
    .locals 1

    .prologue
    .line 181
    sget-object v0, Lcom/google/android/play/b/c;->a:Lcom/google/android/play/b/b;

    invoke-interface {v0, p0, p1}, Lcom/google/android/play/b/b;->a(Landroid/view/View;I)V

    .line 182
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1

    .prologue
    .line 186
    sget-object v0, Lcom/google/android/play/b/c;->a:Lcom/google/android/play/b/b;

    invoke-interface {v0, p0, p1}, Lcom/google/android/play/b/b;->b(Landroid/view/View;I)V

    .line 187
    return-void
.end method
