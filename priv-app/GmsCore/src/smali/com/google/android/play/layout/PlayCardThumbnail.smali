.class public Lcom/google/android/play/layout/PlayCardThumbnail;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/ImageView;

.field private final b:I

.field private final c:I

.field private final d:I

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayCardThumbnail;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 48
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    sget-object v0, Lcom/google/android/play/k;->ai:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 52
    sget v1, Lcom/google/android/play/k;->al:I

    sget v2, Lcom/google/android/play/g;->E:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->b:I

    .line 54
    sget v1, Lcom/google/android/play/k;->aj:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->c:I

    .line 56
    sget v1, Lcom/google/android/play/k;->ak:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->d:I

    .line 58
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 59
    return-void
.end method


# virtual methods
.method public getBaseline()I
    .locals 2

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->e:I

    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 65
    iget v0, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->b:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardThumbnail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->a:Landroid/widget/ImageView;

    .line 66
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->a:Landroid/widget/ImageView;

    iget v1, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->e:I

    iget v2, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->e:I

    iget v3, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->e:I

    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->a:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->e:I

    iget-object v5, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->a:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    .line 118
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 102
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 103
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 105
    iget v2, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->e:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, v0, v2

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 106
    iget v3, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->e:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, v1, v3

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 107
    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->a:Landroid/widget/ImageView;

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v4, v2, v3}, Landroid/widget/ImageView;->measure(II)V

    .line 110
    invoke-virtual {p0, v0, v1}, Lcom/google/android/play/layout/PlayCardThumbnail;->setMeasuredDimension(II)V

    .line 111
    return-void
.end method
