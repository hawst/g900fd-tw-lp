.class public Lcom/google/android/play/search/PlaySearchPlateTextContainer;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Lcom/google/android/play/search/e;


# instance fields
.field private final a:Landroid/view/inputmethod/InputMethodManager;

.field private b:Lcom/google/android/play/search/d;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/widget/EditText;

.field private e:Z

.field private f:Lcom/google/android/play/search/n;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    const-string v0, "input_method"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->a:Landroid/view/inputmethod/InputMethodManager;

    .line 42
    return-void
.end method

.method static synthetic a(Lcom/google/android/play/search/PlaySearchPlateTextContainer;)Lcom/google/android/play/search/d;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->b:Lcom/google/android/play/search/d;

    return-object v0
.end method

.method private b(I)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 79
    if-ne p1, v3, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 81
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->d:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 82
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->d:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->d:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->a:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->d:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 98
    :goto_0
    return-void

    .line 86
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 87
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 88
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->d:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 89
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->d:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->d:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 94
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->a:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 96
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->d:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    goto :goto_0

    .line 91
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 92
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->b:Lcom/google/android/play/search/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->f:Lcom/google/android/play/search/n;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->f:Lcom/google/android/play/search/n;

    invoke-virtual {p0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/n;->b(Landroid/content/Context;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->b(I)V

    .line 76
    return-void
.end method

.method public final a(Lcom/google/android/play/search/d;)V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->b:Lcom/google/android/play/search/d;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->b:Lcom/google/android/play/search/d;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/d;->b(Lcom/google/android/play/search/e;)V

    .line 68
    :cond_0
    iput-object p1, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->b:Lcom/google/android/play/search/d;

    .line 69
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->b:Lcom/google/android/play/search/d;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/d;->a(Lcom/google/android/play/search/e;)V

    .line 70
    new-instance v0, Lcom/google/android/play/search/n;

    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->b:Lcom/google/android/play/search/d;

    invoke-direct {v0, v1}, Lcom/google/android/play/search/n;-><init>(Lcom/google/android/play/search/d;)V

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->f:Lcom/google/android/play/search/n;

    .line 71
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->e:Z

    .line 112
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->d:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->d:Landroid/widget/EditText;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 114
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->e:Z

    .line 115
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 132
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 127
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 102
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 103
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->f:Lcom/google/android/play/search/n;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->f:Lcom/google/android/play/search/n;

    invoke-virtual {p0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/n;->c(Landroid/content/Context;)V

    .line 107
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 47
    sget v0, Lcom/google/android/play/g;->aa:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->c:Landroid/widget/ImageView;

    .line 48
    sget v0, Lcom/google/android/play/g;->ab:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->d:Landroid/widget/EditText;

    .line 49
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->b(I)V

    .line 51
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->c:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/play/search/h;

    invoke-direct {v1, p0}, Lcom/google/android/play/search/h;-><init>(Lcom/google/android/play/search/PlaySearchPlateTextContainer;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->b:Lcom/google/android/play/search/d;

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    .line 164
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->b:Lcom/google/android/play/search/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/d;->a(I)V

    .line 166
    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->b:Lcom/google/android/play/search/d;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->e:Z

    if-nez v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->b:Lcom/google/android/play/search/d;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/d;->a(Ljava/lang/String;)V

    .line 122
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2

    .prologue
    .line 152
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowFocusChanged(Z)V

    .line 153
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->b:Lcom/google/android/play/search/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->b:Lcom/google/android/play/search/d;

    iget v0, v0, Lcom/google/android/play/search/d;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 154
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->b:Lcom/google/android/play/search/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/d;->a(I)V

    .line 155
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->f:Lcom/google/android/play/search/n;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->f:Lcom/google/android/play/search/n;

    invoke-virtual {p0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/n;->c(Landroid/content/Context;)V

    .line 159
    :cond_0
    return-void
.end method
