.class public Lcom/google/android/play/layout/PlayCardViewMini;
.super Lcom/google/android/play/layout/m;
.source "SourceFile"


# instance fields
.field private final w:I

.field private final x:I

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayCardViewMini;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/m;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 50
    sget v1, Lcom/google/android/play/e;->x:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardViewMini;->w:I

    .line 52
    sget v1, Lcom/google/android/play/e;->y:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/PlayCardViewMini;->x:I

    .line 53
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/play/layout/PlayCardViewMini;->z:I

    .line 54
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 21

    .prologue
    .line 172
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getPaddingLeft()I

    move-result v9

    .line 173
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getPaddingRight()I

    move-result v10

    .line 174
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getPaddingTop()I

    move-result v11

    .line 175
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getPaddingBottom()I

    move-result v12

    .line 177
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getWidth()I

    move-result v13

    .line 178
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getHeight()I

    move-result v14

    .line 180
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/layout/PlayCardViewMini;->h:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v15

    .line 181
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/layout/PlayCardViewMini;->h:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/layout/PlayCardViewMini;->h:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v5}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v9

    add-int v6, v11, v15

    invoke-virtual {v4, v9, v11, v5, v6}, Lcom/google/android/play/layout/PlayCardThumbnail;->layout(IIII)V

    .line 184
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/layout/PlayCardViewMini;->i:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 186
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/layout/PlayCardViewMini;->j:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v5}, Lcom/google/android/play/layout/PlayTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 188
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/play/layout/PlayCardViewMini;->k:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v6}, Lcom/google/android/play/layout/StarRatingBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 190
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/play/layout/PlayCardViewMini;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v7}, Lcom/google/android/play/layout/PlayCardLabelView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 192
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/play/layout/PlayCardViewMini;->o:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 195
    add-int/2addr v15, v11

    iget v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v16, v0

    add-int v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->y:I

    move/from16 v16, v0

    sub-int v15, v15, v16

    .line 196
    iget v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v16, v0

    add-int v16, v16, v9

    .line 197
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->i:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->i:Landroid/widget/TextView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v18

    add-int v18, v18, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->i:Landroid/widget/TextView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v19

    add-int v19, v19, v15

    move-object/from16 v0, v17

    move/from16 v1, v16

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-virtual {v0, v1, v15, v2, v3}, Landroid/widget/TextView;->layout(IIII)V

    .line 200
    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v17, v0

    add-int v17, v17, v15

    .line 201
    sub-int v18, v13, v10

    iget v8, v8, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v8, v18, v8

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->o:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->o:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v19

    sub-int v19, v8, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->o:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v20

    add-int v20, v20, v17

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v17

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v8, v3}, Landroid/widget/ImageView;->layout(IIII)V

    .line 205
    sub-int v8, v14, v12

    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v17, v0

    sub-int v8, v8, v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->y:I

    move/from16 v17, v0

    add-int v8, v8, v17

    .line 206
    sub-int v17, v13, v10

    iget v7, v7, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v7, v17, v7

    .line 207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredWidth()I

    move-result v18

    sub-int v18, v7, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewMini;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredHeight()I

    move-result v19

    sub-int v19, v8, v19

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2, v7, v8}, Lcom/google/android/play/layout/PlayCardLabelView;->layout(IIII)V

    .line 210
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/play/layout/PlayCardViewMini;->j:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v7}, Lcom/google/android/play/layout/PlayTextView;->getVisibility()I

    move-result v7

    const/16 v17, 0x8

    move/from16 v0, v17

    if-eq v7, v0, :cond_0

    .line 211
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/play/layout/PlayCardViewMini;->i:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getLineCount()I

    move-result v7

    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v7, v0, :cond_2

    .line 212
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/play/layout/PlayCardViewMini;->i:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v15

    iget v4, v4, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v4, v7

    iget v5, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v4, v5

    .line 214
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/layout/PlayCardViewMini;->j:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/play/layout/PlayCardViewMini;->j:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v7}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredWidth()I

    move-result v7

    add-int v7, v7, v16

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/play/layout/PlayCardViewMini;->j:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v8}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v4

    move/from16 v0, v16

    invoke-virtual {v5, v0, v4, v7, v8}, Lcom/google/android/play/layout/PlayTextView;->layout(IIII)V

    .line 225
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/layout/PlayCardViewMini;->k:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v4}, Lcom/google/android/play/layout/StarRatingBar;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_1

    .line 226
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/layout/PlayCardViewMini;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayCardLabelView;->getTop()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/layout/PlayCardViewMini;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v5}, Lcom/google/android/play/layout/PlayCardLabelView;->getBaseline()I

    move-result v5

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/layout/PlayCardViewMini;->k:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v5}, Lcom/google/android/play/layout/StarRatingBar;->getBaseline()I

    move-result v5

    sub-int/2addr v4, v5

    .line 228
    iget v5, v6, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v5, v9

    .line 229
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/play/layout/PlayCardViewMini;->k:Lcom/google/android/play/layout/StarRatingBar;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/play/layout/PlayCardViewMini;->k:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v7}, Lcom/google/android/play/layout/StarRatingBar;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v5

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/play/layout/PlayCardViewMini;->k:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v8}, Lcom/google/android/play/layout/StarRatingBar;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v4

    invoke-virtual {v6, v5, v4, v7, v8}, Lcom/google/android/play/layout/StarRatingBar;->layout(IIII)V

    .line 234
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/layout/PlayCardViewMini;->r:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 235
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/layout/PlayCardViewMini;->r:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 236
    sub-int v6, v13, v9

    sub-int/2addr v6, v10

    sub-int v4, v6, v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v9

    .line 237
    sub-int v6, v14, v11

    sub-int/2addr v6, v12

    sub-int v5, v6, v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v5, v11

    .line 238
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/play/layout/PlayCardViewMini;->r:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/play/layout/PlayCardViewMini;->r:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/play/layout/PlayCardViewMini;->r:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v5

    invoke-virtual {v6, v4, v5, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 242
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewMini;->a()V

    .line 243
    return-void

    .line 218
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/layout/PlayCardViewMini;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredHeight()I

    move-result v4

    sub-int v4, v8, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/layout/PlayCardViewMini;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v5}, Lcom/google/android/play/layout/PlayCardLabelView;->getBaseline()I

    move-result v5

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/layout/PlayCardViewMini;->j:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v5}, Lcom/google/android/play/layout/PlayTextView;->getBaseline()I

    move-result v5

    sub-int/2addr v4, v5

    .line 220
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/layout/PlayCardViewMini;->j:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/play/layout/PlayCardViewMini;->j:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v7}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredWidth()I

    move-result v7

    add-int v7, v7, v16

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/play/layout/PlayCardViewMini;->j:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v8}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v4

    move/from16 v0, v16

    invoke-virtual {v5, v0, v4, v7, v8}, Lcom/google/android/play/layout/PlayTextView;->layout(IIII)V

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 12

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lcom/google/android/play/layout/PlayCardViewMini;->a(I)V

    .line 68
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewMini;->h:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 70
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 71
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 75
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    iget v4, p0, Lcom/google/android/play/layout/PlayCardViewMini;->w:I

    add-int/2addr v2, v4

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getPaddingTop()I

    move-result v4

    add-int/2addr v2, v4

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getPaddingBottom()I

    move-result v4

    add-int/2addr v2, v4

    .line 77
    const/high16 v4, 0x40000000    # 2.0f

    if-ne v3, v4, :cond_1

    if-lez v1, :cond_1

    move v4, v1

    .line 79
    :goto_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 84
    const/4 v1, 0x0

    sub-int/2addr v2, v4

    div-int/lit8 v2, v2, 0x3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardViewMini;->y:I

    .line 86
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getPaddingLeft()I

    move-result v1

    .line 87
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewMini;->getPaddingRight()I

    move-result v2

    .line 89
    sub-int v1, v6, v1

    sub-int v7, v1, v2

    .line 90
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v7, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 91
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardViewMini;->h:Lcom/google/android/play/layout/PlayCardThumbnail;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v2, v1, v0}, Lcom/google/android/play/layout/PlayCardThumbnail;->measure(II)V

    .line 94
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewMini;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 96
    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardViewMini;->j:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v1}, Lcom/google/android/play/layout/PlayTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 98
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardViewMini;->k:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v2}, Lcom/google/android/play/layout/StarRatingBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 100
    iget-object v3, p0, Lcom/google/android/play/layout/PlayCardViewMini;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v3}, Lcom/google/android/play/layout/PlayCardLabelView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 104
    iget v5, p0, Lcom/google/android/play/layout/PlayCardViewMini;->f:I

    const/4 v8, 0x3

    if-ne v5, v8, :cond_2

    .line 106
    const/4 v5, 0x1

    .line 110
    :goto_1
    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/android/play/layout/PlayCardViewMini;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v5}, Lcom/google/android/play/layout/PlayCardLabelView;->getVisibility()I

    move-result v5

    const/16 v8, 0x8

    if-eq v5, v8, :cond_5

    .line 112
    iget-object v5, p0, Lcom/google/android/play/layout/PlayCardViewMini;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    const/high16 v8, -0x80000000

    invoke-static {v7, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual {v5, v8, v9}, Lcom/google/android/play/layout/PlayCardLabelView;->measure(II)V

    .line 122
    :goto_2
    iget-object v5, p0, Lcom/google/android/play/layout/PlayCardViewMini;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v5}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredWidth()I

    move-result v5

    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v5, v8

    iget v3, v3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v3, v5

    .line 125
    iget-object v5, p0, Lcom/google/android/play/layout/PlayCardViewMini;->o:Landroid/widget/ImageView;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v5, v8, v9}, Landroid/widget/ImageView;->measure(II)V

    .line 128
    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v5, v7, v5

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v0, v5, v0

    .line 129
    iget-object v5, p0, Lcom/google/android/play/layout/PlayCardViewMini;->i:Landroid/widget/TextView;

    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    const/4 v8, 0x0

    invoke-virtual {v5, v0, v8}, Landroid/widget/TextView;->measure(II)V

    .line 132
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewMini;->r:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewMini;->j:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayTextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_7

    .line 136
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewMini;->k:Lcom/google/android/play/layout/StarRatingBar;

    const/4 v1, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v5}, Lcom/google/android/play/layout/StarRatingBar;->measure(II)V

    .line 137
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewMini;->k:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v0}, Lcom/google/android/play/layout/StarRatingBar;->getMeasuredWidth()I

    move-result v0

    .line 138
    iget v1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v0, v1

    iget v1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    add-int/2addr v0, v3

    .line 140
    if-le v0, v7, :cond_6

    .line 141
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewMini;->k:Lcom/google/android/play/layout/StarRatingBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 165
    :cond_0
    :goto_3
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewMini;->r:Landroid/view/View;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 167
    invoke-virtual {p0, v6, v4}, Lcom/google/android/play/layout/PlayCardViewMini;->setMeasuredDimension(II)V

    .line 168
    return-void

    :cond_1
    move v4, v2

    .line 77
    goto/16 :goto_0

    .line 108
    :cond_2
    iget-boolean v5, p0, Lcom/google/android/play/layout/PlayCardViewMini;->g:Z

    if-nez v5, :cond_3

    iget v5, p0, Lcom/google/android/play/layout/PlayCardViewMini;->x:I

    if-lt v6, v5, :cond_4

    :cond_3
    const/4 v5, 0x1

    goto/16 :goto_1

    :cond_4
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 118
    :cond_5
    const/4 v5, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 119
    iget-object v8, p0, Lcom/google/android/play/layout/PlayCardViewMini;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    const/4 v9, 0x0

    invoke-virtual {v8, v5, v9}, Lcom/google/android/play/layout/PlayCardLabelView;->measure(II)V

    goto :goto_2

    .line 143
    :cond_6
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewMini;->k:Lcom/google/android/play/layout/StarRatingBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    goto :goto_3

    .line 148
    :cond_7
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewMini;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLineCount()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_8

    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewMini;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardLabelView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    iget v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v0, v7, v0

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v0, v1

    .line 154
    :goto_4
    int-to-double v2, v0

    const-wide v8, 0x3fd3333333333333L    # 0.3

    int-to-double v10, v7

    mul-double/2addr v8, v10

    cmpg-double v1, v2, v8

    if-gez v1, :cond_a

    .line 155
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewMini;->j:Lcom/google/android/play/layout/PlayTextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    goto :goto_3

    .line 148
    :cond_9
    iget v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v0, v7, v0

    sub-int/2addr v0, v3

    goto :goto_4

    .line 157
    :cond_a
    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardViewMini;->j:Lcom/google/android/play/layout/PlayTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    .line 158
    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardViewMini;->j:Lcom/google/android/play/layout/PlayTextView;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/play/layout/PlayTextView;->measure(II)V

    goto :goto_3
.end method
