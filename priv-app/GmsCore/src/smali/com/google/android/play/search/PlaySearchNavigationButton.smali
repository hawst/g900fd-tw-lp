.class public Lcom/google/android/play/search/PlaySearchNavigationButton;
.super Landroid/widget/ImageView;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/play/search/e;


# static fields
.field private static final a:Z


# instance fields
.field private b:Lcom/google/android/play/search/d;

.field private c:I

.field private d:Lcom/google/android/play/search/a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/search/PlaySearchNavigationButton;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    new-instance v0, Lcom/google/android/play/search/a;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/play/d;->s:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/play/search/a;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->d:Lcom/google/android/play/search/a;

    .line 56
    return-void
.end method

.method static synthetic a(Lcom/google/android/play/search/PlaySearchNavigationButton;)I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->c:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/play/search/PlaySearchNavigationButton;)Lcom/google/android/play/search/d;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->b:Lcom/google/android/play/search/d;

    return-object v0
.end method

.method private b(I)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v1, 0x1

    .line 101
    iget v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->c:I

    if-ne v0, p1, :cond_0

    .line 119
    :goto_0
    return-void

    .line 105
    :cond_0
    if-ne p1, v1, :cond_1

    move v0, v1

    .line 106
    :goto_1
    sget-boolean v3, Lcom/google/android/play/search/PlaySearchNavigationButton;->a:Z

    if-eqz v3, :cond_3

    .line 107
    if-eqz v0, :cond_2

    const/4 v3, 0x0

    :goto_2
    new-array v5, v8, [F

    fill-array-data v5, :array_0

    invoke-static {v5}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v5

    new-instance v6, Lcom/google/android/play/search/g;

    invoke-direct {v6, p0}, Lcom/google/android/play/search/g;-><init>(Lcom/google/android/play/search/PlaySearchNavigationButton;)V

    invoke-virtual {v5, v6}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    const-wide/16 v6, 0x15e

    invoke-virtual {v5, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    sget-object v6, Lcom/google/android/play/search/b;->a:Lcom/google/android/play/search/b;

    invoke-virtual {v5, v6}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-array v6, v8, [F

    sub-float/2addr v4, v3

    aput v4, v6, v2

    aput v3, v6, v1

    invoke-virtual {v5, v6}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    invoke-virtual {v5}, Landroid/animation/ValueAnimator;->start()V

    .line 112
    :goto_3
    if-eqz v0, :cond_4

    .line 113
    sget v0, Lcom/google/android/play/i;->c:I

    .line 117
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 118
    iput p1, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->c:I

    goto :goto_0

    :cond_1
    move v0, v2

    .line 105
    goto :goto_1

    :cond_2
    move v3, v4

    .line 107
    goto :goto_2

    .line 109
    :cond_3
    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->d:Lcom/google/android/play/search/a;

    invoke-virtual {v1, v0}, Lcom/google/android/play/search/a;->a(Z)V

    goto :goto_3

    .line 115
    :cond_4
    sget v0, Lcom/google/android/play/i;->a:I

    goto :goto_4

    .line 107
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic c(Lcom/google/android/play/search/PlaySearchNavigationButton;)Lcom/google/android/play/search/a;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->d:Lcom/google/android/play/search/a;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 88
    if-ne p1, v0, :cond_1

    .line 89
    invoke-direct {p0, v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->b(I)V

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    if-ne p1, v1, :cond_0

    .line 91
    invoke-direct {p0, v1}, Lcom/google/android/play/search/PlaySearchNavigationButton;->b(I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/play/search/d;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->b:Lcom/google/android/play/search/d;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->b:Lcom/google/android/play/search/d;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/d;->b(Lcom/google/android/play/search/e;)V

    .line 82
    :cond_0
    iput-object p1, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->b:Lcom/google/android/play/search/d;

    .line 83
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->b:Lcom/google/android/play/search/d;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/d;->a(Lcom/google/android/play/search/e;)V

    .line 84
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 60
    invoke-super {p0}, Landroid/widget/ImageView;->onFinishInflate()V

    .line 61
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->d:Lcom/google/android/play/search/a;

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->d:Lcom/google/android/play/search/a;

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/a;->a(Z)V

    .line 63
    new-instance v0, Lcom/google/android/play/search/f;

    invoke-direct {v0, p0}, Lcom/google/android/play/search/f;-><init>(Lcom/google/android/play/search/PlaySearchNavigationButton;)V

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    invoke-direct {p0, v1}, Lcom/google/android/play/search/PlaySearchNavigationButton;->b(I)V

    .line 73
    return-void
.end method
