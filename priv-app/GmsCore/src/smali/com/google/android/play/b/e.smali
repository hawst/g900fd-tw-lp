.class Lcom/google/android/play/b/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/play/b/b;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x5
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/android/play/b/e;-><init>()V

    return-void
.end method

.method protected static a(Landroid/content/res/TypedArray;)F
    .locals 2

    .prologue
    .line 133
    sget v0, Lcom/google/android/play/k;->ap:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    return v0
.end method

.method protected static a(Landroid/content/Context;Landroid/util/AttributeSet;I)Landroid/content/res/TypedArray;
    .locals 2

    .prologue
    .line 124
    sget-object v0, Lcom/google/android/play/k;->am:[I

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    return-object v0
.end method

.method protected static b(Landroid/content/res/TypedArray;)F
    .locals 2

    .prologue
    .line 137
    sget v0, Lcom/google/android/play/k;->aq:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    return v0
.end method

.method protected static c(Landroid/content/res/TypedArray;)I
    .locals 2

    .prologue
    .line 141
    sget v0, Lcom/google/android/play/k;->ar:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 146
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 147
    instance-of v1, v0, Lcom/google/android/play/b/a;

    if-eqz v1, :cond_0

    .line 148
    check-cast v0, Lcom/google/android/play/b/a;

    invoke-virtual {v0, p2}, Lcom/google/android/play/b/a;->a(I)V

    .line 153
    :goto_0
    return-void

    .line 150
    :cond_0
    const-string v0, "CardViewGroupDelegates"

    const-string v1, "Unable to set background color. CardView is not using a CardViewBackgroundDrawable"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    .line 111
    invoke-static {p2, p3, p4}, Lcom/google/android/play/b/e;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)Landroid/content/res/TypedArray;

    move-result-object v6

    .line 112
    new-instance v0, Lcom/google/android/play/b/h;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/play/k;->an:I

    invoke-virtual {v6, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-static {v6}, Lcom/google/android/play/b/e;->a(Landroid/content/res/TypedArray;)F

    move-result v3

    invoke-static {v6}, Lcom/google/android/play/b/e;->b(Landroid/content/res/TypedArray;)F

    move-result v4

    invoke-static {v6}, Lcom/google/android/play/b/e;->c(Landroid/content/res/TypedArray;)I

    move-result v5

    int-to-float v5, v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/b/h;-><init>(Landroid/content/res/Resources;Landroid/content/res/ColorStateList;FFF)V

    .line 119
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 120
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    .line 121
    return-void
.end method

.method public final b(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 157
    if-nez p2, :cond_0

    .line 174
    :goto_0
    return-void

    .line 160
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 161
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 162
    instance-of v2, v0, Lcom/google/android/play/b/a;

    if-eqz v2, :cond_1

    .line 164
    :try_start_0
    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 165
    check-cast v0, Lcom/google/android/play/b/a;

    invoke-virtual {v0, v1}, Lcom/google/android/play/b/a;->a(Landroid/content/res/ColorStateList;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 167
    :catch_0
    move-exception v0

    .line 168
    const-string v1, "CardViewGroupDelegates"

    const-string v2, "Unable to set background - ColorStateList not found."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 171
    :cond_1
    const-string v0, "CardViewGroupDelegates"

    const-string v1, "Unable to set background. CardView is not using a CardViewBackgroundDrawable."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
