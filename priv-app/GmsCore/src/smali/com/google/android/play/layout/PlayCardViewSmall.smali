.class public Lcom/google/android/play/layout/PlayCardViewSmall;
.super Lcom/google/android/play/layout/m;
.source "SourceFile"


# instance fields
.field private A:I

.field protected w:Landroid/view/View;

.field private x:I

.field private final y:I

.field private final z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayCardViewSmall;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/m;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->x:I

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 54
    sget v1, Lcom/google/android/play/e;->A:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->y:I

    .line 55
    sget v1, Lcom/google/android/play/e;->j:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->z:I

    .line 56
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Lcom/google/android/play/layout/m;->onFinishInflate()V

    .line 62
    sget v0, Lcom/google/android/play/g;->Y:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewSmall;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->w:Landroid/view/View;

    .line 63
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 24

    .prologue
    .line 195
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getPaddingLeft()I

    move-result v11

    .line 196
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getPaddingRight()I

    move-result v12

    .line 197
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getPaddingTop()I

    move-result v13

    .line 198
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getPaddingBottom()I

    move-result v14

    .line 200
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getWidth()I

    move-result v15

    .line 201
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getHeight()I

    move-result v16

    .line 203
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->h:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v5}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 205
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->h:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v6}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v17

    .line 206
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->h:Lcom/google/android/play/layout/PlayCardThumbnail;

    iget v7, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v7, v11

    iget v8, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v8, v13

    iget v9, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v9, v11

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->h:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v10}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredWidth()I

    move-result v10

    add-int/2addr v9, v10

    iget v5, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v5, v13

    add-int v5, v5, v17

    invoke-virtual {v6, v7, v8, v9, v5}, Lcom/google/android/play/layout/PlayCardThumbnail;->layout(IIII)V

    .line 211
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->i:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 213
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->j:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v6}, Lcom/google/android/play/layout/PlayTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 215
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->w:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 217
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v8}, Lcom/google/android/play/layout/PlayCardLabelView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 219
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->o:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 221
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->q:Lcom/google/android/play/layout/PlayCardSnippet;

    invoke-virtual {v10}, Lcom/google/android/play/layout/PlayCardSnippet;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 224
    add-int v17, v17, v13

    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v18, v0

    add-int v17, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->A:I

    move/from16 v18, v0

    add-int v17, v17, v18

    .line 225
    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v18, v0

    add-int v18, v18, v11

    .line 226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->i:Landroid/widget/TextView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v19

    .line 227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->i:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->i:Landroid/widget/TextView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v21

    add-int v21, v21, v18

    add-int v22, v17, v19

    move-object/from16 v0, v20

    move/from16 v1, v18

    move/from16 v2, v17

    move/from16 v3, v21

    move/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 230
    iget v0, v9, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v20, v0

    add-int v20, v20, v17

    .line 231
    sub-int v21, v15, v12

    iget v9, v9, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v9, v21, v9

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->o:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->o:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v22

    sub-int v22, v9, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->o:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v23

    add-int v23, v23, v20

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v20

    move/from16 v3, v23

    invoke-virtual {v0, v1, v2, v9, v3}, Landroid/widget/ImageView;->layout(IIII)V

    .line 237
    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->A:I

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-static {v9, v0}, Ljava/lang/Math;->max(II)I

    move-result v20

    .line 240
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v9}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredHeight()I

    move-result v21

    .line 241
    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->x:I

    if-nez v9, :cond_2

    sub-int v9, v16, v14

    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v22, v0

    sub-int v9, v9, v22

    sub-int v9, v9, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->A:I

    move/from16 v22, v0

    sub-int v9, v9, v22

    .line 244
    :goto_0
    sub-int v22, v15, v12

    iget v8, v8, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v8, v22, v8

    .line 245
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredWidth()I

    move-result v23

    sub-int v23, v8, v23

    add-int v21, v21, v9

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v21

    invoke-virtual {v0, v1, v9, v8, v2}, Lcom/google/android/play/layout/PlayCardLabelView;->layout(IIII)V

    .line 248
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->j:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v8}, Lcom/google/android/play/layout/PlayTextView;->getVisibility()I

    move-result v8

    const/16 v21, 0x8

    move/from16 v0, v21

    if-eq v8, v0, :cond_0

    .line 250
    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->x:I

    if-nez v8, :cond_3

    add-int v8, v17, v19

    iget v5, v5, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v5, v8

    iget v6, v6, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v5, v6

    add-int v5, v5, v20

    .line 254
    :goto_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->j:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->j:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v8}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredWidth()I

    move-result v8

    add-int v8, v8, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->j:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v17

    add-int v17, v17, v5

    move/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v6, v0, v5, v8, v1}, Lcom/google/android/play/layout/PlayTextView;->layout(IIII)V

    .line 259
    :cond_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v5}, Lcom/google/android/play/layout/PlayCardLabelView;->getBaseline()I

    move-result v5

    add-int/2addr v5, v9

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->w:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getBaseline()I

    move-result v6

    sub-int/2addr v5, v6

    .line 260
    iget v6, v7, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v6, v11

    .line 261
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->w:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->w:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v8, v6

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->w:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v5

    invoke-virtual {v7, v6, v5, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 265
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->q:Lcom/google/android/play/layout/PlayCardSnippet;

    invoke-virtual {v5}, Lcom/google/android/play/layout/PlayCardSnippet;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_1

    .line 266
    sub-int v5, v16, v14

    iget v6, v10, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->A:I

    sub-int/2addr v5, v6

    .line 267
    iget v6, v10, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v6, v11

    .line 268
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->q:Lcom/google/android/play/layout/PlayCardSnippet;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->q:Lcom/google/android/play/layout/PlayCardSnippet;

    invoke-virtual {v8}, Lcom/google/android/play/layout/PlayCardSnippet;->getMeasuredHeight()I

    move-result v8

    sub-int v8, v5, v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->q:Lcom/google/android/play/layout/PlayCardSnippet;

    invoke-virtual {v9}, Lcom/google/android/play/layout/PlayCardSnippet;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v9, v6

    invoke-virtual {v7, v6, v8, v9, v5}, Lcom/google/android/play/layout/PlayCardSnippet;->layout(IIII)V

    .line 272
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->r:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 273
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->r:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 274
    sub-int v7, v15, v11

    sub-int/2addr v7, v12

    sub-int v5, v7, v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v5, v11

    .line 275
    sub-int v7, v16, v13

    sub-int/2addr v7, v14

    sub-int v6, v7, v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v6, v13

    .line 276
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->r:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->r:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v8, v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->r:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v6

    invoke-virtual {v7, v5, v6, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 280
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->a()V

    .line 281
    return-void

    .line 241
    :cond_2
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->i:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getBottom()I

    move-result v9

    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v22, v0

    add-int v9, v9, v22

    add-int v9, v9, v20

    goto/16 :goto_0

    .line 250
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v5}, Lcom/google/android/play/layout/PlayCardLabelView;->getBaseline()I

    move-result v5

    add-int/2addr v5, v9

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/play/layout/PlayCardViewSmall;->j:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v6}, Lcom/google/android/play/layout/PlayTextView;->getBaseline()I

    move-result v6

    sub-int/2addr v5, v6

    goto/16 :goto_1
.end method

.method protected onMeasure(II)V
    .locals 15

    .prologue
    .line 83
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/play/layout/PlayCardViewSmall;->a(I)V

    .line 85
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    .line 86
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getPaddingLeft()I

    move-result v0

    .line 87
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getPaddingRight()I

    move-result v1

    .line 88
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getPaddingTop()I

    move-result v8

    .line 89
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewSmall;->getPaddingBottom()I

    move-result v9

    .line 90
    sub-int v0, v7, v0

    sub-int v10, v0, v1

    .line 92
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->h:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 94
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 95
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 100
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    add-int/2addr v2, v8

    iget v4, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->y:I

    div-int/lit8 v5, v10, 0x2

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v2, v9

    .line 102
    const/high16 v4, 0x40000000    # 2.0f

    if-ne v3, v4, :cond_3

    if-lez v1, :cond_3

    move v6, v1

    .line 105
    :goto_0
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v1, v10, v1

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v1, v2

    .line 106
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->h:Lcom/google/android/play/layout/PlayCardThumbnail;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2, v1, v3}, Lcom/google/android/play/layout/PlayCardThumbnail;->measure(II)V

    .line 109
    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->i:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 111
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->j:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v2}, Lcom/google/android/play/layout/PlayTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 113
    iget-object v3, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v3}, Lcom/google/android/play/layout/PlayCardLabelView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 115
    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->w:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 117
    iget-object v5, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->q:Lcom/google/android/play/layout/PlayCardSnippet;

    invoke-virtual {v5}, Lcom/google/android/play/layout/PlayCardSnippet;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 121
    iget-object v11, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    const/high16 v12, -0x80000000

    invoke-static {v10, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Lcom/google/android/play/layout/PlayCardLabelView;->measure(II)V

    .line 123
    iget-object v11, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v11}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredWidth()I

    move-result v11

    iget v12, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v11, v12

    iget v12, v3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v11, v12

    .line 126
    iget-object v12, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->o:Landroid/widget/ImageView;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/widget/ImageView;->measure(II)V

    .line 129
    iget v12, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v12, v10, v12

    iget v13, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v12, v13

    .line 130
    iget-object v13, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->i:Landroid/widget/TextView;

    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v12, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    const/4 v14, 0x0

    invoke-virtual {v13, v12, v14}, Landroid/widget/TextView;->measure(II)V

    .line 133
    iget-object v12, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->r:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getVisibility()I

    move-result v12

    if-eqz v12, :cond_7

    .line 135
    sub-int v12, v10, v11

    iget v13, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v12, v13

    iget v4, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v4, v12, v4

    .line 137
    iget-object v12, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->w:Landroid/view/View;

    const/high16 v13, -0x80000000

    invoke-static {v4, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    const/4 v13, 0x0

    invoke-virtual {v12, v4, v13}, Landroid/view/View;->measure(II)V

    .line 141
    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->j:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayTextView;->getVisibility()I

    move-result v4

    const/16 v12, 0x8

    if-eq v4, v12, :cond_0

    .line 144
    iget v4, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->x:I

    if-nez v4, :cond_4

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v4, v10, v4

    iget v11, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v4, v11

    .line 150
    :goto_1
    iget-object v11, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->j:Lcom/google/android/play/layout/PlayTextView;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Lcom/google/android/play/layout/PlayTextView;->measure(II)V

    .line 151
    iget-object v11, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->j:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v11}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredWidth()I

    move-result v11

    .line 152
    if-le v11, v4, :cond_0

    .line 153
    iget-object v11, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->j:Lcom/google/android/play/layout/PlayTextView;

    const/high16 v12, 0x40000000    # 2.0f

    invoke-static {v4, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    const/4 v12, 0x0

    invoke-virtual {v11, v4, v12}, Lcom/google/android/play/layout/PlayTextView;->measure(II)V

    .line 158
    :cond_0
    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->q:Lcom/google/android/play/layout/PlayCardSnippet;

    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayCardSnippet;->getVisibility()I

    move-result v4

    const/16 v11, 0x8

    if-eq v4, v11, :cond_1

    .line 159
    iget v4, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v4, v10, v4

    iget v10, v5, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v4, v10

    .line 160
    iget-object v10, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->q:Lcom/google/android/play/layout/PlayCardSnippet;

    const/high16 v11, 0x40000000    # 2.0f

    invoke-static {v4, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    const/high16 v11, 0x40000000    # 2.0f

    invoke-virtual {v10, v4, v11}, Lcom/google/android/play/layout/PlayCardSnippet;->measure(II)V

    .line 164
    :cond_1
    iget v4, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v10, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->i:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v10

    add-int/2addr v4, v10

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v1, v4

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v1, v4

    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->j:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v1, v4

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v1, v2

    .line 167
    iget v2, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->x:I

    if-nez v2, :cond_5

    .line 168
    iget v2, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->m:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v2, v4

    iget v3, v3, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 175
    :cond_2
    :goto_2
    sub-int v2, v6, v8

    sub-int/2addr v2, v9

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    sub-int v0, v2, v0

    sub-int/2addr v0, v1

    .line 181
    if-gtz v0, :cond_6

    div-int/lit8 v0, v0, 0x2

    :goto_3
    iput v0, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->A:I

    .line 188
    :goto_4
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->r:Landroid/view/View;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 190
    invoke-virtual {p0, v7, v6}, Lcom/google/android/play/layout/PlayCardViewSmall;->setMeasuredDimension(II)V

    .line 191
    return-void

    :cond_3
    move v6, v2

    .line 102
    goto/16 :goto_0

    .line 144
    :cond_4
    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v4, v10, v4

    sub-int/2addr v4, v11

    goto/16 :goto_1

    .line 170
    :cond_5
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->q:Lcom/google/android/play/layout/PlayCardSnippet;

    invoke-virtual {v2}, Lcom/google/android/play/layout/PlayCardSnippet;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_2

    .line 171
    iget v2, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v3, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->q:Lcom/google/android/play/layout/PlayCardSnippet;

    invoke-virtual {v3}, Lcom/google/android/play/layout/PlayCardSnippet;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v2, v3

    iget v3, v5, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    goto :goto_2

    .line 181
    :cond_6
    div-int/lit8 v0, v0, 0x4

    iget v1, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->z:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_3

    .line 185
    :cond_7
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/layout/PlayCardViewSmall;->A:I

    goto :goto_4
.end method
