.class public final Lcom/google/android/play/layout/FlowLayoutManager;
.super Landroid/support/v7/widget/ce;
.source "SourceFile"


# static fields
.field private static final d:Landroid/graphics/Rect;


# instance fields
.field private a:Z

.field private final b:Ljava/util/List;

.field private c:Z

.field private e:Lcom/google/android/play/layout/b;

.field private f:Lcom/google/android/play/layout/i;

.field private g:Z

.field private h:I

.field private i:I

.field private j:Lcom/google/android/play/layout/FlowLayoutManager$SavedState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2214
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/play/layout/FlowLayoutManager;->d:Landroid/graphics/Rect;

    return-void
.end method

.method private a(IILcom/google/android/play/layout/d;IILandroid/support/v7/widget/cj;ZLcom/google/android/play/layout/h;)I
    .locals 7

    .prologue
    .line 3365
    invoke-direct {p0, p6, p4, p4, p5}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/support/v7/widget/cj;III)I

    move-result v3

    .line 3366
    invoke-virtual {p0, v3}, Lcom/google/android/play/layout/FlowLayoutManager;->d(I)Landroid/view/View;

    move-result-object v4

    .line 3367
    iget v0, p3, Lcom/google/android/play/layout/d;->l:I

    .line 3368
    if-eqz p8, :cond_3

    iget-boolean v1, p8, Lcom/google/android/play/layout/h;->b:Z

    if-eqz v1, :cond_3

    iget v1, p8, Lcom/google/android/play/layout/h;->m:I

    if-lez v1, :cond_3

    .line 3377
    iget v0, p3, Lcom/google/android/play/layout/d;->k:I

    iget v1, p3, Lcom/google/android/play/layout/d;->l:I

    iget v2, p8, Lcom/google/android/play/layout/h;->m:I

    add-int/2addr v1, v2

    invoke-direct {p0, v4, v0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->b(Landroid/view/View;II)V

    .line 3379
    invoke-static {v4}, Lcom/google/android/play/layout/FlowLayoutManager;->c(Landroid/view/View;)I

    move-result v0

    .line 3387
    :cond_0
    :goto_0
    iget v1, p3, Lcom/google/android/play/layout/d;->o:I

    add-int v5, p1, v1

    .line 3388
    add-int v6, v5, v0

    .line 3389
    iget v0, p3, Lcom/google/android/play/layout/d;->f:I

    add-int v2, p2, v0

    .line 3390
    iget v0, p3, Lcom/google/android/play/layout/d;->k:I

    add-int/2addr v0, v2

    .line 3391
    if-eqz p7, :cond_4

    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->k()I

    move-result v1

    sub-int/2addr v1, v0

    .line 3392
    :goto_1
    if-eqz p7, :cond_1

    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->k()I

    move-result v0

    sub-int/2addr v0, v2

    .line 3393
    :cond_1
    invoke-static {v4, v1, v5, v0, v6}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/view/View;IIII)V

    .line 3395
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->f:Lcom/google/android/play/layout/i;

    if-eqz v0, :cond_2

    .line 3398
    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)Landroid/support/v7/widget/cs;

    .line 3400
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->f:Lcom/google/android/play/layout/i;

    .line 3403
    :cond_2
    return v3

    .line 3380
    :cond_3
    iget-boolean v1, p3, Lcom/google/android/play/layout/d;->n:Z

    if-nez v1, :cond_0

    .line 3383
    iget v0, p3, Lcom/google/android/play/layout/d;->k:I

    iget v1, p3, Lcom/google/android/play/layout/d;->l:I

    invoke-direct {p0, v4, v0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->b(Landroid/view/View;II)V

    .line 3384
    const/4 v0, 0x1

    invoke-virtual {p3, v4, v0}, Lcom/google/android/play/layout/d;->a(Landroid/view/View;Z)V

    .line 3385
    iget v0, p3, Lcom/google/android/play/layout/d;->l:I

    goto :goto_0

    :cond_4
    move v1, v2

    .line 3391
    goto :goto_1
.end method

.method private a(ILcom/google/android/play/layout/f;ILandroid/support/v7/widget/cj;Z)I
    .locals 11

    .prologue
    .line 3317
    instance-of v0, p2, Lcom/google/android/play/layout/c;

    if-eqz v0, :cond_0

    .line 3318
    check-cast p2, Lcom/google/android/play/layout/c;

    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->q()I

    move-result v0

    iget v1, p2, Lcom/google/android/play/layout/c;->d:I

    add-int v2, v0, v1

    iget-object v0, p2, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v10

    const/4 v0, 0x0

    move v9, v0

    move v5, p3

    :goto_0
    if-ge v9, v10, :cond_3

    iget-object v0, p2, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/play/layout/d;

    iget v0, p2, Lcom/google/android/play/layout/c;->e:I

    add-int v4, v0, v9

    const/4 v8, 0x0

    move-object v0, p0

    move v1, p1

    move-object v6, p4

    move/from16 v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/play/layout/FlowLayoutManager;->a(IILcom/google/android/play/layout/d;IILandroid/support/v7/widget/cj;ZLcom/google/android/play/layout/h;)I

    move-result v0

    add-int/lit8 v5, v0, 0x1

    iget v0, v3, Lcom/google/android/play/layout/d;->f:I

    iget v1, v3, Lcom/google/android/play/layout/d;->k:I

    add-int/2addr v0, v1

    iget v1, v3, Lcom/google/android/play/layout/d;->g:I

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0

    :cond_0
    move-object v8, p2

    .line 3321
    check-cast v8, Lcom/google/android/play/layout/h;

    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->q()I

    move-result v0

    iget v1, v8, Lcom/google/android/play/layout/h;->d:I

    add-int v2, v0, v1

    iget-object v3, v8, Lcom/google/android/play/layout/h;->a:Lcom/google/android/play/layout/d;

    iget v4, v8, Lcom/google/android/play/layout/h;->e:I

    move-object v0, p0

    move v1, p1

    move v5, p3

    move-object v6, p4

    move/from16 v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/play/layout/FlowLayoutManager;->a(IILcom/google/android/play/layout/d;IILandroid/support/v7/widget/cj;ZLcom/google/android/play/layout/h;)I

    move-result v0

    add-int/lit8 v3, v0, 0x1

    iget-object v0, v8, Lcom/google/android/play/layout/h;->l:Lcom/google/android/play/layout/j;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    move v6, v0

    :goto_1
    iget-object v0, v8, Lcom/google/android/play/layout/h;->a:Lcom/google/android/play/layout/d;

    iget v0, v0, Lcom/google/android/play/layout/d;->o:I

    add-int/2addr v0, p1

    iget v1, v8, Lcom/google/android/play/layout/h;->j:I

    add-int/2addr v1, v0

    const/4 v0, 0x0

    move v7, v0

    :goto_2
    if-ge v7, v6, :cond_2

    iget-object v0, v8, Lcom/google/android/play/layout/h;->l:Lcom/google/android/play/layout/j;

    iget-object v0, v0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/f;

    move-object v0, p0

    move-object v4, p4

    move/from16 v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/layout/FlowLayoutManager;->a(ILcom/google/android/play/layout/f;ILandroid/support/v7/widget/cj;Z)I

    move-result v3

    iget v0, v2, Lcom/google/android/play/layout/f;->g:I

    add-int/2addr v1, v0

    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_2

    :cond_1
    iget-object v0, v8, Lcom/google/android/play/layout/h;->l:Lcom/google/android/play/layout/j;

    iget-object v0, v0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    move v6, v0

    goto :goto_1

    :cond_2
    move v5, v3

    :cond_3
    return v5
.end method

.method private a(Landroid/support/v7/widget/cj;III)I
    .locals 11

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 2037
    sub-int v3, p2, p3

    packed-switch v3, :pswitch_data_0

    move v3, v1

    .line 2066
    :goto_0
    if-ltz v3, :cond_4

    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->j()I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 2067
    invoke-virtual {p0, v3}, Lcom/google/android/play/layout/FlowLayoutManager;->d(I)Landroid/view/View;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/view/View;)I

    move-result v6

    .line 2068
    if-ne v6, p2, :cond_1

    .line 2170
    :cond_0
    return v3

    :pswitch_0
    move v1, p4

    move v3, p4

    .line 2044
    goto :goto_0

    .line 2049
    :pswitch_1
    add-int/lit8 v1, p4, -0x1

    move v3, v1

    move v1, p4

    .line 2051
    goto :goto_0

    .line 2057
    :pswitch_2
    add-int/lit8 v1, p4, 0x1

    move v3, v1

    .line 2059
    goto :goto_0

    .line 2077
    :cond_1
    if-ne v3, v1, :cond_2

    move v5, v0

    :goto_1
    if-le v6, p2, :cond_3

    move v4, v0

    :goto_2
    if-eq v5, v4, :cond_4

    .line 2079
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Wrong hint precondition.\n\t position="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n\t positionHint="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n\t indexHint="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n\t potentialIndex="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\t insertIndex="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\t realChildPosition(potentialIndex)="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v5, v2

    .line 2077
    goto :goto_1

    :cond_3
    move v4, v2

    goto :goto_2

    .line 2094
    :cond_4
    if-gez v1, :cond_8

    .line 2095
    invoke-direct {p0, p2}, Lcom/google/android/play/layout/FlowLayoutManager;->i(I)I

    move-result v3

    .line 2097
    if-gez v3, :cond_0

    .line 2103
    xor-int/lit8 v1, v3, -0x1

    move v4, v3

    move v3, v1

    move v1, v0

    .line 2106
    :goto_3
    invoke-virtual {p1, p2}, Landroid/support/v7/widget/cj;->b(I)Landroid/view/View;

    move-result-object v5

    .line 2109
    :try_start_0
    invoke-static {v5}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/view/View;)I

    move-result v0

    .line 2110
    if-eq v0, p2, :cond_5

    .line 2111
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Recycler.getViewForPosition("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") returned a view @"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2116
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 2119
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/e;

    .line 2120
    if-nez v0, :cond_6

    const-string v0, "failed: no LayoutParams"

    .line 2122
    :goto_4
    const-string v5, "FlowLayoutManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getOrAddChildWithHint() states at exception:\n\t position="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n\t positionHint="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n\t indexHint="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n\t potentialIndex="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "\n\t insertIndex="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\t usedBinarySearch="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n\t child\'s viewHolderDump="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2131
    throw v2

    .line 2115
    :cond_5
    :try_start_1
    invoke-virtual {p0, v5, v3}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/view/View;I)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2136
    add-int/lit8 v0, v3, -0x1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 2137
    invoke-virtual {p0, v6}, Lcom/google/android/play/layout/FlowLayoutManager;->d(I)Landroid/view/View;

    move-result-object v0

    .line 2138
    invoke-static {v0}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/view/View;)I

    move-result v5

    .line 2139
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/e;

    invoke-virtual {v0}, Lcom/google/android/play/layout/e;->h()Ljava/lang/String;

    move-result-object v2

    .line 2141
    add-int/lit8 v0, v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->j()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 2142
    add-int/lit8 v0, v6, 0x1

    move v7, v5

    move-object v5, v2

    move v2, v0

    :goto_5
    if-gt v2, v9, :cond_0

    .line 2143
    invoke-virtual {p0, v2}, Lcom/google/android/play/layout/FlowLayoutManager;->d(I)Landroid/view/View;

    move-result-object v0

    .line 2144
    invoke-static {v0}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/view/View;)I

    move-result v8

    .line 2145
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/e;

    invoke-virtual {v0}, Lcom/google/android/play/layout/e;->h()Ljava/lang/String;

    move-result-object v6

    .line 2147
    if-gt v8, v7, :cond_7

    .line 2148
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Index/position monotonicity broken!\n\t position="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\n\t positionHint="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\n\t indexHint="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\n\t potentialIndex="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, "\n\t insertIndex="

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\t usedBinarySearch="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n\t p(childAt("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 v3, v2, -0x1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "))="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n\t   viewHolderDump="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n\t p(childAt("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "))="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\t   viewHolderDump="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2120
    :cond_6
    invoke-virtual {v0}, Lcom/google/android/play/layout/e;->h()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 2142
    :cond_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-object v5, v6

    move v7, v8

    goto/16 :goto_5

    :cond_8
    move v4, v3

    move v3, v1

    move v1, v2

    goto/16 :goto_3

    .line 2037
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private a(Landroid/support/v7/widget/cj;Landroid/support/v7/widget/cp;II)I
    .locals 19

    .prologue
    .line 3077
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Landroid/support/v7/widget/cp;->a()I

    move-result v10

    .line 3079
    if-nez v10, :cond_0

    .line 3080
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/play/layout/FlowLayoutManager;->b(Landroid/support/v7/widget/cj;)V

    .line 3081
    invoke-direct/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->b()V

    .line 3082
    const/4 v2, 0x0

    .line 3251
    :goto_0
    return v2

    .line 3087
    :cond_0
    if-ltz p3, :cond_3

    move/from16 v0, p3

    if-ge v0, v10, :cond_3

    const/4 v2, 0x1

    move v8, v2

    .line 3090
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->n()I

    move-result v7

    .line 3091
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->l()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->p()I

    move-result v3

    sub-int v9, v2, v3

    .line 3095
    sub-int v2, v9, v7

    div-int/lit8 v3, v2, 0x2

    .line 3096
    if-eqz v8, :cond_4

    sub-int v2, v7, v3

    move/from16 v16, v2

    .line 3098
    :goto_2
    add-int v11, v9, v3

    .line 3102
    move-object/from16 v0, p2

    iget-boolean v2, v0, Landroid/support/v7/widget/cp;->h:Z

    if-eqz v2, :cond_1

    .line 3103
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/support/v7/widget/cj;)V

    .line 3109
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->j()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v3, v2

    :goto_3
    if-ltz v3, :cond_5

    .line 3110
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/play/layout/FlowLayoutManager;->d(I)Landroid/view/View;

    move-result-object v4

    .line 3111
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/e;

    .line 3112
    invoke-virtual {v2}, Lcom/google/android/play/layout/e;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3117
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ce;->p:Landroid/support/v7/widget/x;

    invoke-virtual {v2, v4}, Landroid/support/v7/widget/x;->a(Landroid/view/View;)I

    move-result v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-super {v0, v1, v2, v4}, Landroid/support/v7/widget/ce;->a(Landroid/support/v7/widget/cj;ILandroid/view/View;)V

    .line 3109
    :cond_2
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_3

    .line 3087
    :cond_3
    const/4 v2, 0x0

    move v8, v2

    goto :goto_1

    :cond_4
    move/from16 v16, v7

    .line 3096
    goto :goto_2

    .line 3122
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->k()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 3123
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->q()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 3124
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ce;->q:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ce;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2}, Landroid/support/v4/view/ay;->n(Landroid/view/View;)I

    move-result v2

    :goto_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 3125
    sget v5, Lcom/google/android/play/g;->q:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/cp;->a(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    sget v5, Lcom/google/android/play/g;->p:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/cp;->a(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    sget v5, Lcom/google/android/play/g;->o:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/cp;->a(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 3128
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->b()V

    .line 3129
    sget v5, Lcom/google/android/play/g;->q:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v3}, Landroid/support/v7/widget/cp;->a(ILjava/lang/Object;)V

    .line 3130
    sget v3, Lcom/google/android/play/g;->p:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/support/v7/widget/cp;->a(ILjava/lang/Object;)V

    .line 3131
    sget v3, Lcom/google/android/play/g;->o:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v2}, Landroid/support/v7/widget/cp;->a(ILjava/lang/Object;)V

    .line 3138
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v3, v2

    :goto_5
    if-ltz v3, :cond_9

    .line 3139
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/j;

    invoke-virtual {v2}, Lcom/google/android/play/layout/j;->c()V

    .line 3138
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_5

    .line 3124
    :cond_8
    const/4 v2, 0x0

    goto :goto_4

    .line 3144
    :cond_9
    if-eqz v8, :cond_c

    move/from16 v6, p3

    .line 3145
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/j;

    iget v2, v2, Lcom/google/android/play/layout/j;->e:I

    if-le v2, v6, :cond_d

    :cond_a
    const/4 v2, -0x1

    .line 3146
    :goto_7
    if-gez v2, :cond_b

    .line 3147
    xor-int/lit8 v2, v2, -0x1

    add-int/lit8 v2, v2, -0x1

    .line 3149
    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v6, v10}, Lcom/google/android/play/layout/FlowLayoutManager;->b(Landroid/support/v7/widget/cj;III)I

    move-result v13

    .line 3152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v2, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/j;

    .line 3153
    if-eqz v8, :cond_12

    invoke-virtual {v2, v6}, Lcom/google/android/play/layout/j;->f(I)I

    move-result v3

    sub-int v5, p4, v3

    .line 3158
    :goto_8
    sub-int v3, v11, v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13, v3, v10}, Lcom/google/android/play/layout/FlowLayoutManager;->c(Landroid/support/v7/widget/cj;III)I

    move-result v3

    .line 3160
    sub-int v4, v11, v3

    .line 3165
    const/4 v3, 0x0

    .line 3166
    if-eqz v8, :cond_21

    .line 3167
    const/4 v3, 0x0

    sub-int v6, v9, v4

    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 3168
    add-int/2addr v5, v3

    .line 3169
    add-int/2addr v4, v3

    move v9, v3

    move v3, v4

    move v4, v5

    :goto_9
    move-object v15, v2

    move v5, v4

    move v6, v13

    .line 3179
    :goto_a
    move/from16 v0, v16

    if-le v5, v0, :cond_14

    iget v2, v15, Lcom/google/android/play/layout/j;->e:I

    if-lez v2, :cond_14

    .line 3180
    add-int/lit8 v2, v13, -0x1

    iget v12, v15, Lcom/google/android/play/layout/j;->e:I

    add-int/lit8 v12, v12, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v12, v10}, Lcom/google/android/play/layout/FlowLayoutManager;->b(Landroid/support/v7/widget/cj;III)I

    move-result v2

    .line 3182
    if-ne v2, v13, :cond_13

    .line 3184
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    .line 3188
    :goto_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v2, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/j;

    .line 3189
    iget v12, v2, Lcom/google/android/play/layout/j;->g:I

    sub-int/2addr v5, v12

    move-object v15, v2

    .line 3190
    goto :goto_a

    .line 3144
    :cond_c
    const/4 v2, 0x0

    move v6, v2

    goto/16 :goto_6

    .line 3145
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    add-int/lit8 v4, v3, -0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/j;

    invoke-virtual {v2}, Lcom/google/android/play/layout/j;->b()I

    move-result v2

    if-gt v2, v6, :cond_e

    xor-int/lit8 v2, v3, -0x1

    goto :goto_7

    :cond_e
    const/4 v2, 0x0

    move v4, v3

    move v5, v2

    :goto_c
    if-ge v5, v4, :cond_11

    add-int v2, v5, v4

    div-int/lit8 v3, v2, 0x2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/j;

    iget v12, v2, Lcom/google/android/play/layout/j;->e:I

    if-ge v6, v12, :cond_f

    move v4, v3

    goto :goto_c

    :cond_f
    invoke-virtual {v2}, Lcom/google/android/play/layout/j;->b()I

    move-result v2

    if-lt v6, v2, :cond_10

    add-int/lit8 v2, v3, 0x1

    move v5, v2

    goto :goto_c

    :cond_10
    move v2, v3

    goto/16 :goto_7

    :cond_11
    xor-int/lit8 v2, v5, -0x1

    goto/16 :goto_7

    :cond_12
    move v5, v7

    .line 3153
    goto/16 :goto_8

    :cond_13
    move v13, v2

    .line 3186
    goto :goto_b

    .line 3194
    :cond_14
    const/4 v2, 0x0

    .line 3195
    if-eqz v8, :cond_20

    .line 3197
    const/4 v2, 0x0

    sub-int v7, v5, v7

    invoke-static {v2, v7}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 3198
    sub-int/2addr v5, v2

    .line 3199
    sub-int/2addr v4, v2

    .line 3200
    sub-int/2addr v3, v2

    .line 3205
    if-lez v2, :cond_20

    if-nez v9, :cond_20

    if-ge v3, v11, :cond_20

    .line 3207
    sub-int v3, v11, v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6, v3, v10}, Lcom/google/android/play/layout/FlowLayoutManager;->c(Landroid/support/v7/widget/cj;III)I

    move-result v3

    .line 3209
    sub-int v3, v11, v3

    move v14, v2

    move v2, v3

    move v3, v5

    .line 3220
    :goto_d
    invoke-static {v2, v11}, Ljava/lang/Math;->min(II)I

    move-result v17

    const/4 v5, -0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ce;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2}, Landroid/support/v4/view/ay;->h(Landroid/view/View;)I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_16

    const/4 v7, 0x1

    :goto_e
    move v12, v13

    :goto_f
    move/from16 v0, v17

    if-ge v3, v0, :cond_1b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v12, v2, :cond_1b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v2, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/android/play/layout/j;

    move-object v8, v0

    iget-object v2, v8, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v18

    const/4 v2, 0x0

    move v10, v2

    :goto_10
    move/from16 v0, v17

    if-ge v3, v0, :cond_1a

    move/from16 v0, v18

    if-ge v10, v0, :cond_1a

    iget-object v2, v8, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/play/layout/f;

    iget v2, v4, Lcom/google/android/play/layout/f;->g:I

    add-int v11, v3, v2

    const/4 v2, -0x1

    if-ne v5, v2, :cond_18

    move/from16 v0, v16

    if-le v11, v0, :cond_18

    iget v2, v4, Lcom/google/android/play/layout/f;->e:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/play/layout/FlowLayoutManager;->i(I)I

    move-result v2

    if-gez v2, :cond_15

    xor-int/lit8 v2, v2, -0x1

    :cond_15
    add-int/lit8 v2, v2, -0x1

    :goto_11
    if-ltz v2, :cond_17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->a(ILandroid/support/v7/widget/cj;)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_11

    :cond_16
    const/4 v7, 0x0

    goto :goto_e

    :cond_17
    const/4 v5, 0x0

    :cond_18
    const/4 v2, -0x1

    if-eq v5, v2, :cond_19

    move-object/from16 v2, p0

    move-object/from16 v6, p1

    invoke-direct/range {v2 .. v7}, Lcom/google/android/play/layout/FlowLayoutManager;->a(ILcom/google/android/play/layout/f;ILandroid/support/v7/widget/cj;Z)I

    move-result v5

    :cond_19
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    move v3, v11

    goto :goto_10

    :cond_1a
    add-int/lit8 v2, v12, 0x1

    move v12, v2

    goto :goto_f

    :cond_1b
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->j()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_12
    if-ltz v2, :cond_1c

    if-lt v2, v5, :cond_1c

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->a(ILandroid/support/v7/widget/cj;)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_12

    .line 3229
    :cond_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v12, v2, :cond_1d

    .line 3230
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v2, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/j;

    iget v2, v2, Lcom/google/android/play/layout/j;->e:I

    add-int/lit8 v4, v2, 0x5

    .line 3232
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v3, v2

    .line 3233
    :goto_13
    add-int/lit8 v2, v12, 0x2

    if-lt v3, v2, :cond_1d

    .line 3234
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/j;

    .line 3235
    iget v2, v2, Lcom/google/android/play/layout/j;->e:I

    if-lt v2, v4, :cond_1d

    .line 3236
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/play/layout/FlowLayoutManager;->a(I)V

    .line 3233
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_13

    .line 3242
    :cond_1d
    iget v2, v15, Lcom/google/android/play/layout/j;->e:I

    add-int/lit8 v4, v2, -0x5

    .line 3243
    add-int/lit8 v2, v13, -0x2

    add-int/lit8 v2, v2, -0x1

    move v3, v2

    :goto_14
    if-ltz v3, :cond_1f

    .line 3244
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/j;

    .line 3245
    iget v2, v2, Lcom/google/android/play/layout/j;->e:I

    if-ge v2, v4, :cond_1e

    .line 3246
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/play/layout/FlowLayoutManager;->a(I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3243
    :cond_1e
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_14

    .line 3251
    :cond_1f
    sub-int v2, v9, v14

    goto/16 :goto_0

    .line 3252
    :catch_0
    move-exception v2

    .line 3254
    const-string v3, "FlowLayoutManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "layoutViewport() state at exception:\n\t referencePosition="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\t referenceOffset="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\t state.getItemCount()="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Landroid/support/v7/widget/cp;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\t didStructureChange="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    iget-boolean v5, v0, Landroid/support/v7/widget/cp;->h:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3259
    invoke-direct/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->g()V

    .line 3260
    invoke-direct/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->t()V

    .line 3262
    throw v2

    :cond_20
    move v14, v2

    move v2, v3

    move v3, v5

    goto/16 :goto_d

    :cond_21
    move v9, v3

    move v3, v4

    move v4, v5

    goto/16 :goto_9
.end method

.method private a(Landroid/support/v7/widget/cj;Lcom/google/android/play/layout/c;IIZI)I
    .locals 11

    .prologue
    .line 2597
    iget-object v0, p2, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2598
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Line must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2600
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/play/layout/c;->b()I

    move-result v2

    .line 2602
    :goto_0
    if-ge v2, p3, :cond_1

    iget v0, p2, Lcom/google/android/play/layout/c;->a:I

    iget v1, p2, Lcom/google/android/play/layout/c;->b:I

    sub-int/2addr v0, v1

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 2603
    iget-object v4, p2, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    iget v5, p2, Lcom/google/android/play/layout/c;->a:I

    iget v6, p2, Lcom/google/android/play/layout/c;->b:I

    iget v7, p2, Lcom/google/android/play/layout/c;->d:I

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p4

    move/from16 v9, p5

    move/from16 v10, p6

    invoke-direct/range {v0 .. v10}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/support/v7/widget/cj;IILjava/util/List;IIIZZI)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2606
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    invoke-virtual {v0}, Lcom/google/android/play/layout/b;->a()Lcom/google/android/play/layout/d;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/android/play/layout/c;->a(Lcom/google/android/play/layout/d;)V

    .line 2609
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2611
    :cond_1
    return v2
.end method

.method private a(Landroid/support/v7/widget/cj;Lcom/google/android/play/layout/f;IIIZI)I
    .locals 7

    .prologue
    .line 2585
    instance-of v0, p2, Lcom/google/android/play/layout/c;

    if-eqz v0, :cond_0

    move-object v2, p2

    .line 2586
    check-cast v2, Lcom/google/android/play/layout/c;

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v4, p5

    move v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/support/v7/widget/cj;Lcom/google/android/play/layout/c;IIZI)I

    move-result v0

    .line 2589
    :goto_0
    return v0

    :cond_0
    move-object v2, p2

    check-cast v2, Lcom/google/android/play/layout/h;

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/support/v7/widget/cj;Lcom/google/android/play/layout/h;III)I

    move-result v0

    goto :goto_0
.end method

.method private a(Landroid/support/v7/widget/cj;Lcom/google/android/play/layout/h;III)I
    .locals 13

    .prologue
    .line 2616
    iget-object v1, p2, Lcom/google/android/play/layout/h;->a:Lcom/google/android/play/layout/d;

    if-nez v1, :cond_0

    .line 2617
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Line must not be empty"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2620
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/play/layout/h;->b()I

    move-result v12

    .line 2621
    iget-object v9, p2, Lcom/google/android/play/layout/h;->l:Lcom/google/android/play/layout/j;

    .line 2623
    if-nez v9, :cond_5

    .line 2628
    iget v1, p2, Lcom/google/android/play/layout/h;->c:I

    if-eqz v1, :cond_1

    iget v1, p2, Lcom/google/android/play/layout/h;->h:I

    if-nez v1, :cond_3

    :cond_1
    move v1, v12

    .line 2683
    :cond_2
    :goto_0
    return v1

    .line 2635
    :cond_3
    invoke-static {v12}, Lcom/google/android/play/layout/j;->a(I)Lcom/google/android/play/layout/j;

    move-result-object v3

    .line 2636
    iget v7, p2, Lcom/google/android/play/layout/h;->c:I

    iget v1, p2, Lcom/google/android/play/layout/h;->d:I

    iget v2, p2, Lcom/google/android/play/layout/h;->i:I

    add-int v8, v1, v2

    const/4 v9, 0x1

    iget v10, p2, Lcom/google/android/play/layout/h;->h:I

    move-object v1, p0

    move-object v2, p1

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    invoke-direct/range {v1 .. v10}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/support/v7/widget/cj;Lcom/google/android/play/layout/j;IIIIIZI)I

    move-result v1

    .line 2641
    iget v2, v3, Lcom/google/android/play/layout/j;->e:I

    if-ne v1, v2, :cond_4

    .line 2642
    invoke-virtual {v3}, Lcom/google/android/play/layout/j;->e()V

    goto :goto_0

    .line 2648
    :cond_4
    iput-object v3, p2, Lcom/google/android/play/layout/h;->l:Lcom/google/android/play/layout/j;

    move v11, v1

    .line 2668
    :goto_1
    iget v1, p2, Lcom/google/android/play/layout/h;->h:I

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Lcom/google/android/play/layout/j;->c(I)I

    move-result v2

    sub-int v10, v1, v2

    .line 2670
    iget v7, p2, Lcom/google/android/play/layout/h;->c:I

    iget v1, p2, Lcom/google/android/play/layout/h;->d:I

    iget v2, p2, Lcom/google/android/play/layout/h;->i:I

    add-int v8, v1, v2

    const/4 v9, 0x1

    move-object v1, p0

    move-object v2, p1

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    invoke-direct/range {v1 .. v10}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/support/v7/widget/cj;Lcom/google/android/play/layout/j;IIIIIZI)I

    move-result v1

    .line 2673
    if-gt v1, v11, :cond_8

    .line 2679
    if-le v1, v12, :cond_2

    .line 2680
    const/4 v2, -0x1

    iput v2, p2, Lcom/google/android/play/layout/g;->g:I

    goto :goto_0

    .line 2651
    :cond_5
    move/from16 v0, p4

    invoke-virtual {v9, v0}, Lcom/google/android/play/layout/j;->c(I)I

    move-result v1

    .line 2652
    invoke-virtual {v9}, Lcom/google/android/play/layout/j;->a()Lcom/google/android/play/layout/f;

    move-result-object v3

    .line 2653
    if-nez v3, :cond_6

    .line 2654
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Empty nested paragraph found!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2656
    :cond_6
    iget v2, v3, Lcom/google/android/play/layout/f;->g:I

    sub-int/2addr v1, v2

    .line 2657
    const/4 v7, 0x1

    iget v2, p2, Lcom/google/android/play/layout/h;->h:I

    sub-int v8, v2, v1

    move-object v1, p0

    move-object v2, p1

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    invoke-direct/range {v1 .. v8}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/support/v7/widget/cj;Lcom/google/android/play/layout/f;IIIZI)I

    move-result v1

    .line 2659
    if-le v1, v12, :cond_7

    .line 2660
    const/4 v2, -0x1

    iput v2, v9, Lcom/google/android/play/layout/g;->g:I

    :cond_7
    move-object v3, v9

    move v11, v1

    goto :goto_1

    :cond_8
    move v11, v1

    goto :goto_1
.end method

.method private a(Landroid/support/v7/widget/cj;Lcom/google/android/play/layout/j;IIIIIZI)I
    .locals 13

    .prologue
    .line 2699
    invoke-virtual {p2}, Lcom/google/android/play/layout/j;->b()I

    move-result v4

    .line 2700
    move/from16 v0, p3

    if-lt v4, v0, :cond_0

    .line 2701
    move/from16 v0, p3

    if-le v4, v0, :cond_1

    .line 2702
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "\u00b6@["

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p2, Lcom/google/android/play/layout/j;->e:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ","

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") should not cover nextSectionStart@"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2708
    :cond_0
    const/4 v6, 0x0

    const/4 v8, 0x0

    iget v2, p2, Lcom/google/android/play/layout/j;->e:I

    if-ne v2, v4, :cond_2

    const/4 v10, 0x1

    :goto_0
    move-object v2, p0

    move-object v3, p1

    move/from16 v5, p5

    move/from16 v7, p6

    move/from16 v9, p7

    move/from16 v11, p8

    move/from16 v12, p9

    invoke-direct/range {v2 .. v12}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/support/v7/widget/cj;IILjava/util/List;IIIZZI)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2735
    :cond_1
    :goto_1
    return v4

    .line 2708
    :cond_2
    const/4 v10, 0x0

    goto :goto_0

    .line 2715
    :cond_3
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget-object v2, v2, Lcom/google/android/play/layout/b;->f:Lcom/google/android/play/layout/e;

    .line 2716
    iget v2, v2, Lcom/google/android/play/layout/e;->s:I

    if-nez v2, :cond_4

    .line 2718
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    invoke-virtual {v2}, Lcom/google/android/play/layout/b;->a()Lcom/google/android/play/layout/d;

    move-result-object v2

    move/from16 v0, p6

    move/from16 v1, p7

    invoke-static {v4, v0, v1, v2}, Lcom/google/android/play/layout/c;->a(IIILcom/google/android/play/layout/d;)Lcom/google/android/play/layout/c;

    move-result-object v4

    move-object v2, p0

    move-object v3, p1

    move/from16 v5, p3

    move/from16 v6, p5

    move/from16 v7, p8

    move/from16 v8, p9

    .line 2720
    invoke-direct/range {v2 .. v8}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/support/v7/widget/cj;Lcom/google/android/play/layout/c;IIZI)I

    move-result v2

    .line 2722
    invoke-virtual {p2, v4}, Lcom/google/android/play/layout/j;->a(Lcom/google/android/play/layout/f;)V

    :goto_2
    move v4, v2

    .line 2735
    goto :goto_1

    .line 2725
    :cond_4
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    invoke-virtual {v2}, Lcom/google/android/play/layout/b;->a()Lcom/google/android/play/layout/d;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget-object v3, v3, Lcom/google/android/play/layout/b;->f:Lcom/google/android/play/layout/e;

    move/from16 v0, p6

    move/from16 v1, p7

    invoke-static {v4, v0, v1, v2, v3}, Lcom/google/android/play/layout/h;->a(IIILcom/google/android/play/layout/d;Lcom/google/android/play/layout/e;)Lcom/google/android/play/layout/h;

    move-result-object v4

    move-object v2, p0

    move-object v3, p1

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    .line 2727
    invoke-direct/range {v2 .. v7}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/support/v7/widget/cj;Lcom/google/android/play/layout/h;III)I

    move-result v2

    .line 2729
    invoke-virtual {p2, v4}, Lcom/google/android/play/layout/j;->a(Lcom/google/android/play/layout/f;)V

    goto :goto_2
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 1814
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/j;

    invoke-virtual {v0}, Lcom/google/android/play/layout/j;->e()V

    .line 1815
    if-nez p1, :cond_0

    .line 1820
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->c:Z

    .line 1822
    :cond_0
    return-void
.end method

.method private a(III)V
    .locals 4

    .prologue
    .line 1901
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1948
    :cond_0
    return-void

    .line 1907
    :cond_1
    const/4 v1, 0x0

    .line 1908
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_4

    .line 1909
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/j;

    .line 1910
    iget v3, v0, Lcom/google/android/play/layout/j;->e:I

    if-lt v3, p2, :cond_3

    iget v3, v0, Lcom/google/android/play/layout/j;->e:I

    if-gtz v3, :cond_2

    iget-boolean v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->c:Z

    if-eqz v3, :cond_3

    .line 1912
    :cond_2
    invoke-virtual {v0, p3}, Lcom/google/android/play/layout/j;->e(I)V

    .line 1908
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    .line 1914
    :cond_3
    add-int/lit8 v0, v2, 0x1

    .line 1928
    :goto_1
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_0

    .line 1930
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/j;

    .line 1931
    invoke-virtual {v0, p1}, Lcom/google/android/play/layout/j;->d(I)I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 1932
    invoke-direct {p0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->a(I)V

    .line 1929
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private a(Landroid/support/v7/widget/cj;Lcom/google/android/play/layout/j;IIII)V
    .locals 12

    .prologue
    .line 2517
    const/4 v2, -0x1

    if-ne p3, v2, :cond_0

    if-gtz p4, :cond_0

    .line 2518
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Both criteria met before any processing"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2519
    :cond_0
    iget v2, p2, Lcom/google/android/play/layout/j;->e:I

    move/from16 v0, p5

    if-lt v2, v0, :cond_1

    .line 2520
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Section started after limit"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2521
    :cond_1
    move/from16 v0, p5

    if-ge p3, v0, :cond_2

    move/from16 v0, p5

    move/from16 v1, p6

    if-le v0, v1, :cond_3

    .line 2522
    :cond_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "positionToCover < nextSectionStart <= totalItemCount does not hold"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2532
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->k()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->m()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->o()I

    move-result v3

    sub-int v7, v2, v3

    .line 2533
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    const/4 v3, -0x1

    iput v3, v2, Lcom/google/android/play/layout/b;->b:I

    .line 2536
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    move/from16 v0, p6

    invoke-virtual {p2, v0}, Lcom/google/android/play/layout/j;->c(I)I

    move-result v3

    iput v3, v2, Lcom/google/android/play/layout/b;->a:I

    .line 2537
    iget v2, p2, Lcom/google/android/play/layout/j;->e:I

    .line 2538
    invoke-virtual {p2}, Lcom/google/android/play/layout/j;->a()Lcom/google/android/play/layout/f;

    move-result-object v4

    .line 2539
    if-eqz v4, :cond_7

    .line 2540
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget v3, v2, Lcom/google/android/play/layout/b;->a:I

    iget v5, v4, Lcom/google/android/play/layout/f;->g:I

    sub-int/2addr v3, v5

    iput v3, v2, Lcom/google/android/play/layout/b;->a:I

    .line 2541
    iget v2, v4, Lcom/google/android/play/layout/f;->e:I

    .line 2543
    if-le v2, p3, :cond_5

    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget v2, v2, Lcom/google/android/play/layout/b;->a:I

    move/from16 v0, p4

    if-lt v2, v0, :cond_5

    .line 2575
    :cond_4
    :goto_0
    return-void

    .line 2547
    :cond_5
    invoke-virtual {v4}, Lcom/google/android/play/layout/f;->b()I

    move-result v10

    .line 2548
    const/4 v8, 0x0

    const/4 v9, -0x1

    move-object v2, p0

    move-object v3, p1

    move/from16 v5, p5

    move/from16 v6, p6

    invoke-direct/range {v2 .. v9}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/support/v7/widget/cj;Lcom/google/android/play/layout/f;IIIZI)I

    move-result v2

    .line 2550
    if-eq v2, v10, :cond_6

    .line 2552
    const/4 v3, -0x1

    iput v3, p2, Lcom/google/android/play/layout/g;->g:I

    .line 2558
    :cond_6
    :goto_1
    iget-object v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    move/from16 v0, p6

    invoke-virtual {p2, v0}, Lcom/google/android/play/layout/j;->c(I)I

    move-result v4

    iput v4, v3, Lcom/google/android/play/layout/b;->a:I

    .line 2563
    :cond_7
    if-le v2, p3, :cond_8

    iget-object v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget v3, v3, Lcom/google/android/play/layout/b;->a:I

    move/from16 v0, p4

    if-ge v3, v0, :cond_9

    :cond_8
    iget-object v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget v3, v3, Lcom/google/android/play/layout/b;->b:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_9

    move/from16 v0, p5

    if-ge v2, v0, :cond_9

    .line 2564
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, -0x1

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move/from16 v5, p5

    move/from16 v6, p6

    move v8, v7

    invoke-direct/range {v2 .. v11}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/support/v7/widget/cj;Lcom/google/android/play/layout/j;IIIIIZI)I

    move-result v2

    goto :goto_1

    .line 2571
    :cond_9
    iget-object v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget v3, v3, Lcom/google/android/play/layout/b;->b:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    if-le v2, p3, :cond_4

    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget v2, v2, Lcom/google/android/play/layout/b;->a:I

    move/from16 v0, p4

    if-lt v2, v0, :cond_4

    .line 2573
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    const/4 v3, -0x1

    iput v3, v2, Lcom/google/android/play/layout/b;->b:I

    goto :goto_0
.end method

.method private a(Landroid/support/v7/widget/cj;IILjava/util/List;IIIZZI)Z
    .locals 12

    .prologue
    .line 2750
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget v1, v1, Lcom/google/android/play/layout/b;->d:I

    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget v2, v2, Lcom/google/android/play/layout/b;->e:I

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/support/v7/widget/cj;III)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->d(I)Landroid/view/View;

    move-result-object v6

    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget v2, v2, Lcom/google/android/play/layout/b;->d:I

    if-ne v2, p2, :cond_1

    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget v2, v2, Lcom/google/android/play/layout/b;->e:I

    if-eq v2, v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Cached next child index incorrect"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget-object v1, v1, Lcom/google/android/play/layout/b;->c:Lcom/google/android/play/layout/d;

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Cached next child missing ItemInfo"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iput p2, v2, Lcom/google/android/play/layout/b;->d:I

    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iput v1, v2, Lcom/google/android/play/layout/b;->e:I

    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget-object v1, v1, Lcom/google/android/play/layout/b;->c:Lcom/google/android/play/layout/d;

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Did not consume previous ItemInfo"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/e;

    iput-object v1, v2, Lcom/google/android/play/layout/b;->f:Lcom/google/android/play/layout/e;

    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    invoke-static {}, Lcom/google/android/play/layout/d;->a()Lcom/google/android/play/layout/d;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/play/layout/b;->c:Lcom/google/android/play/layout/d;

    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget-object v1, v1, Lcom/google/android/play/layout/b;->c:Lcom/google/android/play/layout/d;

    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget-object v2, v2, Lcom/google/android/play/layout/b;->f:Lcom/google/android/play/layout/e;

    invoke-virtual {v1, v2, p3}, Lcom/google/android/play/layout/d;->a(Lcom/google/android/play/layout/e;I)V

    .line 2751
    :cond_3
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget-object v7, v1, Lcom/google/android/play/layout/b;->f:Lcom/google/android/play/layout/e;

    .line 2752
    const/4 v3, 0x1

    .line 2753
    if-eqz p4, :cond_4

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    const/4 v1, 0x1

    move v2, v1

    .line 2756
    :goto_0
    invoke-virtual {v7}, Lcom/google/android/play/layout/e;->g()Z

    move-result v1

    if-eqz v1, :cond_16

    .line 2757
    if-nez p9, :cond_6

    if-eqz p8, :cond_6

    .line 2758
    const/4 v1, 0x0

    .line 2769
    :goto_1
    invoke-virtual {v7}, Lcom/google/android/play/layout/e;->b()I

    move-result v3

    .line 2770
    if-eqz p9, :cond_15

    .line 2771
    sparse-switch v3, :sswitch_data_0

    .line 2789
    if-eqz v2, :cond_15

    const/4 v3, 0x1

    move/from16 v0, p10

    if-gt v0, v3, :cond_15

    .line 2793
    const/4 v1, 0x0

    .line 2939
    :goto_2
    return v1

    .line 2753
    :cond_5
    const/4 v1, 0x0

    move v2, v1

    goto :goto_0

    .line 2760
    :cond_6
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iput p2, v1, Lcom/google/android/play/layout/b;->b:I

    .line 2764
    const/4 v1, 0x0

    goto :goto_2

    .line 2776
    :sswitch_0
    const/4 v1, 0x0

    goto :goto_2

    .line 2783
    :sswitch_1
    if-eqz v2, :cond_15

    .line 2784
    const/4 v1, 0x0

    move v3, v1

    .line 2800
    :goto_3
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget-object v8, v1, Lcom/google/android/play/layout/b;->c:Lcom/google/android/play/layout/d;

    .line 2801
    invoke-virtual {v7}, Lcom/google/android/play/layout/e;->a()I

    move-result v1

    .line 2802
    if-nez v2, :cond_7

    .line 2803
    packed-switch v1, :pswitch_data_0

    .line 2815
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/d;

    invoke-virtual {v8, v1}, Lcom/google/android/play/layout/d;->a(Lcom/google/android/play/layout/d;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2819
    const/4 v1, 0x0

    goto :goto_2

    .line 2805
    :pswitch_0
    const/4 v3, 0x0

    .line 2825
    :cond_7
    :pswitch_1
    iget v1, v7, Lcom/google/android/play/layout/e;->e:I

    const/4 v4, -0x1

    if-ne v1, v4, :cond_8

    .line 2826
    move/from16 v0, p5

    if-ge v0, p3, :cond_8

    .line 2831
    const/4 v1, 0x0

    goto :goto_2

    .line 2811
    :pswitch_2
    const/4 v1, 0x0

    goto :goto_2

    .line 2839
    :cond_8
    if-eqz v3, :cond_9

    if-eqz v2, :cond_9

    move/from16 v0, p5

    if-lt v0, p3, :cond_9

    .line 2840
    const/4 v3, 0x0

    .line 2845
    :cond_9
    const/4 v1, 0x0

    sub-int v4, p5, p6

    iget v5, v8, Lcom/google/android/play/layout/d;->f:I

    sub-int/2addr v4, v5

    iget v5, v8, Lcom/google/android/play/layout/d;->g:I

    sub-int/2addr v4, v5

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 2850
    const/4 v1, 0x0

    .line 2851
    const/4 v4, 0x0

    .line 2852
    iget v9, v7, Lcom/google/android/play/layout/e;->e:I

    invoke-static {v9}, Lcom/google/android/play/utils/a;->a(I)Z

    move-result v9

    if-eqz v9, :cond_14

    iget v9, v8, Lcom/google/android/play/layout/d;->a:I

    if-nez v9, :cond_a

    iget v9, v8, Lcom/google/android/play/layout/d;->b:I

    if-eqz v9, :cond_14

    .line 2854
    :cond_a
    iget v9, v8, Lcom/google/android/play/layout/d;->a:I

    if-lez v9, :cond_d

    .line 2856
    const/4 v1, 0x0

    iget v2, v8, Lcom/google/android/play/layout/d;->a:I

    sub-int v2, v2, p7

    sub-int v2, v2, p6

    iget v9, v8, Lcom/google/android/play/layout/d;->f:I

    sub-int/2addr v2, v9

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2863
    :cond_b
    :goto_4
    sub-int v2, p3, p7

    sub-int v2, v2, p5

    .line 2864
    iget v9, v8, Lcom/google/android/play/layout/d;->b:I

    if-lez v9, :cond_e

    .line 2866
    const/4 v4, 0x0

    iget v9, v8, Lcom/google/android/play/layout/d;->b:I

    sub-int v2, v9, v2

    iget v9, v8, Lcom/google/android/play/layout/d;->g:I

    sub-int/2addr v2, v9

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2875
    :goto_5
    const/4 v4, 0x0

    sub-int/2addr v5, v1

    sub-int/2addr v5, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    move v11, v2

    move v2, v4

    move v4, v11

    .line 2880
    :goto_6
    sget-object v5, Lcom/google/android/play/layout/FlowLayoutManager;->d:Landroid/graphics/Rect;

    invoke-virtual {p0, v6, v5}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2881
    sget-object v5, Lcom/google/android/play/layout/FlowLayoutManager;->d:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    sget-object v9, Lcom/google/android/play/layout/FlowLayoutManager;->d:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    add-int/2addr v5, v9

    .line 2882
    sget-object v9, Lcom/google/android/play/layout/FlowLayoutManager;->d:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    sget-object v10, Lcom/google/android/play/layout/FlowLayoutManager;->d:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v9, v10

    .line 2883
    const/4 v10, 0x0

    sub-int/2addr v2, v5

    invoke-static {v10, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2887
    iget v10, v8, Lcom/google/android/play/layout/d;->c:F

    invoke-virtual {v7, v10, v2, v5}, Lcom/google/android/play/layout/e;->a(FII)I

    move-result v5

    .line 2889
    iget v10, v8, Lcom/google/android/play/layout/d;->c:F

    invoke-virtual {v7, v10, v9, p0}, Lcom/google/android/play/layout/e;->a(FILcom/google/android/play/layout/FlowLayoutManager;)I

    move-result v9

    .line 2894
    if-eqz v3, :cond_f

    if-eqz v2, :cond_c

    invoke-static {v5}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v10

    if-le v10, v2, :cond_f

    .line 2901
    :cond_c
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 2858
    :cond_d
    if-eqz v2, :cond_b

    if-nez p7, :cond_b

    iget v2, v8, Lcom/google/android/play/layout/d;->f:I

    if-nez v2, :cond_b

    .line 2861
    iget v1, v8, Lcom/google/android/play/layout/d;->a:I

    goto :goto_4

    .line 2868
    :cond_e
    if-nez v2, :cond_13

    iget v2, v8, Lcom/google/android/play/layout/d;->g:I

    if-nez v2, :cond_13

    .line 2873
    iget v2, v8, Lcom/google/android/play/layout/d;->b:I

    goto :goto_5

    .line 2905
    :cond_f
    invoke-virtual {v6, v5, v9}, Landroid/view/View;->measure(II)V

    .line 2906
    if-eqz v3, :cond_11

    .line 2908
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    if-le v3, v2, :cond_10

    .line 2914
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 2918
    :cond_10
    invoke-static {v6}, Landroid/support/v4/view/ay;->k(Landroid/view/View;)I

    move-result v2

    const/high16 v3, 0x1000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_11

    .line 2924
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 2933
    :cond_11
    const/4 v2, 0x0

    invoke-virtual {v8, v6, v2}, Lcom/google/android/play/layout/d;->a(Landroid/view/View;Z)V

    .line 2934
    iget v2, v7, Lcom/google/android/play/layout/e;->s:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_12

    .line 2935
    iget v1, v8, Lcom/google/android/play/layout/d;->g:I

    add-int/2addr v1, v4

    iput v1, v8, Lcom/google/android/play/layout/d;->g:I

    .line 2939
    :goto_7
    const/4 v1, 0x1

    goto/16 :goto_2

    .line 2937
    :cond_12
    iget v2, v8, Lcom/google/android/play/layout/d;->f:I

    add-int/2addr v1, v2

    iput v1, v8, Lcom/google/android/play/layout/d;->f:I

    goto :goto_7

    :cond_13
    move v2, v4

    goto/16 :goto_5

    :cond_14
    move v2, v5

    goto/16 :goto_6

    :cond_15
    move v3, v1

    goto/16 :goto_3

    :cond_16
    move v1, v3

    goto/16 :goto_1

    .line 2771
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x8 -> :sswitch_0
    .end sparse-switch

    .line 2803
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private b(Landroid/support/v7/widget/cj;III)I
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 2340
    if-ne p2, v2, :cond_0

    const/4 v1, 0x0

    .line 2341
    :goto_0
    if-eqz v1, :cond_1

    iget v0, v1, Lcom/google/android/play/layout/j;->e:I

    if-le v0, p3, :cond_1

    .line 2342
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Section at "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " impossible to cover position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2340
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/j;

    move-object v1, v0

    goto :goto_0

    .line 2345
    :cond_1
    add-int/lit8 v0, p2, 0x1

    iget-object v4, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    add-int/lit8 v4, p2, 0x1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/j;

    iget v0, v0, Lcom/google/android/play/layout/j;->e:I

    if-lt p3, v0, :cond_2

    .line 2347
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Section at "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " impossible to cover position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2351
    :cond_2
    if-nez v1, :cond_3

    move v4, v3

    .line 2352
    :goto_1
    if-le v4, p3, :cond_4

    .line 2353
    invoke-virtual {v1, p4}, Lcom/google/android/play/layout/j;->c(I)I

    .line 2410
    :goto_2
    return p2

    .line 2351
    :cond_3
    invoke-virtual {v1}, Lcom/google/android/play/layout/j;->b()I

    move-result v0

    move v4, v0

    goto :goto_1

    :cond_4
    move v0, v2

    move v3, v2

    move v2, p3

    .line 2364
    :goto_3
    if-lt v2, v4, :cond_7

    .line 2366
    :try_start_0
    invoke-direct {p0, p1, v2, v3, v0}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/support/v7/widget/cj;III)I

    move-result v3

    .line 2368
    invoke-virtual {p0, v3}, Lcom/google/android/play/layout/FlowLayoutManager;->d(I)Landroid/view/View;

    move-result-object v0

    .line 2370
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/e;

    .line 2371
    invoke-virtual {v0}, Lcom/google/android/play/layout/e;->g()Z

    move-result v5

    .line 2372
    if-nez v5, :cond_5

    if-nez v2, :cond_6

    .line 2374
    :cond_5
    invoke-static {v2}, Lcom/google/android/play/layout/j;->a(I)Lcom/google/android/play/layout/j;

    move-result-object v0

    .line 2375
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    add-int/lit8 p2, p2, 0x1

    invoke-interface {v1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2376
    if-nez v2, :cond_8

    .line 2382
    iput-boolean v5, p0, Lcom/google/android/play/layout/FlowLayoutManager;->c:Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v2, v0

    .line 2406
    :goto_4
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->u()V

    .line 2407
    add-int/lit8 v0, p2, 0x1

    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_9

    move v5, p4

    .line 2409
    :goto_5
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/support/v7/widget/cj;Lcom/google/android/play/layout/j;IIII)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 2411
    :catch_0
    move-exception v0

    .line 2413
    const-string v1, "FlowLayoutManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fillUpForPosition() state at exception: filling section\n\r sectionIndex="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\r position="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\r totalItemCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\r mFillState="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2419
    throw v0

    .line 2365
    :cond_6
    add-int/lit8 v0, v2, -0x1

    move v7, v0

    move v0, v3

    move v3, v2

    move v2, v7

    goto :goto_3

    :cond_7
    move-object v0, v1

    :cond_8
    move-object v2, v0

    .line 2402
    goto :goto_4

    .line 2394
    :catch_1
    move-exception v0

    .line 2396
    const-string v1, "FlowLayoutManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fillUpForPosition() state at exception: finding anchor\n\r sectionIndex="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\r position="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\r totalItemCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2401
    throw v0

    .line 2407
    :cond_9
    :try_start_2
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    add-int/lit8 v1, p2, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/j;

    iget v5, v0, Lcom/google/android/play/layout/j;->e:I
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_5
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1830
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 1831
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/j;

    invoke-virtual {v0}, Lcom/google/android/play/layout/j;->e()V

    .line 1830
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1833
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1834
    return-void
.end method

.method private b(Landroid/view/View;II)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 3407
    sget-object v0, Lcom/google/android/play/layout/FlowLayoutManager;->d:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 3408
    sget-object v0, Lcom/google/android/play/layout/FlowLayoutManager;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int v0, p2, v0

    sget-object v1, Lcom/google/android/play/layout/FlowLayoutManager;->d:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 3410
    sget-object v1, Lcom/google/android/play/layout/FlowLayoutManager;->d:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, p3, v1

    sget-object v2, Lcom/google/android/play/layout/FlowLayoutManager;->d:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 3412
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 3413
    return-void
.end method

.method private c(Landroid/support/v7/widget/cj;III)I
    .locals 10

    .prologue
    const/4 v9, -0x1

    .line 2436
    .line 2437
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/j;

    .line 2439
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->u()V

    move v4, p3

    move-object v2, v0

    move v7, p2

    .line 2441
    :goto_0
    if-lez v4, :cond_4

    .line 2442
    add-int/lit8 v0, v7, 0x1

    :try_start_0
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    move-object v8, v0

    .line 2444
    :goto_1
    if-nez v8, :cond_1

    move v5, p4

    .line 2446
    :goto_2
    const/4 v3, -0x1

    move-object v0, p0

    move-object v1, p1

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/support/v7/widget/cj;Lcom/google/android/play/layout/j;IIII)V

    .line 2448
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget v0, v0, Lcom/google/android/play/layout/b;->a:I

    sub-int/2addr v4, v0

    .line 2449
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iget v0, v0, Lcom/google/android/play/layout/b;->b:I

    .line 2450
    if-eq v0, v9, :cond_2

    .line 2452
    invoke-static {v0}, Lcom/google/android/play/layout/j;->a(I)Lcom/google/android/play/layout/j;

    move-result-object v2

    .line 2453
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v7, 0x1

    :try_start_1
    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    move v7, v1

    goto :goto_0

    .line 2442
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    add-int/lit8 v1, v7, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/j;

    move-object v8, v0

    goto :goto_1

    .line 2444
    :cond_1
    iget v5, v8, Lcom/google/android/play/layout/j;->e:I

    goto :goto_2

    .line 2458
    :cond_2
    invoke-virtual {v2}, Lcom/google/android/play/layout/j;->b()I
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v0

    if-eq v0, p4, :cond_4

    .line 2460
    add-int/lit8 v7, v7, 0x1

    move-object v2, v8

    .line 2466
    goto :goto_0

    .line 2467
    :catch_0
    move-exception v0

    move v1, v7

    .line 2469
    :goto_3
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "fillDownForHeight() states at exception:\n\t startSectionIndex="

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\n\t height="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\n\t totalItemCount="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\n\t remainingHeight="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\t lastSectionIndex="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n\t lastSection="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2477
    if-nez v2, :cond_3

    .line 2478
    const-string v2, "null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2482
    :goto_4
    const-string v2, "\n\t mFillState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2483
    const-string v2, "FlowLayoutManager"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2485
    throw v0

    .line 2480
    :cond_3
    invoke-virtual {v2, v1}, Lcom/google/android/play/layout/j;->a(Ljava/lang/StringBuilder;)V

    goto :goto_4

    .line 2487
    :cond_4
    return v4

    .line 2467
    :catch_1
    move-exception v0

    goto :goto_3
.end method

.method private g()V
    .locals 8

    .prologue
    const/16 v7, 0x3a

    const/4 v6, 0x0

    .line 1951
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 1952
    const-string v0, "FlowLayoutManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Layout in bookkeeping: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " section(s)"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1953
    if-lez v2, :cond_2

    .line 1954
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1955
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/j;

    .line 1956
    const-string v1, "  \u00a70@"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, v0, Lcom/google/android/play/layout/j;->e:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1957
    iget v1, v0, Lcom/google/android/play/layout/j;->e:I

    if-nez v1, :cond_0

    .line 1958
    iget-boolean v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->c:Z

    if-eqz v1, :cond_1

    const-string v1, "(real)"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1960
    :cond_0
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1961
    invoke-virtual {v0, v3}, Lcom/google/android/play/layout/j;->a(Ljava/lang/StringBuilder;)V

    .line 1962
    const-string v0, "FlowLayoutManager"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1963
    const/4 v0, 0x1

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_2

    .line 1964
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1965
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/j;

    .line 1966
    const-string v4, "  \u00a7"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x40

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Lcom/google/android/play/layout/j;->e:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1968
    invoke-virtual {v0, v3}, Lcom/google/android/play/layout/j;->a(Ljava/lang/StringBuilder;)V

    .line 1969
    const-string v0, "FlowLayoutManager"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1963
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1958
    :cond_1
    const-string v1, "(fake)"

    goto :goto_0

    .line 1972
    :cond_2
    return-void
.end method

.method private i(I)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1986
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->j()I

    move-result v0

    .line 1987
    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->d(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/view/View;)I

    move-result v2

    if-le v2, p1, :cond_2

    .line 1988
    :cond_0
    const/4 v0, -0x1

    .line 2006
    :cond_1
    :goto_0
    return v0

    .line 1989
    :cond_2
    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p0, v2}, Lcom/google/android/play/layout/FlowLayoutManager;->d(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/view/View;)I

    move-result v2

    if-ge v2, p1, :cond_3

    .line 1990
    xor-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_3
    move v2, v1

    move v1, v0

    .line 1995
    :goto_1
    if-ge v2, v1, :cond_5

    .line 1996
    add-int v0, v2, v1

    div-int/lit8 v0, v0, 0x2

    .line 1997
    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/FlowLayoutManager;->d(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/view/View;)I

    move-result v3

    .line 1998
    if-eq v3, p1, :cond_1

    .line 2000
    if-ge v3, p1, :cond_4

    .line 2001
    add-int/lit8 v0, v0, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move v1, v0

    .line 2005
    goto :goto_1

    .line 2006
    :cond_5
    xor-int/lit8 v0, v2, -0x1

    goto :goto_0
.end method

.method private t()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2190
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->j()I

    move-result v3

    .line 2191
    const-string v0, "FlowLayoutManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "current child list: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " child(ren)"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2192
    if-lez v3, :cond_0

    .line 2193
    invoke-virtual {p0, v2}, Lcom/google/android/play/layout/FlowLayoutManager;->d(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 2194
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move v1, v2

    .line 2195
    :goto_0
    if-ge v1, v3, :cond_0

    .line 2196
    const-string v5, "  #"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x40

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2197
    invoke-virtual {p0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->d(I)Landroid/view/View;

    move-result-object v5

    .line 2198
    invoke-static {v5}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/view/View;)I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x2c

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2200
    const-string v5, "FlowLayoutManager"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2201
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2195
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2204
    :cond_0
    return-void
.end method

.method private u()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 2286
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    if-nez v0, :cond_0

    .line 2287
    new-instance v0, Lcom/google/android/play/layout/b;

    invoke-direct {v0, v2}, Lcom/google/android/play/layout/b;-><init>(B)V

    iput-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    .line 2289
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->e:Lcom/google/android/play/layout/b;

    iput v2, v0, Lcom/google/android/play/layout/b;->a:I

    iput v1, v0, Lcom/google/android/play/layout/b;->b:I

    iput v1, v0, Lcom/google/android/play/layout/b;->d:I

    iput v1, v0, Lcom/google/android/play/layout/b;->e:I

    iput-object v3, v0, Lcom/google/android/play/layout/b;->f:Lcom/google/android/play/layout/e;

    iget-object v1, v0, Lcom/google/android/play/layout/b;->c:Lcom/google/android/play/layout/d;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/play/layout/b;->c:Lcom/google/android/play/layout/d;

    invoke-virtual {v1}, Lcom/google/android/play/layout/d;->b()V

    iput-object v3, v0, Lcom/google/android/play/layout/b;->c:Lcom/google/android/play/layout/d;

    .line 2290
    :cond_1
    return-void
.end method

.method private v()Landroid/view/View;
    .locals 8

    .prologue
    .line 3563
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->l()I

    move-result v5

    .line 3564
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->j()I

    move-result v6

    .line 3565
    const/4 v3, 0x0

    .line 3566
    const v1, 0x7fffffff

    .line 3567
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v6, :cond_2

    .line 3568
    invoke-virtual {p0, v4}, Lcom/google/android/play/layout/FlowLayoutManager;->d(I)Landroid/view/View;

    move-result-object v2

    .line 3569
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/e;

    invoke-virtual {v0}, Lcom/google/android/play/layout/e;->d()Z

    move-result v0

    if-nez v0, :cond_3

    .line 3570
    invoke-static {v2}, Lcom/google/android/play/layout/FlowLayoutManager;->e(Landroid/view/View;)I

    move-result v0

    .line 3573
    invoke-static {v2}, Lcom/google/android/play/layout/FlowLayoutManager;->g(Landroid/view/View;)I

    move-result v7

    .line 3574
    add-int/2addr v0, v7

    div-int/lit8 v0, v0, 0x2

    .line 3575
    if-ltz v0, :cond_0

    if-gt v0, v5, :cond_0

    .line 3597
    :goto_1
    return-object v2

    .line 3582
    :cond_0
    if-gez v0, :cond_1

    neg-int v0, v0

    .line 3583
    :goto_2
    if-ge v0, v1, :cond_3

    move-object v1, v2

    .line 3567
    :goto_3
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v1

    move v1, v0

    goto :goto_0

    .line 3582
    :cond_1
    sub-int/2addr v0, v5

    goto :goto_2

    :cond_2
    move-object v2, v3

    .line 3597
    goto :goto_1

    :cond_3
    move v0, v1

    move-object v1, v3

    goto :goto_3
.end method


# virtual methods
.method public final synthetic a()Landroid/support/v7/widget/cf;
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/play/layout/e;

    invoke-direct {v0}, Lcom/google/android/play/layout/e;-><init>()V

    return-object v0
.end method

.method public final synthetic a(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/support/v7/widget/cf;
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/play/layout/e;

    invoke-direct {v0, p1, p2}, Lcom/google/android/play/layout/e;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public final synthetic a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/cf;
    .locals 1

    .prologue
    .line 50
    instance-of v0, p1, Lcom/google/android/play/layout/e;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/play/layout/e;

    check-cast p1, Lcom/google/android/play/layout/e;

    invoke-direct {v0, p1}, Lcom/google/android/play/layout/e;-><init>(Lcom/google/android/play/layout/e;)V

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/play/layout/e;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Lcom/google/android/play/layout/e;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/play/layout/e;

    invoke-direct {v0, p1}, Lcom/google/android/play/layout/e;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 3619
    instance-of v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    if-eqz v0, :cond_0

    .line 3620
    check-cast p1, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    iput-object p1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->j:Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    .line 3621
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->h()V

    .line 3623
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 1852
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->b()V

    .line 1853
    invoke-super {p0, p1}, Landroid/support/v7/widget/ce;->a(Landroid/support/v7/widget/RecyclerView;)V

    .line 1854
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    .prologue
    .line 1861
    invoke-direct {p0, p2, p2, p3}, Lcom/google/android/play/layout/FlowLayoutManager;->a(III)V

    .line 1862
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/ce;->a(Landroid/support/v7/widget/RecyclerView;II)V

    .line 1863
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;III)V
    .locals 3

    .prologue
    .line 1879
    add-int v0, p2, p4

    .line 1880
    add-int v1, p3, p4

    .line 1881
    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->a(III)V

    .line 1882
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/ce;->a(Landroid/support/v7/widget/RecyclerView;III)V

    .line 1883
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/cj;)V
    .locals 0

    .prologue
    .line 1838
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->b()V

    .line 1839
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/ce;->a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/cj;)V

    .line 1840
    return-void
.end method

.method public final a(Landroid/support/v7/widget/bv;Landroid/support/v7/widget/bv;)V
    .locals 1

    .prologue
    .line 1845
    iget-boolean v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->f:Lcom/google/android/play/layout/i;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->g:Z

    :cond_0
    instance-of v0, p2, Lcom/google/android/play/layout/a;

    if-eqz v0, :cond_1

    move-object v0, p2

    check-cast v0, Lcom/google/android/play/layout/a;

    iput-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->f:Lcom/google/android/play/layout/i;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->g:Z

    .line 1846
    :cond_1
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->b()V

    .line 1847
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/ce;->a(Landroid/support/v7/widget/bv;Landroid/support/v7/widget/bv;)V

    .line 1848
    return-void
.end method

.method public final a(Landroid/view/View;II)V
    .locals 2

    .prologue
    .line 818
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/play/layout/e;

    if-eqz v0, :cond_0

    .line 819
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Views using FlowLayoutManager.LayoutParams should not be measured with measureChildWithMargins()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 822
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/ce;->a(Landroid/view/View;II)V

    .line 823
    return-void
.end method

.method public final a(Landroid/support/v7/widget/cf;)Z
    .locals 1

    .prologue
    .line 782
    instance-of v0, p1, Lcom/google/android/play/layout/e;

    return v0
.end method

.method public final b(ILandroid/support/v7/widget/cj;Landroid/support/v7/widget/cp;)I
    .locals 2

    .prologue
    .line 3486
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->v()Landroid/view/View;

    move-result-object v0

    .line 3487
    if-nez v0, :cond_0

    .line 3488
    const/4 v0, 0x0

    .line 3495
    :goto_0
    return v0

    .line 3490
    :cond_0
    invoke-static {v0}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/view/View;)I

    move-result v1

    .line 3492
    invoke-static {v0}, Lcom/google/android/play/layout/FlowLayoutManager;->e(Landroid/view/View;)I

    move-result v0

    sub-int/2addr v0, p1

    .line 3493
    invoke-direct {p0, p2, p3, v1, v0}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/support/v7/widget/cj;Landroid/support/v7/widget/cp;II)I

    move-result v0

    .line 3495
    sub-int v0, p1, v0

    goto :goto_0
.end method

.method public final b(Landroid/support/v7/widget/cp;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 3445
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->j()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/FlowLayoutManager;->d(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method public final b(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2011
    invoke-direct {p0, p1}, Lcom/google/android/play/layout/FlowLayoutManager;->i(I)I

    move-result v0

    .line 2012
    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/FlowLayoutManager;->d(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 2

    .prologue
    .line 1870
    add-int v0, p2, p3

    neg-int v1, p3

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->a(III)V

    .line 1871
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/ce;->b(Landroid/support/v7/widget/RecyclerView;II)V

    .line 1872
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 3473
    iput p1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->h:I

    .line 3474
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->i:I

    .line 3475
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->h()V

    .line 3476
    return-void
.end method

.method public final c(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 2

    .prologue
    .line 1887
    iget-boolean v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->a:Z

    if-eqz v0, :cond_0

    .line 1891
    add-int v0, p2, p3

    const/4 v1, 0x0

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->a(III)V

    .line 1893
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/ce;->c(Landroid/support/v7/widget/RecyclerView;II)V

    .line 1894
    return-void
.end method

.method public final c(Landroid/support/v7/widget/cj;Landroid/support/v7/widget/cp;)V
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    const/4 v2, -0x1

    .line 3011
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->j:Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    if-eqz v0, :cond_0

    .line 3012
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->j:Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    iget v0, v0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->a:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->h:I

    .line 3013
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->l()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->j:Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    iget v1, v1, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->b:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->i:I

    .line 3015
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->j:Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    .line 3017
    :cond_0
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->h:I

    if-eq v0, v2, :cond_2

    .line 3018
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->h:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->h:I

    invoke-virtual {p2}, Landroid/support/v7/widget/cp;->a()I

    move-result v1

    if-lt v0, v1, :cond_3

    .line 3019
    :cond_1
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->h:I

    .line 3020
    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->i:I

    .line 3030
    :cond_2
    :goto_0
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->h:I

    if-eq v0, v2, :cond_4

    .line 3031
    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->h:I

    .line 3032
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->i:I

    .line 3034
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->h:I

    .line 3035
    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->i:I

    .line 3048
    :goto_1
    invoke-direct {p0, p1, p2, v1, v0}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/support/v7/widget/cj;Landroid/support/v7/widget/cp;II)I

    .line 3049
    return-void

    .line 3021
    :cond_3
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->i:I

    if-ne v0, v3, :cond_2

    .line 3023
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->n()I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->i:I

    goto :goto_0

    .line 3037
    :cond_4
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->v()Landroid/view/View;

    move-result-object v0

    .line 3038
    if-eqz v0, :cond_5

    .line 3039
    invoke-static {v0}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/view/View;)I

    move-result v1

    .line 3040
    invoke-static {v0}, Lcom/google/android/play/layout/FlowLayoutManager;->e(Landroid/view/View;)I

    move-result v0

    goto :goto_1

    .line 3043
    :cond_5
    const/4 v0, 0x0

    move v1, v2

    goto :goto_1
.end method

.method public final d(Landroid/support/v7/widget/cp;)I
    .locals 1

    .prologue
    .line 3450
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->j()I

    move-result v0

    return v0
.end method

.method public final d()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 3602
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->j:Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    if-eqz v0, :cond_0

    .line 3603
    new-instance v0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->j:Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    invoke-direct {v0, v1}, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;-><init>(Lcom/google/android/play/layout/FlowLayoutManager$SavedState;)V

    .line 3614
    :goto_0
    return-object v0

    .line 3605
    :cond_0
    new-instance v0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    invoke-direct {v0}, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;-><init>()V

    .line 3606
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->v()Landroid/view/View;

    move-result-object v1

    .line 3607
    if-nez v1, :cond_1

    .line 3608
    const/4 v1, -0x1

    iput v1, v0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->a:I

    .line 3609
    const/4 v1, 0x0

    iput v1, v0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->b:F

    goto :goto_0

    .line 3611
    :cond_1
    invoke-static {v1}, Lcom/google/android/play/layout/FlowLayoutManager;->a(Landroid/view/View;)I

    move-result v2

    iput v2, v0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->a:I

    .line 3612
    invoke-static {v1}, Lcom/google/android/play/layout/FlowLayoutManager;->e(Landroid/view/View;)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->l()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, v0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->b:F

    goto :goto_0
.end method

.method public final f(Landroid/support/v7/widget/cp;)I
    .locals 1

    .prologue
    .line 3440
    invoke-virtual {p1}, Landroid/support/v7/widget/cp;->a()I

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 3435
    const/4 v0, 0x1

    return v0
.end method
