.class public Lcom/google/android/play/layout/PlayTabStrip;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:Landroid/graphics/Paint;

.field private c:I

.field private d:F

.field private final e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayTabStrip;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayTabStrip;->setWillNotDraw(Z)V

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 43
    sget v1, Lcom/google/android/play/e;->E:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayTabStrip;->a:I

    .line 45
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/play/layout/PlayTabStrip;->b:Landroid/graphics/Paint;

    .line 47
    sget v1, Lcom/google/android/play/d;->t:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/PlayTabStrip;->e:I

    .line 48
    return-void
.end method

.method private a()V
    .locals 7

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTabStrip;->getChildCount()I

    move-result v3

    .line 83
    iget v0, p0, Lcom/google/android/play/layout/PlayTabStrip;->c:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/play/layout/PlayTabStrip;->d:F

    add-float v4, v0, v1

    .line 84
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 85
    invoke-virtual {p0, v2}, Lcom/google/android/play/layout/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 86
    int-to-float v1, v2

    sub-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 87
    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v5, v1, v5

    if-ltz v5, :cond_0

    const/16 v1, 0x99

    .line 90
    :goto_1
    shl-int/lit8 v1, v1, 0x18

    iget v5, p0, Lcom/google/android/play/layout/PlayTabStrip;->e:I

    const v6, 0xffffff

    and-int/2addr v5, v6

    or-int/2addr v1, v5

    .line 91
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 84
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 87
    :cond_0
    const/high16 v5, 0x437f0000    # 255.0f

    const/high16 v6, 0x42cc0000    # 102.0f

    mul-float/2addr v1, v6

    sub-float v1, v5, v1

    float-to-int v1, v1

    goto :goto_1

    .line 93
    :cond_1
    return-void
.end method


# virtual methods
.method final a(I)V
    .locals 1

    .prologue
    .line 75
    iput p1, p0, Lcom/google/android/play/layout/PlayTabStrip;->c:I

    .line 76
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/layout/PlayTabStrip;->d:F

    .line 77
    invoke-direct {p0}, Lcom/google/android/play/layout/PlayTabStrip;->a()V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTabStrip;->invalidate()V

    .line 79
    return-void
.end method

.method final a(IF)V
    .locals 0

    .prologue
    .line 64
    iput p1, p0, Lcom/google/android/play/layout/PlayTabStrip;->c:I

    .line 65
    iput p2, p0, Lcom/google/android/play/layout/PlayTabStrip;->d:F

    .line 66
    invoke-direct {p0}, Lcom/google/android/play/layout/PlayTabStrip;->a()V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTabStrip;->invalidate()V

    .line 68
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 97
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTabStrip;->getHeight()I

    move-result v4

    .line 98
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTabStrip;->getChildCount()I

    move-result v0

    .line 101
    if-lez v0, :cond_1

    .line 102
    iget v0, p0, Lcom/google/android/play/layout/PlayTabStrip;->c:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 103
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 104
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    .line 105
    iget v2, p0, Lcom/google/android/play/layout/PlayTabStrip;->d:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    iget v2, p0, Lcom/google/android/play/layout/PlayTabStrip;->c:I

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTabStrip;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_0

    .line 108
    iget v2, p0, Lcom/google/android/play/layout/PlayTabStrip;->c:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/play/layout/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 109
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 110
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    .line 112
    iget v5, p0, Lcom/google/android/play/layout/PlayTabStrip;->d:F

    int-to-float v3, v3

    mul-float/2addr v3, v5

    iget v5, p0, Lcom/google/android/play/layout/PlayTabStrip;->d:F

    sub-float v5, v6, v5

    int-to-float v1, v1

    mul-float/2addr v1, v5

    add-float/2addr v1, v3

    float-to-int v1, v1

    .line 114
    iget v3, p0, Lcom/google/android/play/layout/PlayTabStrip;->d:F

    int-to-float v2, v2

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/play/layout/PlayTabStrip;->d:F

    sub-float v3, v6, v3

    int-to-float v0, v0

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    float-to-int v0, v0

    .line 118
    :cond_0
    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/play/layout/PlayTabStrip;->a:I

    sub-int v2, v4, v2

    int-to-float v2, v2

    int-to-float v3, v0

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/play/layout/PlayTabStrip;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 121
    :cond_1
    return-void
.end method
