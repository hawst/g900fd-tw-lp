.class public Lcom/google/android/play/layout/PlayCardLabelView;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:Landroid/text/TextPaint;

.field private final j:Landroid/text/TextPaint;

.field private final k:I

.field private final l:I

.field private m:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayCardLabelView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 69
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 72
    sget v1, Lcom/google/android/play/e;->k:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardLabelView;->e:I

    .line 73
    sget v1, Lcom/google/android/play/e;->l:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardLabelView;->f:I

    .line 74
    sget v1, Lcom/google/android/play/e;->w:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardLabelView;->g:I

    .line 76
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1, v4}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/play/layout/PlayCardLabelView;->j:Landroid/text/TextPaint;

    .line 77
    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardLabelView;->j:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    iput v2, v1, Landroid/text/TextPaint;->density:F

    .line 78
    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardLabelView;->j:Landroid/text/TextPaint;

    iget v2, p0, Lcom/google/android/play/layout/PlayCardLabelView;->g:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 79
    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardLabelView;->j:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 81
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1, v4}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/play/layout/PlayCardLabelView;->i:Landroid/text/TextPaint;

    .line 82
    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardLabelView;->i:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    iput v2, v1, Landroid/text/TextPaint;->density:F

    .line 83
    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardLabelView;->i:Landroid/text/TextPaint;

    iget v2, p0, Lcom/google/android/play/layout/PlayCardLabelView;->g:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 84
    sget v1, Lcom/google/android/play/d;->i:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/PlayCardLabelView;->h:I

    .line 85
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardLabelView;->i:Landroid/text/TextPaint;

    iget v1, p0, Lcom/google/android/play/layout/PlayCardLabelView;->h:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 86
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardLabelView;->i:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->setStrikeThruText(Z)V

    .line 87
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardLabelView;->i:Landroid/text/TextPaint;

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 89
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardLabelView;->j:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    .line 90
    iget v1, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, v0, Landroid/graphics/Paint$FontMetrics;->bottom:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardLabelView;->k:I

    .line 91
    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/play/layout/PlayCardLabelView;->l:I

    .line 93
    invoke-virtual {p0, v3}, Lcom/google/android/play/layout/PlayCardLabelView;->setWillNotDraw(Z)V

    .line 94
    return-void
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 3

    .prologue
    .line 149
    invoke-super {p0, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 151
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 152
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardLabelView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    const/4 v0, 0x1

    .line 156
    :cond_0
    return v0
.end method

.method public getBaseline()I
    .locals 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardLabelView;->getPaddingTop()I

    move-result v0

    iget v1, p0, Lcom/google/android/play/layout/PlayCardLabelView;->l:I

    add-int/2addr v0, v1

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 217
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 218
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardLabelView;->getPaddingLeft()I

    move-result v1

    .line 219
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardLabelView;->getPaddingTop()I

    move-result v2

    .line 221
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardLabelView;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 223
    :goto_0
    iget-object v3, p0, Lcom/google/android/play/layout/PlayCardLabelView;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_0

    .line 225
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardLabelView;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardLabelView;->getPaddingTop()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardLabelView;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardLabelView;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v2

    .line 227
    int-to-float v4, v1

    int-to-float v5, v3

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 228
    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardLabelView;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 229
    neg-int v4, v1

    int-to-float v4, v4

    neg-int v3, v3

    int-to-float v3, v3

    invoke-virtual {p1, v4, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 231
    iget-object v3, p0, Lcom/google/android/play/layout/PlayCardLabelView;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    iget v4, p0, Lcom/google/android/play/layout/PlayCardLabelView;->e:I

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 234
    :cond_0
    iget-boolean v3, p0, Lcom/google/android/play/layout/PlayCardLabelView;->m:Z

    if-eqz v3, :cond_1

    .line 236
    iget-object v3, p0, Lcom/google/android/play/layout/PlayCardLabelView;->a:Ljava/lang/String;

    int-to-float v4, v1

    iget v5, p0, Lcom/google/android/play/layout/PlayCardLabelView;->l:I

    add-int/2addr v5, v2

    int-to-float v5, v5

    iget-object v6, p0, Lcom/google/android/play/layout/PlayCardLabelView;->i:Landroid/text/TextPaint;

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 237
    iget v3, p0, Lcom/google/android/play/layout/PlayCardLabelView;->d:I

    iget v4, p0, Lcom/google/android/play/layout/PlayCardLabelView;->f:I

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 240
    :cond_1
    if-eqz v0, :cond_2

    .line 242
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardLabelView;->b:Ljava/lang/String;

    int-to-float v1, v1

    iget v3, p0, Lcom/google/android/play/layout/PlayCardLabelView;->l:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/play/layout/PlayCardLabelView;->j:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 244
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 245
    return-void

    .line 221
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 161
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v3, 0x40000000    # 2.0f

    if-ne v0, v3, :cond_4

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 164
    :goto_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 166
    iput v2, p0, Lcom/google/android/play/layout/PlayCardLabelView;->d:I

    .line 167
    iput-boolean v2, p0, Lcom/google/android/play/layout/PlayCardLabelView;->m:Z

    .line 169
    iget-object v3, p0, Lcom/google/android/play/layout/PlayCardLabelView;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    move v3, v1

    .line 173
    :goto_1
    if-nez v0, :cond_3

    .line 174
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardLabelView;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_9

    .line 176
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardLabelView;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 177
    if-eqz v3, :cond_0

    .line 178
    iget v5, p0, Lcom/google/android/play/layout/PlayCardLabelView;->e:I

    add-int/2addr v0, v5

    .line 182
    :cond_0
    :goto_2
    if-eqz v3, :cond_1

    .line 184
    iget-object v5, p0, Lcom/google/android/play/layout/PlayCardLabelView;->j:Landroid/text/TextPaint;

    iget-object v6, p0, Lcom/google/android/play/layout/PlayCardLabelView;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v5

    float-to-int v5, v5

    .line 185
    add-int/2addr v0, v5

    .line 188
    :cond_1
    iget-object v5, p0, Lcom/google/android/play/layout/PlayCardLabelView;->a:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 191
    iget-object v5, p0, Lcom/google/android/play/layout/PlayCardLabelView;->i:Landroid/text/TextPaint;

    iget-object v6, p0, Lcom/google/android/play/layout/PlayCardLabelView;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v5

    float-to-int v5, v5

    iput v5, p0, Lcom/google/android/play/layout/PlayCardLabelView;->d:I

    .line 193
    iget v5, p0, Lcom/google/android/play/layout/PlayCardLabelView;->d:I

    if-eqz v3, :cond_6

    iget v3, p0, Lcom/google/android/play/layout/PlayCardLabelView;->f:I

    :goto_3
    add-int/2addr v3, v5

    .line 194
    if-lez v4, :cond_7

    add-int v5, v0, v3

    if-gt v5, v4, :cond_7

    .line 195
    add-int/2addr v0, v3

    .line 196
    iput-boolean v1, p0, Lcom/google/android/play/layout/PlayCardLabelView;->m:Z

    .line 203
    :cond_2
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardLabelView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardLabelView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    add-int v2, v0, v1

    .line 208
    :cond_3
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardLabelView;->c:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_8

    iget v0, p0, Lcom/google/android/play/layout/PlayCardLabelView;->k:I

    .line 210
    :goto_5
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardLabelView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardLabelView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 212
    invoke-virtual {p0, v2, v0}, Lcom/google/android/play/layout/PlayCardLabelView;->setMeasuredDimension(II)V

    .line 213
    return-void

    :cond_4
    move v0, v2

    .line 161
    goto :goto_0

    :cond_5
    move v3, v2

    .line 169
    goto :goto_1

    :cond_6
    move v3, v2

    .line 193
    goto :goto_3

    .line 198
    :cond_7
    iput-boolean v2, p0, Lcom/google/android/play/layout/PlayCardLabelView;->m:Z

    goto :goto_4

    .line 208
    :cond_8
    iget v0, p0, Lcom/google/android/play/layout/PlayCardLabelView;->k:I

    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardLabelView;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_5

    :cond_9
    move v0, v2

    goto :goto_2
.end method
