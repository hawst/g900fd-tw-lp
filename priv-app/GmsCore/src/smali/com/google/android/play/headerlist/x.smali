.class public final Lcom/google/android/play/headerlist/x;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field protected b:I

.field private c:I

.field private final d:Landroid/database/DataSetObserver;

.field private e:Landroid/widget/Adapter;


# direct methods
.method public constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/headerlist/x;->c:I

    .line 30
    iput-object p1, p0, Lcom/google/android/play/headerlist/x;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 31
    new-instance v0, Lcom/google/android/play/headerlist/y;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/y;-><init>(Lcom/google/android/play/headerlist/x;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/x;->d:Landroid/database/DataSetObserver;

    .line 48
    return-void
.end method

.method static synthetic a(Lcom/google/android/play/headerlist/x;)V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/x;->a(Z)V

    return-void
.end method


# virtual methods
.method final a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/headerlist/x;->c:I

    .line 64
    if-eqz p1, :cond_2

    .line 65
    iget-object v0, p0, Lcom/google/android/play/headerlist/x;->e:Landroid/widget/Adapter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/play/headerlist/x;->e:Landroid/widget/Adapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/headerlist/x;->e:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/google/android/play/headerlist/x;->d:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/headerlist/x;->e:Landroid/widget/Adapter;

    iget-object v0, p0, Lcom/google/android/play/headerlist/x;->e:Landroid/widget/Adapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/play/headerlist/x;->e:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/google/android/play/headerlist/x;->d:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_1
    invoke-virtual {p0, v2}, Lcom/google/android/play/headerlist/x;->a(Z)V

    .line 67
    :cond_2
    iput v2, p0, Lcom/google/android/play/headerlist/x;->b:I

    .line 68
    return-void
.end method
