.class public Lcom/google/android/play/layout/l;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# static fields
.field private static e:Z

.field private static f:Z


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field private g:Landroid/graphics/drawable/Drawable;

.field private final h:Landroid/graphics/Rect;

.field private final i:Landroid/graphics/Rect;

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 23
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/layout/l;->e:Z

    .line 25
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, Lcom/google/android/play/layout/l;->f:Z

    return-void

    :cond_0
    move v0, v2

    .line 23
    goto :goto_0

    :cond_1
    move v1, v2

    .line 25
    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 39
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/play/layout/l;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/layout/l;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    iput v4, p0, Lcom/google/android/play/layout/l;->a:I

    .line 30
    iput v4, p0, Lcom/google/android/play/layout/l;->b:I

    .line 31
    iput v4, p0, Lcom/google/android/play/layout/l;->c:I

    .line 32
    iput v4, p0, Lcom/google/android/play/layout/l;->d:I

    .line 34
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/layout/l;->h:Landroid/graphics/Rect;

    .line 35
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/layout/l;->i:Landroid/graphics/Rect;

    .line 36
    iput-boolean v4, p0, Lcom/google/android/play/layout/l;->j:Z

    .line 49
    new-array v0, v5, [I

    const v1, 0x1010109

    aput v1, v0, v4

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 51
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 52
    if-eqz v1, :cond_3

    .line 53
    iget-object v2, p0, Lcom/google/android/play/layout/l;->g:Landroid/graphics/drawable/Drawable;

    if-eq v2, v1, :cond_3

    iget-object v2, p0, Lcom/google/android/play/layout/l;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/play/layout/l;->g:Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v2, p0, Lcom/google/android/play/layout/l;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v2}, Lcom/google/android/play/layout/l;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iput-object v1, p0, Lcom/google/android/play/layout/l;->g:Landroid/graphics/drawable/Drawable;

    iput v4, p0, Lcom/google/android/play/layout/l;->a:I

    iput v4, p0, Lcom/google/android/play/layout/l;->b:I

    iput v4, p0, Lcom/google/android/play/layout/l;->c:I

    iput v4, p0, Lcom/google/android/play/layout/l;->d:I

    if-eqz v1, :cond_4

    invoke-virtual {p0, v4}, Lcom/google/android/play/layout/l;->setWillNotDraw(Z)V

    invoke-virtual {v1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/play/layout/l;->getDrawableState()[I

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_1
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, v2, Landroid/graphics/Rect;->left:I

    iput v1, p0, Lcom/google/android/play/layout/l;->a:I

    iget v1, v2, Landroid/graphics/Rect;->top:I

    iput v1, p0, Lcom/google/android/play/layout/l;->b:I

    iget v1, v2, Landroid/graphics/Rect;->right:I

    iput v1, p0, Lcom/google/android/play/layout/l;->c:I

    iget v1, v2, Landroid/graphics/Rect;->bottom:I

    iput v1, p0, Lcom/google/android/play/layout/l;->d:I

    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/play/layout/l;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/play/layout/l;->invalidate()V

    .line 55
    :cond_3
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 56
    return-void

    .line 53
    :cond_4
    invoke-virtual {p0, v5}, Lcom/google/android/play/layout/l;->setWillNotDraw(Z)V

    goto :goto_0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    .line 161
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->draw(Landroid/graphics/Canvas;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/play/layout/l;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 164
    iget-object v0, p0, Lcom/google/android/play/layout/l;->g:Landroid/graphics/drawable/Drawable;

    .line 166
    iget-boolean v1, p0, Lcom/google/android/play/layout/l;->j:Z

    if-eqz v1, :cond_0

    .line 167
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/play/layout/l;->j:Z

    .line 168
    iget-object v1, p0, Lcom/google/android/play/layout/l;->h:Landroid/graphics/Rect;

    .line 169
    iget-object v2, p0, Lcom/google/android/play/layout/l;->i:Landroid/graphics/Rect;

    .line 171
    invoke-virtual {p0}, Lcom/google/android/play/layout/l;->getWidth()I

    move-result v3

    .line 172
    invoke-virtual {p0}, Lcom/google/android/play/layout/l;->getHeight()I

    move-result v4

    .line 174
    iget v5, p0, Lcom/google/android/play/layout/l;->a:I

    iget v6, p0, Lcom/google/android/play/layout/l;->b:I

    iget v7, p0, Lcom/google/android/play/layout/l;->c:I

    sub-int/2addr v3, v7

    iget v7, p0, Lcom/google/android/play/layout/l;->d:I

    sub-int/2addr v4, v7

    invoke-virtual {v1, v5, v6, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 178
    sget-boolean v3, Lcom/google/android/play/layout/l;->f:Z

    if-eqz v3, :cond_2

    .line 179
    invoke-virtual {p0}, Lcom/google/android/play/layout/l;->getLayoutDirection()I

    move-result v3

    .line 180
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-static {v4, v5, v1, v2, v3}, Landroid/support/v4/view/v;->a(IILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 186
    :goto_0
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 189
    :cond_0
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 191
    :cond_1
    return-void

    .line 184
    :cond_2
    iget-object v3, p0, Lcom/google/android/play/layout/l;->i:Landroid/graphics/Rect;

    invoke-virtual {v3, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public drawableHotspotChanged(FF)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 129
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->drawableHotspotChanged(FF)V

    .line 131
    iget-object v0, p0, Lcom/google/android/play/layout/l;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/google/android/play/layout/l;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setHotspot(FF)V

    .line 134
    :cond_0
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 120
    invoke-super {p0}, Landroid/widget/RelativeLayout;->drawableStateChanged()V

    .line 121
    iget-object v0, p0, Lcom/google/android/play/layout/l;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/layout/l;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/android/play/layout/l;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/play/layout/l;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 124
    :cond_0
    return-void
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 112
    invoke-super {p0}, Landroid/widget/RelativeLayout;->jumpDrawablesToCurrentState()V

    .line 113
    sget-boolean v0, Lcom/google/android/play/layout/l;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/layout/l;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/google/android/play/layout/l;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 116
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 148
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 150
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/layout/l;->j:Z

    .line 151
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 155
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->onSizeChanged(IIII)V

    .line 157
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/layout/l;->j:Z

    .line 158
    return-void
.end method

.method public setVisibility(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 138
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Lcom/google/android/play/layout/l;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 141
    if-nez p1, :cond_1

    const/4 v0, 0x1

    .line 142
    :goto_0
    iget-object v2, p0, Lcom/google/android/play/layout/l;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 144
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 141
    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 106
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/layout/l;->g:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
