.class public Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/HorizontalScrollView;

.field private b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

.field private c:Landroid/support/v4/view/ViewPager;

.field private final d:Lcom/google/android/play/headerlist/w;

.field private e:Ljava/lang/ref/WeakReference;

.field private f:Landroid/support/v4/view/ch;

.field private g:Z

.field private h:F

.field private i:I

.field private j:Z

.field private k:Lcom/google/android/play/headerlist/n;

.field private l:I

.field private m:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 61
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    new-instance v0, Lcom/google/android/play/headerlist/w;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/play/headerlist/w;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;B)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->d:Lcom/google/android/play/headerlist/w;

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->g:Z

    .line 70
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x40a00000    # 5.0f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->h:F

    .line 72
    return-void
.end method

.method static synthetic a(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->c:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method private a(IIZ)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 292
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a:Landroid/widget/HorizontalScrollView;

    if-nez v0, :cond_1

    .line 324
    :cond_0
    :goto_0
    return-void

    .line 295
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildCount()I

    move-result v0

    .line 296
    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    if-ge p1, v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 303
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    if-eqz v2, :cond_0

    .line 307
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 308
    add-int/2addr v2, p2

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v2, v0

    .line 310
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->i:I

    if-eq v2, v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v0

    .line 314
    sub-int v0, v2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 315
    int-to-float v0, v0

    iget v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->h:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    const/4 v0, 0x1

    .line 318
    :goto_1
    if-eqz v0, :cond_3

    if-eqz p3, :cond_3

    .line 319
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v2, v1}, Landroid/widget/HorizontalScrollView;->smoothScrollTo(II)V

    .line 323
    :goto_2
    iput v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->i:I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 315
    goto :goto_1

    .line 321
    :cond_3
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v2, v1}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    goto :goto_2
.end method

.method private a(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 231
    check-cast p1, Landroid/widget/TextView;

    .line 232
    if-eqz p2, :cond_0

    .line 233
    sget v0, Lcom/google/android/play/f;->b:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 234
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 240
    :goto_0
    return-void

    .line 236
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 237
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/d;->j:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;II)V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a(IIZ)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;Z)Z
    .locals 0

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->g:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Lcom/google/android/play/headerlist/n;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->k:Lcom/google/android/play/headerlist/n;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Landroid/support/v4/view/ch;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->f:Landroid/support/v4/view/ch;

    return-object v0
.end method

.method private d()V
    .locals 6

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->removeAllViews()V

    .line 163
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->c:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    move-object v1, v0

    .line 164
    :goto_0
    if-nez v1, :cond_1

    .line 193
    :goto_1
    return-void

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->a()Landroid/support/v4/view/at;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 168
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 169
    invoke-virtual {v1}, Landroid/support/v4/view/at;->c()I

    move-result v3

    .line 170
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v3, :cond_2

    .line 171
    invoke-static {v1, v0}, Lcom/google/android/libraries/bind/b/c;->a(Landroid/support/v4/view/at;I)I

    move-result v4

    .line 172
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {p0, v2, v5}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 173
    invoke-virtual {p0, v5, v1, v4}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a(Landroid/view/View;Landroid/support/v4/view/at;I)V

    .line 174
    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v4, v5}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->addView(Landroid/view/View;)V

    .line 170
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 179
    :cond_2
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/google/android/play/headerlist/u;

    invoke-direct {v1, p0}, Lcom/google/android/play/headerlist/u;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 191
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->c()V

    .line 192
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->a()V

    goto :goto_1
.end method

.method private e()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 368
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->c:Landroid/support/v4/view/ViewPager;

    if-nez v1, :cond_1

    .line 373
    :cond_0
    :goto_0
    return v0

    .line 371
    :cond_1
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v1

    .line 372
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v2, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 373
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v1}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->g:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->d()V

    return-void
.end method


# virtual methods
.method protected final a(I)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 262
    new-instance v0, Lcom/google/android/play/headerlist/v;

    invoke-direct {v0, p0, p1}, Lcom/google/android/play/headerlist/v;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;I)V

    return-object v0
.end method

.method protected a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 206
    sget v0, Lcom/google/android/play/h;->c:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 208
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->l:I

    if-nez v1, :cond_0

    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v1, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->l:I

    :cond_0
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->l:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 209
    iget-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->m:Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a(Landroid/view/View;Z)V

    .line 210
    return-object v0
.end method

.method final a()V
    .locals 1

    .prologue
    .line 118
    sget v0, Lcom/google/android/play/g;->N:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a:Landroid/widget/HorizontalScrollView;

    .line 119
    sget v0, Lcom/google/android/play/g;->M:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    .line 121
    return-void
.end method

.method protected final a(IZ)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 337
    invoke-direct {p0, p1, v1, p2}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a(IIZ)V

    move v0, v1

    .line 338
    :goto_0
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v2}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 339
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v2, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-ne v0, p1, :cond_0

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v3, v2}, Landroid/view/View;->setSelected(Z)V

    .line 338
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    .line 339
    goto :goto_1

    .line 341
    :cond_1
    return-void
.end method

.method public final a(Landroid/support/v4/view/ViewPager;)V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->c:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->c:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/ch;)V

    .line 127
    :cond_0
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->c:Landroid/support/v4/view/ViewPager;

    .line 128
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->c:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_1

    .line 129
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->c:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->d:Lcom/google/android/play/headerlist/w;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/ch;)V

    .line 131
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b()V

    .line 132
    return-void
.end method

.method final a(Landroid/support/v4/view/ch;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->f:Landroid/support/v4/view/ch;

    .line 76
    return-void
.end method

.method protected a(Landroid/view/View;Landroid/support/v4/view/at;I)V
    .locals 1

    .prologue
    .line 221
    check-cast p1, Landroid/widget/TextView;

    .line 222
    invoke-virtual {p2, p3}, Landroid/support/v4/view/at;->b(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    invoke-static {p2, p3}, Lcom/google/android/libraries/bind/b/c;->b(Landroid/support/v4/view/at;I)I

    move-result v0

    .line 224
    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a(I)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    return-void
.end method

.method final a(Z)V
    .locals 4

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->m:Z

    if-eq v0, p1, :cond_0

    .line 85
    iput-boolean p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->m:Z

    .line 87
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildCount()I

    move-result v1

    .line 88
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 89
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v2, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->m:Z

    invoke-direct {p0, v2, v3}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a(Landroid/view/View;Z)V

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92
    :cond_0
    return-void
.end method

.method final a(ZZ)V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->a(Z)V

    .line 102
    iput-boolean p2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->j:Z

    .line 103
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->c()V

    .line 104
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 147
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->c:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_2

    move-object v1, v2

    .line 148
    :goto_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->e:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/at;

    :goto_1
    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->d:Lcom/google/android/play/headerlist/w;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/at;->b(Landroid/database/DataSetObserver;)V

    iput-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->e:Ljava/lang/ref/WeakReference;

    :cond_0
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->d:Lcom/google/android/play/headerlist/w;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/at;->a(Landroid/database/DataSetObserver;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->e:Ljava/lang/ref/WeakReference;

    :cond_1
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->d()V

    .line 149
    return-void

    .line 147
    :cond_2
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->a()Landroid/support/v4/view/at;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_3
    move-object v0, v2

    .line 148
    goto :goto_1
.end method

.method protected final c()V
    .locals 2

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->c:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    .line 333
    :goto_0
    return-void

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v0

    .line 332
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a(IZ)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a()V

    .line 113
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/play/d;->k:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->a(I)V

    .line 115
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .prologue
    .line 346
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->j:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->e()I

    move-result v0

    .line 347
    :goto_0
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 348
    iget-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->j:Z

    if-eqz v1, :cond_0

    .line 349
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->c()V

    .line 350
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xc

    if-lt v1, v2, :cond_0

    .line 351
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->e()I

    move-result v1

    .line 352
    if-eq v1, v0, :cond_0

    .line 353
    sub-int v0, v1, v0

    .line 354
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->setTranslationX(F)V

    .line 355
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 361
    :cond_0
    return-void

    .line 346
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 153
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 154
    if-lez v0, :cond_0

    .line 155
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    invoke-virtual {v1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->c(I)V

    .line 157
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 158
    return-void
.end method
