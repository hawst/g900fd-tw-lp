.class public Lcom/google/android/play/search/PlaySearchOneSuggestion;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/search/PlaySearchOneSuggestion;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/search/PlaySearchOneSuggestion;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/play/search/l;Z)V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchOneSuggestion;->b:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/play/search/l;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    iget-object v0, p1, Lcom/google/android/play/search/l;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchOneSuggestion;->a:Landroid/widget/ImageView;

    iget-object v1, p1, Lcom/google/android/play/search/l;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 47
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchOneSuggestion;->c:Landroid/view/View;

    if-eqz p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 48
    return-void

    .line 47
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 37
    sget v0, Lcom/google/android/play/g;->t:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchOneSuggestion;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchOneSuggestion;->a:Landroid/widget/ImageView;

    .line 38
    sget v0, Lcom/google/android/play/g;->af:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchOneSuggestion;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchOneSuggestion;->b:Landroid/widget/TextView;

    .line 39
    sget v0, Lcom/google/android/play/g;->ag:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchOneSuggestion;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchOneSuggestion;->c:Landroid/view/View;

    .line 40
    return-void
.end method
