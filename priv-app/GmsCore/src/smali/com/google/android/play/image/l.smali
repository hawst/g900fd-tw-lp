.class public final Lcom/google/android/play/image/l;
.super Lcom/android/volley/toolbox/u;
.source "SourceFile"


# static fields
.field private static final f:Landroid/graphics/Matrix;


# instance fields
.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 186
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, Lcom/google/android/play/image/l;->f:Landroid/graphics/Matrix;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;Lcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 7

    .prologue
    .line 193
    move-object v0, p0

    move-object v1, p1

    move-object v2, p5

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/android/volley/toolbox/u;-><init>(Ljava/lang/String;Lcom/android/volley/x;IILandroid/graphics/Bitmap$Config;Lcom/android/volley/w;)V

    .line 194
    return-void
.end method


# virtual methods
.method protected final a(Lcom/android/volley/m;)Lcom/android/volley/v;
    .locals 14

    .prologue
    .line 199
    invoke-super {p0, p1}, Lcom/android/volley/toolbox/u;->a(Lcom/android/volley/m;)Lcom/android/volley/v;

    move-result-object v2

    .line 203
    invoke-virtual {v2}, Lcom/android/volley/v;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/play/utils/a/i;->v:Lcom/google/android/play/utils/a/a;

    invoke-virtual {v0}, Lcom/google/android/play/utils/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 209
    :goto_0
    return-object v0

    .line 208
    :cond_1
    iget-object v0, v2, Lcom/android/volley/v;->a:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    iget-object v1, p1, Lcom/android/volley/m;->b:[B

    array-length v1, v1

    div-int/lit16 v3, v1, 0x400

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v5

    invoke-static {v1, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    sget-object v1, Lcom/google/android/play/image/l;->f:Landroid/graphics/Matrix;

    const/4 v6, 0x0

    invoke-virtual {v5, v0, v1, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    new-instance v6, Landroid/graphics/Paint;

    const/16 v1, 0x8

    invoke-direct {v6, v1}, Landroid/graphics/Paint;-><init>(I)V

    iget-object v1, p0, Lcom/android/volley/p;->b:Ljava/lang/String;

    const-string v7, "ggpht.com"

    invoke-virtual {v1, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    const v1, -0xff0001

    :goto_1
    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    const-string v1, "%dk"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v8

    invoke-static {v1, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v1, "%dh"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v1, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const-string v1, "%dw"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v1, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const/high16 v1, 0x42200000    # 40.0f

    :goto_2
    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    float-to-double v10, v1

    const-wide v12, 0x4008cccccccccccdL    # 3.1

    mul-double/2addr v10, v12

    invoke-virtual {v5}, Landroid/graphics/Canvas;->getHeight()I

    move-result v9

    int-to-double v12, v9

    cmpg-double v9, v10, v12

    if-gtz v9, :cond_2

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v9

    invoke-virtual {v6, v8}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    move-result v9

    invoke-virtual {v6, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    move-result v9

    float-to-double v10, v9

    const-wide v12, 0x3ff199999999999aL    # 1.1

    mul-double/2addr v10, v12

    invoke-virtual {v5}, Landroid/graphics/Canvas;->getWidth()I

    move-result v9

    int-to-double v12, v9

    cmpg-double v9, v10, v12

    if-ltz v9, :cond_4

    :cond_2
    const-wide v10, 0x3fe999999999999aL    # 0.8

    float-to-double v12, v1

    mul-double/2addr v10, v12

    double-to-float v1, v10

    goto :goto_2

    :cond_3
    const v1, -0xff01

    goto/16 :goto_1

    :cond_4
    invoke-virtual {v5}, Landroid/graphics/Canvas;->getHeight()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    sub-float/2addr v9, v1

    const/high16 v10, 0x40800000    # 4.0f

    invoke-virtual {v5, v3, v10, v9, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    const/high16 v3, 0x40a00000    # 5.0f

    add-float/2addr v3, v1

    add-float/2addr v3, v9

    const/high16 v9, 0x40800000    # 4.0f

    invoke-virtual {v5, v7, v9, v3, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    const/high16 v7, 0x40a00000    # 5.0f

    add-float/2addr v1, v7

    add-float/2addr v1, v3

    const/high16 v3, 0x40800000    # 4.0f

    invoke-virtual {v5, v8, v3, v1, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 209
    iget-object v0, v2, Lcom/android/volley/v;->b:Lcom/android/volley/c;

    invoke-static {v4, v0}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;

    move-result-object v0

    goto/16 :goto_0
.end method

.method protected final a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 214
    iget-boolean v0, p0, Lcom/google/android/play/image/l;->g:Z

    if-eqz v0, :cond_0

    .line 219
    :goto_0
    return-void

    .line 217
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/image/l;->g:Z

    .line 218
    invoke-super {p0, p1}, Lcom/android/volley/toolbox/u;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected final synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 185
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/google/android/play/image/l;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method
