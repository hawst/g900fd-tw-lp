.class Lcom/google/android/play/layout/FlowLayoutManager$SavedState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field a:I

.field b:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3537
    new-instance v0, Lcom/google/android/play/layout/k;

    invoke-direct {v0}, Lcom/google/android/play/layout/k;-><init>()V

    sput-object v0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3513
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 3515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3516
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->a:I

    .line 3517
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->b:F

    .line 3518
    return-void
.end method

.method public constructor <init>(Lcom/google/android/play/layout/FlowLayoutManager$SavedState;)V
    .locals 1

    .prologue
    .line 3520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3521
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->a:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->a:I

    .line 3522
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->b:F

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->b:F

    .line 3523
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 3527
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 3532
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3533
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->b:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 3534
    return-void
.end method
