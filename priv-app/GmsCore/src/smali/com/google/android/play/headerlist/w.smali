.class final Lcom/google/android/play/headerlist/w;
.super Landroid/database/DataSetObserver;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/ch;


# instance fields
.field final synthetic a:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

.field private b:I


# direct methods
.method private constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)V
    .locals 0

    .prologue
    .line 376
    iput-object p1, p0, Lcom/google/android/play/headerlist/w;->a:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;B)V
    .locals 0

    .prologue
    .line 376
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/w;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/google/android/play/headerlist/w;->a:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->d(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Landroid/support/v4/view/ch;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, Lcom/google/android/play/headerlist/w;->a:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->d(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Landroid/support/v4/view/ch;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/view/ch;->a(I)V

    .line 410
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/w;->a:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->b(I)V

    .line 411
    iget-object v0, p0, Lcom/google/android/play/headerlist/w;->a:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->c()V

    .line 412
    return-void
.end method

.method public final a(IFI)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 382
    iget-object v0, p0, Lcom/google/android/play/headerlist/w;->a:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->d(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Landroid/support/v4/view/ch;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/google/android/play/headerlist/w;->a:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->d(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Landroid/support/v4/view/ch;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/view/ch;->a(IFI)V

    .line 386
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/w;->a:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->e(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 403
    :cond_1
    :goto_0
    return-void

    .line 389
    :cond_2
    iget-object v0, p0, Lcom/google/android/play/headerlist/w;->a:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildCount()I

    move-result v0

    .line 390
    if-eqz v0, :cond_1

    if-ltz p1, :cond_1

    if-ge p1, v0, :cond_1

    .line 394
    iget-object v0, p0, Lcom/google/android/play/headerlist/w;->a:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->a(IF)V

    .line 395
    iget-object v0, p0, Lcom/google/android/play/headerlist/w;->a:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 396
    if-nez v0, :cond_3

    move v0, v1

    .line 397
    :goto_1
    add-int/lit8 v2, p1, 0x1

    .line 398
    iget-object v3, p0, Lcom/google/android/play/headerlist/w;->a:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-static {v3}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 399
    if-nez v2, :cond_4

    .line 400
    :goto_2
    add-int/2addr v0, v1

    int-to-float v0, v0

    mul-float/2addr v0, p2

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 402
    iget-object v1, p0, Lcom/google/android/play/headerlist/w;->a:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-static {v1, p1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;II)V

    goto :goto_0

    .line 396
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    goto :goto_1

    .line 399
    :cond_4
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v1

    goto :goto_2
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 416
    iput p1, p0, Lcom/google/android/play/headerlist/w;->b:I

    .line 417
    iget-object v0, p0, Lcom/google/android/play/headerlist/w;->a:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->d(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Landroid/support/v4/view/ch;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/google/android/play/headerlist/w;->a:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->d(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Landroid/support/v4/view/ch;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/view/ch;->b(I)V

    .line 420
    :cond_0
    iget v0, p0, Lcom/google/android/play/headerlist/w;->b:I

    if-nez v0, :cond_1

    .line 423
    iget-object v0, p0, Lcom/google/android/play/headerlist/w;->a:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;Z)Z

    .line 425
    :cond_1
    return-void
.end method

.method public final onChanged()V
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/play/headerlist/w;->a:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->f(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)V

    .line 430
    return-void
.end method
