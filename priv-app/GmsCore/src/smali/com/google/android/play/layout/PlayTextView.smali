.class public Lcom/google/android/play/layout/PlayTextView;
.super Landroid/widget/TextView;
.source "SourceFile"


# static fields
.field private static final a:Z


# instance fields
.field private final b:F

.field private c:Z

.field private final d:Ljava/lang/String;

.field private e:Landroid/graphics/Paint;

.field private f:Landroid/graphics/Paint;

.field private g:Landroid/graphics/drawable/GradientDrawable;

.field private h:I

.field private i:I

.field private j:F

.field private k:Landroid/graphics/Paint;

.field private l:Z

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/layout/PlayTextView;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    .line 94
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 96
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 98
    sget-object v1, Lcom/google/android/play/k;->ay:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 100
    sget v2, Lcom/google/android/play/k;->aB:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/play/layout/PlayTextView;->c:Z

    .line 102
    iget-boolean v2, p0, Lcom/google/android/play/layout/PlayTextView;->c:Z

    if-eqz v2, :cond_0

    .line 103
    sget v2, Lcom/google/android/play/k;->aB:I

    sget v3, Lcom/google/android/play/d;->v:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 105
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/google/android/play/layout/PlayTextView;->e:Landroid/graphics/Paint;

    .line 106
    iget-object v3, p0, Lcom/google/android/play/layout/PlayTextView;->e:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 107
    iget-object v3, p0, Lcom/google/android/play/layout/PlayTextView;->e:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 109
    sget v3, Lcom/google/android/play/e;->G:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/PlayTextView;->h:I

    .line 112
    new-instance v3, Landroid/graphics/drawable/GradientDrawable;

    sget-object v4, Landroid/graphics/drawable/GradientDrawable$Orientation;->LEFT_RIGHT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v5, 0x2

    new-array v5, v5, [I

    const v6, 0xffffff

    and-int/2addr v6, v2

    aput v6, v5, v9

    aput v2, v5, v10

    invoke-direct {v3, v4, v5}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    iput-object v3, p0, Lcom/google/android/play/layout/PlayTextView;->g:Landroid/graphics/drawable/GradientDrawable;

    .line 114
    sget v2, Lcom/google/android/play/e;->H:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/play/layout/PlayTextView;->i:I

    .line 117
    :cond_0
    sget v2, Lcom/google/android/play/k;->aC:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/play/layout/PlayTextView;->d:Ljava/lang/String;

    .line 118
    iget-object v2, p0, Lcom/google/android/play/layout/PlayTextView;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 119
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/play/layout/PlayTextView;->f:Landroid/graphics/Paint;

    .line 120
    iget-object v2, p0, Lcom/google/android/play/layout/PlayTextView;->f:Landroid/graphics/Paint;

    sget v3, Lcom/google/android/play/k;->aD:I

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getCurrentTextColor()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 123
    iget-object v2, p0, Lcom/google/android/play/layout/PlayTextView;->f:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getTextSize()F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 124
    iget-object v2, p0, Lcom/google/android/play/layout/PlayTextView;->f:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 125
    iget-object v2, p0, Lcom/google/android/play/layout/PlayTextView;->f:Landroid/graphics/Paint;

    invoke-virtual {v2, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 127
    :cond_1
    sget v2, Lcom/google/android/play/k;->aA:I

    invoke-virtual {v1, v2, v10}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/play/layout/PlayTextView;->m:I

    .line 130
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getTextSize()F

    move-result v2

    .line 131
    sget v3, Lcom/google/android/play/c;->a:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 132
    sget v3, Lcom/google/android/play/k;->az:I

    invoke-virtual {v1, v3, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    .line 134
    if-eqz v0, :cond_4

    if-eqz v3, :cond_4

    .line 138
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    .line 139
    const v3, 0x3f960419    # 1.172f

    mul-float/2addr v3, v2

    iget v4, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->descent:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    add-float/2addr v0, v4

    div-float v0, v3, v0

    .line 141
    sub-float v0, v8, v0

    invoke-static {v7, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/PlayTextView;->b:F

    .line 145
    :goto_0
    iget v0, p0, Lcom/google/android/play/layout/PlayTextView;->b:F

    cmpl-float v0, v0, v7

    if-lez v0, :cond_3

    .line 146
    iget v0, p0, Lcom/google/android/play/layout/PlayTextView;->b:F

    neg-float v0, v0

    mul-float/2addr v0, v2

    .line 147
    sget-boolean v2, Lcom/google/android/play/layout/PlayTextView;->a:Z

    if-eqz v2, :cond_2

    .line 150
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getLineHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getLineSpacingMultiplier()F

    move-result v3

    sub-float/2addr v3, v8

    mul-float/2addr v2, v3

    .line 151
    add-float/2addr v0, v2

    .line 153
    :cond_2
    invoke-virtual {p0, v0, v8}, Lcom/google/android/play/layout/PlayTextView;->setLineSpacing(FF)V

    .line 155
    :cond_3
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 157
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/e;->I:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/PlayTextView;->j:F

    .line 158
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/layout/PlayTextView;->k:Landroid/graphics/Paint;

    .line 159
    iget-object v0, p0, Lcom/google/android/play/layout/PlayTextView;->k:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/play/layout/PlayTextView;->j:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 160
    iget-object v0, p0, Lcom/google/android/play/layout/PlayTextView;->k:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 162
    invoke-virtual {p0, v9}, Lcom/google/android/play/layout/PlayTextView;->setWillNotDraw(Z)V

    .line 163
    return-void

    .line 143
    :cond_4
    iput v7, p0, Lcom/google/android/play/layout/PlayTextView;->b:F

    goto :goto_0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    const/4 v7, 0x0

    const/4 v8, -0x1

    .line 237
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 239
    iget-boolean v0, p0, Lcom/google/android/play/layout/PlayTextView;->l:Z

    if-eqz v0, :cond_0

    .line 246
    iget v0, p0, Lcom/google/android/play/layout/PlayTextView;->j:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 247
    int-to-float v1, v0

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getWidth()I

    move-result v3

    sub-int/2addr v3, v0

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getHeight()I

    move-result v4

    sub-int v0, v4, v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/play/layout/PlayTextView;->k:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 250
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/play/layout/PlayTextView;->c:Z

    if-nez v0, :cond_2

    .line 319
    :cond_1
    :goto_0
    return-void

    .line 254
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getHeight()I

    move-result v4

    .line 255
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getWidth()I

    move-result v9

    .line 256
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getLayout()Landroid/text/Layout;

    move-result-object v10

    .line 257
    if-eqz v10, :cond_1

    move v6, v7

    .line 260
    :goto_1
    invoke-virtual {v10}, Landroid/text/Layout;->getLineCount()I

    move-result v0

    if-ge v6, v0, :cond_1

    .line 263
    invoke-virtual {v10, v6}, Landroid/text/Layout;->getLineTop(I)I

    move-result v0

    .line 264
    invoke-virtual {v10, v6}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v1

    .line 265
    if-gt v0, v4, :cond_9

    if-le v1, v4, :cond_9

    .line 268
    const/4 v1, 0x0

    int-to-float v2, v0

    int-to-float v3, v9

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/play/layout/PlayTextView;->e:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 269
    if-lez v6, :cond_1

    move v0, v6

    .line 276
    :cond_3
    add-int/lit8 v0, v0, -0x1

    .line 277
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v0}, Landroid/text/Layout;->getLineStart(I)I

    move-result v3

    invoke-virtual {v1, v0}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v1

    :goto_2
    if-le v1, v3, :cond_8

    add-int/lit8 v4, v1, -0x1

    invoke-interface {v2, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v4

    if-nez v4, :cond_7

    .line 279
    :goto_3
    if-ne v1, v8, :cond_4

    .line 280
    if-gtz v0, :cond_3

    .line 284
    :cond_4
    if-ne v1, v8, :cond_a

    .line 287
    :goto_4
    invoke-virtual {v10, v0}, Landroid/text/Layout;->getLineTop(I)I

    move-result v8

    .line 288
    invoke-virtual {v10, v0}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v11

    .line 290
    invoke-virtual {v10, v7}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v0

    float-to-int v1, v0

    .line 291
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getPaddingLeft()I

    move-result v0

    .line 292
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getPaddingRight()I

    move-result v3

    .line 294
    add-int/2addr v0, v1

    .line 295
    iget-object v2, p0, Lcom/google/android/play/layout/PlayTextView;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 296
    iget-object v2, p0, Lcom/google/android/play/layout/PlayTextView;->f:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/google/android/play/layout/PlayTextView;->d:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-int v2, v2

    .line 298
    sub-int v4, v9, v3

    sub-int v12, v4, v2

    .line 299
    iget v2, p0, Lcom/google/android/play/layout/PlayTextView;->i:I

    sub-int v2, v12, v2

    if-ge v2, v1, :cond_5

    .line 300
    iget v0, p0, Lcom/google/android/play/layout/PlayTextView;->i:I

    sub-int v7, v12, v0

    .line 302
    int-to-float v1, v7

    int-to-float v2, v8

    sub-int v0, v9, v3

    int-to-float v3, v0

    int-to-float v4, v11

    iget-object v5, p0, Lcom/google/android/play/layout/PlayTextView;->e:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    move v0, v7

    .line 305
    :cond_5
    iget-object v1, p0, Lcom/google/android/play/layout/PlayTextView;->d:Ljava/lang/String;

    int-to-float v2, v12

    add-int/lit8 v3, v6, -0x1

    invoke-virtual {v10, v3}, Landroid/text/Layout;->getLineBaseline(I)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/play/layout/PlayTextView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 309
    :cond_6
    iget-object v1, p0, Lcom/google/android/play/layout/PlayTextView;->g:Landroid/graphics/drawable/GradientDrawable;

    iget v2, p0, Lcom/google/android/play/layout/PlayTextView;->h:I

    sub-int v2, v0, v2

    invoke-virtual {v1, v2, v8, v0, v11}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    .line 314
    iget-object v0, p0, Lcom/google/android/play/layout/PlayTextView;->g:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 277
    :cond_7
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_8
    move v1, v8

    goto :goto_3

    .line 262
    :cond_9
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_1

    :cond_a
    move v7, v1

    goto :goto_4
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    .line 167
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onMeasure(II)V

    .line 169
    iget v0, p0, Lcom/google/android/play/layout/PlayTextView;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    if-eq v0, v1, :cond_0

    .line 178
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v1

    iget v2, p0, Lcom/google/android/play/layout/PlayTextView;->b:F

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getLineHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/play/layout/PlayTextView;->setMeasuredDimension(II)V

    goto :goto_0
.end method
