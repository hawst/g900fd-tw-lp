.class public final Lcom/google/android/play/image/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/play/image/o;


# static fields
.field private static u:Lcom/google/android/play/image/a;


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Paint;

.field private final e:Landroid/graphics/RectF;

.field private final f:Landroid/graphics/Paint;

.field private final g:Landroid/graphics/Paint;

.field private final h:I

.field private final i:I

.field private final j:Landroid/graphics/Paint;

.field private k:Z

.field private final l:Landroid/graphics/Rect;

.field private final m:Landroid/graphics/Rect;

.field private final n:I

.field private final o:I

.field private final p:I

.field private final q:I

.field private final r:F

.field private final s:F

.field private final t:Z


# direct methods
.method private constructor <init>(Landroid/content/res/Resources;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/high16 v3, 0x3f000000    # 0.5f

    const/4 v4, 0x1

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/image/a;->k:Z

    .line 105
    sget v0, Lcom/google/android/play/e;->c:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/image/a;->n:I

    .line 107
    sget v0, Lcom/google/android/play/e;->b:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/image/a;->o:I

    .line 109
    sget v0, Lcom/google/android/play/e;->h:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/image/a;->p:I

    .line 110
    sget v0, Lcom/google/android/play/e;->g:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/image/a;->q:I

    .line 113
    sget v0, Lcom/google/android/play/e;->e:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/play/image/a;->r:F

    .line 115
    sget v0, Lcom/google/android/play/e;->d:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/play/image/a;->s:F

    .line 118
    sget v0, Lcom/google/android/play/d;->b:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 119
    sget v1, Lcom/google/android/play/d;->v:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 123
    sget v2, Lcom/google/android/play/e;->f:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v3

    .line 125
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/google/android/play/image/a;->a:Landroid/graphics/Paint;

    .line 126
    iget-object v3, p0, Lcom/google/android/play/image/a;->a:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 127
    iget-object v0, p0, Lcom/google/android/play/image/a;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 128
    iget-object v0, p0, Lcom/google/android/play/image/a;->a:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/play/image/a;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 131
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/image/a;->b:Landroid/graphics/Paint;

    .line 132
    iget-object v0, p0, Lcom/google/android/play/image/a;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 133
    iget-object v0, p0, Lcom/google/android/play/image/a;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 134
    iget-object v0, p0, Lcom/google/android/play/image/a;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 136
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/play/image/a;->c:Landroid/graphics/Paint;

    .line 137
    iget-object v0, p0, Lcom/google/android/play/image/a;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 139
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/play/image/a;->d:Landroid/graphics/Paint;

    .line 141
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    .line 143
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/image/a;->f:Landroid/graphics/Paint;

    .line 144
    iget-object v0, p0, Lcom/google/android/play/image/a;->f:Landroid/graphics/Paint;

    sget v1, Lcom/google/android/play/d;->c:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 145
    iget-object v0, p0, Lcom/google/android/play/image/a;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 146
    iget-object v0, p0, Lcom/google/android/play/image/a;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 148
    sget v0, Lcom/google/android/play/d;->d:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/image/a;->h:I

    .line 149
    sget v0, Lcom/google/android/play/d;->a:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/image/a;->i:I

    .line 151
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/image/a;->g:Landroid/graphics/Paint;

    .line 152
    iget-object v0, p0, Lcom/google/android/play/image/a;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 153
    iget-object v0, p0, Lcom/google/android/play/image/a;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 154
    iget-object v0, p0, Lcom/google/android/play/image/a;->g:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 156
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/image/a;->j:Landroid/graphics/Paint;

    .line 157
    iget-object v0, p0, Lcom/google/android/play/image/a;->j:Landroid/graphics/Paint;

    sget v1, Lcom/google/android/play/d;->v:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 158
    iget-object v0, p0, Lcom/google/android/play/image/a;->j:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 160
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/image/a;->l:Landroid/graphics/Rect;

    .line 161
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/image/a;->m:Landroid/graphics/Rect;

    .line 163
    iput-boolean v4, p0, Lcom/google/android/play/image/a;->t:Z

    .line 164
    return-void
.end method

.method private static a(FFFFF)F
    .locals 2

    .prologue
    .line 332
    cmpg-float v0, p4, p0

    if-gtz v0, :cond_1

    .line 342
    :cond_0
    :goto_0
    return p2

    .line 335
    :cond_1
    cmpl-float v0, p4, p1

    if-ltz v0, :cond_2

    move p2, p3

    .line 336
    goto :goto_0

    .line 338
    :cond_2
    cmpl-float v0, p0, p1

    if-eqz v0, :cond_0

    .line 342
    sub-float v0, p4, p0

    sub-float v1, p3, p2

    mul-float/2addr v0, v1

    sub-float v1, p1, p0

    div-float/2addr v0, v1

    add-float/2addr p2, v0

    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/content/res/Resources;)Lcom/google/android/play/image/a;
    .locals 2

    .prologue
    .line 98
    const-class v1, Lcom/google/android/play/image/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/play/image/a;->u:Lcom/google/android/play/image/a;

    if-nez v0, :cond_0

    .line 99
    new-instance v0, Lcom/google/android/play/image/a;

    invoke-direct {v0, p0}, Lcom/google/android/play/image/a;-><init>(Landroid/content/res/Resources;)V

    sput-object v0, Lcom/google/android/play/image/a;->u:Lcom/google/android/play/image/a;

    .line 101
    :cond_0
    sget-object v0, Lcom/google/android/play/image/a;->u:Lcom/google/android/play/image/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Landroid/graphics/Canvas;FI)V
    .locals 9

    .prologue
    const/high16 v8, 0x40400000    # 3.0f

    const/high16 v6, 0x40000000    # 2.0f

    .line 380
    iget-object v0, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    .line 381
    iget-object v1, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    .line 382
    iget-object v2, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    .line 383
    iget-object v3, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    .line 387
    iget-object v4, p0, Lcom/google/android/play/image/a;->a:Landroid/graphics/Paint;

    mul-float v5, v6, p2

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 388
    iget-object v4, p0, Lcom/google/android/play/image/a;->a:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v4

    .line 389
    div-float/2addr v4, v6

    .line 390
    iget-object v5, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->left:F

    div-float v7, p2, v8

    sub-float v7, v4, v7

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->left:F

    .line 391
    iget-object v5, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->top:F

    add-float v7, v4, p2

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->top:F

    .line 392
    iget-object v5, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->right:F

    div-float v7, p2, v8

    sub-float v7, v4, v7

    sub-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->right:F

    .line 393
    iget-object v5, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v4, p2

    sub-float v4, v6, v4

    iput v4, v5, Landroid/graphics/RectF;->bottom:F

    .line 394
    iget-object v4, p0, Lcom/google/android/play/image/a;->a:Landroid/graphics/Paint;

    invoke-virtual {v4, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 395
    iget-object v4, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/google/android/play/image/a;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 398
    iget-object v4, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iput v0, v4, Landroid/graphics/RectF;->left:F

    .line 399
    iget-object v0, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 400
    iget-object v0, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 401
    iget-object v0, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iput v3, v0, Landroid/graphics/RectF;->bottom:F

    .line 402
    return-void
.end method

.method private a(Landroid/graphics/Canvas;IFFF)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 349
    iget-object v0, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    .line 350
    iget-object v1, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    .line 351
    iget-object v2, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    .line 352
    iget-object v3, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    .line 354
    iget-object v4, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 355
    iget-object v4, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 356
    iget-object v4, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    int-to-float v5, p2

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 357
    iget-object v4, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    int-to-float v5, p2

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    .line 359
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 362
    invoke-virtual {p1, p5, p5}, Landroid/graphics/Canvas;->scale(FF)V

    .line 365
    invoke-virtual {p1, p4, p4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 368
    iget-object v4, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/google/android/play/image/a;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, p3, p3, v5}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 369
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 372
    iget-object v4, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iput v0, v4, Landroid/graphics/RectF;->left:F

    .line 373
    iget-object v0, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 374
    iget-object v0, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 375
    iget-object v0, p0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iput v3, v0, Landroid/graphics/RectF;->bottom:F

    .line 376
    return-void
.end method

.method private b(II)F
    .locals 5

    .prologue
    .line 176
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 177
    iget v1, p0, Lcom/google/android/play/image/a;->n:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v1, p0, Lcom/google/android/play/image/a;->n:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/play/image/a;->o:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/play/image/a;->r:F

    iget v4, p0, Lcom/google/android/play/image/a;->s:F

    int-to-float v0, v0

    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/play/image/a;->a(FFFFF)F

    move-result v0

    goto :goto_0
.end method

.method private c(II)F
    .locals 5

    .prologue
    .line 189
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 190
    iget v1, p0, Lcom/google/android/play/image/a;->n:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/play/image/a;->o:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/play/image/a;->p:I

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/play/image/a;->q:I

    int-to-float v4, v4

    int-to-float v0, v0

    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/play/image/a;->a(FFFFF)F

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(II)I
    .locals 2

    .prologue
    .line 196
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 197
    iget v1, p0, Lcom/google/android/play/image/a;->n:I

    if-ge v0, v1, :cond_0

    .line 198
    const/4 v0, 0x0

    .line 203
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    invoke-direct {p0, p1, p2}, Lcom/google/android/play/image/a;->c(II)F

    move-result v1

    mul-float/2addr v0, v1

    invoke-direct {p0, p1, p2}, Lcom/google/android/play/image/a;->b(II)F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 18

    .prologue
    .line 209
    if-nez p1, :cond_0

    .line 210
    const/4 v3, 0x0

    .line 319
    :goto_0
    return-object v3

    .line 213
    :cond_0
    invoke-static/range {p2 .. p3}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 216
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/play/image/a;->n:I

    if-lt v13, v3, :cond_4

    const/4 v3, 0x1

    move v9, v3

    .line 219
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/play/image/a;->t:Z

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/google/android/play/image/a;->c(II)F

    move-result v3

    move v10, v3

    .line 222
    :goto_2
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/google/android/play/image/a;->b(II)F

    move-result v14

    .line 224
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/play/image/a;->n:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/play/image/a;->o:I

    int-to-float v4, v4

    const/high16 v5, 0x42400000    # 48.0f

    const/high16 v6, 0x42800000    # 64.0f

    int-to-float v7, v13

    invoke-static {v3, v4, v5, v6, v7}, Lcom/google/android/play/image/a;->a(FFFFF)F

    move-result v3

    float-to-int v3, v3

    .line 227
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/image/a;->a(II)I

    move-result v4

    .line 230
    shl-int/lit8 v15, v3, 0x18

    .line 237
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    .line 238
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v16

    .line 239
    move/from16 v0, v16

    invoke-static {v11, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 240
    move/from16 v0, v16

    invoke-static {v11, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 241
    sub-int v6, v11, v16

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    const/4 v7, 0x1

    if-gt v6, v7, :cond_1

    sub-int v4, v13, v4

    if-lt v3, v4, :cond_1

    if-gt v5, v13, :cond_1

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/play/image/a;->k:Z

    if-eqz v3, :cond_7

    .line 244
    :cond_1
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v13, v13, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 245
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 247
    const/4 v4, 0x0

    const/4 v5, 0x0

    int-to-float v6, v13

    int-to-float v7, v13

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/play/image/a;->j:Landroid/graphics/Paint;

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 253
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/image/a;->l:Landroid/graphics/Rect;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move/from16 v0, v16

    invoke-virtual {v4, v5, v6, v11, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 254
    int-to-float v4, v13

    move/from16 v0, v16

    invoke-static {v11, v0}, Ljava/lang/Math;->max(II)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    .line 256
    int-to-float v5, v11

    mul-float/2addr v5, v4

    float-to-int v5, v5

    .line 257
    move/from16 v0, v16

    int-to-float v6, v0

    mul-float/2addr v4, v6

    float-to-int v4, v4

    .line 258
    sub-int v6, v13, v5

    div-int/lit8 v6, v6, 0x2

    .line 259
    sub-int v7, v13, v4

    div-int/lit8 v7, v7, 0x2

    .line 260
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/play/image/a;->m:Landroid/graphics/Rect;

    add-int/2addr v5, v6

    add-int/2addr v4, v7

    invoke-virtual {v8, v6, v7, v5, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 263
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/image/a;->l:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/image/a;->m:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/play/image/a;->d:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 265
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 266
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-object/from16 p1, v12

    .line 269
    :goto_3
    new-instance v3, Landroid/graphics/BitmapShader;

    sget-object v4, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v6, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v4, v6}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 271
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/image/a;->c:Landroid/graphics/Paint;

    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 273
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v13, v13, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 274
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v11}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 275
    int-to-float v3, v13

    const/high16 v6, 0x40000000    # 2.0f

    div-float v6, v3, v6

    .line 279
    const/high16 v3, 0x3f800000    # 1.0f

    .line 280
    if-eqz v9, :cond_2

    .line 286
    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v3, v14

    .line 290
    const/high16 v7, 0x40000000    # 2.0f

    div-float v7, v14, v7

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 292
    :cond_2
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    const/4 v8, 0x0

    const/4 v12, 0x0

    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v16, v0

    sub-float v16, v16, v3

    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v17, v0

    sub-float v3, v17, v3

    move/from16 v0, v16

    invoke-virtual {v7, v8, v12, v0, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 295
    if-eqz v9, :cond_6

    .line 297
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14, v15}, Lcom/google/android/play/image/a;->a(Landroid/graphics/Canvas;FI)V

    .line 302
    int-to-float v3, v13

    sub-float/2addr v3, v14

    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v8, 0x40000000    # 2.0f

    mul-float/2addr v8, v10

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v7

    sub-float/2addr v3, v7

    .line 303
    int-to-float v7, v5

    div-float v8, v3, v7

    move-object/from16 v3, p0

    move v7, v10

    .line 304
    invoke-direct/range {v3 .. v8}, Lcom/google/android/play/image/a;->a(Landroid/graphics/Canvas;IFFF)V

    .line 306
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/play/image/a;->t:Z

    if-eqz v3, :cond_3

    .line 307
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/play/image/a;->b:Landroid/graphics/Paint;

    invoke-virtual {v8, v10}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/high16 v8, 0x40000000    # 2.0f

    div-float v8, v10, v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v10, v9, Landroid/graphics/RectF;->left:F

    add-float/2addr v10, v8

    iput v10, v9, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v10, v9, Landroid/graphics/RectF;->top:F

    add-float/2addr v10, v8

    iput v10, v9, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v10, v9, Landroid/graphics/RectF;->right:F

    sub-float/2addr v10, v8

    iput v10, v9, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iget v10, v9, Landroid/graphics/RectF;->bottom:F

    sub-float v8, v10, v8

    iput v8, v9, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/play/image/a;->b:Landroid/graphics/Paint;

    invoke-virtual {v4, v8, v9}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iput v3, v4, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iput v5, v3, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iput v6, v3, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/play/image/a;->e:Landroid/graphics/RectF;

    iput v7, v3, Landroid/graphics/RectF;->bottom:F

    .line 317
    :cond_3
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/play/image/a;->c:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    move-object v3, v11

    .line 319
    goto/16 :goto_0

    .line 216
    :cond_4
    const/4 v3, 0x0

    move v9, v3

    goto/16 :goto_1

    .line 219
    :cond_5
    const/4 v3, 0x0

    move v10, v3

    goto/16 :goto_2

    .line 311
    :cond_6
    const/4 v7, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/play/image/a;->a(Landroid/graphics/Canvas;IFFF)V

    .line 313
    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v3, v5}, Lcom/google/android/play/image/a;->a(Landroid/graphics/Canvas;FI)V

    goto :goto_4

    :cond_7
    move v5, v11

    goto/16 :goto_3
.end method

.method public final a(Landroid/graphics/Canvas;II)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 431
    invoke-direct {p0, p2, p3}, Lcom/google/android/play/image/a;->b(II)F

    move-result v0

    .line 432
    int-to-float v1, p2

    sub-float/2addr v1, v0

    float-to-int v1, v1

    .line 433
    int-to-float v2, p3

    sub-float/2addr v2, v0

    float-to-int v2, v2

    .line 435
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 436
    div-float/2addr v0, v4

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 438
    int-to-float v0, v1

    div-float/2addr v0, v4

    int-to-float v1, v2

    div-float/2addr v1, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 439
    iget-object v1, p0, Lcom/google/android/play/image/a;->g:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    .line 440
    iget-object v2, p0, Lcom/google/android/play/image/a;->g:Landroid/graphics/Paint;

    iget v3, p0, Lcom/google/android/play/image/a;->i:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 441
    div-float/2addr v1, v4

    sub-float v1, v0, v1

    iget-object v2, p0, Lcom/google/android/play/image/a;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v0, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 444
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 445
    return-void
.end method

.method public final b(Landroid/graphics/Canvas;II)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 450
    invoke-direct {p0, p2, p3}, Lcom/google/android/play/image/a;->b(II)F

    move-result v0

    .line 451
    int-to-float v1, p2

    sub-float/2addr v1, v0

    float-to-int v1, v1

    .line 452
    int-to-float v2, p3

    sub-float/2addr v2, v0

    float-to-int v2, v2

    .line 454
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 455
    div-float/2addr v0, v4

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 457
    int-to-float v0, v1

    div-float/2addr v0, v4

    int-to-float v1, v2

    div-float/2addr v1, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 458
    iget-object v1, p0, Lcom/google/android/play/image/a;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v0, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 459
    iget-object v1, p0, Lcom/google/android/play/image/a;->g:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    .line 460
    iget-object v2, p0, Lcom/google/android/play/image/a;->g:Landroid/graphics/Paint;

    iget v3, p0, Lcom/google/android/play/image/a;->h:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 461
    div-float/2addr v1, v4

    sub-float v1, v0, v1

    iget-object v2, p0, Lcom/google/android/play/image/a;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v0, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 464
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 465
    return-void
.end method
