.class public Lcom/google/android/play/search/PlaySearchSuggestionsList;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/play/search/e;


# instance fields
.field private a:Landroid/support/v7/widget/RecyclerView;

.field private b:Lcom/google/android/play/search/i;

.field private c:Lcom/google/android/play/search/d;

.field private d:Landroid/support/v7/widget/ce;

.field private e:Landroid/support/v7/widget/bx;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/search/PlaySearchSuggestionsList;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/search/PlaySearchSuggestionsList;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->c:Lcom/google/android/play/search/d;

    if-nez v0, :cond_0

    .line 87
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->c:Lcom/google/android/play/search/d;

    iget v0, v0, Lcom/google/android/play/search/d;->a:I

    .line 82
    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->b:Lcom/google/android/play/search/i;

    invoke-virtual {v1}, Lcom/google/android/play/search/i;->a()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 83
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchSuggestionsList;->setVisibility(I)V

    goto :goto_0

    .line 85
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchSuggestionsList;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/play/search/PlaySearchSuggestionsList;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/play/search/PlaySearchSuggestionsList;->a()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/google/android/play/search/PlaySearchSuggestionsList;->a()V

    .line 92
    return-void
.end method

.method public final a(Lcom/google/android/play/search/d;)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->c:Lcom/google/android/play/search/d;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->c:Lcom/google/android/play/search/d;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/d;->b(Lcom/google/android/play/search/e;)V

    .line 65
    :cond_0
    iput-object p1, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->c:Lcom/google/android/play/search/d;

    .line 66
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->c:Lcom/google/android/play/search/d;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/d;->a(Lcom/google/android/play/search/e;)V

    .line 67
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 97
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 40
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 41
    sget v0, Lcom/google/android/play/g;->ah:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchSuggestionsList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->a:Landroid/support/v7/widget/RecyclerView;

    .line 43
    new-instance v0, Lcom/google/android/play/search/k;

    invoke-virtual {p0}, Lcom/google/android/play/search/PlaySearchSuggestionsList;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/play/search/k;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->d:Landroid/support/v7/widget/ce;

    .line 44
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->d:Landroid/support/v7/widget/ce;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/ce;)V

    .line 46
    new-instance v0, Lcom/google/android/play/search/i;

    invoke-direct {v0}, Lcom/google/android/play/search/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->b:Lcom/google/android/play/search/i;

    .line 47
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->b:Lcom/google/android/play/search/i;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/bv;)V

    .line 48
    new-instance v0, Lcom/google/android/play/search/m;

    invoke-direct {v0, p0}, Lcom/google/android/play/search/m;-><init>(Lcom/google/android/play/search/PlaySearchSuggestionsList;)V

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->e:Landroid/support/v7/widget/bx;

    .line 54
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->b:Lcom/google/android/play/search/i;

    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->e:Landroid/support/v7/widget/bx;

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/i;->a(Landroid/support/v7/widget/bx;)V

    .line 55
    invoke-direct {p0}, Lcom/google/android/play/search/PlaySearchSuggestionsList;->a()V

    .line 56
    return-void
.end method
