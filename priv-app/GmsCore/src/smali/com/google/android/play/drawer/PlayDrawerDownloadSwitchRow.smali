.class Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/Checkable;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field private static final a:Z


# instance fields
.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/Switch;

.field private d:Z

.field private e:I

.field private f:I

.field private g:Z

.field private h:Lcom/google/android/play/drawer/b;

.field private final i:Landroid/view/View$OnTouchListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    new-instance v0, Lcom/google/android/play/drawer/a;

    invoke-direct {v0, p0}, Lcom/google/android/play/drawer/a;-><init>(Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;)V

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->i:Landroid/view/View$OnTouchListener;

    .line 58
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/d;->h:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->e:I

    .line 59
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->d:Z

    return v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->d:Z

    if-eq p2, v0, :cond_0

    .line 153
    invoke-virtual {p0, p2}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->setChecked(Z)V

    .line 155
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->toggle()V

    .line 148
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 4

    .prologue
    .line 134
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 135
    iget-boolean v1, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->d:Z

    if-eqz v1, :cond_0

    .line 136
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x1010106

    aput v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mergeDrawableStates([I[I)[I

    .line 138
    :cond_0
    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 80
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 81
    sget v0, Lcom/google/android/play/g;->f:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->b:Landroid/widget/TextView;

    .line 82
    invoke-virtual {p0, p0}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    sget v0, Lcom/google/android/play/g;->ak:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_0

    .line 85
    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->c:Landroid/widget/Switch;

    .line 86
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->c:Landroid/widget/Switch;

    invoke-virtual {v0, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->c:Landroid/widget/Switch;

    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->i:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 89
    :cond_0
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 172
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 173
    const-class v0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 174
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->d:Z

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    .line 175
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 164
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 165
    const-class v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 166
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCheckable(Z)V

    .line 167
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->d:Z

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setChecked(Z)V

    .line 168
    return-void
.end method

.method public setChecked(Z)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->d:Z

    if-eq v0, p1, :cond_1

    .line 96
    iput-boolean p1, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->d:Z

    sget-boolean v0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->c:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->c:Landroid/widget/Switch;

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->c:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->refreshDrawableState()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->b:Landroid/widget/TextView;

    if-eqz p1, :cond_2

    iget v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->f:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 97
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->g:Z

    if-eqz v0, :cond_3

    .line 110
    :cond_1
    :goto_1
    return-void

    .line 96
    :cond_2
    iget v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->e:I

    goto :goto_0

    .line 100
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_4

    .line 101
    const/16 v0, 0x800

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->sendAccessibilityEvent(I)V

    .line 104
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->g:Z

    .line 105
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->h:Lcom/google/android/play/drawer/b;

    if-eqz v0, :cond_5

    .line 106
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->h:Lcom/google/android/play/drawer/b;

    .line 108
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->g:Z

    goto :goto_1
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->setChecked(Z)V

    .line 130
    return-void

    .line 129
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
