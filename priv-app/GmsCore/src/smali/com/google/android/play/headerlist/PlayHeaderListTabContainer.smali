.class public Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# static fields
.field private static a:[I

.field private static b:[I


# instance fields
.field private final c:I

.field private final d:Landroid/graphics/Paint;

.field private e:I

.field private f:F

.field private g:Z

.field private h:I

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 21
    new-array v0, v1, [I

    sput-object v0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->a:[I

    .line 22
    new-array v0, v1, [I

    sput-object v0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->b:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->setWillNotDraw(Z)V

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 46
    sget v1, Lcom/google/android/play/e;->v:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->c:I

    .line 49
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->d:Landroid/graphics/Paint;

    .line 50
    return-void
.end method

.method private static a(Landroid/view/View;[I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 167
    instance-of v0, p0, Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 168
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v0

    aput v0, p1, v2

    .line 169
    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v0

    aput v0, p1, v3

    .line 181
    :goto_0
    return-void

    .line 172
    :cond_0
    check-cast p0, Landroid/widget/TextView;

    .line 173
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 174
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 175
    :cond_1
    invoke-virtual {p0}, Landroid/widget/TextView;->getLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/widget/TextView;->getRight()I

    move-result v1

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    aput v0, p1, v2

    .line 176
    aget v0, p1, v2

    aput v0, p1, v3

    goto :goto_0

    .line 178
    :cond_2
    invoke-virtual {p0}, Landroid/widget/TextView;->getLeft()I

    move-result v0

    aput v0, p1, v2

    .line 179
    invoke-virtual {p0}, Landroid/widget/TextView;->getRight()I

    move-result v0

    aput v0, p1, v3

    goto :goto_0
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v4, -0x2

    const/4 v1, 0x0

    .line 105
    .line 107
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->g:Z

    if-eqz v0, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/play/e;->t:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v2, v0

    .line 124
    :goto_0
    invoke-virtual {p0, v2, v1, v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->setPadding(IIII)V

    .line 125
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->i:Z

    .line 126
    return-void

    .line 112
    :cond_0
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->h:I

    div-int/lit8 v2, v0, 0x2

    .line 113
    invoke-virtual {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 114
    if-eqz v0, :cond_2

    .line 115
    invoke-virtual {v0, v4, v4}, Landroid/view/View;->measure(II)V

    .line 116
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v2, v0

    .line 118
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, v3}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 119
    if-eqz v3, :cond_1

    .line 120
    invoke-virtual {v3, v4, v4}, Landroid/view/View;->measure(II)V

    .line 121
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    move v5, v2

    move v2, v0

    move v0, v5

    goto :goto_0

    :cond_1
    move v2, v0

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method final a()V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->i:Z

    .line 98
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->invalidate()V

    .line 58
    return-void
.end method

.method final a(IF)V
    .locals 0

    .prologue
    .line 65
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->e:I

    .line 66
    iput p2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->f:F

    .line 67
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->invalidate()V

    .line 68
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->g:Z

    if-eq v0, p1, :cond_0

    .line 131
    iput-boolean p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->g:Z

    .line 132
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->b()V

    .line 134
    :cond_0
    return-void
.end method

.method final b(I)V
    .locals 1

    .prologue
    .line 75
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->e:I

    .line 76
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->f:F

    .line 77
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->invalidate()V

    .line 78
    return-void
.end method

.method final c(I)V
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->h:I

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->i:Z

    if-nez v0, :cond_1

    .line 88
    :cond_0
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->h:I

    .line 89
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->b()V

    .line 91
    :cond_1
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 138
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildCount()I

    move-result v0

    .line 141
    if-lez v0, :cond_1

    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->e:I

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 142
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->e:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->a:[I

    invoke-static {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->a(Landroid/view/View;[I)V

    .line 143
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->f:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->e:I

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 145
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->e:I

    add-int/lit8 v0, v0, 0x1

    .line 146
    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->b:[I

    invoke-static {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->a(Landroid/view/View;[I)V

    .line 147
    sget-object v0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->a:[I

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->f:F

    sget-object v2, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->b:[I

    aget v2, v2, v4

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->f:F

    sub-float v2, v6, v2

    sget-object v3, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->a:[I

    aget v3, v3, v4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    aput v1, v0, v4

    .line 150
    sget-object v0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->a:[I

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->f:F

    sget-object v2, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->b:[I

    aget v2, v2, v5

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->f:F

    sub-float v2, v6, v2

    sget-object v3, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->a:[I

    aget v3, v3, v5

    int-to-float v3, v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    aput v1, v0, v5

    .line 154
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getHeight()I

    move-result v0

    .line 155
    sget-object v1, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->a:[I

    aget v1, v1, v4

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->c:I

    sub-int v2, v0, v2

    int-to-float v2, v2

    sget-object v3, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->a:[I

    aget v3, v3, v5

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->d:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 159
    :cond_1
    return-void
.end method
