.class public final Lcom/google/android/play/headerlist/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field protected a:I

.field private final b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field private final c:Landroid/database/DataSetObserver;

.field private d:Landroid/widget/Adapter;

.field private final e:[Landroid/util/SparseIntArray;

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 3

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/util/SparseIntArray;

    const/4 v1, 0x0

    new-instance v2, Landroid/util/SparseIntArray;

    invoke-direct {v2}, Landroid/util/SparseIntArray;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Landroid/util/SparseIntArray;

    invoke-direct {v2}, Landroid/util/SparseIntArray;-><init>()V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/play/headerlist/q;->e:[Landroid/util/SparseIntArray;

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/headerlist/q;->f:I

    .line 50
    iput-object p1, p0, Lcom/google/android/play/headerlist/q;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 51
    new-instance v0, Lcom/google/android/play/headerlist/r;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/r;-><init>(Lcom/google/android/play/headerlist/q;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/q;->c:Landroid/database/DataSetObserver;

    .line 68
    return-void
.end method

.method private a(Landroid/widget/Adapter;)V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/play/headerlist/q;->d:Landroid/widget/Adapter;

    if-ne v0, p1, :cond_0

    .line 104
    :goto_0
    return-void

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/q;->d:Landroid/widget/Adapter;

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/google/android/play/headerlist/q;->d:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/google/android/play/headerlist/q;->c:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 99
    :cond_1
    iput-object p1, p0, Lcom/google/android/play/headerlist/q;->d:Landroid/widget/Adapter;

    .line 100
    iget-object v0, p0, Lcom/google/android/play/headerlist/q;->d:Landroid/widget/Adapter;

    if-eqz v0, :cond_2

    .line 101
    iget-object v0, p0, Lcom/google/android/play/headerlist/q;->d:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/google/android/play/headerlist/q;->c:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 103
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/q;->a(Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/play/headerlist/q;)V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/q;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/android/play/headerlist/q;->b()Landroid/util/SparseIntArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 84
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/headerlist/q;->f:I

    .line 85
    if-eqz p1, :cond_0

    .line 86
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/q;->a(Landroid/widget/Adapter;)V

    .line 88
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/headerlist/q;->a:I

    .line 89
    return-void
.end method

.method private b()Landroid/util/SparseIntArray;
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/play/headerlist/q;->e:[Landroid/util/SparseIntArray;

    iget v1, p0, Lcom/google/android/play/headerlist/q;->g:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/play/headerlist/q;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/play/headerlist/q;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method


# virtual methods
.method final a()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/q;->a(Z)V

    .line 76
    return-void
.end method

.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 138
    invoke-static {p3, p4}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 139
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/q;->a(Landroid/widget/Adapter;)V

    .line 144
    invoke-direct {p0}, Lcom/google/android/play/headerlist/q;->b()Landroid/util/SparseIntArray;

    move-result-object v5

    .line 145
    iget-object v0, p0, Lcom/google/android/play/headerlist/q;->e:[Landroid/util/SparseIntArray;

    iget v1, p0, Lcom/google/android/play/headerlist/q;->g:I

    add-int/lit8 v1, v1, 0x1

    rem-int/lit8 v1, v1, 0x2

    aget-object v6, v0, v1

    .line 146
    invoke-virtual {v6}, Landroid/util/SparseIntArray;->clear()V

    move v0, p2

    :goto_0
    add-int v1, p2, v4

    if-ge v0, v1, :cond_0

    sub-int v1, v0, p2

    invoke-virtual {p1, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {v6, v0, v1}, Landroid/util/SparseIntArray;->put(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v1, p2

    move v0, v3

    .line 147
    :goto_1
    add-int v7, p2, v4

    if-ge v1, v7, :cond_6

    .line 148
    invoke-virtual {v5, v1, v3}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    .line 149
    if-eq v0, v3, :cond_3

    .line 150
    invoke-virtual {v6, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    sub-int v1, v0, v1

    .line 155
    :goto_2
    iget v5, p0, Lcom/google/android/play/headerlist/q;->g:I

    add-int/lit8 v5, v5, 0x1

    rem-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/android/play/headerlist/q;->g:I

    .line 158
    iget v5, p0, Lcom/google/android/play/headerlist/q;->f:I

    if-eq v5, v3, :cond_1

    if-ne v0, v3, :cond_4

    .line 161
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/headerlist/q;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/view/ViewGroup;)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/q;->f:I

    .line 166
    :goto_3
    iget-object v0, p0, Lcom/google/android/play/headerlist/q;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget v3, p0, Lcom/google/android/play/headerlist/q;->a:I

    invoke-virtual {p1}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v5

    if-nez v5, :cond_5

    :goto_4
    invoke-virtual {v0, v3, v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(III)V

    .line 167
    iget-object v0, p0, Lcom/google/android/play/headerlist/q;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_2

    .line 168
    iget-object v0, p0, Lcom/google/android/play/headerlist/q;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, v4, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    .line 171
    :cond_2
    return-void

    .line 147
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 163
    :cond_4
    iget v0, p0, Lcom/google/android/play/headerlist/q;->f:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/play/headerlist/q;->f:I

    goto :goto_3

    .line 166
    :cond_5
    iget v2, p0, Lcom/google/android/play/headerlist/q;->f:I

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 126
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/q;->a(Landroid/widget/Adapter;)V

    .line 127
    iput p2, p0, Lcom/google/android/play/headerlist/q;->a:I

    .line 128
    iget-object v0, p0, Lcom/google/android/play/headerlist/q;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(I)V

    .line 129
    iget-object v0, p0, Lcom/google/android/play/headerlist/q;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/google/android/play/headerlist/q;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a:Landroid/widget/AbsListView$OnScrollListener;

    iget v1, p0, Lcom/google/android/play/headerlist/q;->a:I

    invoke-interface {v0, p1, v1}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 132
    :cond_0
    return-void
.end method
