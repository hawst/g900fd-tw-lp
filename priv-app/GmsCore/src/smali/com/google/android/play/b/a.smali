.class public abstract Lcom/google/android/play/b/a;
.super Landroid/graphics/drawable/Drawable;
.source "SourceFile"


# instance fields
.field protected a:Landroid/content/res/ColorStateList;

.field protected b:Landroid/graphics/Paint;

.field protected c:F

.field protected d:F


# direct methods
.method constructor <init>(Landroid/content/res/ColorStateList;FF)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 22
    iput p2, p0, Lcom/google/android/play/b/a;->c:F

    .line 23
    iput-object p1, p0, Lcom/google/android/play/b/a;->a:Landroid/content/res/ColorStateList;

    .line 24
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/play/b/a;->b:Landroid/graphics/Paint;

    .line 25
    iget-object v0, p0, Lcom/google/android/play/b/a;->b:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/play/b/a;->a:Landroid/content/res/ColorStateList;

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 26
    iput p3, p0, Lcom/google/android/play/b/a;->d:F

    .line 27
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/b/a;->a:Landroid/content/res/ColorStateList;

    .line 50
    iget-object v0, p0, Lcom/google/android/play/b/a;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 51
    invoke-virtual {p0}, Lcom/google/android/play/b/a;->invalidateSelf()V

    .line 52
    return-void
.end method

.method public final a(Landroid/content/res/ColorStateList;)V
    .locals 4

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/play/b/a;->a:Landroid/content/res/ColorStateList;

    .line 59
    iget-object v0, p0, Lcom/google/android/play/b/a;->b:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/play/b/a;->a:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/google/android/play/b/a;->getState()[I

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/b/a;->a:Landroid/content/res/ColorStateList;

    invoke-virtual {v3}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/play/b/a;->invalidateSelf()V

    .line 62
    return-void
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 66
    const/4 v0, -0x1

    return v0
.end method

.method public isStateful()Z
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/play/b/a;->a:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/b/a;->a:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onStateChange([I)Z
    .locals 3

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/play/b/a;->a:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/b/a;->a:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/google/android/play/b/a;->b:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/play/b/a;->a:Landroid/content/res/ColorStateList;

    iget-object v2, p0, Lcom/google/android/play/b/a;->a:Landroid/content/res/ColorStateList;

    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    invoke-virtual {v1, p1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 34
    invoke-virtual {p0}, Lcom/google/android/play/b/a;->invalidateSelf()V

    .line 35
    const/4 v0, 0x1

    .line 37
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onStateChange([I)Z

    move-result v0

    goto :goto_0
.end method
