.class public abstract Lcom/google/android/play/headerlist/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 685
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 686
    iput-object p1, p0, Lcom/google/android/play/headerlist/j;->b:Landroid/content/Context;

    .line 687
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
    .locals 1

    .prologue
    .line 929
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a()V
    .locals 0

    .prologue
    .line 762
    return-void
.end method

.method protected abstract a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
.end method

.method protected a(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 699
    return-void
.end method

.method protected b()F
    .locals 1

    .prologue
    .line 708
    const v0, 0x3f333333    # 0.7f

    return v0
.end method

.method protected abstract c()I
.end method

.method protected d()I
    .locals 1

    .prologue
    .line 816
    sget v0, Lcom/google/android/play/g;->R:I

    return v0
.end method

.method protected e()I
    .locals 1

    .prologue
    .line 824
    sget v0, Lcom/google/android/play/g;->O:I

    return v0
.end method

.method protected abstract f()Z
.end method

.method protected g()Z
    .locals 1

    .prologue
    .line 795
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract h()I
.end method

.method protected i()I
    .locals 1

    .prologue
    .line 730
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/j;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected j()I
    .locals 1

    .prologue
    .line 833
    sget v0, Lcom/google/android/play/g;->P:I

    return v0
.end method

.method protected k()I
    .locals 1

    .prologue
    .line 846
    const/4 v0, 0x0

    return v0
.end method

.method protected l()I
    .locals 1

    .prologue
    .line 889
    const/4 v0, 0x0

    return v0
.end method

.method protected m()Z
    .locals 1

    .prologue
    .line 718
    const/4 v0, 0x0

    return v0
.end method

.method protected n()I
    .locals 1

    .prologue
    .line 857
    const/4 v0, 0x0

    return v0
.end method

.method protected o()I
    .locals 1

    .prologue
    .line 902
    const/4 v0, 0x0

    return v0
.end method

.method protected p()I
    .locals 1

    .prologue
    .line 915
    const/4 v0, 0x0

    return v0
.end method

.method protected q()I
    .locals 1

    .prologue
    .line 779
    const/4 v0, 0x0

    return v0
.end method
