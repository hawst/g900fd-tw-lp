.class public abstract Lcom/google/android/play/utils/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:Lcom/google/android/play/utils/a/h;


# instance fields
.field private b:Ljava/lang/Object;

.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/play/utils/a/a;->a:Lcom/google/android/play/utils/a/h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/utils/a/a;->b:Ljava/lang/Object;

    .line 41
    iput-object p1, p0, Lcom/google/android/play/utils/a/a;->c:Ljava/lang/String;

    .line 42
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/android/play/utils/a/a;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/a/a;
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lcom/google/android/play/utils/a/b;

    invoke-direct {v0, p0, p0, p1}, Lcom/google/android/play/utils/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/a/a;
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lcom/google/android/play/utils/a/e;

    invoke-direct {v0, p0, p0, p1}, Lcom/google/android/play/utils/a/e;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/a/a;
    .locals 1

    .prologue
    .line 87
    new-instance v0, Lcom/google/android/play/utils/a/d;

    invoke-direct {v0, p0, p0, p1}, Lcom/google/android/play/utils/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/a/a;
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/google/android/play/utils/a/c;

    invoke-direct {v0, p0, p0, p1}, Lcom/google/android/play/utils/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/a/a;
    .locals 1

    .prologue
    .line 105
    new-instance v0, Lcom/google/android/play/utils/a/f;

    invoke-direct {v0, p0, p0, p1}, Lcom/google/android/play/utils/a/f;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/a/a;
    .locals 1

    .prologue
    .line 115
    new-instance v0, Lcom/google/android/play/utils/a/g;

    invoke-direct {v0, p0, p0, p1}, Lcom/google/android/play/utils/a/g;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/play/utils/a/a;->b:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/google/android/play/utils/a/a;->b:Ljava/lang/Object;

    .line 61
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/utils/a/a;->a()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
