.class public final Lcom/google/android/play/image/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static b:I

.field private static c:I


# instance fields
.field private final a:Lcom/android/volley/s;

.field private final d:I

.field private final e:Lcom/google/android/play/image/b;

.field private final f:Ljava/util/HashMap;

.field private final g:Ljava/util/HashMap;

.field private final h:Landroid/os/Handler;

.field private i:Ljava/lang/Runnable;

.field private j:Lcom/google/android/play/image/v;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/high16 v0, 0x300000

    sput v0, Lcom/google/android/play/image/e;->b:I

    .line 51
    const/4 v0, 0x6

    sput v0, Lcom/google/android/play/image/e;->c:I

    return-void
.end method

.method protected static a(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;Lcom/android/volley/x;Lcom/android/volley/w;)Lcom/google/android/play/image/l;
    .locals 7

    .prologue
    .line 326
    new-instance v0, Lcom/google/android/play/image/l;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/play/image/l;-><init>(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;Lcom/android/volley/x;Lcom/android/volley/w;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/play/image/e;)Lcom/google/android/play/image/v;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/play/image/e;->j:Lcom/google/android/play/image/v;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/play/image/e;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/play/image/e;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/n;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/image/e;->a(Ljava/lang/String;Lcom/google/android/play/image/n;)V

    iget-object v1, v0, Lcom/google/android/play/image/n;->a:Lcom/android/volley/p;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/google/android/play/image/n;->a:Lcom/android/volley/p;

    invoke-virtual {v0}, Lcom/android/volley/p;->d()Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v1, "Bitmap error %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/play/utils/b;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    const-string v0, "<null request>"

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/play/image/e;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/Bitmap;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 37
    invoke-virtual {p5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {p5}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v1

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/play/image/e;->d:I

    if-gt v0, v1, :cond_3

    iget-object v4, p0, Lcom/google/android/play/image/e;->e:Lcom/google/android/play/image/b;

    iget-object v0, v4, Lcom/google/android/play/image/b;->a:Lcom/google/android/play/image/d;

    invoke-virtual {v0, p2}, Lcom/google/android/play/image/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-nez v0, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v0

    :goto_0
    move v2, v3

    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/c;

    iget-object v0, v0, Lcom/google/android/play/image/c;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    if-lt v0, v5, :cond_2

    if-ne v0, v5, :cond_1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :goto_2
    new-instance v0, Lcom/google/android/play/image/c;

    invoke-direct {v0, p5, p3, p4}, Lcom/google/android/play/image/c;-><init>(Landroid/graphics/Bitmap;II)V

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v0, v4, Lcom/google/android/play/image/b;->a:Lcom/google/android/play/image/d;

    invoke-virtual {v0, p2, v1}, Lcom/google/android/play/image/d;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_3
    iget-object v0, p0, Lcom/google/android/play/image/e;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/n;

    if-eqz v0, :cond_0

    iput-object p5, v0, Lcom/google/android/play/image/n;->b:Landroid/graphics/Bitmap;

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/image/e;->a(Ljava/lang/String;Lcom/google/android/play/image/n;)V

    const-string v1, "Loaded bitmap %s"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v0, v0, Lcom/google/android/play/image/n;->a:Lcom/android/volley/p;

    invoke-virtual {v0}, Lcom/android/volley/p;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/play/utils/b;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    const-string v0, "%s is too large to cache"

    new-array v1, v6, [Ljava/lang/Object;

    aput-object p1, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/play/utils/b;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    :cond_4
    move v2, v3

    goto :goto_2

    :cond_5
    move-object v1, v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lcom/google/android/play/image/n;)V
    .locals 4

    .prologue
    .line 528
    iget-object v0, p0, Lcom/google/android/play/image/e;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 530
    iget-object v0, p0, Lcom/google/android/play/image/e;->i:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 531
    new-instance v0, Lcom/google/android/play/image/i;

    invoke-direct {v0, p0}, Lcom/google/android/play/image/i;-><init>(Lcom/google/android/play/image/e;)V

    iput-object v0, p0, Lcom/google/android/play/image/e;->i:Ljava/lang/Runnable;

    .line 549
    iget-object v0, p0, Lcom/google/android/play/image/e;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/play/image/e;->i:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 551
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/play/image/e;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/play/image/e;->f:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/play/image/e;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/play/image/e;->g:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/play/image/e;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/image/e;->i:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;IILcom/google/android/play/image/k;)Lcom/google/android/play/image/j;
    .locals 14

    .prologue
    .line 336
    if-gtz p2, :cond_0

    if-lez p3, :cond_f

    :cond_0
    invoke-static/range {p1 .. p3}, Lcom/google/android/play/image/t;->a(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v5

    :goto_0
    new-instance v1, Lcom/google/android/play/image/f;

    move-object v2, p0

    move/from16 v3, p2

    move/from16 v4, p3

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/play/image/f;-><init>(Lcom/google/android/play/image/e;IILjava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v1, Lcom/google/android/play/image/j;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    move-object v2, p0

    move/from16 v6, p2

    move/from16 v7, p3

    invoke-direct/range {v1 .. v8}, Lcom/google/android/play/image/j;-><init>(Lcom/google/android/play/image/e;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;IILcom/google/android/play/image/k;)V

    :goto_1
    return-object v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/play/image/e;->e:Lcom/google/android/play/image/b;

    iget-object v2, v2, Lcom/google/android/play/image/b;->a:Lcom/google/android/play/image/d;

    invoke-virtual {v2, p1}, Lcom/google/android/play/image/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-nez v2, :cond_3

    const/4 v3, 0x0

    :cond_2
    :goto_2
    if-eqz v3, :cond_b

    iget v2, v3, Lcom/google/android/play/image/c;->b:I

    move/from16 v0, p2

    if-ne v2, v0, :cond_b

    iget v2, v3, Lcom/google/android/play/image/c;->c:I

    move/from16 v0, p3

    if-ne v2, v0, :cond_b

    new-instance v1, Lcom/google/android/play/image/j;

    iget-object v3, v3, Lcom/google/android/play/image/c;->a:Landroid/graphics/Bitmap;

    const/4 v8, 0x0

    move-object v2, p0

    move-object v4, p1

    move/from16 v6, p2

    move/from16 v7, p3

    invoke-direct/range {v1 .. v8}, Lcom/google/android/play/image/j;-><init>(Lcom/google/android/play/image/e;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;IILcom/google/android/play/image/k;)V

    goto :goto_1

    :cond_3
    if-eqz p2, :cond_7

    const/4 v3, 0x1

    move v8, v3

    :goto_3
    if-eqz p2, :cond_8

    const/4 v3, 0x1

    move v4, v3

    :goto_4
    const/4 v6, 0x0

    const/4 v3, 0x0

    move v7, v3

    :goto_5
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v7, v3, :cond_9

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/play/image/c;

    iget v9, v3, Lcom/google/android/play/image/c;->b:I

    move/from16 v0, p2

    if-ne v9, v0, :cond_4

    iget v9, v3, Lcom/google/android/play/image/c;->c:I

    move/from16 v0, p3

    if-eq v9, v0, :cond_2

    :cond_4
    if-nez v6, :cond_e

    if-eqz v8, :cond_5

    iget-object v9, v3, Lcom/google/android/play/image/c;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    move/from16 v0, p2

    if-lt v9, v0, :cond_e

    :cond_5
    if-eqz v4, :cond_6

    iget-object v9, v3, Lcom/google/android/play/image/c;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    move/from16 v0, p3

    if-lt v9, v0, :cond_e

    :cond_6
    :goto_6
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    move-object v6, v3

    goto :goto_5

    :cond_7
    const/4 v3, 0x0

    move v8, v3

    goto :goto_3

    :cond_8
    const/4 v3, 0x0

    move v4, v3

    goto :goto_4

    :cond_9
    if-eqz v6, :cond_a

    move-object v3, v6

    goto :goto_2

    :cond_a
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/image/c;

    move-object v3, v2

    goto :goto_2

    :cond_b
    const/4 v8, 0x0

    if-eqz v3, :cond_c

    iget-object v8, v3, Lcom/google/android/play/image/c;->a:Landroid/graphics/Bitmap;

    :cond_c
    new-instance v6, Lcom/google/android/play/image/j;

    move-object v7, p0

    move-object v9, p1

    move-object v10, v5

    move/from16 v11, p2

    move/from16 v12, p3

    move-object/from16 v13, p4

    invoke-direct/range {v6 .. v13}, Lcom/google/android/play/image/j;-><init>(Lcom/google/android/play/image/e;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;IILcom/google/android/play/image/k;)V

    iget-object v2, p0, Lcom/google/android/play/image/e;->f:Ljava/util/HashMap;

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/image/n;

    if-eqz v2, :cond_d

    iget-object v1, v2, Lcom/google/android/play/image/n;->c:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v6

    goto/16 :goto_1

    :cond_d
    invoke-interface {v1}, Lcom/google/android/play/image/m;->a()Lcom/android/volley/p;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/play/image/e;->a:Lcom/android/volley/s;

    invoke-virtual {v2, v1}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    iget-object v2, p0, Lcom/google/android/play/image/e;->f:Ljava/util/HashMap;

    new-instance v3, Lcom/google/android/play/image/n;

    invoke-direct {v3, p0, v1, v6}, Lcom/google/android/play/image/n;-><init>(Lcom/google/android/play/image/e;Lcom/android/volley/p;Lcom/google/android/play/image/j;)V

    invoke-virtual {v2, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v6

    goto/16 :goto_1

    :cond_e
    move-object v3, v6

    goto :goto_6

    :cond_f
    move-object v5, p1

    goto/16 :goto_0
.end method
