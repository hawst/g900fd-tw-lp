.class public final Lcom/google/android/gms/games/g/ac;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5267
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 5268
    iput v0, p0, Lcom/google/android/gms/games/g/ac;->a:I

    iput-boolean v0, p0, Lcom/google/android/gms/games/g/ac;->b:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/ac;->cachedSize:I

    .line 5269
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 5318
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 5319
    iget v1, p0, Lcom/google/android/gms/games/g/ac;->a:I

    if-eqz v1, :cond_0

    .line 5320
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/g/ac;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5323
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ac;->b:Z

    if-eqz v1, :cond_1

    .line 5324
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ac;->b:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5327
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5280
    if-ne p1, p0, :cond_1

    .line 5293
    :cond_0
    :goto_0
    return v0

    .line 5283
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/ac;

    if-nez v2, :cond_2

    move v0, v1

    .line 5284
    goto :goto_0

    .line 5286
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/ac;

    .line 5287
    iget v2, p0, Lcom/google/android/gms/games/g/ac;->a:I

    iget v3, p1, Lcom/google/android/gms/games/g/ac;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 5288
    goto :goto_0

    .line 5290
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ac;->b:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/g/ac;->b:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 5291
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 5298
    iget v0, p0, Lcom/google/android/gms/games/g/ac;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 5300
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/g/ac;->b:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    .line 5301
    return v0

    .line 5300
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5235
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/g/ac;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/g/ac;->b:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 5307
    iget v0, p0, Lcom/google/android/gms/games/g/ac;->a:I

    if-eqz v0, :cond_0

    .line 5308
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/g/ac;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5310
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/games/g/ac;->b:Z

    if-eqz v0, :cond_1

    .line 5311
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ac;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 5313
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5314
    return-void
.end method
