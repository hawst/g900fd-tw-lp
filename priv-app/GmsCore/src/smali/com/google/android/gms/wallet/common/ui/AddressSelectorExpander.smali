.class public Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;
.super Lcom/google/android/gms/wallet/common/ui/bq;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lcom/google/android/gms/wallet/common/ui/v;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field a:Lcom/google/android/gms/wallet/common/ui/x;

.field private g:Lcom/google/checkout/inapp/proto/a/b;

.field private h:Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/bq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/x;

    invoke-direct {v0, p1, p0, p2}, Lcom/google/android/gms/wallet/common/ui/x;-><init>(Landroid/content/Context;Landroid/widget/AdapterView;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/x;

    .line 44
    invoke-super {p0, p0}, Lcom/google/android/gms/wallet/common/ui/bq;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 45
    return-void
.end method


# virtual methods
.method public final a(IZ)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 182
    instance-of v1, v0, Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_3

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 184
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-eq v1, v0, :cond_4

    .line 186
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->getSelectedView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/gms/j;->qm:I

    invoke-static {v1, v2, v4, p2}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;IZZ)Z

    .line 191
    :cond_0
    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->g:Lcom/google/checkout/inapp/proto/a/b;

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_1

    .line 195
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->getSelectedView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->qm:I

    invoke-static {v0, v1, v3, p2}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;IZZ)Z

    .line 197
    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->h(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 198
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->zX:I

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->g:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v3, v3, Lcom/google/checkout/inapp/proto/a/b;->c:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 204
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->l()V

    .line 209
    :cond_2
    :goto_1
    return-void

    .line 182
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 205
    :cond_4
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_2

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->c:Lcom/google/android/gms/wallet/common/ui/bl;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/bl;->b()V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/w;)V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/x;->a(Lcom/google/android/gms/wallet/common/ui/w;)V

    .line 116
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/a/b;)V
    .locals 6

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/x;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/wallet/common/ui/x;->a(Lcom/google/checkout/inapp/proto/a/b;Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->getSelectedView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->getSelectedItemPosition()I

    move-result v3

    const-wide/16 v4, -0x1

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/wallet/common/ui/x;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 60
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/x;->a(Z)V

    .line 89
    return-void
.end method

.method public final a([Lcom/google/checkout/inapp/proto/a/b;)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/x;->a([Lcom/google/checkout/inapp/proto/a/b;)V

    .line 79
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/x;->a()Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/x;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/x;->b(Z)V

    .line 126
    return-void
.end method

.method protected final c()V
    .locals 4

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_0

    .line 232
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->getSelectedView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->qm:I

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;IZZ)Z

    .line 234
    :cond_0
    return-void
.end method

.method protected final d()I
    .locals 4

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->getSelectedView()Landroid/view/View;

    move-result-object v1

    .line 243
    const/4 v0, 0x0

    .line 244
    if-eqz v1, :cond_1

    .line 246
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    .line 247
    sget v2, Lcom/google/android/gms/j;->ad:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->e:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->e:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->ae:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 251
    sget v2, Lcom/google/android/gms/j;->ad:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->e:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->ae:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 258
    :cond_0
    :goto_0
    return v0

    .line 254
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->e:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 256
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    neg-int v0, v0

    goto :goto_0
.end method

.method protected final e()I
    .locals 3

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    .line 265
    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->f:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 266
    add-int/lit8 v0, v0, -0x1

    .line 268
    :cond_0
    return v0
.end method

.method protected final f()I
    .locals 1

    .prologue
    .line 273
    sget v0, Lcom/google/android/gms/n;->z:I

    return v0
.end method

.method protected final g()I
    .locals 1

    .prologue
    .line 278
    sget v0, Lcom/google/android/gms/p;->Ao:I

    return v0
.end method

.method public getSelectedItem()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/z;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/z;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedItemPosition()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/z;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->g:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/z;->a(Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v0

    .line 149
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 150
    add-int/lit8 v0, v0, 0x1

    .line 156
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->f:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    .line 219
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 220
    instance-of v1, v0, Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_0

    .line 221
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/x;

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/wallet/common/ui/x;->a(Lcom/google/checkout/inapp/proto/a/b;Z)V

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->h:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz v0, :cond_1

    .line 224
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->h:Landroid/widget/AdapterView$OnItemClickListener;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 226
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/x;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/wallet/common/ui/x;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 227
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/x;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/wallet/common/ui/x;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 137
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/x;->onNothingSelected(Landroid/widget/AdapterView;)V

    .line 142
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->isEnabled()Z

    move-result v0

    if-eq p1, v0, :cond_0

    .line 100
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/bq;->setEnabled(Z)V

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/z;

    .line 102
    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/z;->notifyDataSetChanged()V

    .line 106
    :cond_0
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->h:Landroid/widget/AdapterView$OnItemClickListener;

    .line 50
    return-void
.end method

.method public setSelection(I)V
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->a(IZ)V

    .line 172
    return-void
.end method
