.class public final Lcom/google/android/gms/droidguard/d;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Lcom/google/android/gms/droidguard/h;

.field private c:Ljava/util/List;

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Z

.field private h:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 262
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 267
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/droidguard/d;->b:Lcom/google/android/gms/droidguard/h;

    .line 286
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/droidguard/d;->c:Ljava/util/List;

    .line 320
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/droidguard/d;->e:Ljava/lang/String;

    .line 337
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/droidguard/d;->g:Z

    .line 381
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/droidguard/d;->h:I

    .line 262
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 384
    iget v0, p0, Lcom/google/android/gms/droidguard/d;->h:I

    if-gez v0, :cond_0

    .line 386
    invoke-virtual {p0}, Lcom/google/android/gms/droidguard/d;->b()I

    .line 388
    :cond_0
    iget v0, p0, Lcom/google/android/gms/droidguard/d;->h:I

    return v0
.end method

.method public final a(Lcom/google/android/gms/droidguard/f;)Lcom/google/android/gms/droidguard/d;
    .locals 1

    .prologue
    .line 303
    if-nez p1, :cond_0

    .line 304
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 307
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/droidguard/d;->c:Ljava/util/List;

    .line 309
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 310
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/droidguard/h;)Lcom/google/android/gms/droidguard/d;
    .locals 1

    .prologue
    .line 271
    if-nez p1, :cond_0

    .line 272
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 274
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/droidguard/d;->a:Z

    .line 275
    iput-object p1, p0, Lcom/google/android/gms/droidguard/d;->b:Lcom/google/android/gms/droidguard/h;

    .line 276
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/droidguard/d;
    .locals 1

    .prologue
    .line 324
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/droidguard/d;->d:Z

    .line 325
    iput-object p1, p0, Lcom/google/android/gms/droidguard/d;->e:Ljava/lang/String;

    .line 326
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/droidguard/d;
    .locals 1

    .prologue
    .line 341
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/droidguard/d;->f:Z

    .line 342
    iput-boolean p1, p0, Lcom/google/android/gms/droidguard/d;->g:Z

    .line 343
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 1

    .prologue
    .line 259
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/android/gms/droidguard/h;

    invoke-direct {v0}, Lcom/google/android/gms/droidguard/h;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/droidguard/d;->a(Lcom/google/android/gms/droidguard/h;)Lcom/google/android/gms/droidguard/d;

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/android/gms/droidguard/f;

    invoke-direct {v0}, Lcom/google/android/gms/droidguard/f;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/droidguard/d;->a(Lcom/google/android/gms/droidguard/f;)Lcom/google/android/gms/droidguard/d;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/droidguard/d;->a(Ljava/lang/String;)Lcom/google/android/gms/droidguard/d;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/droidguard/d;->a(Z)Lcom/google/android/gms/droidguard/d;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x30 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 3

    .prologue
    .line 367
    iget-boolean v0, p0, Lcom/google/android/gms/droidguard/d;->a:Z

    if-eqz v0, :cond_0

    .line 368
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/droidguard/d;->b:Lcom/google/android/gms/droidguard/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 370
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/droidguard/f;

    .line 371
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    goto :goto_0

    .line 373
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/droidguard/d;->d:Z

    if-eqz v0, :cond_2

    .line 374
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/droidguard/d;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 376
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/droidguard/d;->f:Z

    if-eqz v0, :cond_3

    .line 377
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/android/gms/droidguard/d;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(IZ)V

    .line 379
    :cond_3
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 393
    const/4 v0, 0x0

    .line 394
    iget-boolean v1, p0, Lcom/google/android/gms/droidguard/d;->a:Z

    if-eqz v1, :cond_0

    .line 395
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/droidguard/d;->b:Lcom/google/android/gms/droidguard/h;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 398
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/droidguard/d;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/droidguard/f;

    .line 399
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 401
    goto :goto_0

    .line 402
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/droidguard/d;->d:Z

    if-eqz v0, :cond_2

    .line 403
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/android/gms/droidguard/d;->e:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 406
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/droidguard/d;->f:Z

    if-eqz v0, :cond_3

    .line 407
    const/4 v0, 0x6

    iget-boolean v2, p0, Lcom/google/android/gms/droidguard/d;->g:Z

    invoke-static {v0}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 410
    :cond_3
    iput v1, p0, Lcom/google/android/gms/droidguard/d;->h:I

    .line 411
    return v1
.end method
