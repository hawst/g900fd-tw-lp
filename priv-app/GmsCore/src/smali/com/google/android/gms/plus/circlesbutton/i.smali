.class public final Lcom/google/android/gms/plus/circlesbutton/i;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/res/Resources;Landroid/widget/TextView;I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 48
    new-instance v4, Lcom/google/android/gms/plus/circlesbutton/j;

    invoke-direct {v4}, Lcom/google/android/gms/plus/circlesbutton/j;-><init>()V

    packed-switch p2, :pswitch_data_0

    const/4 v0, 0x0

    .line 49
    :goto_0
    if-eqz p1, :cond_0

    if-nez v0, :cond_1

    .line 67
    :cond_0
    :goto_1
    return-void

    .line 48
    :pswitch_0
    sget v2, Lcom/google/android/gms/g;->bX:I

    sget v0, Lcom/google/android/gms/f;->aB:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v3, v2

    move v2, v0

    move v0, v1

    :goto_2
    iput v3, v4, Lcom/google/android/gms/plus/circlesbutton/j;->a:I

    iput v2, v4, Lcom/google/android/gms/plus/circlesbutton/j;->b:I

    iput v0, v4, Lcom/google/android/gms/plus/circlesbutton/j;->c:I

    move-object v0, v4

    goto :goto_0

    :pswitch_1
    sget v3, Lcom/google/android/gms/g;->bX:I

    sget v2, Lcom/google/android/gms/f;->aB:I

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto :goto_2

    :pswitch_2
    sget v3, Lcom/google/android/gms/g;->bX:I

    sget v2, Lcom/google/android/gms/f;->az:I

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto :goto_2

    :pswitch_3
    sget v2, Lcom/google/android/gms/g;->bX:I

    sget v0, Lcom/google/android/gms/f;->az:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v3, v2

    move v2, v0

    move v0, v1

    goto :goto_2

    :pswitch_4
    sget v3, Lcom/google/android/gms/g;->bX:I

    sget v2, Lcom/google/android/gms/f;->aA:I

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto :goto_2

    :pswitch_5
    sget v2, Lcom/google/android/gms/g;->bX:I

    sget v0, Lcom/google/android/gms/f;->aA:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v3, v2

    move v2, v0

    move v0, v1

    goto :goto_2

    .line 53
    :cond_1
    iget v2, v0, Lcom/google/android/gms/plus/circlesbutton/j;->a:I

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 54
    iget v1, v0, Lcom/google/android/gms/plus/circlesbutton/j;->b:I

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 56
    iget v0, v0, Lcom/google/android/gms/plus/circlesbutton/j;->c:I

    packed-switch v0, :pswitch_data_1

    .line 63
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_1

    .line 58
    :pswitch_6
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_1

    .line 48
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 56
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
    .end packed-switch
.end method
