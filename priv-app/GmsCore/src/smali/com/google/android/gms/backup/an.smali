.class final Lcom/google/android/gms/backup/an;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/backup/aj;


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;


# instance fields
.field private final b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "Backup"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "LegacyBackupAccountManager"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/backup/an;->a:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x0

    .line 44
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const-string v2, "com.google.android.backuptransport"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/app/GmsApplication;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 52
    iput-object v0, p0, Lcom/google/android/gms/backup/an;->b:Landroid/content/Context;

    .line 53
    :goto_0
    return-void

    :catch_0
    move-exception v1

    .line 47
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const-string v2, "com.google.android.backup"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/app/GmsApplication;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 52
    :goto_1
    iput-object v0, p0, Lcom/google/android/gms/backup/an;->b:Landroid/content/Context;

    goto :goto_0

    .line 48
    :catch_1
    move-exception v1

    .line 49
    :try_start_2
    sget-object v2, Lcom/google/android/gms/backup/an;->a:Lcom/google/android/gms/auth/d/a;

    const-string v3, "Fail to get legacy transport context."

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 52
    :catchall_0
    move-exception v1

    iput-object v0, p0, Lcom/google/android/gms/backup/an;->b:Landroid/content/Context;

    throw v1
.end method

.method private static b()Landroid/accounts/Account;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 118
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 120
    array-length v2, v1

    if-nez v2, :cond_1

    .line 121
    sget-object v1, Lcom/google/android/gms/backup/an;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "No google accounts found!"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->d(Ljava/lang/String;)V

    .line 126
    :cond_0
    :goto_0
    return-object v0

    .line 123
    :cond_1
    array-length v2, v1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 124
    const/4 v0, 0x0

    aget-object v0, v1, v0

    goto :goto_0
.end method

.method private c()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/gms/backup/an;->b:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 132
    const/4 v0, 0x0

    .line 134
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/backup/an;->b:Landroid/content/Context;

    const-string v1, "BackupTransport.backupAccount"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/accounts/Account;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 58
    invoke-direct {p0}, Lcom/google/android/gms/backup/an;->c()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 59
    if-nez v0, :cond_0

    .line 60
    invoke-static {}, Lcom/google/android/gms/backup/an;->b()Landroid/accounts/Account;

    move-result-object v0

    .line 67
    :goto_0
    return-object v0

    .line 62
    :cond_0
    const-string v1, "accountName"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 63
    const-string v2, "accountType"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 64
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 65
    new-instance v0, Landroid/accounts/Account;

    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 67
    :cond_1
    invoke-static {}, Lcom/google/android/gms/backup/an;->b()Landroid/accounts/Account;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 77
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    invoke-direct {p0}, Lcom/google/android/gms/backup/an;->c()Landroid/content/SharedPreferences;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "accountName"

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "accountType"

    iget-object v2, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/backup/an;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Fail to write legacy backup account shared preference."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.backup.SetBackupAccount"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 85
    const-string v1, "com.google.android.backuptransport"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    const-string v1, "backupAccount"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 87
    const/16 v1, 0x11

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 88
    const-string v1, "backupUserHandle"

    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 90
    :cond_2
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/app/GmsApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method
