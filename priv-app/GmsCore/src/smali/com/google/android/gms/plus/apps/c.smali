.class public final Lcom/google/android/gms/plus/apps/c;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;
.implements Lcom/google/android/gms/plus/apps/au;


# instance fields
.field a:Landroid/accounts/Account;

.field b:I

.field final c:Ljava/util/ArrayList;

.field d:Ljava/util/List;

.field final e:Ljava/util/HashSet;

.field private f:Ljava/lang/String;

.field private g:I

.field private final h:Ljava/util/ArrayList;

.field private i:Ljava/lang/String;

.field private j:Lcom/google/android/gms/plus/apps/as;

.field private k:Lcom/google/android/gms/plus/apps/a;

.field private final l:Ljava/util/Map;

.field private m:Lcom/google/android/gms/common/api/am;

.field private n:Ljava/lang/String;

.field private o:Lcom/google/android/gms/common/api/v;

.field private p:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 108
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/apps/c;->b:I

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/c;->c:Ljava/util/ArrayList;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/c;->h:Ljava/util/ArrayList;

    .line 115
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/c;->e:Ljava/util/HashSet;

    .line 119
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/c;->l:Ljava/util/Map;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/apps/c;
    .locals 2

    .prologue
    .line 144
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 145
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v1, "calling_package_name"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    new-instance v1, Lcom/google/android/gms/plus/apps/c;

    invoke-direct {v1}, Lcom/google/android/gms/plus/apps/c;-><init>()V

    .line 148
    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/c;->setArguments(Landroid/os/Bundle;)V

    .line 149
    return-object v1
.end method

.method static a(Lcom/google/android/gms/plus/internal/ad;Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;Ljava/lang/String;)Lcom/google/android/gms/plus/internal/ab;
    .locals 6

    .prologue
    .line 156
    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "service_googleme"

    aput-object v1, v5, v0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/plus/apps/c;->a(Lcom/google/android/gms/plus/internal/ad;Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/plus/internal/ad;Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/plus/internal/ab;
    .locals 4

    .prologue
    .line 177
    new-instance v0, Lcom/google/android/gms/plus/internal/cn;

    invoke-direct {v0, p1}, Lcom/google/android/gms/plus/internal/cn;-><init>(Landroid/content/Context;)V

    iput-object p4, v0, Lcom/google/android/gms/plus/internal/cn;->a:Ljava/lang/String;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "https://www.googleapis.com/auth/plus.applications.manage"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "https://www.googleapis.com/auth/grants.audit"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "https://www.googleapis.com/auth/plus.circles.read"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "https://www.googleapis.com/auth/plus.me"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/internal/cn;->a([Ljava/lang/String;)Lcom/google/android/gms/plus/internal/cn;

    move-result-object v0

    iput-object p5, v0, Lcom/google/android/gms/plus/internal/cn;->d:[Ljava/lang/String;

    const-string v1, "81"

    iput-object v1, v0, Lcom/google/android/gms/plus/internal/cn;->e:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/cn;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v0

    .line 186
    invoke-interface {p0, p1, v0, p2, p3}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/gms/fitness/result/BleDevicesResult;)V
    .locals 1

    .prologue
    .line 405
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/result/BleDevicesResult;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/result/BleDevicesResult;->b()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/c;->d:Ljava/util/List;

    .line 407
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/c;->f()V

    .line 411
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/c;->m:Lcom/google/android/gms/common/api/am;

    .line 412
    return-void

    .line 409
    :cond_0
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/c;->d(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/apps/c;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/c;->d()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/apps/c;Lcom/google/android/gms/fitness/result/BleDevicesResult;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/apps/c;->a(Lcom/google/android/gms/fitness/result/BleDevicesResult;)V

    return-void
.end method

.method static a(Landroid/content/Context;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 650
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/common/util/a;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/a;->a(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static b(Lcom/google/android/gms/plus/internal/ad;Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;Ljava/lang/String;)Lcom/google/android/gms/plus/internal/ab;
    .locals 6

    .prologue
    .line 168
    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/plus/apps/c;->a(Lcom/google/android/gms/plus/internal/ad;Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    return-object v0
.end method

.method static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 664
    invoke-static {p0}, Lcom/google/android/gms/plus/apps/c;->a(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v3

    .line 665
    array-length v1, v3

    if-nez v1, :cond_1

    .line 683
    :cond_0
    :goto_0
    return-object v0

    .line 669
    :cond_1
    const-string v1, "com.google.android.gms.plus.apps.AppsUtilFragment"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 670
    const-string v4, "prefs_account_name"

    invoke-interface {v1, v4, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 671
    if-nez v0, :cond_2

    .line 672
    aget-object v0, v3, v2

    goto :goto_0

    .line 676
    :cond_2
    array-length v4, v3

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 677
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 676
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 683
    :cond_3
    aget-object v0, v3, v2

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/apps/c;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/c;->h()V

    return-void
.end method

.method private static c(I)Z
    .locals 1

    .prologue
    .line 554
    packed-switch p0, :pswitch_data_0

    .line 560
    :pswitch_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 558
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 554
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private d(Ljava/lang/String;)Lcom/google/android/gms/common/api/v;
    .locals 2

    .prologue
    .line 227
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/fitness/c;->b:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    iput-object p1, v0, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/x;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/c;->p:Z

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->o:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 251
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/c;->e()V

    .line 253
    :cond_0
    return-void
.end method

.method private d(I)V
    .locals 2

    .prologue
    .line 565
    iput p1, p0, Lcom/google/android/gms/plus/apps/c;->b:I

    .line 566
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->e:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/i;

    .line 567
    invoke-interface {v0}, Lcom/google/android/gms/plus/apps/i;->V_()V

    goto :goto_0

    .line 569
    :cond_0
    return-void
.end method

.method private e(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 655
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.apps.AppsUtilFragment"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/q;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 657
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 658
    const-string v1, "prefs_account_name"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 659
    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 660
    return-object p1
.end method

.method private e()V
    .locals 1

    .prologue
    .line 320
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/g;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->o:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 323
    :cond_0
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 359
    iget v0, p0, Lcom/google/android/gms/plus/apps/c;->b:I

    packed-switch v0, :pswitch_data_0

    .line 365
    :goto_0
    :pswitch_0
    return-void

    .line 361
    :pswitch_1
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/c;->d(I)V

    goto :goto_0

    .line 364
    :pswitch_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/c;->d(I)V

    goto :goto_0

    .line 359
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private g()V
    .locals 3

    .prologue
    .line 377
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_0

    .line 378
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/c;->f()V

    .line 402
    :goto_0
    return-void

    .line 381
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->m:Lcom/google/android/gms/common/api/am;

    if-eqz v0, :cond_1

    .line 382
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->m:Lcom/google/android/gms/common/api/am;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->b()V

    .line 384
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/g;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 385
    sget-object v0, Lcom/google/android/gms/fitness/c;->h:Lcom/google/android/gms/fitness/a;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/c;->o:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/a;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/c;->m:Lcom/google/android/gms/common/api/am;

    .line 386
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->m:Lcom/google/android/gms/common/api/am;

    new-instance v1, Lcom/google/android/gms/plus/apps/e;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/apps/e;-><init>(Lcom/google/android/gms/plus/apps/c;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0

    .line 398
    :cond_2
    new-instance v0, Lcom/google/android/gms/fitness/result/BleDevicesResult;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/fitness/result/BleDevicesResult;-><init>(Ljava/util/List;Lcom/google/android/gms/common/api/Status;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/c;->a(Lcom/google/android/gms/fitness/result/BleDevicesResult;)V

    goto :goto_0
.end method

.method private h()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 535
    invoke-static {}, Lcom/google/android/gms/plus/apps/af;->a()Lcom/google/android/gms/plus/apps/af;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/apps/af;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 537
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/c;->i()V

    .line 538
    invoke-direct {p0, v1}, Lcom/google/android/gms/plus/apps/c;->d(I)V

    .line 539
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/16 v1, 0xa

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/au;->b(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 540
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/c;->g()V

    .line 542
    :cond_0
    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 572
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 573
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/c;->i:Ljava/lang/String;

    .line 574
    return-void
.end method


# virtual methods
.method public final synthetic a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 5

    .prologue
    .line 63
    const/16 v0, 0xa

    if-ne p1, v0, :cond_0

    new-instance v1, Lcom/google/android/gms/plus/apps/av;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/c;->a:Landroid/accounts/Account;

    sget-object v0, Lcom/google/android/gms/plus/c/a;->G:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v4, p0, Lcom/google/android/gms/plus/apps/c;->i:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/google/android/gms/plus/apps/av;-><init>(Landroid/content/Context;Landroid/accounts/Account;ILjava/lang/String;)V

    return-object v1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown loader ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method final a()V
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->o:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->o:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->o:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 332
    :cond_1
    return-void
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 503
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/16 v4, 0xa

    .line 63
    check-cast p2, Lcom/google/android/gms/plus/model/a/b;

    iget v0, p1, Landroid/support/v4/a/j;->m:I

    if-ne v0, v4, :cond_1

    check-cast p1, Lcom/google/android/gms/plus/apps/av;

    iget-object v0, p1, Lcom/google/android/gms/common/ui/h;->a:Lcom/google/android/gms/common/c;

    if-eqz p2, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->b()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/c;->i()V

    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/google/android/gms/plus/apps/c;->d(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/v4/app/au;->a(I)V

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->a()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getActivity()Landroid/support/v4/app/q;

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Lcom/google/android/gms/plus/model/a/b;->c()I

    move-result v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/c;->c:Ljava/util/ArrayList;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/plus/model/a/b;->b(I)Lcom/google/android/gms/plus/model/a/a;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a(Lcom/google/android/gms/plus/model/a/a;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v0, p1, Lcom/google/android/gms/plus/apps/av;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/c;->i:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->i:Ljava/lang/String;

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v1, p0}, Landroid/support/v4/app/au;->b(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    goto :goto_0

    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/c;->f()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 193
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 194
    if-nez v0, :cond_1

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/google/android/gms/plus/apps/c;->d(I)V

    .line 201
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 203
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v1

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/gms/plus/apps/d;

    invoke-direct {v3, p0}, Lcom/google/android/gms/plus/apps/d;-><init>(Lcom/google/android/gms/plus/apps/c;)V

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/common/ew;->a(ILandroid/app/Activity;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0

    .line 215
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/plus/apps/c;->p:Z

    if-nez v1, :cond_0

    .line 218
    iput-boolean v2, p0, Lcom/google/android/gms/plus/apps/c;->p:Z

    .line 220
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/common/c;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 222
    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/c;->d()V

    goto :goto_0
.end method

.method final a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 3

    .prologue
    .line 623
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/c;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/c;->f:Ljava/lang/String;

    invoke-static {v0, v1, p1, p2, v2}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 625
    return-void
.end method

.method final a(Lcom/google/android/gms/plus/apps/j;Lcom/google/android/gms/plus/model/a/a;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 609
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->j:Lcom/google/android/gms/plus/apps/as;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/gms/plus/apps/as;->a(Lcom/google/android/gms/plus/model/a/a;Ljava/lang/String;)V

    .line 610
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->l:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 611
    if-nez v0, :cond_0

    .line 612
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 613
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/c;->l:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 615
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 616
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/model/a/a;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 635
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->k:Lcom/google/android/gms/plus/apps/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/apps/a;->a(Lcom/google/android/gms/plus/model/a/a;Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/plus/apps/b;

    .line 636
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 637
    if-eqz v0, :cond_1

    .line 638
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/j;

    .line 639
    invoke-interface {v0}, Lcom/google/android/gms/plus/apps/j;->a()V

    goto :goto_0

    .line 641
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 643
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->o:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 417
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->o:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 418
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/c;->e()V

    .line 421
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/c;->n:Ljava/lang/String;

    .line 439
    :goto_0
    return-void

    .line 424
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/c;->n:Ljava/lang/String;

    .line 425
    sget-object v0, Lcom/google/android/gms/fitness/c;->h:Lcom/google/android/gms/fitness/a;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/c;->o:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/fitness/a;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/apps/g;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/apps/g;-><init>(Lcom/google/android/gms/plus/apps/c;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method final b()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method final b(I)V
    .locals 0

    .prologue
    .line 646
    iput p1, p0, Lcom/google/android/gms/plus/apps/c;->g:I

    .line 647
    return-void
.end method

.method final b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 6

    .prologue
    .line 629
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/c;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v5, p0, Lcom/google/android/gms/plus/apps/c;->f:Ljava/lang/String;

    move-object v3, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 631
    return-void
.end method

.method final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 522
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/apps/c;->e(Ljava/lang/String;)Ljava/lang/String;

    .line 523
    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p1, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/c;->a:Landroid/accounts/Account;

    .line 525
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->o:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 526
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/apps/c;->d(Ljava/lang/String;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/c;->o:Lcom/google/android/gms/common/api/v;

    .line 527
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/c;->e()V

    .line 531
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/c;->h()V

    .line 532
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->n:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/c;->a(Ljava/lang/String;)V

    .line 260
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/c;->n:Ljava/lang/String;

    .line 262
    :cond_0
    return-void
.end method

.method final c()Lcom/google/android/gms/plus/apps/a;
    .locals 1

    .prologue
    .line 619
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->k:Lcom/google/android/gms/plus/apps/a;

    return-object v0
.end method

.method final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 601
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 602
    return-void
.end method

.method public final f_(I)V
    .locals 0

    .prologue
    .line 266
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/c;->d()V

    .line 267
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 336
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 337
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 339
    invoke-static {v0}, Lcom/google/android/gms/plus/apps/as;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/as;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/apps/c;->j:Lcom/google/android/gms/plus/apps/as;

    .line 340
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/c;->j:Lcom/google/android/gms/plus/apps/as;

    invoke-virtual {v1, p0}, Lcom/google/android/gms/plus/apps/as;->a(Lcom/google/android/gms/plus/apps/au;)V

    .line 341
    invoke-static {v0}, Lcom/google/android/gms/plus/apps/a;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/c;->k:Lcom/google/android/gms/plus/apps/a;

    .line 343
    iget v0, p0, Lcom/google/android/gms/plus/apps/c;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 344
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/16 v1, 0xa

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 345
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/c;->g()V

    .line 348
    :cond_0
    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/gms/plus/apps/c;->g:I

    .line 349
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 240
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 241
    packed-switch p1, :pswitch_data_0

    .line 246
    :goto_0
    return-void

    .line 243
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/c;->d()V

    goto :goto_0

    .line 241
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 271
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 272
    if-eqz p1, :cond_0

    .line 273
    const-string v0, "is_in_resolution"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/c;->p:Z

    .line 276
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "account_name"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 277
    if-eqz v0, :cond_6

    .line 278
    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/c;->e(Ljava/lang/String;)Ljava/lang/String;

    .line 288
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/c;->d(Ljava/lang/String;)Lcom/google/android/gms/common/api/v;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/plus/apps/c;->o:Lcom/google/android/gms/common/api/v;

    .line 289
    new-instance v3, Landroid/accounts/Account;

    const-string v4, "com.google"

    invoke-direct {v3, v0, v4}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/gms/plus/apps/c;->a:Landroid/accounts/Account;

    .line 290
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "calling_package_name"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/c;->f:Ljava/lang/String;

    .line 292
    if-eqz p1, :cond_2

    .line 293
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 294
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->h:Ljava/util/ArrayList;

    const-string v3, "disconnected_apps"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 298
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->a:Landroid/accounts/Account;

    if-eqz v0, :cond_4

    if-eqz p1, :cond_3

    const-string v0, "has_error"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    invoke-static {}, Lcom/google/android/gms/plus/apps/af;->a()Lcom/google/android/gms/plus/apps/af;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/android/gms/plus/apps/af;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_4
    move v0, v2

    .line 302
    :goto_0
    if-eqz v0, :cond_5

    .line 303
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->a:Landroid/accounts/Account;

    if-nez v0, :cond_8

    const/4 v0, 0x2

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/c;->d(I)V

    .line 305
    :cond_5
    :goto_2
    return-void

    .line 280
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/c;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 281
    if-nez v0, :cond_1

    goto :goto_2

    :cond_7
    move v0, v1

    .line 298
    goto :goto_0

    .line 303
    :cond_8
    const/4 v0, 0x3

    goto :goto_1
.end method

.method public final onDetach()V
    .locals 2

    .prologue
    .line 463
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 464
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->j:Lcom/google/android/gms/plus/apps/as;

    if-eqz v0, :cond_0

    .line 465
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->j:Lcom/google/android/gms/plus/apps/as;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/apps/as;->b(Lcom/google/android/gms/plus/apps/au;)V

    .line 467
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/c;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/support/v4/app/au;->a(I)V

    .line 468
    return-void
.end method

.method public final onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 443
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 444
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->m:Lcom/google/android/gms/common/api/am;

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/c;->m:Lcom/google/android/gms/common/api/am;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->b()V

    .line 446
    iput-object v1, p0, Lcom/google/android/gms/plus/apps/c;->m:Lcom/google/android/gms/common/api/am;

    .line 447
    iput-object v1, p0, Lcom/google/android/gms/plus/apps/c;->d:Ljava/util/List;

    .line 449
    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 2

    .prologue
    .line 453
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 454
    iget v0, p0, Lcom/google/android/gms/plus/apps/c;->b:I

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/c;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/plus/apps/c;->g:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 455
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/c;->h()V

    .line 458
    :cond_0
    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/gms/plus/apps/c;->g:I

    .line 459
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 472
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 473
    const-string v0, "is_in_resolution"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/apps/c;->p:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 474
    const-string v0, "has_error"

    iget v1, p0, Lcom/google/android/gms/plus/apps/c;->b:I

    invoke-static {v1}, Lcom/google/android/gms/plus/apps/c;->c(I)Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 475
    const-string v0, "disconnected_apps"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/c;->h:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 476
    return-void
.end method

.method public final onStart()V
    .locals 0

    .prologue
    .line 309
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 310
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/c;->e()V

    .line 312
    return-void
.end method
