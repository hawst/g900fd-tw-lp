.class final Lcom/google/android/gms/wallet/common/ui/z;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/x;

.field private final b:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/x;Landroid/content/Context;ILjava/util/List;)V
    .locals 1

    .prologue
    .line 400
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/z;->a:Lcom/google/android/gms/wallet/common/ui/x;

    .line 401
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 403
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/z;->b:Landroid/view/LayoutInflater;

    .line 404
    return-void
.end method

.method private static a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 488
    instance-of v0, p0, Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_0

    .line 489
    const/4 v0, 0x0

    .line 493
    :goto_0
    return v0

    .line 490
    :cond_0
    instance-of v0, p0, Lcom/google/android/gms/wallet/common/ui/y;

    if-eqz v0, :cond_1

    .line 491
    const/4 v0, 0x1

    goto :goto_0

    .line 493
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private a(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/16 v8, 0x8

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 499
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/z;->a(I)Ljava/lang/Object;

    move-result-object v1

    .line 502
    if-eqz p3, :cond_0

    .line 503
    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/z;->a(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/wallet/common/ui/z;->a(Ljava/lang/Object;)I

    move-result v3

    if-eq v0, v3, :cond_0

    move-object p3, v2

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/z;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/x;->a(Lcom/google/android/gms/wallet/common/ui/x;)Landroid/widget/AdapterView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AdapterView;->isEnabled()Z

    move-result v3

    .line 508
    instance-of v0, v1, Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_10

    move-object v0, v1

    .line 509
    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 511
    if-eqz p3, :cond_1

    invoke-static {p3}, Lcom/google/android/gms/wallet/common/ui/z;->a(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 512
    :cond_1
    if-eqz p2, :cond_7

    .line 513
    iget-object v6, p0, Lcom/google/android/gms/wallet/common/ui/z;->b:Landroid/view/LayoutInflater;

    iget-object v7, p0, Lcom/google/android/gms/wallet/common/ui/z;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-static {v7}, Lcom/google/android/gms/wallet/common/ui/x;->b(Lcom/google/android/gms/wallet/common/ui/x;)I

    move-result v7

    invoke-virtual {v6, v7, p4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    .line 522
    :cond_2
    :goto_0
    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/z;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-static {v3, v0}, Lcom/google/android/gms/wallet/common/ui/x;->a(Lcom/google/android/gms/wallet/common/ui/x;Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v3

    if-eqz v3, :cond_8

    move v3, v4

    .line 524
    :goto_1
    iget-object v6, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    if-eqz v6, :cond_9

    iget-object v6, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    .line 526
    :goto_2
    if-eqz p2, :cond_f

    .line 527
    sget v2, Lcom/google/android/gms/j;->af:I

    invoke-virtual {p3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 530
    const-string v7, "\n"

    invoke-static {v6, v7}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 531
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 533
    sget v2, Lcom/google/android/gms/j;->nP:I

    invoke-virtual {p3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 534
    iget-object v6, v0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    .line 535
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_a

    .line 536
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 537
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 542
    :goto_3
    sget v2, Lcom/google/android/gms/j;->fC:I

    invoke-virtual {p3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 543
    iget-object v6, p0, Lcom/google/android/gms/wallet/common/ui/z;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-static {v6, v0}, Lcom/google/android/gms/wallet/common/ui/x;->a(Lcom/google/android/gms/wallet/common/ui/x;Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v6

    .line 544
    if-eqz v6, :cond_b

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->b(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v7

    if-nez v7, :cond_b

    .line 546
    :cond_3
    sget v0, Lcom/google/android/gms/p;->BX:I

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 547
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 596
    :cond_4
    :goto_4
    if-eqz p3, :cond_6

    .line 597
    if-nez p2, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/z;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/x;->g(Lcom/google/android/gms/wallet/common/ui/x;)I

    move-result v0

    if-eqz v0, :cond_5

    .line 598
    sget v0, Lcom/google/android/gms/j;->kS:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 599
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/z;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/x;->g(Lcom/google/android/gms/wallet/common/ui/x;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 600
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 603
    :cond_5
    invoke-virtual {p3, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 604
    invoke-static {p3, v3}, Lcom/google/android/gms/common/util/aw;->a(Landroid/view/View;Z)V

    .line 606
    :cond_6
    return-object p3

    .line 516
    :cond_7
    iget-object v6, p0, Lcom/google/android/gms/wallet/common/ui/z;->b:Landroid/view/LayoutInflater;

    iget-object v7, p0, Lcom/google/android/gms/wallet/common/ui/z;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-static {v7}, Lcom/google/android/gms/wallet/common/ui/x;->c(Lcom/google/android/gms/wallet/common/ui/x;)I

    move-result v7

    invoke-virtual {v6, v7, p4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    goto/16 :goto_0

    :cond_8
    move v3, v5

    .line 522
    goto/16 :goto_1

    .line 524
    :cond_9
    new-instance v6, Lcom/google/t/a/b;

    invoke-direct {v6}, Lcom/google/t/a/b;-><init>()V

    goto/16 :goto_2

    .line 539
    :cond_a
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 548
    :cond_b
    if-eqz v6, :cond_c

    iget-object v7, p0, Lcom/google/android/gms/wallet/common/ui/z;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-static {v7}, Lcom/google/android/gms/wallet/common/ui/x;->d(Lcom/google/android/gms/wallet/common/ui/x;)Z

    move-result v7

    if-eqz v7, :cond_c

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v7

    if-nez v7, :cond_c

    .line 550
    sget v0, Lcom/google/android/gms/p;->Cb:I

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 551
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    .line 552
    :cond_c
    if-nez v6, :cond_d

    iget-object v7, v0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    array-length v7, v7

    if-ne v7, v4, :cond_d

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    const/4 v4, 0x2

    invoke-static {v0, v4}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 555
    sget v0, Lcom/google/android/gms/p;->CS:I

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 556
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    .line 558
    :cond_d
    sget v0, Lcom/google/android/gms/j;->qm:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 560
    if-eqz v6, :cond_e

    if-eqz v0, :cond_e

    .line 561
    invoke-virtual {v0, v5}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 563
    :cond_e
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 566
    :cond_f
    sget v0, Lcom/google/android/gms/j;->ae:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 568
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, v6, Lcom/google/t/a/b;->s:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", "

    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/x;->b()[C

    move-result-object v8

    invoke-static {v6, v7, v2, v8}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;Ljava/lang/String;[C[C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 572
    :cond_10
    instance-of v0, v1, Lcom/google/android/gms/wallet/common/ui/y;

    if-eqz v0, :cond_4

    .line 573
    if-eqz p3, :cond_11

    invoke-static {p3}, Lcom/google/android/gms/wallet/common/ui/z;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 574
    :cond_11
    if-eqz p2, :cond_13

    .line 575
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/z;->b:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/z;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/x;->e(Lcom/google/android/gms/wallet/common/ui/x;)I

    move-result v2

    invoke-virtual {v0, v2, p4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    :cond_12
    :goto_5
    move-object v0, v1

    .line 589
    check-cast v0, Lcom/google/android/gms/wallet/common/ui/y;

    .line 590
    sget v2, Lcom/google/android/gms/j;->eg:I

    invoke-virtual {p3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 591
    iget-object v4, v0, Lcom/google/android/gms/wallet/common/ui/y;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 592
    iget v0, v0, Lcom/google/android/gms/wallet/common/ui/y;->c:I

    invoke-virtual {v2, v0, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_4

    .line 578
    :cond_13
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/z;->b:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/z;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/x;->f(Lcom/google/android/gms/wallet/common/ui/x;)I

    move-result v2

    invoke-virtual {v0, v2, p4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    .line 580
    sget v0, Lcom/google/android/gms/j;->fO:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 582
    if-eqz v0, :cond_12

    .line 583
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/z;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v4, Lcom/google/android/gms/p;->Bx:I

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_5
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/z;)Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 395
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/z;->getCount()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/z;->getCount()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/z;->a(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v4, v0, Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v4, :cond_0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private static a(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 616
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/checkout/inapp/proto/a/b;)I
    .locals 6

    .prologue
    const/4 v2, -0x1

    .line 424
    if-eqz p1, :cond_2

    .line 425
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/z;->getCount()I

    move-result v3

    :goto_0
    if-ge v1, v3, :cond_2

    .line 426
    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/z;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 427
    instance-of v4, v0, Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v4, :cond_1

    .line 428
    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 429
    iget-object v4, p1, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 430
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/z;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-static {v3, v0}, Lcom/google/android/gms/wallet/common/ui/x;->a(Lcom/google/android/gms/wallet/common/ui/x;Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 438
    :goto_1
    return v0

    :cond_0
    move v0, v2

    .line 433
    goto :goto_1

    .line 425
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 438
    goto :goto_1
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 414
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/z;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/aa;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/x;->a(Lcom/google/android/gms/wallet/common/ui/aa;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 456
    if-nez p1, :cond_0

    .line 457
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/z;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 458
    instance-of v1, v0, Lcom/google/android/gms/wallet/common/ui/y;

    if-eqz v1, :cond_0

    .line 459
    check-cast v0, Lcom/google/android/gms/wallet/common/ui/y;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/common/ui/y;->b:Z

    if-nez v0, :cond_0

    .line 460
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/z;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 464
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/gms/wallet/common/ui/z;->a(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 448
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/z;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/aa;

    .line 449
    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/google/android/gms/wallet/common/ui/aa;->b:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 469
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/gms/wallet/common/ui/z;->a(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 443
    const/4 v0, 0x1

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 475
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/z;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 476
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/z;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/x;->a(Lcom/google/android/gms/wallet/common/ui/x;)Landroid/widget/AdapterView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/AdapterView;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 483
    :goto_0
    return v0

    .line 478
    :cond_0
    instance-of v2, v0, Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v2, :cond_1

    .line 479
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/z;->a:Lcom/google/android/gms/wallet/common/ui/x;

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/common/ui/x;->a(Lcom/google/android/gms/wallet/common/ui/x;Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    goto :goto_0

    .line 480
    :cond_1
    instance-of v2, v0, Lcom/google/android/gms/wallet/common/ui/y;

    if-eqz v2, :cond_2

    .line 481
    check-cast v0, Lcom/google/android/gms/wallet/common/ui/y;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/common/ui/y;->b:Z

    goto :goto_0

    :cond_2
    move v0, v1

    .line 483
    goto :goto_0
.end method
