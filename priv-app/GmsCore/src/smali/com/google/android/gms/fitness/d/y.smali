.class final Lcom/google/android/gms/fitness/d/y;
.super Lcom/google/android/gms/fitness/d/b;
.source "SourceFile"


# static fields
.field private static final g:Lcom/google/android/gms/fitness/d/f;


# instance fields
.field private final h:Ljava/lang/String;

.field private final i:Lcom/google/android/gms/fitness/d/a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 14
    new-instance v0, Lcom/google/android/gms/fitness/d/f;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/gms/fitness/d/f;-><init>(Ljava/lang/String;Landroid/content/Intent;Z)V

    sput-object v0, Lcom/google/android/gms/fitness/d/y;->g:Lcom/google/android/gms/fitness/d/f;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/fitness/d/a;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/d/b;-><init>(Landroid/content/Context;)V

    .line 22
    iput-object p2, p0, Lcom/google/android/gms/fitness/d/y;->h:Ljava/lang/String;

    .line 23
    iput-object p3, p0, Lcom/google/android/gms/fitness/d/y;->i:Lcom/google/android/gms/fitness/d/a;

    .line 24
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/fitness/d/f;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 28
    iget-object v0, p0, Lcom/google/android/gms/fitness/d/y;->h:Ljava/lang/String;

    const-string v2, "none"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    sget-object v0, Lcom/google/android/gms/fitness/d/y;->g:Lcom/google/android/gms/fitness/d/f;

    .line 49
    :goto_0
    return-object v0

    .line 32
    :cond_0
    sget-object v0, Lcom/google/android/gms/fitness/g/c;->F:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 33
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 34
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 35
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 36
    sget-object v0, Lcom/google/android/gms/fitness/d/y;->g:Lcom/google/android/gms/fitness/d/f;

    goto :goto_0

    .line 34
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 41
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/fitness/d/y;->i:Lcom/google/android/gms/fitness/d/a;

    iget-object v2, p0, Lcom/google/android/gms/fitness/d/y;->h:Ljava/lang/String;

    invoke-virtual {v0, v2, p1}, Lcom/google/android/gms/fitness/d/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v2

    .line 42
    array-length v3, p2

    move v0, v1

    :goto_2
    if-ge v0, v3, :cond_5

    aget-object v1, p2, v0

    .line 43
    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {v1}, Lcom/google/android/gms/fitness/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 45
    :cond_3
    sget-object v0, Lcom/google/android/gms/fitness/d/y;->g:Lcom/google/android/gms/fitness/d/f;

    goto :goto_0

    .line 42
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 49
    :cond_5
    new-instance v0, Lcom/google/android/gms/fitness/d/f;

    const/4 v1, 0x1

    invoke-direct {v0, v5, v5, v1}, Lcom/google/android/gms/fitness/d/f;-><init>(Ljava/lang/String;Landroid/content/Intent;Z)V

    goto :goto_0
.end method
