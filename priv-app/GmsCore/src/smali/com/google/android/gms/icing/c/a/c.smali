.class public final Lcom/google/android/gms/icing/c/a/c;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:Lcom/google/af/a/d/a/a/c;

.field public c:[Lcom/google/android/gms/icing/c/a/b;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 299
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 300
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/c/a/c;->a:J

    iput-object v2, p0, Lcom/google/android/gms/icing/c/a/c;->b:Lcom/google/af/a/d/a/a/c;

    invoke-static {}, Lcom/google/android/gms/icing/c/a/b;->a()[Lcom/google/android/gms/icing/c/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/c;->c:[Lcom/google/android/gms/icing/c/a/b;

    iput-object v2, p0, Lcom/google/android/gms/icing/c/a/c;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/c;->cachedSize:I

    .line 301
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 7

    .prologue
    .line 375
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 376
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/c;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 377
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/c;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 380
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/c;->b:Lcom/google/af/a/d/a/a/c;

    if-eqz v1, :cond_1

    .line 381
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/c;->b:Lcom/google/af/a/d/a/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 384
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/c;->c:[Lcom/google/android/gms/icing/c/a/b;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/c;->c:[Lcom/google/android/gms/icing/c/a/b;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 385
    const/4 v1, 0x0

    move v6, v1

    move v1, v0

    move v0, v6

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/c;->c:[Lcom/google/android/gms/icing/c/a/b;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 386
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/c;->c:[Lcom/google/android/gms/icing/c/a/b;

    aget-object v2, v2, v0

    .line 387
    if-eqz v2, :cond_2

    .line 388
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 385
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 393
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 314
    if-ne p1, p0, :cond_1

    .line 315
    const/4 v0, 0x1

    .line 337
    :cond_0
    :goto_0
    return v0

    .line 317
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/c;

    if-eqz v1, :cond_0

    .line 320
    check-cast p1, Lcom/google/android/gms/icing/c/a/c;

    .line 321
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/c;->a:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/c/a/c;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 324
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/c;->b:Lcom/google/af/a/d/a/a/c;

    if-nez v1, :cond_3

    .line 325
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/c;->b:Lcom/google/af/a/d/a/a/c;

    if-nez v1, :cond_0

    .line 333
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/c;->c:[Lcom/google/android/gms/icing/c/a/b;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/c;->c:[Lcom/google/android/gms/icing/c/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 337
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/c;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 329
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/c;->b:Lcom/google/af/a/d/a/a/c;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/c;->b:Lcom/google/af/a/d/a/a/c;

    invoke-virtual {v1, v2}, Lcom/google/af/a/d/a/a/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    .line 342
    iget-wide v0, p0, Lcom/google/android/gms/icing/c/a/c;->a:J

    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/c;->a:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 345
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/c;->b:Lcom/google/af/a/d/a/a/c;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 347
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/c;->c:[Lcom/google/android/gms/icing/c/a/b;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 349
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/c;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 350
    return v0

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/c;->b:Lcom/google/af/a/d/a/a/c;

    invoke-virtual {v0}, Lcom/google/af/a/d/a/a/c;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 273
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/c;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/icing/c/a/c;->a:J

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/c;->b:Lcom/google/af/a/d/a/a/c;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/af/a/d/a/a/c;

    invoke-direct {v0}, Lcom/google/af/a/d/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/c;->b:Lcom/google/af/a/d/a/a/c;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/c;->b:Lcom/google/af/a/d/a/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/c;->c:[Lcom/google/android/gms/icing/c/a/b;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/c/a/b;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/c;->c:[Lcom/google/android/gms/icing/c/a/b;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/android/gms/icing/c/a/b;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/b;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/c;->c:[Lcom/google/android/gms/icing/c/a/b;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/android/gms/icing/c/a/b;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/b;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/c/a/c;->c:[Lcom/google/android/gms/icing/c/a/b;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 356
    iget-wide v0, p0, Lcom/google/android/gms/icing/c/a/c;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 357
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/c;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 359
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/c;->b:Lcom/google/af/a/d/a/a/c;

    if-eqz v0, :cond_1

    .line 360
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/c;->b:Lcom/google/af/a/d/a/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 362
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/c;->c:[Lcom/google/android/gms/icing/c/a/b;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/c;->c:[Lcom/google/android/gms/icing/c/a/b;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 363
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/c;->c:[Lcom/google/android/gms/icing/c/a/b;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 364
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/c;->c:[Lcom/google/android/gms/icing/c/a/b;

    aget-object v1, v1, v0

    .line 365
    if-eqz v1, :cond_2

    .line 366
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 363
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 370
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 371
    return-void
.end method
