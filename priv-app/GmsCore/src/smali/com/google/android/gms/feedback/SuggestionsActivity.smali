.class public Lcom/google/android/gms/feedback/SuggestionsActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/feedback/ErrorReport;

.field private b:Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;

.field private c:Lcom/google/android/gms/googlehelp/common/k;

.field private d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->e:Z

    return-void
.end method

.method private static a(Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 289
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {p0, v0, v1}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    .line 292
    :goto_0
    return-object v0

    :cond_0
    new-array v0, v1, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/feedback/SuggestionsActivity;)Lcom/google/android/gms/googlehelp/common/HelpConfig;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/feedback/SuggestionsActivity;Lcom/google/android/gms/googlehelp/common/k;)Lcom/google/android/gms/googlehelp/common/k;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->c:Lcom/google/android/gms/googlehelp/common/k;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/feedback/SuggestionsActivity;Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->a(Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/feedback/SuggestionsActivity;ZZ)V
    .locals 4

    .prologue
    .line 38
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-boolean v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->F:Z

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-static {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Landroid/support/v7/app/d;Lcom/google/android/gms/feedback/ErrorReport;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v2, v0, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/feedback/a/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, v1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget v3, v1, Landroid/app/ApplicationErrorReport;->type:I

    sget-object v1, Lcom/google/android/gms/feedback/a/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v2, v0, v3, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-static {p0, v0}, Lcom/google/android/gms/feedback/FeedbackAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/feedback/ErrorReport;)V

    if-eqz p2, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->f()V

    goto :goto_1
.end method

.method private a(Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 316
    iput-boolean v1, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->e:Z

    .line 317
    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    move-result-object v0

    .line 318
    invoke-virtual {v0, p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->onAttach(Landroid/app/Activity;)V

    .line 319
    invoke-virtual {p1, v0}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->a(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V

    .line 320
    invoke-virtual {p1, v1}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->a(Z)V

    .line 321
    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->c:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "http"

    iget-object v1, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->c:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/k;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const-string v6, "https://www.google.com"

    .line 324
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->c:Lcom/google/android/gms/googlehelp/common/k;

    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v4, v0, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    move-object v0, p1

    move-object v2, p2

    move v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->a(Lcom/google/android/gms/googlehelp/common/k;Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)V

    .line 330
    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->b:Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->hy:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 333
    new-instance v1, Lcom/google/android/gms/feedback/ar;

    invoke-direct {v1, p0, p0}, Lcom/google/android/gms/feedback/ar;-><init>(Lcom/google/android/gms/feedback/SuggestionsActivity;Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 343
    if-nez p3, :cond_1

    .line 344
    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->b:Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->hy:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    new-instance v1, Lcom/google/android/gms/feedback/aq;

    invoke-direct {v1, p0}, Lcom/google/android/gms/feedback/aq;-><init>(Lcom/google/android/gms/feedback/SuggestionsActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 347
    :cond_1
    return-void

    .line 321
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->c:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->b()Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 201
    if-eqz p1, :cond_0

    .line 202
    sget v0, Lcom/google/android/gms/j;->hw:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 203
    sget v0, Lcom/google/android/gms/j;->hv:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 208
    :goto_0
    return-void

    .line 205
    :cond_0
    sget v0, Lcom/google/android/gms/j;->hw:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 206
    sget v0, Lcom/google/android/gms/j;->hv:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/feedback/SuggestionsActivity;)Lcom/google/android/gms/feedback/ErrorReport;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->a:Lcom/google/android/gms/feedback/ErrorReport;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/feedback/SuggestionsActivity;)Lcom/google/android/gms/googlehelp/common/k;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->c:Lcom/google/android/gms/googlehelp/common/k;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/feedback/SuggestionsActivity;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->a(Z)V

    return-void
.end method

.method private e()Landroid/os/AsyncTask;
    .locals 4

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 125
    const-string v1, "com.google.android.gms.feedback.SUGGESTIONS_REQUEST_EXTRA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 127
    :try_start_0
    const-string v1, "com.google.android.gms.feedback.SUGGESTIONS_REQUEST_EXTRA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;

    iput-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 128
    new-instance v0, Lcom/google/android/gms/feedback/ao;

    invoke-direct {v0, p0, p0}, Lcom/google/android/gms/feedback/ao;-><init>(Lcom/google/android/gms/feedback/SuggestionsActivity;Lcom/google/android/gms/feedback/SuggestionsActivity;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :goto_0
    return-object v0

    .line 159
    :catch_0
    move-exception v0

    .line 160
    const-string v1, "GFEEDBACK_SuggestionsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception while creating suggestionTask: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/gms/feedback/SuggestionsActivity;)Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->b:Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 242
    iget-boolean v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->e:Z

    if-eqz v0, :cond_0

    .line 248
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->c:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "https://www.google.com"

    .line 250
    :goto_0
    new-instance v1, Lcom/google/android/gms/googlehelp/common/p;

    invoke-direct {v1}, Lcom/google/android/gms/googlehelp/common/p;-><init>()V

    const-string v2, "FEEDBACK_SUGGESTION_CLOSED"

    iput-object v2, v1, Lcom/google/android/gms/googlehelp/common/p;->a:Ljava/lang/String;

    const/4 v2, 0x0

    iput v2, v1, Lcom/google/android/gms/googlehelp/common/p;->b:I

    iget-object v2, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v2, v2, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/googlehelp/common/p;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/common/p;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/p;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/p;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Lcom/google/android/gms/googlehelp/common/o;

    move-result-object v0

    .line 257
    new-instance v1, Lcom/google/android/gms/feedback/ap;

    invoke-direct {v1, p0, p0, v0}, Lcom/google/android/gms/feedback/ap;-><init>(Lcom/google/android/gms/feedback/SuggestionsActivity;Landroid/app/Activity;Lcom/google/android/gms/googlehelp/common/o;)V

    .line 265
    invoke-static {v1}, Lcom/google/android/gms/feedback/SuggestionsActivity;->a(Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 270
    :cond_0
    :goto_1
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->setResult(I)V

    .line 271
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->finish()V

    .line 272
    return-void

    .line 248
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->c:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->n()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0

    .line 266
    :catch_0
    move-exception v0

    .line 267
    const-string v1, "GFEEDBACK_SuggestionsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception in metric reporting: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public dismiss(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 224
    invoke-direct {p0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->f()V

    .line 225
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 56
    invoke-static {}, Lcom/google/android/gms/feedback/FeedbackActivity;->e()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->a:Lcom/google/android/gms/feedback/ErrorReport;

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->a:Lcom/google/android/gms/feedback/ErrorReport;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-boolean v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->F:Z

    if-eqz v0, :cond_1

    .line 58
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->f()V

    .line 92
    :goto_0
    return-void

    .line 61
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->Z:Lcom/google/android/gms/feedback/ThemeSettings;

    if-eqz v0, :cond_2

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->Z:Lcom/google/android/gms/feedback/ThemeSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/ThemeSettings;->a()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    .line 65
    :goto_1
    if-eqz v0, :cond_5

    sget v0, Lcom/google/android/gms/q;->a:I

    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->setTheme(I)V

    .line 70
    :cond_2
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->Z:Lcom/google/android/gms/feedback/ThemeSettings;

    if-eqz v0, :cond_3

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->Z:Lcom/google/android/gms/feedback/ThemeSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/ThemeSettings;->b()I

    move-result v0

    .line 74
    if-eqz v0, :cond_3

    .line 76
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v1

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v3}, Landroid/support/v7/app/a;->b(Landroid/graphics/drawable/Drawable;)V

    .line 80
    :cond_3
    sget v0, Lcom/google/android/gms/l;->ca:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->setContentView(I)V

    .line 82
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/gms/p;->od:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, ""

    const/4 v4, 0x0

    invoke-static {v0, v1, v3, v4}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Landroid/support/v7/app/a;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    .line 85
    invoke-direct {p0, v2}, Lcom/google/android/gms/feedback/SuggestionsActivity;->a(Z)V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->hj:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;

    iput-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->b:Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/feedback/SuggestionsActivity;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v4, Lcom/google/android/gms/p;->od:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-static {v3, v4, v0, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Landroid/support/v7/app/a;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    .line 91
    invoke-direct {p0, v2}, Lcom/google/android/gms/feedback/SuggestionsActivity;->a(Z)V

    invoke-direct {p0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->e()Landroid/os/AsyncTask;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->a(Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 62
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 65
    :cond_5
    sget v0, Lcom/google/android/gms/q;->b:I

    goto/16 :goto_2
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 279
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 280
    invoke-direct {p0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->f()V

    .line 281
    const/4 v0, 0x1

    .line 283
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/d;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 212
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 216
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 214
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->f()V

    goto :goto_0

    .line 212
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public readMore(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 232
    sget v0, Lcom/google/android/gms/j;->hv:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 233
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->hk:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;

    .line 235
    const-string v1, "FEEDBACK_SUGGESTION_CLICKED"

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/feedback/SuggestionsActivity;->a(Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;Ljava/lang/String;Z)V

    .line 236
    return-void
.end method
