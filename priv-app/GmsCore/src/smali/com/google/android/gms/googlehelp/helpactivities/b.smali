.class public final Lcom/google/android/gms/googlehelp/helpactivities/b;
.super Landroid/support/v4/app/m;
.source "SourceFile"


# static fields
.field public static final j:Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/gms/googlehelp/helpactivities/c;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/helpactivities/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/googlehelp/helpactivities/b;->j:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 42
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    .line 43
    iput v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/b;->k:I

    .line 44
    iput v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/b;->l:I

    .line 45
    iput v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/b;->m:I

    .line 46
    sget-object v0, Lcom/google/android/gms/googlehelp/helpactivities/b;->j:Landroid/content/DialogInterface$OnClickListener;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/b;->o:Landroid/content/DialogInterface$OnClickListener;

    .line 47
    iput v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/b;->n:I

    .line 48
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/googlehelp/helpactivities/b;
    .locals 0

    .prologue
    .line 51
    iput p1, p0, Lcom/google/android/gms/googlehelp/helpactivities/b;->k:I

    .line 52
    return-object p0
.end method

.method public final a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/gms/googlehelp/helpactivities/b;
    .locals 0

    .prologue
    .line 61
    iput p1, p0, Lcom/google/android/gms/googlehelp/helpactivities/b;->m:I

    .line 62
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/helpactivities/b;->o:Landroid/content/DialogInterface$OnClickListener;

    .line 63
    return-object p0
.end method

.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 73
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/b;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 75
    iget v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/b;->k:I

    if-eq v1, v3, :cond_0

    .line 76
    iget v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/b;->k:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 78
    :cond_0
    iget v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/b;->l:I

    if-eq v1, v3, :cond_1

    .line 79
    iget v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/b;->l:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 81
    :cond_1
    iget v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/b;->m:I

    if-eq v1, v3, :cond_2

    .line 82
    iget v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/b;->m:I

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/b;->o:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 84
    :cond_2
    iget v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/b;->n:I

    if-eq v1, v3, :cond_3

    .line 85
    iget v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/b;->n:I

    sget-object v2, Lcom/google/android/gms/googlehelp/helpactivities/b;->j:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 87
    :cond_3
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Lcom/google/android/gms/googlehelp/helpactivities/b;
    .locals 0

    .prologue
    .line 56
    iput p1, p0, Lcom/google/android/gms/googlehelp/helpactivities/b;->l:I

    .line 57
    return-object p0
.end method

.method public final c(I)Lcom/google/android/gms/googlehelp/helpactivities/b;
    .locals 0

    .prologue
    .line 67
    iput p1, p0, Lcom/google/android/gms/googlehelp/helpactivities/b;->n:I

    .line 68
    return-object p0
.end method
