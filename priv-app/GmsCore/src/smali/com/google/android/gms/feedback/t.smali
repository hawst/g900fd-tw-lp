.class public final Lcom/google/android/gms/feedback/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private a:Lcom/google/android/gms/feedback/ErrorReport;

.field private b:[Ljava/lang/String;

.field private c:Lcom/google/android/gms/feedback/Screenshot;

.field private d:Lcom/google/android/gms/feedback/FeedbackActivity;

.field private e:Landroid/os/Handler;

.field private f:Z

.field private g:Lcom/google/android/gms/feedback/e;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/feedback/FeedbackActivity;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 98
    new-instance v0, Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/ErrorReport;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/feedback/t;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;Lcom/google/android/gms/feedback/ErrorReport;)V

    .line 99
    iget-object v1, p0, Lcom/google/android/gms/feedback/t;->g:Lcom/google/android/gms/feedback/e;

    const-string v0, "feedback.REPORT"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/feedback/ErrorReport;

    iput-object v0, v1, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v0, v1, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/ErrorReport;-><init>()V

    iput-object v0, v1, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    :cond_0
    new-instance v0, Lcom/google/android/gms/feedback/f;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/feedback/f;-><init>(Lcom/google/android/gms/feedback/e;B)V

    iput-object v0, v1, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    iget-object v0, v1, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    const-string v2, "feedback.RUNNING_APPS"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/feedback/f;->c:[Ljava/lang/String;

    iget-object v2, v1, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    const-string v0, "feedback.SCREENSHOT_KEY"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/feedback/Screenshot;

    iput-object v0, v2, Lcom/google/android/gms/feedback/f;->e:Lcom/google/android/gms/feedback/Screenshot;

    iget-object v0, v1, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    const-string v2, "feedback.PSD_BUNDLE_KEY"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/feedback/f;->f:Landroid/os/Bundle;

    iget-object v0, v1, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    const-string v1, "feedback.PSBD_FILE_PATH_KEY"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/feedback/f;->g:[Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->g:Lcom/google/android/gms/feedback/e;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/e;->a()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/t;->a:Lcom/google/android/gms/feedback/ErrorReport;

    const-string v0, "feedback.FOUND_ACCOUNTS"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/t;->b:[Ljava/lang/String;

    .line 100
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/feedback/FeedbackActivity;Lcom/google/android/gms/feedback/ErrorReport;)V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/feedback/t;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/Screenshot;)V

    .line 82
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/feedback/FeedbackActivity;Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/Screenshot;)V
    .locals 2

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/google/android/gms/feedback/t;->d:Lcom/google/android/gms/feedback/FeedbackActivity;

    .line 87
    new-instance v0, Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v0, p2}, Lcom/google/android/gms/feedback/ErrorReport;-><init>(Lcom/google/android/gms/feedback/ErrorReport;)V

    iput-object v0, p0, Lcom/google/android/gms/feedback/t;->a:Lcom/google/android/gms/feedback/ErrorReport;

    .line 88
    new-instance v0, Lcom/google/android/gms/feedback/e;

    iget-object v1, p0, Lcom/google/android/gms/feedback/t;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v0, p1, v1, p3}, Lcom/google/android/gms/feedback/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/Screenshot;)V

    iput-object v0, p0, Lcom/google/android/gms/feedback/t;->g:Lcom/google/android/gms/feedback/e;

    .line 90
    iput-object p3, p0, Lcom/google/android/gms/feedback/t;->c:Lcom/google/android/gms/feedback/Screenshot;

    .line 91
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/feedback/t;->e:Landroid/os/Handler;

    .line 92
    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 172
    invoke-static {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Lcom/google/android/gms/feedback/t;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    :goto_0
    return-void

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->d:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Z)V

    goto :goto_0
.end method

.method private f()[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->d:Lcom/google/android/gms/feedback/FeedbackActivity;

    sget v2, Lcom/google/android/gms/p;->ok:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 131
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 132
    const/16 v0, 0x12

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->d:Lcom/google/android/gms/feedback/FeedbackActivity;

    const-string v4, "user"

    invoke-virtual {v0, v4}, Lcom/google/android/gms/feedback/FeedbackActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->getUserRestrictions()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "no_modify_accounts"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :goto_0
    if-nez v0, :cond_1

    .line 134
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->d:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v4, "com.google"

    invoke-virtual {v0, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v4

    .line 136
    array-length v5, v4

    move v0, v1

    :goto_1
    if-ge v0, v5, :cond_1

    aget-object v6, v4, v0

    .line 137
    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/VerifyError; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 132
    goto :goto_0

    :catch_0
    move-exception v0

    .line 145
    :cond_1
    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    .line 146
    aput-object v2, v4, v1

    .line 148
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 149
    add-int/lit8 v1, v1, 0x1

    aput-object v0, v4, v1

    goto :goto_3

    .line 151
    :cond_2
    return-object v4

    .line 143
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method private g()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 183
    invoke-static {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Lcom/google/android/gms/feedback/t;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 193
    :goto_0
    return-void

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->d:Lcom/google/android/gms/feedback/FeedbackActivity;

    sget v1, Lcom/google/android/gms/j;->hm:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 188
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->d:Lcom/google/android/gms/feedback/FeedbackActivity;

    sget v2, Lcom/google/android/gms/j;->hl:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 191
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 192
    iget-object v2, p0, Lcom/google/android/gms/feedback/t;->g:Lcom/google/android/gms/feedback/e;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v1, v0}, Lcom/google/android/gms/feedback/e;->a(Ljava/lang/String;Z)Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/t;->a:Lcom/google/android/gms/feedback/ErrorReport;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 218
    invoke-direct {p0}, Lcom/google/android/gms/feedback/t;->g()V

    .line 219
    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->g:Lcom/google/android/gms/feedback/e;

    const-string v1, "feedback.REPORT"

    iget-object v2, v0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "feedback.RUNNING_APPS"

    iget-object v2, v0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    iget-object v2, v2, Lcom/google/android/gms/feedback/f;->c:[Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    const-string v1, "feedback.SCREENSHOT_KEY"

    iget-object v2, v0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    iget-object v2, v2, Lcom/google/android/gms/feedback/f;->e:Lcom/google/android/gms/feedback/Screenshot;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "feedback.PSD_BUNDLE_KEY"

    iget-object v2, v0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    iget-object v2, v2, Lcom/google/android/gms/feedback/f;->f:Landroid/os/Bundle;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v1, "feedback.PSBD_FILE_PATH_KEY"

    iget-object v0, v0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    iget-object v0, v0, Lcom/google/android/gms/feedback/f;->g:[Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 220
    const-string v0, "feedback.FOUND_ACCOUNTS"

    iget-object v1, p0, Lcom/google/android/gms/feedback/t;->b:[Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 221
    return-void
.end method

.method public final a(Lcom/google/android/gms/feedback/Screenshot;)V
    .locals 3

    .prologue
    .line 196
    iput-object p1, p0, Lcom/google/android/gms/feedback/t;->c:Lcom/google/android/gms/feedback/Screenshot;

    .line 197
    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->g:Lcom/google/android/gms/feedback/e;

    iget-object v1, v0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    iput-object p1, v1, Lcom/google/android/gms/feedback/f;->e:Lcom/google/android/gms/feedback/Screenshot;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/e;->b()V

    iget-object v0, v0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    .line 198
    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->d:Lcom/google/android/gms/feedback/FeedbackActivity;

    iget-object v1, p0, Lcom/google/android/gms/feedback/t;->c:Lcom/google/android/gms/feedback/Screenshot;

    iget-object v2, p0, Lcom/google/android/gms/feedback/t;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Lcom/google/android/gms/feedback/Screenshot;Lcom/google/android/gms/feedback/ErrorReport;)V

    .line 199
    return-void
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->g:Lcom/google/android/gms/feedback/e;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/feedback/e;->a([Ljava/lang/String;[Ljava/lang/String;)V

    .line 261
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/t;->f:Z

    .line 262
    invoke-direct {p0}, Lcom/google/android/gms/feedback/t;->g()V

    .line 263
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/t;->a(Z)V

    .line 264
    return-void
.end method

.method public final a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->b:[Ljava/lang/String;

    return-object v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 210
    invoke-direct {p0}, Lcom/google/android/gms/feedback/t;->g()V

    .line 211
    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->d:Lcom/google/android/gms/feedback/FeedbackActivity;

    iget-object v1, p0, Lcom/google/android/gms/feedback/t;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Lcom/google/android/gms/feedback/ErrorReport;)V

    .line 212
    return-void
.end method

.method public final c()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 237
    invoke-direct {p0}, Lcom/google/android/gms/feedback/t;->f()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/t;->b:[Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Lcom/google/android/gms/feedback/t;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->d:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->d:Lcom/google/android/gms/feedback/FeedbackActivity;

    sget v1, Lcom/google/android/gms/j;->hm:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/feedback/t;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, v1, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->d:Lcom/google/android/gms/feedback/FeedbackActivity;

    sget v1, Lcom/google/android/gms/j;->hl:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/google/android/gms/feedback/t;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-boolean v1, v1, Lcom/google/android/gms/feedback/ErrorReport;->X:Z

    if-nez v1, :cond_3

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->d:Lcom/google/android/gms/feedback/FeedbackActivity;

    iget-object v1, p0, Lcom/google/android/gms/feedback/t;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, p0, Lcom/google/android/gms/feedback/t;->c:Lcom/google/android/gms/feedback/Screenshot;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Lcom/google/android/gms/feedback/Screenshot;)V

    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->U:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->d:Lcom/google/android/gms/feedback/FeedbackActivity;

    iget-object v1, p0, Lcom/google/android/gms/feedback/t;->c:Lcom/google/android/gms/feedback/Screenshot;

    iget-object v5, p0, Lcom/google/android/gms/feedback/t;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-virtual {v0, v1, v5}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Lcom/google/android/gms/feedback/Screenshot;Lcom/google/android/gms/feedback/ErrorReport;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->d:Lcom/google/android/gms/feedback/FeedbackActivity;

    iget-object v1, p0, Lcom/google/android/gms/feedback/t;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, v1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->d:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->ak_()Landroid/support/v7/app/a;

    move-result-object v5

    sget v0, Lcom/google/android/gms/p;->od:I

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-static {v5, v4, v0, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Landroid/support/v7/app/a;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    iget-boolean v0, p0, Lcom/google/android/gms/feedback/t;->f:Z

    if-nez v0, :cond_4

    :goto_1
    invoke-direct {p0, v2}, Lcom/google/android/gms/feedback/t;->a(Z)V

    .line 238
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/feedback/t;->f:Z

    if-nez v0, :cond_2

    .line 239
    invoke-static {}, Lcom/google/android/gms/feedback/at;->a()V

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->e:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/feedback/u;

    invoke-direct {v1, p0}, Lcom/google/android/gms/feedback/u;-><init>(Lcom/google/android/gms/feedback/t;)V

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 248
    :cond_2
    return-void

    :cond_3
    move v1, v3

    .line 237
    goto :goto_0

    :cond_4
    move v2, v3

    goto :goto_1
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 251
    invoke-direct {p0}, Lcom/google/android/gms/feedback/t;->g()V

    .line 252
    invoke-static {}, Lcom/google/android/gms/feedback/at;->b()V

    .line 253
    return-void
.end method

.method public final e()Lcom/google/android/gms/feedback/ErrorReport;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->g:Lcom/google/android/gms/feedback/e;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/e;->a()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    return-object v0
.end method

.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .prologue
    .line 203
    invoke-direct {p0}, Lcom/google/android/gms/feedback/t;->g()V

    .line 204
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 272
    if-nez p2, :cond_0

    .line 273
    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->g:Lcom/google/android/gms/feedback/e;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/gms/feedback/e;->a(Ljava/lang/String;)Lcom/google/android/gms/feedback/ErrorReport;

    .line 274
    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->g:Lcom/google/android/gms/feedback/e;

    iget-object v1, p0, Lcom/google/android/gms/feedback/t;->g:Lcom/google/android/gms/feedback/e;

    invoke-virtual {v1}, Lcom/google/android/gms/feedback/e;->a()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/feedback/e;->a(Ljava/lang/String;Z)Lcom/google/android/gms/feedback/ErrorReport;

    .line 278
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 279
    return-void

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/t;->g:Lcom/google/android/gms/feedback/e;

    iget-object v1, p0, Lcom/google/android/gms/feedback/t;->b:[Ljava/lang/String;

    aget-object v1, v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/feedback/e;->a(Ljava/lang/String;)Lcom/google/android/gms/feedback/ErrorReport;

    goto :goto_0
.end method
