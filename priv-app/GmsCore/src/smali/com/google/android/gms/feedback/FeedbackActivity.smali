.class public Lcom/google/android/gms/feedback/FeedbackActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/MenuItem$OnMenuItemClickListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lcom/google/android/gms/feedback/ax;


# static fields
.field private static a:Lcom/google/android/gms/feedback/t;

.field private static b:Landroid/graphics/Bitmap;


# instance fields
.field private c:Landroid/graphics/Bitmap;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 86
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 130
    iput-boolean v0, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->e:Z

    .line 131
    iput-boolean v0, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->f:Z

    .line 451
    return-void
.end method

.method static synthetic a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 86
    sput-object p0, Lcom/google/android/gms/feedback/FeedbackActivity;->b:Landroid/graphics/Bitmap;

    return-object p0
.end method

.method static synthetic a(Lcom/google/android/gms/feedback/FeedbackActivity;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->c:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/feedback/FeedbackActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->c:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method private static a(Landroid/text/Spannable;)Landroid/text/Spannable;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 495
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v1, Landroid/text/style/URLSpan;

    invoke-interface {p0, v2, v0, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 496
    array-length v3, v0

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 497
    invoke-interface {p0, v4}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    .line 498
    invoke-interface {p0, v4}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    .line 499
    invoke-interface {p0, v4}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 500
    new-instance v7, Lcom/google/android/gms/auth/common/ux/URLSpanNoUnderline;

    invoke-virtual {v4}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v7, v4}, Lcom/google/android/gms/auth/common/ux/URLSpanNoUnderline;-><init>(Ljava/lang/String;)V

    .line 501
    invoke-interface {p0, v7, v5, v6, v2}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 496
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 503
    :cond_0
    return-object p0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 862
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 863
    const-string v0, ""

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 885
    :goto_0
    return-object v0

    .line 868
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 870
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v3, p1, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    .line 871
    const/4 v0, 0x0

    invoke-virtual {v3, p1, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 872
    iget-object v2, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 873
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 877
    :goto_1
    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 878
    :try_start_1
    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 882
    :goto_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 883
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/gms/p;->oj:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 885
    :cond_1
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    :goto_3
    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 875
    :cond_2
    :try_start_2
    iget-object v2, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 880
    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_4
    const-string v2, "unknown"

    goto :goto_2

    .line 885
    :cond_3
    invoke-virtual {v2, v5}, Ljava/lang/String;->codePointAt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isDigit(I)Z

    move-result v3

    if-eqz v3, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " v"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 880
    :catch_1
    move-exception v2

    goto :goto_4
.end method

.method private a(Landroid/content/Intent;)Lcom/google/android/gms/feedback/ErrorReport;
    .locals 2

    .prologue
    .line 193
    new-instance v1, Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v1}, Lcom/google/android/gms/feedback/ErrorReport;-><init>()V

    .line 194
    const-string v0, "android.intent.extra.BUG_REPORT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    const-string v0, "android.intent.extra.BUG_REPORT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/ApplicationErrorReport;

    iput-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    .line 197
    :cond_0
    const-string v0, "com.android.feedback.SAFEPARCELABLE_REPORT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 198
    const-string v0, "com.android.feedback.SAFEPARCELABLE_REPORT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/feedback/ErrorReport;

    .line 199
    iget-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 200
    iget-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->d:Ljava/lang/String;

    .line 202
    iget-boolean v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->F:Z

    if-nez v1, :cond_1

    .line 203
    const-string v1, ""

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    .line 207
    :cond_1
    :goto_0
    iget-boolean v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->F:Z

    iput-boolean v1, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->e:Z

    .line 208
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Landroid/os/Parcelable;Lcom/google/android/gms/feedback/ErrorReport;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 233
    const/4 v0, 0x0

    .line 235
    invoke-static {p2}, Lcom/google/android/gms/feedback/FeedbackActivity;->b(Lcom/google/android/gms/feedback/ErrorReport;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 237
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p2, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 240
    if-eqz p1, :cond_1

    .line 241
    check-cast p1, Lcom/google/android/gms/feedback/Screenshot;

    move-object v0, p1

    .line 253
    :cond_0
    :goto_0
    iget-object v2, p2, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    iput-object v1, v2, Landroid/app/ApplicationErrorReport;->processName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    .line 263
    :goto_1
    if-nez v1, :cond_4

    new-instance v0, Lcom/google/android/gms/feedback/t;

    invoke-direct {v0, p0, p2}, Lcom/google/android/gms/feedback/t;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;Lcom/google/android/gms/feedback/ErrorReport;)V

    :goto_2
    sput-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->a:Lcom/google/android/gms/feedback/t;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/t;->c()V

    .line 264
    :goto_3
    return-void

    .line 242
    :cond_1
    :try_start_1
    iget-object v2, p2, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    if-eqz v2, :cond_2

    .line 243
    iget-object v0, p2, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    iget v2, p2, Lcom/google/android/gms/feedback/ErrorReport;->y:I

    iget v3, p2, Lcom/google/android/gms/feedback/ErrorReport;->x:I

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/feedback/Screenshot;->a([BII)Lcom/google/android/gms/feedback/Screenshot;

    move-result-object v0

    goto :goto_0

    .line 245
    :cond_2
    iget-object v2, p2, Lcom/google/android/gms/feedback/ErrorReport;->U:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 246
    new-instance v0, Lcom/google/android/gms/feedback/Screenshot;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/Screenshot;-><init>()V

    .line 247
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getCacheDir()Ljava/io/File;

    move-result-object v2

    iget-object v3, p2, Lcom/google/android/gms/feedback/ErrorReport;->U:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v2, v3, v0, v4}, Lcom/google/android/gms/feedback/Screenshot;->a(Ljava/io/File;Ljava/lang/String;Lcom/google/android/gms/feedback/Screenshot;Z)Landroid/os/AsyncTask;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 254
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 259
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->finish()V

    goto :goto_3

    .line 249
    :cond_3
    :try_start_2
    iget-object v2, p2, Lcom/google/android/gms/feedback/ErrorReport;->v:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 250
    iget-object v0, p2, Lcom/google/android/gms/feedback/ErrorReport;->v:Ljava/lang/String;

    iget v2, p2, Lcom/google/android/gms/feedback/ErrorReport;->y:I

    iget v3, p2, Lcom/google/android/gms/feedback/ErrorReport;->x:I

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/feedback/Screenshot;->a(Ljava/lang/String;II)Lcom/google/android/gms/feedback/Screenshot;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    goto :goto_0

    .line 263
    :cond_4
    new-instance v0, Lcom/google/android/gms/feedback/t;

    invoke-direct {v0, p0, p2, v1}, Lcom/google/android/gms/feedback/t;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/Screenshot;)V

    goto :goto_2

    :cond_5
    move-object v1, v0

    goto :goto_1
.end method

.method public static a(Landroid/support/v7/app/a;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 847
    invoke-virtual {p0, p1}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    .line 848
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/app/a;->a(Z)V

    .line 850
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 851
    invoke-virtual {p0, p2}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    .line 853
    :cond_0
    invoke-virtual {p0, p3}, Landroid/support/v7/app/a;->a(Landroid/graphics/drawable/Drawable;)V

    .line 854
    return-void
.end method

.method public static a(Landroid/support/v7/app/d;Lcom/google/android/gms/feedback/ErrorReport;)V
    .locals 3

    .prologue
    .line 696
    invoke-static {p1}, Lcom/google/android/gms/feedback/a;->a(Lcom/google/android/gms/feedback/ErrorReport;)Lcom/google/android/gms/feedback/a;

    move-result-object v0

    .line 697
    invoke-virtual {p0}, Landroid/support/v7/app/d;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "ctlDialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/m;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 698
    return-void
.end method

.method public static a(Lcom/google/android/gms/feedback/t;)Z
    .locals 1

    .prologue
    .line 623
    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->a:Lcom/google/android/gms/feedback/t;

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 713
    const-string v2, ","

    invoke-static {p1, v2}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 715
    array-length v4, v3

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 716
    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    move v2, v1

    .line 721
    :goto_1
    if-nez v2, :cond_2

    .line 732
    :cond_0
    :goto_2
    return v0

    .line 715
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 725
    :cond_2
    const-string v2, ","

    invoke-static {p3, v2}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 726
    array-length v4, v3

    move v2, v0

    :goto_3
    if-ge v2, v4, :cond_0

    aget-object v5, v3, v2

    .line 727
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    if-ne v5, p2, :cond_3

    move v0, v1

    .line 729
    goto :goto_2

    .line 726
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    move v2, v0

    goto :goto_1
.end method

.method private static b(Lcom/google/android/gms/feedback/ErrorReport;)Z
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 664
    if-nez p0, :cond_1

    .line 667
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget v1, v1, Landroid/app/ApplicationErrorReport;->type:I

    const/16 v2, 0xb

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static c(Lcom/google/android/gms/feedback/ErrorReport;)Z
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 761
    sget-object v0, Lcom/google/android/gms/feedback/a/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->F:Z

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 770
    :goto_0
    return v0

    .line 764
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v3, v0, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/feedback/a/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget v4, v1, Landroid/app/ApplicationErrorReport;->type:I

    sget-object v1, Lcom/google/android/gms/feedback/a/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v3, v0, v4, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 768
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 770
    goto :goto_0
.end method

.method public static e()Lcom/google/android/gms/feedback/ErrorReport;
    .locals 1

    .prologue
    .line 605
    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->a:Lcom/google/android/gms/feedback/t;

    if-eqz v0, :cond_0

    .line 606
    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->a:Lcom/google/android/gms/feedback/t;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/t;->e()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    .line 608
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f()Lcom/google/android/gms/feedback/t;
    .locals 1

    .prologue
    .line 615
    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->a:Lcom/google/android/gms/feedback/t;

    return-object v0
.end method

.method public static g()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 619
    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->b:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic h()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->b:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private i()V
    .locals 13

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 381
    sget v0, Lcom/google/android/gms/j;->hf:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 382
    sget-object v1, Lcom/google/android/gms/feedback/FeedbackActivity;->a:Lcom/google/android/gms/feedback/t;

    invoke-virtual {v1}, Lcom/google/android/gms/feedback/t;->a()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    new-array v7, v1, [Ljava/lang/String;

    .line 386
    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->d:Ljava/lang/String;

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "anonymous"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    move v4, v3

    .line 392
    :goto_0
    sget-object v5, Lcom/google/android/gms/feedback/FeedbackActivity;->a:Lcom/google/android/gms/feedback/t;

    invoke-virtual {v5}, Lcom/google/android/gms/feedback/t;->a()[Ljava/lang/String;

    move-result-object v8

    array-length v9, v8

    move v5, v2

    :goto_1
    if-ge v5, v9, :cond_2

    aget-object v6, v8, v5

    .line 393
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    sget v12, Lcom/google/android/gms/p;->nK:I

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v7, v2

    .line 395
    if-nez v4, :cond_1

    .line 396
    const-string v10, "google.com"

    invoke-virtual {v6, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    move v1, v2

    .line 400
    :cond_0
    iget-object v10, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->d:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->d:Ljava/lang/String;

    invoke-static {v6, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v1, v2

    move v4, v3

    .line 406
    :cond_1
    add-int/lit8 v6, v2, 0x1

    .line 392
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v2, v6

    goto :goto_1

    .line 408
    :cond_2
    new-instance v2, Landroid/widget/ArrayAdapter;

    sget v4, Lcom/google/android/gms/l;->bZ:I

    invoke-direct {v2, p0, v4, v7}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 410
    const v4, 0x1090009

    invoke-virtual {v2, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 411
    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 412
    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 413
    array-length v2, v7

    if-le v2, v3, :cond_3

    .line 414
    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 416
    :cond_3
    return-void

    :cond_4
    move v1, v3

    move v4, v2

    goto :goto_0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 789
    sget v0, Lcom/google/android/gms/j;->hm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 790
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 791
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/gms/feedback/ErrorReport;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 687
    sget-object v0, Lcom/google/android/gms/feedback/a/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->F:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/feedback/FeedbackActivity;->c(Lcom/google/android/gms/feedback/ErrorReport;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 688
    invoke-static {p0, p1}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Landroid/support/v7/app/d;Lcom/google/android/gms/feedback/ErrorReport;)V

    .line 692
    :goto_1
    return-void

    .line 687
    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v3, v0, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/feedback/a/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget v4, v1, Landroid/app/ApplicationErrorReport;->type:I

    sget-object v1, Lcom/google/android/gms/feedback/a/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v3, v0, v4, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 690
    :cond_2
    invoke-static {p0}, Lcom/google/android/gms/common/util/ak;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/feedback/ac;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/feedback/ac;-><init>(Landroid/content/Context;Lcom/google/android/gms/feedback/ErrorReport;)V

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/ac;->start()V

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->finish()V

    goto :goto_1

    :cond_3
    invoke-static {p1}, Lcom/google/android/gms/feedback/FeedbackActivity;->c(Lcom/google/android/gms/feedback/ErrorReport;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/feedback/SuggestionsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.gms.feedback.SUGGESTIONS_REQUEST_EXTRA"

    new-instance v3, Lcom/google/android/gms/googlehelp/GoogleHelp;

    const-string v4, "gms:feedback:suggestions"

    invoke-direct {v3, v4}, Lcom/google/android/gms/googlehelp/GoogleHelp;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    new-instance v4, Landroid/accounts/Account;

    iget-object v5, p1, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    const-string v6, "com.google"

    invoke-direct {v4, v5, v6}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/accounts/Account;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    :cond_4
    invoke-static {v3}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a(Lcom/google/android/gms/googlehelp/GoogleHelp;)Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v4, v4, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    :cond_5
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {p0, p1}, Lcom/google/android/gms/feedback/FeedbackAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/feedback/ErrorReport;)V

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->finish()V

    goto :goto_1

    :cond_6
    new-instance v0, Lcom/google/android/gms/feedback/ac;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/feedback/ac;-><init>(Landroid/content/Context;Lcom/google/android/gms/feedback/ErrorReport;)V

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/ac;->start()V

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->finish()V

    goto/16 :goto_1
.end method

.method public final a(Lcom/google/android/gms/feedback/Screenshot;)V
    .locals 11

    .prologue
    const/16 v10, 0x21

    const/4 v7, 0x4

    const/16 v4, 0x8

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 324
    sget v0, Lcom/google/android/gms/j;->hs:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 325
    sget v1, Lcom/google/android/gms/j;->hr:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 327
    sget v2, Lcom/google/android/gms/j;->hp:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    .line 328
    sget v3, Lcom/google/android/gms/j;->ht:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    .line 329
    sget v3, Lcom/google/android/gms/j;->hx:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    .line 330
    sget v3, Lcom/google/android/gms/j;->hl:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 332
    if-nez p1, :cond_2

    .line 334
    sget v1, Lcom/google/android/gms/p;->nM:I

    invoke-virtual {v3, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 336
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 337
    sget v0, Lcom/google/android/gms/j;->hx:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 338
    sget v0, Lcom/google/android/gms/j;->ht:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 349
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    sget v0, Lcom/google/android/gms/j;->hn:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<a href="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v4, Lcom/google/android/gms/p;->nY:I

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v2, v5, v8

    invoke-virtual {v1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ">"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/google/android/gms/p;->nZ:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "</a>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "<a href="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v5, Lcom/google/android/gms/p;->oi:I

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-virtual {v1, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ">"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v4, Lcom/google/android/gms/p;->oh:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "</a>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget v4, Lcom/google/android/gms/p;->oa:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/gms/p;->mQ:I

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v6, Lcom/google/android/gms/p;->nU:I

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v4, v7, v8

    aput-object v5, v7, v9

    const/4 v5, 0x2

    aput-object v3, v7, v5

    const/4 v3, 0x3

    aput-object v2, v7, v3

    invoke-virtual {v1, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setLinksClickable(Z)V

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    check-cast v1, Landroid/text/Spannable;

    invoke-static {v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Landroid/text/Spannable;)Landroid/text/Spannable;

    move-result-object v1

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v2, Lcom/google/android/gms/feedback/l;

    invoke-direct {v2, p0, p0}, Lcom/google/android/gms/feedback/l;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;Landroid/app/Activity;)V

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/google/android/gms/feedback/m;

    invoke-direct {v5, p0, v2}, Lcom/google/android/gms/feedback/m;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;Lcom/google/android/gms/feedback/n;)V

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v2

    const/4 v4, -0x1

    if-eq v2, v4, :cond_1

    instance-of v4, v1, Landroid/text/Spannable;

    if-eqz v4, :cond_3

    check-cast v1, Landroid/text/Spannable;

    invoke-interface {v1, v5, v2, v3, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    :goto_1
    invoke-virtual {v0}, Landroid/widget/TextView;->getMovementMethod()Landroid/text/method/MovementMethod;

    move-result-object v1

    if-eqz v1, :cond_0

    instance-of v1, v1, Landroid/text/method/LinkMovementMethod;

    if-nez v1, :cond_1

    :cond_0
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 350
    :cond_1
    return-void

    .line 340
    :cond_2
    sget v4, Lcom/google/android/gms/g;->bL:I

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setMaxHeight(I)V

    .line 341
    sget-object v4, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 342
    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 343
    sget v0, Lcom/google/android/gms/p;->nL:I

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setText(I)V

    .line 345
    invoke-virtual {v2, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 346
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 347
    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto/16 :goto_0

    .line 349
    :cond_3
    invoke-static {v1}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v1, v5, v2, v3, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/feedback/Screenshot;Lcom/google/android/gms/feedback/ErrorReport;)V
    .locals 10

    .prologue
    .line 267
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/feedback/Screenshot;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 270
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    const/16 v1, 0xd

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    iget v4, v1, Landroid/graphics/Point;->x:I

    .line 271
    :goto_1
    sget v0, Lcom/google/android/gms/j;->hs:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 272
    sget v0, Lcom/google/android/gms/j;->hr:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 274
    sget v0, Lcom/google/android/gms/j;->hp:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ProgressBar;

    .line 275
    sget v0, Lcom/google/android/gms/j;->ht:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 276
    sget v0, Lcom/google/android/gms/j;->hx:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 278
    new-instance v0, Lcom/google/android/gms/feedback/k;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/feedback/k;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/Screenshot;ILandroid/widget/ProgressBar;Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/view/View;Landroid/widget/TextView;)V

    .line 316
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 270
    :cond_2
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v4

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 812
    sget v0, Lcom/google/android/gms/j;->ho:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 813
    sget v0, Lcom/google/android/gms/j;->hi:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 814
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 815
    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 816
    return-void

    :cond_0
    move v0, v2

    .line 814
    goto :goto_0

    :cond_1
    move v2, v1

    .line 815
    goto :goto_1
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 598
    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->a:Lcom/google/android/gms/feedback/t;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/feedback/t;->a([Ljava/lang/String;[Ljava/lang/String;)V

    .line 599
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 805
    invoke-static {}, Lcom/google/android/gms/feedback/FeedbackActivity;->e()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->b(Lcom/google/android/gms/feedback/ErrorReport;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 806
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->supportInvalidateOptionsMenu()V

    .line 808
    :cond_0
    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->a:Lcom/google/android/gms/feedback/t;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/t;->e()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    .line 809
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 796
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 918
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/d;->onActivityResult(IILandroid/content/Intent;)V

    .line 919
    packed-switch p1, :pswitch_data_0

    .line 927
    :cond_0
    :goto_0
    return-void

    .line 921
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 922
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->finish()V

    goto :goto_0

    .line 919
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 673
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->hs:I

    if-ne v0, v1, :cond_0

    .line 674
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/feedback/PreviewScreenshotActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->startActivity(Landroid/content/Intent;)V

    .line 676
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 820
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 821
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->supportInvalidateOptionsMenu()V

    .line 822
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 824
    sget v1, Lcom/google/android/gms/j;->hm:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 825
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 826
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v2, 0xf

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    .line 830
    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 836
    :cond_0
    :goto_0
    return-void

    .line 832
    :cond_1
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 834
    invoke-virtual {v0, v1, v4}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 137
    invoke-direct {p0, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Landroid/content/Intent;)Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v2

    .line 138
    iget-object v0, v2, Lcom/google/android/gms/feedback/ErrorReport;->Z:Lcom/google/android/gms/feedback/ThemeSettings;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, v2, Lcom/google/android/gms/feedback/ErrorReport;->Z:Lcom/google/android/gms/feedback/ThemeSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/ThemeSettings;->a()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->f:Z

    .line 142
    iget-boolean v0, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->f:Z

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/gms/q;->a:I

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->setTheme(I)V

    .line 147
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 149
    invoke-static {p0}, Lcom/google/android/gms/feedback/at;->a(Lcom/google/android/gms/feedback/ax;)V

    .line 151
    const-string v0, "com.android.feedback.SCREENSHOT_EXTRA"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 154
    iget-boolean v1, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->e:Z

    if-eqz v1, :cond_3

    .line 155
    const-string v0, "FeedbackActivity"

    const-string v1, "Invalid request for feedback invocation. This action is not permitted"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->finish()V

    .line 164
    :goto_2
    return-void

    .line 139
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 142
    :cond_2
    sget v0, Lcom/google/android/gms/q;->b:I

    goto :goto_1

    .line 160
    :cond_3
    sget v1, Lcom/google/android/gms/l;->au:I

    iget-object v3, v2, Lcom/google/android/gms/feedback/ErrorReport;->Z:Lcom/google/android/gms/feedback/ThemeSettings;

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/google/android/gms/feedback/ThemeSettings;->b()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v4

    new-instance v5, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v5, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v4, v5}, Landroid/support/v7/app/a;->b(Landroid/graphics/drawable/Drawable;)V

    :cond_4
    invoke-virtual {p0, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->setContentView(I)V

    .line 161
    if-eqz p1, :cond_5

    sget-object v1, Lcom/google/android/gms/feedback/FeedbackActivity;->a:Lcom/google/android/gms/feedback/t;

    if-nez v1, :cond_6

    :cond_5
    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Landroid/os/Parcelable;Lcom/google/android/gms/feedback/ErrorReport;)V

    .line 162
    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->j()V

    .line 163
    invoke-direct {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->i()V

    goto :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 628
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    .line 629
    iget-boolean v0, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->f:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/m;->z:I

    .line 632
    :goto_0
    invoke-virtual {v1, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 633
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    .line 629
    :cond_0
    sget v0, Lcom/google/android/gms/m;->y:I

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/gms/feedback/at;->a(Lcom/google/android/gms/feedback/ax;)V

    .line 189
    invoke-super {p0}, Landroid/support/v7/app/d;->onDestroy()V

    .line 190
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 419
    if-nez p3, :cond_0

    .line 421
    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->a:Lcom/google/android/gms/feedback/t;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/t;->e()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    const-string v1, ""

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    .line 422
    sget v0, Lcom/google/android/gms/j;->hl:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 423
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 428
    :goto_0
    return-void

    .line 425
    :cond_0
    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->a:Lcom/google/android/gms/feedback/t;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/t;->a()[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p3

    .line 426
    sget-object v1, Lcom/google/android/gms/feedback/FeedbackActivity;->a:Lcom/google/android/gms/feedback/t;

    invoke-virtual {v1}, Lcom/google/android/gms/feedback/t;->e()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v1

    iput-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 584
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 585
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->finish()V

    .line 586
    const/4 v0, 0x1

    .line 588
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/d;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 436
    invoke-static {}, Lcom/google/android/gms/feedback/FeedbackActivity;->e()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->b(Lcom/google/android/gms/feedback/ErrorReport;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 438
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 439
    sget v1, Lcom/google/android/gms/p;->nW:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->nN:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 441
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 442
    const/4 v0, 0x1

    .line 444
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 217
    sput-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->b:Landroid/graphics/Bitmap;

    .line 218
    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->c:Landroid/graphics/Bitmap;

    .line 219
    invoke-direct {p0, p1}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Landroid/content/Intent;)Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    .line 220
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onNewIntent(Landroid/content/Intent;)V

    .line 221
    const-string v1, "com.android.feedback.SCREENSHOT_EXTRA"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Landroid/os/Parcelable;Lcom/google/android/gms/feedback/ErrorReport;)V

    .line 222
    iget-boolean v0, p0, Lcom/google/android/gms/feedback/FeedbackActivity;->e:Z

    if-nez v0, :cond_0

    .line 223
    invoke-direct {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->i()V

    .line 224
    invoke-direct {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->j()V

    .line 226
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 432
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 650
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 651
    sget v2, Lcom/google/android/gms/j;->hg:I

    if-ne v1, v2, :cond_0

    .line 652
    sget-object v1, Lcom/google/android/gms/feedback/FeedbackActivity;->a:Lcom/google/android/gms/feedback/t;

    invoke-virtual {v1}, Lcom/google/android/gms/feedback/t;->b()V

    .line 658
    :goto_0
    return v0

    .line 654
    :cond_0
    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    .line 655
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->finish()V

    goto :goto_0

    .line 658
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 638
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 639
    invoke-static {}, Lcom/google/android/gms/feedback/FeedbackActivity;->e()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->b(Lcom/google/android/gms/feedback/ErrorReport;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 640
    sget v0, Lcom/google/android/gms/j;->hm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 641
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 642
    invoke-interface {v1, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 645
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 574
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 575
    sget v0, Lcom/google/android/gms/j;->hm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 576
    new-instance v0, Lcom/google/android/gms/feedback/t;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/feedback/t;-><init>(Lcom/google/android/gms/feedback/FeedbackActivity;Landroid/os/Bundle;)V

    sput-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->a:Lcom/google/android/gms/feedback/t;

    .line 577
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 552
    invoke-super {p0}, Landroid/support/v7/app/d;->onResume()V

    .line 553
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/FeedbackActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 554
    sget v0, Lcom/google/android/gms/j;->hm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 555
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocusFromTouch()Z

    .line 556
    iget v2, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 557
    iget v1, v1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v1, v1, 0xf

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    .line 567
    :goto_0
    return-void

    .line 564
    :cond_0
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/feedback/FeedbackActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 566
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 546
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 547
    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->a:Lcom/google/android/gms/feedback/t;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/feedback/t;->a(Landroid/os/Bundle;)V

    .line 548
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 531
    invoke-super {p0}, Landroid/support/v7/app/d;->onStart()V

    .line 532
    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->a:Lcom/google/android/gms/feedback/t;

    if-eqz v0, :cond_0

    .line 533
    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->a:Lcom/google/android/gms/feedback/t;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/t;->c()V

    .line 535
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 525
    invoke-super {p0}, Landroid/support/v7/app/d;->onStop()V

    .line 526
    sget-object v0, Lcom/google/android/gms/feedback/FeedbackActivity;->a:Lcom/google/android/gms/feedback/t;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/t;->d()V

    .line 527
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 801
    return-void
.end method
