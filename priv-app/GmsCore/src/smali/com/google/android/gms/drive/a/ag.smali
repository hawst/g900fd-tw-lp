.class public final Lcom/google/android/gms/drive/a/ag;
.super Lcom/google/android/gms/drive/a/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/a/w;


# instance fields
.field final d:Ljava/util/Set;

.field private final e:Lcom/google/android/gms/drive/a/ac;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/util/Set;)V
    .locals 6

    .prologue
    .line 106
    sget-object v5, Lcom/google/android/gms/drive/a/ac;->a:Lcom/google/android/gms/drive/a/ac;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/a/ag;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/util/Set;Lcom/google/android/gms/drive/a/ac;)V

    .line 107
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/util/Set;Lcom/google/android/gms/drive/a/ac;)V
    .locals 6

    .prologue
    .line 89
    sget-object v1, Lcom/google/android/gms/drive/a/e;->n:Lcom/google/android/gms/drive/a/e;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/a/b;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/a/ac;)V

    .line 91
    iput-object p4, p0, Lcom/google/android/gms/drive/a/ag;->d:Ljava/util/Set;

    .line 92
    iput-object p5, p0, Lcom/google/android/gms/drive/a/ag;->e:Lcom/google/android/gms/drive/a/ac;

    .line 93
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V
    .locals 4

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/gms/drive/a/e;->n:Lcom/google/android/gms/drive/a/e;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/drive/a/b;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    .line 60
    const-string v0, "parentIds"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "parentIds"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 62
    :goto_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/drive/a/ag;->d:Ljava/util/Set;

    .line 63
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 64
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 65
    iget-object v2, p0, Lcom/google/android/gms/drive/a/ag;->d:Ljava/util/Set;

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/drive/DriveId;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 64
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 60
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 69
    :cond_1
    const-string v0, "enforcementMode"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/a/ac;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/a/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/ag;->e:Lcom/google/android/gms/drive/a/ac;

    .line 71
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;B)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/a/ag;-><init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/a/c;
    .locals 10

    .prologue
    const/4 v5, 0x1

    .line 111
    iget-object v1, p3, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    .line 112
    iget-object v2, p3, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    .line 113
    invoke-interface {p1}, Lcom/google/android/gms/drive/database/r;->b()V

    .line 115
    :try_start_0
    invoke-interface {p1, p3, p2}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v7

    .line 117
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 118
    invoke-interface {v7}, Lcom/google/android/gms/drive/database/b/d;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ah;

    .line 119
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 145
    :catchall_0
    move-exception v0

    invoke-interface {p1}, Lcom/google/android/gms/drive/database/r;->c()V

    throw v0

    .line 121
    :cond_0
    const/4 v6, 0x0

    .line 123
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/a/ag;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    .line 124
    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 125
    iget-object v6, v1, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    .line 127
    invoke-interface {p1, p2, v0}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/am;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/am;->i()V

    move v0, v5

    :goto_2
    move v6, v0

    .line 130
    goto :goto_1

    .line 132
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v3

    .line 133
    invoke-interface {v7}, Lcom/google/android/gms/drive/database/b/d;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ah;

    .line 134
    iget-object v8, p0, Lcom/google/android/gms/drive/a/ag;->d:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 135
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    invoke-interface {p1, v3, v0}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/database/model/EntrySpec;)V

    move v0, v5

    :goto_4
    move v6, v0

    .line 138
    goto :goto_3

    .line 139
    :cond_2
    invoke-interface {p1}, Lcom/google/android/gms/drive/database/r;->e()V

    .line 140
    if-eqz v6, :cond_3

    .line 141
    new-instance v0, Lcom/google/android/gms/drive/a/ag;

    sget-object v5, Lcom/google/android/gms/drive/a/ac;->b:Lcom/google/android/gms/drive/a/ac;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/a/ag;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/util/Set;Lcom/google/android/gms/drive/a/ac;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145
    invoke-interface {p1}, Lcom/google/android/gms/drive/database/r;->c()V

    .line 147
    :goto_5
    return-object v0

    .line 145
    :cond_3
    invoke-interface {p1}, Lcom/google/android/gms/drive/database/r;->c()V

    .line 147
    new-instance v0, Lcom/google/android/gms/drive/a/z;

    sget-object v3, Lcom/google/android/gms/drive/a/ac;->b:Lcom/google/android/gms/drive/a/ac;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/a/z;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/a/ac;)V

    goto :goto_5

    :cond_4
    move v0, v6

    goto :goto_4

    :cond_5
    move v0, v6

    goto :goto_2
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/drive/g/aw;)V
    .locals 4

    .prologue
    .line 169
    invoke-virtual {p3}, Lcom/google/android/gms/drive/g/aw;->o()Lcom/google/android/gms/drive/d/f;

    move-result-object v1

    .line 170
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/drive/a/ag;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    .line 172
    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 174
    :cond_0
    invoke-interface {v1, p1, p2, v2}, Lcom/google/android/gms/drive/d/f;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/Set;)V

    .line 175
    return-void
.end method

.method protected final c(Lcom/google/android/gms/drive/g/aw;)V
    .locals 3

    .prologue
    .line 154
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/b;->c(Lcom/google/android/gms/drive/g/aw;)V

    .line 155
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/drive/a/ag;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    .line 159
    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/drive/a/ag;->a(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 160
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/ag;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/drive/a/ag;->d:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 164
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 212
    if-ne p0, p1, :cond_1

    .line 219
    :cond_0
    :goto_0
    return v0

    .line 215
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/drive/a/ag;

    if-nez v2, :cond_2

    move v0, v1

    .line 216
    goto :goto_0

    .line 218
    :cond_2
    check-cast p1, Lcom/google/android/gms/drive/a/ag;

    .line 219
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/a/ag;->a(Lcom/google/android/gms/drive/a/a;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/drive/a/ag;->d:Ljava/util/Set;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/ag;->d:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 188
    invoke-super {p0}, Lcom/google/android/gms/drive/a/b;->f()Lorg/json/JSONObject;

    move-result-object v1

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/drive/a/ag;->d:Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 190
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/drive/a/ag;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    .line 192
    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 194
    :cond_0
    const-string v0, "parentIds"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 196
    :cond_1
    const-string v0, "enforcementMode"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/ag;->e:Lcom/google/android/gms/drive/a/ac;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/a/ac;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 197
    return-object v1
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/gms/drive/a/b;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/EntrySpec;->hashCode()I

    move-result v0

    .line 206
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/drive/a/ag;->d:Ljava/util/Set;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 207
    return v0

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/ag;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final k()Ljava/util/Set;
    .locals 6

    .prologue
    .line 179
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/drive/a/ag;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    .line 181
    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 183
    :cond_0
    return-object v1
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 224
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "SetResourceParentsAction [%s, mParentIds=%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/ag;->h()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/drive/a/ag;->d:Ljava/util/Set;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
