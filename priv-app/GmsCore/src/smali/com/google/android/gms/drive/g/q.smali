.class public final Lcom/google/android/gms/drive/g/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/at;


# instance fields
.field public final a:Lcom/google/android/gms/drive/g/i;

.field public final b:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->c()Lcom/google/android/gms/drive/g/i;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/g/q;-><init>(Lcom/google/android/gms/drive/g/i;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/g/i;)V
    .locals 4

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/q;->b:Ljava/util/concurrent/atomic/AtomicLong;

    .line 27
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/i;

    iput-object v0, p0, Lcom/google/android/gms/drive/g/q;->a:Lcom/google/android/gms/drive/g/i;

    .line 28
    return-void
.end method

.method private a()J
    .locals 4

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/gms/drive/g/q;->b:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/gms/drive/g/q;->a:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v2}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 71
    const-wide/16 v2, 0x0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final c()Z
    .locals 4

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/q;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/google/android/gms/drive/g/q;->c()Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 87
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/q;->a()J

    move-result-wide v0

    .line 88
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 89
    const-wide/16 v2, 0x5

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    goto :goto_0

    .line 94
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 106
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s[%d msec wait time remaining]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "DelayRateLimiter"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-direct {p0}, Lcom/google/android/gms/drive/g/q;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
