.class public final Lcom/google/android/gms/ads/internal/s;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field final a:Lcom/google/android/gms/ads/internal/u;

.field final b:Ljava/lang/Runnable;

.field c:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

.field d:Z

.field e:Z

.field f:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/internal/b;)V
    .locals 2

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/gms/ads/internal/u;

    sget-object v1, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/internal/u;-><init>(Landroid/os/Handler;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/ads/internal/s;-><init>(Lcom/google/android/gms/ads/internal/b;Lcom/google/android/gms/ads/internal/u;)V

    .line 50
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/ads/internal/b;Lcom/google/android/gms/ads/internal/u;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/s;->d:Z

    .line 45
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/s;->e:Z

    .line 46
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/ads/internal/s;->f:J

    .line 54
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/s;->a:Lcom/google/android/gms/ads/internal/u;

    .line 55
    new-instance v0, Lcom/google/android/gms/ads/internal/t;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/ads/internal/t;-><init>(Lcom/google/android/gms/ads/internal/s;Lcom/google/android/gms/ads/internal/b;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/s;->b:Ljava/lang/Runnable;

    .line 68
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/s;->d:Z

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/s;->a:Lcom/google/android/gms/ads/internal/u;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/s;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/u;->a(Ljava/lang/Runnable;)V

    .line 73
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)V
    .locals 2

    .prologue
    .line 95
    const-wide/32 v0, 0xea60

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/gms/ads/internal/s;->a(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;J)V

    .line 96
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;J)V
    .locals 2

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/s;->d:Z

    if-eqz v0, :cond_1

    .line 100
    const-string v0, "An ad refresh is already scheduled."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/s;->c:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/s;->d:Z

    .line 106
    iput-wide p2, p0, Lcom/google/android/gms/ads/internal/s;->f:J

    .line 108
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/s;->e:Z

    if-nez v0, :cond_0

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Scheduling ad refresh "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " milliseconds from now."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/s;->a:Lcom/google/android/gms/ads/internal/u;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/s;->b:Ljava/lang/Runnable;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/u;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
