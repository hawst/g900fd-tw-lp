.class public final Lcom/google/android/gms/drive/database/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/drive/database/p;

.field final b:Ljava/util/concurrent/atomic/AtomicReference;

.field final c:Ljava/lang/ThreadLocal;

.field final d:Ljava/util/concurrent/atomic/AtomicLong;

.field private volatile e:Ljava/util/concurrent/atomic/AtomicLong;

.field private final f:Landroid/content/Context;

.field private final g:Ljava/lang/ThreadLocal;

.field private final h:Ljava/lang/ThreadLocal;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/g/ae;)V
    .locals 1

    .prologue
    .line 204
    const-string v0, "DocList.db"

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/drive/database/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/g/ae;Ljava/lang/String;)V

    .line 205
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/g/ae;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/i;->b:Ljava/util/concurrent/atomic/AtomicReference;

    .line 181
    new-instance v0, Lcom/google/android/gms/drive/database/j;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/database/j;-><init>(Lcom/google/android/gms/drive/database/i;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/i;->c:Ljava/lang/ThreadLocal;

    .line 188
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/i;->d:Ljava/util/concurrent/atomic/AtomicLong;

    .line 194
    new-instance v0, Lcom/google/android/gms/drive/database/l;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/database/l;-><init>(Lcom/google/android/gms/drive/database/i;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/i;->g:Ljava/lang/ThreadLocal;

    .line 201
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/i;->h:Ljava/lang/ThreadLocal;

    .line 208
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/i;->f:Landroid/content/Context;

    .line 209
    new-instance v0, Lcom/google/android/gms/drive/database/p;

    invoke-direct {v0, p1, p3, p2}, Lcom/google/android/gms/drive/database/p;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/drive/g/ae;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/i;->a:Lcom/google/android/gms/drive/database/p;

    .line 210
    return-void
.end method

.method private a(Lcom/google/android/gms/drive/database/model/ae;Landroid/content/ContentValues;Landroid/net/Uri;)J
    .locals 7

    .prologue
    .line 233
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->o()V

    .line 237
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/i;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 239
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 240
    const-string v2, "DocListDatabase"

    const-string v3, "Failed to insert %s object"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->p()V

    return-wide v0

    .line 242
    :cond_1
    if-eqz p3, :cond_0

    .line 243
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/drive/database/i;->a:Lcom/google/android/gms/drive/database/p;

    invoke-static {p3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/database/p;->a(Landroid/net/Uri;)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 247
    :catch_0
    move-exception v0

    .line 248
    :try_start_2
    const-string v1, "DocListDatabase"

    const-string v2, "Failed to save %s object"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 249
    const-string v1, "DocListDatabase"

    const-string v2, "Failed to save into %s object: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 250
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 252
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->p()V

    throw v0
.end method

.method private a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    .line 396
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->o()V

    .line 398
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/i;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 405
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->p()V

    return-object v0

    .line 401
    :catch_0
    move-exception v0

    .line 402
    :try_start_1
    const-string v1, "DocListDatabase"

    const-string v2, "Failed to query %s object"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 403
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 405
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->p()V

    throw v0
.end method

.method private m()V
    .locals 1

    .prologue
    .line 500
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->o()V

    .line 501
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/i;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 502
    return-void
.end method

.method private n()V
    .locals 1

    .prologue
    .line 522
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/i;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 523
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->p()V

    .line 524
    return-void
.end method

.method private o()V
    .locals 4

    .prologue
    .line 657
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/i;->b()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 658
    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 659
    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    .line 660
    iget-object v2, p0, Lcom/google/android/gms/drive/database/i;->c:Ljava/lang/ThreadLocal;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 661
    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 662
    return-void
.end method

.method private p()V
    .locals 4

    .prologue
    .line 668
    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 669
    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    .line 670
    iget-object v2, p0, Lcom/google/android/gms/drive/database/i;->c:Ljava/lang/ThreadLocal;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 671
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 672
    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    .line 674
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->decrementAndGet()J

    .line 675
    return-void
.end method


# virtual methods
.method public final a(JLcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)I
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 315
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-ltz v2, :cond_1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid rowId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 317
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->o()V

    .line 319
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/i;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ae;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 322
    if-eqz p4, :cond_0

    .line 323
    iget-object v1, p0, Lcom/google/android/gms/drive/database/i;->a:Lcom/google/android/gms/drive/database/p;

    invoke-static {p4, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/database/p;->a(Landroid/net/Uri;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 325
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->p()V

    return v0

    :cond_1
    move v0, v1

    .line 315
    goto :goto_0

    .line 326
    :catch_0
    move-exception v0

    .line 327
    :try_start_1
    const-string v1, "DocListDatabase"

    const-string v2, "Failed to delete %s object"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 328
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 330
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->p()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5

    .prologue
    .line 444
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->o()V

    .line 446
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/i;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 451
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->p()V

    return v0

    .line 447
    :catch_0
    move-exception v0

    .line 448
    :try_start_1
    const-string v1, "DocListDatabase"

    const-string v2, "Failed to delete from %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 449
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 451
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->p()V

    throw v0
.end method

.method public final a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 4

    .prologue
    .line 413
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->o()V

    .line 415
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/i;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 420
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->p()V

    return-object v0

    .line 416
    :catch_0
    move-exception v0

    .line 417
    :try_start_1
    const-string v1, "DocListDatabase"

    const-string v2, "Failed to run rawQuery"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 418
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 420
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->p()V

    throw v0
.end method

.method public final a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    .line 383
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->o()V

    .line 385
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, p5

    :try_start_0
    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 387
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->p()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->p()V

    throw v0
.end method

.method final a()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/ak;

    .line 218
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 219
    invoke-interface {v0}, Lcom/google/android/gms/drive/g/ak;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    return-object v0

    .line 218
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(JLcom/google/android/gms/drive/database/model/ae;Landroid/content/ContentValues;Landroid/net/Uri;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 268
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-ltz v2, :cond_1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 270
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->o()V

    .line 272
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/i;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ae;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, p4, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 275
    if-eqz p5, :cond_0

    .line 279
    invoke-virtual {p0, p5, p1, p2}, Lcom/google/android/gms/drive/database/i;->a(Landroid/net/Uri;J)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->p()V

    .line 286
    return-void

    :cond_1
    move v0, v1

    .line 268
    goto :goto_0

    .line 281
    :catch_0
    move-exception v0

    .line 282
    :try_start_1
    const-string v1, "DocListDatabase"

    const-string v2, "Failed to update %s object"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 283
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 285
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->p()V

    throw v0
.end method

.method public final a(Landroid/net/Uri;J)V
    .locals 4

    .prologue
    .line 300
    const-string v0, "null uri"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid rowId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 302
    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->a:Lcom/google/android/gms/drive/database/p;

    invoke-static {p1, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/p;->a(Landroid/net/Uri;)V

    .line 303
    return-void

    .line 301
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/ae;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 428
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SELECT EXISTS (SELECT * FROM "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 430
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_0

    .line 431
    const-string v2, "DocListDatabase"

    const-string v3, "EXISTS returned empty."

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 432
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 436
    :goto_0
    return v0

    .line 434
    :cond_0
    const/4 v2, 0x0

    :try_start_1
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x1

    .line 436
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final b(JLcom/google/android/gms/drive/database/model/ae;Landroid/content/ContentValues;Landroid/net/Uri;)J
    .locals 3

    .prologue
    .line 347
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 348
    invoke-direct {p0, p3, p4, p5}, Lcom/google/android/gms/drive/database/i;->a(Lcom/google/android/gms/drive/database/model/ae;Landroid/content/ContentValues;Landroid/net/Uri;)J

    move-result-wide p1

    .line 351
    :goto_0
    return-wide p1

    .line 350
    :cond_0
    invoke-virtual/range {p0 .. p5}, Lcom/google/android/gms/drive/database/i;->a(JLcom/google/android/gms/drive/database/model/ae;Landroid/content/ContentValues;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 490
    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->g:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    .line 491
    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 492
    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/n;

    .line 493
    iget-object v0, v0, Lcom/google/android/gms/drive/database/n;->a:Ljava/util/Stack;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 497
    :goto_0
    return-void

    .line 495
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->m()V

    goto :goto_0
.end method

.method public final d()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 508
    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->g:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    .line 509
    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 510
    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/n;

    .line 513
    iget-object v1, v0, Lcom/google/android/gms/drive/database/n;->a:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 515
    iget-boolean v4, v0, Lcom/google/android/gms/drive/database/n;->b:Z

    iget-object v1, v0, Lcom/google/android/gms/drive/database/n;->a:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    or-int v1, v4, v2

    iput-boolean v1, v0, Lcom/google/android/gms/drive/database/n;->b:Z

    .line 519
    :goto_2
    return-void

    :cond_0
    move v1, v3

    .line 513
    goto :goto_0

    :cond_1
    move v2, v3

    .line 515
    goto :goto_1

    .line 517
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->n()V

    goto :goto_2
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 556
    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->g:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    .line 557
    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 558
    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/n;

    .line 559
    iget-object v3, v0, Lcom/google/android/gms/drive/database/n;->a:Ljava/util/Stack;

    .line 562
    invoke-virtual {v3}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 564
    invoke-virtual {v3}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 565
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 569
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 562
    goto :goto_0

    .line 567
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/i;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    goto :goto_1
.end method

.method public final f()J
    .locals 9

    .prologue
    .line 683
    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->e:Ljava/util/concurrent/atomic/AtomicLong;

    .line 684
    if-nez v0, :cond_1

    .line 685
    monitor-enter p0

    .line 686
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->e:Ljava/util/concurrent/atomic/AtomicLong;

    .line 687
    if-nez v0, :cond_0

    .line 688
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->t:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    .line 690
    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ar;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " DESC"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "1"

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v4

    .line 699
    const-wide/16 v0, 0x0

    .line 701
    :try_start_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 702
    const/4 v0, 0x0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    move-wide v2, v0

    .line 705
    :goto_0
    :try_start_2
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 707
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    .line 708
    iput-object v0, p0, Lcom/google/android/gms/drive/database/i;->e:Ljava/util/concurrent/atomic/AtomicLong;

    .line 710
    :cond_0
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 712
    :cond_1
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    return-wide v0

    .line 705
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 710
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move-wide v2, v0

    goto :goto_0
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 717
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/i;->a()Landroid/database/sqlite/SQLiteDatabase;

    .line 718
    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->a:Lcom/google/android/gms/drive/database/p;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/p;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public final h()Z
    .locals 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v6, 0x0

    .line 728
    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/i;->a:Lcom/google/android/gms/drive/database/p;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/p;->getDatabaseName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 729
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/i;->b()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v7

    .line 751
    :goto_0
    return v0

    .line 734
    :cond_0
    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "ROWID"

    aput-object v0, v2, v8

    .line 735
    invoke-static {}, Lcom/google/android/gms/drive/database/o;->values()[Lcom/google/android/gms/drive/database/o;

    move-result-object v10

    array-length v11, v10

    move v9, v8

    :goto_1
    if-ge v9, v11, :cond_5

    aget-object v0, v10, v9

    .line 736
    invoke-static {v0}, Lcom/google/android/gms/drive/database/o;->a(Lcom/google/android/gms/drive/database/o;)Lcom/google/android/gms/drive/database/model/ae;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ae;->g()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 737
    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/drive/database/o;->a(Lcom/google/android/gms/drive/database/o;)Lcom/google/android/gms/drive/database/model/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 742
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-eqz v1, :cond_2

    .line 743
    if-eqz v0, :cond_1

    .line 747
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v8

    goto :goto_0

    .line 746
    :cond_2
    if-eqz v0, :cond_3

    .line 747
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 735
    :cond_3
    :goto_2
    add-int/lit8 v1, v9, 0x1

    move v9, v1

    move-object v6, v0

    goto :goto_1

    .line 746
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v6, :cond_4

    .line 747
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    :cond_5
    move v0, v7

    .line 751
    goto :goto_0

    .line 746
    :catchall_1
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    goto :goto_3

    :cond_6
    move-object v0, v6

    goto :goto_2
.end method

.method public final i()J
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 761
    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/i;->a:Lcom/google/android/gms/drive/database/p;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/p;->getDatabaseName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 762
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final j()V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 805
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/i;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v4

    .line 806
    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->g:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    .line 807
    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v5

    .line 808
    const-string v1, "DocListDatabase"

    const-string v6, "Begin savepoint %d"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-static {v1, v6, v7}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 809
    if-nez v5, :cond_0

    .line 810
    iget-object v6, p0, Lcom/google/android/gms/drive/database/i;->h:Ljava/lang/ThreadLocal;

    if-nez v4, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 811
    if-nez v4, :cond_0

    .line 812
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->m()V

    .line 815
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/i;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, ";savepoint s%d"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-static {v4, v6, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 816
    new-instance v1, Lcom/google/android/gms/drive/database/n;

    invoke-direct {v1, v3}, Lcom/google/android/gms/drive/database/n;-><init>(B)V

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 817
    return-void

    :cond_1
    move v1, v3

    .line 810
    goto :goto_0
.end method

.method public final k()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 823
    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->g:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    .line 824
    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 825
    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/n;

    .line 827
    iget-boolean v1, v0, Lcom/google/android/gms/drive/database/n;->c:Z

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/drive/database/n;->a:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    move-result v1

    if-eqz v1, :cond_0

    move v3, v2

    :cond_0
    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 828
    iput-boolean v2, v0, Lcom/google/android/gms/drive/database/n;->c:Z

    .line 829
    return-void

    :cond_1
    move v1, v3

    .line 824
    goto :goto_0
.end method

.method public final l()V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 837
    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->g:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    .line 838
    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 839
    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/database/n;

    .line 841
    iget-object v4, v1, Lcom/google/android/gms/drive/database/n;->a:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->empty()Z

    move-result v4

    invoke-static {v4}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 842
    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    .line 843
    iget-boolean v4, v1, Lcom/google/android/gms/drive/database/n;->c:Z

    if-eqz v4, :cond_0

    iget-boolean v1, v1, Lcom/google/android/gms/drive/database/n;->b:Z

    if-eqz v1, :cond_1

    .line 844
    :cond_0
    const-string v1, "DocListDatabase"

    const-string v4, "Rollback savepoint %d"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v1, v4, v5}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 845
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/i;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, ";rollback to s%d"

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 848
    :cond_1
    const-string v1, "DocListDatabase"

    const-string v4, "Release savepoint %d"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v1, v4, v5}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 849
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/i;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, ";release s%d"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v3

    invoke-static {v4, v5, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 851
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/drive/database/i;->h:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 852
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/i;->e()V

    .line 853
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/i;->n()V

    .line 855
    :cond_2
    return-void

    :cond_3
    move v1, v3

    .line 838
    goto/16 :goto_0
.end method
