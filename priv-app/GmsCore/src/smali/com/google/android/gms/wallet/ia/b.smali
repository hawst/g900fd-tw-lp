.class final Lcom/google/android/gms/wallet/ia/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/cd;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V
    .locals 0

    .prologue
    .line 688
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 727
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->b:Landroid/accounts/Account;

    const/4 v4, 0x1

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v3}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/checkout/inapp/proto/q;

    move-result-object v10

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v3}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->b(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Ljava/util/ArrayList;

    move-result-object v11

    move-object v3, v2

    move-object v6, v2

    move-object v7, v2

    move v8, v5

    move-object v9, v2

    invoke-static/range {v0 .. v11}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;ZZ[I[IILjava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    .line 740
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    const/16 v2, 0x1f5

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 741
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/j;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 691
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Lcom/google/checkout/inapp/proto/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 692
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->b:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/checkout/inapp/proto/q;

    move-result-object v6

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->b(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Ljava/util/ArrayList;

    move-result-object v7

    move-object v2, p1

    move-object v8, v5

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/j;ZZLjava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;Lcom/google/checkout/inapp/proto/a/b;)Landroid/content/Intent;

    move-result-object v0

    .line 702
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    const/16 v2, 0x1f7

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 723
    :goto_0
    return-void

    .line 704
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iput-object p1, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->d:Lcom/google/checkout/inapp/proto/j;

    .line 709
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->l:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/cc;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 712
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->c:Lcom/google/checkout/inapp/proto/t;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    new-array v1, v3, [Lcom/google/checkout/inapp/proto/j;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->d:Lcom/google/checkout/inapp/proto/j;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/checkout/inapp/proto/j;

    .line 715
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->l:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/cc;->a([Lcom/google/checkout/inapp/proto/j;)V

    .line 717
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->l:Lcom/google/android/gms/wallet/common/ui/cc;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->e:Lcom/google/checkout/inapp/proto/j;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    .line 721
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/b;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->c(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/checkout/inapp/proto/j;)V
    .locals 0

    .prologue
    .line 746
    return-void
.end method
