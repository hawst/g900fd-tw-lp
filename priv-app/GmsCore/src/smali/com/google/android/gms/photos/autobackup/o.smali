.class public final Lcom/google/android/gms/photos/autobackup/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/service/b;


# instance fields
.field private final a:Lcom/google/android/gms/photos/autobackup/a/ac;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/o;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    .line 24
    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/o;->b:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/o;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lcom/google/android/gms/photos/autobackup/a/ac;->b(I)V

    .line 42
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 2

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;

    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/o;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/b;->b(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/autobackup/aj;->a(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/o;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/photos/autobackup/a/ac;->b(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/o;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lcom/google/android/gms/photos/autobackup/a/ac;->b(I)V

    goto :goto_0
.end method
