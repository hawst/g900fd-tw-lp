.class public final Lcom/google/android/gms/reminders/provider/a;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# static fields
.field private static volatile a:Lcom/google/android/gms/reminders/provider/a;

.field private static final b:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/reminders/provider/a;->b:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 46
    const/4 v0, 0x0

    const/16 v1, 0xb

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 47
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/reminders/provider/a;
    .locals 3

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/gms/reminders/provider/a;->a:Lcom/google/android/gms/reminders/provider/a;

    if-nez v0, :cond_1

    .line 36
    sget-object v1, Lcom/google/android/gms/reminders/provider/a;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 37
    :try_start_0
    sget-object v0, Lcom/google/android/gms/reminders/provider/a;->a:Lcom/google/android/gms/reminders/provider/a;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/google/android/gms/reminders/provider/a;

    const-string v2, "reminders.db"

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/reminders/provider/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/reminders/provider/a;->a:Lcom/google/android/gms/reminders/provider/a;

    .line 40
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    :cond_1
    sget-object v0, Lcom/google/android/gms/reminders/provider/a;->a:Lcom/google/android/gms/reminders/provider/a;

    return-object v0

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 146
    const-string v0, "SELECT name FROM sqlite_master  WHERE type IN (\'table\',\'view\') AND name NOT LIKE \'sqlite_%\'  UNION ALL  SELECT name FROM sqlite_temp_master  WHERE type IN (\'table\',\'view\')  ORDER BY 1 "

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 152
    if-nez v1, :cond_0

    .line 162
    :goto_0
    return-void

    .line 157
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "DROP TABLE IF EXISTS "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 161
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 56
    const-string v0, "CREATE TABLE account (_id INTEGER PRIMARY KEY AUTOINCREMENT,account_name TEXT NOT NULL,storage_version TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 62
    const-string v0, "CREATE TABLE reminders (_id INTEGER PRIMARY KEY AUTOINCREMENT,account_id INTEGER NOT NULL,is_dirty INTEGER NOT NULL DEFAULT 0,client_assigned_id TEXT,server_assigned_id INTEGER,client_assigned_thread_id TEXT,task_list INTEGER,title TEXT,created_time_millis INTEGER,archived_time_ms INTEGER,archived INTEGER NOT NULL DEFAULT 0,deleted INTEGER NOT NULL DEFAULT 0,pinned INTEGER NOT NULL DEFAULT 0,snoozed INTEGER NOT NULL DEFAULT 0,snoozed_time_millis INTEGER,location_snoozed_until_ms INTEGER,due_date_year INTEGER,due_date_month INTEGER,due_date_day INTEGER,due_date_hour INTEGER,due_date_minute INTEGER,due_date_second INTEGER,due_date_period INTEGER,due_date_absolute_time_ms INTEGER,event_date_year INTEGER,event_date_month INTEGER,event_date_day INTEGER,event_date_hour INTEGER,event_date_minute INTEGER,event_date_second INTEGER,event_date_period INTEGER,event_date_absolute_time_ms INTEGER,lat REAL,lng REAL,name TEXT,radius_meters INTEGER,location_type INTEGER,display_address TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 108
    const-string v0, "CREATE TABLE notification (_id INTEGER PRIMARY KEY AUTOINCREMENT,state INTEGER NOT NULL DEFAULT 0,is_stale INTEGER,trigger_time INTEGER,create_time INTEGER,schedule_time INTEGER,fire_time INTEGER,snooze_time INTEGER,dismiss_time INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 120
    const-string v0, "CREATE TABLE package (_id INTEGER PRIMARY KEY AUTOINCREMENT,package_name TEXT NOT NULL,is_google_signed INTEGER NOT NULL DEFAULT 0);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 126
    const-string v0, "DROP TRIGGER IF EXISTS notification_create_trigger;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TRIGGER notification_create_trigger AFTER INSERT  ON reminders BEGIN  INSERT INTO notification ( _id,is_stale) VALUES ( NEW._id, 1 ); END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TRIGGER IF EXISTS notification_update_trigger;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TRIGGER notification_update_trigger AFTER UPDATE  ON reminders BEGIN  UPDATE notification SET is_stale = 1  WHERE _id =  NEW._id; END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TRIGGER IF EXISTS notification_delete_trigger;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TRIGGER notification_delete_trigger AFTER DELETE  ON reminders BEGIN  UPDATE notification SET is_stale = 1  WHERE _id =  OLD._id; END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method public final onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 139
    invoke-static {p1}, Lcom/google/android/gms/reminders/provider/a;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 140
    invoke-virtual {p0, p1}, Lcom/google/android/gms/reminders/provider/a;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 141
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1

    .prologue
    .line 131
    const/16 v0, 0xb

    if-ge p2, v0, :cond_0

    .line 132
    invoke-static {p1}, Lcom/google/android/gms/reminders/provider/a;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 133
    invoke-virtual {p0, p1}, Lcom/google/android/gms/reminders/provider/a;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 135
    :cond_0
    return-void
.end method
