.class final Lcom/google/android/gms/fitness/a/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/fitness/a/h;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/google/android/gms/fitness/a/w;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Lcom/google/android/gms/fitness/data/DataSet;)Lcom/google/android/gms/fitness/data/DataPoint;
    .locals 23

    .prologue
    .line 99
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    .line 100
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/data/DataPoint;

    .line 101
    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/fitness/data/DataPoint;

    .line 103
    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    .line 104
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    .line 106
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 107
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-static {v2}, Lcom/google/android/gms/fitness/a/v;->a(Lcom/google/android/gms/fitness/data/DataPoint;)F

    move-result v2

    .line 108
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/fitness/data/DataSet;->a()Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v3

    sget-object v8, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/gms/fitness/data/DataPoint;->a(JJLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v3

    const/4 v4, 0x3

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput v2, v4, v5

    const/4 v5, 0x1

    aput v2, v4, v5

    const/4 v5, 0x2

    aput v2, v4, v5

    invoke-virtual {v3, v4}, Lcom/google/android/gms/fitness/data/DataPoint;->a([F)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v2

    .line 145
    :goto_0
    return-object v2

    .line 115
    :cond_0
    sub-long v14, v6, v4

    .line 118
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .line 120
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/data/DataPoint;

    .line 121
    invoke-static {v2}, Lcom/google/android/gms/fitness/a/v;->a(Lcom/google/android/gms/fitness/data/DataPoint;)F

    move-result v3

    .line 123
    const-wide/16 v8, 0x0

    move v10, v3

    move-wide v12, v8

    move v9, v3

    move-object v8, v2

    .line 127
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 128
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/data/DataPoint;

    .line 129
    invoke-static {v2}, Lcom/google/android/gms/fitness/a/v;->a(Lcom/google/android/gms/fitness/data/DataPoint;)F

    move-result v11

    .line 131
    sget-object v17, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v18

    sget-object v17, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v20

    sub-long v18, v18, v20

    move-wide/from16 v0, v18

    long-to-double v0, v0

    move-wide/from16 v18, v0

    .line 133
    long-to-double v0, v14

    move-wide/from16 v20, v0

    div-double v18, v18, v20

    add-float/2addr v3, v11

    float-to-double v0, v3

    move-wide/from16 v20, v0

    mul-double v18, v18, v20

    const-wide/high16 v20, 0x4000000000000000L    # 2.0

    div-double v18, v18, v20

    add-double v12, v12, v18

    .line 135
    cmpg-float v3, v11, v10

    if-gez v3, :cond_3

    move v8, v11

    .line 138
    :goto_2
    cmpl-float v3, v11, v9

    if-lez v3, :cond_2

    move v3, v11

    .line 142
    :goto_3
    invoke-static {v2}, Lcom/google/android/gms/fitness/a/v;->a(Lcom/google/android/gms/fitness/data/DataPoint;)F

    move-result v9

    move v10, v8

    move-object v8, v2

    move/from16 v22, v9

    move v9, v3

    move/from16 v3, v22

    .line 143
    goto :goto_1

    .line 145
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/fitness/data/DataSet;->a()Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v3

    sget-object v8, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/gms/fitness/data/DataPoint;->a(JJLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [F

    const/4 v4, 0x0

    double-to-float v5, v12

    aput v5, v3, v4

    const/4 v4, 0x1

    aput v9, v3, v4

    const/4 v4, 0x2

    aput v10, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/data/DataPoint;->a([F)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v2

    goto :goto_0

    :cond_2
    move v3, v9

    goto :goto_3

    :cond_3
    move v8, v10

    goto :goto_2
.end method
