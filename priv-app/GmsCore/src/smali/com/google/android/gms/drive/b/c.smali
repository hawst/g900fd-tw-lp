.class public final Lcom/google/android/gms/drive/b/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/b/g;


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/model/a;

.field private final b:Lcom/google/android/gms/drive/auth/AppIdentity;

.field private final c:Lcom/google/android/gms/drive/database/model/EntrySpec;

.field private final d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

.field private final e:J

.field private final f:Lcom/google/android/gms/drive/a/a/a;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Lcom/google/android/gms/drive/a/a/l;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcom/google/android/gms/drive/a/a/a;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/a/a/l;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/a;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/c;->a:Lcom/google/android/gms/drive/database/model/a;

    .line 39
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/AppIdentity;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/c;->b:Lcom/google/android/gms/drive/auth/AppIdentity;

    .line 40
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/EntrySpec;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/c;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    .line 41
    iput-object p4, p0, Lcom/google/android/gms/drive/b/c;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 42
    iput-wide p5, p0, Lcom/google/android/gms/drive/b/c;->e:J

    .line 43
    invoke-static {p7}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/a/a;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/c;->f:Lcom/google/android/gms/drive/a/a/a;

    .line 44
    iput-object p8, p0, Lcom/google/android/gms/drive/b/c;->g:Ljava/lang/String;

    .line 45
    iput-object p9, p0, Lcom/google/android/gms/drive/b/c;->h:Ljava/lang/String;

    .line 46
    invoke-static {p10}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/a/l;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/c;->i:Lcom/google/android/gms/drive/a/a/l;

    .line 47
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/drive/a/j;

    iget-object v1, p0, Lcom/google/android/gms/drive/b/c;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, p0, Lcom/google/android/gms/drive/b/c;->b:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-object v3, p0, Lcom/google/android/gms/drive/b/c;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    iget-object v5, p0, Lcom/google/android/gms/drive/b/c;->d:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iget-wide v6, p0, Lcom/google/android/gms/drive/b/c;->e:J

    iget-object v8, p0, Lcom/google/android/gms/drive/b/c;->g:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/gms/drive/b/c;->h:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/gms/drive/b/c;->i:Lcom/google/android/gms/drive/a/a/l;

    move-object v4, p1

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/drive/a/j;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/a/a/l;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/b/c;->f:Lcom/google/android/gms/drive/a/a/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/a/a/a;->a(Lcom/google/android/gms/drive/a/c;)I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    new-instance v2, Lcom/google/android/gms/drive/b/a;

    invoke-direct {v2, v1, v0}, Lcom/google/android/gms/drive/b/a;-><init>(ILcom/google/android/gms/drive/a/c;)V

    return-object v2

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
