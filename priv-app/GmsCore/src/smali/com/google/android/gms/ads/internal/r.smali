.class final Lcom/google/android/gms/ads/internal/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/a/a;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/ads/internal/p/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/internal/p/a;)V
    .locals 0

    .prologue
    .line 1692
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1693
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/r;->a:Lcom/google/android/gms/ads/internal/p/a;

    .line 1694
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    .line 1698
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1699
    const-string v2, "isVisible"

    if-eqz p1, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1700
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/r;->a:Lcom/google/android/gms/ads/internal/p/a;

    const-string v2, "onAdVisibilityChanged"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/ads/internal/p/a;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 1701
    return-void

    .line 1699
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method
