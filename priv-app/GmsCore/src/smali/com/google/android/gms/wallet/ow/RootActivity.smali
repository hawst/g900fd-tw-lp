.class public Lcom/google/android/gms/wallet/ow/RootActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/bf;
.implements Lcom/google/android/gms/wallet/common/ui/cq;
.implements Lcom/google/android/gms/wallet/common/ui/de;
.implements Lcom/google/android/gms/wallet/payform/f;


# instance fields
.field a:Lcom/google/android/gms/wallet/common/ui/bb;

.field b:Lcom/google/android/gms/wallet/common/ui/TopBarView;

.field c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private d:Landroid/accounts/Account;

.field private e:Ljava/util/HashSet;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 74
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/wallet/ow/RootActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 77
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 78
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    const-string v1, "buyFlowConfig"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 80
    return-object v0
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 331
    if-nez p2, :cond_0

    .line 332
    new-instance p2, Landroid/content/Intent;

    invoke-direct {p2}, Landroid/content/Intent;-><init>()V

    .line 334
    :cond_0
    invoke-static {}, Lcom/google/android/gms/wallet/MaskedWallet;->a()Lcom/google/android/gms/wallet/o;

    move-result-object v1

    .line 335
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 336
    const-string v0, "com.google.android.gms.wallet.EXTRA_REQUEST"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;

    .line 338
    if-eqz v0, :cond_1

    .line 339
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/o;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/o;

    .line 342
    :cond_1
    const-string v0, "com.google.android.gms.wallet.EXTRA_GOOGLE_TRANSACTION_ID"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 344
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 345
    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/o;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/o;

    .line 347
    :cond_2
    const-string v0, "com.google.android.gms.wallet.EXTRA_MASKED_WALLET"

    iget-object v1, v1, Lcom/google/android/gms/wallet/o;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 348
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/wallet/ow/RootActivity;->setResult(ILandroid/content/Intent;)V

    .line 349
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->finish()V

    .line 350
    return-void
.end method

.method private a(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->b:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a()V

    .line 170
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->gr:I

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 173
    return-void
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 307
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 308
    const-string v1, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 309
    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/wallet/ow/RootActivity;->a(ILandroid/content/Intent;)V

    .line 310
    return-void
.end method

.method private g()V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v8, -0x1

    .line 196
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->gr:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 200
    instance-of v0, v1, Lcom/google/android/gms/wallet/ow/g;

    if-eqz v0, :cond_8

    move-object v0, v1

    .line 201
    check-cast v0, Lcom/google/android/gms/wallet/ow/g;

    .line 202
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/g;->c()I

    move-result v7

    .line 203
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/g;->d()I

    move-result v8

    .line 206
    :goto_0
    if-eqz v1, :cond_0

    .line 207
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 213
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/wallet/shared/common/d/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 214
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_1
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "RootActivity.NETWORK_ERROR_DIALOG"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 244
    :cond_2
    :goto_1
    return-void

    .line 216
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->e:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->d:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->e:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->d:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->d:Landroid/accounts/Account;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/f;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/ui/dc;->a(Landroid/accounts/Account;[Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/dc;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "RetrieveAuthTokensFragment"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 218
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 219
    const-string v1, "com.google.android.gms.wallet.onlinewallet.ACTION_AUTHENTICATE_INSTRUMENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 220
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->d:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "com.google.android.gms.wallet.EXTRA_SELECTED_INSTRUMENT"

    const-class v5, Lcom/google/checkout/inapp/proto/j;

    invoke-static {v2, v3, v5}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v2

    check-cast v2, Lcom/google/checkout/inapp/proto/j;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v5, "com.google.android.gms.wallet.FULL_WALLET_REQUEST"

    const-class v6, Lcom/google/aa/b/a/a/a/a/i;

    invoke-static {v3, v5, v6}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v3

    check-cast v3, Lcom/google/aa/b/a/a/a/a/i;

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/wallet/ow/d;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/j;Lcom/google/aa/b/a/a/a/a/i;ZLjava/lang/String;)Lcom/google/android/gms/wallet/ow/d;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/RootActivity;->a(Landroid/support/v4/app/Fragment;)V

    goto :goto_1

    .line 221
    :cond_5
    const-string v1, "com.google.android.gms.wallet.onlinewallet.ACTION_GET_MASKED_WALLET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 222
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.EXTRA_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->d:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "com.google.android.gms.wallet.EXTRA_GOOGLE_TRANSACTION_ID"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "com.google.android.gms.wallet.EXTRA_SELECTED_INSTRUMENT_ID"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v9, "com.google.android.gms.wallet.EXTRA_USE_WALLET_BALANCE_CHECKED"

    sget-object v5, Lcom/google/android/gms/wallet/b/h;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-virtual {v6, v9, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v9, "com.google.android.gms.wallet.EXTRA_SELECTED_ADDRESS_ID"

    invoke-virtual {v6, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;II)Lcom/google/android/gms/wallet/ow/g;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/RootActivity;->a(Landroid/support/v4/app/Fragment;)V

    goto/16 :goto_1

    .line 223
    :cond_6
    const-string v1, "com.google.android.gms.wallet.onlinewallet.ACTION_CREATE_WALLET_OBJECTS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 233
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/wallet/ow/q;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 236
    if-eqz v0, :cond_7

    .line 237
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 241
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.CREATE_WALLET_OBJECTS_REQUEST"

    const-class v2, Lcom/google/aa/b/a/a/a/a/g;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/g;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.gms.wallet.WOBS_ISSUER_NAME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "com.google.android.gms.wallet.WOBS_OBJECT_NAME"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->d:Landroid/accounts/Account;

    invoke-static {v3, v4, v0, v1, v2}, Lcom/google/android/gms/wallet/ow/q;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/g;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/ow/q;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/RootActivity;->a(Landroid/support/v4/app/Fragment;)V

    goto/16 :goto_1

    :cond_8
    move v7, v8

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 279
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/RootActivity;->b(I)V

    .line 283
    :goto_0
    return-void

    .line 281
    :cond_0
    const/16 v0, 0x19b

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/RootActivity;->b(I)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 287
    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_0

    .line 288
    packed-switch p1, :pswitch_data_0

    .line 294
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/RootActivity;->a(ILandroid/content/Intent;)V

    .line 300
    :goto_0
    return-void

    .line 290
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->g()V

    goto :goto_0

    .line 298
    :cond_0
    const-string v0, "RootActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown error dialog error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 288
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->d:Landroid/accounts/Account;

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    :goto_0
    return-void

    .line 185
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.EXTRA_GOOGLE_TRANSACTION_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.EXTRA_SELECTED_INSTRUMENT_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.EXTRA_SELECTED_ADDRESS_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 186
    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->d:Landroid/accounts/Account;

    .line 187
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->g()V

    goto :goto_0
.end method

.method public final a(Lcom/google/aa/b/a/a/a/a/i;Lcom/google/checkout/inapp/proto/j;)V
    .locals 1

    .prologue
    .line 429
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->b:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Z)V

    .line 435
    return-void
.end method

.method public final b(Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 193
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "RetrieveAuthTokensFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 264
    if-eqz v0, :cond_0

    .line 265
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 269
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 273
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/RootActivity;->a(ILandroid/content/Intent;)V

    .line 274
    return-void
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 314
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 315
    const-string v1, "com.google.android.gms.wallet.ANALYTICS_SESSION_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 317
    const-string v2, "com.google.android.gms.wallet.MASKED_WALLET_FLOW_TYPE"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 319
    if-eqz v0, :cond_0

    .line 320
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x3

    invoke-static {v0, v3, v2, v1}, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;->a(Landroid/content/Context;IILjava/lang/String;)Ljava/lang/String;

    .line 323
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v3, v0}, Lcom/google/android/gms/wallet/ow/RootActivity;->a(ILandroid/content/Intent;)V

    .line 324
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "buyFlowConfig"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 103
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/dt;->c:Lcom/google/android/gms/wallet/common/ui/dz;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/common/ui/dz;)V

    .line 106
    sget v0, Lcom/google/android/gms/l;->gn:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/RootActivity;->setContentView(I)V

    .line 107
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/Window;)V

    .line 114
    :goto_0
    sget v0, Lcom/google/android/gms/j;->tc:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/RootActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->b:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    .line 116
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->e:Ljava/util/HashSet;

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->c()Landroid/accounts/Account;

    move-result-object v0

    .line 121
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 123
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->b:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v1, p0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Lcom/google/android/gms/wallet/common/ui/cq;)V

    .line 128
    :goto_1
    if-eqz p1, :cond_4

    .line 129
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->d:Landroid/accounts/Account;

    .line 130
    const-string v0, "accountsThatHaveRequestedAuthTokens"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    const-string v0, "accountsThatHaveRequestedAuthTokens"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 134
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->e:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 142
    :cond_0
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->b:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->d:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/accounts/Account;)V

    .line 143
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->gr:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_1

    .line 144
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->g()V

    .line 146
    :cond_1
    return-void

    .line 109
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/dt;->b:Lcom/google/android/gms/wallet/common/ui/dz;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/common/ui/dz;)V

    .line 111
    sget v0, Lcom/google/android/gms/l;->gm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/RootActivity;->setContentView(I)V

    goto :goto_0

    .line 125
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->b:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Lcom/google/android/gms/wallet/common/ui/cq;)V

    goto :goto_1

    .line 137
    :cond_4
    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->d:Landroid/accounts/Account;

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onlinewallet_root_activity"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 150
    invoke-super {p0}, Landroid/support/v7/app/d;->onResume()V

    .line 151
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "RootActivity.NETWORK_ERROR_DIALOG"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 156
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 160
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 162
    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->d:Landroid/accounts/Account;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 163
    const-string v0, "accountsThatHaveRequestedAuthTokens"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->e:Ljava/util/HashSet;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 165
    return-void
.end method
