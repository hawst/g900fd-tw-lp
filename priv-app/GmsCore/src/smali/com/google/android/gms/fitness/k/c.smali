.class public final Lcom/google/android/gms/fitness/k/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/fitness/l/z;

.field public final b:Lcom/google/android/gms/fitness/service/d;

.field public final c:Lcom/google/android/gms/fitness/sync/d;

.field public final d:Lcom/google/android/gms/fitness/sensors/a;

.field public final e:Lcom/google/android/gms/fitness/k/d;

.field public final f:Ljava/util/Map;

.field public final g:Ljava/util/Map;

.field public final h:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/fitness/l/z;Lcom/google/android/gms/fitness/service/d;Lcom/google/android/gms/fitness/sync/d;Lcom/google/android/gms/fitness/sensors/a;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lcom/google/android/gms/fitness/k/d;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/k/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/k/c;->e:Lcom/google/android/gms/fitness/k/d;

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/k/c;->f:Ljava/util/Map;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/k/c;->g:Ljava/util/Map;

    .line 47
    iput-object p1, p0, Lcom/google/android/gms/fitness/k/c;->h:Landroid/content/Context;

    .line 48
    iput-object p2, p0, Lcom/google/android/gms/fitness/k/c;->a:Lcom/google/android/gms/fitness/l/z;

    .line 49
    iput-object p3, p0, Lcom/google/android/gms/fitness/k/c;->b:Lcom/google/android/gms/fitness/service/d;

    .line 50
    iput-object p4, p0, Lcom/google/android/gms/fitness/k/c;->c:Lcom/google/android/gms/fitness/sync/d;

    .line 51
    iput-object p5, p0, Lcom/google/android/gms/fitness/k/c;->d:Lcom/google/android/gms/fitness/sensors/a;

    .line 52
    return-void
.end method

.method private static a(Lcom/google/android/gms/fitness/b/ad;)Lcom/google/android/gms/fitness/data/DataSource;
    .locals 2

    .prologue
    .line 98
    new-instance v0, Lcom/google/android/gms/fitness/b/a/h;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/b/a/h;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Lcom/google/android/gms/fitness/b/ad;->a(Lcom/google/android/gms/fitness/b/g;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 101
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/c/cj;->c(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/b/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/b/a/e;->a:Lcom/google/android/gms/fitness/data/DataSource;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;
    .locals 2

    .prologue
    .line 61
    invoke-static {p1}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/google/android/gms/fitness/k/c;->e:Lcom/google/android/gms/fitness/k/d;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/gms/fitness/k/d;->a(Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/c/e;)V

    .line 63
    iget-object v1, p0, Lcom/google/android/gms/fitness/k/c;->f:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/k/c;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 87
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSource;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v3

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/fitness/k/c;->g:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "Default data source for %s already defined"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v2

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/fitness/k/c;->g:Ljava/util/Map;

    invoke-interface {v0, v3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    return-object p0

    :cond_0
    move v0, v2

    .line 88
    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    .line 77
    invoke-static {p1}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/k/c;

    .line 78
    return-object p0
.end method
