.class final Lcom/google/android/gms/smart_profile/card/view/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;


# instance fields
.field final synthetic a:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

.field private b:Landroid/widget/ImageView;

.field private c:Ljava/lang/String;

.field private d:Z


# direct methods
.method private constructor <init>(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;Landroid/widget/ImageView;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/card/view/j;->a:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 324
    iput-object p2, p0, Lcom/google/android/gms/smart_profile/card/view/j;->b:Landroid/widget/ImageView;

    .line 325
    iput-object p3, p0, Lcom/google/android/gms/smart_profile/card/view/j;->c:Ljava/lang/String;

    .line 326
    iput-boolean p4, p0, Lcom/google/android/gms/smart_profile/card/view/j;->d:Z

    .line 327
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;Landroid/widget/ImageView;Ljava/lang/String;ZB)V
    .locals 0

    .prologue
    .line 317
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/smart_profile/card/view/j;-><init>(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;Landroid/widget/ImageView;Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 4

    .prologue
    .line 331
    new-instance v0, Lcom/google/android/gms/smart_profile/b/g;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/view/j;->a:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/view/j;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/card/view/j;->a:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-static {v3}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->g(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)Lcom/google/android/gms/common/api/v;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/smart_profile/b/g;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/api/v;)V

    return-object v0
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 346
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 317
    check-cast p2, Landroid/graphics/Bitmap;

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/view/j;->a:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget-boolean v1, p0, Lcom/google/android/gms/smart_profile/card/view/j;->d:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/view/j;->b:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/view/j;->a:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-static {v2, v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/view/j;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
