.class public final enum Lcom/google/android/gms/drive/a/ac;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/a/ac;

.field public static final enum b:Lcom/google/android/gms/drive/a/ac;

.field private static final synthetic d:[Lcom/google/android/gms/drive/a/ac;


# instance fields
.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12
    new-instance v0, Lcom/google/android/gms/drive/a/ac;

    const-string v1, "NORMAL"

    const-string v2, "normal"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/gms/drive/a/ac;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/a/ac;->a:Lcom/google/android/gms/drive/a/ac;

    .line 17
    new-instance v0, Lcom/google/android/gms/drive/a/ac;

    const-string v1, "NONE"

    const-string v2, "none"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/gms/drive/a/ac;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/a/ac;->b:Lcom/google/android/gms/drive/a/ac;

    .line 8
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/drive/a/ac;

    sget-object v1, Lcom/google/android/gms/drive/a/ac;->a:Lcom/google/android/gms/drive/a/ac;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/gms/drive/a/ac;->b:Lcom/google/android/gms/drive/a/ac;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/gms/drive/a/ac;->d:[Lcom/google/android/gms/drive/a/ac;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    iput-object p3, p0, Lcom/google/android/gms/drive/a/ac;->c:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/drive/a/ac;
    .locals 5

    .prologue
    .line 40
    invoke-static {}, Lcom/google/android/gms/drive/a/ac;->values()[Lcom/google/android/gms/drive/a/ac;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 41
    iget-object v4, v3, Lcom/google/android/gms/drive/a/ac;->c:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 42
    return-object v3

    .line 40
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown permission enforcement mode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/a/ac;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/google/android/gms/drive/a/ac;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/ac;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/a/ac;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/google/android/gms/drive/a/ac;->d:[Lcom/google/android/gms/drive/a/ac;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/a/ac;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/a/ac;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/gms/drive/a/ac;->c:Ljava/lang/String;

    return-object v0
.end method
