.class public final Lcom/google/android/gms/wallet/common/ui/cp;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/cb;
.implements Lcom/google/android/gms/wallet/common/ui/dp;


# instance fields
.field a:Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;

.field b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

.field c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field e:Lcom/google/android/gms/wallet/common/ui/bh;

.field f:Lcom/google/android/gms/wallet/common/ui/bi;

.field g:Lcom/google/android/gms/wallet/common/ui/ai;

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->h:Z

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/cp;->h:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setEnabled(Z)V

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/cp;->h:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/cp;->h:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 159
    :cond_0
    return-void
.end method

.method private b(Z)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 89
    const/4 v0, 0x3

    new-array v4, v0, [Lcom/google/android/gms/wallet/common/ui/dn;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    aput-object v0, v4, v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->e:Lcom/google/android/gms/wallet/common/ui/bh;

    aput-object v0, v4, v1

    const/4 v0, 0x2

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cp;->f:Lcom/google/android/gms/wallet/common/ui/bi;

    aput-object v3, v4, v0

    .line 95
    array-length v5, v4

    move v3, v2

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    .line 96
    if-eqz p1, :cond_2

    .line 97
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->f()Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v0, :cond_1

    move v0, v1

    .line 95
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 97
    goto :goto_1

    .line 98
    :cond_2
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->e()Z

    move-result v6

    if-nez v6, :cond_0

    .line 102
    :goto_2
    return v2

    :cond_3
    move v2, v0

    goto :goto_2
.end method

.method private c()Lcom/google/checkout/a/a/a/b;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 178
    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 179
    invoke-static {v2}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;)I

    move-result v4

    .line 182
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 188
    :goto_0
    :try_start_1
    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/cp;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    add-int/lit16 v5, v5, 0x7d0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 193
    :goto_1
    new-instance v5, Lcom/google/checkout/a/a/a/b;

    invoke-direct {v5}, Lcom/google/checkout/a/a/a/b;-><init>()V

    .line 194
    if-eqz v0, :cond_0

    .line 195
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v5, Lcom/google/checkout/a/a/a/b;->b:I

    .line 197
    :cond_0
    if-eqz v1, :cond_1

    .line 198
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v5, Lcom/google/checkout/a/a/a/b;->c:I

    .line 200
    :cond_1
    if-eqz v4, :cond_2

    .line 201
    iput v4, v5, Lcom/google/checkout/a/a/a/b;->f:I

    .line 203
    :cond_2
    iput-object v3, v5, Lcom/google/checkout/a/a/a/b;->e:Ljava/lang/String;

    .line 204
    new-instance v0, Lcom/google/checkout/a/a/a/c;

    invoke-direct {v0}, Lcom/google/checkout/a/a/a/c;-><init>()V

    iput-object v0, v5, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    .line 205
    iget-object v0, v5, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    iput-object v2, v0, Lcom/google/checkout/a/a/a/c;->a:Ljava/lang/String;

    .line 207
    return-object v5

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v5

    goto :goto_1
.end method


# virtual methods
.method public final a()Lcom/google/checkout/a/a/a/d;
    .locals 2

    .prologue
    .line 170
    new-instance v0, Lcom/google/checkout/a/a/a/d;

    invoke-direct {v0}, Lcom/google/checkout/a/a/a/d;-><init>()V

    .line 171
    const/4 v1, 0x1

    iput v1, v0, Lcom/google/checkout/a/a/a/d;->a:I

    .line 172
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/cp;->c()Lcom/google/checkout/a/a/a/b;

    move-result-object v1

    iput-object v1, v0, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    .line 173
    return-object v0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 149
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/cp;->h:Z

    .line 150
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/cp;->b()V

    .line 151
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/cp;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/cp;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 108
    const/4 v2, 0x3

    new-array v3, v2, [Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cp;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    aput-object v2, v3, v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cp;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v2, v3, v0

    const/4 v2, 0x2

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/cp;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v4, v3, v2

    .line 114
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 116
    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e()Z

    move-result v6

    if-nez v6, :cond_0

    .line 117
    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->clearFocus()V

    .line 118
    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    .line 122
    :goto_1
    return v0

    .line 114
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 122
    goto :goto_1
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 212
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 214
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cp;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->d(Landroid/app/Activity;)V

    .line 216
    if-eqz p1, :cond_0

    .line 217
    const-string v0, "enabled"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->h:Z

    .line 219
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 224
    sget v0, Lcom/google/android/gms/l;->gE:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 227
    sget v0, Lcom/google/android/gms/j;->dR:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->a:Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;

    .line 229
    sget v0, Lcom/google/android/gms/j;->cB:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    .line 230
    sget v0, Lcom/google/android/gms/j;->fM:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 231
    sget v0, Lcom/google/android/gms/j;->fN:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 233
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/bg;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cp;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cp;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/cp;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/gms/wallet/common/ui/bg;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    .line 235
    new-instance v2, Lcom/google/android/gms/wallet/common/ui/bh;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cp;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/cp;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/gms/wallet/common/ui/bh;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/dn;)V

    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cp;->e:Lcom/google/android/gms/wallet/common/ui/bh;

    .line 236
    new-instance v2, Lcom/google/android/gms/wallet/common/ui/bi;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cp;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/wallet/common/ui/bi;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/dn;)V

    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cp;->f:Lcom/google/android/gms/wallet/common/ui/bi;

    .line 237
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/ai;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cp;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-direct {v0, v2}, Lcom/google/android/gms/wallet/common/ui/ai;-><init>(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->g:Lcom/google/android/gms/wallet/common/ui/ai;

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cp;->e:Lcom/google/android/gms/wallet/common/ui/bh;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cp;->e:Lcom/google/android/gms/wallet/common/ui/bh;

    invoke-virtual {v0, v2, v3, v6}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/am;Lcom/google/android/gms/wallet/common/ui/dn;Z)V

    .line 240
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cp;->f:Lcom/google/android/gms/wallet/common/ui/bi;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cp;->f:Lcom/google/android/gms/wallet/common/ui/bi;

    invoke-virtual {v0, v2, v3, v6}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/am;Lcom/google/android/gms/wallet/common/ui/dn;Z)V

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cp;->g:Lcom/google/android/gms/wallet/common/ui/ai;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cp;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0, v2, v3, v5}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Lcom/google/android/gms/wallet/common/ui/am;Lcom/google/android/gms/wallet/common/ui/dn;Z)V

    .line 243
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cp;->e:Lcom/google/android/gms/wallet/common/ui/bh;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/dn;)V

    .line 244
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cp;->f:Lcom/google/android/gms/wallet/common/ui/bi;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/dn;)V

    .line 246
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cp;->e:Lcom/google/android/gms/wallet/common/ui/bh;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    sget v2, Lcom/google/android/gms/j;->fM:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setNextFocusDownId(I)V

    .line 251
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    sget v2, Lcom/google/android/gms/j;->fN:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusDownId(I)V

    .line 254
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    sget v2, Lcom/google/android/gms/j;->cB:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusUpId(I)V

    .line 255
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->d:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    sget v2, Lcom/google/android/gms/j;->fM:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusUpId(I)V

    .line 257
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cp;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cp;->a:Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Lcom/google/android/gms/wallet/common/ui/ak;)V

    .line 259
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cp;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "disallowedCreditCardTypes"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    .line 261
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cp;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->b()V

    .line 262
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cp;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a([I)V

    .line 264
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/cp;->b()V

    .line 266
    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 271
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 273
    const-string v0, "enabled"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/cp;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 274
    return-void
.end method
