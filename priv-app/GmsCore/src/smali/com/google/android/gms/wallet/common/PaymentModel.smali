.class public Lcom/google/android/gms/wallet/common/PaymentModel;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:Z

.field public b:Lcom/google/checkout/inapp/proto/j;

.field public c:Lcom/google/checkout/inapp/proto/a/b;

.field public d:Lcom/google/aa/b/a/h;

.field public e:Z

.field public f:Ljava/lang/String;

.field public g:I

.field public h:I

.field public i:Lcom/google/checkout/inapp/proto/j;

.field public j:Lcom/google/checkout/inapp/proto/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 128
    new-instance v0, Lcom/google/android/gms/wallet/common/v;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/v;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/PaymentModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    .line 97
    iput v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 116
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;Landroid/os/Parcel;)V

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;Landroid/os/Parcel;)V

    .line 119
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 121
    iget v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122
    iget v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->i:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;Landroid/os/Parcel;)V

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->j:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;Landroid/os/Parcel;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->d:Lcom/google/aa/b/a/h;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;Landroid/os/Parcel;)V

    .line 126
    return-void

    :cond_0
    move v0, v2

    .line 116
    goto :goto_0

    :cond_1
    move v1, v2

    .line 119
    goto :goto_1
.end method
