.class public final Lcom/google/android/gms/reminders/service/a/c;
.super Lcom/google/android/gms/reminders/service/a/a;
.source "SourceFile"


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/reminders/model/TaskEntity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/model/TaskEntity;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/gms/reminders/service/a/a;-><init>(Lcom/google/android/gms/reminders/internal/b;)V

    .line 33
    iput-object p2, p0, Lcom/google/android/gms/reminders/service/a/c;->b:Ljava/lang/String;

    .line 34
    iput-object p3, p0, Lcom/google/android/gms/reminders/service/a/c;->c:Lcom/google/android/gms/reminders/model/TaskEntity;

    .line 35
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 6

    .prologue
    .line 25
    check-cast p1, Lcom/google/android/gms/reminders/service/RemindersIntentService;

    iget-object v0, p0, Lcom/google/android/gms/reminders/service/a/c;->b:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/gms/reminders/a/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/reminders/a/d;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/reminders/service/a/c;->a:Lcom/google/android/gms/reminders/internal/b;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x1770

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/reminders/internal/b;->a(Lcom/google/android/gms/common/api/Status;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/reminders/service/a/c;->c:Lcom/google/android/gms/reminders/model/TaskEntity;

    invoke-static {v1}, Lcom/google/android/gms/reminders/c/b;->a(Lcom/google/android/gms/reminders/model/TaskEntity;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "server_assigned_id"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    const-string v2, "client_assigned_id"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "client_assigned_id"

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v2, "account_id"

    iget-wide v4, v0, Lcom/google/android/gms/reminders/a/d;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "is_dirty"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p1}, Lcom/google/android/gms/reminders/service/RemindersIntentService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/reminders/internal/a/k;->a:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/gms/reminders/service/a/c;->a:Lcom/google/android/gms/reminders/internal/b;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/reminders/internal/b;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0
.end method
