.class public final Lcom/google/android/gms/people/c/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final l:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/common/util/p;

.field private final d:Lcom/google/android/gms/people/c/g;

.field private final e:Lcom/google/android/gms/people/c/a;

.field private f:Lcom/google/android/gms/people/c/e;

.field private g:Lcom/google/android/gms/people/c/e;

.field private final h:Lcom/google/android/gms/people/c/a/f;

.field private final i:Z

.field private final j:Lcom/google/android/gms/people/c/b;

.field private volatile k:Ljava/util/concurrent/CountDownLatch;

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 720
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "value"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/people/c/f;->l:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 759
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/c/f;->a:Ljava/lang/Object;

    .line 113
    new-instance v0, Lcom/google/android/gms/people/c/a;

    invoke-direct {v0}, Lcom/google/android/gms/people/c/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/c/f;->e:Lcom/google/android/gms/people/c/a;

    .line 125
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/people/c/f;->k:Ljava/util/concurrent/CountDownLatch;

    .line 3111
    iput v3, p0, Lcom/google/android/gms/people/c/f;->m:I

    .line 760
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    .line 761
    new-instance v0, Lcom/google/android/gms/people/c/g;

    iget-object v1, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    const-string v2, "pluscontacts.db"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/people/c/g;-><init>(Lcom/google/android/gms/people/c/f;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/people/c/f;->d:Lcom/google/android/gms/people/c/g;

    .line 763
    iput-boolean v3, p0, Lcom/google/android/gms/people/c/f;->i:Z

    .line 764
    new-instance v0, Lcom/google/android/gms/people/c/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/c/b;-><init>(Lcom/google/android/gms/people/c/f;)V

    iput-object v0, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    .line 765
    new-instance v0, Lcom/google/android/gms/people/c/a/f;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/people/c/a/f;-><init>(Lcom/google/android/gms/people/c/f;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/people/c/f;->h:Lcom/google/android/gms/people/c/a/f;

    .line 766
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->a(Landroid/content/Context;)Lcom/google/android/gms/common/util/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/c/f;->c:Lcom/google/android/gms/common/util/p;

    .line 776
    return-void
.end method

.method static synthetic A(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "ALTER TABLE people ADD COLUMN sync_is_alive INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic B(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "ALTER TABLE ac_item ADD COLUMN logging_id TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE emails ADD COLUMN affinity1 REAL;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE emails ADD COLUMN affinity2 REAL;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE emails ADD COLUMN affinity3 REAL;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE emails ADD COLUMN affinity4 REAL;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE emails ADD COLUMN affinity5 REAL;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE emails ADD COLUMN logging_id TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic C(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "ALTER TABLE people ADD COLUMN logging_id TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE ac_people ADD COLUMN logging_id TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic D(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "ALTER TABLE people ADD COLUMN logging_id2 TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE people ADD COLUMN logging_id3 TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE people ADD COLUMN logging_id4 TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE people ADD COLUMN logging_id5 TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE emails ADD COLUMN logging_id2 TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE emails ADD COLUMN logging_id3 TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE emails ADD COLUMN logging_id4 TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE emails ADD COLUMN logging_id5 TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE ac_people ADD COLUMN logging_id2 TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE ac_people ADD COLUMN logging_id3 TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE ac_people ADD COLUMN logging_id4 TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE ac_people ADD COLUMN logging_id5 TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE ac_item ADD COLUMN logging_id2 TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE ac_item ADD COLUMN logging_id3 TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE ac_item ADD COLUMN logging_id4 TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE ac_item ADD COLUMN logging_id5 TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic E(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "ALTER TABLE people ADD COLUMN mobile_owner_id TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 2259
    :try_start_0
    const-string v0, "SELECT last_sync_status FROM owners WHERE account_name=? AND page_gaia_id IS NULL"

    invoke-static {p1}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    long-to-int v0, v0

    .line 2266
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/people/c/f;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;
    .locals 1

    .prologue
    .line 734
    invoke-static {p0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->d()Lcom/google/android/gms/people/c/f;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "DELETE FROM sync_tokens;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/u;->a()V

    return-void
.end method

.method static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2012
    const-string v0, "DELETE FROM people WHERE in_contacts=0 AND NOT EXISTS (SELECT * FROM circle_members AS cm WHERE cm.owner_id = people.owner_id AND cm.qualified_id = people.qualified_id)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2018
    return-void
.end method

.method static synthetic a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V
    .locals 12

    .prologue
    .line 76
    const-string v0, "PeopleDatabaseHelper"

    const-string v1, "Fixing sync"

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/people/debug/c;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/people/service/o;->b(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    iget-object v0, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/gms/people/c/f;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v5

    const-string v0, "com.android.contacts"

    invoke-static {v4, v0}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v6

    const-string v0, "com.google.android.gms.people"

    invoke-static {v4, v0}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v7

    const-string v0, "PeopleDatabaseHelper"

    const-string v8, "Last status=%d  people sync=%s  focus sync=%s"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {p1, v0, v4, v8}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/people/a/a;->ac:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    if-ne v5, v0, :cond_0

    if-eqz v7, :cond_0

    const-string v0, "PeopleDatabaseHelper"

    const-string v5, "Last=failure"

    invoke-static {p1, v0, v4, v5}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/String;)V

    const-string v0, "com.google.android.gms.people"

    const/4 v5, 0x0

    const/4 v8, 0x0

    invoke-static {v5, v8}, Lcom/google/android/gms/people/sync/u;->a(ZZ)Landroid/os/Bundle;

    move-result-object v5

    invoke-static {v4, v0, v5}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    sget-object v0, Lcom/google/android/gms/people/a/a;->ab:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v6, :cond_1

    if-nez v7, :cond_1

    const-string v0, "PeopleDatabaseHelper"

    const-string v5, "Re-enabling"

    invoke-static {p1, v0, v4, v5}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/String;)V

    const-string v0, "com.google.android.gms.people"

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2410
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2411
    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2412
    const-string v1, "value"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2413
    const-string v1, "properties"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2414
    return-void
.end method

.method static synthetic a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Locale;)V
    .locals 0

    .prologue
    .line 76
    invoke-static {p0, p1}, Lcom/google/android/gms/people/c/f;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Locale;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/people/c/f;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->d:Lcom/google/android/gms/people/c/g;

    invoke-static {p1}, Lcom/google/android/gms/people/c/g;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->d:Lcom/google/android/gms/people/c/g;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->d:Lcom/google/android/gms/people/c/g;

    invoke-static {p1}, Lcom/google/android/gms/people/c/g;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public static a()Z
    .locals 3

    .prologue
    .line 725
    const-string v0, "1"

    const-string v1, "gms.people.read_only"

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/google/android/gms/common/util/av;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 727
    if-eqz v0, :cond_0

    .line 728
    const-string v1, "PeopleDatabaseHelper"

    const-string v2, "gms.people.read_only is set.  Some features are disabled."

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 730
    :cond_0
    return v0
.end method

.method private a(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 3593
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 3595
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 3597
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return v1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static b(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;
    .locals 3

    .prologue
    .line 738
    new-instance v0, Lcom/google/android/gms/people/c/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/c/f;-><init>(Landroid/content/Context;)V

    .line 739
    invoke-static {}, Lcom/google/android/gms/people/c/f;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 741
    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {p0, v1}, Lcom/google/android/gms/people/c/f;->b(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)Z

    .line 742
    const-string v1, "gcoreVersion"

    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->f(Landroid/content/Context;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->a(Landroid/content/Context;)V

    .line 746
    return-object v0
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 779
    const-string v0, "pluscontacts.db"

    return-object v0
.end method

.method static b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 2024
    const-string v0, "SELECT sql FROM sqlite_master WHERE name=\'owners\' AND type=\'table\'"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2026
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2027
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2028
    const-string v1, "cover_photo_id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2029
    const-string v0, "ALTER TABLE owners ADD COLUMN cover_photo_url TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2030
    const-string v0, "ALTER TABLE owners ADD COLUMN  cover_photo_height INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2032
    const-string v0, "ALTER TABLE owners ADD COLUMN  cover_photo_width INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2034
    const-string v0, "ALTER TABLE owners ADD COLUMN cover_photo_id TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2036
    :cond_0
    return-void
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Locale;)V
    .locals 2

    .prologue
    .line 2417
    const-string v0, "dbLocale"

    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/people/c/f;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2418
    return-void
.end method

.method private static b(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 3212
    const-string v0, "PeopleDatabaseHelper"

    const-string v2, "addNewAccounts"

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3216
    invoke-static {p0}, Lcom/google/android/gms/people/service/o;->a(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    .line 3218
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3221
    :try_start_0
    const-string v2, "SELECT DISTINCT account_name FROM owners"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 3226
    const/4 v3, -0x1

    :try_start_1
    invoke-interface {v2, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 3227
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3228
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 3230
    invoke-interface {v0, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3233
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 3253
    :catchall_1
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 3233
    :cond_0
    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 3238
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 3239
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3240
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 3241
    const-string v5, "account_name"

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3242
    const-string v5, "owners"

    const/4 v6, 0x0

    invoke-virtual {p1, v5, v6, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 3244
    add-int/lit8 v2, v2, 0x1

    .line 3246
    const-string v5, "PeopleDatabaseHelper"

    const/4 v6, 0x0

    const-string v7, "Account added"

    invoke-static {p0, v5, v0, v6, v7}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3248
    const-string v5, "PeopleDatabaseHelper"

    const-string v6, "Account added"

    const/4 v7, 0x0

    invoke-static {v5, v6, v0, v7}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 3251
    :cond_1
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 3253
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3255
    const-string v0, "PeopleDatabaseHelper"

    const-string v3, "addNewAccounts: done"

    invoke-static {v0, v3}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3257
    if-lez v2, :cond_2

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method static c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2039
    const-string v0, "ALTER TABLE owners ADD COLUMN last_full_people_sync_time INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2041
    return-void
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 2734
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2735
    const-string v0, "circleId"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 2736
    const-string v0, "qualifiedId"

    invoke-static {p4, v0}, Lcom/google/android/gms/people/internal/at;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2738
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2740
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v1

    const-string v2, "SELECT count(1) FROM circle_members WHERE owner_id=? AND circle_id=? AND qualified_id=?"

    invoke-static {v0, p3, p4}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static d(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2249
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2250
    const-string v1, "last_sync_status"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2251
    const-string v1, "last_successful_sync_time"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2253
    const-string v1, "owners"

    invoke-virtual {p0, v1, v0, v3, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2254
    return-void
.end method

.method static synthetic e(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "UPDATE owners SET gaia_id=(SELECT gaia_id FROM owners AS i WHERE owners.account_name = i.account_name ) WHERE page_gaia_id IS NOT NULL"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2448
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2449
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2451
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v0

    .line 2452
    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 2454
    iget-object v1, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2456
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "UPDATE people SET etag=?1 ,last_modified=?2 WHERE (owner_id=?3) AND ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/people/internal/at;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/people/c/f;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v4}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2466
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/service/h;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2468
    return-void
.end method

.method static synthetic f(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "DROP TABLE gaia_id_map;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE gaia_id_map (owner_id INTEGER NOT NULL,contact_id TEXT NOT NULL,value TEXT NOT NULL,gaia_id TEXT NOT NULL,UNIQUE (owner_id, contact_id, value),FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2477
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2479
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "EXISTS (SELECT 1 FROM circle_members AS cm WHERE (cm.circle_id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p3}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AND(cm.qualified_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "people.qualified_id))"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/people/c/f;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2485
    return-void
.end method

.method static synthetic g(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "DROP TABLE IF EXISTS application_packages;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS facl_applications;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS facl_circles;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE application_packages (_id INTEGER PRIMARY KEY AUTOINCREMENT,dev_console_id TEXT NOT NULL,package_name TEXT NOT NULL,certificate_hash TEXT NOT NULL,UNIQUE (package_name));"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE facl_people (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,dev_console_id TEXT NOT NULL,qualified_id TEXT NOT NULL,UNIQUE (owner_id, dev_console_id, qualified_id),FOREIGN KEY (owner_id, qualified_id) REFERENCES people(owner_id, qualified_id));"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE facl_applications (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,dev_console_id TEXT NOT NULL,everyone INTEGER NOT NULL,UNIQUE (owner_id, dev_console_id),FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private g(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2549
    .line 2550
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2552
    const-string p3, ""

    move v0, v1

    .line 2554
    :goto_0
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    if-eqz p3, :cond_1

    move v3, v1

    :goto_1
    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/people/c/e;->a()V

    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v3, p1, p2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "UPDATE people SET in_circle=(  CASE WHEN EXISTS( SELECT 1 FROM circle_members cm WHERE cm.qualified_id=people.qualified_id AND cm.owner_id=people.owner_id ) THEN 1 ELSE 0 END)  WHERE (?2 OR qualified_id=?3) AND owner_id=?1"

    if-eqz v0, :cond_2

    const-string v3, "1"

    :goto_2
    invoke-static {v5, v3, p3}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v6, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v4}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v4, v2}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 2555
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    if-eqz p3, :cond_3

    :goto_3
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->a()V

    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "DELETE FROM people WHERE in_circle=0  AND in_contacts=0 AND (?2 OR qualified_id=?3) AND owner_id=?1"

    if-eqz v0, :cond_4

    const-string v1, "1"

    :goto_4
    invoke-static {v4, v1, p3}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v5, v1}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v3, v2}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 2557
    if-nez v0, :cond_0

    .line 2558
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/gms/people/internal/at;->k(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "people.qualified_id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p3}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/people/c/f;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2560
    :cond_0
    return-void

    :cond_1
    move v3, v2

    .line 2554
    goto :goto_1

    :cond_2
    :try_start_2
    const-string v3, "0"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {v4, v2}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v0

    :cond_3
    move v1, v2

    .line 2555
    goto :goto_3

    :cond_4
    :try_start_3
    const-string v1, "0"
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_4

    :catchall_1
    move-exception v0

    invoke-virtual {v3, v2}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v0

    :cond_5
    move v0, v2

    goto/16 :goto_0
.end method

.method static synthetic h(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "DROP TABLE IF EXISTS applications;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS application_packages;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS facl_applications;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS facl_people;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE applications (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,dev_console_id TEXT NOT NULL,UNIQUE (owner_id, dev_console_id),FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE application_packages (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,dev_console_id TEXT NOT NULL,package_name TEXT NOT NULL,certificate_hash TEXT NOT NULL,FOREIGN KEY (owner_id, dev_console_id) REFERENCES applications(owner_id, dev_console_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE facl_people (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,dev_console_id TEXT NOT NULL,qualified_id TEXT NOT NULL,UNIQUE (owner_id, dev_console_id, qualified_id) ON CONFLICT IGNORE,FOREIGN KEY (owner_id, qualified_id) REFERENCES people(owner_id, qualified_id) ON DELETE CASCADE,FOREIGN KEY (owner_id, dev_console_id) REFERENCES applications(owner_id, dev_console_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private h(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 2688
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2689
    const-string v0, "circleId"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 2691
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2693
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v1

    const-string v2, "SELECT count(1) FROM circles WHERE owner_id=? AND circle_id=?"

    invoke-static {v0, p3}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic i(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "ALTER TABLE people ADD COLUMN invisible_3p INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic j(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "DELETE FROM sync_tokens WHERE name = \'people\';"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DELETE FROM sync_tokens WHERE name = \'gaiamap\';"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DELETE FROM sync_tokens WHERE name = \'autocomplete\';"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic k(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "ALTER TABLE owners ADD COLUMN is_dasher INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE owners ADD COLUMN dasher_domain TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE people ADD COLUMN in_viewer_domain INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic l(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "DROP TABLE IF EXISTS postal_address;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE postal_address (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,qualified_id TEXT NOT NULL,postal_address TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,FOREIGN KEY (owner_id, qualified_id) REFERENCES people(owner_id, qualified_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX IF NOT EXISTS postal_address_person ON postal_address (owner_id, qualified_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic m(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "DELETE FROM people WHERE name IS NULL;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic n(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "ALTER TABLE people ADD COLUMN name_verified INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic o(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "ALTER TABLE owners ADD COLUMN etag TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE people ADD COLUMN in_circle INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "UPDATE people SET in_circle=1;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE people ADD COLUMN in_contacts INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE owner_emails (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,email TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE owner_phones (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,phone TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE owner_postal_address (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,postal_address TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic p(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "CREATE TABLE owner_sync_requests (_id INTEGER PRIMARY KEY AUTOINCREMENT,account_name TEXT NOT NULL,page_gaia_id TEXT,sync_requested_time INTEGER NOT NULL DEFAULT 0,UNIQUE (account_name, page_gaia_id));"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private q()V
    .locals 2

    .prologue
    .line 3159
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    iget-object v1, v0, Lcom/google/android/gms/people/c/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    iget-object v0, v0, Lcom/google/android/gms/people/c/b;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 3163
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/service/h;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/service/h;->b()V

    .line 3164
    return-void
.end method

.method static synthetic q(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "ALTER TABLE owners ADD COLUMN sync_circles_to_contacts INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE owners ADD COLUMN sync_evergreen_to_contacts INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic r(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "DELETE from people WHERE in_circle=0 AND in_contacts=0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private r()Z
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3285
    const-string v0, "PeopleDatabaseHelper"

    const-string v3, "removeStaleOwners"

    invoke-static {v0, v3}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3287
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v3

    .line 3289
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 3291
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/service/o;->a(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    .line 3292
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 3294
    iget-object v6, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/gms/people/service/h;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;

    move-result-object v6

    .line 3297
    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->a()V

    .line 3300
    :try_start_0
    const-string v7, "SELECT _id,account_name,page_gaia_id FROM owners"

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v7

    .line 3307
    const/4 v8, -0x1

    :try_start_1
    invoke-interface {v7, v8}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 3308
    :cond_0
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 3309
    const/4 v8, 0x0

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 3310
    const/4 v10, 0x1

    invoke-interface {v7, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 3311
    const/4 v11, 0x2

    invoke-interface {v7, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 3313
    invoke-interface {v0, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 3314
    const/4 v11, 0x4

    invoke-static {v11}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    .line 3317
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3321
    invoke-interface {v5, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3324
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 3353
    :catchall_1
    move-exception v0

    invoke-virtual {v3, v2}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v0

    .line 3324
    :cond_1
    :try_start_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 3327
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 3328
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3329
    iget-object v7, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    const-string v8, "PeopleDatabaseHelper"

    const/4 v9, 0x0

    const-string v10, "Account removed"

    invoke-static {v7, v8, v0, v9, v10}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3331
    const-string v7, "PeopleDatabaseHelper"

    const-string v8, "Account removed"

    const/4 v9, 0x0

    invoke-static {v7, v8, v0, v9}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 3332
    iget-object v7, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v7}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/people/ah;->a()Lcom/google/android/gms/people/a/c;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/google/android/gms/people/a/c;->c(Ljava/lang/String;)V

    .line 3333
    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v6, v0, v7, v8}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1

    .line 3336
    :cond_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 3337
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "Removing owner: "

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3338
    const-string v7, "PeopleDatabaseHelper"

    invoke-static {v7, v0}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3339
    iget-object v7, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    const-string v10, "PeopleDatabaseHelper"

    invoke-static {v7, v10, v0}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3340
    const-string v0, "DELETE FROM owners WHERE _id=?"

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v0, v7}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3344
    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->b()Z

    goto :goto_2

    .line 3348
    :cond_3
    const-string v0, "contactsCleanupPending"

    const/4 v5, 0x1

    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Z)V

    .line 3351
    :cond_4
    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 3353
    invoke-virtual {v3, v2}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 3355
    invoke-virtual {v6}, Lcom/google/android/gms/people/service/h;->b()V

    .line 3357
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    .line 3358
    :goto_3
    const/4 v1, 0x3

    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 3359
    const-string v1, "PeopleDatabaseHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removeStaleOwners: done: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3362
    :cond_5
    return v0

    :cond_6
    move v0, v2

    .line 3357
    goto :goto_3
.end method

.method static synthetic s(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "ALTER TABLE gaia_id_map ADD COLUMN type INTEGER;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "UPDATE gaia_id_map SET type = (CASE WHEN value LIKE \'%@%\' then 1 ELSE 2 END);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE new_table ( owner_id INTEGER NOT NULL, contact_id TEXT NOT NULL, value TEXT NOT NULL, gaia_id TEXT NOT NULL, type INTEGER NOT NULL, UNIQUE (owner_id, contact_id, value), FOREIGN KEY (owner_id) REFERENCES owners (_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "INSERT INTO new_table SELECT * FROM gaia_id_map;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE gaia_id_map;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE new_table RENAME TO gaia_id_map;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DELETE FROM sync_tokens WHERE name = \'gaiamap\';"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private s()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3372
    const-string v0, "PeopleDatabaseHelper"

    const-string v3, "cleanupEvergreenPeople"

    invoke-static {v0, v3}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3374
    sget-object v0, Lcom/google/android/gms/people/a/a;->y:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 3396
    :goto_0
    return v0

    .line 3378
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v3

    .line 3379
    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->a()V

    .line 3383
    :try_start_0
    const-string v0, "people"

    const-string v4, "in_circle=0"

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v4, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 3384
    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3386
    invoke-virtual {v3, v1}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 3388
    if-lez v0, :cond_1

    .line 3389
    const-string v3, "contactsCleanupPending"

    invoke-virtual {p0, v3, v2}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Z)V

    .line 3391
    :cond_1
    const/4 v3, 0x3

    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 3392
    const-string v3, "PeopleDatabaseHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "cleanupEvergreenPeople: evergreen people deleted: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3396
    :cond_2
    if-lez v0, :cond_3

    move v0, v2

    goto :goto_0

    .line 3386
    :catchall_0
    move-exception v0

    invoke-virtual {v3, v1}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v0

    :cond_3
    move v0, v1

    .line 3396
    goto :goto_0
.end method

.method static synthetic t(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "DELETE FROM owners WHERE account_name=\'\';"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private t()Z
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3406
    const-string v0, "PeopleDatabaseHelper"

    const-string v3, "cleanUpNonGplusAccounts"

    invoke-static {v0, v3}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3408
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v3

    .line 3410
    invoke-direct {p0}, Lcom/google/android/gms/people/c/f;->u()Ljava/util/List;

    move-result-object v4

    .line 3412
    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->a()V

    .line 3414
    :try_start_0
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3415
    const-string v0, "PeopleDatabaseHelper"

    const-string v5, "Detected G+ -> non-G+ owner(s).  Cleanining up..."

    invoke-static {v0, v5}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3417
    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    .line 3419
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 3420
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3421
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Scrubbing account: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 3422
    const-string v9, "PeopleDatabaseHelper"

    invoke-static {v9, v8}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3423
    iget-object v9, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    const-string v10, "PeopleDatabaseHelper"

    const/4 v11, 0x0

    invoke-static {v9, v10, v0, v11, v8}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3424
    const/4 v8, 0x0

    aput-object v0, v5, v8

    .line 3426
    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->b()Z

    .line 3429
    const-string v8, "DELETE FROM sync_tokens WHERE owner_id IN(SELECT _id FROM owners WHERE account_name=?  AND page_gaia_id IS NULL)  AND name !=\'gaiamap\'"

    invoke-virtual {v3, v8, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3437
    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->b()Z

    .line 3440
    const-string v8, "DELETE FROM people WHERE owner_id IN(SELECT _id FROM owners WHERE account_name=?  AND page_gaia_id IS NULL) "

    invoke-virtual {v3, v8, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3445
    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->b()Z

    .line 3447
    const-string v8, "DELETE FROM ac_people WHERE owner_id IN(SELECT _id FROM owners WHERE account_name=?  AND page_gaia_id IS NULL) "

    invoke-virtual {v3, v8, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3453
    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->b()Z

    .line 3456
    const-string v8, "DELETE FROM circles WHERE owner_id IN(SELECT _id FROM owners WHERE account_name=?  AND page_gaia_id IS NULL) "

    invoke-virtual {v3, v8, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3462
    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->b()Z

    .line 3467
    invoke-virtual {v6}, Landroid/content/ContentValues;->clear()V

    .line 3468
    const-string v8, "page_gaia_id"

    invoke-virtual {v6, v8}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 3469
    const-string v8, "sync_to_contacts"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3470
    const-string v8, "sync_circles_to_contacts"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3471
    const-string v8, "sync_evergreen_to_contacts"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3473
    const-string v8, "owners"

    const-string v9, "account_name=? AND page_gaia_id IS NULL"

    invoke-virtual {v3, v8, v6, v9, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3478
    iget-object v8, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v8}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v8

    const-string v9, "CleanupNonG+"

    invoke-virtual {v8, v0, v9}, Lcom/google/android/gms/people/sync/u;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3481
    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 3490
    :catchall_0
    move-exception v0

    invoke-virtual {v3, v2}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v0

    .line 3485
    :cond_0
    :try_start_1
    const-string v0, "contactsCleanupPending"

    const/4 v5, 0x1

    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Z)V

    .line 3488
    :cond_1
    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3490
    invoke-virtual {v3, v2}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 3493
    const-string v0, "PeopleDatabaseHelper"

    const-string v3, "cleanUpNonGplusAccounts done."

    invoke-static {v0, v3}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3495
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    return v0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method private u()Ljava/util/List;
    .locals 6

    .prologue
    .line 3507
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3510
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v1

    const-string v2, "SELECT DISTINCT account_name FROM owners WHERE page_gaia_id IS NULL"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 3516
    const/4 v2, -0x1

    :try_start_0
    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 3517
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3518
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 3519
    const/4 v3, 0x3

    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3520
    const-string v3, "PeopleDatabaseHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Account="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3524
    :cond_1
    invoke-virtual {p0, v2}, Lcom/google/android/gms/people/c/f;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 3525
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3526
    const-string v2, "PeopleDatabaseHelper"

    const-string v3, "  Has no people"

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3541
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 3532
    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v2

    .line 3533
    iget-object v3, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v3, v2}, Lcom/google/android/gms/people/service/o;->a(Landroid/content/Context;Landroid/accounts/Account;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 3534
    const/4 v3, 0x3

    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 3535
    const-string v3, "PeopleDatabaseHelper"

    const-string v4, "  G+ disabled"

    invoke-static {v3, v4}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3537
    :cond_3
    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3541
    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3543
    return-object v0
.end method

.method static synthetic u(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "ALTER TABLE people ADD COLUMN affinity1 REAL;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE people ADD COLUMN affinity2 REAL;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE people ADD COLUMN affinity3 REAL;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE people ADD COLUMN affinity4 REAL;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE people ADD COLUMN affinity5 REAL;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE people ADD COLUMN people_in_common TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic v(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "DROP INDEX IF EXISTS ac_item_container_person_id;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS ac_item_container;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS ac_index_1;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS ac_index_item_id;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS ac_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS ac_item;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS ac_container;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS ac_people;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE ac_people (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,people_v2_id TEXT NOT NULL,qualified_id TEXT,sync_is_alive INTEGER NOT NULL DEFAULT 0,affinity1 REAL,affinity2 REAL,affinity3 REAL,affinity4 REAL,affinity5 REAL,UNIQUE (owner_id,people_v2_id),FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE ac_container (_id INTEGER PRIMARY KEY AUTOINCREMENT,people_id INTEGER NOT NULL,container_type INTEGER NOT NULL,profile_type INTEGER NOT NULL,gaia_id TEXT,contact_id TEXT,compressed_avatar_url TEXT,has_avatar INTEGER NOT NULL DEFAULT 0,in_circle INTEGER NOT NULL DEFAULT 0,in_viewer_domain INTEGER NOT NULL DEFAULT 0,display_name TEXT,formatted_name TEXT,given_name TEXT,family_name TEXT,middle_name TEXT,honorific_prefix TEXT,honorific_suffix TEXT,yomi_given_name TEXT,yomi_family_name TEXT,yomi_honorific_prefix TEXT,yomi_honorific_suffix TEXT,nickname TEXT,FOREIGN KEY (people_id) REFERENCES ac_people(_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE ac_item (_id INTEGER PRIMARY KEY AUTOINCREMENT,container_id INTEGER NOT NULL,item_type INTEGER NOT NULL,is_edge_key INTEGER,value TEXT NOT NULL,value2 TEXT,value_type INTEGER,custom_label TEXT,affinity1 REAL,affinity2 REAL,affinity3 REAL,affinity4 REAL,affinity5 REAL,FOREIGN KEY (container_id) REFERENCES ac_container(_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE ac_index (item_id INTEGER NOT NULL,owner_id INTEGER NOT NULL,is_normalized INTEGER NOT NULL,kind INTEGER NOT NULL,value TEXT NOT NULL COLLATE NOCASE,FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE,FOREIGN KEY (item_id) REFERENCES ac_item(_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX ac_index_1 ON ac_index (owner_id,value,kind);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX ac_index_item_id ON ac_index (item_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX ac_item_container ON ac_item (container_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX ac_item_container_person_id ON ac_container (people_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX IF NOT EXISTS ac_people_v2_id ON ac_people (owner_id,people_v2_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic w(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "DROP TABLE IF EXISTS temp_gaia_ordinal;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE temp_gaia_ordinal (ordinal INTEGER NOT NULL,gaia_id TEXT NOT NULL);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic x(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "DROP TABLE IF EXISTS temp_gaia_ordinal;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE temp_gaia_ordinal (ordinal INTEGER NOT NULL,qualified_id TEXT NOT NULL);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic y(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "DELETE FROM sync_tokens WHERE name = \'me\';"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic z(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 76
    const-string v0, "DROP TABLE IF EXISTS temp_gaia_ordinal;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE temp_gaia_ordinal (ordinal INTEGER NOT NULL,gaia_id TEXT NULL,qualified_id TEXT NOT NULL);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)I
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2965
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2966
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2968
    if-nez p4, :cond_0

    if-nez p5, :cond_0

    .line 3010
    :goto_0
    return v0

    .line 2975
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2976
    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2978
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v4

    .line 2979
    invoke-virtual {v4}, Lcom/google/android/gms/people/c/e;->a()V

    .line 2981
    :try_start_0
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 2982
    if-eqz p4, :cond_1

    .line 2983
    const-string v2, "name"

    invoke-virtual {v5, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2985
    :cond_1
    if-eqz p5, :cond_2

    .line 2986
    const-string v6, "for_sharing"

    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v0

    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2988
    :cond_2
    const-string v2, "last_modified"

    iget-object v6, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/gms/people/internal/at;->a(Landroid/content/Context;)Lcom/google/android/gms/common/util/p;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v2, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2990
    const-string v2, "circles"

    const-string v6, "owner_id=? AND circle_id=? AND type=-1"

    invoke-static {v3, p3}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v2, v5, v6, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 3001
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/people/c/f;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3003
    iget-object v3, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/people/service/h;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;

    move-result-object v3

    .line 3005
    const/4 v5, 0x2

    invoke-virtual {v3, p1, p2, v5}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3006
    invoke-virtual {v4}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3008
    if-lez v2, :cond_4

    .line 3010
    :goto_2
    invoke-virtual {v4, v1}, Lcom/google/android/gms/people/c/e;->a(Z)V

    goto :goto_0

    :cond_3
    move v2, v1

    .line 2986
    goto :goto_1

    :cond_4
    move v0, v1

    .line 3008
    goto :goto_2

    .line 3010
    :catchall_0
    move-exception v0

    invoke-virtual {v4, v1}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2822
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2823
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2824
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2826
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2827
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2829
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/people/c/f;->h(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2871
    :goto_0
    return v0

    .line 2835
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v4

    .line 2836
    invoke-virtual {v4}, Lcom/google/android/gms/people/c/e;->a()V

    .line 2838
    :try_start_0
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 2839
    const-string v3, "owner_id"

    invoke-virtual {v5, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2840
    const-string v0, "circle_id"

    invoke-virtual {v5, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2842
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v1

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2843
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2844
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/people/c/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 2845
    const-string v7, "qualified_id"

    invoke-virtual {v5, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2846
    const-string v7, "circle_members"

    invoke-virtual {v4, v7, v5}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2847
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/people/c/f;->g(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2868
    :catchall_0
    move-exception v0

    invoke-virtual {v4, v1}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v0

    .line 2849
    :cond_1
    :try_start_1
    const-string v7, "PeopleDatabaseHelper"

    const-string v8, "Person %s is already a a member of circle %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    const/4 v0, 0x1

    aput-object p3, v9, v0

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2855
    :cond_2
    const-string v3, "PeopleDatabaseHelper"

    const-string v7, "Failed to add non-existant person %s to circle %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v0, v8, v9

    const/4 v0, 0x1

    aput-object p3, v8, v0

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 2859
    goto :goto_1

    .line 2861
    :cond_3
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/people/c/f;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2863
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/service/h;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;

    move-result-object v0

    const/4 v5, 0x6

    invoke-virtual {v0, p1, p2, v5}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2866
    invoke-virtual {v4}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2868
    invoke-virtual {v4, v1}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 2871
    if-eqz v3, :cond_4

    move v0, v1

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2758
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2759
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2760
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761
    const-string v0, "qualifiedId"

    invoke-static {p3, v0}, Lcom/google/android/gms/people/internal/at;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2763
    const/4 v1, 0x1

    .line 2765
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v2

    .line 2766
    invoke-virtual {v2}, Lcom/google/android/gms/people/c/e;->a()V

    .line 2768
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2769
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2771
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2772
    invoke-virtual {v4}, Landroid/content/ContentValues;->clear()V

    .line 2773
    const-string v0, "owner_id"

    invoke-virtual {v4, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2774
    const-string v0, "qualified_id"

    invoke-virtual {v4, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2777
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2778
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/people/c/f;->h(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2782
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/gms/people/c/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 2783
    const-string v6, "circle_id"

    invoke-virtual {v4, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2784
    const-string v0, "circle_members"

    invoke-virtual {v2, v0, v4}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2814
    :catchall_0
    move-exception v0

    invoke-virtual {v2, v7}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v0

    .line 2787
    :cond_0
    const/4 v0, 0x3

    :goto_1
    move v1, v0

    .line 2789
    goto :goto_0

    .line 2792
    :cond_1
    :try_start_1
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2793
    const-string v5, "circle_members"

    const-string v6, "owner_id=? AND circle_id=? AND qualified_id=?"

    invoke-static {v3, v0, p3}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v5, v6, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_2

    .line 2802
    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/people/c/f;->g(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2804
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/people/c/f;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2806
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/service/h;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;

    move-result-object v0

    const/4 v3, 0x6

    invoke-virtual {v0, p1, p2, v3}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2812
    :goto_3
    invoke-virtual {v2}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2814
    invoke-virtual {v2, v7}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 2816
    return v1

    .line 2809
    :cond_3
    const/4 v1, 0x2

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2376
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v0

    const-string v1, "properties"

    sget-object v2, Lcom/google/android/gms/people/c/f;->l:[Ljava/lang/String;

    const-string v3, "name=?"

    invoke-static {p1}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2379
    const/4 v0, 0x0

    .line 2381
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2382
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2385
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2388
    if-eqz v0, :cond_1

    :goto_0
    return-object v0

    .line 2385
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    move-object v0, p2

    .line 2388
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2673
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2674
    const-string v0, "qualifiedId"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 2676
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2678
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v1

    const-string v2, "SELECT name FROM people WHERE owner_id=? AND qualified_id=?"

    invoke-static {v0, p3}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0, p4}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/people/c/e;)V
    .locals 1

    .prologue
    .line 2328
    const-string v0, "DROP INDEX IF EXISTS ac_people_v2_id;"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;)V

    .line 2329
    const-string v0, "DROP INDEX IF EXISTS ac_item_container_person_id;"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;)V

    .line 2330
    const-string v0, "DROP INDEX IF EXISTS ac_item_container;"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;)V

    .line 2331
    const-string v0, "DROP INDEX IF EXISTS ac_index_1;"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;)V

    .line 2332
    const-string v0, "DROP INDEX IF EXISTS search_person_id_index;"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;)V

    .line 2333
    const-string v0, "DROP INDEX IF EXISTS search_value;"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;)V

    .line 2334
    const-string v0, "DROP INDEX IF EXISTS ac_index_item_id;"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;)V

    .line 2335
    const-string v0, "DROP TABLE IF EXISTS search_index;"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;)V

    .line 2336
    const-string v0, "DROP TABLE IF EXISTS ac_index;"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;)V

    .line 2337
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->d:Lcom/google/android/gms/people/c/g;

    iget-object v0, p1, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0}, Lcom/google/android/gms/people/c/g;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2340
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->d:Lcom/google/android/gms/people/c/g;

    iget-object v0, p1, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0}, Lcom/google/android/gms/people/c/g;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2341
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2396
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2619
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2620
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2623
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v2

    .line 2624
    invoke-virtual {v2}, Lcom/google/android/gms/people/c/e;->a()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2626
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2630
    const-string v3, "people"

    const-string v4, "qualified_id=? AND owner_id=?"

    invoke-static {p3, v1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v4, v1}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2634
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/people/c/f;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2636
    iget-object v1, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/people/service/h;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;

    move-result-object v1

    const/4 v3, 0x6

    invoke-virtual {v1, p1, p2, v3}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2639
    invoke-virtual {v2}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2641
    const/4 v1, 0x0

    :try_start_2
    invoke-virtual {v2, v1}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 2643
    const/4 v0, 0x1

    .line 2646
    :goto_0
    return v0

    .line 2641
    :catchall_0
    move-exception v1

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v1
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0

    .line 2644
    :catch_0
    move-exception v1

    .line 2645
    const-string v2, "PeopleDatabaseHelper"

    const-string v3, "Failed to remove a person"

    invoke-static {v2, v3, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final varargs a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2705
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2706
    const-string v0, "circleId"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2708
    array-length v0, p3

    if-nez v0, :cond_1

    move v1, v2

    .line 2727
    :cond_0
    :goto_0
    return v1

    .line 2712
    :cond_1
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 2713
    const-string v0, "SELECT count(1) FROM circles"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2714
    const-string v0, " WHERE owner_id=? AND ("

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move v0, v1

    .line 2715
    :goto_1
    array-length v4, p3

    if-ge v0, v4, :cond_3

    .line 2716
    if-lez v0, :cond_2

    .line 2717
    const-string v4, " OR "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2719
    :cond_2
    const-string v4, "circle_id=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2715
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2721
    :cond_3
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2723
    array-length v0, p3

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    .line 2724
    iget-object v4, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v4, p1, p2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    .line 2725
    array-length v4, p3

    invoke-static {p3, v1, v0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2727
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v4

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3, v0}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    array-length v0, p3

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)Z
    .locals 2

    .prologue
    .line 2392
    if-eqz p2, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2876
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2877
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2878
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2880
    iget-object v2, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2881
    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2883
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/people/c/f;->h(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2909
    :cond_0
    :goto_0
    return v0

    .line 2887
    :cond_1
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 2888
    const-string v4, "owner_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2889
    const-string v2, "circle_id"

    invoke-virtual {v3, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2890
    const-string v2, "name"

    invoke-virtual {v3, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2891
    const-string v2, "type"

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2892
    const-string v2, "last_modified"

    iget-object v4, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/people/internal/at;->a(Landroid/content/Context;)Lcom/google/android/gms/common/util/p;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2896
    const-string v2, "sort_key"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "p"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2898
    const-string v2, "for_sharing"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2900
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v2

    .line 2901
    const-string v4, "circles"

    invoke-virtual {v2, v4, v3}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 2902
    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 2903
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/service/h;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;

    move-result-object v0

    .line 2905
    const/4 v2, 0x2

    invoke-virtual {v0, p1, p2, v2}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2906
    invoke-virtual {v0}, Lcom/google/android/gms/people/service/h;->b()V

    move v0, v1

    .line 2907
    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2403
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/people/c/f;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2404
    return-void
.end method

.method public final b(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 2421
    if-eqz p2, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2422
    return-void

    .line 2421
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 3173
    iget-object v1, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/ah;->d()Lcom/google/android/gms/people/c/f;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v1, p1, v4}, Lcom/google/android/gms/people/c/b;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    cmp-long v1, v2, v6

    if-ltz v1, :cond_1

    .line 3180
    :cond_0
    :goto_0
    return v0

    .line 3178
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->n()Z

    .line 3180
    iget-object v1, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/ah;->d()Lcom/google/android/gms/people/c/f;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v1, p1, v4}, Lcom/google/android/gms/people/c/b;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    cmp-long v1, v2, v6

    if-gez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 2652
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2653
    const-string v0, "qualifiedId"

    invoke-static {p3, v0}, Lcom/google/android/gms/people/internal/at;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2655
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2657
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v1

    const-string v2, "SELECT count(1) FROM people WHERE owner_id=? AND qualified_id=?"

    invoke-static {v0, p3}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/gms/people/c/e;
    .locals 7

    .prologue
    .line 2349
    iget-object v6, p0, Lcom/google/android/gms/people/c/f;->a:Ljava/lang/Object;

    monitor-enter v6

    .line 2350
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->f:Lcom/google/android/gms/people/c/e;

    if-nez v0, :cond_0

    .line 2351
    new-instance v0, Lcom/google/android/gms/people/c/e;

    iget-object v1, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/people/c/f;->e:Lcom/google/android/gms/people/c/a;

    iget-object v2, p0, Lcom/google/android/gms/people/c/f;->d:Lcom/google/android/gms/people/c/g;

    invoke-virtual {v2}, Lcom/google/android/gms/people/c/g;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const/4 v5, 0x0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/c/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/c/f;Lcom/google/android/gms/people/c/a;Landroid/database/sqlite/SQLiteDatabase;Z)V

    iput-object v0, p0, Lcom/google/android/gms/people/c/f;->f:Lcom/google/android/gms/people/c/e;

    .line 2354
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->f:Lcom/google/android/gms/people/c/e;

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 2355
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2508
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2510
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v1

    .line 2511
    invoke-virtual {v1}, Lcom/google/android/gms/people/c/e;->a()V

    .line 2513
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2515
    const-string v2, "UPDATE circles SET people_count=( SELECT count(1) FROM circle_members AS m WHERE m.owner_id=circles.owner_id AND m.circle_id=circles.circle_id), last_modified=?2 WHERE owner_id=?1 AND type=-1"

    iget-object v3, p0, Lcom/google/android/gms/people/c/f;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v3}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2531
    invoke-virtual {v1}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2533
    invoke-virtual {v1, v6}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 2534
    return-void

    .line 2533
    :catchall_0
    move-exception v0

    invoke-virtual {v1, v6}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2919
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2920
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2922
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2923
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2925
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v1

    .line 2926
    invoke-virtual {v1}, Lcom/google/android/gms/people/c/e;->a()V

    .line 2930
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/people/c/f;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2933
    const-string v2, "DELETE FROM circles WHERE owner_id=? AND circle_id=? AND type=-1"

    invoke-static {v0, p3}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2942
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/people/c/f;->g(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2944
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/service/h;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;

    move-result-object v0

    .line 2946
    const/4 v2, 0x6

    invoke-virtual {v0, p1, p2, v2}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2947
    invoke-virtual {v1}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2949
    invoke-virtual {v1, v3}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 2950
    return-void

    .line 2949
    :catchall_0
    move-exception v0

    invoke-virtual {v1, v3}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v0
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 3553
    const-string v0, "SELECT 1 FROM people WHERE owner_id IN ((SELECT _id FROM owners WHERE account_name=?  AND page_gaia_id IS NULL)  ) LIMIT 1"

    invoke-static {p1}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SELECT 1 FROM ac_people p JOIN ac_container c ON p._id=c.people_id WHERE (p.owner_id IN ((SELECT _id FROM owners WHERE account_name=?  AND page_gaia_id IS NULL)  )) AND (c.container_type!=1) LIMIT 1"

    invoke-static {p1}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lcom/google/android/gms/people/c/e;
    .locals 7

    .prologue
    .line 2359
    iget-object v6, p0, Lcom/google/android/gms/people/c/f;->a:Ljava/lang/Object;

    monitor-enter v6

    .line 2360
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->g:Lcom/google/android/gms/people/c/e;

    if-nez v0, :cond_0

    .line 2361
    new-instance v0, Lcom/google/android/gms/people/c/e;

    iget-object v1, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/people/c/f;->e:Lcom/google/android/gms/people/c/a;

    iget-object v2, p0, Lcom/google/android/gms/people/c/f;->d:Lcom/google/android/gms/people/c/g;

    invoke-virtual {v2}, Lcom/google/android/gms/people/c/g;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const/4 v5, 0x1

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/c/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/c/f;Lcom/google/android/gms/people/c/a;Landroid/database/sqlite/SQLiteDatabase;Z)V

    iput-object v0, p0, Lcom/google/android/gms/people/c/f;->g:Lcom/google/android/gms/people/c/e;

    .line 2364
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->g:Lcom/google/android/gms/people/c/e;

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 2365
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    .line 3606
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3609
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v1

    const-string v2, "SELECT 1 FROM owners WHERE _id=? AND sync_evergreen_to_contacts !=0 AND sync_to_contacts !=0"

    iget-object v3, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v3, p1, p2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3019
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 3020
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 3022
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 3023
    const-string v3, "avatar"

    invoke-static {p3}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3027
    const-string v3, "etag"

    iget-object v4, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/people/internal/at;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3030
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v3

    .line 3031
    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->a()V

    .line 3034
    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    invoke-virtual {v4, p1, p2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3035
    const-string v5, "owners"

    const-string v6, "_id=?"

    invoke-static {v4}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v5, v2, v6, v4}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 3037
    if-ne v2, v0, :cond_0

    .line 3038
    iget-object v2, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/people/service/h;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, p1, p2, v4}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3040
    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3044
    :goto_0
    invoke-virtual {v3, v1}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 3046
    return v0

    .line 3044
    :catchall_0
    move-exception v0

    invoke-virtual {v3, v1}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final e()Lcom/google/android/gms/people/c/b;
    .locals 1

    .prologue
    .line 2369
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->j:Lcom/google/android/gms/people/c/b;

    return-object v0
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 2425
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->k:Ljava/util/concurrent/CountDownLatch;

    .line 2426
    if-eqz v0, :cond_0

    .line 2427
    const-string v1, "PeopleDatabaseHelper"

    const-string v2, "Opening sync latch"

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2428
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 2430
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/people/c/f;->k:Ljava/util/concurrent/CountDownLatch;

    .line 2431
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 2434
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->k:Ljava/util/concurrent/CountDownLatch;

    .line 2435
    if-nez v0, :cond_0

    .line 2440
    :goto_0
    return-void

    .line 2438
    :cond_0
    const-string v1, "PeopleDatabaseHelper"

    const-string v2, "Waiting for sync latch..."

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2439
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    goto :goto_0
.end method

.method public final h()Lcom/google/android/gms/people/c/a/f;
    .locals 1

    .prologue
    .line 3050
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->h:Lcom/google/android/gms/people/c/a/f;

    return-object v0
.end method

.method public final i()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3059
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    .line 3060
    const-string v3, "dbLocale"

    const-string v4, ""

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3062
    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3092
    :goto_0
    return v0

    .line 3066
    :cond_0
    const-string v4, "Updating DB locale: %s -> %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v0

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 3068
    const-string v4, "PeopleDatabaseHelper"

    invoke-static {v4, v3}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3069
    iget-object v4, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    const-string v5, "PeopleDatabaseHelper"

    invoke-static {v4, v5, v3}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3071
    iget-object v3, p0, Lcom/google/android/gms/people/c/f;->h:Lcom/google/android/gms/people/c/a/f;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/people/c/a/f;->a(Ljava/util/Locale;)V

    .line 3074
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v3

    .line 3077
    iget-object v4, v3, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v4, v4, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    iget-object v4, v3, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->setLocale(Ljava/util/Locale;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v4, v3, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v4, v4, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 3079
    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->a()V

    .line 3082
    :try_start_1
    iget-object v4, p0, Lcom/google/android/gms/people/c/f;->h:Lcom/google/android/gms/people/c/a/f;

    invoke-virtual {v4}, Lcom/google/android/gms/people/c/a/f;->a()V

    .line 3084
    iget-object v4, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->c(Landroid/content/Context;)V

    .line 3086
    iget-object v4, v3, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v4, v2}, Lcom/google/android/gms/people/c/f;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Locale;)V

    .line 3088
    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3090
    invoke-virtual {v3, v0}, Lcom/google/android/gms/people/c/e;->a(Z)V

    move v0, v1

    .line 3092
    goto :goto_0

    .line 3077
    :catchall_0
    move-exception v0

    iget-object v1, v3, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v1, v1, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 3090
    :catchall_1
    move-exception v1

    invoke-virtual {v3, v0}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v1
.end method

.method final j()V
    .locals 2

    .prologue
    .line 3121
    const-string v0, "PeopleTx"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3122
    const-string v0, "PeopleDatabaseHelper"

    const-string v1, "onBeginTransaction"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3124
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/people/c/f;->m:I

    .line 3125
    return-void
.end method

.method final k()V
    .locals 2

    .prologue
    .line 3128
    const-string v0, "PeopleTx"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3129
    const-string v0, "PeopleDatabaseHelper"

    const-string v1, "onYieldTransaction"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3131
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/people/c/f;->m:I

    .line 3133
    invoke-direct {p0}, Lcom/google/android/gms/people/c/f;->q()V

    .line 3134
    return-void
.end method

.method final l()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 3137
    const-string v0, "PeopleTx"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3138
    const-string v0, "PeopleDatabaseHelper"

    const-string v1, "onCommitTransaction"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3140
    :cond_0
    iput v2, p0, Lcom/google/android/gms/people/c/f;->m:I

    .line 3142
    invoke-direct {p0}, Lcom/google/android/gms/people/c/f;->q()V

    .line 3143
    return-void
.end method

.method final m()V
    .locals 2

    .prologue
    .line 3146
    const-string v0, "PeopleTx"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3147
    const-string v0, "PeopleDatabaseHelper"

    const-string v1, "onRollbackTransaction"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3149
    :cond_0
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/people/c/f;->m:I

    .line 3151
    invoke-direct {p0}, Lcom/google/android/gms/people/c/f;->q()V

    .line 3152
    return-void
.end method

.method public final n()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3189
    iget-object v0, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0, v1}, Lcom/google/android/gms/people/c/f;->b(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v0

    .line 3191
    if-eqz v0, :cond_0

    .line 3193
    iget-object v1, p0, Lcom/google/android/gms/people/c/f;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/people/service/h;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;

    move-result-object v1

    .line 3195
    const/4 v2, 0x1

    invoke-virtual {v1, v3, v3, v2}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 3196
    invoke-virtual {v1}, Lcom/google/android/gms/people/service/h;->b()V

    .line 3198
    :cond_0
    return v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 3266
    invoke-direct {p0}, Lcom/google/android/gms/people/c/f;->r()Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 3269
    invoke-direct {p0}, Lcom/google/android/gms/people/c/f;->s()Z

    move-result v1

    or-int/2addr v0, v1

    .line 3270
    invoke-direct {p0}, Lcom/google/android/gms/people/c/f;->t()Z

    move-result v1

    or-int/2addr v0, v1

    .line 3272
    return v0
.end method

.method public final p()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3627
    const-string v1, "PeopleDatabaseHelper"

    const-string v2, "clearSyncToContactsFlagsForAllOwners"

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3628
    invoke-virtual {p0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v2

    .line 3630
    invoke-virtual {v2}, Lcom/google/android/gms/people/c/e;->a()V

    .line 3634
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 3635
    const-string v3, "sync_to_contacts"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3636
    const-string v3, "sync_circles_to_contacts"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3637
    const-string v3, "sync_evergreen_to_contacts"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3639
    const-string v3, "owners"

    const-string v4, "page_gaia_id IS NULL AND sync_to_contacts!=0"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 3643
    invoke-virtual {v2}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3645
    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 3647
    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    .line 3645
    :catchall_0
    move-exception v1

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v1
.end method
