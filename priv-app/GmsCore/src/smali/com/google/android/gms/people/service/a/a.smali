.class abstract Lcom/google/android/gms/people/service/a/a;
.super Lcom/google/android/gms/people/service/a;
.source "SourceFile"


# instance fields
.field private final g:Lcom/google/android/gms/people/internal/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/google/android/gms/people/service/a;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V

    .line 23
    iput-object p4, p0, Lcom/google/android/gms/people/service/a/a;->g:Lcom/google/android/gms/people/internal/f;

    .line 24
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/people/service/b;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 72
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    const-string v0, "PeopleService"

    const-string v1, "Delivering Bundle:"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v0, "PeopleService"

    invoke-static {p2}, Lcom/google/android/gms/people/internal/at;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/a;->g:Lcom/google/android/gms/people/internal/f;

    iget v1, p1, Lcom/google/android/gms/people/service/b;->a:I

    iget-object v2, p1, Lcom/google/android/gms/people/service/b;->b:Landroid/os/Bundle;

    invoke-interface {v0, v1, v2, p2}, Lcom/google/android/gms/people/internal/f;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-void

    .line 81
    :catch_0
    move-exception v0

    .line 82
    const-string v1, "PeopleService"

    const-string v2, "Unknown error"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 83
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/google/android/gms/people/service/a;->e:Z

    if-eqz v0, :cond_0

    .line 60
    :goto_0
    return-void

    .line 35
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/people/service/a/a;->f()Landroid/util/Pair;

    move-result-object v1

    .line 37
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/people/service/b;

    .line 38
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 59
    :goto_1
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/service/a/a;->a(Lcom/google/android/gms/people/service/b;Landroid/os/Bundle;)V

    goto :goto_0

    .line 40
    :catch_0
    move-exception v0

    .line 41
    const-string v1, "PeopleService"

    const-string v2, "Bad operation"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/f/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 42
    sget-object v0, Lcom/google/android/gms/people/service/b;->i:Lcom/google/android/gms/people/service/b;

    .line 43
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    goto :goto_1

    .line 45
    :catch_1
    move-exception v0

    .line 48
    iget-object v1, p0, Lcom/google/android/gms/people/service/a/a;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/people/service/a;->d:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/people/f/n;->a(Landroid/content/Context;Ljava/lang/RuntimeException;Ljava/lang/String;)V

    .line 50
    sget-object v0, Lcom/google/android/gms/people/service/b;->h:Lcom/google/android/gms/people/service/b;

    .line 51
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    goto :goto_1

    .line 53
    :catch_2
    move-exception v0

    .line 54
    const-string v1, "PeopleService"

    const-string v2, "Error during operation"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/f/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 55
    iget-object v1, p0, Lcom/google/android/gms/people/service/a/a;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/gms/people/service/a/a;->c()Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1, v0}, Lcom/google/android/gms/people/f/n;->a(Landroid/content/Context;Ljava/lang/Exception;)Lcom/google/android/gms/people/service/b;

    move-result-object v0

    .line 56
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    goto :goto_1
.end method

.method public abstract f()Landroid/util/Pair;
.end method
