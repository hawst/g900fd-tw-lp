.class public final Lcom/google/android/gms/drive/database/model/c;
.super Lcom/google/android/gms/drive/database/model/ad;
.source "SourceFile"


# static fields
.field public static final a:Landroid/net/Uri;


# instance fields
.field private b:Ljava/util/Date;

.field private c:J

.field private d:J

.field private g:Ljava/util/Date;

.field private h:Ljava/util/Date;

.field private i:J

.field private final j:Ljava/lang/String;

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/DatabaseNotificationContentProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/accounts"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/database/model/c;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;)V
    .locals 6

    .prologue
    const-wide v4, 0x7fffffffffffffffL

    const-wide/16 v2, 0x0

    .line 53
    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->a()Lcom/google/android/gms/drive/database/model/d;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/database/model/c;->a:Landroid/net/Uri;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/database/model/ad;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)V

    .line 39
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/c;->b:Ljava/util/Date;

    .line 40
    iput-wide v2, p0, Lcom/google/android/gms/drive/database/model/c;->c:J

    .line 41
    iput-wide v2, p0, Lcom/google/android/gms/drive/database/model/c;->d:J

    .line 42
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/c;->g:Ljava/util/Date;

    .line 43
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/c;->h:Ljava/util/Date;

    .line 47
    iput-wide v2, p0, Lcom/google/android/gms/drive/database/model/c;->i:J

    .line 58
    invoke-virtual {p2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/c;->j:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public static a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/c;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 210
    new-instance v2, Lcom/google/android/gms/drive/database/model/c;

    sget-object v0, Lcom/google/android/gms/drive/database/model/e;->a:Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/e;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/drive/database/model/c;-><init>(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;)V

    .line 213
    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->a()Lcom/google/android/gms/drive/database/model/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/d;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/drive/database/model/c;->d(J)V

    .line 214
    sget-object v0, Lcom/google/android/gms/drive/database/model/e;->b:Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/e;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->e(Landroid/database/Cursor;)Z

    move-result v0

    iput-boolean v0, v2, Lcom/google/android/gms/drive/database/model/c;->k:Z

    .line 216
    sget-object v0, Lcom/google/android/gms/drive/database/model/e;->c:Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/e;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/gms/drive/database/model/c;->c:J

    .line 218
    sget-object v0, Lcom/google/android/gms/drive/database/model/e;->d:Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/e;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 219
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v0}, Lcom/google/android/gms/drive/database/model/c;->a(Ljava/util/Date;)V

    .line 221
    sget-object v0, Lcom/google/android/gms/drive/database/model/e;->h:Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/e;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 223
    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/drive/database/model/c;->b(J)V

    .line 224
    sget-object v0, Lcom/google/android/gms/drive/database/model/e;->e:Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/e;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v3

    .line 225
    if-nez v3, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, v2, Lcom/google/android/gms/drive/database/model/c;->g:Ljava/util/Date;

    .line 227
    sget-object v0, Lcom/google/android/gms/drive/database/model/e;->f:Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/e;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    .line 229
    if-nez v0, :cond_1

    :goto_1
    iput-object v1, v2, Lcom/google/android/gms/drive/database/model/c;->h:Ljava/util/Date;

    .line 231
    sget-object v0, Lcom/google/android/gms/drive/database/model/e;->g:Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/e;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, v2, Lcom/google/android/gms/drive/database/model/c;->i:J

    .line 234
    return-object v2

    .line 225
    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    goto :goto_0

    .line 229
    :cond_1
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    goto :goto_1
.end method


# virtual methods
.method public final a(J)V
    .locals 1

    .prologue
    .line 97
    iput-wide p1, p0, Lcom/google/android/gms/drive/database/model/c;->c:J

    .line 98
    return-void
.end method

.method protected final a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 185
    sget-object v0, Lcom/google/android/gms/drive/database/model/e;->a:Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/e;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/c;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    sget-object v0, Lcom/google/android/gms/drive/database/model/e;->b:Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/e;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/drive/database/model/c;->k:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 187
    sget-object v0, Lcom/google/android/gms/drive/database/model/e;->c:Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/e;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/c;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 188
    sget-object v0, Lcom/google/android/gms/drive/database/model/e;->d:Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/e;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/c;->b:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 189
    sget-object v0, Lcom/google/android/gms/drive/database/model/e;->h:Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/e;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/c;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/c;->g:Ljava/util/Date;

    if-eqz v0, :cond_0

    .line 192
    sget-object v0, Lcom/google/android/gms/drive/database/model/e;->e:Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/e;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/c;->g:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 197
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/c;->h:Ljava/util/Date;

    if-eqz v0, :cond_1

    .line 198
    sget-object v0, Lcom/google/android/gms/drive/database/model/e;->f:Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/e;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/c;->h:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 203
    :goto_1
    sget-object v0, Lcom/google/android/gms/drive/database/model/e;->g:Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/e;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/c;->i:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 204
    return-void

    .line 195
    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/database/model/e;->e:Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/e;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0

    .line 201
    :cond_1
    sget-object v0, Lcom/google/android/gms/drive/database/model/e;->f:Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/e;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 77
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/c;->b:Ljava/util/Date;

    .line 79
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/c;->k:Z

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/model/c;->k:Z

    .line 67
    return-void
.end method

.method public final b(J)V
    .locals 3

    .prologue
    .line 108
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 109
    iput-wide p1, p0, Lcom/google/android/gms/drive/database/model/c;->d:J

    .line 110
    return-void

    .line 108
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/c;->g:Ljava/util/Date;

    .line 128
    return-void
.end method

.method public final c(J)V
    .locals 1

    .prologue
    .line 163
    iput-wide p1, p0, Lcom/google/android/gms/drive/database/model/c;->i:J

    .line 164
    return-void
.end method

.method public final c(Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/c;->h:Ljava/util/Date;

    .line 150
    return-void
.end method

.method public final c()Z
    .locals 4

    .prologue
    .line 85
    iget-wide v0, p0, Lcom/google/android/gms/drive/database/model/c;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 93
    iget-wide v0, p0, Lcom/google/android/gms/drive/database/model/c;->c:J

    return-wide v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 104
    iget-wide v0, p0, Lcom/google/android/gms/drive/database/model/c;->d:J

    return-wide v0
.end method

.method public final f()Ljava/util/Date;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/c;->h:Ljava/util/Date;

    return-object v0
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 156
    iget-wide v0, p0, Lcom/google/android/gms/drive/database/model/c;->i:J

    return-wide v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/c;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 239
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Account[%s, sqlId=%d%s]"

    const/4 v0, 0x3

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/c;->j:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v0, 0x1

    iget-wide v4, p0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/c;->h:Ljava/util/Date;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ", clipped"

    goto :goto_0
.end method
