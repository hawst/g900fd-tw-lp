.class public final Lcom/google/android/gms/ads/internal/b;
.super Lcom/google/android/gms/ads/internal/client/p;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/a;
.implements Lcom/google/android/gms/ads/internal/g/b;
.implements Lcom/google/android/gms/ads/internal/g/n;
.implements Lcom/google/android/gms/ads/internal/g/p;
.implements Lcom/google/android/gms/ads/internal/k/e;
.implements Lcom/google/android/gms/ads/internal/l;
.implements Lcom/google/android/gms/ads/internal/o/g;
.implements Lcom/google/android/gms/ads/internal/overlay/i;
.implements Lcom/google/android/gms/ads/internal/overlay/n;
.implements Lcom/google/android/gms/ads/internal/purchase/l;
.implements Lcom/google/android/gms/ads/internal/request/b;
.implements Lcom/google/android/gms/ads/internal/request/j;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private a:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

.field private final b:Lcom/google/android/gms/ads/internal/k/a/a;

.field private final c:Lcom/google/android/gms/ads/internal/h;

.field private final d:Lcom/google/android/gms/ads/internal/s;

.field private final e:Lcom/google/android/gms/ads/internal/a/c;

.field private f:Z

.field private final g:Landroid/content/ComponentCallbacks;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Ljava/lang/String;Lcom/google/android/gms/ads/internal/k/a/a;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V
    .locals 1

    .prologue
    .line 301
    new-instance v0, Lcom/google/android/gms/ads/internal/h;

    invoke-direct {v0, p1, p2, p3, p5}, Lcom/google/android/gms/ads/internal/h;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Ljava/lang/String;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V

    invoke-direct {p0, v0, p4}, Lcom/google/android/gms/ads/internal/b;-><init>(Lcom/google/android/gms/ads/internal/h;Lcom/google/android/gms/ads/internal/k/a/a;)V

    .line 302
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/ads/internal/h;Lcom/google/android/gms/ads/internal/k/a/a;)V
    .locals 2

    .prologue
    .line 306
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/client/p;-><init>()V

    .line 282
    new-instance v0, Lcom/google/android/gms/ads/internal/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/internal/c;-><init>(Lcom/google/android/gms/ads/internal/b;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/b;->g:Landroid/content/ComponentCallbacks;

    .line 307
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    .line 308
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/b;->b:Lcom/google/android/gms/ads/internal/k/a/a;

    .line 309
    new-instance v0, Lcom/google/android/gms/ads/internal/s;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/internal/s;-><init>(Lcom/google/android/gms/ads/internal/b;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/b;->d:Lcom/google/android/gms/ads/internal/s;

    .line 311
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/g;->b(Landroid/content/Context;)V

    .line 313
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/o/e;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V

    .line 314
    invoke-static {}, Lcom/google/android/gms/ads/internal/o/e;->a()Lcom/google/android/gms/ads/internal/o/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/o/e;->h()Lcom/google/android/gms/ads/internal/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/b;->e:Lcom/google/android/gms/ads/internal/a/c;

    .line 315
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->g:Landroid/content/ComponentCallbacks;

    invoke-virtual {v0, v1}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 316
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/ads/internal/b;)Lcom/google/android/gms/ads/internal/h;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Landroid/os/Bundle;)Lcom/google/android/gms/ads/internal/request/w;
    .locals 20

    .prologue
    .line 1430
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v7

    .line 1435
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1436
    iget-object v3, v7, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 1441
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 1444
    const/4 v3, 0x0

    .line 1445
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v2, v2, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/internal/g;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1447
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 1448
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/ads/internal/g;->getLocationOnScreen([I)V

    .line 1449
    const/4 v3, 0x0

    aget v5, v2, v3

    .line 1450
    const/4 v3, 0x1

    aget v6, v2, v3

    .line 1453
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/internal/g;->getWidth()I

    move-result v9

    .line 1454
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/internal/g;->getHeight()I

    move-result v10

    .line 1455
    const/4 v2, 0x0

    .line 1456
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v3}, Lcom/google/android/gms/ads/internal/g;->isShown()Z

    move-result v3

    if-eqz v3, :cond_0

    add-int v3, v5, v9

    if-lez v3, :cond_0

    add-int v3, v6, v10

    if-lez v3, :cond_0

    iget v3, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    if-gt v5, v3, :cond_0

    iget v3, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    if-gt v6, v3, :cond_0

    .line 1461
    const/4 v2, 0x1

    .line 1465
    :cond_0
    new-instance v3, Landroid/os/Bundle;

    const/4 v11, 0x5

    invoke-direct {v3, v11}, Landroid/os/Bundle;-><init>(I)V

    .line 1466
    const-string v11, "x"

    invoke-virtual {v3, v11, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1467
    const-string v5, "y"

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1468
    const-string v5, "width"

    invoke-virtual {v3, v5, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1469
    const-string v5, "height"

    invoke-virtual {v3, v5, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1470
    const-string v5, "visible"

    invoke-virtual {v3, v5, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1473
    :cond_1
    invoke-static {}, Lcom/google/android/gms/ads/internal/o/e;->c()Ljava/lang/String;

    move-result-object v9

    .line 1476
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    new-instance v5, Lcom/google/android/gms/ads/internal/o/c;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v6, v6, Lcom/google/android/gms/ads/internal/h;->b:Ljava/lang/String;

    invoke-direct {v5, v9, v6}, Lcom/google/android/gms/ads/internal/o/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v5, v2, Lcom/google/android/gms/ads/internal/h;->m:Lcom/google/android/gms/ads/internal/o/c;

    .line 1477
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->m:Lcom/google/android/gms/ads/internal/o/c;

    iget-object v5, v2, Lcom/google/android/gms/ads/internal/o/c;->c:Ljava/lang/Object;

    monitor-enter v5

    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    iput-wide v10, v2, Lcom/google/android/gms/ads/internal/o/c;->i:J

    iget-object v6, v2, Lcom/google/android/gms/ads/internal/o/c;->a:Lcom/google/android/gms/ads/internal/o/e;

    invoke-static {}, Lcom/google/android/gms/ads/internal/o/e;->d()Lcom/google/android/gms/ads/internal/o/f;

    move-result-object v6

    iget-wide v10, v2, Lcom/google/android/gms/ads/internal/o/c;->i:J

    iget-object v12, v6, Lcom/google/android/gms/ads/internal/o/f;->a:Ljava/lang/Object;

    monitor-enter v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-wide v14, v6, Lcom/google/android/gms/ads/internal/o/f;->d:J

    const-wide/16 v16, -0x1

    cmp-long v2, v14, v16

    if-nez v2, :cond_2

    iput-wide v10, v6, Lcom/google/android/gms/ads/internal/o/f;->d:J

    iget-wide v10, v6, Lcom/google/android/gms/ads/internal/o/f;->d:J

    iput-wide v10, v6, Lcom/google/android/gms/ads/internal/o/f;->c:J

    :goto_1
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/client/AdRequestParcel;->c:Landroid/os/Bundle;

    if-eqz v2, :cond_3

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/client/AdRequestParcel;->c:Landroid/os/Bundle;

    const-string v10, "gw"

    const/4 v11, 0x2

    invoke-virtual {v2, v10, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    const/4 v10, 0x1

    if-ne v2, v10, :cond_3

    monitor-exit v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_2
    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1479
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-static {v2, v0, v9}, Lcom/google/android/gms/ads/internal/o/e;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/o/g;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v12

    .line 1480
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v19

    .line 1481
    new-instance v2, Lcom/google/android/gms/ads/internal/request/w;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v5, v5, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v6, v6, Lcom/google/android/gms/ads/internal/h;->b:Ljava/lang/String;

    sget-object v10, Lcom/google/android/gms/ads/internal/o/e;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v11, v11, Lcom/google/android/gms/ads/internal/h;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v13, v13, Lcom/google/android/gms/ads/internal/h;->t:Ljava/util/List;

    invoke-static {}, Lcom/google/android/gms/ads/internal/o/e;->f()Z

    move-result v15

    iget v0, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v16, v0

    iget v0, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v17, v0

    iget v0, v4, Landroid/util/DisplayMetrics;->density:F

    move/from16 v18, v0

    move-object/from16 v4, p1

    move-object/from16 v14, p2

    invoke-direct/range {v2 .. v19}, Lcom/google/android/gms/ads/internal/request/w;-><init>(Landroid/os/Bundle;Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Landroid/content/pm/PackageInfo;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Landroid/os/Bundle;Ljava/util/List;Landroid/os/Bundle;ZIIFLjava/lang/String;)V

    return-object v2

    .line 1438
    :catch_0
    move-exception v2

    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1477
    :cond_2
    :try_start_4
    iput-wide v10, v6, Lcom/google/android/gms/ads/internal/o/f;->c:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    :try_start_5
    monitor-exit v12

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v2

    monitor-exit v5

    throw v2

    :cond_3
    :try_start_6
    iget v2, v6, Lcom/google/android/gms/ads/internal/o/f;->f:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v6, Lcom/google/android/gms/ads/internal/o/f;->f:I

    monitor-exit v12
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 1306
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to load ad: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 1307
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->h:Lcom/google/android/gms/ads/internal/client/f;

    if-eqz v0, :cond_0

    .line 1309
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->h:Lcom/google/android/gms/ads/internal/client/f;

    invoke-interface {v0, p1}, Lcom/google/android/gms/ads/internal/client/f;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1314
    :cond_0
    :goto_0
    return-void

    .line 1310
    :catch_0
    move-exception v0

    .line 1311
    const-string v1, "Could not call AdListener.onAdFailedToLoad()."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private b(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 1282
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1285
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/gms/ads/internal/g;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1286
    return-void
.end method

.method private b(Z)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 1554
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    if-nez v0, :cond_1

    .line 1555
    const-string v0, "Ad state was null when trying to ping impression URLs."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 1593
    :cond_0
    :goto_0
    return-void

    .line 1558
    :cond_1
    const-string v0, "Pinging Impression URLs."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 1560
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->m:Lcom/google/android/gms/ads/internal/o/c;

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/o/c;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-wide v2, v0, Lcom/google/android/gms/ads/internal/o/c;->j:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    iget-wide v2, v0, Lcom/google/android/gms/ads/internal/o/c;->e:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/ads/internal/o/c;->e:J

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/c;->a:Lcom/google/android/gms/ads/internal/o/e;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/ads/internal/o/e;->a(Lcom/google/android/gms/ads/internal/o/c;)V

    :cond_2
    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/c;->a:Lcom/google/android/gms/ads/internal/o/e;

    invoke-static {}, Lcom/google/android/gms/ads/internal/o/e;->d()Lcom/google/android/gms/ads/internal/o/f;

    move-result-object v0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/f;->a:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget v3, v0, Lcom/google/android/gms/ads/internal/o/f;->e:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lcom/google/android/gms/ads/internal/o/f;->e:I

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1563
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->e:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 1564
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/o/a;->e:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/ads/internal/util/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    .line 1571
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->o:Lcom/google/android/gms/ads/internal/k/d;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->o:Lcom/google/android/gms/ads/internal/k/d;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/k/d;->d:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 1573
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/h;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v4, v4, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v4, v4, Lcom/google/android/gms/ads/internal/o/a;->o:Lcom/google/android/gms/ads/internal/k/d;

    iget-object v5, v4, Lcom/google/android/gms/ads/internal/k/d;->d:Ljava/util/List;

    move v4, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/k/k;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/ads/internal/o/a;Ljava/lang/String;ZLjava/util/List;)V

    .line 1583
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->l:Lcom/google/android/gms/ads/internal/k/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->l:Lcom/google/android/gms/ads/internal/k/c;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/k/c;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1585
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/h;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v4, v4, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v4, v4, Lcom/google/android/gms/ads/internal/o/a;->l:Lcom/google/android/gms/ads/internal/k/c;

    iget-object v5, v4, Lcom/google/android/gms/ads/internal/k/c;->f:Ljava/util/List;

    move v4, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/k/k;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/ads/internal/o/a;Ljava/lang/String;ZLjava/util/List;)V

    goto/16 :goto_0

    .line 1560
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private b(Lcom/google/android/gms/ads/internal/o/a;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1616
    iget-boolean v0, p1, Lcom/google/android/gms/ads/internal/o/a;->k:Z

    if-eqz v0, :cond_6

    .line 1620
    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/a;->m:Lcom/google/android/gms/ads/internal/k/a/d;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/k/a/d;->a()Lcom/google/android/gms/b/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1627
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/g;->getNextView()Landroid/view/View;

    move-result-object v2

    .line 1628
    if-eqz v2, :cond_1

    .line 1629
    instance-of v1, v2, Lcom/google/android/gms/ads/internal/p/a;

    if-eqz v1, :cond_0

    move-object v1, v2

    .line 1630
    check-cast v1, Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/p/a;->destroy()V

    .line 1632
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/internal/g;->removeView(Landroid/view/View;)V

    .line 1636
    :cond_1
    :try_start_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/internal/b;->b(Landroid/view/View;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 1655
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/g;->getChildCount()I

    move-result v0

    if-le v0, v4, :cond_3

    .line 1656
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/g;->showNext()V

    .line 1660
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    if-eqz v0, :cond_5

    .line 1662
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/g;->getNextView()Landroid/view/View;

    move-result-object v0

    .line 1665
    instance-of v1, v0, Lcom/google/android/gms/ads/internal/p/a;

    if-eqz v1, :cond_7

    .line 1666
    check-cast v0, Lcom/google/android/gms/ads/internal/p/a;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/ads/internal/p/a;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V

    .line 1672
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->m:Lcom/google/android/gms/ads/internal/k/a/d;

    if-eqz v0, :cond_5

    .line 1674
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->m:Lcom/google/android/gms/ads/internal/k/a/d;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/k/a/d;->c()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1682
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/ads/internal/g;->setVisibility(I)V

    move v0, v4

    .line 1684
    :goto_3
    return v0

    .line 1621
    :catch_0
    move-exception v0

    .line 1622
    const-string v1, "Could not get View from mediation adapter."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v3

    .line 1623
    goto :goto_3

    .line 1637
    :catch_1
    move-exception v0

    .line 1638
    const-string v1, "Could not add mediation view to view hierarchy."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v3

    .line 1639
    goto :goto_3

    .line 1641
    :cond_6
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/a;->r:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    if-eqz v0, :cond_2

    .line 1646
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    iget-object v1, p1, Lcom/google/android/gms/ads/internal/o/a;->r:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/p/a;->a(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V

    .line 1647
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/g;->removeAllViews()V

    .line 1648
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    iget-object v1, p1, Lcom/google/android/gms/ads/internal/o/a;->r:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget v1, v1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->g:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/g;->setMinimumWidth(I)V

    .line 1649
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    iget-object v1, p1, Lcom/google/android/gms/ads/internal/o/a;->r:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget v1, v1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->d:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/g;->setMinimumHeight(I)V

    .line 1650
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/internal/b;->b(Landroid/view/View;)V

    goto/16 :goto_0

    .line 1667
    :cond_7
    if-eqz v0, :cond_4

    .line 1668
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/ads/internal/g;->removeView(Landroid/view/View;)V

    goto :goto_1

    .line 1676
    :catch_2
    move-exception v0

    const-string v0, "Could not destroy previous mediation adapter."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private v()V
    .locals 2

    .prologue
    .line 1348
    const-string v0, "Ad finished loading."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    .line 1349
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->h:Lcom/google/android/gms/ads/internal/client/f;

    if-eqz v0, :cond_0

    .line 1351
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->h:Lcom/google/android/gms/ads/internal/client/f;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/client/f;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1356
    :cond_0
    :goto_0
    return-void

    .line 1352
    :catch_0
    move-exception v0

    .line 1353
    const-string v1, "Could not call AdListener.onAdLoaded()."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private w()V
    .locals 2

    .prologue
    .line 1602
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    if-eqz v0, :cond_1

    .line 1603
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget v0, v0, Lcom/google/android/gms/ads/internal/h;->x:I

    if-nez v0, :cond_0

    .line 1604
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->destroy()V

    .line 1606
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    .line 1607
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/ads/internal/h;->z:Z

    .line 1609
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 820
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/b;->t()V

    .line 821
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 790
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object p1, v0, Lcom/google/android/gms/ads/internal/h;->w:Landroid/view/View;

    .line 791
    new-instance v0, Lcom/google/android/gms/ads/internal/o/a;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->l:Lcom/google/android/gms/ads/internal/o/b;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/ads/internal/o/a;-><init>(Lcom/google/android/gms/ads/internal/o/b;Lcom/google/android/gms/ads/internal/p/a;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/b;->a(Lcom/google/android/gms/ads/internal/o/a;)V

    .line 798
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V
    .locals 2

    .prologue
    .line 1079
    const-string v0, "setAdSize must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 1081
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object p1, v0, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    .line 1084
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget v0, v0, Lcom/google/android/gms/ads/internal/h;->x:I

    if-nez v0, :cond_0

    .line 1086
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/internal/p/a;->a(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V

    .line 1090
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/g;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 1091
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/g;->getNextView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/g;->removeView(Landroid/view/View;)V

    .line 1093
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    iget v1, p1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->g:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/g;->setMinimumWidth(I)V

    .line 1094
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    iget v1, p1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->d:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/g;->setMinimumHeight(I)V

    .line 1095
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/g;->requestLayout()V

    .line 1096
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/client/c;)V
    .locals 1

    .prologue
    .line 1073
    const-string v0, "setAdListener must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 1074
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object p1, v0, Lcom/google/android/gms/ads/internal/h;->g:Lcom/google/android/gms/ads/internal/client/c;

    .line 1075
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/client/f;)V
    .locals 1

    .prologue
    .line 1067
    const-string v0, "setAdListener must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 1068
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object p1, v0, Lcom/google/android/gms/ads/internal/h;->h:Lcom/google/android/gms/ads/internal/client/f;

    .line 1069
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/client/s;)V
    .locals 1

    .prologue
    .line 1100
    const-string v0, "setAppEventListener must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 1101
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object p1, v0, Lcom/google/android/gms/ads/internal/h;->n:Lcom/google/android/gms/ads/internal/client/s;

    .line 1102
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/e/a/d;)V
    .locals 1

    .prologue
    .line 1114
    const-string v0, "setOnCustomRenderedAdLoadedListener must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 1116
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object p1, v0, Lcom/google/android/gms/ads/internal/h;->s:Lcom/google/android/gms/ads/internal/e/a/d;

    .line 1117
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/f/a/g;)V
    .locals 1

    .prologue
    .line 1135
    const-string v0, "setOnAppInstallAdLoadedListener must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 1137
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object p1, v0, Lcom/google/android/gms/ads/internal/h;->q:Lcom/google/android/gms/ads/internal/f/a/g;

    .line 1138
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/f/a/j;)V
    .locals 1

    .prologue
    .line 1141
    const-string v0, "setOnContentAdLoadedListener must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 1143
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object p1, v0, Lcom/google/android/gms/ads/internal/h;->r:Lcom/google/android/gms/ads/internal/f/a/j;

    .line 1144
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/o/a;)V
    .locals 13

    .prologue
    const/4 v5, 0x3

    const/4 v12, -0x2

    const/4 v7, 0x0

    const-wide/16 v10, -0x1

    const/4 v4, 0x0

    .line 645
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object v7, v0, Lcom/google/android/gms/ads/internal/h;->i:Lcom/google/android/gms/ads/internal/util/a;

    .line 647
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/a;->w:Lcom/google/android/gms/ads/internal/f/d;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v6, v0

    .line 652
    :goto_0
    iget v0, p1, Lcom/google/android/gms/ads/internal/o/a;->d:I

    if-eq v0, v12, :cond_0

    iget v0, p1, Lcom/google/android/gms/ads/internal/o/a;->d:I

    if-eq v0, v5, :cond_0

    .line 654
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->A:Ljava/util/HashSet;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/o/e;->a(Ljava/util/HashSet;)V

    .line 658
    :cond_0
    iget v0, p1, Lcom/google/android/gms/ads/internal/o/a;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    .line 786
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v6, v4

    .line 647
    goto :goto_0

    .line 663
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->a:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->a:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iput-object v7, p0, Lcom/google/android/gms/ads/internal/b;->a:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    move-object v1, v0

    move v0, v4

    :goto_2
    or-int/2addr v0, v6

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v2, v2, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-eqz v2, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget v0, v0, Lcom/google/android/gms/ads/internal/h;->x:I

    if-nez v0, :cond_4

    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/g;->a(Landroid/webkit/WebView;)V

    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->d:Lcom/google/android/gms/ads/internal/s;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/s;->d:Z

    if-eqz v0, :cond_5

    .line 664
    const-string v0, "Ad refresh scheduled."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 669
    :cond_5
    iget v0, p1, Lcom/google/android/gms/ads/internal/o/a;->d:I

    if-ne v0, v5, :cond_6

    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/a;->o:Lcom/google/android/gms/ads/internal/k/d;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/a;->o:Lcom/google/android/gms/ads/internal/k/d;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/k/d;->e:Ljava/util/List;

    if-eqz v0, :cond_6

    .line 672
    const-string v0, "Pinging no fill URLs."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 673
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v3, v2, Lcom/google/android/gms/ads/internal/h;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/ads/internal/o/a;->o:Lcom/google/android/gms/ads/internal/k/d;

    iget-object v5, v2, Lcom/google/android/gms/ads/internal/k/d;->e:Ljava/util/List;

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/k/k;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/ads/internal/o/a;Ljava/lang/String;ZLjava/util/List;)V

    .line 683
    :cond_6
    iget v0, p1, Lcom/google/android/gms/ads/internal/o/a;->d:I

    if-eq v0, v12, :cond_c

    .line 685
    iget v0, p1, Lcom/google/android/gms/ads/internal/o/a;->d:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/internal/b;->a(I)V

    goto :goto_1

    .line 663
    :cond_7
    iget-object v1, p1, Lcom/google/android/gms/ads/internal/o/a;->a:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v0, v1, Lcom/google/android/gms/ads/internal/client/AdRequestParcel;->c:Landroid/os/Bundle;

    if-eqz v0, :cond_8

    iget-object v0, v1, Lcom/google/android/gms/ads/internal/client/AdRequestParcel;->c:Landroid/os/Bundle;

    const-string v2, "_noRefresh"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_2

    :cond_8
    move v0, v4

    goto :goto_2

    :cond_9
    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget v0, v0, Lcom/google/android/gms/ads/internal/h;->x:I

    if-nez v0, :cond_4

    iget-wide v2, p1, Lcom/google/android/gms/ads/internal/o/a;->h:J

    const-wide/16 v8, 0x0

    cmp-long v0, v2, v8

    if-lez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->d:Lcom/google/android/gms/ads/internal/s;

    iget-wide v2, p1, Lcom/google/android/gms/ads/internal/o/a;->h:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/ads/internal/s;->a(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;J)V

    goto :goto_3

    :cond_a
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/a;->o:Lcom/google/android/gms/ads/internal/k/d;

    if-eqz v0, :cond_b

    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/a;->o:Lcom/google/android/gms/ads/internal/k/d;

    iget-wide v2, v0, Lcom/google/android/gms/ads/internal/k/d;->g:J

    const-wide/16 v8, 0x0

    cmp-long v0, v2, v8

    if-lez v0, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->d:Lcom/google/android/gms/ads/internal/s;

    iget-object v2, p1, Lcom/google/android/gms/ads/internal/o/a;->o:Lcom/google/android/gms/ads/internal/k/d;

    iget-wide v2, v2, Lcom/google/android/gms/ads/internal/k/d;->g:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/ads/internal/s;->a(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;J)V

    goto :goto_3

    :cond_b
    iget-boolean v0, p1, Lcom/google/android/gms/ads/internal/o/a;->k:Z

    if-nez v0, :cond_4

    iget v0, p1, Lcom/google/android/gms/ads/internal/o/a;->d:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->d:Lcom/google/android/gms/ads/internal/s;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/s;->a(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)V

    goto/16 :goto_3

    .line 691
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-nez v0, :cond_e

    if-nez v6, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget v0, v0, Lcom/google/android/gms/ads/internal/h;->x:I

    if-nez v0, :cond_e

    .line 693
    invoke-direct {p0, p1}, Lcom/google/android/gms/ads/internal/b;->b(Lcom/google/android/gms/ads/internal/o/a;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 694
    invoke-direct {p0, v4}, Lcom/google/android/gms/ads/internal/b;->a(I)V

    goto/16 :goto_1

    .line 698
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    if-eqz v0, :cond_e

    .line 699
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/g;->a(Lcom/google/android/gms/ads/internal/g;)Lcom/google/android/gms/ads/internal/util/l;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/ads/internal/o/a;->v:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/ads/internal/util/l;->b:Ljava/lang/String;

    .line 704
    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->p:Lcom/google/android/gms/ads/internal/k/f;

    if-eqz v0, :cond_f

    .line 705
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->p:Lcom/google/android/gms/ads/internal/k/f;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/ads/internal/k/f;->a(Lcom/google/android/gms/ads/internal/k/e;)V

    .line 709
    :cond_f
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/a;->p:Lcom/google/android/gms/ads/internal/k/f;

    if-eqz v0, :cond_10

    .line 710
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/a;->p:Lcom/google/android/gms/ads/internal/k/f;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/ads/internal/k/f;->a(Lcom/google/android/gms/ads/internal/k/e;)V

    .line 712
    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->e:Lcom/google/android/gms/ads/internal/a/c;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/a/c;->a(Lcom/google/android/gms/ads/internal/o/a;)V

    .line 714
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object p1, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    .line 717
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->m:Lcom/google/android/gms/ads/internal/o/c;

    iget-wide v2, p1, Lcom/google/android/gms/ads/internal/o/a;->t:J

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/o/c;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-wide v2, v0, Lcom/google/android/gms/ads/internal/o/c;->j:J

    iget-wide v2, v0, Lcom/google/android/gms/ads/internal/o/c;->j:J

    cmp-long v2, v2, v10

    if-eqz v2, :cond_11

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/c;->a:Lcom/google/android/gms/ads/internal/o/e;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/ads/internal/o/e;->a(Lcom/google/android/gms/ads/internal/o/c;)V

    :cond_11
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 718
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->m:Lcom/google/android/gms/ads/internal/o/c;

    iget-wide v2, p1, Lcom/google/android/gms/ads/internal/o/a;->u:J

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/o/c;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-wide v8, v0, Lcom/google/android/gms/ads/internal/o/c;->j:J

    cmp-long v5, v8, v10

    if-eqz v5, :cond_12

    iput-wide v2, v0, Lcom/google/android/gms/ads/internal/o/c;->d:J

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/c;->a:Lcom/google/android/gms/ads/internal/o/e;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/ads/internal/o/e;->a(Lcom/google/android/gms/ads/internal/o/c;)V

    :cond_12
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 719
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->m:Lcom/google/android/gms/ads/internal/o/c;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v1, v1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/c;->c:Ljava/lang/Object;

    monitor-enter v2

    :try_start_2
    iget-wide v8, v0, Lcom/google/android/gms/ads/internal/o/c;->j:J

    cmp-long v3, v8, v10

    if-eqz v3, :cond_13

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    iput-wide v8, v0, Lcom/google/android/gms/ads/internal/o/c;->g:J

    if-nez v1, :cond_13

    iget-wide v8, v0, Lcom/google/android/gms/ads/internal/o/c;->g:J

    iput-wide v8, v0, Lcom/google/android/gms/ads/internal/o/c;->e:J

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/o/c;->a:Lcom/google/android/gms/ads/internal/o/e;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/ads/internal/o/e;->a(Lcom/google/android/gms/ads/internal/o/c;)V

    :cond_13
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 720
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->m:Lcom/google/android/gms/ads/internal/o/c;

    iget-boolean v1, p1, Lcom/google/android/gms/ads/internal/o/a;->k:Z

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/c;->c:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-wide v8, v0, Lcom/google/android/gms/ads/internal/o/c;->j:J

    cmp-long v3, v8, v10

    if-eqz v3, :cond_14

    iput-boolean v1, v0, Lcom/google/android/gms/ads/internal/o/c;->f:Z

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/o/c;->a:Lcom/google/android/gms/ads/internal/o/e;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/ads/internal/o/e;->a(Lcom/google/android/gms/ads/internal/o/c;)V

    :cond_14
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 723
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-nez v0, :cond_15

    if-nez v6, :cond_15

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget v0, v0, Lcom/google/android/gms/ads/internal/h;->x:I

    if-nez v0, :cond_15

    .line 725
    invoke-direct {p0, v4}, Lcom/google/android/gms/ads/internal/b;->b(Z)V

    .line 730
    :cond_15
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->v:Lcom/google/android/gms/ads/internal/o/h;

    if-nez v0, :cond_16

    .line 731
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    new-instance v1, Lcom/google/android/gms/ads/internal/o/h;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->b:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/google/android/gms/ads/internal/o/h;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/google/android/gms/ads/internal/h;->v:Lcom/google/android/gms/ads/internal/o/h;

    .line 736
    :cond_16
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/a;->o:Lcom/google/android/gms/ads/internal/k/d;

    if-eqz v0, :cond_1e

    .line 737
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/a;->o:Lcom/google/android/gms/ads/internal/k/d;

    iget v1, v0, Lcom/google/android/gms/ads/internal/k/d;->h:I

    .line 738
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/a;->o:Lcom/google/android/gms/ads/internal/k/d;

    iget v0, v0, Lcom/google/android/gms/ads/internal/k/d;->i:I

    .line 741
    :goto_4
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->v:Lcom/google/android/gms/ads/internal/o/h;

    iget-object v3, v2, Lcom/google/android/gms/ads/internal/o/h;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_4
    iput v1, v2, Lcom/google/android/gms/ads/internal/o/h;->b:I

    iput v0, v2, Lcom/google/android/gms/ads/internal/o/h;->c:I

    iget-object v0, v2, Lcom/google/android/gms/ads/internal/o/h;->d:Lcom/google/android/gms/ads/internal/o/e;

    iget-object v1, v2, Lcom/google/android/gms/ads/internal/o/h;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/ads/internal/o/e;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/o/h;)V

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 743
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget v0, v0, Lcom/google/android/gms/ads/internal/h;->x:I

    if-nez v0, :cond_1d

    .line 744
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-nez v0, :cond_18

    .line 745
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/o/a;->a()Z

    move-result v0

    if-nez v0, :cond_17

    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/a;->j:Lorg/json/JSONObject;

    if-eqz v0, :cond_18

    .line 747
    :cond_17
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->e:Lcom/google/android/gms/ads/internal/a/c;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/ads/internal/a/c;->a(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/ads/internal/o/a;)Lcom/google/android/gms/ads/internal/a/d;

    move-result-object v0

    .line 749
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/o/a;->a()Z

    move-result v1

    if-eqz v1, :cond_18

    if-eqz v0, :cond_18

    .line 750
    new-instance v1, Lcom/google/android/gms/ads/internal/r;

    iget-object v2, p1, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-direct {v1, v2}, Lcom/google/android/gms/ads/internal/r;-><init>(Lcom/google/android/gms/ads/internal/p/a;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/a/d;->a(Lcom/google/android/gms/ads/internal/a/a;)V

    .line 755
    :cond_18
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    if-eqz v0, :cond_19

    .line 756
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->a()V

    .line 758
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->f()Lcom/google/android/gms/ads/internal/p/c;

    move-result-object v0

    .line 759
    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/c;->c()V

    .line 762
    :cond_19
    if-eqz v6, :cond_1a

    .line 763
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/a;->w:Lcom/google/android/gms/ads/internal/f/d;

    .line 764
    instance-of v1, v0, Lcom/google/android/gms/ads/internal/f/b;

    if-eqz v1, :cond_1b

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->r:Lcom/google/android/gms/ads/internal/f/a/j;

    if-eqz v1, :cond_1b

    .line 766
    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->w:Lcom/google/android/gms/ads/internal/f/d;

    instance-of v0, v0, Lcom/google/android/gms/ads/internal/f/b;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->r:Lcom/google/android/gms/ads/internal/f/a/j;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/h;->r:Lcom/google/android/gms/ads/internal/f/a/j;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->w:Lcom/google/android/gms/ads/internal/f/d;

    check-cast v0, Lcom/google/android/gms/ads/internal/f/b;

    invoke-interface {v1, v0}, Lcom/google/android/gms/ads/internal/f/a/j;->a(Lcom/google/android/gms/ads/internal/f/a/d;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0

    .line 777
    :cond_1a
    :goto_5
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/b;->v()V

    goto/16 :goto_1

    .line 717
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 718
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 719
    :catchall_2
    move-exception v0

    monitor-exit v2

    throw v0

    .line 720
    :catchall_3
    move-exception v0

    monitor-exit v2

    throw v0

    .line 741
    :catchall_4
    move-exception v0

    monitor-exit v3

    throw v0

    .line 766
    :catch_0
    move-exception v0

    const-string v1, "Could not call OnContentAdLoadedListener.onContentAdLoaded()."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 767
    :cond_1b
    instance-of v0, v0, Lcom/google/android/gms/ads/internal/f/a;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->q:Lcom/google/android/gms/ads/internal/f/a/g;

    if-eqz v0, :cond_1c

    .line 769
    :try_start_6
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->w:Lcom/google/android/gms/ads/internal/f/d;

    instance-of v0, v0, Lcom/google/android/gms/ads/internal/f/a;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->q:Lcom/google/android/gms/ads/internal/f/a/g;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/h;->q:Lcom/google/android/gms/ads/internal/f/a/g;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->w:Lcom/google/android/gms/ads/internal/f/d;

    check-cast v0, Lcom/google/android/gms/ads/internal/f/a;

    invoke-interface {v1, v0}, Lcom/google/android/gms/ads/internal/f/a/g;->a(Lcom/google/android/gms/ads/internal/f/a/a;)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_5

    :catch_1
    move-exception v0

    const-string v1, "Could not call OnAppInstallAdLoadedListener.onAppInstallAdLoaded()."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 771
    :cond_1c
    const-string v0, "No matching listener for retrieved native ad template."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 772
    invoke-direct {p0, v4}, Lcom/google/android/gms/ads/internal/b;->a(I)V

    goto/16 :goto_1

    .line 779
    :cond_1d
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->w:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/a;->j:Lorg/json/JSONObject;

    if-eqz v0, :cond_1

    .line 780
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->e:Lcom/google/android/gms/ads/internal/a/c;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/h;->w:Landroid/view/View;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/ads/internal/a/c;->a(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/ads/internal/o/a;Landroid/view/View;)Lcom/google/android/gms/ads/internal/a/d;

    goto/16 :goto_1

    :cond_1e
    move v0, v4

    move v1, v4

    goto/16 :goto_4
.end method

.method public final a(Lcom/google/android/gms/ads/internal/o/b;)V
    .locals 12

    .prologue
    const/4 v6, 0x1

    const/4 v11, 0x0

    const/4 v2, 0x0

    .line 512
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object v11, v0, Lcom/google/android/gms/ads/internal/h;->f:Lcom/google/android/gms/ads/internal/util/a;

    .line 514
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object p1, v0, Lcom/google/android/gms/ads/internal/h;->l:Lcom/google/android/gms/ads/internal/o/b;

    .line 519
    invoke-virtual {p0, v11}, Lcom/google/android/gms/ads/internal/b;->a(Ljava/util/List;)V

    .line 522
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->t:Z

    if-nez v0, :cond_a

    .line 523
    new-instance v10, Lcom/google/android/gms/ads/internal/i;

    invoke-direct {v10}, Lcom/google/android/gms/ads/internal/i;-><init>()V

    .line 524
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v4, v3, Lcom/google/android/gms/ads/internal/h;->d:Lcom/google/android/a/w;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v5, v3, Lcom/google/android/gms/ads/internal/h;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    move v3, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/p/a;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;ZZLcom/google/android/a/w;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)Lcom/google/android/gms/ads/internal/p/a;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/ads/internal/o/e;->i()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_9

    const-string v3, "gads:interstitial_follow_url"

    invoke-virtual {v1, v3, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->f()Lcom/google/android/gms/ads/internal/p/c;

    move-result-object v3

    move-object v4, p0

    move-object v5, p0

    move-object v6, p0

    move-object v8, p0

    move-object v9, p0

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/gms/ads/internal/p/c;->a(Lcom/google/android/gms/ads/internal/a;Lcom/google/android/gms/ads/internal/g/b;Lcom/google/android/gms/ads/internal/overlay/n;ZLcom/google/android/gms/ads/internal/g/n;Lcom/google/android/gms/ads/internal/g/p;Lcom/google/android/gms/ads/internal/i;)V

    .line 525
    :goto_1
    new-instance v1, Lcom/google/android/gms/ads/internal/k;

    invoke-direct {v1, p1, v0}, Lcom/google/android/gms/ads/internal/k;-><init>(Lcom/google/android/gms/ads/internal/o/b;Lcom/google/android/gms/ads/internal/p/a;)V

    iput-object v1, v10, Lcom/google/android/gms/ads/internal/i;->a:Lcom/google/android/gms/ads/internal/j;

    .line 528
    new-instance v1, Lcom/google/android/gms/ads/internal/d;

    invoke-direct {v1, p0, v10}, Lcom/google/android/gms/ads/internal/d;-><init>(Lcom/google/android/gms/ads/internal/b;Lcom/google/android/gms/ads/internal/i;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/p/a;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 535
    new-instance v1, Lcom/google/android/gms/ads/internal/e;

    invoke-direct {v1, p0, v10}, Lcom/google/android/gms/ads/internal/e;-><init>(Lcom/google/android/gms/ads/internal/b;Lcom/google/android/gms/ads/internal/i;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/p/a;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v3, v0

    .line 543
    :goto_2
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/b;->d:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, p1, Lcom/google/android/gms/ads/internal/o/b;->d:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iput-object v1, v0, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    .line 546
    :cond_0
    iget v0, p1, Lcom/google/android/gms/ads/internal/o/b;->e:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_5

    .line 547
    new-instance v0, Lcom/google/android/gms/ads/internal/o/a;

    invoke-direct {v0, p1, v3}, Lcom/google/android/gms/ads/internal/o/a;-><init>(Lcom/google/android/gms/ads/internal/o/b;Lcom/google/android/gms/ads/internal/p/a;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/b;->a(Lcom/google/android/gms/ads/internal/o/a;)V

    .line 586
    :goto_3
    return-void

    .line 524
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/g;->getNextView()Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/gms/ads/internal/p/a;

    if-eqz v1, :cond_3

    check-cast v0, Lcom/google/android/gms/ads/internal/p/a;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/ads/internal/p/a;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V

    :cond_2
    :goto_4
    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->f()Lcom/google/android/gms/ads/internal/p/c;

    move-result-object v3

    move-object v4, p0

    move-object v5, p0

    move-object v6, p0

    move-object v7, p0

    move v8, v2

    move-object v9, p0

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/gms/ads/internal/p/c;->a(Lcom/google/android/gms/ads/internal/a;Lcom/google/android/gms/ads/internal/overlay/i;Lcom/google/android/gms/ads/internal/g/b;Lcom/google/android/gms/ads/internal/overlay/n;ZLcom/google/android/gms/ads/internal/g/n;Lcom/google/android/gms/ads/internal/i;)V

    goto :goto_1

    :cond_3
    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/ads/internal/g;->removeView(Landroid/view/View;)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v4, v3, Lcom/google/android/gms/ads/internal/h;->d:Lcom/google/android/a/w;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v5, v3, Lcom/google/android/gms/ads/internal/h;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    move v3, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/p/a;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;ZZLcom/google/android/a/w;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)Lcom/google/android/gms/ads/internal/p/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->h:[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    if-nez v1, :cond_2

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/internal/b;->b(Landroid/view/View;)V

    goto :goto_4

    .line 557
    :cond_5
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->h:Z

    if-nez v0, :cond_7

    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->s:Z

    if-eqz v0, :cond_7

    .line 560
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->b:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 561
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/net/Uri$Builder;->query(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    .line 564
    :cond_6
    new-instance v0, Lcom/google/android/gms/ads/internal/e/a;

    iget-object v1, p1, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->c:Ljava/lang/String;

    invoke-direct {v0, p0, v11, v1}, Lcom/google/android/gms/ads/internal/e/a;-><init>(Lcom/google/android/gms/ads/internal/l;Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->s:Lcom/google/android/gms/ads/internal/e/a/d;

    if-eqz v1, :cond_7

    .line 568
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    const/4 v4, 0x1

    iput v4, v1, Lcom/google/android/gms/ads/internal/h;->x:I

    .line 569
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->s:Lcom/google/android/gms/ads/internal/e/a/d;

    invoke-interface {v1, v0}, Lcom/google/android/gms/ads/internal/e/a/d;->a(Lcom/google/android/gms/ads/internal/e/a/a;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_3

    .line 573
    :catch_0
    move-exception v0

    .line 574
    const-string v1, "Could not call the onCustomRenderedAdLoadedListener."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 578
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput v2, v0, Lcom/google/android/gms/ads/internal/h;->x:I

    .line 579
    iget-object v6, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/b;->b:Lcom/google/android/gms/ads/internal/k/a/a;

    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->t:Z

    if-eqz v0, :cond_8

    new-instance v0, Lcom/google/android/gms/ads/internal/request/ac;

    new-instance v3, Lcom/google/android/gms/ads/internal/a/r;

    invoke-direct {v3}, Lcom/google/android/gms/ads/internal/a/r;-><init>()V

    move-object v2, p0

    move-object v4, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/request/ac;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/b;Lcom/google/android/gms/ads/internal/a/r;Lcom/google/android/gms/ads/internal/o/b;Lcom/google/android/gms/ads/internal/request/j;)V

    :goto_5
    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/util/a;->e()V

    iput-object v0, v6, Lcom/google/android/gms/ads/internal/h;->i:Lcom/google/android/gms/ads/internal/util/a;

    goto/16 :goto_3

    :cond_8
    new-instance v0, Lcom/google/android/gms/ads/internal/request/k;

    move-object v2, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/request/k;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/o/b;Lcom/google/android/gms/ads/internal/p/a;Lcom/google/android/gms/ads/internal/k/a/a;Lcom/google/android/gms/ads/internal/request/j;)V

    goto :goto_5

    :cond_9
    move v7, v2

    goto/16 :goto_0

    :cond_a
    move-object v3, v11

    goto/16 :goto_2
.end method

.method public final a(Lcom/google/android/gms/ads/internal/purchase/a/d;)V
    .locals 1

    .prologue
    .line 1106
    const-string v0, "setInAppPurchaseListener must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 1108
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object p1, v0, Lcom/google/android/gms/ads/internal/h;->p:Lcom/google/android/gms/ads/internal/purchase/a/d;

    .line 1109
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/purchase/a/n;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1122
    const-string v0, "setPlayStorePurchaseParams must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 1124
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    new-instance v1, Lcom/google/android/gms/ads/internal/purchase/m;

    invoke-direct {v1, p2}, Lcom/google/android/gms/ads/internal/purchase/m;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/google/android/gms/ads/internal/h;->u:Lcom/google/android/gms/ads/internal/purchase/m;

    .line 1125
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object p1, v0, Lcom/google/android/gms/ads/internal/h;->o:Lcom/google/android/gms/ads/internal/purchase/a/n;

    .line 1126
    invoke-static {}, Lcom/google/android/gms/ads/internal/o/e;->e()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 1127
    new-instance v0, Lcom/google/android/gms/ads/internal/purchase/c;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->o:Lcom/google/android/gms/ads/internal/purchase/a/n;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/h;->u:Lcom/google/android/gms/ads/internal/purchase/m;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/ads/internal/purchase/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/purchase/a/n;Lcom/google/android/gms/ads/internal/purchase/m;)V

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/purchase/c;->e()V

    .line 1131
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 851
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->n:Lcom/google/android/gms/ads/internal/client/s;

    if-eqz v0, :cond_0

    .line 853
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->n:Lcom/google/android/gms/ads/internal/client/s;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/ads/internal/client/s;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 858
    :cond_0
    :goto_0
    return-void

    .line 854
    :catch_0
    move-exception v0

    .line 855
    const-string v1, "Could not call the AppEventListener."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 866
    new-instance v0, Lcom/google/android/gms/ads/internal/purchase/e;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/google/android/gms/ads/internal/purchase/e;-><init>(Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Context;Ljava/lang/String;)V

    .line 871
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->p:Lcom/google/android/gms/ads/internal/purchase/a/d;

    if-nez v1, :cond_5

    .line 872
    const-string v1, "InAppPurchaseListener is not set. Try to launch default purchase flow."

    invoke-static {v1}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 873
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/Context;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 875
    const-string v0, "Google Play Service unavailable, cannot launch default purchase flow."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 917
    :goto_0
    return-void

    .line 878
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->o:Lcom/google/android/gms/ads/internal/purchase/a/n;

    if-nez v1, :cond_1

    .line 879
    const-string v0, "PlayStorePurchaseListener is not set."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 882
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->u:Lcom/google/android/gms/ads/internal/purchase/m;

    if-nez v1, :cond_2

    .line 883
    const-string v0, "PlayStorePurchaseVerifier is not initialized."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 886
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-boolean v1, v1, Lcom/google/android/gms/ads/internal/h;->y:Z

    if-eqz v1, :cond_3

    .line 887
    const-string v0, "An in-app purchase request is already in progress, abort"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 890
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/gms/ads/internal/h;->y:Z

    .line 893
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->o:Lcom/google/android/gms/ads/internal/purchase/a/n;

    invoke-interface {v1, p1}, Lcom/google/android/gms/ads/internal/purchase/a/n;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 894
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/ads/internal/h;->y:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 898
    :catch_0
    move-exception v0

    const-string v0, "Could not start In-App purchase."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 899
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-boolean v3, v0, Lcom/google/android/gms/ads/internal/h;->y:Z

    goto :goto_0

    .line 902
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-boolean v2, v2, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->e:Z

    new-instance v3, Lcom/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel;

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v4, v4, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v5, v5, Lcom/google/android/gms/ads/internal/h;->u:Lcom/google/android/gms/ads/internal/purchase/m;

    invoke-direct {v3, v4, v5, v0, p0}, Lcom/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/purchase/m;Lcom/google/android/gms/ads/internal/purchase/a/a;Lcom/google/android/gms/ads/internal/purchase/l;)V

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/ads/internal/purchase/f;->a(Landroid/content/Context;ZLcom/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel;)V

    goto :goto_0

    .line 912
    :cond_5
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->p:Lcom/google/android/gms/ads/internal/purchase/a/d;

    invoke-interface {v1, v0}, Lcom/google/android/gms/ads/internal/purchase/a/d;->a(Lcom/google/android/gms/ads/internal/purchase/a/a;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 914
    :catch_1
    move-exception v0

    const-string v0, "Could not start In-App purchase."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;ZILandroid/content/Intent;Lcom/google/android/gms/ads/internal/purchase/g;)V
    .locals 8

    .prologue
    .line 927
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->o:Lcom/google/android/gms/ads/internal/purchase/a/n;

    if-eqz v0, :cond_0

    .line 928
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v7, v0, Lcom/google/android/gms/ads/internal/h;->o:Lcom/google/android/gms/ads/internal/purchase/a/n;

    new-instance v0, Lcom/google/android/gms/ads/internal/purchase/h;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/ads/internal/purchase/h;-><init>(Landroid/content/Context;Ljava/lang/String;ZILandroid/content/Intent;Lcom/google/android/gms/ads/internal/purchase/g;)V

    invoke-interface {v7, v0}, Lcom/google/android/gms/ads/internal/purchase/a/n;->a(Lcom/google/android/gms/ads/internal/purchase/a/k;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 941
    :cond_0
    :goto_0
    sget-object v0, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/ads/internal/f;

    invoke-direct {v1, p0, p4}, Lcom/google/android/gms/ads/internal/f;-><init>(Lcom/google/android/gms/ads/internal/b;Landroid/content/Intent;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 954
    return-void

    .line 937
    :catch_0
    move-exception v0

    const-string v0, "Fail to invoke PlayStorePurchaseListener."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/HashSet;)V
    .locals 1

    .prologue
    .line 1597
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object p1, v0, Lcom/google/android/gms/ads/internal/h;->A:Ljava/util/HashSet;

    .line 1598
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 1147
    const-string v0, "setNativeTemplates must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 1148
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object p1, v0, Lcom/google/android/gms/ads/internal/h;->t:Ljava/util/List;

    .line 1149
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1153
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-boolean p1, v0, Lcom/google/android/gms/ads/internal/h;->z:Z

    .line 1154
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 382
    const-string v0, "loadAd must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 384
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->f:Lcom/google/android/gms/ads/internal/util/a;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->i:Lcom/google/android/gms/ads/internal/util/a;

    if-eqz v0, :cond_3

    .line 388
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->a:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    if-eqz v0, :cond_1

    .line 389
    const-string v0, "Aborting last ad request since another ad request is already in progress. The current request object will still be cached for future refreshes."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 392
    :cond_1
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/b;->a:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    .line 437
    :cond_2
    :goto_0
    return v1

    .line 396
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    if-eqz v0, :cond_4

    .line 397
    const-string v0, "An interstitial is already loading. Aborting."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 402
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v4, v4, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.permission.INTERNET"

    invoke-static {v0, v4, v5}, Lcom/google/android/gms/ads/internal/util/g;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v4, v4, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    const-string v5, "Missing internet permission in AndroidManifest.xml."

    const-string v6, "Missing internet permission in AndroidManifest.xml. You must have the following declaration: <uses-permission android:name=\"android.permission.INTERNET\" />"

    invoke-static {v0, v4, v5, v6}, Lcom/google/android/gms/ads/internal/util/client/a;->a(Landroid/view/ViewGroup;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move v0, v1

    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v4, v4, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/ads/internal/util/g;->a(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v4, v4, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    const-string v5, "Missing AdActivity with android:configChanges in AndroidManifest.xml."

    const-string v6, "Missing AdActivity with android:configChanges in AndroidManifest.xml. You must have the following declaration within the <application> element: <activity android:name=\"com.google.android.gms.ads.AdActivity\" android:configChanges=\"keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize\" />"

    invoke-static {v0, v4, v5, v6}, Lcom/google/android/gms/ads/internal/util/client/a;->a(Landroid/view/ViewGroup;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move v0, v1

    :cond_7
    if-nez v0, :cond_8

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v4, v4, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v4, v4, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-nez v4, :cond_8

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v4, v4, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v4, v1}, Lcom/google/android/gms/ads/internal/g;->setVisibility(I)V

    :cond_8
    if-eqz v0, :cond_2

    .line 406
    const-string v0, "Starting ad request."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    .line 409
    iget-boolean v0, p1, Lcom/google/android/gms/ads/internal/client/AdRequestParcel;->f:Z

    if-nez v0, :cond_9

    .line 410
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "Use AdRequest.Builder.addTestDevice(\""

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v4, v4, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/ads/internal/util/client/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\") to get test ads on this device."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    .line 415
    :cond_9
    invoke-static {}, Lcom/google/android/gms/ads/internal/o/e;->a()Lcom/google/android/gms/ads/internal/o/e;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v4, v4, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/ads/internal/o/e;->a(Landroid/content/Context;)Lcom/google/android/gms/ads/internal/b/d;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/b/d;->d()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/b/d;->c()V

    :cond_a
    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/b/d;->b()Lcom/google/android/gms/ads/internal/b/a;

    move-result-object v4

    if-eqz v4, :cond_c

    iget-object v0, v4, Lcom/google/android/gms/ads/internal/b/a;->f:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "In AdManger: loadAd, "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/google/android/gms/ads/internal/b/a;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    move-object v4, v0

    :goto_2
    if-eqz v4, :cond_b

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string v3, "fingerprint"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "v"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 422
    :goto_3
    iget-object v3, p0, Lcom/google/android/gms/ads/internal/b;->d:Lcom/google/android/gms/ads/internal/s;

    invoke-virtual {v3}, Lcom/google/android/gms/ads/internal/s;->a()V

    .line 425
    iget-object v3, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput v1, v3, Lcom/google/android/gms/ads/internal/h;->x:I

    .line 428
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/ads/internal/b;->a(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Landroid/os/Bundle;)Lcom/google/android/gms/ads/internal/request/w;

    move-result-object v0

    .line 432
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v4, v4, Lcom/google/android/gms/ads/internal/h;->d:Lcom/google/android/a/w;

    new-instance v5, Lcom/google/android/gms/ads/internal/request/c;

    invoke-direct {v5, v3, v0, v4, p0}, Lcom/google/android/gms/ads/internal/request/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/request/w;Lcom/google/android/a/w;Lcom/google/android/gms/ads/internal/request/b;)V

    invoke-virtual {v5}, Lcom/google/android/gms/ads/internal/request/c;->e()V

    iput-object v5, v1, Lcom/google/android/gms/ads/internal/h;->f:Lcom/google/android/gms/ads/internal/util/a;

    move v1, v2

    .line 437
    goto/16 :goto_0

    :cond_b
    move-object v0, v3

    .line 415
    goto :goto_3

    :cond_c
    move-object v4, v3

    goto :goto_2

    :cond_d
    move v0, v2

    goto/16 :goto_1
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 320
    const-string v0, "destroy must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 321
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->g:Landroid/content/ComponentCallbacks;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object v1, v0, Lcom/google/android/gms/ads/internal/h;->h:Lcom/google/android/gms/ads/internal/client/f;

    .line 323
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object v1, v0, Lcom/google/android/gms/ads/internal/h;->n:Lcom/google/android/gms/ads/internal/client/s;

    .line 324
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object v1, v0, Lcom/google/android/gms/ads/internal/h;->o:Lcom/google/android/gms/ads/internal/purchase/a/n;

    .line 325
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object v1, v0, Lcom/google/android/gms/ads/internal/h;->p:Lcom/google/android/gms/ads/internal/purchase/a/d;

    .line 326
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iput-object v1, v0, Lcom/google/android/gms/ads/internal/h;->s:Lcom/google/android/gms/ads/internal/e/a/d;

    .line 327
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->d:Lcom/google/android/gms/ads/internal/s;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/s;->a()V

    .line 328
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->e:Lcom/google/android/gms/ads/internal/a/c;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v3, v0, Lcom/google/android/gms/ads/internal/a/c;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v0, v0, Lcom/google/android/gms/ads/internal/a/c;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, v2}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/internal/a/d;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/a/d;->f()V

    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    if-eqz v0, :cond_5

    .line 333
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    .line 334
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->m:Lcom/google/android/gms/ads/internal/k/a/d;

    .line 337
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/b;->r()V

    .line 340
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    if-eqz v2, :cond_2

    .line 341
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/internal/g;->removeAllViews()V

    .line 344
    :cond_2
    if-eqz v1, :cond_3

    .line 345
    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/p/a;->destroy()V

    .line 347
    :cond_3
    if-eqz v0, :cond_4

    .line 349
    :try_start_1
    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/k/a/d;->c()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 354
    :cond_4
    :goto_1
    return-void

    .line 328
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 351
    :catch_0
    move-exception v0

    const-string v0, "Could not destroy mediation adapter."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)V
    .locals 2

    .prologue
    .line 1053
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/g;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1054
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/ads/internal/util/g;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/b;->f:Z

    if-nez v0, :cond_0

    .line 1058
    invoke-virtual {p0, p1}, Lcom/google/android/gms/ads/internal/b;->a(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)Z

    .line 1063
    :goto_0
    return-void

    .line 1060
    :cond_0
    const-string v0, "Ad is not visible. Not refreshing ad."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    .line 1061
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->d:Lcom/google/android/gms/ads/internal/s;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/internal/s;->a(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)V

    goto :goto_0
.end method

.method public final c()Lcom/google/android/gms/b/l;
    .locals 1

    .prologue
    .line 358
    const-string v0, "getAdFrame must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 359
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/gms/ads/internal/client/AdSizeParcel;
    .locals 1

    .prologue
    .line 364
    const-string v0, "getAdSize must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 365
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 371
    const-string v0, "isLoaded must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 372
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->f:Lcom/google/android/gms/ads/internal/util/a;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->i:Lcom/google/android/gms/ads/internal/util/a;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 825
    const-string v0, "Ad leaving application."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->h:Lcom/google/android/gms/ads/internal/client/f;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->h:Lcom/google/android/gms/ads/internal/client/f;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/client/f;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 826
    :cond_0
    :goto_0
    return-void

    .line 825
    :catch_0
    move-exception v0

    const-string v1, "Could not call AdListener.onAdLeftApplication()."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final g()V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    .line 830
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->e:Lcom/google/android/gms/ads/internal/a/c;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/a/c;->a(Lcom/google/android/gms/ads/internal/o/a;)V

    .line 831
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-eqz v0, :cond_0

    .line 832
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/b;->w()V

    .line 834
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/b;->f:Z

    .line 835
    const-string v0, "Ad closing."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->h:Lcom/google/android/gms/ads/internal/client/f;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->h:Lcom/google/android/gms/ads/internal/client/f;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/client/f;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 836
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/h;->m:Lcom/google/android/gms/ads/internal/o/c;

    iget-object v2, v1, Lcom/google/android/gms/ads/internal/o/c;->c:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    iget-wide v4, v1, Lcom/google/android/gms/ads/internal/o/c;->j:J

    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    iget-object v0, v1, Lcom/google/android/gms/ads/internal/o/c;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, v1, Lcom/google/android/gms/ads/internal/o/c;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/internal/o/d;

    iget-wide v4, v0, Lcom/google/android/gms/ads/internal/o/d;->b:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/google/android/gms/ads/internal/o/d;->b:J

    iget-object v0, v1, Lcom/google/android/gms/ads/internal/o/c;->a:Lcom/google/android/gms/ads/internal/o/e;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/o/e;->a(Lcom/google/android/gms/ads/internal/o/c;)V

    :cond_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    .line 835
    :catch_0
    move-exception v0

    const-string v1, "Could not call AdListener.onAdClosed()."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 836
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 842
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-eqz v0, :cond_0

    .line 843
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/internal/b;->b(Z)V

    .line 845
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/b;->f:Z

    .line 846
    const-string v0, "Ad opening."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->h:Lcom/google/android/gms/ads/internal/client/f;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->h:Lcom/google/android/gms/ads/internal/client/f;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/client/f;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 847
    :cond_1
    :goto_0
    return-void

    .line 846
    :catch_0
    move-exception v0

    const-string v1, "Could not call AdListener.onAdOpened()."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 962
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/b;->t()V

    .line 963
    return-void
.end method

.method public final j()V
    .locals 0

    .prologue
    .line 968
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/b;->g()V

    .line 969
    return-void
.end method

.method public final k()V
    .locals 0

    .prologue
    .line 974
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/b;->f()V

    .line 975
    return-void
.end method

.method public final l()V
    .locals 0

    .prologue
    .line 980
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/b;->h()V

    .line 981
    return-void
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 985
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    if-eqz v0, :cond_0

    .line 986
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Mediation adapter "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/o/a;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " refreshed, but mediation adapters should never refresh."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 990
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/internal/b;->b(Z)V

    .line 993
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/b;->v()V

    .line 994
    return-void
.end method

.method public final n()V
    .locals 3

    .prologue
    .line 999
    const-string v0, "recordManualImpression must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 1001
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    if-nez v0, :cond_1

    .line 1002
    const-string v0, "Ad state was null when trying to ping manual tracking URLs."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 1014
    :cond_0
    :goto_0
    return-void

    .line 1005
    :cond_1
    const-string v0, "Pinging manual tracking URLs."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 1008
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1009
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/o/a;->f:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/ads/internal/util/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0
.end method

.method public final o()V
    .locals 3

    .prologue
    .line 1018
    const-string v0, "pause must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 1019
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget v0, v0, Lcom/google/android/gms/ads/internal/h;->x:I

    if-nez v0, :cond_0

    .line 1021
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/g;->a(Landroid/webkit/WebView;)V

    .line 1023
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->m:Lcom/google/android/gms/ads/internal/k/a/d;

    if-eqz v0, :cond_1

    .line 1025
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->m:Lcom/google/android/gms/ads/internal/k/a/d;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/k/a/d;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1030
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->e:Lcom/google/android/gms/ads/internal/a/c;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/a/c;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    iget-object v0, v0, Lcom/google/android/gms/ads/internal/a/c;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/internal/a/d;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/a/d;->g()V

    :cond_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1031
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->d:Lcom/google/android/gms/ads/internal/s;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/ads/internal/s;->e:Z

    iget-boolean v1, v0, Lcom/google/android/gms/ads/internal/s;->d:Z

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/s;->a:Lcom/google/android/gms/ads/internal/u;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/s;->b:Ljava/lang/Runnable;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/ads/internal/u;->a(Ljava/lang/Runnable;)V

    .line 1032
    :cond_3
    return-void

    .line 1027
    :catch_0
    move-exception v0

    const-string v0, "Could not pause mediation adapter."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 1030
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final p()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1036
    const-string v0, "resume must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 1037
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget v0, v0, Lcom/google/android/gms/ads/internal/h;->x:I

    if-nez v0, :cond_0

    .line 1039
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/g;->b(Landroid/webkit/WebView;)V

    .line 1041
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->m:Lcom/google/android/gms/ads/internal/k/a/d;

    if-eqz v0, :cond_1

    .line 1043
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->m:Lcom/google/android/gms/ads/internal/k/a/d;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/k/a/d;->e()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1048
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->d:Lcom/google/android/gms/ads/internal/s;

    iput-boolean v2, v0, Lcom/google/android/gms/ads/internal/s;->e:Z

    iget-boolean v1, v0, Lcom/google/android/gms/ads/internal/s;->d:Z

    if-eqz v1, :cond_2

    iput-boolean v2, v0, Lcom/google/android/gms/ads/internal/s;->d:Z

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/s;->c:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-wide v2, v0, Lcom/google/android/gms/ads/internal/s;->f:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/ads/internal/s;->a(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;J)V

    .line 1049
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->e:Lcom/google/android/gms/ads/internal/a/c;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/a/c;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    iget-object v0, v0, Lcom/google/android/gms/ads/internal/a/c;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/internal/a/d;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/a/d;->h()V

    :cond_3
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    .line 1045
    :catch_0
    move-exception v0

    const-string v0, "Could not resume mediation adapter."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 1049
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final q()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1158
    const-string v0, "showInterstitial must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 1160
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-nez v0, :cond_1

    .line 1161
    const-string v0, "Cannot call showInterstitial on a banner ad."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 1221
    :cond_0
    :goto_0
    return-void

    .line 1165
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    if-nez v0, :cond_2

    .line 1166
    const-string v0, "The interstitial has not loaded."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 1170
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget v0, v0, Lcom/google/android/gms/ads/internal/h;->x:I

    if-eq v0, v1, :cond_0

    .line 1175
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1176
    const-string v0, "The interstitial is already showing."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 1179
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/p/a;->a(Z)V

    .line 1180
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/o/a;->a()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->j:Lorg/json/JSONObject;

    if-eqz v0, :cond_5

    .line 1181
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->e:Lcom/google/android/gms/ads/internal/a/c;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v4, v4, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/ads/internal/a/c;->a(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/ads/internal/o/a;)Lcom/google/android/gms/ads/internal/a/d;

    move-result-object v0

    .line 1183
    iget-object v3, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    invoke-virtual {v3}, Lcom/google/android/gms/ads/internal/o/a;->a()Z

    move-result v3

    if-eqz v3, :cond_5

    if-eqz v0, :cond_5

    .line 1184
    new-instance v3, Lcom/google/android/gms/ads/internal/r;

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v4, v4, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v4, v4, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-direct {v3, v4}, Lcom/google/android/gms/ads/internal/r;-><init>(Lcom/google/android/gms/ads/internal/p/a;)V

    invoke-virtual {v0, v3}, Lcom/google/android/gms/ads/internal/a/d;->a(Lcom/google/android/gms/ads/internal/a/a;)V

    .line 1187
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/o/a;->k:Z

    if-eqz v0, :cond_6

    .line 1189
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->m:Lcom/google/android/gms/ads/internal/k/a/d;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/k/a/d;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1190
    :catch_0
    move-exception v0

    .line 1191
    const-string v1, "Could not show interstitial."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1192
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/b;->w()V

    goto/16 :goto_0

    .line 1195
    :cond_6
    new-instance v8, Lcom/google/android/gms/ads/internal/InterstitialAdParameterParcel;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/h;->z:Z

    invoke-direct {v8, v0, v2}, Lcom/google/android/gms/ads/internal/InterstitialAdParameterParcel;-><init>(ZZ)V

    .line 1197
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_7

    .line 1198
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 1199
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 1200
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 1201
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1202
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1203
    iget v0, v3, Landroid/graphics/Rect;->bottom:I

    if-eqz v0, :cond_7

    iget v0, v4, Landroid/graphics/Rect;->bottom:I

    if-eqz v0, :cond_7

    .line 1204
    new-instance v8, Lcom/google/android/gms/ads/internal/InterstitialAdParameterParcel;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-boolean v5, v0, Lcom/google/android/gms/ads/internal/h;->z:Z

    iget v0, v3, Landroid/graphics/Rect;->top:I

    iget v3, v4, Landroid/graphics/Rect;->top:I

    if-ne v0, v3, :cond_8

    move v0, v1

    :goto_1
    invoke-direct {v8, v5, v0}, Lcom/google/android/gms/ads/internal/InterstitialAdParameterParcel;-><init>(ZZ)V

    .line 1210
    :cond_7
    new-instance v0, Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v4, v1, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget v5, v1, Lcom/google/android/gms/ads/internal/o/a;->g:I

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v6, v1, Lcom/google/android/gms/ads/internal/h;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v7, v1, Lcom/google/android/gms/ads/internal/o/a;->v:Ljava/lang/String;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;-><init>(Lcom/google/android/gms/ads/internal/a;Lcom/google/android/gms/ads/internal/overlay/i;Lcom/google/android/gms/ads/internal/overlay/n;Lcom/google/android/gms/ads/internal/p/a;ILcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Ljava/lang/String;Lcom/google/android/gms/ads/internal/InterstitialAdParameterParcel;)V

    .line 1219
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/overlay/c;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;)V

    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 1204
    goto :goto_1
.end method

.method public final r()V
    .locals 2

    .prologue
    .line 1225
    const-string v0, "stopLoading must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 1228
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget v0, v0, Lcom/google/android/gms/ads/internal/h;->x:I

    if-nez v0, :cond_0

    .line 1230
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->stopLoading()V

    .line 1231
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    .line 1235
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->f:Lcom/google/android/gms/ads/internal/util/a;

    if-eqz v0, :cond_1

    .line 1236
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->f:Lcom/google/android/gms/ads/internal/util/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/util/a;->f()V

    .line 1240
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->i:Lcom/google/android/gms/ads/internal/util/a;

    if-eqz v0, :cond_2

    .line 1241
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->i:Lcom/google/android/gms/ads/internal/util/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/util/a;->f()V

    .line 1243
    :cond_2
    return-void
.end method

.method public final s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1272
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    if-eqz v0, :cond_0

    .line 1273
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->n:Ljava/lang/String;

    .line 1275
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()V
    .locals 6

    .prologue
    .line 1506
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    if-nez v0, :cond_1

    .line 1507
    const-string v0, "Ad state was null when trying to ping click URLs."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 1543
    :cond_0
    :goto_0
    return-void

    .line 1510
    :cond_1
    const-string v0, "Pinging click URLs."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 1511
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->m:Lcom/google/android/gms/ads/internal/o/c;

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/o/c;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-wide v2, v0, Lcom/google/android/gms/ads/internal/o/c;->j:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    new-instance v2, Lcom/google/android/gms/ads/internal/o/d;

    invoke-direct {v2}, Lcom/google/android/gms/ads/internal/o/d;-><init>()V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/gms/ads/internal/o/d;->a:J

    iget-object v3, v0, Lcom/google/android/gms/ads/internal/o/c;->b:Ljava/util/LinkedList;

    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-wide v2, v0, Lcom/google/android/gms/ads/internal/o/c;->h:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/gms/ads/internal/o/c;->h:J

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/c;->a:Lcom/google/android/gms/ads/internal/o/e;

    invoke-static {}, Lcom/google/android/gms/ads/internal/o/e;->d()Lcom/google/android/gms/ads/internal/o/f;

    move-result-object v2

    iget-object v3, v2, Lcom/google/android/gms/ads/internal/o/f;->a:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget v4, v2, Lcom/google/android/gms/ads/internal/o/f;->b:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v2, Lcom/google/android/gms/ads/internal/o/f;->b:I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/c;->a:Lcom/google/android/gms/ads/internal/o/e;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/ads/internal/o/e;->a(Lcom/google/android/gms/ads/internal/o/c;)V

    :cond_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1514
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->c:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 1515
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/o/a;->c:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/ads/internal/util/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    .line 1522
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->o:Lcom/google/android/gms/ads/internal/k/d;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/a;->o:Lcom/google/android/gms/ads/internal/k/d;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/k/d;->c:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 1524
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/h;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/h;->b:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v5, v5, Lcom/google/android/gms/ads/internal/h;->k:Lcom/google/android/gms/ads/internal/o/a;

    iget-object v5, v5, Lcom/google/android/gms/ads/internal/o/a;->o:Lcom/google/android/gms/ads/internal/k/d;

    iget-object v5, v5, Lcom/google/android/gms/ads/internal/k/d;->c:Ljava/util/List;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/k/k;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/ads/internal/o/a;Ljava/lang/String;ZLjava/util/List;)V

    .line 1536
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->g:Lcom/google/android/gms/ads/internal/client/c;

    if-eqz v0, :cond_0

    .line 1538
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b;->c:Lcom/google/android/gms/ads/internal/h;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/h;->g:Lcom/google/android/gms/ads/internal/client/c;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/client/c;->a()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 1539
    :catch_0
    move-exception v0

    .line 1540
    const-string v1, "Could not notify onAdClicked event."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1511
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v3

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final u()V
    .locals 1

    .prologue
    .line 1547
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/internal/b;->b(Z)V

    .line 1548
    return-void
.end method
