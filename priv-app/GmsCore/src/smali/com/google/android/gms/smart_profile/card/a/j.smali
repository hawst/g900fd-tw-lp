.class public final Lcom/google/android/gms/smart_profile/card/a/j;
.super Lcom/google/android/gms/smart_profile/card/a/b;
.source "SourceFile"


# instance fields
.field private e:Ljava/util/ArrayList;

.field private f:Lcom/google/ac/c/a/a/a/aq;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/a/b;-><init>()V

    .line 43
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 129
    const-string v0, "loaderIds"

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/j;->e:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 130
    return-void
.end method

.method public final a(Landroid/support/v4/app/au;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    sget v1, Lcom/google/android/gms/j;->nW:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;

    .line 136
    if-nez v0, :cond_0

    .line 170
    :goto_0
    return-void

    .line 139
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->removeAllViews()V

    .line 140
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 141
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/j;->f:Lcom/google/ac/c/a/a/a/aq;

    iget-object v1, v1, Lcom/google/ac/c/a/a/a/aq;->b:[Lcom/google/ac/c/a/a/a/ap;

    array-length v2, v1

    sget-object v1, Lcom/google/android/gms/smart_profile/a/a;->p:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v6

    move v3, v4

    .line 142
    :goto_1
    if-ge v3, v6, :cond_2

    .line 143
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/j;->f:Lcom/google/ac/c/a/a/a/aq;

    iget-object v1, v1, Lcom/google/ac/c/a/a/a/aq;->b:[Lcom/google/ac/c/a/a/a/ap;

    aget-object v7, v1, v3

    .line 144
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v1, Landroid/support/v7/widget/CardView;

    invoke-virtual {v1}, Landroid/support/v7/widget/CardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/gms/l;->ff:I

    invoke-virtual {v1, v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 147
    new-instance v2, Lcom/google/android/gms/smart_profile/card/a/l;

    invoke-direct {v2, p0, v7}, Lcom/google/android/gms/smart_profile/card/a/l;-><init>(Lcom/google/android/gms/smart_profile/card/a/j;Lcom/google/ac/c/a/a/a/ap;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    iget-object v2, v7, Lcom/google/ac/c/a/a/a/ap;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 165
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/a/j;->e:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v8, 0x0

    new-instance v9, Lcom/google/android/gms/smart_profile/card/a/m;

    iget-object v7, v7, Lcom/google/ac/c/a/a/a/ap;->b:Ljava/lang/String;

    invoke-direct {v9, p0, v1, v7, v4}, Lcom/google/android/gms/smart_profile/card/a/m;-><init>(Lcom/google/android/gms/smart_profile/card/a/j;Landroid/widget/ImageView;Ljava/lang/String;B)V

    invoke-virtual {p1, v2, v8, v9}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 142
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 169
    :cond_2
    invoke-virtual {v0, v5}, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/smart_profile/card/a/c;Landroid/os/Bundle;Lcom/google/android/gms/smart_profile/bg;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 74
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/smart_profile/card/a/b;->a(Lcom/google/android/gms/smart_profile/card/a/c;Landroid/os/Bundle;Lcom/google/android/gms/smart_profile/bg;)V

    .line 76
    if-nez p3, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->a:Ljava/lang/Object;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/j;->f:Lcom/google/ac/c/a/a/a/aq;

    .line 81
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/a/j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    if-eqz p2, :cond_4

    const-string v0, "loaderIds"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 87
    const-string v0, "loaderIds"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/j;->e:Ljava/util/ArrayList;

    .line 97
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    sget v1, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 98
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v1, Landroid/support/v7/widget/CardView;

    invoke-virtual {v1}, Landroid/support/v7/widget/CardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/gms/p;->xi:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    invoke-virtual {p3}, Lcom/google/android/gms/smart_profile/bg;->q()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    sget v1, Lcom/google/android/gms/j;->tR:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 104
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 105
    invoke-virtual {p3}, Lcom/google/android/gms/smart_profile/bg;->q()I

    move-result v1

    invoke-static {v1, v0}, Lcom/google/android/gms/smart_profile/bf;->a(ILandroid/widget/TextView;)V

    .line 107
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v1, Landroid/support/v7/widget/CardView;

    invoke-virtual {v1}, Landroid/support/v7/widget/CardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->xj:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 109
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    invoke-virtual {p3}, Lcom/google/android/gms/smart_profile/bg;->q()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 111
    new-instance v1, Lcom/google/android/gms/smart_profile/card/a/k;

    invoke-direct {v1, p0, p3}, Lcom/google/android/gms/smart_profile/card/a/k;-><init>(Lcom/google/android/gms/smart_profile/card/a/j;Lcom/google/android/gms/smart_profile/bg;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 80
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/ac/c/a/a/a/f;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/f;->a:Lcom/google/ac/c/a/a/a/aq;

    goto :goto_1

    .line 89
    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/j;->e:Ljava/util/ArrayList;

    move v1, v2

    .line 91
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/j;->f:Lcom/google/ac/c/a/a/a/aq;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/aq;->b:[Lcom/google/ac/c/a/a/a/ap;

    array-length v0, v0

    if-ge v1, v0, :cond_2

    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->p:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v1, v0, :cond_2

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/j;->e:Ljava/util/ArrayList;

    invoke-interface {p1}, Lcom/google/android/gms/smart_profile/card/a/c;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 179
    invoke-super {p0}, Lcom/google/android/gms/smart_profile/card/a/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/j;->f:Lcom/google/ac/c/a/a/a/aq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/j;->f:Lcom/google/ac/c/a/a/a/aq;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/aq;->b:[Lcom/google/ac/c/a/a/a/ap;

    if-nez v0, :cond_2

    :cond_0
    move v1, v2

    :goto_0
    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v1, v0, :cond_1

    const/4 v2, 0x1

    :cond_1
    return v2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/j;->f:Lcom/google/ac/c/a/a/a/aq;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/aq;->b:[Lcom/google/ac/c/a/a/a/ap;

    array-length v0, v0

    move v1, v0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    .prologue
    .line 174
    sget-object v0, Lcom/google/android/gms/smart_profile/c;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method
