.class public final Lcom/google/android/gms/plus/oob/af;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/g;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 203
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 216
    :cond_1
    :goto_0
    return-object v0

    .line 206
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 207
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_4

    .line 208
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/g;

    .line 209
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/g;->e()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 210
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/g;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 207
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 216
    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/plus/oob/e;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 96
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/plus/oob/af;->a(Lcom/google/android/gms/plus/oob/e;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static a(Lcom/google/android/gms/plus/oob/e;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Ljava/lang/CharSequence;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 115
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    :goto_0
    return-object p1

    .line 119
    :cond_0
    new-instance v3, Landroid/text/SpannableStringBuilder;

    const-string v0, "<p>|</p>"

    const-string v1, " "

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 120
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const-class v1, Landroid/text/style/URLSpan;

    invoke-virtual {v3, v2, v0, v1}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 121
    if-nez v0, :cond_3

    move v1, v2

    :goto_1
    move v5, v2

    .line 122
    :goto_2
    if-ge v5, v1, :cond_c

    .line 123
    aget-object v6, v0, v5

    .line 124
    invoke-virtual {v6}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v7

    .line 125
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 126
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "#"

    invoke-virtual {v7, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x1

    :goto_3
    if-eqz v4, :cond_7

    if-eqz p3, :cond_5

    invoke-static {v7, p3}, Lcom/google/android/gms/plus/oob/af;->a(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/g;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-interface {p0, p2, v4}, Lcom/google/android/gms/plus/oob/e;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/g;)Landroid/text/style/ClickableSpan;

    move-result-object v4

    .line 128
    :goto_4
    if-eqz v4, :cond_1

    .line 129
    invoke-virtual {v3, v6}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    .line 130
    invoke-virtual {v3, v6}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v8

    .line 131
    invoke-virtual {v3, v6}, Landroid/text/SpannableStringBuilder;->getSpanFlags(Ljava/lang/Object;)I

    move-result v9

    .line 132
    invoke-virtual {v3, v4, v7, v8, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 136
    :cond_1
    invoke-virtual {v3, v6}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 122
    :cond_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    .line 121
    :cond_3
    array-length v1, v0

    goto :goto_1

    :cond_4
    move v4, v2

    .line 126
    goto :goto_3

    :cond_5
    if-eqz p4, :cond_6

    invoke-static {v7, p4}, Lcom/google/android/gms/plus/oob/af;->b(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/e;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-interface {p0, p2, v4}, Lcom/google/android/gms/plus/oob/e;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/e;)Landroid/text/style/ClickableSpan;

    move-result-object v4

    goto :goto_4

    :cond_6
    if-eqz p5, :cond_a

    invoke-static {v7, p5}, Lcom/google/android/gms/plus/oob/af;->c(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-interface {p0, p2, v4}, Lcom/google/android/gms/plus/oob/e;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;)Landroid/text/style/ClickableSpan;

    move-result-object v4

    goto :goto_4

    :cond_7
    if-eqz p3, :cond_8

    invoke-static {v7, p3}, Lcom/google/android/gms/plus/oob/af;->a(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/g;

    move-result-object v4

    invoke-interface {p0, p2, v4, v7}, Lcom/google/android/gms/plus/oob/e;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/g;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v4

    goto :goto_4

    :cond_8
    if-eqz p4, :cond_9

    invoke-static {v7, p4}, Lcom/google/android/gms/plus/oob/af;->b(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/e;

    move-result-object v4

    invoke-interface {p0, p2, v4, v7}, Lcom/google/android/gms/plus/oob/e;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/e;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v4

    goto :goto_4

    :cond_9
    if-eqz p5, :cond_a

    invoke-static {v7, p5}, Lcom/google/android/gms/plus/oob/af;->c(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;

    move-result-object v4

    invoke-interface {p0, p2, v4, v7}, Lcom/google/android/gms/plus/oob/e;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v4

    goto :goto_4

    :cond_a
    const-string v4, "UpgradeAccount"

    const/4 v8, 0x5

    invoke-static {v4, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_b

    const-string v4, "UpgradeAccount"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Failed to create ClickableSpan for url: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    const/4 v4, 0x0

    goto :goto_4

    :cond_c
    move-object p1, v3

    .line 139
    goto/16 :goto_0
.end method

.method public static a()Z
    .locals 3

    .prologue
    .line 75
    sget-object v0, Lcom/google/android/gms/plus/c/a;->T:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 76
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "*"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public static a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;)Z
    .locals 2

    .prologue
    .line 359
    if-eqz p0, :cond_0

    const-string v0, "name"

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "name"

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;)Z
    .locals 2

    .prologue
    .line 266
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ok"

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/e;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 224
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 237
    :cond_1
    :goto_0
    return-object v0

    .line 227
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 228
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_4

    .line 229
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/e;

    .line 230
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/e;->e()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 231
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/e;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 228
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 237
    goto :goto_0
.end method

.method public static b(Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;)Z
    .locals 2

    .prologue
    .line 275
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 276
    :cond_0
    const/4 v0, 0x0

    .line 278
    :goto_0
    return v0

    :cond_1
    const-string v0, "error"

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 246
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 259
    :cond_1
    :goto_0
    return-object v0

    .line 249
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 250
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_4

    .line 251
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;

    .line 252
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;->e()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 253
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 250
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 259
    goto :goto_0
.end method

.method public static c(Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;)Z
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 286
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;->d()Lcom/google/android/gms/plus/service/v1whitelisted/models/fm;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fm;->e()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v2, v4

    .line 304
    :cond_1
    :goto_0
    return v2

    .line 293
    :cond_2
    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;->d()Lcom/google/android/gms/plus/service/v1whitelisted/models/fm;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fm;->d()Ljava/util/List;

    move-result-object v6

    .line 294
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    move v5, v4

    move v1, v4

    move v3, v4

    .line 295
    :goto_1
    if-ge v5, v7, :cond_4

    .line 296
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    .line 297
    const-string v8, "termsOfService"

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    move v1, v2

    .line 300
    :cond_3
    const-string v8, "button"

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 301
    add-int/lit8 v0, v3, 0x1

    .line 295
    :goto_2
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v3, v0

    goto :goto_1

    .line 304
    :cond_4
    if-eqz v1, :cond_5

    if-eq v3, v2, :cond_1

    :cond_5
    move v2, v4

    goto :goto_0

    :cond_6
    move v0, v3

    goto :goto_2
.end method

.method public static d(Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;)Lcom/google/android/gms/plus/service/v1whitelisted/models/b;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 320
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;->d()Lcom/google/android/gms/plus/service/v1whitelisted/models/fm;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fm;->e()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 332
    :cond_1
    :goto_0
    return-object v0

    .line 324
    :cond_2
    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;->d()Lcom/google/android/gms/plus/service/v1whitelisted/models/fm;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fm;->d()Ljava/util/List;

    move-result-object v3

    .line 325
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    .line 326
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    .line 327
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    .line 328
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->e()Z

    move-result v5

    if-nez v5, :cond_1

    .line 326
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 332
    goto :goto_0
.end method
