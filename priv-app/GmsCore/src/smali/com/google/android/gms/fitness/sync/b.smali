.class public final Lcom/google/android/gms/fitness/sync/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private final e:Lcom/google/android/gms/fitness/data/Device;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Device;->a(Landroid/content/Context;)Lcom/google/android/gms/fitness/data/Device;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sync/b;->e:Lcom/google/android/gms/fitness/data/Device;

    .line 48
    return-void
.end method

.method private a(JLcom/google/android/gms/fitness/l/z;Lcom/google/android/gms/fitness/sync/d;)V
    .locals 11

    .prologue
    .line 155
    :goto_0
    sget-object v0, Lcom/google/android/gms/fitness/g/c;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p3, v0, p1, p2}, Lcom/google/android/gms/fitness/l/z;->a(IJ)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 156
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/l/c;

    iget-wide v6, v0, Lcom/google/android/gms/fitness/l/c;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v3, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v5, v0, Lcom/google/android/gms/fitness/l/c;->d:[B

    invoke-static {v5}, Lcom/google/af/a/b/a/a/o;->a([B)Lcom/google/af/a/b/a/a/o;

    move-result-object v5

    iget-boolean v6, v0, Lcom/google/android/gms/fitness/l/c;->c:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, v5, Lcom/google/af/a/b/a/a/o;->a:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    iget-boolean v6, v0, Lcom/google/android/gms/fitness/l/c;->c:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, v5, Lcom/google/af/a/b/a/a/o;->a:Ljava/lang/Boolean;

    iget-object v6, v5, Lcom/google/af/a/b/a/a/o;->c:Lcom/google/af/a/b/a/a/c;

    iget-wide v8, v0, Lcom/google/android/gms/fitness/l/c;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, v6, Lcom/google/af/a/b/a/a/c;->f:Ljava/lang/Long;

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :catch_0
    move-exception v5

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "unable to parse: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v5}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    :try_start_1
    invoke-interface {p4, v2}, Lcom/google/android/gms/fitness/sync/d;->a(Ljava/util/List;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/fitness/sync/b;->c:I
    :try_end_1
    .catch Lcom/google/android/gms/fitness/sync/g; {:try_start_1 .. :try_end_1} :catch_2

    :cond_1
    if-nez v1, :cond_2

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_2
    :try_start_2
    invoke-interface {p3, v0}, Lcom/google/android/gms/fitness/l/z;->a(Ljava/util/List;)I
    :try_end_2
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_2 .. :try_end_2} :catch_3

    :goto_3
    :try_start_3
    invoke-interface {p3, v1}, Lcom/google/android/gms/fitness/l/z;->a(Ljava/util/Set;)V
    :try_end_3
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unable to update retry count: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_0

    :catch_2
    move-exception v0

    const-string v2, "unable to applyChange"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v5}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/sync/g;->a()Z

    move-result v2

    if-nez v2, :cond_1

    throw v0

    :cond_2
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    const-string v0, "Sync failures: %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v0, v5}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/af/a/b/a/a/o;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/l/c;

    if-nez v1, :cond_3

    const-string v1, "Received failure %s not in changes %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    const/4 v0, 0x1

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v7

    aput-object v7, v6, v0

    invoke-static {v1, v6}, Lcom/google/android/gms/fitness/m/a;->f(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_4

    :cond_3
    iget-wide v0, v1, Lcom/google/android/gms/fitness/l/c;->a:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v0, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    move-object v1, v2

    goto/16 :goto_2

    :catch_3
    move-exception v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "unable to clear changeLogs: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_3

    .line 158
    :cond_5
    return-void
.end method

.method private a(Lcom/google/android/gms/fitness/l/z;Lcom/google/android/gms/fitness/sync/d;Lcom/google/android/gms/fitness/data/DataSource;Ljava/lang/String;Lcom/google/android/gms/fitness/data/Device;)V
    .locals 13

    .prologue
    .line 58
    const-string v2, "download data source: %s pageToken: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p3, v3, v4

    const/4 v4, 0x1

    aput-object p4, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 59
    sget-object v2, Lcom/google/android/gms/fitness/g/c;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    .line 60
    const-string v2, "down sync disabled"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 68
    :goto_1
    :try_start_0
    invoke-interface/range {p2 .. p4}, Lcom/google/android/gms/fitness/sync/d;->a(Lcom/google/android/gms/fitness/data/DataSource;Ljava/lang/String;)Lcom/google/android/gms/fitness/apiary/a;
    :try_end_0
    .catch Lcom/google/android/gms/fitness/sync/g; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v12

    .line 78
    iget v2, p0, Lcom/google/android/gms/fitness/sync/b;->d:I

    iget-object v3, v12, Lcom/google/android/gms/fitness/apiary/a;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/gms/fitness/sync/b;->d:I

    .line 79
    iget v2, p0, Lcom/google/android/gms/fitness/sync/b;->d:I

    iget-object v3, v12, Lcom/google/android/gms/fitness/apiary/a;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/gms/fitness/sync/b;->d:I

    .line 82
    iget-object v3, v12, Lcom/google/android/gms/fitness/apiary/a;->b:Ljava/util/List;

    .line 83
    const-string v2, "deletion count: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v4}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 85
    const/4 v2, 0x0

    :try_start_1
    move-object/from16 v0, p3

    invoke-interface {p1, v0, v3, v2}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/data/DataSource;Ljava/util/List;Z)I

    move-result v2

    .line 87
    const-string v4, "deleted %s out of %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v6

    const/4 v2, 0x1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v4, v5}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 93
    :goto_2
    iget-object v3, v12, Lcom/google/android/gms/fitness/apiary/a;->a:Ljava/util/List;

    .line 94
    const-string v2, "insertions count: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v4}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 96
    :try_start_2
    move-object/from16 v0, p3

    invoke-interface {p1, v0, v3}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/data/DataSource;Ljava/util/List;)Ljava/util/Set;

    move-result-object v2

    .line 97
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 98
    const-string v4, "dataPoint already exists locally: %s out of %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v6

    const/4 v2, 0x1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v4, v5}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 105
    :cond_2
    :goto_3
    iget-object v2, v12, Lcom/google/android/gms/fitness/apiary/a;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/af/a/b/a/a/i;

    .line 107
    :try_start_3
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v4, v2, Lcom/google/af/a/b/a/a/i;->a:Lcom/google/af/a/b/a/a/h;

    iget-object v4, v4, Lcom/google/af/a/b/a/a/h;->a:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v4

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v6, v2, Lcom/google/af/a/b/a/a/i;->b:Lcom/google/af/a/b/a/a/h;

    iget-object v6, v6, Lcom/google/af/a/b/a/a/h;->a:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v6

    const/4 v9, 0x0

    move-object v3, p1

    move-object/from16 v8, p3

    invoke-interface/range {v3 .. v9}, Lcom/google/android/gms/fitness/l/z;->a(JJLcom/google/android/gms/fitness/data/DataSource;Z)I

    move-result v3

    .line 113
    const-string v4, "deleted %d in [%s-%s]"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v6

    const/4 v3, 0x1

    iget-object v6, v2, Lcom/google/af/a/b/a/a/i;->a:Lcom/google/af/a/b/a/a/h;

    iget-object v6, v6, Lcom/google/af/a/b/a/a/h;->a:Ljava/lang/Long;

    aput-object v6, v5, v3

    const/4 v3, 0x2

    iget-object v6, v2, Lcom/google/af/a/b/a/a/i;->b:Lcom/google/af/a/b/a/a/h;

    iget-object v6, v6, Lcom/google/af/a/b/a/a/h;->a:Ljava/lang/Long;

    aput-object v6, v5, v3

    invoke-static {v4, v5}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_4

    .line 115
    :catch_0
    move-exception v3

    const-string v4, "unable to delete range: %s source:%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v2, 0x1

    aput-object p3, v5, v2

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_4

    .line 69
    :catch_1
    move-exception v2

    .line 70
    const-string v3, "unable to list: %s pageToken: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p3, v4, v5

    const/4 v5, 0x1

    aput-object p4, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 71
    invoke-virtual {v2}, Lcom/google/android/gms/fitness/sync/g;->a()Z

    move-result v3

    if-nez v3, :cond_0

    .line 74
    throw v2

    .line 88
    :catch_2
    move-exception v2

    const-string v4, "could not delete batch: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v6

    invoke-static {v2, v4, v5}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_2

    .line 101
    :catch_3
    move-exception v2

    const-string v4, "unable to insert batch: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v6

    invoke-static {v2, v4, v5}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_3

    .line 123
    :cond_3
    const-wide/16 v8, -0x1

    .line 124
    move-object/from16 v0, p3

    move-object/from16 v1, p5

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/sync/b;->a(Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/Device;)Z

    move-result v5

    .line 125
    if-nez p4, :cond_4

    if-eqz v5, :cond_4

    .line 126
    invoke-static/range {p3 .. p3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/google/android/gms/fitness/l/z;->a(Ljava/util/Collection;)J

    move-result-wide v8

    .line 129
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 130
    iget-object v10, v12, Lcom/google/android/gms/fitness/apiary/a;->d:Ljava/lang/String;

    .line 131
    new-instance v3, Lcom/google/android/gms/fitness/c/f;

    move-object/from16 v4, p3

    invoke-direct/range {v3 .. v10}, Lcom/google/android/gms/fitness/c/f;-><init>(Lcom/google/android/gms/fitness/data/DataSource;ZJJLjava/lang/String;)V

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    invoke-virtual {v12}, Lcom/google/android/gms/fitness/apiary/a;->a()Z

    move-result v2

    if-nez v2, :cond_5

    .line 134
    invoke-interface {p1, v11}, Lcom/google/android/gms/fitness/l/z;->b(Ljava/util/List;)V

    goto/16 :goto_0

    :cond_5
    move-object/from16 p4, v10

    goto/16 :goto_1
.end method

.method static a(Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/Device;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 143
    invoke-static {p0}, Lcom/google/android/gms/fitness/a/e;->a(Lcom/google/android/gms/fitness/data/DataSource;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 148
    :cond_0
    :goto_0
    return v0

    .line 147
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/DataSource;->f()Lcom/google/android/gms/fitness/data/Device;

    move-result-object v1

    .line 148
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/Device;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Device;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/fitness/sync/d;Lcom/google/android/gms/fitness/l/z;Lcom/google/android/gms/fitness/k/a;)V
    .locals 26

    .prologue
    .line 244
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    new-instance v13, Lcom/google/android/gms/fitness/sync/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/fitness/sync/b;->e:Lcom/google/android/gms/fitness/data/Device;

    invoke-direct {v13, v4}, Lcom/google/android/gms/fitness/sync/a;-><init>(Lcom/google/android/gms/fitness/data/Device;)V

    new-instance v16, Ljava/util/HashSet;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashSet;-><init>()V

    invoke-interface/range {p2 .. p2}, Lcom/google/android/gms/fitness/l/z;->o_()Ljava/util/List;

    move-result-object v6

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p2

    invoke-interface {v0, v6}, Lcom/google/android/gms/fitness/l/z;->c(Ljava/util/Collection;)Landroid/support/v4/g/s;

    move-result-object v12

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/fitness/data/DataSource;

    invoke-virtual {v12, v4}, Landroid/support/v4/g/s;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/fitness/c/f;

    if-eqz v5, :cond_1

    iget-wide v10, v5, Lcom/google/android/gms/fitness/c/f;->c:J

    const-wide/16 v18, 0x0

    cmp-long v5, v10, v18

    if-nez v5, :cond_0

    :cond_1
    invoke-static {v4}, Lcom/google/android/gms/fitness/a/e;->a(Lcom/google/android/gms/fitness/data/DataSource;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 245
    :catch_0
    move-exception v4

    .line 247
    new-instance v5, Lcom/google/android/gms/fitness/sync/g;

    const/4 v6, 0x0

    invoke-direct {v5, v6, v4}, Lcom/google/android/gms/fitness/sync/g;-><init>(ZLjava/lang/Throwable;)V

    throw v5

    .line 244
    :cond_2
    :try_start_1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v4

    iput v4, v13, Lcom/google/android/gms/fitness/sync/a;->b:I

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    sget-object v4, Lcom/google/android/gms/fitness/g/c;->n:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_3
    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Lcom/google/android/gms/fitness/sync/d;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/fitness/data/DataSource;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "unable to sync: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v4, v7}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    :cond_4
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_2
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/fitness/data/DataSource;

    invoke-virtual {v12, v5}, Landroid/support/v4/g/s;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/fitness/c/f;

    if-eqz v4, :cond_5

    iget-wide v6, v4, Lcom/google/android/gms/fitness/c/f;->c:J

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-nez v4, :cond_7

    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    iget-object v4, v13, Lcom/google/android/gms/fitness/sync/a;->a:Lcom/google/android/gms/fitness/data/Device;

    invoke-static {v5, v4}, Lcom/google/android/gms/fitness/sync/b;->a(Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/Device;)Z

    move-result v19

    const-wide/16 v10, -0x1

    if-nez v19, :cond_11

    sget-object v6, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    sget-object v4, Lcom/google/android/gms/fitness/g/c;->t:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v8, v4

    invoke-virtual {v6, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    sub-long v6, v20, v6

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v6

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide/from16 v0, v20

    invoke-virtual {v4, v0, v1}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v8

    move-object/from16 v4, p1

    invoke-interface/range {v4 .. v9}, Lcom/google/android/gms/fitness/sync/d;->a(Lcom/google/android/gms/fitness/data/DataSource;JJ)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_11

    move-object/from16 v0, p2

    invoke-interface {v0, v5, v4}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/data/DataSource;Ljava/util/List;)Ljava/util/Set;

    const-string v10, "Fitness"

    const/4 v11, 0x2

    invoke-static {v10, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_6

    const-string v10, "backfilling %1$s: %2$tF %2$tT-%3$tF %3$tT %4$d points"

    const/4 v11, 0x4

    new-array v11, v11, [Ljava/lang/Object;

    const/16 v22, 0x0

    invoke-virtual {v5}, Lcom/google/android/gms/fitness/data/DataSource;->h()Ljava/lang/String;

    move-result-object v23

    aput-object v23, v11, v22

    const/16 v22, 0x1

    sget-object v23, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, v23

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    aput-object v23, v11, v22

    const/16 v22, 0x2

    sget-object v23, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, v23

    invoke-virtual {v0, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v11, v22

    const/4 v8, 0x3

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v11, v8

    invoke-static {v10, v11}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_6
    move-wide v9, v6

    :goto_3
    new-instance v4, Lcom/google/android/gms/fitness/c/f;

    const/4 v11, 0x0

    move/from16 v6, v19

    move-wide/from16 v7, v20

    invoke-direct/range {v4 .. v11}, Lcom/google/android/gms/fitness/c/f;-><init>(Lcom/google/android/gms/fitness/data/DataSource;ZJJLjava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    invoke-virtual {v5}, Lcom/google/android/gms/fitness/data/DataSource;->h()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    :cond_8
    :try_start_2
    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/l/z;->b(Ljava/util/List;)V
    :try_end_2
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_2 .. :try_end_2} :catch_1

    :goto_4
    :try_start_3
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/fitness/sync/d;->a()Ljava/util/Collection;
    :try_end_3
    .catch Lcom/google/android/gms/fitness/sync/g; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v4

    :try_start_4
    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v5

    iput v5, v13, Lcom/google/android/gms/fitness/sync/a;->c:I

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_9
    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/fitness/data/DataSource;

    invoke-virtual {v6}, Lcom/google/android/gms/fitness/data/DataSource;->h()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    iget-object v5, v13, Lcom/google/android/gms/fitness/sync/a;->a:Lcom/google/android/gms/fitness/data/Device;

    invoke-static {v6, v5}, Lcom/google/android/gms/fitness/sync/b;->a(Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/Device;)Z

    move-result v7

    new-instance v5, Lcom/google/android/gms/fitness/c/f;

    const-wide/16 v8, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x0

    invoke-direct/range {v5 .. v12}, Lcom/google/android/gms/fitness/c/f;-><init>(Lcom/google/android/gms/fitness/data/DataSource;ZJJLjava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :catch_1
    move-exception v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "unable to save sync status: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_4

    :catch_2
    move-exception v4

    const-string v5, "unable to download data sources"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/sync/g;->a()Z

    move-result v5

    if-nez v5, :cond_b

    throw v4
    :try_end_4
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_4 .. :try_end_4} :catch_0

    :cond_a
    :try_start_5
    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/l/z;->b(Ljava/util/List;)V
    :try_end_5
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_5 .. :try_end_5} :catch_3

    :cond_b
    :goto_6
    :try_start_6
    iget v4, v13, Lcom/google/android/gms/fitness/sync/a;->b:I

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/gms/fitness/sync/b;->a:I

    iget v4, v13, Lcom/google/android/gms/fitness/sync/a;->c:I

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/gms/fitness/sync/b;->b:I

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v0, v14, v15, v1, v2}, Lcom/google/android/gms/fitness/sync/b;->a(JLcom/google/android/gms/fitness/l/z;Lcom/google/android/gms/fitness/sync/d;)V

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/fitness/k/a;->b()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_c
    :goto_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/fitness/c/c;

    iget-object v7, v4, Lcom/google/android/gms/fitness/c/c;->b:Lcom/google/android/gms/fitness/data/Subscription;

    invoke-virtual {v7}, Lcom/google/android/gms/fitness/data/Subscription;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gms/fitness/a/e;->a(Lcom/google/android/gms/fitness/data/DataSource;)Z

    move-result v8

    if-nez v8, :cond_c

    sget-object v8, Lcom/google/android/gms/fitness/c/e;->b:Lcom/google/android/gms/fitness/c/e;

    iget-object v4, v4, Lcom/google/android/gms/fitness/c/c;->c:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v8, v4}, Lcom/google/android/gms/fitness/c/e;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {v5, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :catch_3
    move-exception v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "unable to save: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_6

    :cond_d
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_8
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/gms/fitness/data/DataSource;

    move-object/from16 v0, p2

    invoke-interface {v0, v7}, Lcom/google/android/gms/fitness/l/z;->c(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/c/f;

    move-result-object v4

    if-eqz v4, :cond_f

    iget-object v8, v4, Lcom/google/android/gms/fitness/c/f;->e:Ljava/lang/String;

    iget-boolean v4, v4, Lcom/google/android/gms/fitness/c/f;->b:Z

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/fitness/sync/b;->e:Lcom/google/android/gms/fitness/data/Device;

    move-object/from16 v4, p0

    move-object/from16 v5, p2

    move-object/from16 v6, p1

    invoke-direct/range {v4 .. v9}, Lcom/google/android/gms/fitness/sync/b;->a(Lcom/google/android/gms/fitness/l/z;Lcom/google/android/gms/fitness/sync/d;Lcom/google/android/gms/fitness/data/DataSource;Ljava/lang/String;Lcom/google/android/gms/fitness/data/Device;)V

    goto :goto_8

    :cond_e
    const-string v4, "Skipping download of local data source: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v7}, Lcom/google/android/gms/fitness/data/DataSource;->h()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/gms/fitness/m/a;->e(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_8

    :cond_f
    const-string v4, "Data source [%s] is not found."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_6
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_8

    .line 248
    :cond_10
    return-void

    :cond_11
    move-wide v9, v10

    goto/16 :goto_3
.end method
