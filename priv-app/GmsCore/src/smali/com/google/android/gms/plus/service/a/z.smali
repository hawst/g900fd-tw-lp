.class public final Lcom/google/android/gms/plus/service/a/z;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/android/gms/plus/internal/f;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lcom/google/android/gms/plus/internal/f;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/z;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 37
    iput p2, p0, Lcom/google/android/gms/plus/service/a/z;->b:I

    .line 38
    iput-object p3, p0, Lcom/google/android/gms/plus/service/a/z;->c:Ljava/lang/String;

    .line 39
    iput-object p4, p0, Lcom/google/android/gms/plus/service/a/z;->d:Lcom/google/android/gms/plus/internal/f;

    .line 40
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 45
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/z;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget v1, p0, Lcom/google/android/gms/plus/service/a/z;->b:I

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/z;->c:Ljava/lang/String;

    iget-object v1, p2, Lcom/google/android/gms/plus/b/b;->e:Lcom/google/android/gms/plus/b/d;

    const-string v2, "third_party"

    invoke-virtual {v1, p1, v0, v2}, Lcom/google/android/gms/plus/b/d;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 47
    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/z;->d:Lcom/google/android/gms/plus/internal/f;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v0, v1}, Lcom/google/android/gms/plus/internal/f;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    .line 62
    :goto_0
    return-void

    .line 48
    :catch_0
    move-exception v0

    .line 49
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 50
    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v4, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 51
    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/z;->d:Lcom/google/android/gms/plus/internal/f;

    invoke-static {v5, v1}, Lcom/google/android/gms/common/data/DataHolder;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lcom/google/android/gms/plus/internal/f;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0

    .line 55
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/z;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 56
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/z;->d:Lcom/google/android/gms/plus/internal/f;

    invoke-static {v5, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-interface {v1, v0, v3}, Lcom/google/android/gms/plus/internal/f;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0

    .line 59
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/z;->d:Lcom/google/android/gms/plus/internal/f;

    const/4 v1, 0x7

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lcom/google/android/gms/plus/internal/f;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/z;->d:Lcom/google/android/gms/plus/internal/f;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/z;->d:Lcom/google/android/gms/plus/internal/f;

    const/16 v1, 0x8

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/f;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    .line 70
    :cond_0
    return-void
.end method
