.class public final Lcom/google/android/gms/location/internal/w;
.super Lcom/google/android/gms/location/internal/a;
.source "SourceFile"


# instance fields
.field private final g:Lcom/google/android/gms/location/internal/t;

.field private final h:Lcom/google/android/gms/location/copresence/internal/c;

.field private final i:Lcom/google/android/gms/audiomodem/a/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 75
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/location/internal/w;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 83
    invoke-direct/range {p0 .. p5}, Lcom/google/android/gms/location/internal/a;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;Ljava/lang/String;)V

    .line 84
    new-instance v0, Lcom/google/android/gms/location/internal/t;

    iget-object v1, p0, Lcom/google/android/gms/location/internal/w;->a:Lcom/google/android/gms/location/internal/z;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/location/internal/t;-><init>(Landroid/content/Context;Lcom/google/android/gms/location/internal/z;)V

    iput-object v0, p0, Lcom/google/android/gms/location/internal/w;->g:Lcom/google/android/gms/location/internal/t;

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/location/internal/w;->a:Lcom/google/android/gms/location/internal/z;

    invoke-static {p1, p6, p7, v0}, Lcom/google/android/gms/location/copresence/internal/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/internal/z;)Lcom/google/android/gms/location/copresence/internal/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/location/internal/w;->h:Lcom/google/android/gms/location/copresence/internal/c;

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/location/internal/w;->a:Lcom/google/android/gms/location/internal/z;

    invoke-static {p1, v0}, Lcom/google/android/gms/audiomodem/a/d;->a(Landroid/content/Context;Lcom/google/android/gms/location/internal/z;)Lcom/google/android/gms/audiomodem/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/location/internal/w;->i:Lcom/google/android/gms/audiomodem/a/d;

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 56
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/location/internal/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;Ljava/lang/String;)V

    .line 57
    new-instance v0, Lcom/google/android/gms/location/internal/t;

    iget-object v1, p0, Lcom/google/android/gms/location/internal/w;->a:Lcom/google/android/gms/location/internal/z;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/location/internal/t;-><init>(Landroid/content/Context;Lcom/google/android/gms/location/internal/z;)V

    iput-object v0, p0, Lcom/google/android/gms/location/internal/w;->g:Lcom/google/android/gms/location/internal/t;

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/location/internal/w;->a:Lcom/google/android/gms/location/internal/z;

    invoke-static {p1, v2, v2, v0}, Lcom/google/android/gms/location/copresence/internal/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/internal/z;)Lcom/google/android/gms/location/copresence/internal/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/location/internal/w;->h:Lcom/google/android/gms/location/copresence/internal/c;

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/location/internal/w;->a:Lcom/google/android/gms/location/internal/z;

    invoke-static {p1, v0}, Lcom/google/android/gms/audiomodem/a/d;->a(Landroid/content/Context;Lcom/google/android/gms/location/internal/z;)Lcom/google/android/gms/audiomodem/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/location/internal/w;->i:Lcom/google/android/gms/audiomodem/a/d;

    .line 61
    return-void
.end method


# virtual methods
.method public final a(Landroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/gms/location/internal/w;->g:Lcom/google/android/gms/location/internal/t;

    iget-object v1, v0, Lcom/google/android/gms/location/internal/t;->a:Lcom/google/android/gms/location/internal/z;

    invoke-interface {v1}, Lcom/google/android/gms/location/internal/z;->a()V

    iget-object v0, v0, Lcom/google/android/gms/location/internal/t;->a:Lcom/google/android/gms/location/internal/z;

    invoke-interface {v0}, Lcom/google/android/gms/location/internal/z;->b()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/q;

    invoke-interface {v0, p1}, Lcom/google/android/gms/location/internal/q;->b(Landroid/app/PendingIntent;)V

    .line 283
    return-void
.end method

.method public final a(Landroid/location/Location;I)V
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/gms/location/internal/w;->g:Lcom/google/android/gms/location/internal/t;

    iget-object v1, v0, Lcom/google/android/gms/location/internal/t;->a:Lcom/google/android/gms/location/internal/z;

    invoke-interface {v1}, Lcom/google/android/gms/location/internal/z;->a()V

    iget-object v0, v0, Lcom/google/android/gms/location/internal/t;->a:Lcom/google/android/gms/location/internal/z;

    invoke-interface {v0}, Lcom/google/android/gms/location/internal/z;->b()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/q;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/location/internal/q;->a(Landroid/location/Location;I)V

    .line 305
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/n;Landroid/os/Looper;)V
    .locals 3

    .prologue
    .line 244
    iget-object v1, p0, Lcom/google/android/gms/location/internal/w;->g:Lcom/google/android/gms/location/internal/t;

    monitor-enter v1

    .line 245
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/location/internal/w;->g:Lcom/google/android/gms/location/internal/t;

    iget-object v2, v0, Lcom/google/android/gms/location/internal/t;->a:Lcom/google/android/gms/location/internal/z;

    invoke-interface {v2}, Lcom/google/android/gms/location/internal/z;->a()V

    invoke-virtual {v0, p2, p3}, Lcom/google/android/gms/location/internal/t;->a(Lcom/google/android/gms/location/n;Landroid/os/Looper;)Lcom/google/android/gms/location/internal/v;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/gms/location/internal/t;->a:Lcom/google/android/gms/location/internal/z;

    invoke-interface {v0}, Lcom/google/android/gms/location/internal/z;->b()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/q;

    invoke-interface {v0, p1, v2}, Lcom/google/android/gms/location/internal/q;->a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/j;)V

    .line 246
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/location/internal/LocationRequestInternal;Landroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/gms/location/internal/w;->g:Lcom/google/android/gms/location/internal/t;

    iget-object v1, v0, Lcom/google/android/gms/location/internal/t;->a:Lcom/google/android/gms/location/internal/z;

    invoke-interface {v1}, Lcom/google/android/gms/location/internal/z;->a()V

    iget-object v0, v0, Lcom/google/android/gms/location/internal/t;->a:Lcom/google/android/gms/location/internal/z;

    invoke-interface {v0}, Lcom/google/android/gms/location/internal/z;->b()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/q;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/location/internal/q;->a(Lcom/google/android/gms/location/internal/LocationRequestInternal;Landroid/app/PendingIntent;)V

    .line 256
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/internal/LocationRequestInternal;Lcom/google/android/gms/location/n;Landroid/os/Looper;)V
    .locals 3

    .prologue
    .line 230
    iget-object v1, p0, Lcom/google/android/gms/location/internal/w;->g:Lcom/google/android/gms/location/internal/t;

    monitor-enter v1

    .line 231
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/location/internal/w;->g:Lcom/google/android/gms/location/internal/t;

    iget-object v2, v0, Lcom/google/android/gms/location/internal/t;->a:Lcom/google/android/gms/location/internal/z;

    invoke-interface {v2}, Lcom/google/android/gms/location/internal/z;->a()V

    invoke-virtual {v0, p2, p3}, Lcom/google/android/gms/location/internal/t;->a(Lcom/google/android/gms/location/n;Landroid/os/Looper;)Lcom/google/android/gms/location/internal/v;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/gms/location/internal/t;->a:Lcom/google/android/gms/location/internal/z;

    invoke-interface {v0}, Lcom/google/android/gms/location/internal/z;->b()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/q;

    invoke-interface {v0, p1, v2}, Lcom/google/android/gms/location/internal/q;->a(Lcom/google/android/gms/location/internal/LocationRequestInternal;Lcom/google/android/gms/location/j;)V

    .line 232
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/location/n;)V
    .locals 4

    .prologue
    .line 272
    iget-object v1, p0, Lcom/google/android/gms/location/internal/w;->g:Lcom/google/android/gms/location/internal/t;

    iget-object v0, v1, Lcom/google/android/gms/location/internal/t;->a:Lcom/google/android/gms/location/internal/z;

    invoke-interface {v0}, Lcom/google/android/gms/location/internal/z;->a()V

    const-string v0, "Invalid null listener"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v1, Lcom/google/android/gms/location/internal/t;->d:Ljava/util/HashMap;

    monitor-enter v2

    :try_start_0
    iget-object v0, v1, Lcom/google/android/gms/location/internal/t;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/v;

    iget-object v3, v1, Lcom/google/android/gms/location/internal/t;->b:Landroid/content/ContentProviderClient;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/google/android/gms/location/internal/t;->d:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/google/android/gms/location/internal/t;->b:Landroid/content/ContentProviderClient;

    invoke-virtual {v3}, Landroid/content/ContentProviderClient;->release()Z

    const/4 v3, 0x0

    iput-object v3, v1, Lcom/google/android/gms/location/internal/t;->b:Landroid/content/ContentProviderClient;

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/v;->a()V

    iget-object v1, v1, Lcom/google/android/gms/location/internal/t;->a:Lcom/google/android/gms/location/internal/z;

    invoke-interface {v1}, Lcom/google/android/gms/location/internal/z;->b()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/location/internal/q;

    invoke-interface {v1, v0}, Lcom/google/android/gms/location/internal/q;->a(Lcom/google/android/gms/location/j;)V

    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 94
    iget-object v2, p0, Lcom/google/android/gms/location/internal/w;->g:Lcom/google/android/gms/location/internal/t;

    monitor-enter v2

    .line 95
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/location/internal/w;->c_()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    :try_start_1
    iget-object v3, p0, Lcom/google/android/gms/location/internal/w;->g:Lcom/google/android/gms/location/internal/t;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v4, v3, Lcom/google/android/gms/location/internal/t;->d:Ljava/util/HashMap;

    monitor-enter v4
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    iget-object v0, v3, Lcom/google/android/gms/location/internal/t;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/v;

    if-eqz v0, :cond_0

    iget-object v1, v3, Lcom/google/android/gms/location/internal/t;->a:Lcom/google/android/gms/location/internal/z;

    invoke-interface {v1}, Lcom/google/android/gms/location/internal/z;->b()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/location/internal/q;

    invoke-interface {v1, v0}, Lcom/google/android/gms/location/internal/q;->a(Lcom/google/android/gms/location/j;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v4

    throw v0
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catch_0
    move-exception v0

    :try_start_5
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 102
    :catch_1
    move-exception v0

    .line 107
    :try_start_6
    const-string v1, "LocationClientImpl"

    const-string v3, "Client disconnected before listeners could be cleaned up"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 110
    :cond_1
    :goto_1
    invoke-super {p0}, Lcom/google/android/gms/location/internal/a;->b()V

    .line 111
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    return-void

    .line 98
    :cond_2
    :try_start_7
    iget-object v0, v3, Lcom/google/android/gms/location/internal/t;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 101
    :try_start_8
    iget-object v1, p0, Lcom/google/android/gms/location/internal/w;->g:Lcom/google/android/gms/location/internal/t;

    iget-boolean v0, v1, Lcom/google/android/gms/location/internal/t;->c:Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    if-eqz v0, :cond_1

    :try_start_9
    iget-object v0, v1, Lcom/google/android/gms/location/internal/t;->a:Lcom/google/android/gms/location/internal/z;

    invoke-interface {v0}, Lcom/google/android/gms/location/internal/z;->a()V

    iget-object v0, v1, Lcom/google/android/gms/location/internal/t;->a:Lcom/google/android/gms/location/internal/z;

    invoke-interface {v0}, Lcom/google/android/gms/location/internal/z;->b()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/q;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Lcom/google/android/gms/location/internal/q;->a(Z)V

    const/4 v0, 0x0

    iput-boolean v0, v1, Lcom/google/android/gms/location/internal/t;->c:Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_1

    :catch_2
    move-exception v0

    :try_start_a
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 111
    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final f()Landroid/location/Location;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/gms/location/internal/w;->g:Lcom/google/android/gms/location/internal/t;

    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/t;->a()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public final g()Lcom/google/android/gms/location/copresence/internal/c;
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/google/android/gms/location/internal/w;->h:Lcom/google/android/gms/location/copresence/internal/c;

    return-object v0
.end method

.method public final l()Lcom/google/android/gms/audiomodem/a/d;
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/android/gms/location/internal/w;->i:Lcom/google/android/gms/audiomodem/a/d;

    return-object v0
.end method
