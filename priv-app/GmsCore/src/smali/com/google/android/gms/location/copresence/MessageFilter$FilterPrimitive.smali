.class public Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:Lcom/google/android/gms/location/copresence/AccessKey;

.field public final d:J

.field public final e:Lcom/google/android/gms/location/copresence/People;

.field public f:Ljava/lang/String;

.field private final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/gms/location/copresence/q;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/q;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/AccessKey;JLcom/google/android/gms/location/copresence/People;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput p1, p0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->g:I

    .line 79
    iput-object p2, p0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->a:Ljava/lang/String;

    .line 80
    iput-object p3, p0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->b:Ljava/lang/String;

    .line 81
    iput-object p4, p0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->c:Lcom/google/android/gms/location/copresence/AccessKey;

    .line 82
    iput-wide p5, p0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->d:J

    .line 83
    iput-object p7, p0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->e:Lcom/google/android/gms/location/copresence/People;

    .line 84
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/AccessKey;JLcom/google/android/gms/location/copresence/People;)V
    .locals 2

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->g:I

    .line 92
    iput-object p1, p0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->a:Ljava/lang/String;

    .line 93
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->b:Ljava/lang/String;

    .line 94
    iput-object p3, p0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->c:Lcom/google/android/gms/location/copresence/AccessKey;

    .line 95
    iput-wide p4, p0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->d:J

    .line 96
    invoke-static {p6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/People;

    iput-object v0, p0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->e:Lcom/google/android/gms/location/copresence/People;

    .line 97
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/AccessKey;JLcom/google/android/gms/location/copresence/People;B)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 38
    move-object v0, p0

    move-object v2, p2

    move-object v3, v1

    move-wide v4, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/AccessKey;JLcom/google/android/gms/location/copresence/People;)V

    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->g:I

    return v0
.end method

.method public final a(Lcom/google/android/gms/location/copresence/Message;)Z
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p1}, Lcom/google/android/gms/location/copresence/Message;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "(namespace="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", accessKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->c:Lcom/google/android/gms/location/copresence/AccessKey;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ttl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", senders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->e:Lcom/google/android/gms/location/copresence/People;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "REDACTED"

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 118
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/location/copresence/q;->a(Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;Landroid/os/Parcel;I)V

    .line 119
    return-void
.end method
