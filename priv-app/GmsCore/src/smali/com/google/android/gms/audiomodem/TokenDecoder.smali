.class public Lcom/google/android/gms/audiomodem/TokenDecoder;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/audiomodem/ac;


# instance fields
.field private volatile a:I

.field private b:J

.field private c:J

.field private final d:Ljava/util/Set;

.field private final e:Ljava/util/Set;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private final j:Lcom/google/android/gms/audiomodem/Encoding;

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/audiomodem/Encoding;III)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->a:I

    .line 41
    iput v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->f:I

    .line 54
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->j:Lcom/google/android/gms/audiomodem/Encoding;

    .line 55
    iput p2, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->k:I

    .line 56
    iput p3, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->l:I

    .line 57
    iput p4, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->m:I

    .line 58
    sget-object v0, Lcom/google/android/gms/audiomodem/m;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/audiomodem/TokenDecoder;->a(J)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->n:J

    .line 60
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->d:Ljava/util/Set;

    .line 62
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->e:Ljava/util/Set;

    .line 65
    invoke-direct {p0}, Lcom/google/android/gms/audiomodem/TokenDecoder;->c()Z

    .line 66
    return-void
.end method

.method private a(J)I
    .locals 5

    .prologue
    .line 277
    long-to-double v0, p1

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    iget v2, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->l:I

    int-to-double v2, v2

    mul-double/2addr v0, v2

    iget v2, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->m:I

    int-to-double v2, v2

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method private c()Z
    .locals 21

    .prologue
    .line 69
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gms/audiomodem/TokenDecoder;->f:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 70
    const-string v2, "audioModem"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 71
    const-string v2, "TokenDecoder: tryInitialize failed because TokenDecoder has been released"

    const-string v3, "audioModem"

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_0
    const/4 v2, 0x0

    .line 138
    :goto_0
    return v2

    .line 75
    :cond_1
    invoke-static {}, Lcom/google/android/gms/audiomodem/av;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 76
    const/4 v2, 0x0

    goto :goto_0

    .line 78
    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gms/audiomodem/TokenDecoder;->f:I

    if-nez v2, :cond_5

    .line 79
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/audiomodem/TokenDecoder;->j:Lcom/google/android/gms/audiomodem/Encoding;

    invoke-virtual {v2}, Lcom/google/android/gms/audiomodem/Encoding;->b()I

    move-result v2

    .line 80
    packed-switch v2, :pswitch_data_0

    .line 130
    const-string v2, "audioModem"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 131
    const-string v2, "TokenDecoder: Received an Encoder with unknown type"

    const-string v3, "audioModem"

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 82
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/audiomodem/TokenDecoder;->j:Lcom/google/android/gms/audiomodem/Encoding;

    invoke-virtual {v2}, Lcom/google/android/gms/audiomodem/Encoding;->c()Lcom/google/android/gms/audiomodem/DsssEncoding;

    move-result-object v20

    .line 83
    const-string v2, "audioModem"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 84
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TokenDecoder: Initializing DSSS decoder with shouldUseOdp: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/gms/audiomodem/m;->A:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "audioModem"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gms/audiomodem/TokenDecoder;->k:I

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/audiomodem/DsssEncoding;->n()I

    move-result v3

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/audiomodem/DsssEncoding;->c()Z

    move-result v4

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/audiomodem/DsssEncoding;->d()Z

    move-result v5

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/audiomodem/DsssEncoding;->e()I

    move-result v6

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/audiomodem/DsssEncoding;->f()I

    move-result v7

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/audiomodem/DsssEncoding;->g()F

    move-result v8

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/audiomodem/DsssEncoding;->h()I

    move-result v9

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/audiomodem/DsssEncoding;->i()F

    move-result v10

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/audiomodem/DsssEncoding;->j()I

    move-result v11

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/audiomodem/DsssEncoding;->k()I

    move-result v12

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/audiomodem/DsssEncoding;->l()I

    move-result v13

    sget-object v14, Lcom/google/android/gms/audiomodem/m;->A:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v14}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Boolean;

    invoke-virtual {v14}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v14

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/audiomodem/DsssEncoding;->e()I

    move-result v15

    int-to-double v0, v15

    move-wide/from16 v18, v0

    invoke-static/range {v16 .. v19}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-int v15, v0

    add-int/lit8 v15, v15, -0x1

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/audiomodem/DsssEncoding;->h()I

    move-result v16

    mul-int v15, v15, v16

    int-to-float v15, v15

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/audiomodem/DsssEncoding;->g()F

    move-result v16

    div-float v16, v15, v16

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/audiomodem/DsssEncoding;->n()I

    move-result v15

    const/16 v17, 0x8

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/audiomodem/DsssEncoding;->j()I

    move-result v18

    div-int v17, v17, v18

    mul-int v17, v17, v15

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/audiomodem/DsssEncoding;->c()Z

    move-result v15

    if-eqz v15, :cond_6

    const/4 v15, 0x2

    :goto_1
    add-int v15, v15, v17

    int-to-float v15, v15

    mul-float v16, v16, v15

    sget-object v15, Lcom/google/android/gms/audiomodem/m;->y:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v15}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Float;

    invoke-virtual {v15}, Ljava/lang/Float;->floatValue()F

    move-result v15

    mul-float v15, v15, v16

    sget-object v16, Lcom/google/android/gms/audiomodem/m;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/Long;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-float v0, v0

    move/from16 v16, v0

    const/high16 v17, 0x447a0000    # 1000.0f

    div-float v16, v16, v17

    sget-object v17, Lcom/google/android/gms/audiomodem/m;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/audiomodem/TokenDecoder;->l:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/audiomodem/TokenDecoder;->m:I

    move/from16 v19, v0

    invoke-static/range {v2 .. v19}, Lcom/google/android/gms/audiomodem/TokenDecoder;->nativeInitializeDsss(IIZZIIFIFIIIZFFIFI)V

    .line 107
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/audiomodem/DsssEncoding;->b()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/gms/audiomodem/TokenDecoder;->h:I

    .line 108
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/gms/audiomodem/DsssEncoding;->m()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/gms/audiomodem/TokenDecoder;->i:I

    .line 135
    :goto_2
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/gms/audiomodem/TokenDecoder;->f:I

    .line 136
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gms/audiomodem/TokenDecoder;->k:I

    invoke-static {v2}, Lcom/google/android/gms/audiomodem/TokenDecoder;->nativeGetMaxSafeInputSize(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/gms/audiomodem/TokenDecoder;->g:I

    .line 138
    :cond_5
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 87
    :cond_6
    const/4 v15, 0x1

    goto :goto_1

    .line 111
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/audiomodem/TokenDecoder;->j:Lcom/google/android/gms/audiomodem/Encoding;

    invoke-virtual {v2}, Lcom/google/android/gms/audiomodem/Encoding;->d()Lcom/google/android/gms/audiomodem/DtmfEncoding;

    move-result-object v14

    .line 112
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gms/audiomodem/TokenDecoder;->k:I

    invoke-virtual {v14}, Lcom/google/android/gms/audiomodem/DtmfEncoding;->i()I

    move-result v3

    invoke-virtual {v14}, Lcom/google/android/gms/audiomodem/DtmfEncoding;->c()Z

    move-result v4

    invoke-virtual {v14}, Lcom/google/android/gms/audiomodem/DtmfEncoding;->d()F

    move-result v5

    invoke-virtual {v14}, Lcom/google/android/gms/audiomodem/DtmfEncoding;->e()I

    move-result v6

    invoke-virtual {v14}, Lcom/google/android/gms/audiomodem/DtmfEncoding;->f()I

    move-result v7

    int-to-float v7, v7

    const/high16 v8, 0x447a0000    # 1000.0f

    div-float/2addr v7, v8

    invoke-virtual {v14}, Lcom/google/android/gms/audiomodem/DtmfEncoding;->g()I

    move-result v8

    invoke-virtual {v14}, Lcom/google/android/gms/audiomodem/DtmfEncoding;->b()I

    move-result v9

    int-to-float v10, v9

    sget-object v9, Lcom/google/android/gms/audiomodem/m;->z:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v9}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Float;

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v9

    mul-float/2addr v9, v10

    sget-object v10, Lcom/google/android/gms/audiomodem/m;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v10}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    long-to-float v10, v10

    const/high16 v11, 0x447a0000    # 1000.0f

    div-float/2addr v10, v11

    sget-object v11, Lcom/google/android/gms/audiomodem/m;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v11}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/gms/audiomodem/TokenDecoder;->l:I

    int-to-float v12, v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/gms/audiomodem/TokenDecoder;->m:I

    invoke-static/range {v2 .. v13}, Lcom/google/android/gms/audiomodem/TokenDecoder;->nativeInitializeDtmf(IIZFIFIFFIFI)V

    .line 126
    invoke-virtual {v14}, Lcom/google/android/gms/audiomodem/DtmfEncoding;->b()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/gms/audiomodem/TokenDecoder;->h:I

    .line 127
    invoke-virtual {v14}, Lcom/google/android/gms/audiomodem/DtmfEncoding;->h()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/gms/audiomodem/TokenDecoder;->i:I

    goto/16 :goto_2

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private d()V
    .locals 4

    .prologue
    .line 223
    invoke-direct {p0}, Lcom/google/android/gms/audiomodem/TokenDecoder;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 237
    :cond_0
    return-void

    .line 226
    :cond_1
    new-instance v0, Lcom/google/whispernet/Data$DecodedTokens;

    invoke-direct {v0}, Lcom/google/whispernet/Data$DecodedTokens;-><init>()V

    .line 227
    iget v1, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->k:I

    iget v2, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->h:I

    iget v3, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->i:I

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/audiomodem/TokenDecoder;->nativeGetTokens(ILcom/google/whispernet/Data$DecodedTokens;II)V

    .line 228
    iget-object v1, v0, Lcom/google/whispernet/Data$DecodedTokens;->token:[Lcom/google/whispernet/Data$DecodedToken;

    .line 229
    if-eqz v1, :cond_0

    array-length v0, v1

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/audiomodem/q;

    .line 235
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/audiomodem/q;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method private static native nativeDetectBroadcaster(I)Z
.end method

.method private static native nativeGetMaxSafeInputSize(I)I
.end method

.method private static native nativeGetTokens(ILcom/google/whispernet/Data$DecodedTokens;II)V
.end method

.method private static native nativeInitializeDsss(IIZZIIFIFIIIZFFIFI)V
.end method

.method private static native nativeInitializeDtmf(IIZFIFIFFIFI)V
.end method

.method private static native nativeProcessSamples(I[BII)V
.end method

.method private static native nativeWipeInternalData(I)V
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 240
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->f:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 241
    const-string v0, "audioModem"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    const-string v0, "TokenDecoder: Wiping internal data."

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    :cond_0
    iget v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->k:I

    invoke-static {v0}, Lcom/google/android/gms/audiomodem/TokenDecoder;->nativeWipeInternalData(I)V

    .line 246
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->a:I

    .line 247
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    monitor-exit p0

    return-void

    .line 240
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/audiomodem/r;)V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 157
    return-void
.end method

.method public final declared-synchronized a([BI)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 167
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/audiomodem/TokenDecoder;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 220
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 170
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->g:I

    if-le p2, v0, :cond_2

    .line 171
    const-string v0, "audioModem"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TokenDecoder: Process samples input of size "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " exceeds maximum safe size "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; signal may be lost"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :cond_2
    iget v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->k:I

    const/4 v1, 0x0

    invoke-static {v0, p1, v1, p2}, Lcom/google/android/gms/audiomodem/TokenDecoder;->nativeProcessSamples(I[BII)V

    .line 180
    iget v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->k:I

    invoke-static {v0}, Lcom/google/android/gms/audiomodem/TokenDecoder;->nativeDetectBroadcaster(I)Z

    move-result v0

    .line 181
    if-eqz v0, :cond_5

    .line 183
    iget v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->a:I

    if-eq v0, v4, :cond_3

    .line 184
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->a:I

    .line 185
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->c:J

    .line 188
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 189
    :cond_4
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 190
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/audiomodem/r;

    .line 191
    iget-boolean v2, v0, Lcom/google/android/gms/audiomodem/r;->a:Z

    if-eqz v2, :cond_4

    .line 192
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 193
    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/r;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 167
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 197
    :cond_5
    :try_start_2
    iget-wide v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->c:J

    int-to-long v2, p2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->c:J

    .line 198
    iget v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->a:I

    if-ne v0, v4, :cond_6

    .line 199
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->a:I

    .line 202
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 203
    :cond_7
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 204
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/audiomodem/r;

    .line 205
    iget-boolean v2, v0, Lcom/google/android/gms/audiomodem/r;->b:Z

    if-eqz v2, :cond_7

    iget-wide v2, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->c:J

    iget-wide v4, v0, Lcom/google/android/gms/audiomodem/r;->c:J

    invoke-direct {p0, v4, v5}, Lcom/google/android/gms/audiomodem/TokenDecoder;->a(J)I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_7

    .line 208
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 209
    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/r;->b()V

    goto :goto_2

    .line 215
    :cond_8
    iget-wide v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->b:J

    int-to-long v2, p2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->b:J

    .line 216
    iget-wide v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->b:J

    iget-wide v2, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->n:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 217
    invoke-direct {p0}, Lcom/google/android/gms/audiomodem/TokenDecoder;->d()V

    .line 218
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->b:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/gms/audiomodem/q;)Z
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/audiomodem/q;)Z
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/TokenDecoder;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
