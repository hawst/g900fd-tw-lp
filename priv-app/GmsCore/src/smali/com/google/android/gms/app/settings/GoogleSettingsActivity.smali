.class public Lcom/google/android/gms/app/settings/GoogleSettingsActivity;
.super Lcom/google/android/gms/common/widget/a/b;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private a:Lcom/google/android/gms/appstate/e/a;

.field private b:Lcom/google/android/gms/common/widget/a/i;

.field private c:Lcom/google/android/gms/common/widget/a/i;

.field private d:Lcom/google/android/gms/common/widget/a/o;

.field private e:Z

.field private f:Lcom/google/android/gms/common/api/v;

.field private g:Lcom/google/android/location/reporting/service/l;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/gms/common/widget/a/b;-><init>()V

    .line 449
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 446
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/app/settings/GoogleSettingsActivity;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->f:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/app/settings/GoogleSettingsActivity;)Lcom/google/android/gms/common/widget/a/i;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->b:Lcom/google/android/gms/common/widget/a/i;

    return-object v0
.end method


# virtual methods
.method final a(Lcom/google/android/gms/common/widget/a/i;)V
    .locals 2

    .prologue
    .line 266
    iget-boolean v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->e:Z

    if-nez v0, :cond_0

    .line 267
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget v1, Lcom/google/android/gms/p;->dx:I

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/i;Landroid/content/Intent;I)Lcom/google/android/gms/common/widget/a/o;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->e:Z

    .line 271
    :cond_0
    return-void

    .line 267
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/common/widget/a/l;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/16 v8, 0xe

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 105
    sget v0, Lcom/google/android/gms/p;->eQ:I

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/widget/a/l;->f(I)Lcom/google/android/gms/common/widget/a/i;

    move-result-object v1

    .line 106
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.apps.plus.PRIVACY_SETTINGS"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "account"

    new-instance v3, Landroid/accounts/Account;

    const-string v4, "fake"

    const-string v5, "com.google"

    invoke-direct {v3, v4, v5}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    sget v2, Lcom/google/android/gms/p;->fa:I

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/i;Landroid/content/Intent;I)Lcom/google/android/gms/common/widget/a/o;

    .line 107
    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/udc/b/a;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_b

    .line 108
    :cond_0
    :goto_0
    iput-object v1, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->b:Lcom/google/android/gms/common/widget/a/i;

    .line 110
    sget v0, Lcom/google/android/gms/p;->eT:I

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/widget/a/l;->f(I)Lcom/google/android/gms/common/widget/a/i;

    move-result-object v0

    .line 111
    const-string v1, "com.google.android.gms.settings.ADS_PRIVACY"

    invoke-direct {p0, v1}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->ed:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/i;Landroid/content/Intent;I)Lcom/google/android/gms/common/widget/a/o;

    .line 112
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->e()Z

    move-result v1

    if-nez v1, :cond_c

    .line 113
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.fitness.settings.GOOGLE_FITNESS_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/gms/p;->ep:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/i;Landroid/content/Intent;I)Lcom/google/android/gms/common/widget/a/o;

    .line 114
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "com.google.android.gms.games.SHOW_GOOGLE_SETTINGS"

    invoke-direct {p0, v1}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->eq:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/i;Landroid/content/Intent;I)Lcom/google/android/gms/common/widget/a/o;

    .line 115
    :cond_3
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.apps.maps.LOCATION_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/gms/p;->eX:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/i;Landroid/content/Intent;I)Lcom/google/android/gms/common/widget/a/o;

    .line 116
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gms/app/settings/DataManagementActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget v2, Lcom/google/android/gms/p;->eR:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/i;Landroid/content/Intent;I)Lcom/google/android/gms/common/widget/a/o;

    .line 118
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_d

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.googlequicksearchbox.action.PRIVACY_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/gms/p;->fc:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/i;Landroid/content/Intent;I)Lcom/google/android/gms/common/widget/a/o;

    .line 119
    :goto_2
    invoke-static {p0}, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {p0}, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->fe:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/i;Landroid/content/Intent;I)Lcom/google/android/gms/common/widget/a/o;

    .line 120
    :cond_4
    iput-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->c:Lcom/google/android/gms/common/widget/a/i;

    .line 122
    const-string v0, "[internal]"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/widget/a/l;->a(Ljava/lang/CharSequence;)Lcom/google/android/gms/common/widget/a/i;

    move-result-object v1

    .line 123
    invoke-static {v8}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "com.google.android.gms.auth.setup.CONFIG_REMOTE_SETUP"

    invoke-direct {p0, v0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v0, v7}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    const-string v4, "x_auth_setup_configuration_title"

    invoke-virtual {v2, v4, v3, v6}, Landroid/content/res/Resources;->getValue(Ljava/lang/String;Landroid/util/TypedValue;Z)V

    invoke-virtual {v3}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/i;Landroid/content/Intent;Ljava/lang/CharSequence;)Lcom/google/android/gms/common/widget/a/o;

    .line 124
    :cond_5
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/android/gms/common/a/b;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "Download Service debug"

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/i;Landroid/content/Intent;Ljava/lang/CharSequence;)Lcom/google/android/gms/common/widget/a/o;

    .line 125
    :cond_7
    invoke-static {v8}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "com.google.android.gms.auth.setup.PULL_ACCOUNT_SETUP"

    invoke-direct {p0, v0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v0, v7}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    const-string v4, "x_auth_setup_pull_account_title"

    invoke-virtual {v2, v4, v3, v6}, Landroid/content/res/Resources;->getValue(Ljava/lang/String;Landroid/util/TypedValue;Z)V

    invoke-virtual {v3}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/i;Landroid/content/Intent;Ljava/lang/CharSequence;)Lcom/google/android/gms/common/widget/a/o;

    .line 126
    :cond_8
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_9

    sget-object v0, Lcom/google/android/gms/common/a/b;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "People debug"

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/i;Landroid/content/Intent;Ljava/lang/CharSequence;)Lcom/google/android/gms/common/widget/a/o;

    .line 127
    :cond_a
    return-void

    .line 107
    :cond_b
    const-string v0, "com.google.android.gms.settings.UDC_SETTINGS"

    invoke-direct {p0, v0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sget v2, Lcom/google/android/gms/p;->ze:I

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/i;Landroid/content/Intent;I)Lcom/google/android/gms/common/widget/a/o;

    goto/16 :goto_0

    .line 112
    :cond_c
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.plus.action.MANAGE_APPS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "com.google.android.gms.extras.ALL_APPS"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    sget v2, Lcom/google/android/gms/p;->eo:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/i;Landroid/content/Intent;I)Lcom/google/android/gms/common/widget/a/o;

    goto/16 :goto_1

    .line 118
    :cond_d
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.googlequicksearchbox.action.PRIVACY_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/gms/p;->fd:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/i;Landroid/content/Intent;I)Lcom/google/android/gms/common/widget/a/o;

    goto/16 :goto_2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 83
    invoke-super {p0, p1}, Lcom/google/android/gms/common/widget/a/b;->onCreate(Landroid/os/Bundle;)V

    .line 85
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    const-string v0, "GoogleSettingsActivity"

    const-string v1, "Cannot run for restricted users."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Landroid/content/ComponentName;)V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->finish()V

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 95
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    sget v1, Lcom/google/android/gms/h;->I:I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(I)V

    .line 98
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "plusone:autobackup_show_google_settings_entry"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/google/android/gms/app/settings/c;

    invoke-direct {v1, p0, v3}, Lcom/google/android/gms/app/settings/c;-><init>(Lcom/google/android/gms/app/settings/GoogleSettingsActivity;B)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/x;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/photos/autobackup/d;->b:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v3, v1}, Lcom/google/android/gms/common/api/w;->a(Landroid/support/v4/app/q;ILcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->f:Lcom/google/android/gms/common/api/v;

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 171
    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 172
    sget v1, Lcom/google/android/gms/m;->E:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    sget v0, Lcom/google/android/gms/j;->da:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 175
    new-instance v0, Lcom/google/android/gms/common/c/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/c/a;-><init>(Landroid/content/Context;)V

    const-string v1, "enable_offline_otp_v2"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/c/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/j;->he:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 176
    :cond_0
    sget-object v0, Lcom/google/android/gms/app/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/gms/j;->tG:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 178
    :cond_1
    return v2

    .line 175
    :cond_2
    invoke-static {}, Lcom/google/android/gms/auth/authzen/a/b;->c()Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 183
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 185
    sget v1, Lcom/google/android/gms/j;->da:I

    if-ne v1, v0, :cond_0

    .line 186
    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    .line 187
    new-instance v1, Lcom/google/android/gms/appstate/e/a;

    invoke-direct {v1}, Lcom/google/android/gms/appstate/e/a;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a:Lcom/google/android/gms/appstate/e/a;

    .line 188
    iget-object v1, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a:Lcom/google/android/gms/appstate/e/a;

    const-string v2, "clear_app_state_dialog"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/appstate/e/a;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 210
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 190
    :cond_0
    sget v1, Lcom/google/android/gms/j;->rs:I

    if-ne v1, v0, :cond_1

    .line 191
    sget-object v0, Lcom/google/android/gms/common/a/b;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 193
    new-instance v1, Lcom/google/android/gms/googlehelp/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/googlehelp/b;-><init>(Landroid/app/Activity;)V

    const-string v2, "android_main"

    invoke-static {v2}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/net/Uri;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/b;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 197
    :cond_1
    sget v1, Lcom/google/android/gms/j;->he:I

    if-ne v1, v0, :cond_2

    .line 198
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/otp/OtpActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 200
    :cond_2
    sget v1, Lcom/google/android/gms/j;->tG:I

    if-ne v1, v0, :cond_3

    .line 201
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 203
    :cond_3
    sget v1, Lcom/google/android/gms/j;->rt:I

    if-ne v1, v0, :cond_4

    .line 204
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/app/settings/OpenSourceLicensesActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 207
    :cond_4
    invoke-super {p0, p1}, Lcom/google/android/gms/common/widget/a/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_1
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 153
    const-string v0, "GoogleSettingsActivity"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    const-string v0, "GoogleSettingsActivity"

    const-string v1, "GoogleSettingsActivity.onServiceConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    :cond_0
    invoke-static {p2}, Lcom/google/android/location/reporting/service/m;->a(Landroid/os/IBinder;)Lcom/google/android/location/reporting/service/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->g:Lcom/google/android/location/reporting/service/l;

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->d:Lcom/google/android/gms/common/widget/a/o;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->c:Lcom/google/android/gms/common/widget/a/i;

    if-nez v0, :cond_2

    const-string v0, "GoogleSettingsActivity"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GoogleSettingsActivity"

    const-string v1, "Setting items not set up yet."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :cond_1
    :goto_0
    return-void

    .line 158
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->g:Lcom/google/android/location/reporting/service/l;

    if-nez v0, :cond_3

    const-string v0, "GoogleSettingsActivity"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GoogleSettingsActivity"

    const-string v1, "Not connected, will add location settings when connect."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->g:Lcom/google/android/location/reporting/service/l;

    invoke-interface {v0}, Lcom/google/android/location/reporting/service/l;->a()Lcom/google/android/location/reporting/config/ReportingConfig;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->a(Landroid/content/Context;Lcom/google/android/location/reporting/config/ReportingConfig;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->c:Lcom/google/android/gms/common/widget/a/i;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.location.settings.GOOGLE_LOCATION_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/gms/p;->eW:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/i;Landroid/content/Intent;I)Lcom/google/android/gms/common/widget/a/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->d:Lcom/google/android/gms/common/widget/a/o;

    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->d:Lcom/google/android/gms/common/widget/a/o;

    sget v1, Lcom/google/android/gms/j;->di:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/o;->b(I)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "GoogleSettingsActivity"

    const-string v2, "Exception while fetching reporting config"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 163
    const-string v0, "GoogleSettingsActivity"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    const-string v0, "GoogleSettingsActivity"

    const-string v1, "GoogleSettingsActivity.onServiceDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->g:Lcom/google/android/location/reporting/service/l;

    .line 167
    return-void
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 131
    invoke-super {p0}, Lcom/google/android/gms/common/widget/a/b;->onStart()V

    .line 134
    invoke-static {p0, p0}, Lcom/google/android/location/reporting/service/PreferenceService;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    .line 135
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 139
    invoke-super {p0}, Lcom/google/android/gms/common/widget/a/b;->onStop()V

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->g:Lcom/google/android/location/reporting/service/l;

    if-eqz v0, :cond_0

    .line 141
    invoke-virtual {p0, p0}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 142
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->g:Lcom/google/android/location/reporting/service/l;

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a:Lcom/google/android/gms/appstate/e/a;

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a:Lcom/google/android/gms/appstate/e/a;

    invoke-virtual {v0}, Lcom/google/android/gms/appstate/e/a;->b()V

    .line 149
    :cond_1
    return-void
.end method
