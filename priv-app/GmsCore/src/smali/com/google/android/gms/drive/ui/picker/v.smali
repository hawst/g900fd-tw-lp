.class final Lcom/google/android/gms/drive/ui/picker/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/ui/picker/m;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/ui/picker/m;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/v;->a:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 347
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 348
    sget v1, Lcom/google/android/gms/j;->eJ:I

    if-ne v0, v1, :cond_0

    .line 349
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/v;->a:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-static {v0}, Lcom/google/android/gms/drive/ui/picker/m;->d(Lcom/google/android/gms/drive/ui/picker/m;)V

    .line 357
    :goto_0
    return-void

    .line 350
    :cond_0
    sget v1, Lcom/google/android/gms/j;->eI:I

    if-ne v0, v1, :cond_1

    .line 351
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/v;->a:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/m;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/support/v4/app/q;->setResult(I)V

    .line 352
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/v;->a:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/m;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->finish()V

    .line 353
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/v;->a:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-static {v0, v5}, Lcom/google/android/gms/drive/ui/picker/m;->a(Lcom/google/android/gms/drive/ui/picker/m;I)V

    goto :goto_0

    .line 355
    :cond_1
    const-string v1, "PickEntryDialogFragment"

    const-string v2, "Unknown view clicked: %s, %s."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    aput-object p1, v3, v5

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method
