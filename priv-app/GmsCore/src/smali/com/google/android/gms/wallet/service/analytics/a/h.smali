.class public abstract Lcom/google/android/gms/wallet/service/analytics/a/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public b:J

.field public c:Lcom/google/k/f/a/a/l;

.field public d:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Lcom/google/k/f/a/a/l;

    invoke-direct {v0}, Lcom/google/k/f/a/a/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/h;->c:Lcom/google/k/f/a/a/l;

    .line 46
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/h;->b:J

    .line 47
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/gms/wallet/common/ag;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-wide/16 v0, -0x1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/wallet/common/ag;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/h;->b:J

    .line 40
    const-class v0, Lcom/google/k/f/a/a/l;

    new-instance v1, Lcom/google/k/f/a/a/l;

    invoke-direct {v1}, Lcom/google/k/f/a/a/l;-><init>()V

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/wallet/common/ag;->a(Ljava/lang/Class;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/k/f/a/a/l;

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/h;->c:Lcom/google/k/f/a/a/l;

    .line 41
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/common/ag;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/h;->d:Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V
.end method

.method protected a(Lcom/google/android/gms/wallet/common/ah;)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/h;->c:Lcom/google/k/f/a/a/l;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/wallet/common/ah;->a(Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/wallet/common/ah;

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/h;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/wallet/common/ah;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ah;

    .line 57
    return-void
.end method

.method protected final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 86
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v0

    .line 87
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/analytics/a/h;->c:Lcom/google/k/f/a/a/l;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/k/f/a/a/l;->b:Ljava/lang/String;

    .line 88
    invoke-static {p2, v0}, Lcom/google/android/gms/common/util/o;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 89
    if-eqz v1, :cond_2

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/h;->c:Lcom/google/k/f/a/a/l;

    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v2, v0, Lcom/google/k/f/a/a/l;->c:I

    .line 91
    iget-object v0, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/h;->c:Lcom/google/k/f/a/a/l;

    iget-object v2, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/k/f/a/a/l;->d:Ljava/lang/String;

    .line 94
    :cond_0
    invoke-static {v1}, Lcom/google/android/gms/common/util/o;->a(Landroid/content/pm/PackageInfo;)I

    move-result v0

    .line 95
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 96
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/analytics/a/h;->c:Lcom/google/k/f/a/a/l;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/k/f/a/a/l;->a:Ljava/lang/String;

    .line 101
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/h;->c:Lcom/google/k/f/a/a/l;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/k/f/a/a/l;->e:Ljava/lang/String;

    .line 102
    return-void

    .line 99
    :cond_2
    const-string v1, "SessionState"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to retrieve package info for requestInfo for: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected abstract a(Lcom/google/k/f/a/a/j;)V
.end method

.method public abstract a()Z
.end method

.method public final b()Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 108
    new-instance v0, Lcom/google/k/f/a/a/j;

    invoke-direct {v0}, Lcom/google/k/f/a/a/j;-><init>()V

    .line 109
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/analytics/a/h;->c:Lcom/google/k/f/a/a/l;

    iput-object v1, v0, Lcom/google/k/f/a/a/j;->a:Lcom/google/k/f/a/a/l;

    .line 110
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a/h;->a(Lcom/google/k/f/a/a/j;)V

    .line 111
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 119
    invoke-static {}, Lcom/google/android/gms/wallet/common/af;->a()Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/wallet/service/analytics/a/h;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/wallet/common/ah;->a(J)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    .line 121
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a/h;->a(Lcom/google/android/gms/wallet/common/ah;)V

    .line 122
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
