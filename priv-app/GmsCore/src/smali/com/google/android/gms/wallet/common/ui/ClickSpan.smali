.class public Lcom/google/android/gms/wallet/common/ui/ClickSpan;
.super Landroid/text/style/URLSpan;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/wallet/common/ui/al;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/google/android/gms/wallet/common/ui/al;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    .line 35
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ui/ClickSpan;->a:Lcom/google/android/gms/wallet/common/ui/al;

    .line 36
    return-void
.end method

.method private static a(Landroid/text/SpannableString;Ljava/util/Set;Lcom/google/android/gms/wallet/common/ui/al;)Landroid/text/SpannableString;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 99
    invoke-virtual {p0}, Landroid/text/SpannableString;->length()I

    move-result v0

    const-class v2, Landroid/text/style/URLSpan;

    invoke-virtual {p0, v1, v0, v2}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 100
    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 101
    invoke-virtual {v3}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v4

    .line 102
    if-eqz p1, :cond_0

    invoke-interface {p1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 103
    :cond_0
    invoke-virtual {p0, v3}, Landroid/text/SpannableString;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    .line 104
    invoke-virtual {p0, v3}, Landroid/text/SpannableString;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    .line 105
    invoke-virtual {p0, v3}, Landroid/text/SpannableString;->getSpanFlags(Ljava/lang/Object;)I

    move-result v7

    .line 106
    invoke-virtual {p0, v3}, Landroid/text/SpannableString;->removeSpan(Ljava/lang/Object;)V

    .line 107
    new-instance v3, Lcom/google/android/gms/wallet/common/ui/ClickSpan;

    invoke-direct {v3, v4, p2}, Lcom/google/android/gms/wallet/common/ui/ClickSpan;-><init>(Ljava/lang/String;Lcom/google/android/gms/wallet/common/ui/al;)V

    invoke-virtual {p0, v3, v5, v6, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 100
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 110
    :cond_2
    return-object p0
.end method

.method public static a(Landroid/widget/TextView;ILjava/util/Set;Lcom/google/android/gms/wallet/common/ui/al;)V
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p2, p3}, Lcom/google/android/gms/wallet/common/ui/ClickSpan;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/util/Set;Lcom/google/android/gms/wallet/common/ui/al;)V

    .line 63
    return-void
.end method

.method public static a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-static {p0, p1, v0, v0}, Lcom/google/android/gms/wallet/common/ui/ClickSpan;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/util/Set;Lcom/google/android/gms/wallet/common/ui/al;)V

    .line 53
    return-void
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/String;Ljava/util/Set;Lcom/google/android/gms/wallet/common/ui/al;)V
    .locals 2

    .prologue
    .line 73
    new-instance v0, Landroid/text/SpannableString;

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-static {v0, p2, p3}, Lcom/google/android/gms/wallet/common/ui/ClickSpan;->a(Landroid/text/SpannableString;Ljava/util/Set;Lcom/google/android/gms/wallet/common/ui/al;)Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    invoke-virtual {p0}, Landroid/widget/TextView;->getMovementMethod()Landroid/text/method/MovementMethod;

    move-result-object v0

    .line 75
    if-eqz v0, :cond_0

    instance-of v0, v0, Landroid/text/method/LinkMovementMethod;

    if-nez v0, :cond_1

    .line 76
    :cond_0
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 78
    :cond_1
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ClickSpan;->a:Lcom/google/android/gms/wallet/common/ui/al;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ClickSpan;->a:Lcom/google/android/gms/wallet/common/ui/al;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ClickSpan;->getURL()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/wallet/common/ui/al;->onClick(Landroid/view/View;Ljava/lang/String;)V

    .line 45
    :goto_0
    return-void

    .line 43
    :cond_0
    invoke-super {p0, p1}, Landroid/text/style/URLSpan;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/text/style/URLSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 116
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 117
    return-void
.end method
