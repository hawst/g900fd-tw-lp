.class public Lcom/google/android/gms/appstate/provider/AppStateContentProvider;
.super Lcom/google/android/gms/common/e/a;
.source "SourceFile"


# static fields
.field private static final a:Landroid/content/UriMatcher;

.field private static final b:[Ljava/lang/String;

.field private static final c:Lcom/android/a/a/a;

.field private static final d:Lcom/android/a/a/a;

.field private static final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 40
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->a:Landroid/content/UriMatcher;

    .line 43
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->b:[Ljava/lang/String;

    .line 73
    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "client_contexts._id"

    invoke-virtual {v0, v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/appstate/provider/e;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->c:Lcom/android/a/a/a;

    .line 79
    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "app_states._id"

    invoke-virtual {v0, v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/appstate/provider/d;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/appstate/provider/e;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->d:Lcom/android/a/a/a;

    .line 86
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "app_states"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "client_contexts"

    const-string v2, "client_context_id"

    const-string v3, "client_contexts._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/gms/common/e/a;-><init>()V

    .line 67
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 367
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 368
    sget-object v1, Lcom/google/android/gms/appstate/provider/e;->b:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->b:[Ljava/lang/String;

    const-string v3, "account_name=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    aput-object p2, v4, v5

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 372
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 373
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 376
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 380
    const-string v0, "client_context_id IN "

    invoke-static {v0, v6}, Lcom/google/android/gms/common/e/d;->a(Ljava/lang/String;Ljava/util/Collection;)Lcom/google/android/gms/common/e/d;

    move-result-object v0

    .line 382
    const-string v1, "app_states"

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/d;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/gms/common/e/d;->a:[Ljava/lang/String;

    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic b()Landroid/content/UriMatcher;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->a:Landroid/content/UriMatcher;

    return-object v0
.end method

.method private static c(Landroid/net/Uri;)Lcom/google/android/gms/appstate/provider/b;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 61
    sget-object v0, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    .line 62
    if-ltz v3, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "Unrecognized URI: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 63
    const-class v0, Lcom/google/android/gms/appstate/provider/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/appstate/provider/b;

    aget-object v0, v0, v3

    return-object v0

    :cond_0
    move v0, v2

    .line 62
    goto :goto_0
.end method

.method private static d(Landroid/net/Uri;)J
    .locals 3

    .prologue
    .line 109
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    .line 110
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    .line 111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Given URI is malformed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_0
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method


# virtual methods
.method protected final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 240
    invoke-virtual {p3}, Landroid/content/ContentValues;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 241
    const/4 v0, 0x0

    .line 297
    :goto_0
    return v0

    .line 245
    :cond_0
    sget-object v0, Lcom/google/android/gms/appstate/provider/a;->a:[I

    invoke-static {p2}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->c(Landroid/net/Uri;)Lcom/google/android/gms/appstate/provider/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/appstate/provider/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 295
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid update URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 247
    :pswitch_0
    const-string v0, "package_name"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    const-string v0, "package_uid"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    const-string v0, "account_name"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    const-string v0, "client_contexts"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 255
    :pswitch_1
    const-string v0, "package_name"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    const-string v0, "package_uid"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    const-string v0, "account_name"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    const-string v0, "client_contexts"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto :goto_0

    .line 263
    :pswitch_2
    const-string v0, "app_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 264
    const-string v0, "key"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 266
    new-instance v0, Lcom/google/android/gms/common/e/b;

    invoke-direct {v0, p2, p4, p5}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 267
    const-string v1, "client_context_id"

    invoke-static {p2}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->d(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;J)V

    .line 268
    const-string v1, "app_states"

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    invoke-virtual {p1, v1, p3, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 274
    :pswitch_3
    const-string v0, "app_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 275
    const-string v0, "key"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 278
    const-string v0, "app_states"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 283
    :pswitch_4
    const-string v0, "app_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 284
    const-string v0, "key"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 286
    new-instance v0, Lcom/google/android/gms/common/e/b;

    invoke-direct {v0, p2, p4, p5}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 287
    const-string v1, "app_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 288
    const-string v1, "state_key"

    const-string v2, "key"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    const-string v1, "client_context_id"

    invoke-static {p2}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->d(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;J)V

    .line 290
    const-string v1, "app_states"

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    invoke-virtual {p1, v1, p3, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 245
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 303
    sget-object v0, Lcom/google/android/gms/appstate/provider/a;->a:[I

    invoke-static {p2}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->c(Landroid/net/Uri;)Lcom/google/android/gms/appstate/provider/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/appstate/provider/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 344
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid delete URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 307
    :pswitch_0
    new-array v2, v2, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    move v0, v6

    .line 310
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 311
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v5

    const-string v2, "app_states"

    const-string v3, "client_context_id=?"

    invoke-virtual {p1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v2, "client_contexts"

    const-string v3, "_id=?"

    invoke-virtual {p1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    add-int/2addr v0, v2

    goto :goto_0

    .line 314
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 341
    :goto_1
    return v0

    .line 314
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 320
    :pswitch_1
    new-instance v0, Lcom/google/android/gms/common/e/b;

    invoke-direct {v0, p2, p3, p4}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 321
    const-string v1, "client_context_id"

    invoke-static {p2}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->d(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;J)V

    .line 322
    const-string v1, "app_states"

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 328
    :pswitch_2
    const-string v0, "app_states"

    const-string v1, "_id=?"

    new-array v2, v2, [Ljava/lang/String;

    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 332
    :pswitch_3
    new-instance v0, Lcom/google/android/gms/common/e/b;

    invoke-direct {v0, p2, p3, p4}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 333
    const-string v1, "app_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 334
    const-string v1, "state_key"

    const-string v2, "key"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    const-string v1, "client_context_id"

    invoke-static {p2}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->d(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;J)V

    .line 336
    const-string v1, "app_states"

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 341
    :pswitch_4
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 303
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected final a(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .locals 1

    .prologue
    .line 395
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 121
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 122
    new-instance v1, Lcom/google/android/gms/common/e/b;

    invoke-direct {v1, p2, p4, p5}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 123
    sget-object v2, Lcom/google/android/gms/appstate/provider/a;->a:[I

    invoke-static {p2}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->c(Landroid/net/Uri;)Lcom/google/android/gms/appstate/provider/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/appstate/provider/b;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 172
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid query URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :pswitch_0
    const-string v2, "client_contexts"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 130
    sget-object v2, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->c:Lcom/android/a/a/a;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 176
    :goto_0
    invoke-virtual {v1}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v3

    .line 177
    iget-object v4, v1, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    move-object v1, p1

    move-object v2, p3

    move-object v6, v5

    move-object v7, p6

    move-object v8, v5

    .line 180
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 184
    instance-of v1, v0, Landroid/database/AbstractWindowedCursor;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 186
    if-eqz v0, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/appstate/provider/c;->a:Landroid/net/Uri;

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 190
    :cond_0
    return-object v0

    .line 135
    :pswitch_1
    const-string v2, "client_contexts._id"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 136
    const-string v2, "client_contexts"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 137
    sget-object v2, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->c:Lcom/android/a/a/a;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    goto :goto_0

    .line 142
    :pswitch_2
    const-string v2, "client_context_id"

    invoke-static {p2}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->d(Landroid/net/Uri;)J

    move-result-wide v6

    invoke-virtual {v1, v2, v6, v7}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;J)V

    .line 143
    sget-object v2, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 144
    sget-object v2, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->d:Lcom/android/a/a/a;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    goto :goto_0

    .line 149
    :pswitch_3
    const-string v2, "app_states._id"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 150
    sget-object v2, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 151
    sget-object v2, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->d:Lcom/android/a/a/a;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    goto :goto_0

    .line 156
    :pswitch_4
    const-string v2, "client_context_id"

    invoke-static {p2}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->d(Landroid/net/Uri;)J

    move-result-wide v6

    invoke-virtual {v1, v2, v6, v7}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;J)V

    .line 157
    const-string v2, "app_id"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 158
    const-string v2, "state_key"

    const-string v3, "key"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    sget-object v2, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 160
    sget-object v2, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->d:Lcom/android/a/a/a;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    goto :goto_0

    .line 165
    :pswitch_5
    const-string v2, "account_name"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 166
    sget-object v2, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 167
    sget-object v2, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->d:Lcom/android/a/a/a;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    goto/16 :goto_0

    .line 123
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected final a(Landroid/net/Uri;)Landroid/database/sqlite/SQLiteOpenHelper;
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/appstate/provider/f;->a(Landroid/content/Context;)Lcom/google/android/gms/appstate/provider/f;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 195
    sget-object v0, Lcom/google/android/gms/appstate/provider/a;->a:[I

    invoke-static {p2}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->c(Landroid/net/Uri;)Lcom/google/android/gms/appstate/provider/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/appstate/provider/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 231
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid insert URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :pswitch_1
    const-string v0, "package_name"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 200
    const-string v1, "package_uid"

    invoke-virtual {p3, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 201
    if-lez v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 202
    const-string v1, "account_name"

    invoke-virtual {p3, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 206
    new-instance v3, Lcom/google/android/gms/common/e/b;

    invoke-direct {v3, p2, v5, v5}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 207
    const-string v4, "package_name"

    invoke-virtual {v3, v4, v0}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    const-string v0, "package_uid"

    int-to-long v6, v2

    invoke-virtual {v3, v0, v6, v7}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;J)V

    .line 209
    const-string v0, "account_name"

    invoke-virtual {v3, v0, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    const-string v4, "client_contexts"

    invoke-virtual {v3}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v3, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 226
    :goto_1
    return-object v0

    .line 201
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 216
    :pswitch_2
    const-string v0, "app_id"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 217
    const-string v1, "key"

    invoke-virtual {p3, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 218
    const-string v1, "key"

    invoke-virtual {p3, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 219
    invoke-static {p2}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->d(Landroid/net/Uri;)J

    move-result-wide v2

    .line 222
    new-instance v6, Lcom/google/android/gms/common/e/b;

    invoke-direct {v6, p2}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 223
    const-string v4, "app_id"

    invoke-virtual {v6, v4, v0}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const-string v0, "key"

    invoke-virtual {v6, v0, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    const-string v0, "client_context_id"

    invoke-virtual {v6, v0, v2, v3}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;J)V

    .line 226
    const-string v4, "app_states"

    invoke-virtual {v6}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v6, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected final a()V
    .locals 4

    .prologue
    .line 387
    invoke-virtual {p0}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/appstate/provider/c;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 389
    return-void
.end method

.method protected final b(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    const-string v0, "app_state.db"

    return-object v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 400
    sget-object v0, Lcom/google/android/gms/appstate/provider/a;->a:[I

    invoke-static {p1}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->c(Landroid/net/Uri;)Lcom/google/android/gms/appstate/provider/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/appstate/provider/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 419
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 402
    :pswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.appstate.client_contexts"

    .line 415
    :goto_0
    return-object v0

    .line 406
    :pswitch_1
    const-string v0, "vnd.android.cursor.item/vnd.google.android.appstate.client_context"

    goto :goto_0

    .line 410
    :pswitch_2
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.appstate.states"

    goto :goto_0

    .line 415
    :pswitch_3
    const-string v0, "vnd.android.cursor.item/vnd.google.android.appstate.state"

    goto :goto_0

    .line 400
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
