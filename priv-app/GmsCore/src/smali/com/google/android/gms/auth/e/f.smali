.class public final Lcom/google/android/gms/auth/e/f;
.super Lcom/google/android/gms/auth/e/a;
.source "SourceFile"


# instance fields
.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/e/a;-><init>(Landroid/content/Context;)V

    .line 44
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 95
    sget-object v0, Lcom/google/android/gms/auth/b/a;->r:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v1

    sget-object v0, Lcom/google/android/gms/auth/b/a;->w:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    new-instance v1, Lcom/google/android/gms/auth/f/k;

    invoke-direct {v1}, Lcom/google/android/gms/auth/f/k;-><init>()V

    .line 101
    iget v0, p0, Lcom/google/android/gms/auth/e/f;->d:I

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/f/k;->a(I)Lcom/google/android/gms/auth/f/k;

    .line 102
    iget v0, p0, Lcom/google/android/gms/auth/e/f;->e:I

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/f/k;->b(I)Lcom/google/android/gms/auth/f/k;

    .line 103
    iget v0, p0, Lcom/google/android/gms/auth/e/f;->f:I

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/f/k;->c(I)Lcom/google/android/gms/auth/f/k;

    .line 104
    iget v0, p0, Lcom/google/android/gms/auth/e/f;->g:I

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/auth/f/k;->a(J)Lcom/google/android/gms/auth/f/k;

    .line 105
    iget v0, p0, Lcom/google/android/gms/auth/e/f;->h:I

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/f/k;->d(I)Lcom/google/android/gms/auth/f/k;

    .line 106
    iget v0, p0, Lcom/google/android/gms/auth/e/f;->i:I

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/auth/f/k;->b(J)Lcom/google/android/gms/auth/f/k;

    .line 107
    iget-boolean v0, p0, Lcom/google/android/gms/auth/e/f;->j:Z

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/f/k;->a(Z)Lcom/google/android/gms/auth/f/k;

    .line 108
    iget-boolean v0, p0, Lcom/google/android/gms/auth/e/f;->k:Z

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/f/k;->b(Z)Lcom/google/android/gms/auth/f/k;

    .line 109
    iget-boolean v0, p0, Lcom/google/android/gms/auth/e/f;->l:Z

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/f/k;->c(Z)Lcom/google/android/gms/auth/f/k;

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/auth/e/f;->m:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/auth/e/f;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/f/g;

    .line 112
    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/f/k;->a(Lcom/google/android/gms/auth/f/g;)Lcom/google/android/gms/auth/f/k;

    goto :goto_1

    .line 116
    :cond_2
    new-instance v0, Lcom/google/android/gms/auth/f/b;

    invoke-direct {v0}, Lcom/google/android/gms/auth/f/b;-><init>()V

    .line 117
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/f/b;->a(I)Lcom/google/android/gms/auth/f/b;

    .line 118
    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/f/b;->a(Lcom/google/android/gms/auth/f/k;)Lcom/google/android/gms/auth/f/b;

    .line 119
    const-string v1, "TrustAgent"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/auth/e/f;->a(Ljava/lang/String;Lcom/google/android/gms/auth/f/b;)V

    goto :goto_0
.end method
