.class public final Lcom/google/android/gms/drive/api/a/ah;
.super Lcom/google/android/gms/drive/api/a/d;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/internal/SetDrivePreferencesRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/SetDrivePreferencesRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p3}, Lcom/google/android/gms/drive/api/a/d;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V

    .line 26
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/ah;->e:Lcom/google/android/gms/drive/internal/SetDrivePreferencesRequest;

    .line 27
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 32
    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->a()Lcom/google/android/gms/drive/g/aw;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->m()Lcom/google/android/gms/drive/g/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/ah;->e:Lcom/google/android/gms/drive/internal/SetDrivePreferencesRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/SetDrivePreferencesRequest;->a()Lcom/google/android/gms/drive/DrivePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/DrivePreferences;->a()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/g/ae;->a(Z)V

    .line 35
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ah;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-interface {v0}, Lcom/google/android/gms/drive/internal/ca;->a()V

    .line 36
    return-void
.end method
