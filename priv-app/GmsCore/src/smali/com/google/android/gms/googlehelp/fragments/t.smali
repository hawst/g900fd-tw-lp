.class final Lcom/google/android/gms/googlehelp/fragments/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Landroid/webkit/WebSettings;

.field final synthetic b:Landroid/webkit/WebView;


# direct methods
.method constructor <init>(Landroid/webkit/WebSettings;Landroid/webkit/WebView;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/fragments/t;->a:Landroid/webkit/WebSettings;

    iput-object p2, p0, Lcom/google/android/gms/googlehelp/fragments/t;->b:Landroid/webkit/WebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 199
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 200
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/t;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 201
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/t;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 202
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/t;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 204
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
