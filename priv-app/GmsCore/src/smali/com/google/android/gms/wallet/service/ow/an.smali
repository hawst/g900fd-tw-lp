.class public final Lcom/google/android/gms/wallet/service/ow/an;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const-string v0, "com.google.android.gms.wallet.service.OwServerStateTracker"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/an;->a:Landroid/content/SharedPreferences;

    .line 31
    return-void
.end method

.method private static b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u2063LAST_KNOWN_OW_SERVER_STATE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(I)Ljava/lang/Boolean;
    .locals 10

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x0

    .line 121
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/an;->a:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lcom/google/android/gms/wallet/service/ow/an;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 122
    if-eqz v1, :cond_0

    .line 123
    invoke-static {v1}, Lcom/google/android/gms/wallet/common/af;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ag;

    move-result-object v1

    .line 124
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ag;->a(Z)Z

    move-result v2

    .line 125
    invoke-virtual {v1, v6, v7}, Lcom/google/android/gms/wallet/common/ag;->a(J)J

    move-result-wide v4

    .line 126
    invoke-virtual {v1, v6, v7}, Lcom/google/android/gms/wallet/common/ag;->a(J)J

    move-result-wide v6

    .line 127
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v4, v8, v4

    cmp-long v1, v4, v6

    if-gez v1, :cond_0

    .line 128
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 131
    :cond_0
    return-object v0
.end method

.method public final a(ILcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v3, 0x0

    .line 34
    .line 35
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 81
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized response type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :pswitch_1
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    move-object v2, v0

    .line 84
    :goto_0
    if-nez v2, :cond_0

    .line 118
    :goto_1
    return-void

    .line 48
    :pswitch_2
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/b/a/c;

    .line 49
    if-eqz v0, :cond_3

    .line 50
    iget-object v1, v0, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    if-eqz v1, :cond_3

    iget-object v0, v0, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    iget v0, v0, Lcom/google/aa/b/a/j;->a:I

    const/16 v1, 0x3d

    if-ne v0, v1, :cond_3

    .line 53
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    move-object v2, v0

    goto :goto_0

    :pswitch_3
    move-object v2, v3

    .line 79
    goto :goto_0

    .line 87
    :cond_0
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v2, v0, :cond_2

    .line 89
    sget-object v0, Lcom/google/android/gms/wallet/b/f;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 111
    :cond_1
    :goto_2
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/ow/an;->a:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-static {p1}, Lcom/google/android/gms/wallet/service/ow/an;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/google/android/gms/wallet/common/af;->a()Lcom/google/android/gms/wallet/common/ah;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v5, v2}, Lcom/google/android/gms/wallet/common/ah;->a(Z)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Lcom/google/android/gms/wallet/common/ah;->a(J)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/wallet/common/ah;->a(J)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ah;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1

    .line 91
    :cond_2
    sget-object v0, Lcom/google/android/gms/wallet/b/f;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 92
    iget-object v4, p0, Lcom/google/android/gms/wallet/service/ow/an;->a:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lcom/google/android/gms/wallet/service/ow/an;->b(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 93
    if-eqz v3, :cond_1

    .line 94
    invoke-static {v3}, Lcom/google/android/gms/wallet/common/af;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ag;

    move-result-object v3

    .line 95
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/gms/wallet/common/ag;->a(Z)Z

    move-result v4

    .line 96
    if-nez v4, :cond_1

    .line 97
    invoke-virtual {v3, v6, v7}, Lcom/google/android/gms/wallet/common/ag;->a(J)J

    move-result-wide v4

    .line 98
    invoke-virtual {v3, v6, v7}, Lcom/google/android/gms/wallet/common/ag;->a(J)J

    move-result-wide v0

    .line 101
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    cmp-long v3, v4, v0

    if-ltz v3, :cond_1

    .line 104
    long-to-double v0, v0

    const-wide v4, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double/2addr v4, v0

    sget-object v0, Lcom/google/android/gms/wallet/b/f;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-double v0, v0

    mul-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    sget-object v0, Lcom/google/android/gms/wallet/b/f;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    goto/16 :goto_2

    :cond_3
    move-object v2, v3

    goto/16 :goto_0

    .line 35
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
