.class public final Lcom/google/android/gms/drive/database/model/au;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Lcom/google/android/gms/drive/database/model/aw;

.field c:Lcom/google/android/gms/drive/database/model/ae;

.field d:Lcom/google/android/gms/drive/database/model/ab;

.field e:Z

.field final f:Ljava/util/Set;

.field public g:Z

.field h:Ljava/lang/Object;

.field i:Lcom/google/android/gms/drive/database/model/av;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object v1, p0, Lcom/google/android/gms/drive/database/model/au;->c:Lcom/google/android/gms/drive/database/model/ae;

    .line 91
    iput-object v1, p0, Lcom/google/android/gms/drive/database/model/au;->d:Lcom/google/android/gms/drive/database/model/ab;

    .line 92
    iput-boolean v2, p0, Lcom/google/android/gms/drive/database/model/au;->e:Z

    .line 93
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/au;->f:Ljava/util/Set;

    .line 94
    iput-boolean v2, p0, Lcom/google/android/gms/drive/database/model/au;->g:Z

    .line 95
    iput-object v1, p0, Lcom/google/android/gms/drive/database/model/au;->h:Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/google/android/gms/drive/database/model/av;->a:Lcom/google/android/gms/drive/database/model/av;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/au;->i:Lcom/google/android/gms/drive/database/model/av;

    .line 107
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/au;->a:Ljava/lang/String;

    .line 108
    iput-object p2, p0, Lcom/google/android/gms/drive/database/model/au;->b:Lcom/google/android/gms/drive/database/model/aw;

    .line 109
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 191
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/au;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/au;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 192
    const-string v0, "FieldDefinition"

    const-string v1, "Ignoring isIndexed constraint as field also has uniqueness constraint (on just this field, and therefore SQLite will have to create an index on that. For field: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 198
    iput-boolean v3, p0, Lcom/google/android/gms/drive/database/model/au;->e:Z

    .line 200
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/database/model/au;
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/model/au;->e:Z

    .line 156
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/model/au;->b()V

    .line 157
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcom/google/android/gms/drive/database/model/av;->a:Lcom/google/android/gms/drive/database/model/av;

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;Lcom/google/android/gms/drive/database/model/av;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;Lcom/google/android/gms/drive/database/model/av;)Lcom/google/android/gms/drive/database/model/au;
    .locals 0

    .prologue
    .line 143
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/au;->c:Lcom/google/android/gms/drive/database/model/ae;

    .line 146
    iput-object p2, p0, Lcom/google/android/gms/drive/database/model/au;->d:Lcom/google/android/gms/drive/database/model/ab;

    .line 147
    iput-object p3, p0, Lcom/google/android/gms/drive/database/model/au;->i:Lcom/google/android/gms/drive/database/model/av;

    .line 148
    return-object p0
.end method

.method public final a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/au;->h:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "defaultValue already set"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 209
    const-string v0, "null defaultValue"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/au;->h:Ljava/lang/Object;

    .line 211
    return-object p0

    .line 208
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final varargs a([Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/au;
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/au;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/au;->f:Ljava/util/Set;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/au;->f:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/au;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 181
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/model/au;->b()V

    .line 182
    return-object p0
.end method
