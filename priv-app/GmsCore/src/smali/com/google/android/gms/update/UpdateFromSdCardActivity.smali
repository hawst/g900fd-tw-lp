.class public Lcom/google/android/gms/update/UpdateFromSdCardActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static b:Z


# instance fields
.field private a:Lcom/google/android/gms/update/UpdateFromSdCardService;

.field private c:Ljava/lang/String;

.field private d:Landroid/view/View;

.field private e:Landroid/app/Dialog;

.field private f:I

.field private g:Z

.field private h:Landroid/os/Handler;

.field private i:J

.field private j:J

.field private k:Ljava/util/List;

.field private l:Landroid/content/BroadcastReceiver;

.field private m:Landroid/content/BroadcastReceiver;

.field private n:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    .line 96
    new-instance v0, Lcom/google/android/gms/update/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/update/w;-><init>(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->l:Landroid/content/BroadcastReceiver;

    .line 111
    new-instance v0, Lcom/google/android/gms/update/x;

    invoke-direct {v0, p0}, Lcom/google/android/gms/update/x;-><init>(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->m:Landroid/content/BroadcastReceiver;

    .line 125
    new-instance v0, Lcom/google/android/gms/update/y;

    invoke-direct {v0, p0}, Lcom/google/android/gms/update/y;-><init>(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->n:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)J
    .locals 2

    .prologue
    .line 67
    iget-wide v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->i:J

    return-wide v0
.end method

.method static synthetic a(Lcom/google/android/gms/update/UpdateFromSdCardActivity;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->d:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/update/UpdateFromSdCardActivity;Lcom/google/android/gms/update/UpdateFromSdCardService;)Lcom/google/android/gms/update/UpdateFromSdCardService;
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/update/UpdateFromSdCardActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->c:Ljava/lang/String;

    return-object p1
.end method

.method private a(I)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v4, 0x0

    .line 313
    sget v0, Lcom/google/android/gms/j;->sb:I

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(II)V

    sget v0, Lcom/google/android/gms/j;->sc:I

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(II)V

    sget v0, Lcom/google/android/gms/j;->rL:I

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(II)V

    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(II)V

    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(II)V

    sget v0, Lcom/google/android/gms/j;->A:I

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(II)V

    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "dismissButtonQualifier"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "dismissSnackbar"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/google/android/gms/j;->rO:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/update/SystemUpdateSnackbar;

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/update/SystemUpdateSnackbar;->setVisibility(I)V

    :cond_0
    sget v0, Lcom/google/android/gms/j;->cm:I

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(II)V

    sget v0, Lcom/google/android/gms/j;->sQ:I

    sget v1, Lcom/google/android/gms/p;->yk:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(II)V

    .line 314
    const-string v0, "UpdateFromSdCardActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "drawing for the current service status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    iput p1, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->f:I

    .line 320
    packed-switch p1, :pswitch_data_0

    .line 525
    :goto_0
    :pswitch_0
    return-void

    .line 322
    :pswitch_1
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "UNKNOWN_STATUS"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    sget v0, Lcom/google/android/gms/j;->eg:I

    sget v1, Lcom/google/android/gms/p;->yl:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(II)V

    .line 326
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->yi:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(II)V

    .line 330
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(II)V

    goto :goto_0

    .line 333
    :pswitch_2
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "STATUS_STEADY"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-direct {p0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/update/UpdateFromSdCardService;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 341
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->e:Landroid/app/Dialog;

    if-nez v0, :cond_1

    .line 342
    new-instance v0, Landroid/app/Dialog;

    invoke-direct {v0, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->e:Landroid/app/Dialog;

    .line 343
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->e:Landroid/app/Dialog;

    sget v1, Lcom/google/android/gms/l;->fv:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    .line 344
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->e:Landroid/app/Dialog;

    sget v1, Lcom/google/android/gms/p;->yj:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    .line 350
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 351
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->e:Landroid/app/Dialog;

    sget v1, Lcom/google/android/gms/j;->gb:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 353
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->e:Landroid/app/Dialog;

    sget v1, Lcom/google/android/gms/j;->gc:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 355
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->e:Landroid/app/Dialog;

    sget v1, Lcom/google/android/gms/j;->gb:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/update/z;

    invoke-direct {v1, p0}, Lcom/google/android/gms/update/z;-><init>(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 369
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->e:Landroid/app/Dialog;

    sget v1, Lcom/google/android/gms/j;->ge:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 370
    iget-object v1, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->e:Landroid/app/Dialog;

    sget v3, Lcom/google/android/gms/j;->ga:I

    invoke-virtual {v1, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 375
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 376
    new-instance v3, Lcom/google/android/gms/update/aa;

    invoke-direct {v3, p0}, Lcom/google/android/gms/update/aa;-><init>(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 393
    new-instance v0, Lcom/google/android/gms/update/ab;

    invoke-direct {v0, p0}, Lcom/google/android/gms/update/ab;-><init>(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)V

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 401
    new-instance v1, Lcom/google/android/gms/update/d;

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/update/d;-><init>(Landroid/app/Activity;Ljava/util/List;)V

    .line 403
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->e:Landroid/app/Dialog;

    sget v2, Lcom/google/android/gms/j;->gd:I

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 405
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 407
    new-instance v2, Lcom/google/android/gms/update/ac;

    invoke-direct {v2, p0, v1}, Lcom/google/android/gms/update/ac;-><init>(Lcom/google/android/gms/update/UpdateFromSdCardActivity;Lcom/google/android/gms/update/d;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 445
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->e:Landroid/app/Dialog;

    new-instance v1, Lcom/google/android/gms/update/ad;

    invoke-direct {v1, p0}, Lcom/google/android/gms/update/ad;-><init>(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 461
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    .line 353
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_1

    .line 365
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->e:Landroid/app/Dialog;

    sget v1, Lcom/google/android/gms/j;->gb:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 464
    :pswitch_3
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "STATUS_VERIFIED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    sget v0, Lcom/google/android/gms/j;->sb:I

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(II)V

    .line 466
    sget v0, Lcom/google/android/gms/j;->eg:I

    sget v1, Lcom/google/android/gms/p;->yn:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(II)V

    .line 469
    sget v0, Lcom/google/android/gms/j;->eg:I

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(II)V

    .line 471
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->yp:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(II)V

    .line 472
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(II)V

    .line 474
    sget v0, Lcom/google/android/gms/j;->A:I

    sget v1, Lcom/google/android/gms/p;->yi:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(II)V

    .line 477
    sget v0, Lcom/google/android/gms/j;->A:I

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(II)V

    goto/16 :goto_0

    .line 480
    :pswitch_4
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "STATUS_FAILED_TO_VERIFY"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    sget v0, Lcom/google/android/gms/j;->eg:I

    sget v1, Lcom/google/android/gms/p;->ym:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(II)V

    .line 483
    sget v0, Lcom/google/android/gms/j;->eg:I

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(II)V

    .line 484
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->xT:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(II)V

    .line 485
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(II)V

    goto/16 :goto_0

    .line 488
    :pswitch_5
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "STATUS_VERIFIED_LOW_BATTERY"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    sget v0, Lcom/google/android/gms/j;->eg:I

    sget v1, Lcom/google/android/gms/p;->yn:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(II)V

    .line 492
    sget v0, Lcom/google/android/gms/j;->eg:I

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(II)V

    .line 493
    sget v0, Lcom/google/android/gms/p;->xO:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(I)V

    goto/16 :goto_0

    .line 496
    :pswitch_6
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "STATUS_VERIFIED_LOW_BATTERY_CHARGING"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    sget v0, Lcom/google/android/gms/j;->eg:I

    sget v1, Lcom/google/android/gms/p;->yn:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(II)V

    .line 500
    sget v0, Lcom/google/android/gms/j;->eg:I

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(II)V

    .line 501
    sget v0, Lcom/google/android/gms/p;->xN:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(I)V

    goto/16 :goto_0

    .line 504
    :pswitch_7
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "STATUS_VERIFYING"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 512
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 513
    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 514
    sget v0, Lcom/google/android/gms/j;->eg:I

    sget v1, Lcom/google/android/gms/p;->yo:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(II)V

    .line 516
    sget v0, Lcom/google/android/gms/j;->eg:I

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(II)V

    .line 517
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 519
    sget v0, Lcom/google/android/gms/j;->A:I

    sget v1, Lcom/google/android/gms/p;->yi:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(II)V

    .line 522
    sget v0, Lcom/google/android/gms/j;->A:I

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(II)V

    goto/16 :goto_0

    .line 320
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_7
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_3
    .end packed-switch
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 544
    invoke-virtual {p0, p2}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(ILjava/lang/CharSequence;Z)V

    .line 545
    return-void
.end method

.method private a(ILjava/lang/CharSequence;Z)V
    .locals 2

    .prologue
    .line 548
    invoke-virtual {p0, p1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 549
    if-nez v1, :cond_0

    .line 554
    :goto_0
    return-void

    .line 550
    :cond_0
    instance-of v0, v1, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 551
    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 553
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method

.method private a(JZ)V
    .locals 11

    .prologue
    const/4 v9, 0x1

    .line 697
    iget-wide v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->i:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 724
    :cond_0
    :goto_0
    return-void

    .line 701
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 702
    iget-wide v2, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->i:J

    cmp-long v2, v0, v2

    if-ltz v2, :cond_3

    .line 703
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    if-nez v0, :cond_2

    .line 704
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "Lost connection to the service. Cannot install."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 709
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b()V

    .line 710
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-virtual {v0}, Lcom/google/android/gms/update/UpdateFromSdCardService;->b()V

    goto :goto_0

    .line 713
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->i:J

    sub-long/2addr v2, v0

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v2, v2

    add-int/lit8 v2, v2, 0x1

    .line 714
    sget v3, Lcom/google/android/gms/j;->eg:I

    invoke-virtual {p0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/gms/n;->y:I

    new-array v6, v9, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v2, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3, v4, v9}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(ILjava/lang/CharSequence;Z)V

    .line 718
    iget-wide v4, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->i:J

    add-int/lit8 v2, v2, -0x1

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    sub-long v2, v4, v2

    sub-long v0, v2, v0

    long-to-int v0, v0

    .line 719
    if-eqz p3, :cond_0

    .line 720
    iget-object v1, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->h:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/update/ae;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/google/android/gms/update/ae;-><init>(Lcom/google/android/gms/update/UpdateFromSdCardActivity;JZ)V

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/update/UpdateFromSdCardActivity;JZ)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(JZ)V

    return-void
.end method

.method private static a()Z
    .locals 2

    .prologue
    .line 177
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/a/a;->b()I

    move-result v0

    if-lez v0, :cond_0

    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v1, "user"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 284
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->l:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 288
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    .line 290
    :goto_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 557
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "displayButtonQualifier"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 559
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "displaySnackbar"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/google/android/gms/j;->rO:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/update/SystemUpdateSnackbar;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/update/SystemUpdateSnackbar;->setText(I)V

    invoke-virtual {v0}, Lcom/google/android/gms/update/SystemUpdateSnackbar;->a()V

    .line 564
    :cond_0
    :goto_0
    return-void

    .line 561
    :cond_1
    sget v0, Lcom/google/android/gms/j;->cm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 562
    sget v0, Lcom/google/android/gms/j;->cm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private b(II)V
    .locals 1

    .prologue
    .line 619
    invoke-virtual {p0, p1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 620
    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Landroid/view/View;->setVisibility(I)V

    .line 621
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->c()V

    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-virtual {v0}, Lcom/google/android/gms/update/UpdateFromSdCardService;->a()I

    move-result v0

    .line 307
    invoke-direct {p0, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(I)V

    .line 308
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->g:Z

    return v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 660
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    if-eqz v0, :cond_0

    .line 661
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-virtual {v0}, Lcom/google/android/gms/update/UpdateFromSdCardService;->c()V

    .line 663
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->finish()V

    .line 664
    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->d()V

    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 687
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "stopCountdown"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    iget-wide v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->i:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 689
    iput-wide v2, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->i:J

    .line 690
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->j:J

    .line 691
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->g:Z

    .line 693
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->c()V

    .line 694
    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->g()V

    return-void
.end method

.method private f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 743
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 744
    const/4 v0, 0x0

    .line 750
    :goto_0
    return-object v0

    .line 746
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 747
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 748
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 750
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic f(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)Ljava/util/List;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k:Ljava/util/List;

    return-object v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 763
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 767
    :cond_0
    :goto_0
    return-void

    .line 766
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method static synthetic h(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(I)V

    return-void
.end method

.method static synthetic j(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)Lcom/google/android/gms/update/UpdateFromSdCardService;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->e:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->d:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 625
    iget v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 626
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-virtual {v0}, Lcom/google/android/gms/update/UpdateFromSdCardService;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->f:I

    .line 628
    :cond_0
    iget v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->f:I

    packed-switch v0, :pswitch_data_0

    .line 651
    :goto_0
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->d()V

    .line 657
    :cond_1
    :goto_1
    return-void

    .line 630
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->z:I

    if-ne v0, v1, :cond_4

    .line 631
    iget-boolean v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->g:Z

    if-eqz v0, :cond_2

    .line 633
    invoke-direct {p0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->e()V

    goto :goto_1

    .line 636
    :cond_2
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "installing update"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "startCountdown"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->i:J

    cmp-long v0, v0, v6

    if-lez v0, :cond_3

    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "skipping, as a countdown is already in progress."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    iput-boolean v4, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->g:Z

    invoke-static {p0}, Lcom/google/android/gms/update/o;->b(Landroid/content/Context;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x2710

    add-long/2addr v0, v2

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->i:J

    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "resumeCountdown"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->xU:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(II)V

    sget v0, Lcom/google/android/gms/j;->A:I

    const/16 v1, 0x8

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(II)V

    iget-wide v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->i:J

    invoke-direct {p0, v0, v1, v4}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(JZ)V

    goto :goto_1

    .line 639
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->A:I

    if-ne v0, v1, :cond_1

    .line 641
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 642
    iput-object v2, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->c:Ljava/lang/String;

    .line 643
    iput-object v2, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->d:Landroid/view/View;

    .line 644
    iput-wide v6, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->i:J

    goto :goto_0

    .line 649
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->z:I

    if-ne v0, v1, :cond_1

    goto :goto_0

    .line 628
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const-wide/16 v6, 0x0

    .line 182
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 186
    invoke-static {}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    sget v0, Lcom/google/android/gms/p;->yt:I

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 189
    const/16 v1, 0x11

    invoke-virtual {v0}, Landroid/widget/Toast;->getXOffset()I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/Toast;->getYOffset()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 191
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 192
    invoke-virtual {p0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->finish()V

    .line 243
    :goto_0
    return-void

    .line 196
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k:Ljava/util/List;

    .line 204
    if-eqz p1, :cond_5

    .line 205
    const-string v0, "path_array"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 206
    const-string v0, "path_array"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 207
    iget-object v4, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 210
    :cond_1
    const-string v0, "last_clicked_item"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 211
    const-string v0, "last_clicked_item"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->c:Ljava/lang/String;

    .line 213
    :cond_2
    const-string v0, "countdown_end"

    invoke-virtual {p1, v0, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->i:J

    .line 217
    :goto_2
    iput-wide v6, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->j:J

    .line 221
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->f:I

    .line 222
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b:Z

    if-nez v0, :cond_6

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->n:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b:Z

    if-nez v0, :cond_3

    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "bindService call to UpdateFromSdCardService returned false!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->h:Landroid/os/Handler;

    .line 227
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 228
    sget v0, Lcom/google/android/gms/l;->fs:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->setContentView(I)V

    .line 231
    sget v0, Lcom/google/android/gms/j;->mo:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 232
    if-eqz v0, :cond_4

    .line 233
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 239
    :cond_4
    :goto_4
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_SHARED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.gms.update.UpdateFromSdCard.STATUS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->l:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 241
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 242
    sget v0, Lcom/google/android/gms/j;->A:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 215
    :cond_5
    iput-wide v6, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->i:J

    goto/16 :goto_2

    .line 222
    :cond_6
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 236
    :cond_7
    sget v0, Lcom/google/android/gms/l;->fr:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->setContentView(I)V

    goto :goto_4
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 264
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    invoke-super {p0}, Landroid/support/v4/app/q;->onDestroy()V

    .line 269
    invoke-static {}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 272
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b()V

    .line 273
    sget-boolean v0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b:Z

    if-nez v0, :cond_2

    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "Skipping disconnect, as not connected."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->e:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0

    .line 273
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->n:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    goto :goto_1
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 598
    const-string v1, "UpdateFromSdCardActivity"

    const-string v2, "activity key down"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    .line 600
    iget-object v1, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-virtual {v1}, Lcom/google/android/gms/update/UpdateFromSdCardService;->a()I

    move-result v1

    .line 602
    if-ne v1, v0, :cond_1

    .line 604
    iget-object v1, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 605
    invoke-virtual {p0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->finish()V

    .line 615
    :goto_0
    return v0

    .line 607
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->g()V

    .line 608
    invoke-direct {p0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->c()V

    goto :goto_0

    .line 611
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->d()V

    goto :goto_0

    .line 615
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/q;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 254
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    invoke-super {p0}, Landroid/support/v4/app/q;->onPause()V

    .line 257
    iget-boolean v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->g:Z

    if-nez v0, :cond_0

    .line 258
    invoke-direct {p0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->e()V

    .line 260
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 247
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    invoke-super {p0}, Landroid/support/v4/app/q;->onResume()V

    .line 250
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 530
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "onSaveInstanceState"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 532
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 533
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 534
    iget-object v1, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 535
    const-string v1, "path_array"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 537
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 538
    const-string v0, "last_clicked_item"

    iget-object v1, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    :cond_1
    const-string v0, "countdown_end"

    iget-wide v2, p0, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->i:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 541
    return-void
.end method
