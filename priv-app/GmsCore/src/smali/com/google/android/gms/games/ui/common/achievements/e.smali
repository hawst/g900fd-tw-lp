.class final Lcom/google/android/gms/games/ui/common/achievements/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Landroid/widget/TextView;

.field final synthetic b:Landroid/view/View;

.field final synthetic c:Landroid/widget/TextView;

.field final synthetic d:Landroid/widget/TextView;

.field final synthetic e:Landroid/widget/TextView;

.field final synthetic f:Lcom/google/android/gms/games/ui/common/achievements/d;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/common/achievements/d;Landroid/widget/TextView;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/achievements/e;->f:Lcom/google/android/gms/games/ui/common/achievements/d;

    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/achievements/e;->a:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/google/android/gms/games/ui/common/achievements/e;->b:Landroid/view/View;

    iput-object p4, p0, Lcom/google/android/gms/games/ui/common/achievements/e;->c:Landroid/widget/TextView;

    iput-object p5, p0, Lcom/google/android/gms/games/ui/common/achievements/e;->d:Landroid/widget/TextView;

    iput-object p6, p0, Lcom/google/android/gms/games/ui/common/achievements/e;->e:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x3

    const/4 v6, -0x2

    .line 240
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/e;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 246
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/e;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 247
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/e;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    .line 248
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/achievements/e;->a:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    .line 249
    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/achievements/e;->d:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    .line 251
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 254
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 258
    add-int/2addr v1, v2

    add-int/2addr v1, v3

    if-gt v1, v0, :cond_1

    .line 260
    sget v0, Lcom/google/android/gms/j;->kb:I

    invoke-virtual {v4, v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 261
    sget v0, Lcom/google/android/gms/j;->kb:I

    invoke-virtual {v5, v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 268
    :goto_1
    const/4 v0, 0x1

    sget v1, Lcom/google/android/gms/j;->kb:I

    invoke-virtual {v4, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 269
    const/16 v0, 0xb

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 270
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/e;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 271
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/e;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 272
    return-void

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/e;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    .line 265
    :cond_1
    sget v0, Lcom/google/android/gms/j;->si:I

    invoke-virtual {v4, v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 266
    sget v0, Lcom/google/android/gms/j;->si:I

    invoke-virtual {v5, v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_1
.end method
