.class public final Lcom/google/android/gms/fitness/c/a/a/d;
.super Lcom/google/android/gms/fitness/c/a/a/a;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/fitness/data/DataType;

.field private final c:Lcom/google/android/gms/fitness/data/DataType;

.field private final d:Lcom/google/android/gms/fitness/data/Field;

.field private final e:Lcom/google/android/gms/fitness/data/Field;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/fitness/data/DataType;Lcom/google/android/gms/fitness/data/DataType;Lcom/google/android/gms/fitness/data/Field;Lcom/google/android/gms/fitness/data/Field;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 37
    invoke-direct {p0}, Lcom/google/android/gms/fitness/c/a/a/a;-><init>()V

    .line 38
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataType;

    iput-object v0, p0, Lcom/google/android/gms/fitness/c/a/a/d;->b:Lcom/google/android/gms/fitness/data/DataType;

    .line 39
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataType;

    iput-object v0, p0, Lcom/google/android/gms/fitness/c/a/a/d;->c:Lcom/google/android/gms/fitness/data/DataType;

    .line 40
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/Field;

    iput-object v0, p0, Lcom/google/android/gms/fitness/c/a/a/d;->d:Lcom/google/android/gms/fitness/data/Field;

    .line 41
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/Field;

    iput-object v0, p0, Lcom/google/android/gms/fitness/c/a/a/d;->e:Lcom/google/android/gms/fitness/data/Field;

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/fitness/c/a/a/d;->d:Lcom/google/android/gms/fitness/data/Field;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Field;->b()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Format must be FORMAT_INT32"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/fitness/c/a/a/d;->e:Lcom/google/android/gms/fitness/data/Field;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Field;->b()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "Format must be FORMAT_INT32"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 46
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataType;->b()Ljava/util/List;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/fitness/c/a/a/d;->d:Lcom/google/android/gms/fitness/data/Field;

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v3, "%s not a field of %s"

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/gms/fitness/c/a/a/d;->d:Lcom/google/android/gms/fitness/data/Field;

    aput-object v5, v4, v2

    aput-object p1, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 48
    invoke-virtual {p2}, Lcom/google/android/gms/fitness/data/DataType;->b()Ljava/util/List;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/fitness/c/a/a/d;->e:Lcom/google/android/gms/fitness/data/Field;

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v3, "%s not a field of %s"

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/gms/fitness/c/a/a/d;->e:Lcom/google/android/gms/fitness/data/Field;

    aput-object v5, v4, v2

    aput-object p2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 50
    return-void

    :cond_0
    move v0, v2

    .line 42
    goto :goto_0

    :cond_1
    move v0, v2

    .line 44
    goto :goto_1
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/fitness/data/DataSource;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Lcom/google/android/gms/fitness/data/f;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/data/f;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/fitness/c/a/a/d;->c:Lcom/google/android/gms/fitness/data/DataType;

    iput-object v1, v0, Lcom/google/android/gms/fitness/data/f;->a:Lcom/google/android/gms/fitness/data/DataType;

    sget-object v1, Lcom/google/android/gms/fitness/data/Application;->a:Lcom/google/android/gms/fitness/data/Application;

    iput-object v1, v0, Lcom/google/android/gms/fitness/data/f;->e:Lcom/google/android/gms/fitness/data/Application;

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/android/gms/fitness/data/f;->b:I

    .line 69
    iget-object v1, p0, Lcom/google/android/gms/fitness/c/a/a/d;->a:Lcom/google/android/gms/fitness/data/Device;

    if-eqz v1, :cond_0

    .line 70
    iget-object v1, p0, Lcom/google/android/gms/fitness/c/a/a/d;->a:Lcom/google/android/gms/fitness/data/Device;

    iput-object v1, v0, Lcom/google/android/gms/fitness/data/f;->d:Lcom/google/android/gms/fitness/data/Device;

    .line 72
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/f;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataPoint;)Ljava/lang/Iterable;
    .locals 12

    .prologue
    const-wide/32 v10, 0x7fffffff

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 77
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataPoint;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/c/a/a/d;->b:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Require %s measurement instead of: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/fitness/c/a/a/d;->b:Lcom/google/android/gms/fitness/data/DataType;

    aput-object v2, v1, v7

    aput-object p1, v1, v8

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->f(Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v7

    :goto_0
    if-nez v0, :cond_1

    .line 78
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 92
    :goto_1
    return-object v0

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/c/a/a/d;->d:Lcom/google/android/gms/fitness/data/Field;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Lcom/google/android/gms/fitness/data/Field;)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Value;->a()Z

    move-result v0

    goto :goto_0

    .line 81
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/c/a/a/d;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v1

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/fitness/c/a/a/d;->d:Lcom/google/android/gms/fitness/data/Field;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Lcom/google/android/gms/fitness/data/Field;)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Value;->c()I

    move-result v0

    int-to-long v2, v0

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    .line 84
    cmp-long v0, v2, v10

    if-gtz v0, :cond_2

    :goto_2
    long-to-int v0, v2

    .line 86
    iget-object v2, p0, Lcom/google/android/gms/fitness/c/a/a/d;->e:Lcom/google/android/gms/fitness/data/Field;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Lcom/google/android/gms/fitness/data/Field;)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/fitness/data/Value;->a(I)V

    .line 87
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->c(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/fitness/data/DataPoint;->a(JJLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/DataPoint;

    .line 91
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataPoint;->e()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/fitness/data/DataPoint;->a(J)Lcom/google/android/gms/fitness/data/DataPoint;

    .line 92
    new-array v0, v8, [Lcom/google/android/gms/fitness/data/DataPoint;

    aput-object v1, v0, v7

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 84
    :cond_2
    sub-long/2addr v2, v10

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    goto :goto_2
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/fitness/data/Device;)V
    .locals 0

    .prologue
    .line 29
    invoke-super {p0, p1}, Lcom/google/android/gms/fitness/c/a/a/a;->a(Lcom/google/android/gms/fitness/data/Device;)V

    return-void
.end method

.method public final b()Lcom/google/android/gms/fitness/data/DataType;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/fitness/c/a/a/d;->b:Lcom/google/android/gms/fitness/data/DataType;

    return-object v0
.end method

.method public final c()Lcom/google/android/gms/fitness/data/DataType;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/fitness/c/a/a/d;->c:Lcom/google/android/gms/fitness/data/DataType;

    return-object v0
.end method
