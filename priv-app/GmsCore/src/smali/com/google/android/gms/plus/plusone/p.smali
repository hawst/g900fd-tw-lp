.class public final Lcom/google/android/gms/plus/plusone/p;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/common/es;
.implements Lcom/google/android/gms/common/et;


# static fields
.field static final a:[Ljava/lang/String;


# instance fields
.field b:Landroid/widget/ProgressBar;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Lcom/google/android/gms/plus/internal/ad;

.field private i:Lcom/google/android/gms/plus/internal/ab;

.field private j:Lcom/google/android/gms/plus/data/a/a;

.field private k:Lcom/google/android/gms/plus/data/a/c;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Lcom/google/android/gms/common/c;

.field private s:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

.field private final t:Lcom/google/android/gms/plus/internal/aq;

.field private final u:Lcom/google/android/gms/plus/internal/aj;

.field private final v:Lcom/google/android/gms/plus/internal/aq;

.field private final w:Lcom/google/android/gms/plus/internal/au;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 74
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "https://www.googleapis.com/auth/plus.me"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "https://www.googleapis.com/auth/pos"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "https://www.googleapis.com/auth/plus.stream.write"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/plus/plusone/p;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 199
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 105
    new-instance v0, Lcom/google/android/gms/plus/plusone/q;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/plusone/q;-><init>(Lcom/google/android/gms/plus/plusone/p;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->t:Lcom/google/android/gms/plus/internal/aq;

    .line 125
    new-instance v0, Lcom/google/android/gms/plus/plusone/r;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/plusone/r;-><init>(Lcom/google/android/gms/plus/plusone/p;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->u:Lcom/google/android/gms/plus/internal/aj;

    .line 155
    new-instance v0, Lcom/google/android/gms/plus/plusone/s;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/plusone/s;-><init>(Lcom/google/android/gms/plus/plusone/p;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->v:Lcom/google/android/gms/plus/internal/aq;

    .line 174
    new-instance v0, Lcom/google/android/gms/plus/plusone/t;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/plusone/t;-><init>(Lcom/google/android/gms/plus/plusone/p;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->w:Lcom/google/android/gms/plus/internal/au;

    .line 200
    sget-object v0, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->h:Lcom/google/android/gms/plus/internal/ad;

    .line 201
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/plusone/p;Lcom/google/android/gms/plus/data/a/a;)Lcom/google/android/gms/plus/data/a/a;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/p;->j:Lcom/google/android/gms/plus/data/a/a;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/plus/plusone/p;Lcom/google/android/gms/plus/data/a/c;)Lcom/google/android/gms/plus/data/a/c;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/p;->k:Lcom/google/android/gms/plus/data/a/c;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/plus/plusone/p;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/p;->g:Ljava/lang/String;

    return-object p1
.end method

.method private a(Lcom/google/android/gms/plus/data/a/a;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 466
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 467
    if-nez v0, :cond_0

    .line 494
    :goto_0
    return-void

    .line 470
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getView()Landroid/view/View;

    move-result-object v1

    sget v3, Lcom/google/android/gms/j;->pj:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Landroid/view/ViewGroup;

    .line 472
    invoke-virtual {v6}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 473
    new-instance v7, Lcom/google/android/gms/plus/plusone/u;

    invoke-direct {v7, v0}, Lcom/google/android/gms/plus/plusone/u;-><init>(Landroid/content/Context;)V

    .line 474
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->i:Lcom/google/android/gms/plus/internal/ab;

    invoke-virtual {v7, v1}, Lcom/google/android/gms/plus/plusone/u;->a(Lcom/google/android/gms/plus/internal/ab;)V

    .line 475
    invoke-virtual {v7, p1}, Lcom/google/android/gms/plus/plusone/u;->a(Lcom/google/android/gms/plus/data/a/a;)Z

    move-result v1

    .line 477
    iget-boolean v3, p0, Lcom/google/android/gms/plus/plusone/p;->m:Z

    if-nez v3, :cond_1

    if-eqz p1, :cond_1

    .line 478
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/gms/plus/plusone/p;->m:Z

    .line 479
    if-eqz v1, :cond_2

    .line 480
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/a/g;->m:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/plus/a/h;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 493
    :cond_1
    :goto_1
    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 486
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/a/g;->p:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/plus/a/h;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private a(Lcom/google/android/gms/plus/data/a/c;)V
    .locals 4

    .prologue
    .line 452
    if-nez p1, :cond_0

    .line 463
    :goto_0
    return-void

    .line 455
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->pm:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/data/internal/PlusImageView;

    .line 457
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->i:Lcom/google/android/gms/plus/internal/ab;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Lcom/google/android/gms/plus/internal/ab;)V

    .line 458
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->i:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v1}, Lcom/google/android/gms/plus/internal/ab;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "content"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "com.google.android.gms.plus"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "avatars"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Landroid/net/Uri;)V

    .line 460
    invoke-virtual {p1}, Lcom/google/android/gms/plus/data/a/c;->a()Ljava/lang/String;

    move-result-object v1

    .line 461
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getView()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/google/android/gms/j;->pn:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 462
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 447
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->pi:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 448
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 449
    return-void

    .line 448
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/plusone/p;)Z
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/p;->l:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/plusone/p;)Lcom/google/android/gms/plus/internal/aq;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->v:Lcom/google/android/gms/plus/internal/aq;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/plusone/p;Lcom/google/android/gms/plus/data/a/a;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/plusone/p;->a(Lcom/google/android/gms/plus/data/a/a;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/plus/plusone/p;Lcom/google/android/gms/plus/data/a/c;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/plusone/p;->a(Lcom/google/android/gms/plus/data/a/c;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/plus/plusone/p;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/plus/plusone/p;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/plus/plusone/p;)Lcom/google/android/gms/plus/internal/ab;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->i:Lcom/google/android/gms/plus/internal/ab;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/plus/plusone/p;)Z
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/p;->p:Z

    return v0
.end method

.method static synthetic g(Lcom/google/android/gms/plus/plusone/p;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/plus/plusone/p;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/plus/plusone/p;)Lcom/google/android/gms/plus/data/a/a;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->j:Lcom/google/android/gms/plus/data/a/a;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/gms/plus/plusone/p;)Z
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/p;->o:Z

    return v0
.end method

.method static synthetic k(Lcom/google/android/gms/plus/plusone/p;)Z
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/p;->q:Z

    return v0
.end method

.method static synthetic l(Lcom/google/android/gms/plus/plusone/p;)Lcom/google/android/gms/plus/data/a/c;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->k:Lcom/google/android/gms/plus/data/a/c;

    return-object v0
.end method


# virtual methods
.method public final T_()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 521
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 522
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->i:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    .line 531
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/p;->n:Z

    if-nez v0, :cond_1

    .line 533
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 534
    if-eqz v0, :cond_1

    .line 535
    iput-boolean v6, p0, Lcom/google/android/gms/plus/plusone/p;->n:Z

    .line 536
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/gms/plus/a/g;->n:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/plus/a/h;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 540
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/plus/a/h;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/plus/a/h;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 546
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/p;->l:Z

    if-eqz v0, :cond_2

    .line 547
    iput-boolean v6, p0, Lcom/google/android/gms/plus/plusone/p;->o:Z

    .line 550
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 552
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->i:Lcom/google/android/gms/plus/internal/ab;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->t:Lcom/google/android/gms/plus/internal/aq;

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/p;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/aq;Ljava/lang/String;)V

    .line 558
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->j:Lcom/google/android/gms/plus/data/a/a;

    if-nez v0, :cond_3

    .line 559
    iput-boolean v6, p0, Lcom/google/android/gms/plus/plusone/p;->p:Z

    .line 560
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->i:Lcom/google/android/gms/plus/internal/ab;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->u:Lcom/google/android/gms/plus/internal/aj;

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/p;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/aj;Ljava/lang/String;)V

    .line 562
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->k:Lcom/google/android/gms/plus/data/a/c;

    if-nez v0, :cond_4

    .line 563
    iput-boolean v6, p0, Lcom/google/android/gms/plus/plusone/p;->q:Z

    .line 564
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->i:Lcom/google/android/gms/plus/internal/ab;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->w:Lcom/google/android/gms/plus/internal/au;

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/au;)V

    .line 566
    :cond_4
    invoke-direct {p0, v6}, Lcom/google/android/gms/plus/plusone/p;->a(Z)V

    .line 567
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->b:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/p;->b(Landroid/widget/ProgressBar;)V

    .line 568
    :cond_5
    :goto_1
    return-void

    .line 523
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->i:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v1}, Lcom/google/android/gms/plus/internal/ab;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 525
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 526
    if-eqz v0, :cond_5

    .line 527
    invoke-virtual {v0, v6}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_1

    .line 554
    :cond_7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/p;->l:Z

    .line 555
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->i:Lcom/google/android/gms/plus/internal/ab;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->v:Lcom/google/android/gms/plus/internal/aq;

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/p;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/p;->g:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/aq;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final W_()V
    .locals 0

    .prologue
    .line 573
    return-void
.end method

.method public final a()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 625
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 626
    if-nez v0, :cond_1

    .line 649
    :cond_0
    :goto_0
    return-void

    .line 629
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->i:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v1}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 630
    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/p;->d:Z

    if-eqz v1, :cond_2

    .line 632
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/a/g;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/plus/a/h;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 643
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/plus/a/h;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/plus/a/h;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 647
    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/p;->d:Z

    if-eqz v1, :cond_3

    const/4 v1, -0x1

    :goto_2
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    goto :goto_0

    .line 638
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/a/g;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/plus/a/h;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_1

    .line 647
    :cond_3
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 604
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 605
    if-nez v0, :cond_0

    .line 618
    :goto_0
    return-void

    .line 609
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->r:Lcom/google/android/gms/common/c;

    .line 610
    const/4 v1, -0x1

    if-eq p1, v1, :cond_1

    .line 611
    invoke-virtual {v0, p1}, Landroid/app/Activity;->setResult(I)V

    .line 612
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 616
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->i:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    goto :goto_0
.end method

.method public final a(Landroid/widget/ProgressBar;)V
    .locals 1

    .prologue
    .line 502
    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/p;->b:Landroid/widget/ProgressBar;

    .line 503
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->b:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/p;->b(Landroid/widget/ProgressBar;)V

    .line 504
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 578
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 579
    if-nez v0, :cond_0

    .line 596
    :goto_0
    return-void

    .line 583
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 584
    invoke-virtual {v0, v3}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    .line 585
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 588
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Lcom/google/android/gms/common/c;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 591
    :catch_0
    move-exception v1

    invoke-virtual {v0, v3}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    .line 594
    :cond_2
    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/p;->r:Lcom/google/android/gms/common/c;

    goto :goto_0
.end method

.method protected final b(Landroid/widget/ProgressBar;)V
    .locals 1

    .prologue
    .line 512
    if-eqz p1, :cond_1

    .line 513
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->i:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/p;->o:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/p;->p:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/p;->q:Z

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 516
    :cond_1
    return-void

    .line 513
    :cond_2
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    .line 274
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 277
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 278
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->pe:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 279
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getView()Landroid/view/View;

    move-result-object v1

    sget v3, Lcom/google/android/gms/j;->pf:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 281
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 283
    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 285
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/gms/p;->ux:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 292
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->pl:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 295
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->pk:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 298
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->pg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 300
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->i:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/plusone/p;->a(Z)V

    .line 301
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->k:Lcom/google/android/gms/plus/data/a/c;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/plusone/p;->a(Lcom/google/android/gms/plus/data/a/c;)V

    .line 302
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->j:Lcom/google/android/gms/plus/data/a/a;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/plusone/p;->a(Lcom/google/android/gms/plus/data/a/a;)V

    .line 303
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->b:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/p;->b(Landroid/widget/ProgressBar;)V

    .line 306
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->b(Landroid/os/Bundle;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->s:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    .line 307
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->s:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    const-string v2, "gppo0"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/k;->a(Landroid/app/Activity;Lcom/google/android/gms/plus/internal/PlusCommonExtras;Ljava/lang/String;)V

    .line 309
    return-void

    .line 287
    :catch_0
    move-exception v2

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 288
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v7, -0x1

    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 314
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 315
    if-nez v0, :cond_1

    .line 388
    :cond_0
    :goto_0
    return-void

    .line 318
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->i:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v1}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 322
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 323
    sget v3, Lcom/google/android/gms/j;->pl:I

    if-ne v1, v3, :cond_4

    .line 324
    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/p;->d:Z

    if-eqz v1, :cond_2

    .line 326
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/a/g;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/plus/a/h;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 337
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/a/h;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/plus/a/h;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    invoke-static {v0, v1, v3, v4, v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 341
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->i:Lcom/google/android/gms/plus/internal/ab;

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/p;->c:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/google/android/gms/plus/internal/ab;->b(Lcom/google/android/gms/plus/internal/aq;Ljava/lang/String;)V

    .line 342
    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/p;->d:Z

    if-eqz v1, :cond_3

    move v1, v6

    :goto_2
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 343
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 332
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/a/g;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/plus/a/h;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move v1, v7

    .line 342
    goto :goto_2

    .line 344
    :cond_4
    sget v3, Lcom/google/android/gms/j;->pk:I

    if-ne v1, v3, :cond_16

    .line 346
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/a/g;->o:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/plus/a/h;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 350
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/a/h;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/plus/a/h;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    invoke-static {v0, v1, v3, v4, v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 356
    new-instance v4, Lcom/google/android/gms/plus/p;

    invoke-direct {v4, v0}, Lcom/google/android/gms/plus/p;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->c:Ljava/lang/String;

    iget-object v3, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    const-string v5, "android.intent.extra.TEXT"

    invoke-virtual {v3, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    const-string v1, "text/plain"

    iget-object v3, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    invoke-virtual {v3, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->b:Ljava/util/ArrayList;

    if-eqz v1, :cond_b

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v8, :cond_b

    move v3, v8

    :goto_3
    const-string v1, "android.intent.action.SEND_MULTIPLE"

    iget-object v5, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    const-string v9, "com.google.android.apps.plus.GOOGLE_INTERACTIVE_POST"

    invoke-virtual {v1, v9, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v3, :cond_5

    if-nez v9, :cond_c

    :cond_5
    move v1, v8

    :goto_4
    const-string v10, "Call-to-action buttons are only available for URLs."

    invoke-static {v1, v10}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    if-eqz v9, :cond_6

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    const-string v10, "com.google.android.apps.plus.CONTENT_URL"

    invoke-virtual {v1, v10}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    :cond_6
    move v1, v8

    :goto_5
    const-string v10, "The content URL is required for interactive posts."

    invoke-static {v1, v10}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    if-eqz v9, :cond_7

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    const-string v9, "com.google.android.apps.plus.CONTENT_URL"

    invoke-virtual {v1, v9}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    const-string v9, "com.google.android.apps.plus.CONTENT_DEEP_LINK_ID"

    invoke-virtual {v1, v9}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    :cond_7
    move v1, v8

    :goto_6
    const-string v9, "Must set content URL or content deep-link ID to use a call-to-action button."

    invoke-static {v1, v9}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    const-string v9, "com.google.android.apps.plus.CONTENT_DEEP_LINK_ID"

    invoke-virtual {v1, v9}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    const-string v9, "com.google.android.apps.plus.CONTENT_DEEP_LINK_ID"

    invoke-virtual {v1, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_f

    const-string v1, "GooglePlusPlatform"

    const-string v9, "The provided deep-link ID is empty."

    invoke-static {v1, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v6

    :goto_7
    const-string v9, "The specified deep-link ID was malformed."

    invoke-static {v1, v9}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    :cond_8
    if-nez v3, :cond_9

    if-eqz v5, :cond_9

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    const-string v9, "android.intent.action.SEND"

    invoke-virtual {v1, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->b:Ljava/util/ArrayList;

    if-eqz v1, :cond_11

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_11

    iget-object v9, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    const-string v10, "android.intent.extra.STREAM"

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {v9, v10, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :goto_8
    iput-object v2, v4, Lcom/google/android/gms/plus/p;->b:Ljava/util/ArrayList;

    :cond_9
    if-eqz v3, :cond_a

    if-nez v5, :cond_a

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->b:Ljava/util/ArrayList;

    if-eqz v1, :cond_12

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_12

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    const-string v2, "android.intent.extra.STREAM"

    iget-object v3, v4, Lcom/google/android/gms/plus/p;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    :cond_a
    :goto_9
    const-string v1, "com.google.android.gms.plus.action.SHARE_INTERNAL_GOOGLE"

    iget-object v2, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    const-string v2, "com.google.android.gms"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    .line 360
    :goto_a
    const-string v2, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 361
    const-string v2, "com.google.android.apps.plus.IS_FROM_PLUSONE"

    invoke-virtual {v1, v2, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 362
    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/p;->s:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a(Landroid/content/Intent;)V

    .line 363
    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/plusone/p;->startActivity(Landroid/content/Intent;)V

    .line 365
    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/p;->d:Z

    if-eqz v1, :cond_15

    :goto_b
    invoke-virtual {v0, v7}, Landroid/app/Activity;->setResult(I)V

    .line 366
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_b
    move v3, v6

    .line 356
    goto/16 :goto_3

    :cond_c
    move v1, v6

    goto/16 :goto_4

    :cond_d
    move v1, v6

    goto/16 :goto_5

    :cond_e
    move v1, v6

    goto/16 :goto_6

    :cond_f
    const-string v9, " "

    invoke-virtual {v1, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_10

    const-string v1, "GooglePlusPlatform"

    const-string v9, "Spaces are not allowed in deep-link IDs."

    invoke-static {v1, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v6

    goto/16 :goto_7

    :cond_10
    move v1, v8

    goto/16 :goto_7

    :cond_11
    iget-object v1, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    const-string v9, "android.intent.extra.STREAM"

    invoke-virtual {v1, v9}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto/16 :goto_8

    :cond_12
    iget-object v1, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto :goto_9

    :cond_13
    iget-object v1, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_14

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    const-string v2, "com.google.android.gms.plus.action.SHARE_GOOGLE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    const-string v2, "com.google.android.gms"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    goto :goto_a

    :cond_14
    iget-object v1, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    const-string v2, "com.google.android.apps.plus"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, v4, Lcom/google/android/gms/plus/p;->a:Landroid/content/Intent;

    goto :goto_a

    :cond_15
    move v7, v6

    .line 365
    goto :goto_b

    .line 367
    :cond_16
    sget v3, Lcom/google/android/gms/j;->pg:I

    if-ne v1, v3, :cond_0

    .line 368
    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/p;->d:Z

    if-eqz v1, :cond_17

    .line 370
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/a/g;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/plus/a/h;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 381
    :goto_c
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/plus/a/h;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/plus/a/h;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 385
    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/p;->d:Z

    if-eqz v1, :cond_18

    :goto_d
    invoke-virtual {v0, v7}, Landroid/app/Activity;->setResult(I)V

    .line 386
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 376
    :cond_17
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/a/g;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/plus/a/h;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_c

    :cond_18
    move v7, v6

    .line 385
    goto :goto_d
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 211
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 213
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 217
    const-string v1, "PlusOneFragment#mAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 218
    const-string v2, "PlusOneFragment#mToken"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/plus/plusone/p;->g:Ljava/lang/String;

    .line 219
    const-string v2, "PlusOneFragment#mApplyPlusOne"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/gms/plus/plusone/p;->d:Z

    .line 220
    const-string v2, "PlusOneFragment#mUrl"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/plus/plusone/p;->c:Ljava/lang/String;

    .line 221
    const-string v2, "PlusOneFragment#mCallingPackage"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    .line 223
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 228
    new-instance v2, Lcom/google/android/gms/plus/internal/cn;

    invoke-direct {v2, v0}, Lcom/google/android/gms/plus/internal/cn;-><init>(Landroid/content/Context;)V

    iput-object v1, v2, Lcom/google/android/gms/plus/internal/cn;->a:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/plus/plusone/p;->a:[Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/plus/internal/cn;->a([Ljava/lang/String;)Lcom/google/android/gms/plus/internal/cn;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/p;->e:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/plus/internal/cn;->c:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/plus/internal/cn;->b:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/cn;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v1

    .line 234
    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/p;->h:Lcom/google/android/gms/plus/internal/ad;

    invoke-interface {v2, v0, v1, p0, p0}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->i:Lcom/google/android/gms/plus/internal/ab;

    .line 236
    iput-boolean v3, p0, Lcom/google/android/gms/plus/plusone/p;->o:Z

    .line 237
    iput-boolean v3, p0, Lcom/google/android/gms/plus/plusone/p;->p:Z

    .line 238
    iput-boolean v3, p0, Lcom/google/android/gms/plus/plusone/p;->q:Z

    .line 240
    if-nez p1, :cond_1

    .line 241
    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/p;->d:Z

    iput-boolean v1, p0, Lcom/google/android/gms/plus/plusone/p;->l:Z

    .line 242
    iput-boolean v3, p0, Lcom/google/android/gms/plus/plusone/p;->m:Z

    .line 243
    iput-boolean v3, p0, Lcom/google/android/gms/plus/plusone/p;->n:Z

    .line 244
    invoke-virtual {v0, v3}, Landroid/app/Activity;->setResult(I)V

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    const-string v0, "pendingInsert"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/p;->l:Z

    .line 247
    const-string v0, "loggedPreview"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/p;->m:Z

    .line 248
    const-string v0, "loggedPlusOne"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/p;->n:Z

    .line 249
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 250
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    .line 252
    :cond_2
    const-string v0, "token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 253
    const-string v0, "token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->g:Ljava/lang/String;

    .line 255
    :cond_3
    const-string v0, "linkPreview"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 256
    new-instance v0, Lcom/google/android/gms/plus/data/a/a;

    const-string v1, "linkPreview"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/data/a/a;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->j:Lcom/google/android/gms/plus/data/a/a;

    .line 259
    :cond_4
    const-string v0, "signUpState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    new-instance v0, Lcom/google/android/gms/plus/data/a/c;

    const-string v1, "signUpState"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/data/a/c;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->k:Lcom/google/android/gms/plus/data/a/c;

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 269
    sget v0, Lcom/google/android/gms/l;->dV:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 407
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 408
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->r:Lcom/google/android/gms/common/c;

    if-eqz v0, :cond_0

    .line 410
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->r:Lcom/google/android/gms/common/c;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/c;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 417
    :cond_0
    :goto_0
    return-void

    .line 413
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->r:Lcom/google/android/gms/common/c;

    .line 414
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v4/app/q;->showDialog(I)V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 421
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 423
    const-string v0, "pendingInsert"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/p;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 424
    const-string v0, "loggedPreview"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/p;->m:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 425
    const-string v0, "loggedPlusOne"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/p;->n:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 427
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 428
    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 431
    const-string v0, "token"

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->j:Lcom/google/android/gms/plus/data/a/a;

    if-eqz v0, :cond_2

    .line 434
    const-string v0, "linkPreview"

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->j:Lcom/google/android/gms/plus/data/a/a;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/data/a/a;->f()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 436
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->k:Lcom/google/android/gms/plus/data/a/c;

    if-eqz v0, :cond_3

    .line 437
    const-string v0, "signUpState"

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/p;->k:Lcom/google/android/gms/plus/data/a/c;

    iget-object v1, v1, Lcom/google/android/gms/plus/data/a/c;->a:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 439
    :cond_3
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 398
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 399
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->r:Lcom/google/android/gms/common/c;

    .line 400
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->i:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 401
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->b:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/p;->b(Landroid/widget/ProgressBar;)V

    .line 402
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 392
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 393
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/p;->i:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->b()V

    .line 394
    return-void
.end method
