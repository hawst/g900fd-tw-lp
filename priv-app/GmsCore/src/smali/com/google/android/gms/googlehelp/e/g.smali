.class public final Lcom/google/android/gms/googlehelp/e/g;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:J

.field public c:Z

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1418
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1419
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/g;->a:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/googlehelp/e/g;->b:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/e/g;->c:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/g;->d:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/googlehelp/e/g;->cachedSize:I

    .line 1420
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 1496
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1497
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/g;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1498
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/g;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1501
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/googlehelp/e/g;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 1502
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/googlehelp/e/g;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1505
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/googlehelp/e/g;->c:Z

    if-eqz v1, :cond_2

    .line 1506
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/gms/googlehelp/e/g;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1509
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/g;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1510
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/g;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1513
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1433
    if-ne p1, p0, :cond_1

    .line 1460
    :cond_0
    :goto_0
    return v0

    .line 1436
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/googlehelp/e/g;

    if-nez v2, :cond_2

    move v0, v1

    .line 1437
    goto :goto_0

    .line 1439
    :cond_2
    check-cast p1, Lcom/google/android/gms/googlehelp/e/g;

    .line 1440
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/g;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 1441
    iget-object v2, p1, Lcom/google/android/gms/googlehelp/e/g;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1442
    goto :goto_0

    .line 1444
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/g;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/g;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1445
    goto :goto_0

    .line 1447
    :cond_4
    iget-wide v2, p0, Lcom/google/android/gms/googlehelp/e/g;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/googlehelp/e/g;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    .line 1448
    goto :goto_0

    .line 1450
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/gms/googlehelp/e/g;->c:Z

    iget-boolean v3, p1, Lcom/google/android/gms/googlehelp/e/g;->c:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 1451
    goto :goto_0

    .line 1453
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/g;->d:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1454
    iget-object v2, p1, Lcom/google/android/gms/googlehelp/e/g;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1455
    goto :goto_0

    .line 1457
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/g;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/g;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1458
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1465
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/g;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1468
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/googlehelp/e/g;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/googlehelp/e/g;->b:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 1470
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/e/g;->c:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v2

    .line 1471
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/g;->d:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 1473
    return v0

    .line 1465
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/g;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1470
    :cond_1
    const/16 v0, 0x4d5

    goto :goto_1

    .line 1471
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/g;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 1389
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/g;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/googlehelp/e/g;->b:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/e/g;->c:Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/g;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 1479
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/g;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1480
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/g;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1482
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/googlehelp/e/g;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 1483
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/googlehelp/e/g;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 1485
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/e/g;->c:Z

    if-eqz v0, :cond_2

    .line 1486
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/gms/googlehelp/e/g;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1488
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/g;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1489
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/g;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1491
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1492
    return-void
.end method
