.class final Lcom/google/android/gms/games/ui/common/matches/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/common/matches/r;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/common/matches/r;)V
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/matches/s;->a:Lcom/google/android/gms/games/ui/common/matches/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 4

    .prologue
    .line 324
    check-cast p1, Lcom/google/android/gms/games/z;

    invoke-interface {p1}, Lcom/google/android/gms/games/z;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/gms/games/z;->c()Lcom/google/android/gms/games/t;

    move-result-object v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/s;->a:Lcom/google/android/gms/games/ui/common/matches/r;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/common/matches/r;->isDetached()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/s;->a:Lcom/google/android/gms/games/ui/common/matches/r;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/common/matches/r;->isRemoving()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/games/t;->w_()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/s;->a:Lcom/google/android/gms/games/ui/common/matches/r;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/common/matches/r;->a(Lcom/google/android/gms/games/ui/common/matches/r;)Lcom/google/android/gms/games/ui/q;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/ui/q;->b(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/games/t;->w_()V

    goto :goto_0

    :cond_3
    if-eqz v1, :cond_4

    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/s;->a:Lcom/google/android/gms/games/ui/common/matches/r;

    invoke-virtual {v1}, Lcom/google/android/gms/games/t;->a()I

    move-result v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/ui/common/matches/r;->a(Lcom/google/android/gms/games/ui/common/matches/r;I)I

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/s;->a:Lcom/google/android/gms/games/ui/common/matches/r;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/r;->c(Lcom/google/android/gms/games/ui/common/matches/r;)Lcom/google/android/gms/games/ui/common/matches/ac;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/s;->a:Lcom/google/android/gms/games/ui/common/matches/r;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/common/matches/r;->b(Lcom/google/android/gms/games/ui/common/matches/r;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/common/matches/ac;->f(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/s;->a:Lcom/google/android/gms/games/ui/common/matches/r;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/r;->e(Lcom/google/android/gms/games/ui/common/matches/r;)Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/s;->a:Lcom/google/android/gms/games/ui/common/matches/r;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/common/matches/r;->d(Lcom/google/android/gms/games/ui/common/matches/r;)Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/s;->a:Lcom/google/android/gms/games/ui/common/matches/r;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/common/matches/r;->b(Lcom/google/android/gms/games/ui/common/matches/r;)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->a(ZI)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_4
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/games/t;->w_()V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/google/android/gms/games/t;->w_()V

    :cond_5
    throw v0
.end method
