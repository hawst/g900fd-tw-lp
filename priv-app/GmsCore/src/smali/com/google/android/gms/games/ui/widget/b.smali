.class public final Lcom/google/android/gms/games/ui/widget/b;
.super Landroid/view/View;
.source "SourceFile"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:F

.field private c:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 37
    return-void
.end method


# virtual methods
.method final a(I)V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/b;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 106
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/b;->invalidate()V

    .line 107
    return-void
.end method

.method final a(JZ)V
    .locals 3

    .prologue
    .line 81
    if-eqz p3, :cond_0

    .line 82
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/b;->b:F

    .line 85
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 86
    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 87
    invoke-virtual {v0, p1, p2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 89
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/google/android/gms/games/ui/widget/c;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/games/ui/widget/c;-><init>(Lcom/google/android/gms/games/ui/widget/b;Landroid/animation/ValueAnimator;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 96
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/b;->setVisibility(I)V

    .line 97
    return-void

    .line 85
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method final a(Lcom/google/android/gms/games/PlayerLevelInfo;)V
    .locals 6

    .prologue
    .line 54
    invoke-virtual {p1}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v0

    .line 55
    if-eqz v0, :cond_0

    .line 56
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/widget/b;->a:Landroid/graphics/Paint;

    .line 57
    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/b;->a:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 58
    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/b;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/b;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/res/Resources;I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 60
    invoke-virtual {p1}, Lcom/google/android/gms/games/PlayerLevelInfo;->e()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v1

    .line 61
    if-eq v1, v0, :cond_1

    .line 62
    invoke-virtual {p1}, Lcom/google/android/gms/games/PlayerLevelInfo;->b()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevel;->c()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-float v1, v2

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevel;->d()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevel;->c()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-float v0, v2

    div-float v0, v1, v0

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/b;->b:F

    .line 70
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/b;->c:F

    .line 71
    return-void

    .line 66
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/b;->b:F

    goto :goto_0
.end method

.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/b;->c:F

    .line 118
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/b;->invalidate()V

    .line 119
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 111
    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    const/high16 v2, 0x42340000    # 45.0f

    iget v0, p0, Lcom/google/android/gms/games/ui/widget/b;->c:F

    iget v3, p0, Lcom/google/android/gms/games/ui/widget/b;->b:F

    mul-float/2addr v0, v3

    const/high16 v3, 0x43b40000    # 360.0f

    mul-float/2addr v0, v3

    float-to-int v0, v0

    int-to-float v3, v0

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/gms/games/ui/widget/b;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 113
    return-void
.end method
