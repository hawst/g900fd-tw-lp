.class public Lcom/google/android/gms/wallet/ia/CreateProfileActivity;
.super Lcom/google/android/gms/wallet/common/ui/dq;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/google/android/gms/wallet/common/a/m;
.implements Lcom/google/android/gms/wallet/common/a/q;
.implements Lcom/google/android/gms/wallet/common/ui/ah;
.implements Lcom/google/android/gms/wallet/common/ui/bf;
.implements Lcom/google/android/gms/wallet/common/ui/cr;
.implements Lcom/google/android/gms/wallet/common/ui/dp;


# static fields
.field private static final w:Ljava/lang/String;


# instance fields
.field private A:Z

.field private B:Lcom/google/android/gms/wallet/common/ui/dh;

.field private C:Ljava/util/ArrayList;

.field private D:Ljava/util/ArrayList;

.field private E:I

.field private F:Ljava/util/ArrayList;

.field private G:Landroid/util/Pair;

.field private H:I

.field private I:Lcom/google/android/gms/wallet/common/a/n;

.field private J:Lcom/google/android/gms/wallet/common/a/k;

.field private K:Z

.field private L:Z

.field private M:Z

.field private N:Lcom/google/checkout/inapp/proto/ag;

.field private O:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

.field private P:Lcom/google/checkout/inapp/proto/q;

.field private Q:Ljava/util/ArrayList;

.field private R:Ljava/lang/String;

.field private S:Ljava/lang/String;

.field private T:I

.field private final U:Lcom/google/android/gms/wallet/service/l;

.field protected a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field protected b:Landroid/accounts/Account;

.field c:I

.field d:Z

.field e:Landroid/view/View;

.field f:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

.field g:Landroid/widget/TextView;

.field h:Landroid/view/View;

.field i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

.field j:Lcom/google/android/gms/wallet/common/ui/an;

.field k:Landroid/widget/CheckBox;

.field l:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

.field m:Landroid/widget/CheckBox;

.field n:Landroid/widget/TextView;

.field o:Landroid/widget/TextView;

.field p:Landroid/widget/TextView;

.field q:Lcom/google/android/gms/wallet/common/ui/bb;

.field r:Lcom/google/android/gms/wallet/common/ui/bb;

.field s:Landroid/widget/CheckBox;

.field t:Landroid/widget/TextView;

.field u:Landroid/view/View;

.field v:Landroid/widget/FrameLayout;

.field private x:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private y:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    const-string v0, "createProfile"

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->w:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/dq;-><init>()V

    .line 145
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->K:Z

    .line 146
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->L:Z

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->M:Z

    .line 148
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->c:I

    .line 150
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->d:Z

    .line 157
    iput v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->T:I

    .line 962
    new-instance v0, Lcom/google/android/gms/wallet/ia/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ia/k;-><init>(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->U:Lcom/google/android/gms/wallet/service/l;

    return-void
.end method

.method private a(Lcom/google/android/gms/wallet/common/ui/bb;Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/bb;
    .locals 2

    .prologue
    .line 903
    if-eqz p1, :cond_0

    .line 904
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 908
    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    .line 910
    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 911
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 912
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;Lcom/google/checkout/inapp/proto/ag;)Lcom/google/checkout/inapp/proto/ag;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->N:Lcom/google/checkout/inapp/proto/ag;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->z:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;I)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->c(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Ljava/util/ArrayList;)V

    return-void
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 13

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/16 v12, 0x8

    const/4 v6, 0x0

    .line 568
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->C:Ljava/util/ArrayList;

    .line 569
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->D:Ljava/util/ArrayList;

    .line 571
    iget v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->E:I

    invoke-static {v0}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->a(I)Ljava/lang/String;

    move-result-object v5

    .line 572
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v8, v6

    move v4, v6

    :goto_0
    if-ge v8, v9, :cond_3

    .line 573
    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->a()Ljava/lang/String;

    move-result-object v10

    .line 577
    invoke-static {v10}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->b(Ljava/lang/String;)I

    move-result v0

    const/16 v7, 0x85

    if-eq v0, v7, :cond_f

    .line 578
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->Q:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 585
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v11

    move v7, v6

    .line 586
    :goto_1
    if-ge v7, v11, :cond_0

    .line 587
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->Q:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/identity/intents/model/CountrySpecification;

    invoke-virtual {v0}, Lcom/google/android/gms/identity/intents/model/CountrySpecification;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 589
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->D:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 596
    :cond_0
    :goto_2
    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    move v0, v1

    .line 572
    :goto_3
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move v4, v0

    goto :goto_0

    .line 586
    :cond_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    .line 594
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->D:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 601
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 602
    const-string v0, "CreateProfileActivity"

    const-string v1, "Integrator has either improperly configured BrokerAndRelationships or legal docs for country. No allowed country codes found. Exiting."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    invoke-virtual {p0, v2, v3}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(ILandroid/content/Intent;)V

    .line 684
    :goto_4
    return-void

    .line 608
    :cond_4
    if-nez v4, :cond_e

    .line 609
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->D:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    .line 611
    :goto_5
    invoke-static {v4}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->E:I

    .line 612
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->f:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/view/View$OnClickListener;)V

    .line 613
    iget v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->H:I

    if-eqz v0, :cond_a

    .line 614
    iget v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->H:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->c(I)V

    .line 619
    :goto_6
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v5, Lcom/google/android/gms/j;->lb:I

    invoke-virtual {v0, v5}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    .line 622
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-nez v0, :cond_5

    .line 623
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a()Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v5

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->L:Z

    if-nez v0, :cond_b

    move v0, v1

    :goto_7
    invoke-virtual {v5, v0}, Lcom/google/android/gms/wallet/common/ui/s;->a(Z)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->D:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/List;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->G:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/ArrayList;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    sget v4, Lcom/google/android/gms/p;->BP:I

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/s;->a(I)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->F:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/Collection;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    iget-boolean v4, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->K:Z

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/s;->b(Z)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/s;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    .line 632
    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;)Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    .line 633
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v4, Lcom/google/android/gms/j;->lb:I

    iget-object v5, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0, v4, v5}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 636
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/cr;)V

    .line 638
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->M:Z

    if-eqz v0, :cond_c

    .line 639
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->ku:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/an;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->j:Lcom/google/android/gms/wallet/common/ui/an;

    .line 641
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->j:Lcom/google/android/gms/wallet/common/ui/an;

    if-nez v0, :cond_6

    .line 642
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->b:Landroid/accounts/Account;

    iget-object v5, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->R:Ljava/lang/String;

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/wallet/common/ui/an;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;I[I[ILjava/lang/String;)Lcom/google/android/gms/wallet/common/ui/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->j:Lcom/google/android/gms/wallet/common/ui/an;

    .line 648
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->ku:I

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->j:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 651
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->j:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/an;->a(Lcom/google/android/gms/wallet/common/ui/ah;)V

    .line 653
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->k:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 654
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->bV:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    .line 657
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-eqz v0, :cond_7

    .line 658
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->k:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->d(Z)V

    .line 671
    :cond_7
    :goto_8
    iget v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->E:I

    if-eqz v0, :cond_8

    .line 672
    iget v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->E:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->b(I)V

    .line 675
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->e:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 676
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 677
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 678
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 681
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 683
    :cond_9
    invoke-virtual {p0, v6}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Z)V

    goto/16 :goto_4

    .line 616
    :cond_a
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l()V

    goto/16 :goto_6

    :cond_b
    move v0, v6

    .line 623
    goto/16 :goto_7

    .line 661
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 662
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->u:Landroid/view/View;

    invoke-virtual {v0, v12}, Landroid/view/View;->setVisibility(I)V

    .line 663
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->p:Landroid/widget/TextView;

    if-eqz v0, :cond_d

    .line 664
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 666
    :cond_d
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->d(Z)V

    .line 667
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->k:Landroid/widget/CheckBox;

    invoke-virtual {v0, v12}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 668
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m()V

    goto :goto_8

    :cond_e
    move-object v4, v5

    goto/16 :goto_5

    :cond_f
    move v0, v4

    goto/16 :goto_3
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l()V

    return-void
.end method

.method private b(Z)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 499
    const/4 v0, 0x0

    .line 500
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->k:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-nez v3, :cond_0

    .line 501
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    .line 503
    :cond_0
    const/4 v3, 0x3

    new-array v4, v3, [Lcom/google/android/gms/wallet/common/ui/dn;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    aput-object v3, v4, v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->j:Lcom/google/android/gms/wallet/common/ui/an;

    aput-object v3, v4, v1

    const/4 v3, 0x2

    aput-object v0, v4, v3

    .line 506
    array-length v5, v4

    move v3, v2

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_5

    aget-object v6, v4, v3

    .line 507
    if-eqz v6, :cond_1

    .line 508
    if-eqz p1, :cond_3

    .line 511
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->f()Z

    move-result v6

    if-eqz v6, :cond_2

    if-eqz v0, :cond_2

    move v0, v1

    .line 506
    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 511
    goto :goto_1

    .line 512
    :cond_3
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->e()Z

    move-result v6

    if-nez v6, :cond_1

    .line 517
    :cond_4
    :goto_2
    return v2

    .line 516
    :cond_5
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->c(Z)Z

    move-result v3

    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    move v2, v1

    goto :goto_2
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)Lcom/google/checkout/inapp/proto/ag;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->N:Lcom/google/checkout/inapp/proto/ag;

    return-object v0
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 928
    iput p1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->H:I

    .line 929
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 930
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 931
    return-void
.end method

.method private c(Z)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 521
    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 522
    if-eqz p1, :cond_0

    .line 523
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->o:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 524
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->o:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 541
    :cond_0
    :goto_0
    return v0

    .line 528
    :cond_1
    if-eqz p1, :cond_2

    .line 529
    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->O:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/w;->g(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 531
    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->o:Landroid/widget/TextView;

    sget v3, Lcom/google/android/gms/p;->CM:I

    new-array v0, v0, [Ljava/lang/Object;

    sget v4, Lcom/google/android/gms/p;->Cr:I

    invoke-virtual {p0, v4}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p0, v3, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 539
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    move v0, v1

    .line 541
    goto :goto_0

    .line 535
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->o:Landroid/widget/TextView;

    sget v3, Lcom/google/android/gms/p;->CM:I

    new-array v0, v0, [Ljava/lang/Object;

    sget v4, Lcom/google/android/gms/p;->CH:I

    invoke-virtual {p0, v4}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p0, v3, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->C:Ljava/util/ArrayList;

    return-object v0
.end method

.method private d(Z)V
    .locals 5

    .prologue
    const/16 v1, 0x8

    const/4 v0, 0x0

    .line 740
    if-eqz p1, :cond_1

    .line 741
    sget v0, Lcom/google/android/gms/j;->bW:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 742
    sget v0, Lcom/google/android/gms/j;->bU:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 743
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-eqz v0, :cond_0

    .line 744
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 757
    :cond_0
    :goto_0
    return-void

    .line 748
    :cond_1
    sget v1, Lcom/google/android/gms/j;->bW:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 749
    sget v1, Lcom/google/android/gms/j;->bU:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 750
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-eqz v1, :cond_2

    .line 751
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    goto :goto_0

    .line 754
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h()Lcom/google/t/a/b;

    move-result-object v1

    iget-object v1, v1, Lcom/google/t/a/b;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h()Lcom/google/t/a/b;

    move-result-object v2

    iget-object v2, v2, Lcom/google/t/a/b;->s:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a()Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->L:Z

    if-nez v4, :cond_3

    const/4 v0, 0x1

    :cond_3
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/s;->a(Z)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->D:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/List;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->G:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/ArrayList;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->F:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/Collection;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->K:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/s;->b(Z)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/s;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;)Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    new-instance v0, Lcom/google/t/a/b;

    invoke-direct {v0}, Lcom/google/t/a/b;-><init>()V

    iput-object v2, v0, Lcom/google/t/a/b;->s:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/t/a/b;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->bV:I

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    goto/16 :goto_0
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->r:Lcom/google/android/gms/wallet/common/ui/bb;

    const-string v1, "inapp.SignupActivity.CreateProfileNetworkErrorDialog"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Lcom/google/android/gms/wallet/common/ui/bb;Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->r:Lcom/google/android/gms/wallet/common/ui/bb;

    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->q:Lcom/google/android/gms/wallet/common/ui/bb;

    const-string v1, "inapp.SignupActivity.LegalDocsNetworkErrorDialog"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Lcom/google/android/gms/wallet/common/ui/bb;Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->q:Lcom/google/android/gms/wallet/common/ui/bb;

    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 385
    new-instance v0, Lcom/google/checkout/inapp/proto/af;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/af;-><init>()V

    .line 386
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->y:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    if-eqz v1, :cond_0

    .line 387
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->y:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/n;->a([Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;)[Lcom/google/checkout/inapp/proto/y;

    move-result-object v1

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/af;->b:[Lcom/google/checkout/inapp/proto/y;

    .line 392
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->k()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/checkout/inapp/proto/af;)V

    .line 394
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Z)V

    .line 395
    return-void

    .line 390
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->x:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/af;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method private i()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 425
    invoke-virtual {p0, v6}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Z)V

    .line 426
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h()Lcom/google/t/a/b;

    move-result-object v4

    .line 427
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a()Ljava/lang/String;

    move-result-object v2

    .line 428
    const/4 v0, 0x0

    .line 429
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->M:Z

    if-eqz v1, :cond_0

    .line 432
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->k:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_8

    move-object v1, v2

    move-object v3, v4

    .line 440
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->j:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/an;->a()Lcom/google/checkout/a/a/a/d;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/a/a/a/d;

    .line 442
    iget-object v5, v0, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iput-object v3, v5, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    .line 445
    iget-boolean v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->K:Z

    if-eqz v3, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 446
    iget-object v3, v0, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iput-object v1, v3, Lcom/google/checkout/a/a/a/b;->g:Ljava/lang/String;

    .line 450
    :cond_0
    new-instance v1, Lcom/google/checkout/inapp/proto/ab;

    invoke-direct {v1}, Lcom/google/checkout/inapp/proto/ab;-><init>()V

    .line 454
    new-instance v3, Lcom/google/checkout/a/a/b/b;

    invoke-direct {v3}, Lcom/google/checkout/a/a/b/b;-><init>()V

    .line 455
    iput-object v4, v3, Lcom/google/checkout/a/a/b/b;->a:Lcom/google/t/a/b;

    .line 456
    if-eqz v0, :cond_1

    .line 457
    iput-object v0, v3, Lcom/google/checkout/a/a/b/b;->c:Lcom/google/checkout/a/a/a/d;

    .line 459
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->K:Z

    if-eqz v0, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 460
    iput-object v2, v3, Lcom/google/checkout/a/a/b/b;->b:Ljava/lang/String;

    .line 463
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->s:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 464
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->s:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    iput-boolean v0, v3, Lcom/google/checkout/a/a/b/b;->d:Z

    .line 466
    :cond_3
    iput-object v3, v1, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    .line 467
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->z:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 468
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->z:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/checkout/inapp/proto/ab;->c:Ljava/lang/String;

    .line 470
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->y:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    if-eqz v0, :cond_9

    .line 471
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->y:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/n;->a([Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;)[Lcom/google/checkout/inapp/proto/y;

    move-result-object v0

    iput-object v0, v1, Lcom/google/checkout/inapp/proto/ab;->e:[Lcom/google/checkout/inapp/proto/y;

    .line 476
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->P:Lcom/google/checkout/inapp/proto/q;

    if-eqz v0, :cond_6

    .line 477
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->P:Lcom/google/checkout/inapp/proto/q;

    iput-object v0, v1, Lcom/google/checkout/inapp/proto/ab;->a:Lcom/google/checkout/inapp/proto/q;

    .line 479
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->O:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->c()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 480
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->O:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->c()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/checkout/inapp/proto/ab;->d:[Ljava/lang/String;

    .line 483
    :cond_7
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->k()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/checkout/inapp/proto/ab;)V

    .line 485
    iput-boolean v6, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Z

    .line 486
    return-void

    .line 436
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h()Lcom/google/t/a/b;

    move-result-object v1

    .line 437
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a()Ljava/lang/String;

    move-result-object v0

    move-object v3, v1

    move-object v1, v0

    goto/16 :goto_0

    .line 473
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->x:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 474
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->x:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/checkout/inapp/proto/ab;->f:Ljava/lang/String;

    goto :goto_1
.end method

.method private j()V
    .locals 2

    .prologue
    .line 792
    iget v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->c:I

    if-gez v0, :cond_0

    .line 793
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->k()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->U:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->c(Lcom/google/android/gms/wallet/service/l;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->c:I

    .line 798
    :cond_0
    return-void
.end method

.method private k()Lcom/google/android/gms/wallet/common/ui/dh;
    .locals 2

    .prologue
    .line 894
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->B:Lcom/google/android/gms/wallet/common/ui/dh;

    if-nez v0, :cond_0

    .line 895
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dh;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->B:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 898
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->B:Lcom/google/android/gms/wallet/common/ui/dh;

    return-object v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 934
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->H:I

    .line 935
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->g:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 936
    return-void
.end method

.method private m()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1090
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->f:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 1091
    sget v0, Lcom/google/android/gms/j;->fA:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1092
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->s:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 1093
    sget v0, Lcom/google/android/gms/j;->dN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 1094
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->M:Z

    if-eqz v1, :cond_0

    .line 1095
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->k:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 1097
    :cond_0
    if-eqz v0, :cond_1

    .line 1098
    invoke-static {p0, v0}, Lcom/google/android/gms/common/util/ae;->b(Landroid/content/Context;Landroid/view/View;)Z

    .line 1100
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/wallet/common/a/k;
    .locals 1

    .prologue
    .line 956
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->J:Lcom/google/android/gms/wallet/common/a/k;

    if-nez v0, :cond_0

    .line 957
    new-instance v0, Lcom/google/android/gms/wallet/common/a/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/a/k;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->J:Lcom/google/android/gms/wallet/common/a/k;

    .line 959
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->J:Lcom/google/android/gms/wallet/common/a/k;

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->j:Lcom/google/android/gms/wallet/common/ui/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->j:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/an;->getId()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 201
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m()V

    .line 202
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->T:I

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->f:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->requestFocus()Z

    .line 206
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 364
    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_2

    .line 365
    packed-switch p1, :pswitch_data_0

    .line 377
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected button "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 367
    :pswitch_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(ILandroid/content/Intent;)V

    .line 375
    :goto_0
    return-void

    .line 370
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->N:Lcom/google/checkout/inapp/proto/ag;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->C:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 371
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i()V

    goto :goto_0

    .line 373
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->h()V

    goto :goto_0

    .line 380
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected errorCode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 365
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(ILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 940
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/dq;->a(ILandroid/content/Intent;)V

    .line 942
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/ui/dr;->a(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->R:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryActivityClosedEvent;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    .line 944
    return-void
.end method

.method final a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 867
    if-eqz p1, :cond_4

    .line 868
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 872
    :goto_0
    if-eqz p1, :cond_0

    .line 873
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->f:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 875
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-eqz v0, :cond_1

    .line 876
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-nez p1, :cond_5

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Z)V

    .line 878
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->j:Lcom/google/android/gms/wallet/common/ui/an;

    if-eqz v0, :cond_2

    .line 879
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->j:Lcom/google/android/gms/wallet/common/ui/an;

    if-nez p1, :cond_6

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/an;->a(Z)V

    .line 881
    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->k:Landroid/widget/CheckBox;

    if-nez p1, :cond_7

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 882
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-eqz v0, :cond_3

    .line 883
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-nez p1, :cond_8

    :goto_4
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Z)V

    .line 885
    :cond_3
    return-void

    .line 870
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->h:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_5
    move v0, v2

    .line 876
    goto :goto_1

    :cond_6
    move v0, v2

    .line 879
    goto :goto_2

    :cond_7
    move v0, v2

    .line 881
    goto :goto_3

    :cond_8
    move v1, v2

    .line 883
    goto :goto_4
.end method

.method public final b()Lcom/google/android/gms/wallet/common/a/n;
    .locals 1

    .prologue
    .line 948
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->I:Lcom/google/android/gms/wallet/common/a/n;

    if-nez v0, :cond_0

    .line 949
    new-instance v0, Lcom/google/android/gms/wallet/common/a/n;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/a/n;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->I:Lcom/google/android/gms/wallet/common/a/n;

    .line 951
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->I:Lcom/google/android/gms/wallet/common/a/n;

    return-object v0
.end method

.method public final b(I)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 705
    iput p1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->E:I

    .line 706
    invoke-static {p1}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 708
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    .line 709
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 710
    sget v1, Lcom/google/android/gms/p;->Cs:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v3, Lcom/google/android/gms/p;->Cr:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/gms/wallet/common/w;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 714
    invoke-static {v2}, Lcom/google/android/gms/wallet/common/w;->g(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 715
    sget v3, Lcom/google/android/gms/p;->Cq:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 723
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m:Landroid/widget/CheckBox;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 724
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->n:Landroid/widget/TextView;

    invoke-static {v3, v1}, Lcom/google/android/gms/wallet/common/ui/ClickSpan;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 725
    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->O:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    .line 726
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/w;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->S:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 730
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 732
    :cond_1
    return-void

    .line 718
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->b()Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/gms/p;->CH:I

    invoke-virtual {p0, v4}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/wallet/common/w;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 720
    sget v4, Lcom/google/android/gms/p;->CL:I

    invoke-virtual {p0, v4}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v6

    aput-object v1, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 736
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No legal documents for selected country"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 490
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 495
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 547
    const/4 v0, 0x0

    .line 548
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->k:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-nez v3, :cond_0

    .line 549
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    .line 551
    :cond_0
    const/4 v3, 0x3

    new-array v3, v3, [Lcom/google/android/gms/wallet/common/ui/dp;

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    aput-object v4, v3, v2

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->j:Lcom/google/android/gms/wallet/common/ui/an;

    aput-object v4, v3, v1

    const/4 v4, 0x2

    aput-object v0, v3, v4

    .line 554
    array-length v4, v3

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    .line 555
    if-eqz v5, :cond_1

    invoke-interface {v5}, Lcom/google/android/gms/wallet/common/ui/dp;->g()Z

    move-result v5

    if-eqz v5, :cond_1

    move v0, v1

    .line 564
    :goto_1
    return v0

    .line 554
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 559
    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->c(Z)Z

    move-result v0

    if-nez v0, :cond_3

    .line 560
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->o:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearFocus()V

    .line 561
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->o:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    move v0, v1

    .line 562
    goto :goto_1

    :cond_3
    move v0, v2

    .line 564
    goto :goto_1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 343
    const/high16 v0, -0x10000

    and-int/2addr v0, p1

    if-eqz v0, :cond_1

    .line 344
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/dq;->onActivityResult(IILandroid/content/Intent;)V

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 346
    :cond_1
    const/16 v0, 0x1388

    if-ne p1, v0, :cond_2

    .line 347
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i()V

    goto :goto_0

    .line 353
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->ku:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 355
    if-eqz v0, :cond_0

    .line 356
    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 347
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .prologue
    .line 689
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->O:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    if-eqz v0, :cond_1

    .line 690
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->O:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->S:Ljava/lang/String;

    .line 691
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->b(Z)Z

    .line 695
    :cond_0
    :goto_0
    return-void

    .line 692
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->k:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_0

    .line 693
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->d(Z)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 700
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->O:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->O:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1388

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 701
    :goto_0
    return-void

    .line 700
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->g()Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 210
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dq;->onCreate(Landroid/os/Bundle;)V

    .line 212
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dg;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/dg;

    .line 213
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ck;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/ck;

    .line 215
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 217
    const-string v0, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 218
    const-string v0, "com.google.android.gms.wallet.account"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->b:Landroid/accounts/Account;

    .line 219
    const-string v0, "com.google.android.gms.wallet.brokerId"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->x:Ljava/lang/String;

    .line 220
    const-string v0, "com.google.android.gms.wallet.brokerAndRelationships"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    .line 222
    if-eqz v0, :cond_0

    array-length v4, v0

    if-lez v4, :cond_0

    .line 223
    array-length v4, v0

    const-class v5, [Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    invoke-static {v0, v4, v5}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->y:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    .line 226
    :cond_0
    const-string v0, "com.google.android.gms.wallet.unadjustedCartId"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->z:Ljava/lang/String;

    .line 228
    const-string v0, "com.google.android.gms.wallet.addressHints"

    const-class v4, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->F:Ljava/util/ArrayList;

    .line 230
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->F:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/a/e;->a(Ljava/util/Collection;)Landroid/util/Pair;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->G:Landroid/util/Pair;

    .line 231
    const-string v0, "legalDocsForCountries"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->C:Ljava/util/ArrayList;

    .line 233
    const-string v0, "com.google.android.gms.wallet.countrySpecificationsFilter"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->Q:Ljava/util/ArrayList;

    .line 235
    const-string v0, "com.google.android.gms.wallet.defaultCountryCode"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 237
    invoke-static {v0}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->E:I

    .line 239
    const-string v0, "com.google.android.gms.wallet.requiresInstrument"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->M:Z

    .line 241
    const-string v0, "com.google.android.gms.wallet.requiresCreditCardFullAddress"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->L:Z

    .line 243
    const-string v0, "com.google.android.gms.wallet.phoneNumberRequired"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->K:Z

    .line 245
    const-string v0, "com.google.android.gms.wallet.accountReference"

    const-class v4, Lcom/google/checkout/inapp/proto/q;

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/q;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->P:Lcom/google/checkout/inapp/proto/q;

    .line 247
    const-string v0, "com.google.android.gms.wallet.infoText"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->C:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->x:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->y:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    if-eqz v0, :cond_7

    :cond_1
    move v0, v2

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 253
    sget v0, Lcom/google/android/gms/l;->gb:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->setContentView(I)V

    .line 255
    sget v0, Lcom/google/android/gms/j;->dM:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->e:Landroid/view/View;

    .line 256
    sget v0, Lcom/google/android/gms/j;->rk:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->s:Landroid/widget/CheckBox;

    .line 257
    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->s:Landroid/widget/CheckBox;

    sget-object v0, Lcom/google/android/gms/wallet/b/a;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 258
    sget v0, Lcom/google/android/gms/j;->fB:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->n:Landroid/widget/TextView;

    .line 259
    sget v0, Lcom/google/android/gms/j;->fy:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m:Landroid/widget/CheckBox;

    .line 260
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 261
    sget v0, Lcom/google/android/gms/j;->fz:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->o:Landroid/widget/TextView;

    .line 262
    sget v0, Lcom/google/android/gms/j;->pN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->h:Landroid/view/View;

    .line 263
    sget v0, Lcom/google/android/gms/j;->cj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->f:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    .line 264
    sget v0, Lcom/google/android/gms/j;->bZ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->k:Landroid/widget/CheckBox;

    .line 266
    sget v0, Lcom/google/android/gms/j;->ch:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->g:Landroid/widget/TextView;

    .line 267
    sget v0, Lcom/google/android/gms/j;->nM:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->t:Landroid/widget/TextView;

    .line 268
    sget v0, Lcom/google/android/gms/j;->nL:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->u:Landroid/view/View;

    .line 269
    sget v0, Lcom/google/android/gms/j;->ku:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->v:Landroid/widget/FrameLayout;

    .line 271
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 272
    sget v0, Lcom/google/android/gms/j;->lc:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/gms/p;->Ce:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 274
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->t:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/gms/p;->Cm:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 276
    sget v0, Lcom/google/android/gms/j;->bW:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/gms/p;->zZ:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->P:Lcom/google/checkout/inapp/proto/q;

    if-eqz v0, :cond_3

    .line 281
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->f:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    sget v4, Lcom/google/android/gms/p;->zq:I

    invoke-virtual {p0, v4}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Ljava/lang/CharSequence;)V

    .line 283
    :cond_3
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 284
    sget v0, Lcom/google/android/gms/j;->kq:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->p:Landroid/widget/TextView;

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 289
    :cond_4
    if-eqz p1, :cond_8

    .line 290
    const-string v0, "errorMessageResourceId"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->H:I

    .line 291
    const-string v0, "pendingRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Z

    .line 293
    const-string v0, "regionCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->E:I

    .line 294
    const-string v0, "serviceConnectionSavePoint"

    const/4 v3, -0x1

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->c:I

    .line 296
    const-string v0, "legalDocumentsResponse"

    const-class v3, Lcom/google/checkout/inapp/proto/ag;

    invoke-static {p1, v0, v3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/ag;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->N:Lcom/google/checkout/inapp/proto/ag;

    .line 298
    const-string v0, "analyticsSessionId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->R:Ljava/lang/String;

    .line 299
    const-string v0, "tosCheckboxCheckedForCountry"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->S:Ljava/lang/String;

    .line 301
    const-string v0, "viewState"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->T:I

    .line 312
    :goto_1
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    .line 313
    if-eqz v0, :cond_9

    .line 314
    invoke-virtual {v0}, Landroid/support/v7/app/a;->a()V

    .line 316
    sget v1, Lcom/google/android/gms/p;->CD:I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->c(I)V

    .line 325
    :goto_2
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->k()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    if-nez v0, :cond_5

    .line 326
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->b:Landroid/accounts/Account;

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/wallet/common/ui/dh;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->B:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 328
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->B:Lcom/google/android/gms/wallet/common/ui/dh;

    sget-object v3, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->w:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 333
    :cond_5
    sget v0, Lcom/google/android/gms/j;->ua:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;)V

    .line 334
    iget v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->T:I

    if-ne v0, v2, :cond_6

    .line 335
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m()V

    .line 337
    :cond_6
    return-void

    :cond_7
    move v0, v1

    .line 249
    goto/16 :goto_0

    .line 303
    :cond_8
    const-string v0, "signup"

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->b:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v0, v3, v4}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->R:Ljava/lang/String;

    .line 308
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v3

    const-string v4, "create_profile"

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 318
    :cond_9
    sget v0, Lcom/google/android/gms/j;->tc:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    .line 319
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setVisibility(I)V

    .line 320
    sget v1, Lcom/google/android/gms/p;->CD:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(I)V

    goto :goto_2
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 838
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/dq;->onPause()V

    .line 839
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->d:Z

    .line 840
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->j()V

    .line 841
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 802
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/dq;->onResume()V

    .line 803
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->d:Z

    .line 804
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->k()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->U:Lcom/google/android/gms/wallet/service/l;

    iget v2, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->c:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/service/k;->b(Lcom/google/android/gms/wallet/service/l;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->c:I

    .line 805
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "inapp.SignupActivity.LegalDocsNetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->q:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 808
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "inapp.SignupActivity.CreateProfileNetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->r:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 812
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->q:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_2

    .line 813
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->q:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 830
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->r:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_1

    .line 831
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->r:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 833
    :cond_1
    iput-boolean v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->d:Z

    .line 834
    return-void

    .line 815
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->N:Lcom/google/checkout/inapp/proto/ag;

    if-eqz v0, :cond_3

    .line 817
    iput-boolean v3, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->d:Z

    .line 818
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->U:Lcom/google/android/gms/wallet/service/l;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->N:Lcom/google/checkout/inapp/proto/ag;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/inapp/proto/ag;)V

    .line 825
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Z

    if-eqz v0, :cond_0

    .line 826
    invoke-virtual {p0, v3}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Z)V

    goto :goto_0

    .line 819
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->C:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 821
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->C:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Ljava/util/ArrayList;)V

    goto :goto_1

    .line 823
    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->h()V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 845
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dq;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 847
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->j()V

    .line 849
    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 850
    const-string v0, "pendingRequest"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 851
    const-string v0, "regionCode"

    iget v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->E:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 852
    const-string v0, "errorMessageResourceId"

    iget v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->H:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 853
    const-string v0, "analyticsSessionId"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->R:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    const-string v0, "viewState"

    iget v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->T:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 855
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->O:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    if-eqz v0, :cond_0

    .line 856
    const-string v0, "tosCheckboxCheckedForCountry"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->O:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->N:Lcom/google/checkout/inapp/proto/ag;

    if-eqz v0, :cond_1

    .line 860
    const-string v0, "legalDocumentsResponse"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->N:Lcom/google/checkout/inapp/proto/ag;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 863
    :cond_1
    return-void
.end method
