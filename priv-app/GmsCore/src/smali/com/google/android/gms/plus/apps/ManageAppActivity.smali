.class public Lcom/google/android/gms/plus/apps/ManageAppActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/common/es;
.implements Lcom/google/android/gms/common/et;
.implements Lcom/google/android/gms/plus/apps/ae;
.implements Lcom/google/android/gms/plus/apps/ak;
.implements Lcom/google/android/gms/plus/apps/am;
.implements Lcom/google/android/gms/plus/apps/au;
.implements Lcom/google/android/gms/plus/internal/ae;
.implements Lcom/google/android/gms/plus/internal/af;
.implements Lcom/google/android/gms/plus/internal/ag;
.implements Lcom/google/android/gms/plus/internal/ah;


# static fields
.field private static final a:Ljava/util/ArrayList;


# instance fields
.field private b:Landroid/accounts/Account;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

.field private h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Landroid/view/View;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/view/View;

.field private r:Landroid/widget/TextView;

.field private s:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

.field private t:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

.field private u:Z

.field private v:Z

.field private w:Lcom/google/android/gms/plus/apps/al;

.field private final x:Lcom/google/android/gms/plus/internal/ad;

.field private y:Lcom/google/android/gms/plus/internal/ab;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 165
    sget-object v0, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;-><init>(Lcom/google/android/gms/plus/internal/ad;)V

    .line 166
    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/plus/internal/ad;)V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 170
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->x:Lcom/google/android/gms/plus/internal/ad;

    .line 171
    return-void
.end method

.method private static a(Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;Lcom/google/android/gms/common/people/data/Audience;Ljava/util/ArrayList;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;
    .locals 2

    .prologue
    .line 653
    new-instance v1, Lcom/google/android/gms/plus/internal/model/apps/a;

    invoke-direct {v1}, Lcom/google/android/gms/plus/internal/model/apps/a;-><init>()V

    .line 654
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object p1

    :cond_0
    iput-object p1, v1, Lcom/google/android/gms/plus/internal/model/apps/a;->b:Lcom/google/android/gms/common/people/data/Audience;

    .line 655
    if-nez p2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f()Ljava/util/ArrayList;

    move-result-object p2

    :cond_1
    iput-object p2, v1, Lcom/google/android/gms/plus/internal/model/apps/a;->c:Ljava/util/ArrayList;

    .line 656
    if-nez p3, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->h()Z

    move-result v0

    :goto_0
    iput-boolean v0, v1, Lcom/google/android/gms/plus/internal/model/apps/a;->d:Z

    .line 658
    if-nez p4, :cond_5

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->i()Z

    move-result v0

    :goto_1
    iput-boolean v0, v1, Lcom/google/android/gms/plus/internal/model/apps/a;->e:Z

    .line 660
    if-nez p5, :cond_6

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->j()Z

    move-result v0

    :goto_2
    iput-boolean v0, v1, Lcom/google/android/gms/plus/internal/model/apps/a;->f:Z

    .line 662
    if-nez p6, :cond_7

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->k()Z

    move-result v0

    :goto_3
    iput-boolean v0, v1, Lcom/google/android/gms/plus/internal/model/apps/a;->g:Z

    .line 664
    if-nez p7, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->m()Ljava/lang/String;

    move-result-object p7

    :cond_2
    iput-object p7, v1, Lcom/google/android/gms/plus/internal/model/apps/a;->i:Ljava/lang/String;

    .line 666
    if-nez p8, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->n()Ljava/lang/String;

    move-result-object p8

    :cond_3
    iput-object p8, v1, Lcom/google/android/gms/plus/internal/model/apps/a;->j:Ljava/lang/String;

    .line 668
    if-nez p9, :cond_8

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->l()I

    move-result v0

    :goto_4
    iput v0, v1, Lcom/google/android/gms/plus/internal/model/apps/a;->h:I

    .line 670
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/plus/internal/model/apps/a;->a:Ljava/lang/String;

    .line 671
    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/a;->a()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v0

    return-object v0

    .line 656
    :cond_4
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 658
    :cond_5
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_1

    .line 660
    :cond_6
    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_2

    .line 662
    :cond_7
    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_3

    .line 668
    :cond_8
    invoke-virtual {p9}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_4
.end method

.method private a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 869
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V
    .locals 2

    .prologue
    .line 965
    new-instance v0, Lcom/google/android/gms/common/server/y;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/server/y;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/y;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/server/y;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/common/analytics/d;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/y;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    .line 971
    if-eqz p2, :cond_0

    .line 972
    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)Lcom/google/android/gms/common/server/y;

    .line 974
    :cond_0
    invoke-static {p0, v0}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/y;)V

    .line 975
    return-void

    .line 965
    :cond_1
    sget-object v0, Lcom/google/android/gms/common/analytics/d;->j:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V
    .locals 12

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v10, 0x1

    .line 777
    invoke-virtual {p1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->e()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 778
    iget-object v5, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->p:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    sget v0, Lcom/google/android/gms/p;->uh:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 782
    :goto_1
    return-void

    .line 778
    :cond_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v10, :cond_4

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_3
    sget v0, Lcom/google/android/gms/p;->uf:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    move v4, v0

    move-object v2, v1

    move-object v3, v1

    :goto_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_7

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v8

    if-ne v8, v10, :cond_5

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v8

    if-ne v8, v10, :cond_5

    move-object v11, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v11

    :goto_3
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_2

    :cond_5
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v8

    if-ne v8, v10, :cond_6

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v8

    const/4 v9, 0x4

    if-ne v8, v9, :cond_6

    move-object v2, v3

    move-object v11, v0

    move-object v0, v1

    move-object v1, v11

    goto :goto_3

    :cond_6
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v8

    if-ne v8, v10, :cond_b

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_b

    move-object v1, v2

    move-object v2, v3

    goto :goto_3

    :cond_7
    if-eqz v3, :cond_8

    invoke-virtual {v3}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->uj:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_8
    if-eqz v2, :cond_9

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->ud:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v6}, Lcom/google/android/gms/common/people/data/Audience;->b()I

    move-result v0

    if-ne v0, v10, :cond_3

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->uc:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 780
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->p:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_b
    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_3
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 675
    invoke-static {p1}, Lcom/google/android/gms/plus/f/e;->a(Ljava/lang/CharSequence;)Lcom/google/android/gms/plus/f/e;

    move-result-object v0

    .line 676
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    .line 677
    const-string v2, "progress_dialog"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 678
    invoke-virtual {v1}, Landroid/support/v4/app/aj;->b()I

    .line 679
    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 891
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->o:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 892
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->q:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 893
    return-void
.end method

.method private b(Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V
    .locals 2

    .prologue
    .line 785
    invoke-virtual {p1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 786
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->r:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 790
    :goto_0
    return-void

    .line 788
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->r:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 897
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->q:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 898
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 413
    sget v0, Lcom/google/android/gms/j;->lm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 414
    sget v0, Lcom/google/android/gms/j;->dF:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 415
    return-void
.end method

.method private f()Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;
    .locals 13

    .prologue
    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 599
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.APP_IS_ASPEN"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 600
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.APP_HAS_CONN_READ"

    invoke-virtual {v0, v1, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    .line 601
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.APP_IS_FITNESS"

    invoke-virtual {v0, v1, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    .line 602
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.APP_PACKAGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 603
    if-eqz v0, :cond_0

    .line 604
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 606
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    .line 607
    if-eqz v4, :cond_0

    .line 608
    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 609
    new-instance v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->c:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->e:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->f:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZLjava/lang/String;Ljava/lang/String;ZZB)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 625
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    .line 617
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.APP_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 618
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 619
    const-string v0, "AppSettings"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 620
    const-string v0, "AppSettings"

    const-string v1, "Missing required EXTRA_APP_NAME"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    move-object v0, v11

    .line 622
    goto :goto_0

    .line 624
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "com.google.android.gms.plus.APP_ICON_URL"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 625
    new-instance v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->c:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->e:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->f:Ljava/lang/String;

    move-object v4, v11

    move v10, v12

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZLjava/lang/String;Ljava/lang/String;ZZB)V

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 682
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "progress_dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/f/e;

    .line 684
    if-eqz v0, :cond_0

    .line 685
    invoke-virtual {v0}, Lcom/google/android/gms/plus/f/e;->a()V

    .line 687
    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 708
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->i()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 710
    :cond_1
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    sget v1, Lcom/google/android/gms/h;->cs:I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(I)V

    .line 713
    sget v0, Lcom/google/android/gms/j;->p:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 716
    sget v0, Lcom/google/android/gms/j;->nl:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->o:Landroid/view/View;

    .line 717
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->o:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 718
    sget v0, Lcom/google/android/gms/j;->nk:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->p:Landroid/widget/TextView;

    .line 721
    sget v0, Lcom/google/android/gms/j;->fV:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->q:Landroid/view/View;

    .line 722
    sget v0, Lcom/google/android/gms/j;->fU:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->r:Landroid/widget/TextView;

    .line 724
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->i()V

    .line 727
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->j()V

    .line 728
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->k()V

    .line 729
    return-void
.end method

.method private i()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 732
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    if-nez v0, :cond_1

    .line 733
    iput-boolean v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->m:Z

    .line 774
    :cond_0
    :goto_0
    return-void

    .line 737
    :cond_1
    sget v0, Lcom/google/android/gms/j;->n:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 738
    sget v0, Lcom/google/android/gms/j;->p:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 739
    sget v0, Lcom/google/android/gms/j;->jo:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 740
    sget v0, Lcom/google/android/gms/j;->o:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 741
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 742
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->c()Ljava/lang/String;

    move-result-object v1

    .line 743
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 744
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 752
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 753
    sget v0, Lcom/google/android/gms/j;->ni:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 754
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->o:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 755
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V

    .line 758
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 759
    sget v0, Lcom/google/android/gms/j;->fT:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 760
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->q:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 761
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->q:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 762
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b(Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V

    .line 768
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 769
    sget v0, Lcom/google/android/gms/j;->ni:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 770
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->o:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 771
    sget v0, Lcom/google/android/gms/j;->n:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 772
    sget v0, Lcom/google/android/gms/j;->p:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 749
    :cond_4
    sget v1, Lcom/google/android/gms/p;->ug:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method

.method private j()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 793
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 794
    sget v0, Lcom/google/android/gms/j;->qH:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 795
    sget v0, Lcom/google/android/gms/j;->qI:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 796
    sget v0, Lcom/google/android/gms/j;->qG:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 798
    :cond_0
    return-void
.end method

.method private k()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 801
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 803
    sget v0, Lcom/google/android/gms/j;->eq:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 804
    sget v0, Lcom/google/android/gms/j;->es:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 805
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 806
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 807
    sget v0, Lcom/google/android/gms/j;->er:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->ub:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 813
    :goto_0
    return-void

    .line 810
    :cond_0
    sget v0, Lcom/google/android/gms/j;->eq:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 811
    sget v0, Lcom/google/android/gms/j;->es:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private l()V
    .locals 1

    .prologue
    .line 978
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->setResult(I)V

    .line 979
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->finish()V

    .line 980
    return-void
.end method


# virtual methods
.method public final T_()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 338
    invoke-static {}, Lcom/google/android/gms/plus/apps/af;->a()Lcom/google/android/gms/plus/apps/af;

    const/4 v0, 0x3

    invoke-static {p0, v0}, Lcom/google/android/gms/plus/apps/af;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 339
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->i()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->m()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 342
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->l:Z

    if-nez v0, :cond_4

    .line 343
    iput-boolean v9, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->l:Z

    .line 344
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->m:Z

    .line 345
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Lcom/google/android/gms/plus/internal/ab;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-interface {v0, p0, v1, v2}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/af;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;)V

    .line 364
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->f:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->n:Z

    if-nez v0, :cond_3

    .line 365
    iput-boolean v9, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->n:Z

    .line 367
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Lcom/google/android/gms/plus/internal/ab;

    sget-object v0, Lcom/google/android/gms/plus/c/a;->G:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->z:Ljava/lang/String;

    invoke-interface {v1, p0, v0, v2}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/ah;ILjava/lang/String;)V

    .line 371
    :cond_3
    return-void

    .line 346
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    if-eqz v0, :cond_5

    .line 347
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Lcom/google/android/gms/plus/internal/ab;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v2

    invoke-interface {v0, p0, v1, v2}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/ae;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V

    goto :goto_0

    .line 348
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    if-eqz v0, :cond_2

    .line 349
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Lcom/google/android/gms/plus/internal/ab;

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f()Ljava/util/ArrayList;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->h()Z

    move-result v5

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->i()Z

    move-result v6

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->j()Z

    move-result v7

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->k()Z

    move-result v8

    move-object v1, p0

    invoke-interface/range {v0 .. v8}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/ag;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;Ljava/util/List;ZZZZ)V

    goto :goto_0

    .line 362
    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->e()V

    goto :goto_0
.end method

.method public final W_()V
    .locals 0

    .prologue
    .line 375
    return-void
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 640
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->finish()V

    .line 641
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 0

    .prologue
    .line 379
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->l()V

    .line 380
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V
    .locals 3

    .prologue
    .line 419
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->l:Z

    .line 420
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->m:Z

    .line 421
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 422
    iput-object p2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    .line 425
    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->j()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Z

    .line 426
    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->k()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Z

    .line 427
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->i()V

    .line 433
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->e()V

    .line 434
    return-void

    .line 428
    :cond_1
    const-string v0, "AppSettings"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 429
    const-string v0, "AppSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to load application ACLs: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/model/a/b;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 386
    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->n:Z

    .line 387
    if-eqz p2, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 388
    invoke-virtual {p2}, Lcom/google/android/gms/plus/model/a/b;->c()I

    move-result v1

    .line 389
    :goto_0
    if-ge v0, v1, :cond_0

    .line 390
    invoke-virtual {p2, v0}, Lcom/google/android/gms/plus/model/a/b;->b(I)Lcom/google/android/gms/plus/model/a/a;

    move-result-object v2

    .line 391
    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->c:Ljava/lang/String;

    invoke-interface {v2}, Lcom/google/android/gms/plus/model/a/a;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 392
    invoke-interface {v2}, Lcom/google/android/gms/plus/model/a/a;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->e:Ljava/lang/String;

    .line 393
    invoke-interface {v2}, Lcom/google/android/gms/plus/model/a/a;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->f:Ljava/lang/String;

    .line 394
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->f()Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    .line 395
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->j()V

    .line 396
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->k()V

    .line 400
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    if-eqz p3, :cond_1

    .line 402
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->n:Z

    .line 403
    iput-object p3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->z:Ljava/lang/String;

    .line 404
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Lcom/google/android/gms/plus/internal/ab;

    sget-object v0, Lcom/google/android/gms/plus/c/a;->G:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, p0, v0, p3}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/ah;ILjava/lang/String;)V

    .line 407
    :cond_1
    return-void

    .line 389
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/model/a/a;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 631
    if-eqz p2, :cond_0

    .line 632
    sget v0, Lcom/google/android/gms/j;->aG:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 633
    invoke-static {p0}, Lcom/google/android/gms/plus/apps/a;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/apps/a;->a(Lcom/google/android/gms/plus/model/a/a;Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/plus/apps/b;

    .line 636
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/model/a/a;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 554
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->w:Lcom/google/android/gms/plus/apps/al;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/apps/al;->a(Lcom/google/android/gms/plus/model/a/a;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 555
    sget v0, Lcom/google/android/gms/p;->tw:I

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/google/android/gms/plus/apps/a;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/a;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/gms/plus/apps/a;->a(Lcom/google/android/gms/plus/model/a/a;)Lcom/google/android/gms/plus/apps/b;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/plus/apps/b;->a:Ljava/lang/CharSequence;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Ljava/lang/String;)V

    .line 563
    :goto_0
    return-void

    .line 558
    :cond_0
    sget v0, Lcom/google/android/gms/p;->tu:I

    new-array v1, v1, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/google/android/gms/plus/model/a/a;->d()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/f/d;->b(Ljava/lang/String;)Lcom/google/android/gms/plus/f/d;

    move-result-object v0

    .line 561
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "error_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/f/d;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/model/a/a;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 567
    iget-boolean v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->k:Z

    if-nez v2, :cond_0

    .line 590
    :goto_0
    return v0

    .line 570
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g()V

    .line 572
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v2

    if-nez v2, :cond_2

    .line 573
    :cond_1
    sget v2, Lcom/google/android/gms/p;->tu:I

    new-array v3, v1, [Ljava/lang/Object;

    invoke-interface {p2}, Lcom/google/android/gms/plus/model/a/a;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/plus/f/d;->b(Ljava/lang/String;)Lcom/google/android/gms/plus/f/d;

    move-result-object v2

    .line 577
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v3

    .line 578
    const-string v4, "error_dialog"

    invoke-virtual {v3, v2, v4}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 579
    invoke-virtual {v3}, Landroid/support/v4/app/aj;->a()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move v0, v1

    .line 590
    goto :goto_0

    .line 584
    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 586
    const-string v2, "com.google.android.gms.plus.DISCONNECTED_APP_ID"

    invoke-interface {p2}, Lcom/google/android/gms/plus/model/a/a;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 587
    const/4 v2, -0x1

    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->setResult(ILandroid/content/Intent;)V

    .line 588
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->finish()V

    goto :goto_1

    .line 582
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/c;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 438
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g()V

    .line 440
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 441
    :cond_0
    sget v0, Lcom/google/android/gms/p;->tZ:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 442
    iput-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    .line 443
    iput-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    .line 463
    :cond_1
    :goto_0
    return-void

    .line 444
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    if-eqz v0, :cond_1

    .line 447
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    move-object v9, v2

    invoke-static/range {v0 .. v9}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;Lcom/google/android/gms/common/people/data/Audience;Ljava/util/ArrayList;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    .line 452
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V

    .line 453
    const-string v0, "AppSettings"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 454
    const-string v0, "AppSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "mAppACLs: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    :cond_3
    sget-object v0, Lcom/google/android/gms/common/analytics/c;->p:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/analytics/e;->a(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    .line 461
    iput-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 467
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g()V

    .line 469
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 470
    :cond_0
    sget v0, Lcom/google/android/gms/p;->tZ:I

    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 488
    :goto_0
    iput-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    .line 489
    return-void

    .line 472
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->h()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->i()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->j()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->k()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->m()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->n()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->l()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static/range {v0 .. v9}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;Lcom/google/android/gms/common/people/data/Audience;Ljava/util/ArrayList;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    .line 477
    const-string v0, "AppSettings"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 478
    const-string v0, "AppSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mAppAcls: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b(Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V

    .line 481
    sget-object v0, Lcom/google/android/gms/common/analytics/c;->o:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->h()Z

    move-result v3

    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->i()Z

    move-result v4

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/analytics/e;->a(Ljava/util/List;ZZ)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    goto :goto_0
.end method

.method public final i_()Z
    .locals 1

    .prologue
    .line 547
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->finish()V

    .line 548
    const/4 v0, 0x1

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v1, -0x1

    const/4 v0, 0x1

    .line 493
    if-nez p1, :cond_3

    .line 494
    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Z)V

    .line 495
    if-ne p2, v1, :cond_1

    .line 496
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 497
    :goto_0
    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/people/data/a;

    invoke-direct {v0}, Lcom/google/android/gms/common/people/data/a;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/a;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    :cond_0
    invoke-static {p3}, Lcom/google/android/gms/common/audience/a/d;->c(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/common/people/data/a;

    invoke-direct {v2, v0}, Lcom/google/android/gms/common/people/data/a;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/people/data/a;->a(Ljava/util/Collection;)Lcom/google/android/gms/common/people/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/a;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    .line 498
    sget v1, Lcom/google/android/gms/p;->uk:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Ljava/lang/String;)V

    .line 500
    new-instance v1, Lcom/google/android/gms/plus/internal/model/apps/a;

    invoke-direct {v1}, Lcom/google/android/gms/plus/internal/model/apps/a;-><init>()V

    iput-object v0, v1, Lcom/google/android/gms/plus/internal/model/apps/a;->b:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/a;->a()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    .line 501
    const-string v0, "AppSettings"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 502
    const-string v0, "AppSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mPaclToWrite: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    :cond_1
    :goto_1
    return-void

    .line 496
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    goto :goto_0

    .line 505
    :cond_3
    if-ne p1, v0, :cond_1

    .line 506
    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b(Z)V

    .line 508
    if-ne p2, v1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Z

    if-eqz v0, :cond_1

    .line 509
    :cond_4
    sget v0, Lcom/google/android/gms/p;->uk:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Ljava/lang/String;)V

    .line 510
    invoke-static {p3}, Lcom/google/android/gms/common/audience/a/o;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/audience/a/q;

    move-result-object v0

    .line 511
    new-instance v1, Lcom/google/android/gms/plus/internal/model/apps/a;

    invoke-direct {v1}, Lcom/google/android/gms/plus/internal/model/apps/a;-><init>()V

    invoke-interface {v0}, Lcom/google/android/gms/common/audience/a/q;->c()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/plus/internal/model/apps/a;->c:Ljava/util/ArrayList;

    invoke-interface {v0}, Lcom/google/android/gms/common/audience/a/q;->d()Z

    move-result v2

    iput-boolean v2, v1, Lcom/google/android/gms/plus/internal/model/apps/a;->d:Z

    invoke-interface {v0}, Lcom/google/android/gms/common/audience/a/q;->e()Z

    move-result v0

    iput-boolean v0, v1, Lcom/google/android/gms/plus/internal/model/apps/a;->e:Z

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Z

    iput-boolean v0, v1, Lcom/google/android/gms/plus/internal/model/apps/a;->f:Z

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Z

    iput-boolean v0, v1, Lcom/google/android/gms/plus/internal/model/apps/a;->g:Z

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/a;->a()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    .line 518
    const-string v0, "AppSettings"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 519
    const-string v0, "AppSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mFaclToWrite: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 527
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 528
    sget v1, Lcom/google/android/gms/j;->p:I

    if-ne v0, v1, :cond_1

    .line 529
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.action.VIEW_ACTIVITY_LOG"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.extras.ACCOUNT_NAME"

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "application"

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/common/analytics/d;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lcom/google/android/gms/common/analytics/d;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->d:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 543
    :cond_0
    :goto_0
    return-void

    .line 530
    :cond_1
    sget v1, Lcom/google/android/gms/j;->nl:I

    if-ne v0, v1, :cond_3

    .line 531
    invoke-direct {p0, v3}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a:Ljava/util/ArrayList;

    :cond_2
    invoke-static {}, Lcom/google/android/gms/common/audience/a/a;->a()Lcom/google/android/gms/common/audience/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/gms/common/audience/a/b;->a(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/b;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/gms/common/audience/a/b;->a(Ljava/util/List;)Lcom/google/android/gms/common/audience/a/b;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->ui:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/audience/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/b;

    move-result-object v0

    const-string v1, "81"

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/audience/a/b;->b(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/audience/a/b;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->startActivityForResult(Landroid/content/Intent;I)V

    sget-object v0, Lcom/google/android/gms/common/analytics/c;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0, v10}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    goto :goto_0

    .line 532
    :cond_3
    sget v1, Lcom/google/android/gms/j;->fV:I

    if-ne v0, v1, :cond_5

    .line 533
    invoke-direct {p0, v3}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b(Z)V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->h()Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->i()Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->j()Z

    move-result v3

    iget-object v4, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->k()Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v5}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->l()I

    move-result v5

    invoke-static {v5}, Lcom/google/android/gms/common/a/a;->a(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v6}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->m()Ljava/lang/String;

    move-result-object v6

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a:Ljava/util/ArrayList;

    :cond_4
    invoke-static {}, Lcom/google/android/gms/common/audience/a/o;->a()Lcom/google/android/gms/common/audience/a/p;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b:Landroid/accounts/Account;

    iget-object v8, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v7, v8}, Lcom/google/android/gms/common/audience/a/p;->p(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/p;

    move-result-object v7

    invoke-interface {v7, v0}, Lcom/google/android/gms/common/audience/a/p;->e(Ljava/util/List;)Lcom/google/android/gms/common/audience/a/p;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/audience/a/p;->d(Z)Lcom/google/android/gms/common/audience/a/p;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/gms/common/audience/a/p;->c(Z)Lcom/google/android/gms/common/audience/a/p;

    move-result-object v0

    invoke-interface {v0, v9}, Lcom/google/android/gms/common/audience/a/p;->g(Z)Lcom/google/android/gms/common/audience/a/p;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/google/android/gms/common/audience/a/p;->f(Z)Lcom/google/android/gms/common/audience/a/p;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/google/android/gms/common/audience/a/p;->e(Z)Lcom/google/android/gms/common/audience/a/p;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/google/android/gms/common/audience/a/p;->o(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/p;

    move-result-object v0

    invoke-interface {v0, v6}, Lcom/google/android/gms/common/audience/a/p;->n(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/p;

    move-result-object v0

    const-string v1, "81"

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/audience/a/p;->g(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    invoke-virtual {p0, v0, v9}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->startActivityForResult(Landroid/content/Intent;I)V

    sget-object v0, Lcom/google/android/gms/common/analytics/c;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0, v10}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    goto/16 :goto_0

    .line 534
    :cond_5
    sget v1, Lcom/google/android/gms/j;->es:I

    if-ne v0, v1, :cond_6

    .line 535
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->k:Z

    if-eqz v0, :cond_0

    .line 536
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->i:Z

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/plus/apps/aj;->a(Landroid/accounts/Account;Lcom/google/android/gms/plus/model/a/a;Ljava/lang/String;Z)Lcom/google/android/gms/plus/apps/aj;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "disconnect_source_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/apps/aj;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 541
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 175
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 176
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 177
    const-string v0, "AppSettings"

    const-string v1, "This activity is not available for restricted profile."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->setResult(I)V

    .line 179
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->finish()V

    .line 267
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->d:Ljava/lang/String;

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 187
    :cond_2
    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->setResult(I)V

    .line 188
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->finish()V

    goto :goto_0

    .line 193
    :cond_3
    if-eqz p1, :cond_4

    .line 194
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b:Landroid/accounts/Account;

    .line 195
    const-string v0, "app_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->c:Ljava/lang/String;

    .line 196
    const-string v0, "app_entity"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    .line 197
    const-string v0, "app_acls"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    .line 198
    const-string v0, "app_acls_loading"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->l:Z

    .line 199
    const-string v0, "app_acls_loaded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->m:Z

    .line 200
    const-string v0, "pacl_to_write"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    .line 201
    const-string v0, "facl_to_write"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    .line 202
    const-string v0, "facl_show_circles"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Z

    .line 203
    const-string v0, "facl_show_contacts"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Z

    .line 204
    const-string v0, "scopes"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->e:Ljava/lang/String;

    .line 205
    const-string v0, "revoke_handle"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->f:Ljava/lang/String;

    .line 206
    const-string v0, "is_signed_up"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 207
    const-string v0, "is_signed_up"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->i:Z

    .line 208
    iput-boolean v5, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->j:Z

    .line 213
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b:Landroid/accounts/Account;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->c:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 214
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.ACCOUNT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b:Landroid/accounts/Account;

    .line 215
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.APP_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->c:Ljava/lang/String;

    .line 216
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.APP_SCOPES"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->e:Ljava/lang/String;

    .line 217
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.APP_REVOKE_HANDLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->f:Ljava/lang/String;

    .line 219
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b:Landroid/accounts/Account;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->c:Ljava/lang/String;

    if-nez v0, :cond_9

    .line 220
    :cond_7
    const-string v0, "AppSettings"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 221
    const-string v0, "AppSettings"

    const-string v1, "Missing required extra(s): account=%s appId=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b:Landroid/accounts/Account;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->c:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    :cond_8
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->l()V

    goto/16 :goto_0

    .line 228
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "service_googleme"

    aput-object v2, v1, v4

    invoke-static {p0, v0, v1, p0}, Lcom/google/android/gms/common/d/a;->a(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;)Landroid/accounts/AccountManagerFuture;

    .line 232
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    .line 233
    const-string v0, "disconnect_source_fragment"

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/al;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->w:Lcom/google/android/gms/plus/apps/al;

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->w:Lcom/google/android/gms/plus/apps/al;

    if-nez v0, :cond_a

    .line 236
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b:Landroid/accounts/Account;

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/al;->a(Landroid/accounts/Account;)Lcom/google/android/gms/plus/apps/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->w:Lcom/google/android/gms/plus/apps/al;

    .line 237
    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 238
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->w:Lcom/google/android/gms/plus/apps/al;

    const-string v2, "disconnect_source_fragment"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 239
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 243
    :cond_a
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->f()Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-nez v0, :cond_b

    .line 246
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->l()V

    goto/16 :goto_0

    .line 249
    :cond_b
    invoke-static {p0}, Lcom/google/android/gms/plus/apps/a;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/apps/a;->a(Lcom/google/android/gms/plus/model/a/a;)Lcom/google/android/gms/plus/apps/b;

    move-result-object v1

    .line 250
    iget-boolean v0, v1, Lcom/google/android/gms/plus/apps/b;->c:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 251
    invoke-static {p0}, Lcom/google/android/gms/plus/apps/as;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/as;

    move-result-object v0

    .line 252
    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/apps/as;->a(Lcom/google/android/gms/plus/apps/au;)V

    .line 253
    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/plus/apps/as;->a(Lcom/google/android/gms/plus/model/a/a;Ljava/lang/String;)V

    .line 256
    :cond_c
    sget v0, Lcom/google/android/gms/l;->dQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->setContentView(I)V

    .line 259
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->m:Z

    if-eqz v0, :cond_d

    .line 260
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->e()V

    .line 263
    :cond_d
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->i()Z

    move-result v2

    if-eqz v2, :cond_e

    sget v2, Lcom/google/android/gms/p;->ul:I

    invoke-virtual {v0, v2}, Landroid/support/v7/app/a;->c(I)V

    :goto_1
    invoke-virtual {v0, v5}, Landroid/support/v7/app/a;->a(Z)V

    sget v0, Lcom/google/android/gms/j;->aH:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, v1, Lcom/google/android/gms/plus/apps/b;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v0, Lcom/google/android/gms/j;->aG:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, v1, Lcom/google/android/gms/plus/apps/b;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h()V

    .line 265
    invoke-static {}, Lcom/google/android/gms/plus/apps/af;->a()Lcom/google/android/gms/plus/apps/af;

    const-string v0, "disabled_dialog"

    invoke-static {p0, v6}, Lcom/google/android/gms/plus/apps/af;->a(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {p0, v6}, Lcom/google/android/gms/plus/apps/af;->b(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/plus/apps/ad;->a(Ljava/lang/CharSequence;)Lcom/google/android/gms/plus/apps/ad;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcom/google/android/gms/plus/apps/ad;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 263
    :cond_e
    sget v2, Lcom/google/android/gms/p;->ue:I

    invoke-virtual {v0, v2}, Landroid/support/v7/app/a;->c(I)V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 309
    const/4 v0, 0x1

    sget v1, Lcom/google/android/gms/p;->eU:I

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 310
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 315
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 329
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 317
    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/plus/c/a;->A:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 318
    invoke-static {p0, v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 322
    const-class v1, Lcom/google/android/gms/common/activity/WebViewActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 323
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 327
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 325
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 315
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 282
    invoke-super {p0}, Landroid/support/v7/app/d;->onPause()V

    .line 283
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->b()V

    .line 286
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->k:Z

    .line 287
    return-void
.end method

.method public onResumeFragments()V
    .locals 1

    .prologue
    .line 271
    invoke-super {p0}, Landroid/support/v7/app/d;->onResumeFragments()V

    .line 273
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 276
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->k:Z

    .line 277
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->w:Lcom/google/android/gms/plus/apps/al;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/apps/al;->a()V

    .line 278
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 291
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 292
    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b:Landroid/accounts/Account;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 293
    const-string v0, "app_id"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    const-string v0, "app_entity"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->g:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 295
    const-string v0, "app_acls"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 296
    const-string v0, "app_acls_loading"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 297
    const-string v0, "app_acls_loaded"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->m:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 298
    const-string v0, "pacl_to_write"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->s:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 299
    const-string v0, "facl_to_write"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->t:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 300
    const-string v0, "facl_show_circles"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 301
    const-string v0, "facl_show_contacts"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 302
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->j:Z

    if-eqz v0, :cond_0

    .line 303
    const-string v0, "is_signed_up"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 305
    :cond_0
    return-void
.end method

.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 2

    .prologue
    .line 989
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->j:Z

    .line 991
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->i:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 995
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageAppActivity;->h()V

    .line 997
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->i:Z

    if-eqz v0, :cond_1

    .line 998
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->x:Lcom/google/android/gms/plus/internal/ad;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p0, p0, p0, v1}, Lcom/google/android/gms/plus/apps/c;->a(Lcom/google/android/gms/plus/internal/ad;Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;Ljava/lang/String;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Lcom/google/android/gms/plus/internal/ab;

    .line 1004
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->k:Z

    if-eqz v0, :cond_0

    .line 1005
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 1007
    :cond_0
    return-void

    .line 1001
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->x:Lcom/google/android/gms/plus/internal/ad;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->b:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p0, p0, p0, v1}, Lcom/google/android/gms/plus/apps/c;->b(Lcom/google/android/gms/plus/internal/ad;Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;Ljava/lang/String;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageAppActivity;->y:Lcom/google/android/gms/plus/internal/ab;

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method
