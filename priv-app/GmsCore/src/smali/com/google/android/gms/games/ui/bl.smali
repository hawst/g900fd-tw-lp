.class public final Lcom/google/android/gms/games/ui/bl;
.super Lcom/google/android/gms/games/ui/cv;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/LayoutInflater;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Landroid/view/View$OnClickListener;

.field private h:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/cv;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/games/ui/bl;->a:Landroid/content/Context;

    .line 35
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/bl;->b:Landroid/view/LayoutInflater;

    .line 36
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bl;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/bl;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bl;->notifyDataSetChanged()V

    .line 45
    return-void
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 172
    if-nez p2, :cond_0

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bl;->b:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->bT:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 174
    new-instance v0, Lcom/google/android/gms/games/ui/bm;

    invoke-direct {v0, p0, p0, p2}, Lcom/google/android/gms/games/ui/bm;-><init>(Lcom/google/android/gms/games/ui/bl;Lcom/google/android/gms/games/ui/o;Landroid/view/View;)V

    .line 175
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 180
    :goto_0
    iget-object v1, v0, Lcom/google/android/gms/games/ui/bm;->c:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/google/android/gms/games/ui/bm;->g:Lcom/google/android/gms/games/ui/bl;

    iget-object v2, v2, Lcom/google/android/gms/games/ui/bl;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/bm;->g:Lcom/google/android/gms/games/ui/bl;

    iget-object v1, v1, Lcom/google/android/gms/games/ui/bl;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/google/android/gms/games/ui/bm;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/bm;->d:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/google/android/gms/games/ui/bm;->g:Lcom/google/android/gms/games/ui/bl;

    iget-object v2, v2, Lcom/google/android/gms/games/ui/bl;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v1, v0, Lcom/google/android/gms/games/ui/bm;->b:Landroid/view/View;

    iget-object v2, v0, Lcom/google/android/gms/games/ui/bm;->g:Lcom/google/android/gms/games/ui/bl;

    iget-object v2, v2, Lcom/google/android/gms/games/ui/bl;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/bm;->g:Lcom/google/android/gms/games/ui/bl;

    iget-boolean v1, v1, Lcom/google/android/gms/games/ui/bl;->e:Z

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/gms/games/ui/bm;->b:Landroid/view/View;

    iget-object v2, v0, Lcom/google/android/gms/games/ui/bm;->g:Lcom/google/android/gms/games/ui/bl;

    iget-object v2, v2, Lcom/google/android/gms/games/ui/bl;->h:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/bm;->b:Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/bm;->e:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/bm;->f:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/bm;->g:Lcom/google/android/gms/games/ui/bl;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/bl;->f:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    :goto_2
    return-object p2

    .line 177
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/bm;

    goto :goto_0

    .line 180
    :cond_1
    iget-object v1, v0, Lcom/google/android/gms/games/ui/bm;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    iget-object v1, v0, Lcom/google/android/gms/games/ui/bm;->b:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setClickable(Z)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/bm;->b:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, v0, Lcom/google/android/gms/games/ui/bm;->e:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method
