.class final Lcom/google/android/gms/fitness/sensors/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field final a:Ljava/util/Queue;

.field final b:Ljava/util/Set;

.field final c:Landroid/bluetooth/BluetoothDevice;

.field final d:Lcom/google/android/gms/fitness/sensors/a/m;

.field final e:Lcom/google/k/k/a/aq;

.field final f:Ljava/lang/Object;

.field final g:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final h:Ljava/lang/Object;

.field i:Lcom/google/k/k/a/aq;

.field final j:Ljava/lang/Object;

.field k:Lcom/google/k/k/a/aq;

.field final l:Ljava/util/concurrent/ConcurrentHashMap;

.field m:Landroid/bluetooth/BluetoothGatt;

.field final n:Ljava/lang/Object;

.field o:I

.field final p:Lcom/google/android/gms/fitness/sensors/a/ae;

.field private final q:Landroid/bluetooth/BluetoothGattCallback;

.field private final r:Lcom/google/k/k/a/ai;

.field private final s:Landroid/content/Context;

.field private final t:Landroid/os/Handler;

.field private final u:Landroid/bluetooth/BluetoothAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;Lcom/google/android/gms/fitness/sensors/a/ae;Landroid/os/Handler;Lcom/google/android/gms/fitness/sensors/a/m;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Lcom/google/android/gms/fitness/sensors/a/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/fitness/sensors/a/f;-><init>(Lcom/google/android/gms/fitness/sensors/a/e;)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->q:Landroid/bluetooth/BluetoothGattCallback;

    .line 206
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->b:Ljava/util/Set;

    .line 216
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->f:Ljava/lang/Object;

    .line 217
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 219
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->h:Ljava/lang/Object;

    .line 222
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->j:Ljava/lang/Object;

    .line 236
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->n:Ljava/lang/Object;

    .line 238
    iput v1, p0, Lcom/google/android/gms/fitness/sensors/a/e;->o:I

    .line 251
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/a/e;->s:Landroid/content/Context;

    .line 252
    iput-object p2, p0, Lcom/google/android/gms/fitness/sensors/a/e;->c:Landroid/bluetooth/BluetoothDevice;

    .line 253
    iput-object p4, p0, Lcom/google/android/gms/fitness/sensors/a/e;->t:Landroid/os/Handler;

    .line 254
    const-string v0, "Callback is not provided"

    invoke-static {p5, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/sensors/a/m;

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->d:Lcom/google/android/gms/fitness/sensors/a/m;

    .line 255
    invoke-static {}, Lcom/google/k/k/a/aq;->b()Lcom/google/k/k/a/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->e:Lcom/google/k/k/a/aq;

    .line 256
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->a:Ljava/util/Queue;

    .line 257
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v4, 0x0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    move v3, v2

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    .line 261
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor$DiscardPolicy;

    invoke-direct {v0}, Ljava/util/concurrent/ThreadPoolExecutor$DiscardPolicy;-><init>()V

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->setRejectedExecutionHandler(Ljava/util/concurrent/RejectedExecutionHandler;)V

    .line 262
    invoke-static {v1}, Lcom/google/k/k/a/ak;->a(Ljava/util/concurrent/ExecutorService;)Lcom/google/k/k/a/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->r:Lcom/google/k/k/a/ai;

    .line 264
    const-string v0, "bluetooth"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothManager;

    .line 266
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->u:Landroid/bluetooth/BluetoothAdapter;

    .line 267
    iput-object p3, p0, Lcom/google/android/gms/fitness/sensors/a/e;->p:Lcom/google/android/gms/fitness/sensors/a/ae;

    .line 268
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->l:Ljava/util/concurrent/ConcurrentHashMap;

    .line 269
    return-void
.end method

.method private d()Lcom/google/k/k/a/af;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 300
    const-string v0, "Setting up disconnect future."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 301
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/e;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 302
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->i:Lcom/google/k/k/a/aq;

    if-nez v0, :cond_0

    .line 303
    const-string v0, "New disconnect future is created."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 304
    invoke-static {}, Lcom/google/k/k/a/aq;->b()Lcom/google/k/k/a/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->i:Lcom/google/k/k/a/aq;

    .line 305
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->i:Lcom/google/k/k/a/aq;

    .line 306
    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/e;->t:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/gms/fitness/sensors/a/g;

    invoke-direct {v3, p0, v0}, Lcom/google/android/gms/fitness/sensors/a/g;-><init>(Lcom/google/android/gms/fitness/sensors/a/e;Lcom/google/k/k/a/aq;)V

    invoke-static {}, Lcom/google/android/gms/fitness/sensors/a/e;->e()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 308
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->i:Lcom/google/k/k/a/aq;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 309
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static e()J
    .locals 4

    .prologue
    .line 323
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->ah:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private f()Z
    .locals 3

    .prologue
    .line 544
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->m:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->connect()Z

    move-result v0

    .line 545
    if-nez v0, :cond_0

    .line 546
    const-string v1, "Failed to initiate connection request."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 548
    :cond_0
    return v0
.end method


# virtual methods
.method public final a()Lcom/google/k/k/a/af;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 283
    const-string v0, "Disconnecting from BLE device %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/e;->c:Landroid/bluetooth/BluetoothDevice;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 284
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/e;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 285
    const/4 v0, 0x3

    :try_start_0
    iput v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->o:I

    .line 286
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 287
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/e;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 288
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->m:Landroid/bluetooth/BluetoothGatt;

    if-eqz v0, :cond_0

    .line 289
    invoke-direct {p0}, Lcom/google/android/gms/fitness/sensors/a/e;->d()Lcom/google/k/k/a/af;

    move-result-object v0

    .line 290
    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/e;->m:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothGatt;->disconnect()V

    .line 291
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 294
    :goto_0
    return-object v0

    .line 286
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 293
    :cond_0
    :try_start_2
    const-string v0, "GATT is already destroyed. Not disconnecting."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 294
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 296
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Landroid/bluetooth/BluetoothGattCharacteristic;)Lcom/google/k/k/a/af;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 389
    const-string v0, "listenOrRead on characteristic %s"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 391
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getProperties()I

    move-result v0

    .line 392
    and-int/lit8 v1, v0, 0x2

    if-lez v1, :cond_0

    .line 393
    invoke-virtual {p0, p1}, Lcom/google/android/gms/fitness/sensors/a/e;->b(Landroid/bluetooth/BluetoothGattCharacteristic;)Lcom/google/k/k/a/af;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/google/k/a/v;->a(Ljava/lang/Object;)Lcom/google/k/a/u;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/k/k/a/n;->b(Lcom/google/k/k/a/af;Lcom/google/k/a/u;)Lcom/google/k/k/a/af;

    move-result-object v0

    .line 402
    :goto_0
    return-object v0

    .line 394
    :cond_0
    and-int/lit8 v0, v0, 0x10

    if-lez v0, :cond_1

    .line 395
    invoke-static {}, Lcom/google/k/k/a/aq;->b()Lcom/google/k/k/a/aq;

    move-result-object v0

    .line 396
    new-instance v1, Lcom/google/android/gms/fitness/sensors/a/p;

    invoke-direct {v1, p0, p1, v0, v3}, Lcom/google/android/gms/fitness/sensors/a/p;-><init>(Lcom/google/android/gms/fitness/sensors/a/e;Landroid/bluetooth/BluetoothGattCharacteristic;Lcom/google/k/k/a/aq;B)V

    .line 397
    invoke-virtual {p0, v1}, Lcom/google/android/gms/fitness/sensors/a/e;->a(Lcom/google/android/gms/fitness/sensors/a/o;)V

    goto :goto_0

    .line 400
    :cond_1
    const-string v0, "Characteristic %s is not a read or notify property"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 402
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v0

    goto :goto_0
.end method

.method final a(Lcom/google/android/gms/fitness/sensors/a/o;)V
    .locals 1

    .prologue
    .line 563
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->a:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 564
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/sensors/a/e;->c()V

    .line 565
    return-void
.end method

.method final b()Lcom/google/k/k/a/af;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 461
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->u:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 462
    const-string v0, "Bluetooth adapter is not enabled, not connecting."

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 463
    new-instance v0, Lcom/google/android/gms/fitness/sensors/a/k;

    const-string v1, "Bluetooth adapter is not enabled."

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/fitness/sensors/a/k;-><init>(Lcom/google/android/gms/fitness/sensors/a/e;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/k/k/a/n;->a(Ljava/lang/Throwable;)Lcom/google/k/k/a/af;

    move-result-object v0

    .line 504
    :goto_0
    return-object v0

    .line 467
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/e;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 468
    :try_start_0
    iget v2, p0, Lcom/google/android/gms/fitness/sensors/a/e;->o:I

    .line 469
    iget v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->o:I

    packed-switch v0, :pswitch_data_0

    .line 479
    const-string v0, "Connecting to device."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 480
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->o:I

    .line 481
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/e;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 485
    :try_start_1
    const-string v0, "Setting up connect future."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 486
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->k:Lcom/google/k/k/a/aq;

    if-nez v0, :cond_1

    .line 487
    const-string v0, "Creating new connection future."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 488
    invoke-static {}, Lcom/google/k/k/a/aq;->b()Lcom/google/k/k/a/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->k:Lcom/google/k/k/a/aq;

    .line 490
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->k:Lcom/google/k/k/a/aq;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v3}, Lcom/google/k/a/v;->a(Ljava/lang/Object;)Lcom/google/k/a/u;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/k/k/a/n;->b(Lcom/google/k/k/a/af;Lcom/google/k/a/u;)Lcom/google/k/k/a/af;

    move-result-object v0

    .line 491
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 492
    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/a/e;->f:Ljava/lang/Object;

    monitor-enter v3

    .line 493
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/e;->m:Landroid/bluetooth/BluetoothGatt;

    if-eqz v1, :cond_2

    const-string v1, "Connecting using existing Gatt."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v4}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-direct {p0}, Lcom/google/android/gms/fitness/sensors/a/e;->f()Z

    move-result v1

    .line 495
    :goto_1
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 496
    if-nez v1, :cond_3

    .line 497
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/e;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 498
    :try_start_3
    const-string v0, "Restoring to previous state %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 499
    iput v2, p0, Lcom/google/android/gms/fitness/sensors/a/e;->o:I

    .line 500
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 501
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v0

    goto :goto_0

    .line 471
    :pswitch_0
    :try_start_4
    const-string v0, "Already connected to device."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 472
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 481
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 474
    :pswitch_1
    :try_start_5
    const-string v0, "Connection is already in progress."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 475
    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/e;->j:Ljava/lang/Object;

    monitor-enter v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 476
    :try_start_6
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->k:Lcom/google/k/k/a/aq;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v3}, Lcom/google/k/a/v;->a(Ljava/lang/Object;)Lcom/google/k/a/u;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/k/k/a/n;->b(Lcom/google/k/k/a/af;Lcom/google/k/a/u;)Lcom/google/k/k/a/af;

    move-result-object v0

    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    monitor-exit v1

    goto/16 :goto_0

    .line 477
    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 491
    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0

    .line 493
    :cond_2
    :try_start_8
    const-string v1, "Connecting using new Gatt."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v4}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/e;->c:Landroid/bluetooth/BluetoothDevice;

    iget-object v4, p0, Lcom/google/android/gms/fitness/sensors/a/e;->s:Landroid/content/Context;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/gms/fitness/sensors/a/e;->q:Landroid/bluetooth/BluetoothGattCallback;

    invoke-virtual {v1, v4, v5, v6}, Landroid/bluetooth/BluetoothDevice;->connectGatt(Landroid/content/Context;ZLandroid/bluetooth/BluetoothGattCallback;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/e;->m:Landroid/bluetooth/BluetoothGatt;

    invoke-direct {p0}, Lcom/google/android/gms/fitness/sensors/a/e;->f()Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    move-result v1

    goto :goto_1

    .line 495
    :catchall_3
    move-exception v0

    monitor-exit v3

    throw v0

    .line 500
    :catchall_4
    move-exception v0

    monitor-exit v1

    throw v0

    .line 503
    :cond_3
    const-string v1, "Setting up connection timeout checker."

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/e;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_9
    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/e;->k:Lcom/google/k/k/a/aq;

    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/a/e;->t:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/gms/fitness/sensors/a/h;

    invoke-direct {v4, p0, v2}, Lcom/google/android/gms/fitness/sensors/a/h;-><init>(Lcom/google/android/gms/fitness/sensors/a/e;Lcom/google/k/k/a/af;)V

    invoke-static {}, Lcom/google/android/gms/fitness/sensors/a/e;->e()J

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    goto/16 :goto_0

    :catchall_5
    move-exception v0

    monitor-exit v1

    throw v0

    .line 469
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Landroid/bluetooth/BluetoothGattCharacteristic;)Lcom/google/k/k/a/af;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 415
    const-string v0, "read on characteristic %s"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 417
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getProperties()I

    move-result v0

    .line 418
    and-int/lit8 v0, v0, 0x2

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Characteristic %s does not have read property"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 423
    new-instance v4, Lcom/google/android/gms/fitness/sensors/a/q;

    invoke-direct {v4, p0, p1, v2}, Lcom/google/android/gms/fitness/sensors/a/q;-><init>(Lcom/google/android/gms/fitness/sensors/a/e;Landroid/bluetooth/BluetoothGattCharacteristic;B)V

    .line 424
    invoke-static {}, Lcom/google/k/k/a/aq;->b()Lcom/google/k/k/a/aq;

    move-result-object v3

    .line 425
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v0, v5, v3}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/k/a/aq;

    .line 427
    if-eqz v0, :cond_1

    .line 428
    const-string v3, "There is already a read request for %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v3, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 432
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 418
    goto :goto_0

    .line 431
    :cond_1
    invoke-virtual {p0, v4}, Lcom/google/android/gms/fitness/sensors/a/e;->a(Lcom/google/android/gms/fitness/sensors/a/o;)V

    move-object v0, v3

    .line 432
    goto :goto_1
.end method

.method final c()V
    .locals 3

    .prologue
    .line 568
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/e;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 569
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->o:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    .line 570
    const-string v0, "Not running commands, not connected."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 571
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/sensors/a/e;->b()Lcom/google/k/k/a/af;

    .line 572
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 584
    :cond_0
    :goto_0
    return-void

    .line 574
    :cond_1
    monitor-exit v1

    .line 575
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/e;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/sensors/a/o;

    .line 576
    if-eqz v0, :cond_0

    .line 577
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/e;->r:Lcom/google/k/k/a/ai;

    invoke-interface {v1, v0}, Lcom/google/k/k/a/ai;->a(Ljava/util/concurrent/Callable;)Lcom/google/k/k/a/af;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/fitness/sensors/a/j;

    invoke-direct {v1, p0}, Lcom/google/android/gms/fitness/sensors/a/j;-><init>(Lcom/google/android/gms/fitness/sensors/a/e;)V

    invoke-static {v0, v1}, Lcom/google/k/k/a/n;->a(Lcom/google/k/k/a/af;Lcom/google/k/k/a/m;)V

    goto :goto_0

    .line 574
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
