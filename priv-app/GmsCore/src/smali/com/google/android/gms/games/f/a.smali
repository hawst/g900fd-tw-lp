.class public final Lcom/google/android/gms/games/f/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Z

.field private static b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/google/android/gms/games/ui/n;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/games/f/a;->a:Z

    .line 211
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gms/games/f/a;->b:Z

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/g/af;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 638
    new-instance v0, Lcom/google/android/gms/games/g/af;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/af;-><init>()V

    .line 639
    iput-boolean v1, v0, Lcom/google/android/gms/games/g/af;->a:Z

    .line 640
    iput-object p2, v0, Lcom/google/android/gms/games/g/af;->b:Ljava/lang/String;

    .line 641
    iput-object p1, v0, Lcom/google/android/gms/games/g/af;->d:Ljava/lang/String;

    .line 642
    const-string v1, "6777000"

    iput-object v1, v0, Lcom/google/android/gms/games/g/af;->e:Ljava/lang/String;

    .line 648
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 649
    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 650
    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    int-to-long v2, v1

    iput-wide v2, v0, Lcom/google/android/gms/games/g/af;->f:J
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 654
    :goto_0
    return-object v0

    .line 652
    :catch_0
    move-exception v1

    const-string v1, "GamesPlayLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not find package info for package: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;IIJ)V
    .locals 4

    .prologue
    .line 509
    new-instance v0, Lcom/google/android/gms/games/g/r;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/r;-><init>()V

    .line 510
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {p0, v1, v2}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/g/af;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    .line 512
    new-instance v1, Lcom/google/android/gms/games/g/aq;

    invoke-direct {v1}, Lcom/google/android/gms/games/g/aq;-><init>()V

    iput-object v1, v0, Lcom/google/android/gms/games/g/r;->p:Lcom/google/android/gms/games/g/aq;

    .line 513
    iget-object v1, v0, Lcom/google/android/gms/games/g/r;->p:Lcom/google/android/gms/games/g/aq;

    iput p2, v1, Lcom/google/android/gms/games/g/aq;->a:I

    .line 514
    iget-object v1, v0, Lcom/google/android/gms/games/g/r;->p:Lcom/google/android/gms/games/g/aq;

    iput p3, v1, Lcom/google/android/gms/games/g/aq;->b:I

    .line 515
    iget-object v1, v0, Lcom/google/android/gms/games/g/r;->p:Lcom/google/android/gms/games/g/aq;

    iput-wide p4, v1, Lcom/google/android/gms/games/g/aq;->c:J

    .line 516
    invoke-static {p0, p1, v0}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/g/r;)V

    .line 517
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 394
    new-instance v6, Lcom/google/android/gms/games/g/r;

    invoke-direct {v6}, Lcom/google/android/gms/games/g/r;-><init>()V

    .line 395
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/g/af;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    .line 397
    if-nez p3, :cond_1

    move v1, v2

    .line 398
    :goto_0
    new-array v7, v1, [Lcom/google/android/gms/games/g/c;

    move v5, v2

    .line 400
    :goto_1
    if-ge v5, v1, :cond_2

    .line 401
    invoke-virtual {p3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/f/b;

    .line 402
    new-instance v3, Lcom/google/android/gms/games/g/c;

    invoke-direct {v3}, Lcom/google/android/gms/games/g/c;-><init>()V

    aput-object v3, v7, v5

    .line 403
    aget-object v3, v7, v5

    iget-object v8, v0, Lcom/google/android/gms/games/f/b;->a:Ljava/lang/String;

    iput-object v8, v3, Lcom/google/android/gms/games/g/c;->a:Ljava/lang/String;

    .line 408
    iget-object v3, v0, Lcom/google/android/gms/games/f/b;->b:Ljava/lang/String;

    .line 409
    if-nez v3, :cond_0

    .line 410
    const-string v3, ""

    .line 412
    :cond_0
    aget-object v8, v7, v5

    iput-object v3, v8, Lcom/google/android/gms/games/g/c;->b:Ljava/lang/String;

    .line 413
    aget-object v3, v7, v5

    iget v0, v0, Lcom/google/android/gms/games/f/b;->c:I

    sparse-switch v0, :sswitch_data_0

    const-string v8, "GamesPlayLogger"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "The notification type: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, " is not supported. Please add it to the proto google3/logs/proto/wireless/android/play/playlog/play_games.proto, sync with the server, and update mapping in GamesPlayerLogger."

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    :goto_2
    iput v0, v3, Lcom/google/android/gms/games/g/c;->c:I

    .line 400
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    .line 397
    :cond_1
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v1, v0

    goto :goto_0

    :sswitch_0
    move v0, v4

    .line 413
    goto :goto_2

    :sswitch_1
    const/4 v0, 0x2

    goto :goto_2

    :sswitch_2
    const/4 v0, 0x3

    goto :goto_2

    :sswitch_3
    const/4 v0, 0x4

    goto :goto_2

    :sswitch_4
    const/4 v0, 0x5

    goto :goto_2

    .line 415
    :cond_2
    new-instance v0, Lcom/google/android/gms/games/g/t;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/t;-><init>()V

    iput v4, v0, Lcom/google/android/gms/games/g/t;->a:I

    new-instance v2, Lcom/google/android/gms/games/g/b;

    invoke-direct {v2}, Lcom/google/android/gms/games/g/b;-><init>()V

    iput p2, v2, Lcom/google/android/gms/games/g/b;->a:I

    array-length v3, v7

    if-lez v3, :cond_3

    iput-object v7, v2, Lcom/google/android/gms/games/g/b;->d:[Lcom/google/android/gms/games/g/c;

    :cond_3
    if-ltz v1, :cond_4

    iput v1, v2, Lcom/google/android/gms/games/g/b;->c:I

    :cond_4
    iput-object v2, v0, Lcom/google/android/gms/games/g/t;->b:Lcom/google/android/gms/games/g/b;

    iput-object v0, v6, Lcom/google/android/gms/games/g/r;->f:Lcom/google/android/gms/games/g/t;

    .line 416
    invoke-static {p0, p1, v6}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/g/r;)V

    .line 417
    return-void

    .line 413
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x8 -> :sswitch_3
        0x10 -> :sswitch_4
    .end sparse-switch
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/g/r;)V
    .locals 6

    .prologue
    .line 616
    sget-boolean v0, Lcom/google/android/gms/games/f/a;->b:Z

    if-nez v0, :cond_0

    .line 632
    :goto_0
    return-void

    .line 620
    :cond_0
    sget-boolean v0, Lcom/google/android/gms/games/f/a;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "GamesPlayLogger"

    invoke-virtual {p2}, Lcom/google/android/gms/games/g/r;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 625
    :cond_1
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 627
    :try_start_0
    new-instance v0, Lcom/google/android/gms/playlog/a;

    const/4 v1, 0x5

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/gms/playlog/a;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    .line 628
    const/4 v1, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/g/r;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v0, v1, v4, v5}, Lcom/google/android/gms/playlog/a;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    .line 629
    invoke-virtual {v0}, Lcom/google/android/gms/playlog/a;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 631
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .locals 2

    .prologue
    .line 376
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 377
    if-eqz p5, :cond_0

    .line 378
    new-instance v1, Lcom/google/android/gms/games/f/b;

    invoke-direct {v1, p1, p5, p3}, Lcom/google/android/gms/games/f/b;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 380
    :cond_0
    invoke-static {p0, p2, p4, v0}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 381
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 2

    .prologue
    .line 431
    new-instance v0, Lcom/google/android/gms/games/g/r;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/r;-><init>()V

    .line 432
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, p1}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/g/af;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    .line 434
    new-instance v1, Lcom/google/android/gms/games/g/ac;

    invoke-direct {v1}, Lcom/google/android/gms/games/g/ac;-><init>()V

    iput p3, v1, Lcom/google/android/gms/games/g/ac;->a:I

    iput-boolean p4, v1, Lcom/google/android/gms/games/g/ac;->b:Z

    iput-object v1, v0, Lcom/google/android/gms/games/g/r;->h:Lcom/google/android/gms/games/g/ac;

    .line 435
    invoke-static {p0, p2, v0}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/g/r;)V

    .line 436
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 236
    new-instance v0, Lcom/google/android/gms/games/g/r;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/r;-><init>()V

    .line 237
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/g/af;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    .line 239
    new-instance v1, Lcom/google/android/gms/games/g/v;

    invoke-direct {v1}, Lcom/google/android/gms/games/g/v;-><init>()V

    iput p4, v1, Lcom/google/android/gms/games/g/v;->a:I

    iput-object v1, v0, Lcom/google/android/gms/games/g/r;->c:Lcom/google/android/gms/games/g/v;

    .line 240
    invoke-static {p0, p3, v0}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/g/r;)V

    .line 241
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 3

    .prologue
    .line 255
    new-instance v0, Lcom/google/android/gms/games/g/r;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/r;-><init>()V

    .line 256
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/g/af;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    .line 258
    iget-object v1, v0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/games/g/af;->e:Ljava/lang/String;

    .line 259
    new-instance v1, Lcom/google/android/gms/games/g/ao;

    invoke-direct {v1}, Lcom/google/android/gms/games/g/ao;-><init>()V

    iput p5, v1, Lcom/google/android/gms/games/g/ao;->c:I

    iput-object v1, v0, Lcom/google/android/gms/games/g/r;->b:Lcom/google/android/gms/games/g/ao;

    .line 260
    invoke-static {p0, p3, v0}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/g/r;)V

    .line 261
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 8

    .prologue
    .line 531
    const-string v6, ""

    const/4 v7, 0x4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 533
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)V
    .locals 8

    .prologue
    .line 548
    const-string v6, ""

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move v7, p6

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 550
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 567
    new-instance v0, Lcom/google/android/gms/games/g/r;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/r;-><init>()V

    .line 568
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/g/af;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    .line 570
    new-instance v1, Lcom/google/android/gms/games/g/ap;

    invoke-direct {v1}, Lcom/google/android/gms/games/g/ap;-><init>()V

    iput-object v1, v0, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    .line 571
    iget-object v1, v0, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    iput p4, v1, Lcom/google/android/gms/games/g/ap;->a:I

    .line 572
    iget-object v1, v0, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    iput p7, v1, Lcom/google/android/gms/games/g/ap;->e:I

    .line 573
    iget-object v1, v0, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    iput-object p5, v1, Lcom/google/android/gms/games/g/ap;->b:Ljava/lang/String;

    .line 574
    iget-object v1, v0, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/android/gms/games/g/ap;->c:I

    .line 575
    iget-object v1, v0, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    iput-object p6, v1, Lcom/google/android/gms/games/g/ap;->d:Ljava/lang/String;

    .line 576
    invoke-static {p0, p3, v0}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/g/r;)V

    .line 577
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JIIJ)V
    .locals 4

    .prologue
    .line 278
    new-instance v0, Lcom/google/android/gms/games/g/r;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/r;-><init>()V

    .line 279
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/g/af;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    .line 281
    new-instance v1, Lcom/google/android/gms/games/g/ao;

    invoke-direct {v1}, Lcom/google/android/gms/games/g/ao;-><init>()V

    iput-object v1, v0, Lcom/google/android/gms/games/g/r;->b:Lcom/google/android/gms/games/g/ao;

    .line 282
    iget-object v1, v0, Lcom/google/android/gms/games/g/r;->b:Lcom/google/android/gms/games/g/ao;

    new-instance v2, Lcom/google/android/gms/games/g/d;

    invoke-direct {v2}, Lcom/google/android/gms/games/g/d;-><init>()V

    iput p6, v2, Lcom/google/android/gms/games/g/d;->a:I

    iput-wide p4, v2, Lcom/google/android/gms/games/g/d;->b:J

    iput p7, v2, Lcom/google/android/gms/games/g/d;->c:I

    iput-wide p8, v2, Lcom/google/android/gms/games/g/d;->d:J

    iput-object v2, v1, Lcom/google/android/gms/games/g/ao;->d:Lcom/google/android/gms/games/g/d;

    .line 284
    invoke-static {p0, p3, v0}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/g/r;)V

    .line 285
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZIZZ[I)V
    .locals 4

    .prologue
    .line 481
    new-instance v0, Lcom/google/android/gms/games/g/r;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/r;-><init>()V

    .line 482
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {p0, v1, v2}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/g/af;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    .line 484
    new-instance v1, Lcom/google/android/gms/games/g/e;

    invoke-direct {v1}, Lcom/google/android/gms/games/g/e;-><init>()V

    iput-object v1, v0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    .line 485
    iget-object v1, v0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    iput-object p2, v1, Lcom/google/android/gms/games/g/e;->a:Ljava/lang/String;

    .line 486
    iget-object v1, v0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    iput-object p3, v1, Lcom/google/android/gms/games/g/e;->b:Ljava/lang/String;

    .line 487
    iget-object v1, v0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    iput-wide p4, v1, Lcom/google/android/gms/games/g/e;->c:J

    .line 488
    iget-object v1, v0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    iput-boolean p6, v1, Lcom/google/android/gms/games/g/e;->d:Z

    .line 489
    iget-object v1, v0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    iput p7, v1, Lcom/google/android/gms/games/g/e;->e:I

    .line 490
    iget-object v1, v0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    iput-boolean p8, v1, Lcom/google/android/gms/games/g/e;->g:Z

    .line 491
    iget-object v1, v0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    iput-boolean p9, v1, Lcom/google/android/gms/games/g/e;->f:Z

    .line 492
    if-eqz p10, :cond_0

    array-length v1, p10

    if-lez v1, :cond_0

    .line 493
    iget-object v1, v0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    iput-object p10, v1, Lcom/google/android/gms/games/g/e;->h:[I

    .line 495
    :cond_0
    invoke-static {p0, p1, v0}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/g/r;)V

    .line 496
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/g/ag;)V
    .locals 2

    .prologue
    .line 590
    new-instance v0, Lcom/google/android/gms/games/g/r;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/r;-><init>()V

    .line 591
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/g/af;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    .line 593
    iput-object p4, v0, Lcom/google/android/gms/games/g/r;->j:Lcom/google/android/gms/games/g/ag;

    .line 594
    invoke-static {p0, p3, v0}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/g/r;)V

    .line 595
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/g/as;)V
    .locals 2

    .prologue
    .line 608
    new-instance v0, Lcom/google/android/gms/games/g/r;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/r;-><init>()V

    .line 609
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/g/af;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    .line 611
    iput-object p4, v0, Lcom/google/android/gms/games/g/r;->l:Lcom/google/android/gms/games/g/as;

    .line 612
    invoke-static {p0, p3, v0}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/g/r;)V

    .line 613
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;I)V
    .locals 6

    .prologue
    .line 450
    new-instance v3, Lcom/google/android/gms/games/g/r;

    invoke-direct {v3}, Lcom/google/android/gms/games/g/r;-><init>()V

    .line 451
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/g/af;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    .line 453
    new-instance v0, Lcom/google/android/gms/games/g/am;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/am;-><init>()V

    iput-object v0, v3, Lcom/google/android/gms/games/g/r;->n:Lcom/google/android/gms/games/g/am;

    .line 454
    iget-object v0, v3, Lcom/google/android/gms/games/g/r;->n:Lcom/google/android/gms/games/g/am;

    iput p5, v0, Lcom/google/android/gms/games/g/am;->b:I

    .line 455
    iget-object v0, v3, Lcom/google/android/gms/games/g/r;->n:Lcom/google/android/gms/games/g/am;

    invoke-virtual {p4}, Ljava/util/HashMap;->size()I

    move-result v1

    new-array v1, v1, [I

    iput-object v1, v0, Lcom/google/android/gms/games/g/am;->a:[I

    .line 456
    const/4 v0, 0x0

    .line 457
    invoke-virtual {p4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 459
    iget-object v2, v3, Lcom/google/android/gms/games/g/r;->n:Lcom/google/android/gms/games/g/am;

    iget-object v5, v2, Lcom/google/android/gms/games/g/am;->a:[I

    add-int/lit8 v2, v1, 0x1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v5, v1

    move v1, v2

    .line 460
    goto :goto_0

    .line 461
    :cond_0
    invoke-static {p0, p3, v3}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/g/r;)V

    .line 462
    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 298
    new-instance v0, Lcom/google/android/gms/games/g/r;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/r;-><init>()V

    .line 299
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/g/af;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    .line 301
    new-instance v1, Lcom/google/android/gms/games/g/ad;

    invoke-direct {v1}, Lcom/google/android/gms/games/g/ad;-><init>()V

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/android/gms/games/g/ad;->a:I

    iput p4, v1, Lcom/google/android/gms/games/g/ad;->b:I

    iput-object v1, v0, Lcom/google/android/gms/games/g/r;->m:Lcom/google/android/gms/games/g/ad;

    .line 303
    invoke-static {p0, p3, v0}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/g/r;)V

    .line 304
    return-void
.end method
