.class final Lcom/google/android/gms/audiomodem/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/io/PipedInputStream;

.field final synthetic b:Ljava/lang/Thread;

.field final synthetic c:Lcom/google/android/gms/audiomodem/y;


# direct methods
.method constructor <init>(Lcom/google/android/gms/audiomodem/y;Ljava/io/PipedInputStream;Ljava/lang/Thread;)V
    .locals 0

    .prologue
    .line 271
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/ab;->c:Lcom/google/android/gms/audiomodem/y;

    iput-object p2, p0, Lcom/google/android/gms/audiomodem/ab;->a:Ljava/io/PipedInputStream;

    iput-object p3, p0, Lcom/google/android/gms/audiomodem/ab;->b:Ljava/lang/Thread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    const/4 v13, 0x6

    const/4 v12, 0x3

    const/4 v2, 0x0

    .line 274
    const-string v0, "audioModem"

    invoke-static {v0, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    const-string v0, "AudioRecorder: Processor thread started"

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    :cond_0
    invoke-static {}, Lcom/google/android/gms/audiomodem/s;->a()Lcom/google/android/gms/audiomodem/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/s;->b()Lcom/google/whispernet/b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/whispernet/b;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v3, v0

    .line 278
    :goto_0
    if-eqz v3, :cond_1

    .line 279
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/ab;->c:Lcom/google/android/gms/audiomodem/y;

    iget-object v0, v0, Lcom/google/android/gms/audiomodem/y;->k:Lcom/google/android/gms/audiomodem/aj;

    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/aj;->a()V

    .line 281
    :cond_1
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 282
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/ab;->c:Lcom/google/android/gms/audiomodem/y;

    iget-object v0, v0, Lcom/google/android/gms/audiomodem/y;->d:[B

    array-length v0, v0

    new-array v4, v0, [B

    .line 284
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/ab;->c:Lcom/google/android/gms/audiomodem/y;

    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/y;->b()I

    move-result v0

    mul-int/lit8 v5, v0, 0x2

    .line 286
    add-int/lit8 v0, v5, -0x1

    new-array v6, v0, [B

    .line 289
    :cond_2
    :goto_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/ab;->a:Ljava/io/PipedInputStream;

    const/4 v7, 0x0

    array-length v8, v4

    invoke-virtual {v0, v4, v7, v8}, Ljava/io/PipedInputStream;->read([BII)I

    move-result v7

    .line 290
    if-gez v7, :cond_7

    .line 291
    const-string v0, "audioModem"

    const/4 v7, 0x3

    invoke-static {v0, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 292
    const-string v0, "AudioRecorder: Processor exiting loop, no more data in pipe"

    const-string v7, "audioModem"

    invoke-static {v7, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 335
    :cond_3
    :try_start_1
    const-string v0, "audioModem"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 336
    const-string v0, "AudioRecorder: Processor waiting for recorder to exit"

    const-string v2, "audioModem"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/ab;->b:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V

    .line 339
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/ab;->a:Ljava/io/PipedInputStream;

    invoke-virtual {v0}, Ljava/io/PipedInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 350
    :cond_5
    :goto_2
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    if-lez v0, :cond_d

    .line 351
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 352
    array-length v0, v1

    div-int/2addr v0, v5

    mul-int v2, v5, v0

    .line 354
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/ab;->c:Lcom/google/android/gms/audiomodem/y;

    iget-object v0, v0, Lcom/google/android/gms/audiomodem/y;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/audiomodem/ac;

    .line 355
    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/audiomodem/ac;->a([BI)V

    goto :goto_3

    .line 277
    :cond_6
    sget-object v0, Lcom/google/android/gms/audiomodem/m;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v3, v0

    goto/16 :goto_0

    .line 296
    :cond_7
    :try_start_2
    sget-object v0, Lcom/google/android/gms/audiomodem/m;->u:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v7, v0, :cond_8

    .line 298
    const-string v0, "audioModem"

    const/4 v8, 0x5

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 299
    const-string v0, "AudioRecorder: Audio samples may have been lost."

    const-string v8, "audioModem"

    invoke-static {v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    :cond_8
    const/4 v0, 0x0

    invoke-virtual {v1, v4, v0, v7}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 304
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    int-to-long v8, v0

    iget-object v0, p0, Lcom/google/android/gms/audiomodem/ab;->c:Lcom/google/android/gms/audiomodem/y;

    iget-wide v10, v0, Lcom/google/android/gms/audiomodem/y;->i:J

    cmp-long v0, v8, v10

    if-lez v0, :cond_11

    .line 305
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    .line 308
    array-length v0, v7

    div-int/2addr v0, v5

    mul-int v8, v5, v0

    .line 311
    array-length v0, v7

    sub-int v9, v0, v8

    move v0, v2

    .line 312
    :goto_4
    if-ge v0, v9, :cond_9

    .line 313
    add-int v10, v8, v0

    aget-byte v10, v7, v10

    aput-byte v10, v6, v0

    .line 312
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 315
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/ab;->c:Lcom/google/android/gms/audiomodem/y;

    iget-object v0, v0, Lcom/google/android/gms/audiomodem/y;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/audiomodem/ac;

    .line 316
    invoke-interface {v0, v7, v8}, Lcom/google/android/gms/audiomodem/ac;->a([BI)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_5

    .line 326
    :catch_0
    move-exception v0

    .line 327
    :goto_6
    const-string v7, "audioModem"

    invoke-static {v7, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 328
    const-string v7, "AudioRecorder: Processor failed to read from pipe"

    const-string v8, "audioModem"

    invoke-static {v8, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 318
    :cond_a
    if-eqz v3, :cond_b

    .line 319
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/ab;->c:Lcom/google/android/gms/audiomodem/y;

    iget-object v0, v0, Lcom/google/android/gms/audiomodem/y;->k:Lcom/google/android/gms/audiomodem/aj;

    invoke-virtual {v0, v7, v8}, Lcom/google/android/gms/audiomodem/aj;->a([BI)V

    .line 322
    :cond_b
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 323
    const/4 v1, 0x0

    :try_start_4
    invoke-virtual {v0, v6, v1, v9}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :goto_7
    move-object v1, v0

    .line 330
    goto/16 :goto_1

    .line 341
    :catch_1
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 376
    :goto_8
    return-void

    .line 343
    :catch_2
    move-exception v0

    .line 344
    const-string v2, "audioModem"

    invoke-static {v2, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 345
    const-string v2, "AudioRecorder: Processor failed to close pipe"

    const-string v4, "audioModem"

    invoke-static {v4, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    .line 357
    :cond_c
    if-eqz v3, :cond_d

    .line 358
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/ab;->c:Lcom/google/android/gms/audiomodem/y;

    iget-object v0, v0, Lcom/google/android/gms/audiomodem/y;->k:Lcom/google/android/gms/audiomodem/aj;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/audiomodem/aj;->a([BI)V

    .line 362
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/ab;->c:Lcom/google/android/gms/audiomodem/y;

    iget-object v0, v0, Lcom/google/android/gms/audiomodem/y;->b:Lcom/google/android/gms/audiomodem/bb;

    iget-object v1, v0, Lcom/google/android/gms/audiomodem/bb;->a:Landroid/media/AudioRecord;

    invoke-virtual {v1}, Landroid/media/AudioRecord;->release()V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/audiomodem/bb;->a:Landroid/media/AudioRecord;

    .line 363
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/ab;->c:Lcom/google/android/gms/audiomodem/y;

    iget-object v0, v0, Lcom/google/android/gms/audiomodem/y;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/audiomodem/ac;

    .line 364
    invoke-interface {v0}, Lcom/google/android/gms/audiomodem/ac;->a()V

    goto :goto_9

    .line 366
    :cond_e
    const-string v0, "audioModem"

    invoke-static {v0, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 367
    const-string v0, "AudioRecorder: Processor thread exiting"

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    :cond_f
    if-eqz v3, :cond_10

    .line 370
    iget-object v1, p0, Lcom/google/android/gms/audiomodem/ab;->c:Lcom/google/android/gms/audiomodem/y;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v1, Lcom/google/android/gms/audiomodem/y;->m:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/gms/audiomodem/y;->l:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/gms/audiomodem/y;->k:Lcom/google/android/gms/audiomodem/aj;

    new-instance v3, Lcom/google/android/gms/audiomodem/bo;

    iget-object v4, v1, Lcom/google/android/gms/audiomodem/y;->b:Lcom/google/android/gms/audiomodem/bb;

    iget v4, v4, Lcom/google/android/gms/audiomodem/bb;->d:I

    invoke-virtual {v1}, Lcom/google/android/gms/audiomodem/y;->b()I

    move-result v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/audiomodem/bo;-><init>(II)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".wav"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/audiomodem/aj;->a(Lcom/google/android/gms/audiomodem/bo;Ljava/lang/String;)Ljava/io/File;

    new-instance v2, Lcom/google/d/a/a/b;

    invoke-direct {v2}, Lcom/google/d/a/a/b;-><init>()V

    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/d/a/a/b;->e:Ljava/lang/String;

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/d/a/a/b;->f:Ljava/lang/String;

    iget-object v3, v1, Lcom/google/android/gms/audiomodem/y;->b:Lcom/google/android/gms/audiomodem/bb;

    iget v3, v3, Lcom/google/android/gms/audiomodem/bb;->b:I

    invoke-static {v3}, Lcom/google/android/gms/audiomodem/bb;->a(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/d/a/a/b;->g:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/gms/audiomodem/y;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/d/a/a/b;->h:Ljava/lang/Integer;

    iget-object v3, v1, Lcom/google/android/gms/audiomodem/y;->b:Lcom/google/android/gms/audiomodem/bb;

    iget v3, v3, Lcom/google/android/gms/audiomodem/bb;->d:I

    int-to-float v3, v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    iput-object v3, v2, Lcom/google/d/a/a/b;->i:Ljava/lang/Float;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v2, Lcom/google/d/a/a/b;->y:Ljava/lang/Long;

    invoke-static {v2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    iget-object v1, v1, Lcom/google/android/gms/audiomodem/y;->j:Lcom/google/android/gms/audiomodem/ah;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ".metadata"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/audiomodem/ah;->a(Ljava/lang/String;[B)Ljava/io/File;

    .line 371
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/ab;->c:Lcom/google/android/gms/audiomodem/y;

    iget-object v0, v0, Lcom/google/android/gms/audiomodem/y;->k:Lcom/google/android/gms/audiomodem/aj;

    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/aj;->b()V

    .line 375
    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/ab;->c:Lcom/google/android/gms/audiomodem/y;

    iget-object v0, v0, Lcom/google/android/gms/audiomodem/y;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/audiomodem/ap;->a(Landroid/content/Context;)V

    goto/16 :goto_8

    .line 326
    :catch_3
    move-exception v1

    move-object v14, v1

    move-object v1, v0

    move-object v0, v14

    goto/16 :goto_6

    :cond_11
    move-object v0, v1

    goto/16 :goto_7
.end method
