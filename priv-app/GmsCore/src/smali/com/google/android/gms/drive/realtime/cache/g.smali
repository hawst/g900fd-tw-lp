.class final Lcom/google/android/gms/drive/realtime/cache/g;
.super Lcom/google/android/gms/drive/realtime/cache/a;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/drive/realtime/cache/i;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/android/gms/drive/realtime/cache/i;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/realtime/cache/a;-><init>(Lcom/google/android/gms/drive/realtime/cache/n;)V

    .line 34
    iput-object p2, p0, Lcom/google/android/gms/drive/realtime/cache/g;->b:Lcom/google/android/gms/drive/realtime/cache/i;

    .line 35
    return-void
.end method


# virtual methods
.method protected final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8

    .prologue
    .line 40
    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/a;->a:Lcom/google/android/gms/drive/realtime/cache/n;

    .line 41
    iget-object v0, v1, Lcom/google/android/gms/drive/realtime/cache/n;->a:Lcom/google/android/gms/drive/realtime/e;

    iget-object v0, v0, Lcom/google/android/gms/drive/realtime/e;->a:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v2

    .line 42
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/c;->a()Lcom/google/android/gms/drive/realtime/cache/a/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/realtime/cache/a/c;->b(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v3

    .line 43
    sget-object v0, Lcom/google/android/gms/drive/ai;->M:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 44
    if-ge v3, v0, :cond_0

    .line 45
    const-string v1, "CompactSnapshotOperation"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "No need to compact "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " (history length: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; threshold: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    :goto_0
    return-void

    .line 49
    :cond_0
    const-string v0, "CompactSnapshotOperation"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Compacting snapshot for Realtime document: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/drive/realtime/cache/n;->b()Lcom/google/c/a/a/b/a/d;

    move-result-object v0

    iget-object v6, v0, Lcom/google/c/a/a/b/a/d;->c:Lcom/google/c/a/a/b/c/f;
    :try_end_0
    .catch Lcom/google/android/gms/drive/realtime/cache/p; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    new-instance v5, Lcom/google/c/a/a/b/b/a/b/w;

    new-instance v0, Lcom/google/c/a/a/b/b/a/b/u;

    invoke-direct {v0}, Lcom/google/c/a/a/b/b/a/b/u;-><init>()V

    invoke-virtual {v6}, Lcom/google/c/a/a/b/c/f;->e()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/a/a/b/b/a/b/u;->a:Ljava/lang/Iterable;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/b/u;->a()Lcom/google/c/a/a/b/b/a/b/t;

    move-result-object v0

    sget-object v1, Lcom/google/c/a/a/b/b/a/o;->a:Lcom/google/c/a/a/b/b/a/o;

    invoke-direct {v5, v0, v1}, Lcom/google/c/a/a/b/b/a/b/w;-><init>(Lcom/google/c/a/a/b/b/a/b/t;Lcom/google/c/a/a/b/b/a/o;)V

    .line 61
    new-instance v0, Lcom/google/c/a/a/b/c/f;

    invoke-virtual {v6}, Lcom/google/c/a/a/b/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Lcom/google/c/a/a/b/c/f;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Lcom/google/c/a/a/b/c/f;->c()Z

    move-result v3

    invoke-virtual {v6}, Lcom/google/c/a/a/b/c/f;->d()I

    move-result v4

    invoke-interface {v5}, Lcom/google/c/a/a/b/b/a/b;->a()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v6}, Lcom/google/c/a/a/b/c/f;->f()Ljava/util/Collection;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/c/a/a/b/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;ZILjava/util/List;Ljava/util/Collection;Ljava/lang/String;)V

    .line 65
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a()Lcom/google/android/gms/drive/realtime/cache/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/cache/g;->b:Lcom/google/android/gms/drive/realtime/cache/i;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/drive/realtime/cache/i;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 67
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/c;->a()Lcom/google/android/gms/drive/realtime/cache/a/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/realtime/cache/a/c;->c(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v0

    .line 68
    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/a;->a:Lcom/google/android/gms/drive/realtime/cache/n;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/realtime/cache/n;->j()Lcom/google/android/gms/drive/database/model/br;

    move-result-object v1

    const/4 v2, 0x0

    iput v2, v1, Lcom/google/android/gms/drive/database/model/br;->h:I

    .line 69
    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/a;->a:Lcom/google/android/gms/drive/realtime/cache/n;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/realtime/cache/n;->j()Lcom/google/android/gms/drive/database/model/br;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/br;->i()V

    .line 70
    const-string v1, "CompactSnapshotOperation"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Compacted mutation log (merged "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " changes)"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 54
    :catch_0
    move-exception v0

    const-string v0, "CompactSnapshotOperation"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Aborting compaction for Realtime document (invalid cache): "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
