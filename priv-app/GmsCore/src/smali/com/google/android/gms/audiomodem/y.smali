.class final Lcom/google/android/gms/audiomodem/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field b:Lcom/google/android/gms/audiomodem/bb;

.field final c:Ljava/util/Set;

.field volatile d:[B

.field final e:Landroid/os/Handler;

.field volatile f:Z

.field volatile g:Z

.field volatile h:J

.field volatile i:J

.field final j:Lcom/google/android/gms/audiomodem/ah;

.field final k:Lcom/google/android/gms/audiomodem/aj;

.field final l:Ljava/lang/String;

.field final m:Ljava/lang/ThreadLocal;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-boolean v0, p0, Lcom/google/android/gms/audiomodem/y;->f:Z

    .line 49
    iput-boolean v0, p0, Lcom/google/android/gms/audiomodem/y;->g:Z

    .line 51
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/audiomodem/y;->h:J

    .line 60
    new-instance v0, Lcom/google/android/gms/audiomodem/z;

    invoke-direct {v0, p0}, Lcom/google/android/gms/audiomodem/z;-><init>(Lcom/google/android/gms/audiomodem/y;)V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/y;->m:Ljava/lang/ThreadLocal;

    .line 70
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/y;->a:Landroid/content/Context;

    .line 71
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/y;->c:Ljava/util/Set;

    .line 75
    invoke-static {p1}, Lcom/google/android/gms/audiomodem/bb;->a(Landroid/content/Context;)Lcom/google/android/gms/audiomodem/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/y;->b:Lcom/google/android/gms/audiomodem/bb;

    .line 77
    sget-object v0, Lcom/google/android/gms/audiomodem/m;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/audiomodem/y;->a(J)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/gms/audiomodem/y;->i:J

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/y;->b:Lcom/google/android/gms/audiomodem/bb;

    iget v0, v0, Lcom/google/android/gms/audiomodem/bb;->e:I

    int-to-float v1, v0

    sget-object v0, Lcom/google/android/gms/audiomodem/m;->w:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/y;->d:[B

    .line 82
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "processorThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 83
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 84
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/gms/audiomodem/y;->e:Landroid/os/Handler;

    .line 86
    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    invoke-static {}, Lcom/google/android/gms/audiomodem/s;->a()Lcom/google/android/gms/audiomodem/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/s;->b()Lcom/google/whispernet/b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/whispernet/b;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    :goto_0
    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 89
    new-instance v0, Lcom/google/android/gms/audiomodem/ah;

    invoke-static {}, Lcom/google/android/gms/audiomodem/l;->a()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/audiomodem/ah;-><init>(Ljava/io/File;I)V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/y;->j:Lcom/google/android/gms/audiomodem/ah;

    .line 91
    new-instance v0, Lcom/google/android/gms/audiomodem/aj;

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/y;->a:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/audiomodem/l;->a()I

    move-result v3

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/gms/audiomodem/aj;-><init>(Landroid/content/Context;Ljava/io/File;I)V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/y;->k:Lcom/google/android/gms/audiomodem/aj;

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "android_id"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x20

    const/16 v2, 0x5f

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/y;->l:Ljava/lang/String;

    .line 94
    return-void

    .line 86
    :cond_0
    sget-object v0, Lcom/google/android/gms/audiomodem/m;->s:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method private a(J)I
    .locals 5

    .prologue
    .line 168
    long-to-double v0, p1

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    .line 169
    iget-object v2, p0, Lcom/google/android/gms/audiomodem/y;->b:Lcom/google/android/gms/audiomodem/bb;

    iget v2, v2, Lcom/google/android/gms/audiomodem/bb;->d:I

    mul-int/lit8 v2, v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/audiomodem/y;->b()I

    move-result v3

    mul-int/2addr v2, v3

    int-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/google/android/gms/audiomodem/y;->f:Z

    if-eqz v0, :cond_0

    .line 98
    const/4 v0, 0x0

    .line 120
    :goto_0
    return v0

    .line 101
    :cond_0
    const-string v0, "audioModem"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 102
    const-string v0, "AudioRecorder: onGservicesSettingsChanged"

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :cond_1
    sget-object v0, Lcom/google/android/gms/audiomodem/m;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/audiomodem/y;->a(J)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/gms/audiomodem/y;->i:J

    .line 109
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/y;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/audiomodem/bb;->a(Landroid/content/Context;)Lcom/google/android/gms/audiomodem/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/y;->b:Lcom/google/android/gms/audiomodem/bb;

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/y;->b:Lcom/google/android/gms/audiomodem/bb;

    iget v0, v0, Lcom/google/android/gms/audiomodem/bb;->e:I

    int-to-float v1, v0

    sget-object v0, Lcom/google/android/gms/audiomodem/m;->w:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/y;->d:[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :cond_2
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 114
    :catch_0
    move-exception v0

    const-string v0, "audioModem"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 115
    const-string v0, "AudioRecorder: Unable to create RecordingConfiguration with new settings; using old RecordingConfiguration"

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/y;->b:Lcom/google/android/gms/audiomodem/bb;

    iget-boolean v0, v0, Lcom/google/android/gms/audiomodem/bb;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
