.class public final Lcom/google/android/gms/gcm/notification/d;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/gcm/notification/c;

.field public b:Lcom/google/android/gms/gcm/notification/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 346
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 347
    iput-object v0, p0, Lcom/google/android/gms/gcm/notification/d;->a:Lcom/google/android/gms/gcm/notification/c;

    iput-object v0, p0, Lcom/google/android/gms/gcm/notification/d;->b:Lcom/google/android/gms/gcm/notification/b;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/gcm/notification/d;->cachedSize:I

    .line 348
    return-void
.end method

.method public static a([B)Lcom/google/android/gms/gcm/notification/d;
    .locals 1

    .prologue
    .line 418
    new-instance v0, Lcom/google/android/gms/gcm/notification/d;

    invoke-direct {v0}, Lcom/google/android/gms/gcm/notification/d;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/gcm/notification/d;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 371
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 372
    iget-object v1, p0, Lcom/google/android/gms/gcm/notification/d;->a:Lcom/google/android/gms/gcm/notification/c;

    if-eqz v1, :cond_0

    .line 373
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/gcm/notification/d;->a:Lcom/google/android/gms/gcm/notification/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 376
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/gcm/notification/d;->b:Lcom/google/android/gms/gcm/notification/b;

    if-eqz v1, :cond_1

    .line 377
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/gcm/notification/d;->b:Lcom/google/android/gms/gcm/notification/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 380
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 323
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/gcm/notification/d;->a:Lcom/google/android/gms/gcm/notification/c;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/gcm/notification/c;

    invoke-direct {v0}, Lcom/google/android/gms/gcm/notification/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/gcm/notification/d;->a:Lcom/google/android/gms/gcm/notification/c;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/gcm/notification/d;->a:Lcom/google/android/gms/gcm/notification/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/gcm/notification/d;->b:Lcom/google/android/gms/gcm/notification/b;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/gcm/notification/b;

    invoke-direct {v0}, Lcom/google/android/gms/gcm/notification/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/gcm/notification/d;->b:Lcom/google/android/gms/gcm/notification/b;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/gcm/notification/d;->b:Lcom/google/android/gms/gcm/notification/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 360
    iget-object v0, p0, Lcom/google/android/gms/gcm/notification/d;->a:Lcom/google/android/gms/gcm/notification/c;

    if-eqz v0, :cond_0

    .line 361
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/gcm/notification/d;->a:Lcom/google/android/gms/gcm/notification/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 363
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/notification/d;->b:Lcom/google/android/gms/gcm/notification/b;

    if-eqz v0, :cond_1

    .line 364
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/gcm/notification/d;->b:Lcom/google/android/gms/gcm/notification/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 366
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 367
    return-void
.end method
