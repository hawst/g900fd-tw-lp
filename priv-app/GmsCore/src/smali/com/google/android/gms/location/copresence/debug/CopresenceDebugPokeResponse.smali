.class public Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/location/copresence/debug/b;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/gms/location/copresence/debug/b;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/debug/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeResponse;->CREATOR:Lcom/google/android/gms/location/copresence/debug/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeResponse;-><init>(I)V

    .line 38
    return-void
.end method

.method constructor <init>(I)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput p1, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeResponse;->a:I

    .line 34
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeResponse;->a:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeResponse;->CREATOR:Lcom/google/android/gms/location/copresence/debug/b;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 53
    invoke-static {p0, p1}, Lcom/google/android/gms/location/copresence/debug/b;->a(Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeResponse;Landroid/os/Parcel;)V

    .line 54
    return-void
.end method
