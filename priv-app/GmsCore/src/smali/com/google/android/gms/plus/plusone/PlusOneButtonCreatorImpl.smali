.class public final Lcom/google/android/gms/plus/plusone/PlusOneButtonCreatorImpl;
.super Lcom/google/android/gms/plus/internal/m;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/DynamiteApi;
.end annotation

.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/m;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;)Landroid/content/res/Resources;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 98
    :try_start_0
    const-string v1, "com.google.android.gms"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 107
    if-eqz v1, :cond_0

    .line 108
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 110
    :cond_0
    :goto_0
    return-object v0

    .line 102
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xf
    .end annotation

    .prologue
    .line 87
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_0

    .line 88
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0, p0}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_0
    const-string v0, "PlusOneButtonCreatorImpl"

    invoke-static {v0, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/b/l;IILjava/lang/String;)Lcom/google/android/gms/b/l;
    .locals 2

    .prologue
    .line 46
    invoke-static {p1}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 47
    invoke-static {v0}, Lcom/google/android/gms/plus/plusone/PlusOneButtonCreatorImpl;->a(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v1

    .line 48
    if-nez v1, :cond_0

    .line 49
    const-string v1, "Could not load GMS resources!"

    invoke-static {v1}, Lcom/google/android/gms/plus/plusone/PlusOneButtonCreatorImpl;->a(Ljava/lang/String;)V

    .line 52
    :cond_0
    new-instance v1, Lcom/google/android/gms/plus/plusone/c;

    invoke-direct {v1, v0, p2, p3, p4}, Lcom/google/android/gms/plus/plusone/c;-><init>(Landroid/content/Context;IILjava/lang/String;)V

    .line 53
    invoke-static {v1}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/b/l;IILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/b/l;
    .locals 6

    .prologue
    .line 74
    invoke-static {p1}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 75
    invoke-static {v1}, Lcom/google/android/gms/plus/plusone/PlusOneButtonCreatorImpl;->a(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v0

    .line 76
    if-nez v0, :cond_0

    .line 77
    const-string v0, "Could not load GMS resources!"

    invoke-static {v0}, Lcom/google/android/gms/plus/plusone/PlusOneButtonCreatorImpl;->a(Ljava/lang/String;)V

    .line 80
    :cond_0
    new-instance v0, Lcom/google/android/gms/plus/plusone/e;

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/plusone/e;-><init>(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method
