.class final Lcom/google/android/gms/wallet/service/ow/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/c/i;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/wallet/c/i;

.field private c:Lcom/google/android/apps/common/a/a/i;

.field private d:Lcom/google/android/apps/common/a/a/h;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/i;->a:Landroid/content/Context;

    .line 246
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/i;->b:Lcom/google/android/gms/wallet/c/i;

    .line 247
    new-instance v0, Lcom/google/android/apps/common/a/a/i;

    invoke-direct {v0, p3}, Lcom/google/android/apps/common/a/a/i;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/i;->c:Lcom/google/android/apps/common/a/a/i;

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/i;->c:Lcom/google/android/apps/common/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/common/a/a/i;->a()Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/i;->d:Lcom/google/android/apps/common/a/a/h;

    .line 249
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 237
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/service/ow/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;)V

    return-void
.end method

.method private a(ILjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 291
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/i;->c:Lcom/google/android/apps/common/a/a/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/i;->d:Lcom/google/android/apps/common/a/a/h;

    if-nez v0, :cond_1

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 294
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "result_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 297
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 298
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/i;->c:Lcom/google/android/apps/common/a/a/i;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/i;->d:Lcom/google/android/apps/common/a/a/h;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/common/a/a/i;->a(Lcom/google/android/apps/common/a/a/h;[Ljava/lang/String;)Z

    .line 302
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/i;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/i;->c:Lcom/google/android/apps/common/a/a/i;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/apps/common/a/a/i;)V

    .line 303
    iput-object v5, p0, Lcom/google/android/gms/wallet/service/ow/i;->c:Lcom/google/android/apps/common/a/a/i;

    .line 304
    iput-object v5, p0, Lcom/google/android/gms/wallet/service/ow/i;->d:Lcom/google/android/apps/common/a/a/h;

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/i;->b:Lcom/google/android/gms/wallet/c/i;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/wallet/c/i;->a(ILandroid/os/Bundle;)V

    .line 280
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/service/ow/i;->a(ILjava/lang/String;)V

    .line 281
    return-void
.end method

.method public final a(ILcom/google/android/gms/wallet/FullWallet;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/i;->b:Lcom/google/android/gms/wallet/c/i;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/wallet/c/i;->a(ILcom/google/android/gms/wallet/FullWallet;Landroid/os/Bundle;)V

    .line 267
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/service/ow/i;->a(ILjava/lang/String;)V

    .line 268
    return-void
.end method

.method public final a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/i;->b:Lcom/google/android/gms/wallet/c/i;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/wallet/c/i;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    .line 260
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/service/ow/i;->a(ILjava/lang/String;)V

    .line 261
    return-void
.end method

.method public final a(IZLandroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/i;->b:Lcom/google/android/gms/wallet/c/i;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/wallet/c/i;->a(IZLandroid/os/Bundle;)V

    .line 274
    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/service/ow/i;->a(ILjava/lang/String;)V

    .line 275
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/wallet/firstparty/GetInstrumentsResponse;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/i;->b:Lcom/google/android/gms/wallet/c/i;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/wallet/c/i;->a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/wallet/firstparty/GetInstrumentsResponse;Landroid/os/Bundle;)V

    .line 287
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/service/ow/i;->a(ILjava/lang/String;)V

    .line 288
    return-void
.end method

.method public final asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/i;->b:Lcom/google/android/gms/wallet/c/i;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/c/i;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method
