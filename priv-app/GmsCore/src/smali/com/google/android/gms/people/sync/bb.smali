.class abstract Lcom/google/android/gms/people/sync/bb;
.super Lcom/google/android/gms/people/sync/k;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Map;

.field private final b:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/sync/j;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/util/Map;Z)V
    .locals 0

    .prologue
    .line 2432
    invoke-direct {p0, p2, p1}, Lcom/google/android/gms/people/sync/k;-><init>(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Lcom/google/android/gms/people/sync/j;)V

    .line 2433
    iput-object p3, p0, Lcom/google/android/gms/people/sync/bb;->a:Ljava/util/Map;

    .line 2434
    iput-boolean p4, p0, Lcom/google/android/gms/people/sync/bb;->b:Z

    .line 2435
    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;ILjava/lang/Object;)V
.end method

.method public final a(Ljava/lang/Object;ILjava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2439
    if-eqz p2, :cond_1

    if-eq p2, v1, :cond_1

    const/4 v0, 0x2

    if-eq p2, v0, :cond_1

    .line 2442
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2443
    const-string v0, "PeopleSync"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported container="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2464
    :cond_0
    :goto_0
    return-void

    .line 2447
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/people/sync/bb;->b:Z

    if-eqz v0, :cond_2

    if-ne p2, v1, :cond_0

    .line 2452
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/sync/bb;->a:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2453
    if-nez v0, :cond_3

    .line 2454
    if-ne p2, v1, :cond_0

    .line 2458
    const-string v0, "PeopleSync"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown container="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2463
    :cond_3
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/people/sync/bb;->a(Ljava/lang/Object;ILjava/lang/Object;)V

    goto :goto_0
.end method
