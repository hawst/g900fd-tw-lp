.class public final Lcom/google/android/gms/plus/service/a/x;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field a:Z

.field private final b:Lcom/google/android/gms/common/server/ClientContext;

.field private final c:Lcom/google/android/gms/plus/internal/c;

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;IIILjava/lang/String;Lcom/google/android/gms/plus/internal/c;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/service/a/x;->a:Z

    .line 50
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/x;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 51
    iput p2, p0, Lcom/google/android/gms/plus/service/a/x;->d:I

    .line 52
    iput p3, p0, Lcom/google/android/gms/plus/service/a/x;->e:I

    .line 53
    iput p4, p0, Lcom/google/android/gms/plus/service/a/x;->f:I

    .line 54
    iput-object p5, p0, Lcom/google/android/gms/plus/service/a/x;->g:Ljava/lang/String;

    .line 55
    iput-object p6, p0, Lcom/google/android/gms/plus/service/a/x;->c:Lcom/google/android/gms/plus/internal/c;

    .line 56
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 60
    iget-boolean v0, p0, Lcom/google/android/gms/plus/service/a/x;->a:Z

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/x;->c:Lcom/google/android/gms/plus/internal/c;

    const/16 v1, 0xd

    invoke-static {v1, v7}, Lcom/google/android/gms/common/data/DataHolder;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v7}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    .line 63
    const-string v0, "ListPeople"

    const-string v1, "ListPeople canceled"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    :goto_0
    return-void

    .line 67
    :cond_0
    const-string v0, "ListPeople"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    .line 69
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/x;->g:Ljava/lang/String;

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/gms/plus/service/a/x;->d:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    iget v1, p0, Lcom/google/android/gms/plus/service/a/x;->f:I

    if-gez v1, :cond_2

    .line 72
    if-eqz v0, :cond_1

    .line 73
    const-string v0, "ListPeople"

    const-string v1, "Running people.list locally"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/x;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/x;->b:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/gms/plus/service/a/x;->e:I

    invoke-static {p1, v0, v1, v2, v3}, Lcom/google/android/gms/people/pub/b;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZI)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v6

    .line 81
    if-eqz v6, :cond_2

    .line 82
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {p1, v1}, Lcom/google/android/gms/people/pub/b;->a(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 85
    sget-object v0, Lcom/google/android/gms/plus/c/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-gez v0, :cond_2

    .line 87
    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/gms/plus/a/m;->l:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/common/analytics/y;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/x;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v5

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/x;->c:Lcom/google/android/gms/plus/internal/c;

    const/4 v1, 0x0

    invoke-interface {v0, v6, v1}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 102
    :catch_0
    move-exception v0

    .line 103
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 104
    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v8, v0, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 105
    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/x;->c:Lcom/google/android/gms/plus/internal/c;

    invoke-static {v9, v1}, Lcom/google/android/gms/common/data/DataHolder;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v7}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0

    .line 98
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/x;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget v2, p0, Lcom/google/android/gms/plus/service/a/x;->d:I

    iget v3, p0, Lcom/google/android/gms/plus/service/a/x;->e:I

    iget v0, p0, Lcom/google/android/gms/plus/service/a/x;->f:I

    if-gez v0, :cond_3

    const/16 v4, 0x64

    :goto_1
    iget-object v5, p0, Lcom/google/android/gms/plus/service/a/x;->g:Ljava/lang/String;

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/plus/b/b;->a(Lcom/google/android/gms/common/server/ClientContext;IIILjava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 101
    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/x;->c:Lcom/google/android/gms/plus/internal/c;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v0, v1}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/google/android/gms/auth/ae; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_0

    .line 109
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/x;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 110
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/x;->c:Lcom/google/android/gms/plus/internal/c;

    invoke-static {v9, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-interface {v1, v0, v7}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 98
    :cond_3
    :try_start_2
    iget v4, p0, Lcom/google/android/gms/plus/service/a/x;->f:I
    :try_end_2
    .catch Lcom/google/android/gms/auth/ae; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 113
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/x;->c:Lcom/google/android/gms/plus/internal/c;

    const/4 v1, 0x7

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v7}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/x;->c:Lcom/google/android/gms/plus/internal/c;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/x;->c:Lcom/google/android/gms/plus/internal/c;

    const/16 v1, 0x8

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    .line 126
    :cond_0
    return-void
.end method
