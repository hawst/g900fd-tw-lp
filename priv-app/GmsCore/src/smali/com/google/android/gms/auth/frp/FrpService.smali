.class public Lcom/google/android/gms/auth/frp/FrpService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;


# instance fields
.field private b:Lcom/google/android/gms/auth/frp/f;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "GLSActivity"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "FactoryResetProtectionActivity"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/frp/FrpService;->a:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 32
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/gms/auth/frp/FrpService;->b:Lcom/google/android/gms/auth/frp/f;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/frp/f;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 99
    new-instance v0, Lcom/google/android/gms/auth/frp/f;

    new-instance v1, Lcom/google/android/gms/auth/a/k;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/a/k;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/auth/frp/f;-><init>(Lcom/google/android/gms/auth/frp/FrpService;Lcom/google/android/gms/auth/a/k;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/frp/FrpService;->b:Lcom/google/android/gms/auth/frp/f;

    .line 100
    return-void
.end method
