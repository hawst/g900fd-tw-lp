.class public final Lcom/google/android/gms/wearable/node/bb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wearable/d/c;


# instance fields
.field final a:Ljava/lang/String;

.field final b:Landroid/content/Context;

.field public c:Lcom/google/android/gms/wearable/node/ba;

.field final d:Ljava/util/ArrayList;

.field final e:Ljava/util/HashMap;

.field f:Ljava/util/LinkedList;

.field private final g:Lcom/google/android/gms/wearable/node/bg;

.field private final h:Ljava/lang/Object;

.field private i:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bb;->d:Ljava/util/ArrayList;

    .line 81
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bb;->h:Ljava/lang/Object;

    .line 83
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bb;->e:Ljava/util/HashMap;

    .line 149
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bb;->f:Ljava/util/LinkedList;

    .line 142
    iput-object p2, p0, Lcom/google/android/gms/wearable/node/bb;->a:Ljava/lang/String;

    .line 143
    iput-object p1, p0, Lcom/google/android/gms/wearable/node/bb;->b:Landroid/content/Context;

    .line 144
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "WearableTransport.WriteWatchdogHandler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 145
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 146
    new-instance v1, Lcom/google/android/gms/wearable/node/bg;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/wearable/node/bg;-><init>(Lcom/google/android/gms/wearable/node/bb;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/gms/wearable/node/bb;->g:Lcom/google/android/gms/wearable/node/bg;

    .line 147
    return-void
.end method

.method public static a(Lcom/google/android/gms/wearable/c/m;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/android/gms/wearable/c/m;->a:Lcom/google/android/gms/wearable/c/g;

    if-eqz v0, :cond_0

    const-string v0, "ChannelControl"

    .line 468
    :goto_0
    return-object v0

    .line 457
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/c/m;->b:Lcom/google/android/gms/wearable/c/h;

    if-eqz v0, :cond_1

    const-string v0, "ChannelData"

    goto :goto_0

    .line 458
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wearable/c/m;->c:Lcom/google/android/gms/wearable/c/f;

    if-eqz v0, :cond_2

    const-string v0, "ChannelCloseResponse"

    goto :goto_0

    .line 459
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wearable/c/m;->d:Lcom/google/android/gms/wearable/c/q;

    if-eqz v0, :cond_3

    const-string v0, "SetAsset"

    goto :goto_0

    .line 460
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wearable/c/m;->e:Lcom/google/android/gms/wearable/c/a;

    if-eqz v0, :cond_4

    const-string v0, "AckAsset"

    goto :goto_0

    .line 461
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wearable/c/m;->f:Lcom/google/android/gms/wearable/c/j;

    if-eqz v0, :cond_5

    const-string v0, "FetchAsset"

    goto :goto_0

    .line 462
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wearable/c/m;->g:Lcom/google/android/gms/wearable/c/i;

    if-eqz v0, :cond_6

    const-string v0, "Connect"

    goto :goto_0

    .line 463
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/wearable/c/m;->h:Lcom/google/android/gms/wearable/c/s;

    if-eqz v0, :cond_7

    const-string v0, "SyncStart"

    goto :goto_0

    .line 464
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/wearable/c/m;->i:Lcom/google/android/gms/wearable/c/r;

    if-eqz v0, :cond_8

    const-string v0, "SetDataItem"

    goto :goto_0

    .line 465
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/wearable/c/m;->j:Lcom/google/android/gms/wearable/c/p;

    if-eqz v0, :cond_9

    const-string v0, "RpcRequest"

    goto :goto_0

    .line 466
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/wearable/c/m;->l:Lcom/google/android/gms/wearable/c/k;

    if-eqz v0, :cond_a

    const-string v0, "FilePiece"

    goto :goto_0

    .line 467
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/wearable/c/m;->k:Lcom/google/android/gms/wearable/c/l;

    if-eqz v0, :cond_b

    const-string v0, "Heartbeat"

    goto :goto_0

    .line 468
    :cond_b
    const-string v0, "UnknownType"

    goto :goto_0
.end method

.method private static a(I)V
    .locals 2

    .prologue
    .line 321
    new-instance v0, Lcom/google/k/f/n;

    invoke-direct {v0}, Lcom/google/k/f/n;-><init>()V

    .line 322
    new-instance v1, Lcom/google/k/f/m;

    invoke-direct {v1}, Lcom/google/k/f/m;-><init>()V

    iput-object v1, v0, Lcom/google/k/f/n;->f:Lcom/google/k/f/m;

    .line 323
    iget-object v1, v0, Lcom/google/k/f/n;->f:Lcom/google/k/f/m;

    iput p0, v1, Lcom/google/k/f/m;->a:I

    .line 324
    sget-object v1, Lcom/google/android/gms/wearable/b/a;->a:Lcom/google/android/gms/wearable/b/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wearable/b/a;->a(Lcom/google/k/f/n;)V

    .line 325
    return-void
.end method

.method private a(Lcom/google/android/gms/wearable/node/ab;)V
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/node/z;

    .line 352
    invoke-interface {v0, p1}, Lcom/google/android/gms/wearable/node/z;->a(Lcom/google/android/gms/wearable/node/aa;)V

    goto :goto_0

    .line 354
    :cond_0
    return-void
.end method

.method private b(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/google/android/gms/wearable/node/be;Lcom/google/android/gms/wearable/ConnectionConfiguration;)Lcom/google/android/gms/wearable/c/i;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v8, 0x3

    const/4 v2, 0x1

    .line 253
    new-instance v4, Lcom/google/android/gms/wearable/c/m;

    invoke-direct {v4}, Lcom/google/android/gms/wearable/c/m;-><init>()V

    .line 254
    new-instance v0, Lcom/google/android/gms/wearable/c/i;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/c/i;-><init>()V

    iput-object v0, v4, Lcom/google/android/gms/wearable/c/m;->g:Lcom/google/android/gms/wearable/c/i;

    .line 255
    iget-object v0, v4, Lcom/google/android/gms/wearable/c/m;->g:Lcom/google/android/gms/wearable/c/i;

    iget-object v5, p0, Lcom/google/android/gms/wearable/node/bb;->c:Lcom/google/android/gms/wearable/node/ba;

    invoke-interface {v5}, Lcom/google/android/gms/wearable/node/ba;->b()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/google/android/gms/wearable/c/i;->a:Ljava/lang/String;

    .line 256
    iget-object v0, v4, Lcom/google/android/gms/wearable/c/m;->g:Lcom/google/android/gms/wearable/c/i;

    iget-object v5, p0, Lcom/google/android/gms/wearable/node/bb;->c:Lcom/google/android/gms/wearable/node/ba;

    invoke-interface {v5}, Lcom/google/android/gms/wearable/node/ba;->b()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/google/android/gms/wearable/c/i;->b:Ljava/lang/String;

    .line 257
    iget-object v5, v4, Lcom/google/android/gms/wearable/c/m;->g:Lcom/google/android/gms/wearable/c/i;

    sget-object v0, Lcom/google/android/gms/common/util/e;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, v5, Lcom/google/android/gms/wearable/c/i;->c:J

    .line 258
    iget-object v0, v4, Lcom/google/android/gms/wearable/c/m;->g:Lcom/google/android/gms/wearable/c/i;

    iput v2, v0, Lcom/google/android/gms/wearable/c/i;->d:I

    .line 259
    iget-object v0, v4, Lcom/google/android/gms/wearable/c/m;->g:Lcom/google/android/gms/wearable/c/i;

    iput v3, v0, Lcom/google/android/gms/wearable/c/i;->e:I

    .line 262
    iget-object v0, v4, Lcom/google/android/gms/wearable/c/m;->g:Lcom/google/android/gms/wearable/c/i;

    iput v8, v0, Lcom/google/android/gms/wearable/c/i;->f:I

    .line 263
    invoke-static {v4}, Lcom/google/android/gms/wearable/node/bh;->a(Lcom/google/android/gms/wearable/c/m;)Lcom/google/android/gms/wearable/c/n;

    move-result-object v0

    .line 266
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/wearable/node/bh;->a()Lcom/google/android/gms/wearable/node/bi;

    move-result-object v4

    .line 267
    invoke-static {v4, p2, v0, p3}, Lcom/google/android/gms/wearable/node/bh;->a(Lcom/google/android/gms/wearable/node/bi;Ljava/io/OutputStream;Lcom/google/android/gms/wearable/c/n;Lcom/google/android/gms/wearable/node/be;)I

    .line 268
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/c/n;->a()Lcom/google/android/gms/wearable/c/n;

    .line 269
    invoke-static {v4, p1, v0, p3}, Lcom/google/android/gms/wearable/node/bh;->a(Lcom/google/android/gms/wearable/node/bi;Ljava/io/InputStream;Lcom/google/android/gms/wearable/c/n;Lcom/google/android/gms/wearable/node/be;)I

    .line 270
    invoke-static {v0}, Lcom/google/android/gms/wearable/node/bh;->a(Lcom/google/android/gms/wearable/c/n;)Lcom/google/android/gms/wearable/c/m;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 278
    iget-object v0, v4, Lcom/google/android/gms/wearable/c/m;->g:Lcom/google/android/gms/wearable/c/i;

    if-nez v0, :cond_1

    .line 280
    const-string v0, "wearable"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "error, peer didn\'t start with a connect message, found: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/google/android/gms/wearable/node/bb;->a(Lcom/google/android/gms/wearable/c/m;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    invoke-static {v8}, Lcom/google/android/gms/wearable/node/bb;->a(I)V

    move-object v0, v1

    .line 317
    :goto_0
    return-object v0

    .line 272
    :catch_0
    move-exception v0

    const-string v0, "wearable"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    const-string v0, "wearable"

    const-string v2, "error while connecting to peer"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    :cond_0
    invoke-static {v8}, Lcom/google/android/gms/wearable/node/bb;->a(I)V

    move-object v0, v1

    .line 276
    goto :goto_0

    .line 287
    :cond_1
    invoke-virtual {p4}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->d()I

    move-result v0

    const/4 v5, 0x2

    if-ne v0, v5, :cond_2

    invoke-virtual {p4}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->c()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 290
    invoke-static {}, Lcom/google/android/gms/wearable/service/h;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v5, "client_node_id"

    invoke-interface {v0, v5, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 291
    if-nez v0, :cond_3

    .line 292
    invoke-static {}, Lcom/google/android/gms/wearable/service/h;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, v4, Lcom/google/android/gms/wearable/c/m;->g:Lcom/google/android/gms/wearable/c/i;

    iget-object v1, v1, Lcom/google/android/gms/wearable/c/i;->a:Ljava/lang/String;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v5, "client_node_id"

    invoke-interface {v0, v5, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 298
    :cond_2
    iget-object v0, v4, Lcom/google/android/gms/wearable/c/m;->g:Lcom/google/android/gms/wearable/c/i;

    iget v0, v0, Lcom/google/android/gms/wearable/c/i;->d:I

    iget-object v1, v4, Lcom/google/android/gms/wearable/c/m;->g:Lcom/google/android/gms/wearable/c/i;

    iget v1, v1, Lcom/google/android/gms/wearable/c/i;->e:I

    if-lez v0, :cond_5

    if-lt v2, v1, :cond_4

    move v0, v2

    :goto_1
    if-nez v0, :cond_7

    .line 300
    const-string v0, "wearable"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error: wire protocol version mismatch - our version: 1, our minimum supported version: 0; peer version: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v4, Lcom/google/android/gms/wearable/c/m;->g:Lcom/google/android/gms/wearable/c/i;

    iget v2, v2, Lcom/google/android/gms/wearable/c/i;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", peer minimum supported version: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v4, Lcom/google/android/gms/wearable/c/m;->g:Lcom/google/android/gms/wearable/c/i;

    iget v2, v2, Lcom/google/android/gms/wearable/c/i;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    invoke-static {v8}, Lcom/google/android/gms/wearable/node/bb;->a(I)V

    .line 306
    new-instance v0, Lcom/google/android/gms/wearable/node/bk;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/node/bk;-><init>()V

    throw v0

    .line 293
    :cond_3
    iget-object v1, v4, Lcom/google/android/gms/wearable/c/m;->g:Lcom/google/android/gms/wearable/c/i;

    iget-object v1, v1, Lcom/google/android/gms/wearable/c/i;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 294
    new-instance v0, Lcom/google/android/gms/wearable/node/i;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/node/i;-><init>()V

    throw v0

    :cond_4
    move v0, v3

    .line 298
    goto :goto_1

    :cond_5
    if-ltz v0, :cond_6

    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v3

    goto :goto_1

    .line 309
    :cond_7
    const-string v0, "wearable"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Peer handshake connect succeeded, peer version: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v4, Lcom/google/android/gms/wearable/c/m;->g:Lcom/google/android/gms/wearable/c/i;

    iget v3, v3, Lcom/google/android/gms/wearable/c/i;->d:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    invoke-static {}, Lcom/google/android/gms/wearable/service/h;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, v4, Lcom/google/android/gms/wearable/c/m;->g:Lcom/google/android/gms/wearable/c/i;

    iget-wide v6, v1, Lcom/google/android/gms/wearable/c/i;->c:J

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "peer_android_id"

    invoke-interface {v0, v1, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 315
    invoke-static {v2}, Lcom/google/android/gms/wearable/node/bb;->a(I)V

    .line 317
    iget-object v0, v4, Lcom/google/android/gms/wearable/c/m;->g:Lcom/google/android/gms/wearable/c/i;

    goto/16 :goto_0
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/node/z;

    .line 358
    invoke-interface {v0, p1}, Lcom/google/android/gms/wearable/node/z;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 360
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/wearable/node/be;
    .locals 2

    .prologue
    .line 245
    new-instance v0, Lcom/google/android/gms/wearable/node/be;

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bb;->g:Lcom/google/android/gms/wearable/node/bg;

    invoke-direct {v0, v1}, Lcom/google/android/gms/wearable/node/be;-><init>(Landroid/os/Handler;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wearable/ConnectionConfiguration;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 807
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bb;->e:Ljava/util/HashMap;

    monitor-enter v2

    .line 808
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bb;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 809
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 810
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/node/bd;

    .line 811
    iget-object v4, v0, Lcom/google/android/gms/wearable/node/bd;->a:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    if-eqz v4, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/wearable/node/bd;->a:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 812
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 816
    :goto_0
    return-object v1

    .line 815
    :cond_1
    monitor-exit v2

    .line 816
    const/4 v1, 0x0

    goto :goto_0

    .line 815
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/util/ad;ZZ)V
    .locals 3

    .prologue
    .line 420
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->a()V

    .line 421
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bb;->e:Ljava/util/HashMap;

    monitor-enter v1

    .line 422
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bb;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/node/bd;

    .line 423
    iget-object v0, v0, Lcom/google/android/gms/wearable/node/bd;->b:Lcom/google/android/gms/wearable/node/ab;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/wearable/node/ab;->a(Lcom/google/android/gms/common/util/ad;ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 425
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 427
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->println()V

    .line 428
    const-string v0, "connection stats"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 429
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->a()V

    .line 430
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bb;->f:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_1

    .line 431
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bb;->f:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/node/be;

    .line 432
    invoke-virtual {v0, p1}, Lcom/google/android/gms/wearable/node/be;->a(Lcom/google/android/gms/common/util/ad;)V

    .line 430
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 434
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->b()V

    .line 436
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->b()V

    .line 437
    return-void
.end method

.method public final a(Lcom/google/android/gms/wearable/node/z;)V
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 364
    return-void
.end method

.method public final a(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/google/android/gms/wearable/node/be;Lcom/google/android/gms/wearable/ConnectionConfiguration;)V
    .locals 14

    .prologue
    .line 166
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bb;->h:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bb;->i:Ljava/util/concurrent/ExecutorService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bb;->i:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    new-instance v1, Lcom/google/android/gms/wearable/node/ay;

    invoke-direct {v1}, Lcom/google/android/gms/wearable/node/ay;-><init>()V

    invoke-static {v1}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wearable/node/bb;->i:Ljava/util/concurrent/ExecutorService;

    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    move-object/from16 v0, p3

    iput-object v1, v0, Lcom/google/android/gms/wearable/node/be;->a:Ljava/lang/Thread;

    .line 169
    const/4 v10, 0x0

    .line 170
    const/4 v9, 0x0

    .line 171
    const/4 v8, 0x0

    .line 174
    :try_start_1
    iget-object v11, p0, Lcom/google/android/gms/wearable/node/bb;->e:Ljava/util/HashMap;

    monitor-enter v11
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 175
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bb;->f:Ljava/util/LinkedList;

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->push(Ljava/lang/Object;)V

    .line 177
    invoke-direct/range {p0 .. p4}, Lcom/google/android/gms/wearable/node/bb;->b(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/google/android/gms/wearable/node/be;Lcom/google/android/gms/wearable/ConnectionConfiguration;)Lcom/google/android/gms/wearable/c/i;

    move-result-object v5

    .line 178
    if-nez v5, :cond_3

    .line 179
    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 226
    const-string v1, "wearable"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 227
    const-string v1, "wearable"

    const-string v2, "network processing loop is finished"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :cond_2
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/wearable/node/be;->a()V

    .line 241
    :goto_0
    return-void

    .line 166
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 182
    :cond_3
    :try_start_3
    const-string v1, "wearable"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 183
    const-string v1, "wearable"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "connected to node "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v5, Lcom/google/android/gms/wearable/c/i;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    :cond_4
    new-instance v1, Lcom/google/android/gms/wearable/node/ab;

    const-string v2, "wearable"

    const-string v3, "WearableTransport"

    const/16 v7, 0x3000

    move-object/from16 v4, p2

    move-object/from16 v6, p3

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/wearable/node/ab;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;Lcom/google/android/gms/wearable/c/i;Lcom/google/android/gms/wearable/node/be;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 187
    :try_start_4
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bb;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/google/android/gms/wearable/node/bc;

    iget-object v4, v5, Lcom/google/android/gms/wearable/c/i;->a:Ljava/lang/String;

    invoke-direct {v3, p0, v4, v1}, Lcom/google/android/gms/wearable/node/bc;-><init>(Lcom/google/android/gms/wearable/node/bb;Ljava/lang/String;Ljava/util/concurrent/Callable;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_5

    move-result-object v6

    .line 189
    :try_start_5
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bb;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/google/android/gms/wearable/node/bc;

    iget-object v4, v5, Lcom/google/android/gms/wearable/c/i;->a:Ljava/lang/String;

    new-instance v7, Lcom/google/android/gms/wearable/node/bf;

    move-object/from16 v0, p3

    invoke-direct {v7, p0, p1, v5, v0}, Lcom/google/android/gms/wearable/node/bf;-><init>(Lcom/google/android/gms/wearable/node/bb;Ljava/io/InputStream;Lcom/google/android/gms/wearable/c/i;Lcom/google/android/gms/wearable/node/be;)V

    invoke-direct {v3, p0, v4, v7}, Lcom/google/android/gms/wearable/node/bc;-><init>(Lcom/google/android/gms/wearable/node/bb;Ljava/lang/String;Ljava/util/concurrent/Callable;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_6

    move-result-object v7

    .line 191
    :try_start_6
    iget-object v10, p0, Lcom/google/android/gms/wearable/node/bb;->e:Ljava/util/HashMap;

    iget-object v12, v5, Lcom/google/android/gms/wearable/c/i;->a:Ljava/lang/String;

    new-instance v2, Lcom/google/android/gms/wearable/node/bd;

    move-object v3, p0

    move-object/from16 v4, p4

    move-object v5, v1

    move-object v8, p1

    move-object/from16 v9, p2

    invoke-direct/range {v2 .. v9}, Lcom/google/android/gms/wearable/node/bd;-><init>(Lcom/google/android/gms/wearable/node/bb;Lcom/google/android/gms/wearable/ConnectionConfiguration;Lcom/google/android/gms/wearable/node/ab;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/io/InputStream;Ljava/io/OutputStream;)V

    invoke-virtual {v10, v12, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    invoke-direct {p0, v1}, Lcom/google/android/gms/wearable/node/bb;->a(Lcom/google/android/gms/wearable/node/ab;)V

    .line 194
    monitor-exit v11
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_7

    .line 196
    :try_start_7
    const-string v2, "wearable"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 197
    const-string v2, "wearable"

    const-string v3, "blocking until network processing loop finishes..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 204
    :cond_5
    :try_start_8
    invoke-interface {v6}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_8
    .catch Ljava/util/concurrent/CancellationException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 213
    :goto_1
    :try_start_9
    invoke-interface {v7}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_9
    .catch Ljava/util/concurrent/CancellationException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 226
    :goto_2
    const-string v2, "wearable"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 227
    const-string v2, "wearable"

    const-string v3, "network processing loop is finished"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :cond_6
    invoke-virtual {v1}, Lcom/google/android/gms/wearable/node/ab;->a()Lcom/google/android/gms/wearable/c/i;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/wearable/c/i;->a:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/gms/wearable/node/bb;->b(Ljava/lang/String;)V

    .line 234
    if-eqz v6, :cond_7

    .line 235
    const/4 v1, 0x1

    invoke-interface {v6, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 237
    :cond_7
    if-eqz v7, :cond_8

    .line 238
    const/4 v1, 0x1

    invoke-interface {v7, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 240
    :cond_8
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/wearable/node/be;->a()V

    goto/16 :goto_0

    .line 194
    :catchall_1
    move-exception v1

    move-object v7, v8

    move-object v6, v9

    move-object v2, v10

    :goto_3
    :try_start_a
    monitor-exit v11

    throw v1
    :try_end_a
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 220
    :catch_0
    move-exception v1

    .line 221
    :goto_4
    :try_start_b
    const-string v3, "wearable"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 222
    const-string v3, "wearable"

    const-string v4, "Writer or reader thread threw an exception"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 224
    :cond_9
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "worker thread exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/node/be;->a(Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    .line 226
    const-string v1, "wearable"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 227
    const-string v1, "wearable"

    const-string v3, "network processing loop is finished"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :cond_a
    if-eqz v2, :cond_b

    .line 231
    invoke-virtual {v2}, Lcom/google/android/gms/wearable/node/ab;->a()Lcom/google/android/gms/wearable/c/i;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/wearable/c/i;->a:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/gms/wearable/node/bb;->b(Ljava/lang/String;)V

    .line 234
    :cond_b
    if-eqz v6, :cond_c

    .line 235
    const/4 v1, 0x1

    invoke-interface {v6, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 237
    :cond_c
    if-eqz v7, :cond_d

    .line 238
    const/4 v1, 0x1

    invoke-interface {v7, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 240
    :cond_d
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/wearable/node/be;->a()V

    goto/16 :goto_0

    .line 206
    :catch_1
    move-exception v2

    :try_start_c
    const-string v2, "wearable"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 207
    const-string v2, "wearable"

    const-string v3, "Writer thread was cancelled."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :cond_e
    const-string v2, "reader thread canceled"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wearable/node/be;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 220
    :catch_2
    move-exception v2

    move-object v13, v2

    move-object v2, v1

    move-object v1, v13

    goto :goto_4

    .line 215
    :catch_3
    move-exception v2

    const-string v2, "wearable"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 216
    const-string v2, "wearable"

    const-string v3, "Reader thread was cancelled."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    :cond_f
    const-string v2, "reader thread canceled"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wearable/node/be;->a(Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    goto/16 :goto_2

    .line 226
    :catchall_2
    move-exception v2

    move-object v13, v2

    move-object v2, v1

    move-object v1, v13

    :goto_5
    const-string v3, "wearable"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 227
    const-string v3, "wearable"

    const-string v4, "network processing loop is finished"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :cond_10
    if-eqz v2, :cond_11

    .line 231
    invoke-virtual {v2}, Lcom/google/android/gms/wearable/node/ab;->a()Lcom/google/android/gms/wearable/c/i;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/wearable/c/i;->a:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/google/android/gms/wearable/node/bb;->b(Ljava/lang/String;)V

    .line 234
    :cond_11
    if-eqz v6, :cond_12

    .line 235
    const/4 v2, 0x1

    invoke-interface {v6, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 237
    :cond_12
    if-eqz v7, :cond_13

    .line 238
    const/4 v2, 0x1

    invoke-interface {v7, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 240
    :cond_13
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/wearable/node/be;->a()V

    throw v1

    .line 226
    :catchall_3
    move-exception v1

    move-object v7, v8

    move-object v6, v9

    move-object v2, v10

    goto :goto_5

    :catchall_4
    move-exception v1

    goto :goto_5

    .line 220
    :catch_4
    move-exception v1

    move-object v7, v8

    move-object v6, v9

    move-object v2, v10

    goto/16 :goto_4

    .line 194
    :catchall_5
    move-exception v2

    move-object v7, v8

    move-object v6, v9

    move-object v13, v2

    move-object v2, v1

    move-object v1, v13

    goto/16 :goto_3

    :catchall_6
    move-exception v2

    move-object v7, v8

    move-object v13, v2

    move-object v2, v1

    move-object v1, v13

    goto/16 :goto_3

    :catchall_7
    move-exception v2

    move-object v13, v2

    move-object v2, v1

    move-object v1, v13

    goto/16 :goto_3
.end method

.method final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x3

    .line 368
    const-string v0, "wearable"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    const-string v0, "wearable"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCallableEnded: nodeId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bb;->e:Ljava/util/HashMap;

    monitor-enter v1

    .line 372
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bb;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/node/bd;

    .line 373
    if-nez v0, :cond_1

    .line 374
    const-string v0, "wearable"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error getting connection reference for nodeId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". A thread may have leaked!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    monitor-exit v1

    .line 415
    :goto_0
    return-void

    .line 379
    :cond_1
    const-string v2, "wearable"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 380
    const-string v2, "wearable"

    const-string v3, "onCallableEnded - closing reader and writer streams"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383
    :cond_2
    :try_start_1
    iget-object v2, v0, Lcom/google/android/gms/wearable/node/bd;->e:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 388
    :goto_1
    :try_start_2
    iget-object v2, v0, Lcom/google/android/gms/wearable/node/bd;->f:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 394
    :goto_2
    :try_start_3
    iget-object v2, v0, Lcom/google/android/gms/wearable/node/bd;->d:Ljava/util/concurrent/Future;

    invoke-interface {v2}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v0, Lcom/google/android/gms/wearable/node/bd;->c:Ljava/util/concurrent/Future;

    invoke-interface {v2}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 395
    const-string v0, "wearable"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 396
    const-string v0, "wearable"

    const-string v2, "onCallableEnded - both reader and writer threads are already closed"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bb;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 415
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 402
    :cond_4
    :try_start_4
    iget-object v2, v0, Lcom/google/android/gms/wearable/node/bd;->d:Ljava/util/concurrent/Future;

    invoke-interface {v2}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v2

    if-nez v2, :cond_6

    .line 403
    const-string v2, "wearable"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 404
    const-string v2, "wearable"

    const-string v3, "onCallableEnded - stopping reader thread"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    :cond_5
    iget-object v2, v0, Lcom/google/android/gms/wearable/node/bd;->d:Ljava/util/concurrent/Future;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 409
    :cond_6
    iget-object v2, v0, Lcom/google/android/gms/wearable/node/bd;->c:Ljava/util/concurrent/Future;

    invoke-interface {v2}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v2

    if-nez v2, :cond_8

    .line 410
    const-string v2, "wearable"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 411
    const-string v2, "wearable"

    const-string v3, "onCallableEnded - stopping writer thread"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    :cond_7
    iget-object v0, v0, Lcom/google/android/gms/wearable/node/bd;->c:Ljava/util/concurrent/Future;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 415
    :cond_8
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :catch_0
    move-exception v2

    goto :goto_2

    :catch_1
    move-exception v2

    goto :goto_1
.end method
