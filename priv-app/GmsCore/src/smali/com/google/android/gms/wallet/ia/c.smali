.class final Lcom/google/android/gms/wallet/ia/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/cd;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V
    .locals 0

    .prologue
    .line 751
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/c;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 773
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/c;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/c;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->b:Landroid/accounts/Account;

    const/4 v4, 0x1

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/c;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v3}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/checkout/inapp/proto/q;

    move-result-object v10

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/c;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v3}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->b(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Ljava/util/ArrayList;

    move-result-object v11

    move-object v3, v2

    move-object v6, v2

    move-object v7, v2

    move v8, v5

    move-object v9, v2

    invoke-static/range {v0 .. v11}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;ZZ[I[IILjava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    .line 786
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/c;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    const/16 v2, 0x1f6

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 787
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/j;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 754
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/c;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Lcom/google/checkout/inapp/proto/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 755
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/c;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/c;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->b:Landroid/accounts/Account;

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/c;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/checkout/inapp/proto/q;

    move-result-object v6

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/c;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->b(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Ljava/util/ArrayList;

    move-result-object v7

    move-object v2, p1

    move-object v8, v5

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/j;ZZLjava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;Lcom/google/checkout/inapp/proto/a/b;)Landroid/content/Intent;

    move-result-object v0

    .line 765
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/c;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    const/16 v2, 0x1f8

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 769
    :goto_0
    return-void

    .line 767
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/c;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iput-object p1, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->e:Lcom/google/checkout/inapp/proto/j;

    goto :goto_0
.end method

.method public final b(Lcom/google/checkout/inapp/proto/j;)V
    .locals 0

    .prologue
    .line 792
    return-void
.end method
