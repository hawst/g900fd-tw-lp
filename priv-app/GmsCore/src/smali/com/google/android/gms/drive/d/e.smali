.class public final Lcom/google/android/gms/drive/d/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/internal/model/File;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/model/File;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/d/e;->a:Ljava/lang/String;

    .line 30
    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/model/File;->r()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/d/e;->b:Ljava/lang/String;

    .line 31
    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/model/File;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/d/e;->c:Ljava/lang/String;

    .line 32
    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/model/File;->N()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/d/e;->d:Ljava/lang/String;

    .line 33
    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/model/File;->y()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/d/e;->e:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/google/android/gms/drive/d/e;->f:Ljava/lang/String;

    .line 35
    return-void
.end method
