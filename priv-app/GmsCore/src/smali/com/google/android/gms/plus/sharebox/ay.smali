.class final Lcom/google/android/gms/plus/sharebox/ay;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/sharebox/au;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/sharebox/au;)V
    .locals 0

    .prologue
    .line 876
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/ay;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 5

    .prologue
    .line 876
    check-cast p1, Lcom/google/android/gms/people/e;

    invoke-interface {p1}, Lcom/google/android/gms/people/e;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/people/e;->c()Lcom/google/android/gms/people/model/e;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ay;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bf;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ay;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bf;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/plus/sharebox/au;->p()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/bf;->a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/plus/sharebox/AddToCircleData;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-interface {p1}, Lcom/google/android/gms/people/e;->c()Lcom/google/android/gms/people/model/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/model/e;->c()I

    move-result v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_3

    new-instance v3, Lcom/google/android/gms/plus/sharebox/Circle;

    invoke-interface {p1}, Lcom/google/android/gms/people/e;->c()Lcom/google/android/gms/people/model/e;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/android/gms/people/model/e;->b(I)Lcom/google/android/gms/people/model/d;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/gms/plus/sharebox/Circle;-><init>(Lcom/google/android/gms/people/model/d;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    invoke-interface {p1}, Lcom/google/android/gms/people/e;->w_()V

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ay;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bf;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ay;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bf;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bf;->e()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ay;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bf;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bf;->e()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ay;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bf;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ay;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bf;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/plus/sharebox/bf;->e()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/bf;->a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/plus/sharebox/AddToCircleData;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {p1}, Lcom/google/android/gms/people/e;->w_()V

    throw v0
.end method
