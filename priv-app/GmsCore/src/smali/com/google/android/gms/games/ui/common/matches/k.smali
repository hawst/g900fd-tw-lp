.class public final Lcom/google/android/gms/games/ui/common/matches/k;
.super Lcom/google/android/gms/games/ui/s;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/common/api/aq;
.implements Lcom/google/android/gms/games/ui/ch;
.implements Lcom/google/android/gms/games/ui/common/matches/b;
.implements Lcom/google/android/gms/games/ui/common/matches/e;
.implements Lcom/google/android/gms/games/ui/common/matches/i;


# instance fields
.field private l:Lcom/google/android/gms/games/ui/common/matches/a;

.field private m:Lcom/google/android/gms/games/ui/common/matches/d;

.field private n:Lcom/google/android/gms/games/ui/common/matches/h;

.field private o:Lcom/google/android/gms/games/ui/common/matches/u;

.field private p:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

.field private q:Z

.field private r:I

.field private s:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    sget v0, Lcom/google/android/gms/l;->bd:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/s;-><init>(I)V

    .line 68
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->r:I

    .line 74
    return-void
.end method

.method private C()V
    .locals 3

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->m:Lcom/google/android/gms/games/ui/common/matches/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->a()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->n:Lcom/google/android/gms/games/ui/common/matches/h;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/common/matches/h;->a()I

    move-result v1

    add-int/2addr v1, v0

    .line 247
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/k;->h:Lcom/google/android/gms/games/ui/d/p;

    if-nez v1, :cond_1

    const/4 v0, 0x3

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/ui/d/p;->b(I)V

    .line 249
    if-nez v1, :cond_0

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->p:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->q:Z

    iget v2, p0, Lcom/google/android/gms/games/ui/common/matches/k;->r:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->a(ZI)V

    .line 252
    :cond_0
    return-void

    .line 247
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private D()V
    .locals 3

    .prologue
    .line 335
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->b()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 336
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 351
    :goto_0
    return-void

    .line 340
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/q;->z()V

    .line 342
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v1

    .line 343
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/bh;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 344
    sget-object v1, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/multiplayer/d;->b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0

    .line 347
    :cond_1
    sget-object v2, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/bh;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Lcom/google/android/gms/games/multiplayer/d;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method private E()V
    .locals 3

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 460
    :cond_0
    :goto_0
    return-void

    .line 456
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->b()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 457
    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 458
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/y;->b(Lcom/google/android/gms/common/api/v;Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/matches/k;I)I
    .locals 0

    .prologue
    .line 48
    iput p1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->r:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/matches/k;)Lcom/google/android/gms/games/ui/q;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/matches/k;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->r:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/common/matches/k;)Lcom/google/android/gms/games/ui/common/matches/a;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->l:Lcom/google/android/gms/games/ui/common/matches/a;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/common/matches/k;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->q:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/common/matches/k;)Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->p:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/common/matches/k;)Lcom/google/android/gms/games/ui/q;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/games/ui/common/matches/k;)Lcom/google/android/gms/games/ui/q;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    return-object v0
.end method


# virtual methods
.method public final B()V
    .locals 0

    .prologue
    .line 326
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->D()V

    .line 327
    return-void
.end method

.method public final K_()V
    .locals 1

    .prologue
    .line 314
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 318
    :goto_0
    return-void

    .line 317
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->D()V

    goto :goto_0
.end method

.method protected final a(Landroid/view/View;)Lcom/google/android/gms/games/ui/d/p;
    .locals 11

    .prologue
    .line 125
    invoke-static {p0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/games/ui/cn;

    move-result-object v0

    .line 127
    const/4 v10, 0x0

    .line 128
    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/cn;->a(Landroid/content/Context;)I

    move-result v10

    .line 131
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/d/p;

    const v2, 0x102000a

    sget v3, Lcom/google/android/gms/j;->ln:I

    sget v4, Lcom/google/android/gms/j;->ms:I

    sget v5, Lcom/google/android/gms/j;->mv:I

    sget v6, Lcom/google/android/gms/j;->hc:I

    move-object v1, p1

    move-object v7, p0

    move-object v8, p0

    move-object v9, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/games/ui/d/p;-><init>(Landroid/view/View;IIIIILcom/google/android/gms/games/ui/d/r;Lcom/google/android/gms/games/ui/d/q;Lcom/google/android/gms/games/ui/d/s;I)V

    return-object v0
.end method

.method final a()V
    .locals 3

    .prologue
    .line 443
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->q:Z

    .line 444
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->l:Lcom/google/android/gms/games/ui/common/matches/a;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->q:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/a;->a(Z)V

    .line 445
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->p:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->q:Z

    iget v2, p0, Lcom/google/android/gms/games/ui/common/matches/k;->r:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->a(ZI)V

    .line 446
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->E()V

    .line 447
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 5

    .prologue
    .line 48
    check-cast p1, Lcom/google/android/gms/games/multiplayer/e;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/e;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/e;->c()Lcom/google/android/gms/games/multiplayer/a;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->d()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/a;->w_()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/ui/q;->b(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/a;->w_()V

    goto :goto_0

    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/q;->A()V

    new-instance v2, Lcom/google/android/gms/games/ui/common/matches/g;

    invoke-direct {v2, v1}, Lcom/google/android/gms/games/ui/common/matches/g;-><init>(Lcom/google/android/gms/common/data/d;)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/k;->m:Lcom/google/android/gms/games/ui/common/matches/d;

    iget-object v4, v2, Lcom/google/android/gms/games/ui/common/matches/g;->a:Lcom/google/android/gms/common/data/w;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/ui/common/matches/d;->a(Lcom/google/android/gms/common/data/d;)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/k;->n:Lcom/google/android/gms/games/ui/common/matches/h;

    iget-object v4, v2, Lcom/google/android/gms/games/ui/common/matches/g;->b:Lcom/google/android/gms/common/data/w;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/ui/common/matches/h;->a(Lcom/google/android/gms/common/data/d;)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/k;->h:Lcom/google/android/gms/games/ui/d/p;

    iget-object v4, v2, Lcom/google/android/gms/games/ui/common/matches/g;->a:Lcom/google/android/gms/common/data/w;

    invoke-interface {v4}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v4

    iget-object v2, v2, Lcom/google/android/gms/games/ui/common/matches/g;->b:Lcom/google/android/gms/common/data/w;

    invoke-interface {v2}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v2

    add-int/2addr v2, v4

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v2, v4}, Lcom/google/android/gms/games/ui/d/p;->a(IIZ)V

    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/a;->c()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->p:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/common/matches/k;->q:Z

    iget v3, p0, Lcom/google/android/gms/games/ui/common/matches/k;->r:I

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->a(ZI)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/a;->w_()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->H()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/a;->w_()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    .line 164
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->s:Ljava/lang/String;

    .line 166
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->m:Lcom/google/android/gms/games/ui/common/matches/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/games/ui/common/matches/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->n:Lcom/google/android/gms/games/ui/common/matches/h;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/games/ui/common/matches/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    sget-object v0, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/multiplayer/d;->b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 175
    sget-object v0, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/p;

    invoke-interface {v0, p1, v4}, Lcom/google/android/gms/games/p;->a(Lcom/google/android/gms/common/api/v;I)V

    .line 189
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->t()V

    .line 190
    return-void

    .line 177
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->e()Ljava/lang/String;

    move-result-object v0

    .line 179
    sget-object v1, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    invoke-interface {v1, p1, v0}, Lcom/google/android/gms/games/multiplayer/d;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 184
    sget-object v1, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/p;

    invoke-interface {v1, p1, v0, v4}, Lcom/google/android/gms/games/p;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->o:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/u;->a(Lcom/google/android/gms/games/Game;)V

    .line 257
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->o:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/u;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V

    .line 281
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->C()V

    .line 282
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->o:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/ui/common/matches/u;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;ZLjava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->n:Lcom/google/android/gms/games/ui/common/matches/h;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/ui/common/matches/h;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;ZLjava/util/ArrayList;)V

    .line 306
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->C()V

    .line 307
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->o:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/u;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 230
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->o:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/ui/common/matches/u;->a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 355
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 364
    :goto_0
    return-void

    .line 359
    :cond_0
    if-eqz p1, :cond_3

    .line 360
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/cg;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    check-cast v0, Lcom/google/android/gms/games/ui/cg;

    const/16 v1, 0x14

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/cg;->a(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->s:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/cf;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/common/matches/k;->b(Z)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/m;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/common/matches/m;-><init>()V

    invoke-virtual {v0, p0, v2}, Lcom/google/android/gms/games/ui/b/a;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    const-string v2, "confirmationDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    goto :goto_0

    .line 362
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->a()V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->o:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/u;->b(Lcom/google/android/gms/games/Game;)V

    .line 262
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->o:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/u;->b(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V

    .line 287
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->C()V

    .line 288
    return-void
.end method

.method final b(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 388
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/common/matches/k;->q:Z

    .line 389
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->l:Lcom/google/android/gms/games/ui/common/matches/a;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->q:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/a;->a(Z)V

    .line 390
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->p:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->q:Z

    iget v2, p0, Lcom/google/android/gms/games/ui/common/matches/k;->r:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->a(ZI)V

    .line 392
    if-eqz p1, :cond_0

    .line 393
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->s:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/cf;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 398
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->b()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 399
    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 400
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    invoke-interface {v1, v0, v3}, Lcom/google/android/gms/games/y;->b(Lcom/google/android/gms/common/api/v;Z)V

    .line 402
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    invoke-static {v0}, Lcom/google/android/gms/games/d;->c(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/d/ab;->a(Landroid/content/Context;)I

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/gms/games/y;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;I)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/common/matches/l;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/common/matches/l;-><init>(Lcom/google/android/gms/games/ui/common/matches/k;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 438
    :cond_1
    return-void
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->o:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/u;->c(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 235
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->C()V

    .line 236
    return-void
.end method

.method public final d(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->o:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/u;->d(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 241
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->C()V

    .line 242
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 471
    const/4 v0, 0x1

    return v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 79
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/s;->onActivityCreated(Landroid/os/Bundle;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/matches/v;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/v;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/matches/v;->T()Lcom/google/android/gms/games/ui/common/matches/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->o:Lcom/google/android/gms/games/ui/common/matches/u;

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->o:Lcom/google/android/gms/games/ui/common/matches/u;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 86
    sget v0, Lcom/google/android/gms/h;->aw:I

    sget v1, Lcom/google/android/gms/p;->jo:I

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/common/matches/k;->a(III)V

    .line 89
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/common/matches/a;-><init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/common/matches/b;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->l:Lcom/google/android/gms/games/ui/common/matches/a;

    .line 91
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->l:Lcom/google/android/gms/games/ui/common/matches/a;

    sget-object v0, Lcom/google/android/gms/games/ui/n;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/matches/a;->c(Z)V

    .line 92
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/d;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/common/matches/d;-><init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/common/matches/e;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->m:Lcom/google/android/gms/games/ui/common/matches/d;

    .line 94
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/h;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/common/matches/h;-><init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/common/matches/i;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->n:Lcom/google/android/gms/games/ui/common/matches/h;

    .line 97
    new-instance v0, Lcom/google/android/gms/games/ui/by;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/by;-><init>()V

    .line 98
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->l:Lcom/google/android/gms/games/ui/common/matches/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 99
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->m:Lcom/google/android/gms/games/ui/common/matches/d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 100
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/k;->n:Lcom/google/android/gms/games/ui/common/matches/h;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 101
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/by;->a()Lcom/google/android/gms/games/ui/bw;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/k;->a(Landroid/support/v7/widget/bv;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->p:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->setVisibility(I)V

    .line 107
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->p:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    if-ne p1, v0, :cond_0

    .line 465
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->q:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/k;->a(Z)V

    .line 467
    :cond_0
    return-void

    .line 465
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDetach()V
    .locals 2

    .prologue
    .line 139
    invoke-super {p0}, Lcom/google/android/gms/games/ui/s;->onDetach()V

    .line 141
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->u()V

    .line 142
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 143
    const-string v0, "InvitationListFragment"

    const-string v1, "Tearing down without finishing creation"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->q:Z

    if-eqz v0, :cond_0

    .line 148
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->E()V

    goto :goto_0
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->q:Z

    if-eqz v0, :cond_0

    .line 155
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->E()V

    .line 157
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/games/ui/s;->onStop()V

    .line 158
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 111
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/s;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 114
    sget v0, Lcom/google/android/gms/j;->kj:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->p:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    .line 116
    sget-object v0, Lcom/google/android/gms/games/ui/n;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->p:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->setVisibility(I)V

    .line 121
    :goto_0
    return-void

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/k;->p:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected final v()V
    .locals 0

    .prologue
    .line 331
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/k;->D()V

    .line 332
    return-void
.end method
