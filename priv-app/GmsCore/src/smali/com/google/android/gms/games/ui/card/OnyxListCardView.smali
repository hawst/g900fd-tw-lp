.class public final Lcom/google/android/gms/games/ui/card/OnyxListCardView;
.super Lcom/google/android/gms/games/ui/card/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/card/f;
.implements Lcom/google/android/gms/games/ui/card/g;
.implements Lcom/google/android/gms/games/ui/card/h;
.implements Lcom/google/android/gms/games/ui/card/l;
.implements Lcom/google/android/gms/games/ui/card/m;
.implements Lcom/google/android/gms/games/ui/card/n;
.implements Lcom/google/android/gms/games/ui/card/o;


# instance fields
.field private h:Landroid/view/View;

.field private i:Landroid/widget/FrameLayout;

.field private j:Landroid/widget/RelativeLayout;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/database/CharArrayBuffer;

.field private m:Landroid/view/View;

.field private n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field private o:Landroid/widget/TextView;

.field private p:Lcom/google/android/gms/games/ui/card/e;

.field private q:Landroid/view/View;

.field private r:I

.field private s:Landroid/support/v7/widget/bp;

.field private t:Landroid/view/View;

.field private u:Landroid/widget/Button;

.field private v:Landroid/graphics/drawable/Drawable;

.field private w:Lcom/google/android/gms/games/ui/card/r;

.field private x:Landroid/widget/Button;

.field private y:Landroid/graphics/drawable/Drawable;

.field private z:Lcom/google/android/gms/games/ui/card/t;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/a;-><init>(Landroid/content/Context;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/ui/card/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/card/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/bp;)V
    .locals 0

    .prologue
    .line 260
    iput-object p1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->s:Landroid/support/v7/widget/bp;

    .line 261
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 191
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/ui/card/e;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->p:Lcom/google/android/gms/games/ui/card/e;

    .line 231
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/ui/card/r;)V
    .locals 0

    .prologue
    .line 339
    iput-object p1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->w:Lcom/google/android/gms/games/ui/card/r;

    .line 340
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/ui/card/t;)V
    .locals 0

    .prologue
    .line 381
    iput-object p1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->z:Lcom/google/android/gms/games/ui/card/t;

    .line 382
    return-void
.end method

.method public final a(Ljava/util/ArrayList;I)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 200
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 202
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->m:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 204
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    array-length v4, v0

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    .line 206
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->a:Lcom/google/android/gms/games/ui/br;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    aget-object v6, v0, v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-interface {v5, v6, v0, p2}, Lcom/google/android/gms/games/ui/br;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    .line 205
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    aget-object v0, v0, v1

    invoke-virtual {v0, v8}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    goto :goto_1

    .line 209
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    array-length v0, v0

    if-le v3, v0, :cond_2

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->o:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v4, Lcom/google/android/gms/p;->ka:I

    new-array v5, v7, [Ljava/lang/Object;

    add-int/lit8 v3, v3, -0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v2

    invoke-virtual {v1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    :goto_2
    return-void

    .line 214
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public final d(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 166
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 167
    if-eqz p1, :cond_0

    .line 168
    sget v2, Lcom/google/android/gms/g;->am:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 172
    iget-object v3, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v2, v2, v4, v2}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 173
    sget v3, Lcom/google/android/gms/g;->al:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v3, v2

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 175
    sget v3, Lcom/google/android/gms/g;->al:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 182
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 183
    return-void

    .line 178
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v4, v4, v4, v4}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 179
    sget v2, Lcom/google/android/gms/g;->al:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_0
.end method

.method public final e()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 117
    invoke-super {p0}, Lcom/google/android/gms/games/ui/card/a;->e()V

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->h:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 122
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 123
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->removeViewAt(I)V

    .line 121
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->k:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/f;->q:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->a(Landroid/widget/TextView;I)V

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->m:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 132
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->g(I)V

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->t:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->u:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->v:Landroid/graphics/drawable/Drawable;

    const/16 v1, 0x10

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->u:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 136
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->x:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 137
    return-void

    .line 135
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->u:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public final e(Z)V
    .locals 3

    .prologue
    .line 235
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    array-length v1, v1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 236
    iget-object v2, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setClickable(Z)V

    .line 235
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setClickable(Z)V

    .line 239
    return-void
.end method

.method public final e_(I)V
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->k:Landroid/widget/TextView;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->a(Landroid/widget/TextView;I)V

    .line 318
    return-void
.end method

.method public final g(I)V
    .locals 2

    .prologue
    .line 265
    iput p1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->r:I

    .line 266
    if-lez p1, :cond_0

    .line 267
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->q:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 271
    :goto_0
    return-void

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->q:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final h(I)V
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->t:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 327
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->u:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 328
    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->k:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 302
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 303
    return-void
.end method

.method public final i(I)V
    .locals 2

    .prologue
    .line 356
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->u:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 357
    return-void
.end method

.method public final i(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 248
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    array-length v1, v1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 249
    iget-object v2, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 248
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 252
    return-void
.end method

.method public final j(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 366
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->t:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 367
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->x:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 368
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->x:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 369
    return-void
.end method

.method public final k(I)V
    .locals 2

    .prologue
    .line 398
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->x:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 399
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-ne p1, v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->p:Lcom/google/android/gms/games/ui/card/e;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/card/e;->w()V

    .line 434
    :goto_0
    return-void

    .line 414
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    if-ne p1, v0, :cond_1

    .line 415
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->p:Lcom/google/android/gms/games/ui/card/e;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/card/e;->w()V

    goto :goto_0

    .line 416
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    if-ne p1, v0, :cond_2

    .line 417
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->p:Lcom/google/android/gms/games/ui/card/e;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/card/e;->w()V

    goto :goto_0

    .line 418
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    if-ne p1, v0, :cond_3

    .line 419
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->p:Lcom/google/android/gms/games/ui/card/e;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/card/e;->w()V

    goto :goto_0

    .line 420
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->o:Landroid/widget/TextView;

    if-ne p1, v0, :cond_4

    .line 421
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->p:Lcom/google/android/gms/games/ui/card/e;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/card/e;->x()V

    goto :goto_0

    .line 422
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->q:Landroid/view/View;

    if-ne p1, v0, :cond_5

    .line 423
    new-instance v0, Landroid/support/v7/widget/bn;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/v7/widget/bn;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 424
    iget v1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->r:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bn;->a(I)V

    .line 425
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->s:Landroid/support/v7/widget/bp;

    iput-object v1, v0, Landroid/support/v7/widget/bn;->c:Landroid/support/v7/widget/bp;

    .line 426
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->q:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/view/View;Landroid/support/v7/widget/bn;)V

    goto :goto_0

    .line 427
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->u:Landroid/widget/Button;

    if-ne p1, v0, :cond_6

    .line 428
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->w:Lcom/google/android/gms/games/ui/card/r;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/card/r;->y()V

    goto :goto_0

    .line 429
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->x:Landroid/widget/Button;

    if-ne p1, v0, :cond_7

    .line 430
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->z:Lcom/google/android/gms/games/ui/card/t;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/card/t;->z()V

    goto :goto_0

    .line 432
    :cond_7
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/card/a;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected final onFinishInflate()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 75
    invoke-super {p0}, Lcom/google/android/gms/games/ui/card/a;->onFinishInflate()V

    .line 77
    sget v0, Lcom/google/android/gms/j;->bP:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->h:Landroid/view/View;

    .line 79
    sget v0, Lcom/google/android/gms/j;->kb:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->i:Landroid/widget/FrameLayout;

    .line 81
    sget v0, Lcom/google/android/gms/j;->lC:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->j:Landroid/widget/RelativeLayout;

    .line 83
    sget v0, Lcom/google/android/gms/j;->qW:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->k:Landroid/widget/TextView;

    .line 84
    new-instance v0, Landroid/database/CharArrayBuffer;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->l:Landroid/database/CharArrayBuffer;

    .line 86
    sget v0, Lcom/google/android/gms/j;->bJ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->m:Landroid/view/View;

    .line 87
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    .line 88
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v0, Lcom/google/android/gms/j;->bz:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    aput-object v0, v1, v3

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    aget-object v0, v0, v3

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Z)V

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    aget-object v0, v0, v3

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v0, Lcom/google/android/gms/j;->bA:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    aput-object v0, v1, v2

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    aget-object v0, v0, v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Z)V

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    aget-object v0, v0, v2

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v0, Lcom/google/android/gms/j;->bB:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    aput-object v0, v1, v4

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    aget-object v0, v0, v4

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Z)V

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    aget-object v0, v0, v4

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v0, Lcom/google/android/gms/j;->bC:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    aput-object v0, v1, v5

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    aget-object v0, v0, v5

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Z)V

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->n:[Lcom/google/android/gms/common/images/internal/LoadingImageView;

    aget-object v0, v0, v5

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    sget v0, Lcom/google/android/gms/j;->bH:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->o:Landroid/widget/TextView;

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    sget v0, Lcom/google/android/gms/j;->dH:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->q:Landroid/view/View;

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->q:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    sget v0, Lcom/google/android/gms/j;->E:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->t:Landroid/view/View;

    .line 107
    sget v0, Lcom/google/android/gms/j;->pD:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->u:Landroid/widget/Button;

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->u:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->u:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->v:Landroid/graphics/drawable/Drawable;

    .line 110
    sget v0, Lcom/google/android/gms/j;->qU:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->x:Landroid/widget/Button;

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->x:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->x:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxListCardView;->y:Landroid/graphics/drawable/Drawable;

    .line 113
    return-void
.end method
