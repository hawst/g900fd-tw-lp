.class final Lcom/google/android/gms/drive/ui/picker/view/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;


# instance fields
.field a:Z

.field final synthetic b:Lcom/google/android/gms/drive/ui/picker/view/DocListView;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)V
    .locals 1

    .prologue
    .line 633
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/view/e;->b:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 635
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/e;->a:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/ui/picker/view/DocListView;B)V
    .locals 0

    .prologue
    .line 633
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/ui/picker/view/e;-><init>(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 4

    .prologue
    .line 639
    new-instance v1, Lcom/google/android/gms/drive/ui/picker/a/j;

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/e;->b:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/view/e;->b:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-static {v2}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->c(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)Lcom/google/android/gms/drive/ui/picker/a/k;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/view/e;->b:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-static {v3}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->d(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/gms/drive/ui/picker/a/j;-><init>(Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;Lcom/google/android/gms/drive/ui/picker/a/k;Ljava/lang/String;)V

    return-object v1
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 1

    .prologue
    .line 662
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/e;->b:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-static {v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->e(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)V

    .line 663
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 633
    check-cast p2, Lcom/google/android/gms/drive/ui/picker/a/l;

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/e;->b:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-static {v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->e(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)V

    iget-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/e;->a:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/e;->b:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-static {v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->b(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/e;->b:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-static {v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->f(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)Lcom/google/android/gms/drive/ui/picker/a/n;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/gms/drive/ui/picker/a/n;->a(Lcom/google/android/gms/drive/ui/picker/a/l;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/e;->b:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->i()V

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/e;->b:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/e;->b:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-static {v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->g(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)V

    goto :goto_0
.end method
