.class public Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/s;


# instance fields
.field final a:I

.field public final b:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

.field final c:[Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;

.field public final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/appdatasearch/s;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/s;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->CREATOR:Lcom/google/android/gms/appdatasearch/s;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;[Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;Z)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput p1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->a:I

    .line 49
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    .line 50
    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->c:[Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;

    .line 51
    iput-boolean p4, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->d:Z

    .line 52
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;ZLjava/util/Map;)V
    .locals 2

    .prologue
    .line 73
    const/4 v0, 0x1

    invoke-static {p3}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->a(Ljava/util/Map;)[Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;

    move-result-object v1

    invoke-direct {p0, v0, p1, v1, p2}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;-><init>(ILcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;[Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;Z)V

    .line 74
    return-void
.end method

.method private static a(Ljava/util/Map;)[Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;
    .locals 6

    .prologue
    .line 56
    if-nez p0, :cond_0

    .line 57
    const/4 v0, 0x0

    .line 68
    :goto_0
    return-object v0

    .line 59
    :cond_0
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    new-array v3, v0, [Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;

    .line 61
    const/4 v0, 0x0

    .line 62
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 63
    new-instance v5, Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/appdatasearch/Feature;

    invoke-direct {v5, v1, v0}, Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;-><init>(Ljava/lang/String;[Lcom/google/android/gms/appdatasearch/Feature;)V

    aput-object v5, v3, v2

    .line 66
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 67
    goto :goto_1

    :cond_1
    move-object v0, v3

    .line 68
    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->CREATOR:Lcom/google/android/gms/appdatasearch/s;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->CREATOR:Lcom/google/android/gms/appdatasearch/s;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/s;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;Landroid/os/Parcel;I)V

    .line 111
    return-void
.end method
