.class public final Lcom/google/android/gms/drive/d/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/d/d;


# instance fields
.field private final a:Lcom/google/android/gms/drive/internal/model/File;

.field private final b:Lcom/google/android/gms/drive/auth/g;

.field private final c:Lcom/google/android/gms/common/server/ClientContext;

.field private final d:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/internal/model/File;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 4

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/File;

    iput-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    .line 45
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-static {}, Lcom/google/android/gms/drive/auth/AppIdentity;->a()Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 p2, 0x0

    :cond_1
    iput-object p2, p0, Lcom/google/android/gms/drive/d/a/b;->b:Lcom/google/android/gms/drive/auth/g;

    .line 47
    iput-object p3, p0, Lcom/google/android/gms/drive/d/a/b;->c:Lcom/google/android/gms/common/server/ClientContext;

    .line 49
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 50
    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/model/File;->D()Ljava/util/List;

    move-result-object v0

    .line 51
    if-eqz v0, :cond_3

    .line 52
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/ParentReference;

    .line 53
    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/ParentReference;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v0, "root"

    :goto_1
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/ParentReference;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 57
    :cond_3
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->d:Ljava/util/Set;

    .line 58
    return-void
.end method


# virtual methods
.method public final A()J
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->q()J

    move-result-wide v0

    return-wide v0
.end method

.method public final B()Z
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->u()Lcom/google/android/gms/drive/internal/model/File$Labels;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File$Labels;->e()Z

    move-result v0

    return v0
.end method

.method public final C()Z
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->u()Lcom/google/android/gms/drive/internal/model/File$Labels;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File$Labels;->b()Z

    move-result v0

    return v0
.end method

.method public final D()Ljava/lang/String;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final E()J
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->F()J

    move-result-wide v0

    return-wide v0
.end method

.method public final F()Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->N()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final G()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->O()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final H()Ljava/lang/String;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->K()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final I()Ljava/util/List;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->E()Ljava/util/List;

    move-result-object v0

    .line 260
    if-nez v0, :cond_0

    .line 261
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 263
    :cond_0
    return-object v0
.end method

.method public final J()Z
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->c()Z

    move-result v0

    return v0
.end method

.method public final K()Ljava/lang/String;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->M()Lcom/google/android/gms/drive/internal/model/Permission;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/Permission;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final L()Ljava/lang/String;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->r()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final M()Ljava/lang/String;
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->x()Lcom/google/android/gms/drive/internal/model/FileLocalId;

    move-result-object v0

    .line 275
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/FileLocalId;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final N()Ljava/lang/Long;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 281
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->g()Ljava/lang/String;

    move-result-object v0

    .line 285
    invoke-virtual {p0}, Lcom/google/android/gms/drive/d/a/b;->M()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    .line 286
    const-string v0, "ApiaryDocEntry"

    const-string v2, "Getting the localId but not the creatorAppId from server."

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    invoke-virtual {p0}, Lcom/google/android/gms/drive/d/a/b;->f()Ljava/util/List;

    move-result-object v0

    .line 288
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 289
    const-string v0, "ApiaryDocEntry"

    const-string v2, "Singleton entry is not authorized for any app. This shouldn\'t happen."

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    const-string v0, "ApiaryDocEntry"

    const-string v2, "Unauthorized singleton entry: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/internal/model/File;->s()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v1

    .line 297
    :goto_0
    return-object v0

    .line 293
    :cond_0
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 297
    :cond_1
    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public final O()Ljava/lang/String;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->x()Lcom/google/android/gms/drive/internal/model/FileLocalId;

    move-result-object v0

    .line 304
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/FileLocalId;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final P()Lcom/google/android/gms/drive/internal/model/User;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->I()Lcom/google/android/gms/drive/internal/model/User;

    move-result-object v0

    return-object v0
.end method

.method public final Q()Lcom/google/android/gms/drive/internal/model/User;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->v()Lcom/google/android/gms/drive/internal/model/User;

    move-result-object v0

    return-object v0
.end method

.method public final R()Z
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->P()Z

    move-result v0

    return v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->u()Lcom/google/android/gms/drive/internal/model/File$Labels;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File$Labels;->c()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->G()Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Lcom/google/android/gms/common/server/ClientContext;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->c:Lcom/google/android/gms/common/server/ClientContext;

    return-object v0
.end method

.method public final e()Ljava/util/Set;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->d:Ljava/util/Set;

    return-object v0
.end method

.method public final f()Ljava/util/List;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->d()Ljava/util/List;

    move-result-object v0

    .line 99
    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->s()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->H()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->A()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->w()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->y()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->L()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->u()Lcom/google/android/gms/drive/internal/model/File$Labels;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File$Labels;->d()Z

    move-result v0

    return v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->z()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->o()Z

    move-result v0

    return v0
.end method

.method public final t()Z
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->l()Z

    move-result v0

    return v0
.end method

.method public final u()Ljava/util/List;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->C()Ljava/util/List;

    move-result-object v0

    .line 183
    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final x()Z
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->e()Z

    move-result v0

    return v0
.end method

.method public final y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/b;->a:Lcom/google/android/gms/drive/internal/model/File;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
