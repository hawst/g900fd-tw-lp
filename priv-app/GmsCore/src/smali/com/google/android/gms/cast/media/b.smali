.class final Lcom/google/android/gms/cast/media/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/cast/c/z;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/media/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/media/a;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/google/android/gms/cast/media/b;->a:Lcom/google/android/gms/cast/media/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 230
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "DeviceScanner.Listener#onAllDevicesOffline"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/cast/media/b;->a:Lcom/google/android/gms/cast/media/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/a;->d(Lcom/google/android/gms/cast/media/a;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 234
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 235
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 236
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/cast/media/z;

    .line 237
    if-eqz v1, :cond_2

    .line 238
    iget-object v1, v1, Lcom/google/android/gms/cast/media/z;->a:Lcom/google/android/gms/cast/CastDevice;

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/cast/media/b;->a:Lcom/google/android/gms/cast/media/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/a;->a(Lcom/google/android/gms/cast/media/a;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/y;

    .line 240
    if-eqz v0, :cond_0

    .line 241
    iput-boolean v5, v0, Lcom/google/android/gms/cast/media/y;->b:Z

    .line 242
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v3, "device %s is in use; not removing route"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/b;->a:Lcom/google/android/gms/cast/media/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/a;->e(Lcom/google/android/gms/cast/media/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/aa;

    .line 245
    invoke-interface {v0, v1}, Lcom/google/android/gms/cast/media/aa;->a(Lcom/google/android/gms/cast/CastDevice;)V

    goto :goto_1

    .line 247
    :cond_1
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v3, "device %s is NOT in use; removing route"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 248
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 251
    :cond_2
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v1

    const-string v3, "rogue device %s found; removing route"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 252
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 255
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/cast/media/b;->a:Lcom/google/android/gms/cast/media/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/a;->f(Lcom/google/android/gms/cast/media/a;)V

    .line 256
    return-void
.end method

.method public final a(Lcom/google/android/gms/cast/CastDevice;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 189
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "DeviceScanner.Listener#onDeviceOnline: %s"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/cast/media/b;->a:Lcom/google/android/gms/cast/media/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/a;->a(Lcom/google/android/gms/cast/media/a;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/y;

    .line 191
    if-eqz v0, :cond_0

    .line 192
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v1

    const-string v2, "Device is already in use."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    iput-boolean v4, v0, Lcom/google/android/gms/cast/media/y;->b:Z

    .line 196
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 197
    iget-object v0, p0, Lcom/google/android/gms/cast/media/b;->a:Lcom/google/android/gms/cast/media/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/a;->b(Lcom/google/android/gms/cast/media/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/q;

    .line 198
    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4, v2, v3}, Lcom/google/android/gms/cast/c/q;->a(Ljava/lang/String;J)V

    goto :goto_0

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/media/b;->a:Lcom/google/android/gms/cast/media/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/a;->c(Lcom/google/android/gms/cast/media/a;)Lcom/google/android/gms/cast/c/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/cast/c/h;->a(Lcom/google/android/gms/cast/CastDevice;)V

    .line 205
    return-void
.end method

.method public final b(Lcom/google/android/gms/cast/CastDevice;)V
    .locals 4

    .prologue
    .line 209
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "DeviceScanner.Listener#onDeviceStateChanged: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 211
    iget-object v0, p0, Lcom/google/android/gms/cast/media/b;->a:Lcom/google/android/gms/cast/media/a;

    iget-object v0, v0, Landroid/support/v7/c/f;->e:Landroid/support/v7/c/e;

    .line 212
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/c/e;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/google/android/gms/cast/media/b;->a:Lcom/google/android/gms/cast/media/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/a;->c(Lcom/google/android/gms/cast/media/a;)Lcom/google/android/gms/cast/c/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/cast/c/h;->a(Lcom/google/android/gms/cast/CastDevice;)V

    .line 215
    :cond_0
    return-void
.end method

.method public final c(Lcom/google/android/gms/cast/CastDevice;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 219
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "DeviceScanner.Listener#onDeviceOffline: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 220
    iget-object v0, p0, Lcom/google/android/gms/cast/media/b;->a:Lcom/google/android/gms/cast/media/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/a;->a(Lcom/google/android/gms/cast/media/a;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/y;

    .line 221
    if-eqz v0, :cond_0

    .line 222
    iput-boolean v3, v0, Lcom/google/android/gms/cast/media/y;->b:Z

    .line 226
    :goto_0
    return-void

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/b;->a:Lcom/google/android/gms/cast/media/a;

    invoke-static {v0, p1}, Lcom/google/android/gms/cast/media/a;->a(Lcom/google/android/gms/cast/media/a;Lcom/google/android/gms/cast/CastDevice;)V

    goto :goto_0
.end method
