.class final Lcom/google/android/gms/wallet/common/ui/bz;
.super Landroid/view/inputmethod/InputConnectionWrapper;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/FormEditText;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Landroid/view/inputmethod/InputConnection;)V
    .locals 1

    .prologue
    .line 640
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/bz;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 641
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Landroid/view/inputmethod/InputConnectionWrapper;-><init>(Landroid/view/inputmethod/InputConnection;Z)V

    .line 642
    return-void
.end method


# virtual methods
.method public final commitText(Ljava/lang/CharSequence;I)Z
    .locals 3

    .prologue
    .line 646
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bz;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/bz;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/FormEditText;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 647
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/InputConnectionWrapper;->commitText(Ljava/lang/CharSequence;I)Z

    move-result v0

    .line 648
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/bz;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c(Lcom/google/android/gms/wallet/common/ui/FormEditText;)Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 649
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/bz;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/bz;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c(Lcom/google/android/gms/wallet/common/ui/FormEditText;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 651
    :cond_0
    return v0
.end method
