.class public final Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/drive/database/r;

.field private final b:Lcom/google/android/gms/drive/g/at;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/i;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/r;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->a:Lcom/google/android/gms/drive/database/r;

    .line 50
    new-instance v0, Lcom/google/android/gms/drive/g/q;

    invoke-direct {v0, p2}, Lcom/google/android/gms/drive/g/q;-><init>(Lcom/google/android/gms/drive/g/i;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->b:Lcom/google/android/gms/drive/g/at;

    .line 51
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/g/aw;)V
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->c()Lcom/google/android/gms/drive/g/i;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;-><init>(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/i;)V

    .line 46
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/d/d;Ljava/lang/Boolean;JZ)V
    .locals 10

    .prologue
    .line 76
    invoke-static {p1}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v2

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->a:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->b:Lcom/google/android/gms/drive/g/at;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/g/at;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->b()V

    .line 82
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {p2}, Lcom/google/android/gms/drive/d/d;->r()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, Lcom/google/android/gms/drive/d/d;->g()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, p1, v1, v3}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->z()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {p2}, Lcom/google/android/gms/drive/d/d;->M()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 87
    invoke-interface {p2}, Lcom/google/android/gms/drive/d/d;->N()Ljava/lang/Long;

    move-result-object v1

    if-nez v1, :cond_0

    .line 88
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Creator packagingId of singleton resource (resourceId: %s) not provided. This should never happen."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p2}, Lcom/google/android/gms/drive/d/d;->g()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->c()V

    throw v0

    .line 96
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {p2}, Lcom/google/android/gms/drive/d/d;->M()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2}, Lcom/google/android/gms/drive/d/d;->N()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {p2}, Lcom/google/android/gms/drive/d/d;->J()Z

    move-result v6

    invoke-interface/range {v1 .. v6}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;JZ)Ljava/util/List;

    move-result-object v1

    .line 99
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 102
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ah;

    .line 103
    invoke-interface {p2}, Lcom/google/android/gms/drive/d/d;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->c(Ljava/lang/String;)V

    :cond_1
    move-object v1, v0

    .line 108
    invoke-static {p2, v1}, Lcom/google/android/gms/drive/database/h;->a(Lcom/google/android/gms/drive/d/d;Lcom/google/android/gms/drive/database/model/ah;)V

    .line 109
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/database/model/ah;->b(Z)V

    .line 111
    if-eqz p3, :cond_2

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ah;->A()Z

    move-result v0

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eq v0, v3, :cond_2

    .line 112
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/database/model/ah;->c(Z)V

    .line 115
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    .line 117
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 118
    invoke-interface {p2}, Lcom/google/android/gms/drive/d/d;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 120
    :try_start_2
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 122
    :catch_0
    move-exception v5

    :try_start_3
    const-string v5, "DocEntrySynchronizer"

    const-string v6, "App ID has invalid syntax: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    invoke-static {v5, v6, v7}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 125
    :cond_3
    if-eqz p6, :cond_5

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0, v1, v3}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/ah;Ljava/util/Set;)V

    .line 138
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v2

    .line 139
    new-instance v3, Ljava/util/HashMap;

    invoke-interface {v2}, Lcom/google/android/gms/drive/database/b/d;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/HashMap;-><init>(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 141
    :try_start_4
    invoke-interface {v2}, Lcom/google/android/gms/drive/database/b/d;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ah;

    .line 142
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v5

    invoke-interface {v3, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 145
    :catchall_1
    move-exception v0

    :try_start_5
    invoke-interface {v2}, Lcom/google/android/gms/drive/database/b/d;->close()V

    throw v0

    .line 132
    :cond_5
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->a:Lcom/google/android/gms/drive/database/r;

    sget-object v6, Lcom/google/android/gms/drive/auth/a;->a:Lcom/google/android/gms/drive/auth/a;

    invoke-interface {v0, v1, v4, v5, v6}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/ah;JLcom/google/android/gms/drive/auth/a;)Lcom/google/android/gms/drive/auth/a;

    goto :goto_2

    .line 145
    :cond_6
    invoke-interface {v2}, Lcom/google/android/gms/drive/database/b/d;->close()V

    .line 148
    invoke-interface {p2}, Lcom/google/android/gms/drive/d/d;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 149
    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->a:Lcom/google/android/gms/drive/database/r;

    const-string v5, "application/vnd.google-apps.folder"

    invoke-interface {v4, p1, v5, v0}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v4

    .line 152
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ah;->x()Z

    move-result v5

    const-string v6, "Parent is not a collection: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    invoke-static {v5, v6, v7}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 154
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ah;->y()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ah;->z()Z

    move-result v0

    if-nez v0, :cond_9

    .line 155
    const-string v0, ""

    invoke-virtual {v4, v0}, Lcom/google/android/gms/drive/database/model/ah;->a(Ljava/lang/String;)V

    .line 156
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    .line 161
    :cond_8
    :goto_4
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ah;

    .line 162
    if-nez v0, :cond_7

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->a:Lcom/google/android/gms/drive/database/r;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/am;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/am;->i()V

    goto :goto_3

    .line 157
    :cond_9
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ah;->B()J

    move-result-wide v6

    cmp-long v0, v6, p4

    if-gez v0, :cond_8

    .line 158
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    goto :goto_4

    .line 169
    :cond_a
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ah;

    .line 170
    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->a:Lcom/google/android/gms/drive/database/r;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/database/model/EntrySpec;)V

    goto :goto_5

    .line 176
    :cond_b
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ah;->e()V

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->e()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->c()V

    .line 181
    return-void
.end method
