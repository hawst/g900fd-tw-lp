.class public Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lcom/google/android/gms/wallet/common/ui/dp;


# instance fields
.field a:Landroid/widget/Spinner;

.field b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

.field private c:Lcom/google/checkout/inapp/proto/a/d;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->d:I

    .line 51
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a(Landroid/content/Context;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->d:I

    .line 57
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a(Landroid/content/Context;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->d:I

    .line 63
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a(Landroid/content/Context;)V

    .line 64
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 67
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 68
    sget v1, Lcom/google/android/gms/l;->hz:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 70
    sget v0, Lcom/google/android/gms/j;->nA:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a:Landroid/widget/Spinner;

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 72
    sget v0, Lcom/google/android/gms/j;->mf:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    .line 73
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/checkout/inapp/proto/a/d;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a()Lcom/google/checkout/inapp/proto/a/d;

    move-result-object v0

    .line 123
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->c:Lcom/google/checkout/inapp/proto/a/d;

    goto :goto_0
.end method

.method public final a(Lcom/google/checkout/inapp/proto/a/d;Lcom/google/checkout/inapp/proto/a/d;Lcom/google/checkout/inapp/proto/a/d;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 88
    iput-object p2, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->c:Lcom/google/checkout/inapp/proto/a/d;

    .line 89
    if-eqz p3, :cond_0

    iget-wide v0, p3, Lcom/google/checkout/inapp/proto/a/d;->a:J

    .line 91
    :goto_0
    iget-wide v2, p2, Lcom/google/checkout/inapp/proto/a/d;->a:J

    cmp-long v0, v2, v0

    if-ltz v0, :cond_1

    .line 92
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->Cj:I

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v4}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/d;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->Ck:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    .line 97
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/gms/l;->hh:I

    invoke-direct {v1, v2, v3, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 99
    sget v0, Lcom/google/android/gms/l;->hi:I

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a:Landroid/widget/Spinner;

    iget v1, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->d:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, v5}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 111
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->c:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/a/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a(Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v0, p1, p3}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a(Lcom/google/checkout/inapp/proto/a/d;Lcom/google/checkout/inapp/proto/a/d;)V

    .line 113
    return-void

    .line 89
    :cond_0
    iget-wide v0, p1, Lcom/google/checkout/inapp/proto/a/d;->a:J

    goto :goto_0

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a:Landroid/widget/Spinner;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->e()Z

    move-result v0

    .line 192
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->e()Z

    move-result v0

    .line 180
    if-nez v0, :cond_0

    .line 181
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->f()Z

    .line 183
    :cond_0
    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->g()Z

    move-result v0

    return v0
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 137
    packed-switch p3, :pswitch_data_0

    .line 146
    :goto_0
    return-void

    .line 139
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->setVisibility(I)V

    goto :goto_0

    .line 142
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->setVisibility(I)V

    .line 143
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->requestFocus()Z

    goto :goto_0

    .line 137
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 175
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 160
    instance-of v0, p1, Landroid/os/Bundle;

    if-nez v0, :cond_1

    .line 161
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    check-cast p1, Landroid/os/Bundle;

    .line 165
    const-string v0, "superSavedInstanceState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 166
    const-string v0, "amountSpinnerIndex"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    const-string v0, "amountSpinnerIndex"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->d:I

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 150
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 151
    const-string v1, "superSavedInstanceState"

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 152
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 153
    const-string v1, "amountSpinnerIndex"

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 155
    :cond_0
    return-object v0
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 129
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 131
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->setEnabled(Z)V

    .line 133
    return-void
.end method
