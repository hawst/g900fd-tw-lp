.class public Lcom/google/android/gms/ocr/SecuredCreditCardOcrActivity;
.super Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;-><init>()V

    return-void
.end method

.method private static a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 70
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.google.android.gms.ocr.EXP_DATE_RECOGNITION_ENABLED"

    sget-object v0, Lcom/google/android/gms/ocr/a/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-static {}, Lcom/google/android/gms/ocr/SecuredCreditCardOcrActivity;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/ocr/SecuredCreditCardOcrActivity;->setResult(ILandroid/content/Intent;)V

    .line 43
    invoke-super {p0}, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;->onBackPressed()V

    .line 44
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->c(Landroid/app/Activity;)Ljava/lang/String;

    .line 29
    invoke-virtual {p0}, Lcom/google/android/gms/ocr/SecuredCreditCardOcrActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "RECOGNIZE_EXPIRATION_DATE"

    sget-object v0, Lcom/google/android/gms/ocr/a/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 33
    invoke-virtual {p0}, Lcom/google/android/gms/ocr/SecuredCreditCardOcrActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "com.google.android.gms.ocr.THEME"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 35
    invoke-virtual {p0}, Lcom/google/android/gms/ocr/SecuredCreditCardOcrActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "FULLSCREEN_MODE"

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 37
    invoke-super {p0, p1}, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    return-void

    :cond_0
    move v0, v1

    .line 35
    goto :goto_0
.end method

.method public onCreditCardOcrCaptured(Landroid/content/Intent;I)V
    .locals 5

    .prologue
    .line 50
    invoke-static {}, Lcom/google/android/gms/ocr/SecuredCreditCardOcrActivity;->a()Landroid/content/Intent;

    move-result-object v1

    .line 51
    if-eqz p1, :cond_0

    .line 52
    const-string v0, "CREDIT_CARD_OCR_RESULT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;

    .line 54
    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->getExpYear()I

    move-result v2

    rem-int/lit8 v2, v2, 0x64

    add-int/lit16 v2, v2, 0x7d0

    .line 58
    new-instance v3, Lcom/google/android/gms/ocr/CreditCardOcrResult;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->getCardNum()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->getExpMonth()I

    move-result v0

    invoke-direct {v3, v4, v0, v2}, Lcom/google/android/gms/ocr/CreditCardOcrResult;-><init>(Ljava/lang/String;II)V

    .line 62
    const-string v0, "com.google.android.gms.ocr.CREDIT_CARD_OCR_RESULT"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 65
    :cond_0
    invoke-virtual {p0, p2, v1}, Lcom/google/android/gms/ocr/SecuredCreditCardOcrActivity;->setResult(ILandroid/content/Intent;)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/gms/ocr/SecuredCreditCardOcrActivity;->finish()V

    .line 67
    return-void
.end method
