.class public final Lcom/google/android/gms/smart_profile/card/a/a;
.super Lcom/google/android/gms/smart_profile/card/a/b;
.source "SourceFile"


# instance fields
.field private e:Z

.field private f:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/a/b;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->wd:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;)V
    .locals 6

    .prologue
    .line 110
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->x()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 111
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 112
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->w()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Nicknames;

    .line 115
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Nicknames;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Nicknames;->b()Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Nicknames;->b()Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->b()Ljava/lang/String;

    move-result-object v3

    const-string v4, "cp2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 117
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Nicknames;->d()Ljava/lang/String;

    move-result-object v3

    .line 120
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 121
    new-instance v4, Lcom/google/android/gms/smart_profile/card/view/h;

    invoke-direct {v4}, Lcom/google/android/gms/smart_profile/card/view/h;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v5, Lcom/google/android/gms/p;->wa:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/smart_profile/card/view/h;->c(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    .line 128
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 130
    :cond_1
    invoke-static {p2, v1}, Lcom/google/android/gms/smart_profile/card/view/g;->a(Ljava/util/List;Ljava/util/List;)V

    .line 132
    :cond_2
    return-void
.end method

.method private b(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;)V
    .locals 8

    .prologue
    .line 135
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 136
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 137
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;

    .line 139
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;->d()Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;->d()Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->b()Ljava/lang/String;

    move-result-object v1

    const-string v4, "cp2"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 141
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v1, Landroid/support/v7/widget/CardView;

    invoke-virtual {v1}, Landroid/support/v7/widget/CardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/android/gms/smart_profile/n;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 144
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 145
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;->f()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, v1}, Lcom/google/android/gms/smart_profile/card/a/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 148
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 149
    new-instance v5, Lcom/google/android/gms/smart_profile/card/view/h;

    invoke-direct {v5}, Lcom/google/android/gms/smart_profile/card/view/h;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v1, Landroid/support/v7/widget/CardView;

    invoke-virtual {v1}, Landroid/support/v7/widget/CardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v6, Lcom/google/android/gms/p;->vY:I

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/android/gms/smart_profile/card/view/h;->c(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/n;->a(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    if-eqz v0, :cond_1

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xe

    if-lt v4, v5, :cond_1

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/n;->a(Ljava/util/Calendar;)Ljava/util/Date;

    move-result-object v0

    sget-object v4, Landroid/provider/CalendarContract;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "time"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    new-instance v0, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Landroid/content/Intent;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    .line 157
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 149
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 159
    :cond_2
    invoke-static {p2, v2}, Lcom/google/android/gms/smart_profile/card/view/g;->a(Ljava/util/List;Ljava/util/List;)V

    .line 161
    :cond_3
    return-void
.end method

.method private c(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;)V
    .locals 7

    .prologue
    .line 164
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->F()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 165
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 166
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->E()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;

    .line 169
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;->d()Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;->d()Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->b()Ljava/lang/String;

    move-result-object v1

    const-string v4, "cp2"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v1, v4}, Lcom/google/android/gms/smart_profile/card/a/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 175
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 176
    new-instance v5, Lcom/google/android/gms/smart_profile/card/view/h;

    invoke-direct {v5}, Lcom/google/android/gms/smart_profile/card/view/h;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v1, Landroid/support/v7/widget/CardView;

    invoke-virtual {v1}, Landroid/support/v7/widget/CardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v6, Lcom/google/android/gms/p;->wc:I

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/android/gms/smart_profile/card/view/h;->c(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v1

    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.SEARCH"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v5, "query"

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "vnd.android.cursor.dir/contact"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v1, v4}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Landroid/content/Intent;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    .line 184
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 186
    :cond_1
    invoke-static {p2, v2}, Lcom/google/android/gms/smart_profile/card/view/g;->a(Ljava/util/List;Ljava/util/List;)V

    .line 188
    :cond_2
    return-void
.end method

.method private d(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;)V
    .locals 6

    .prologue
    .line 191
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->p()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 192
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 193
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->o()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;

    .line 195
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;->d()Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;->d()Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->b()Ljava/lang/String;

    move-result-object v3

    const-string v4, "cp2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 197
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;->f()Ljava/lang/String;

    move-result-object v3

    .line 202
    const-string v4, "custom"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 203
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;->g()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v3, v0}, Lcom/google/android/gms/smart_profile/card/a/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 204
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    new-instance v4, Lcom/google/android/gms/smart_profile/card/view/h;

    invoke-direct {v4}, Lcom/google/android/gms/smart_profile/card/view/h;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v5, Lcom/google/android/gms/p;->vZ:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/smart_profile/card/view/h;->c(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    .line 212
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 214
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;->b()Ljava/lang/String;

    move-result-object v3

    .line 215
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;->g()Ljava/lang/String;

    move-result-object v0

    .line 216
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 217
    new-instance v4, Lcom/google/android/gms/smart_profile/card/view/h;

    invoke-direct {v4}, Lcom/google/android/gms/smart_profile/card/view/h;-><init>()V

    invoke-virtual {v4, v3}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/gms/smart_profile/card/view/h;->c(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    .line 223
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 226
    :cond_2
    invoke-static {p2, v1}, Lcom/google/android/gms/smart_profile/card/view/g;->a(Ljava/util/List;Ljava/util/List;)V

    .line 228
    :cond_3
    return-void
.end method

.method private e(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;)V
    .locals 6

    .prologue
    .line 232
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 233
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 234
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Abouts;

    .line 237
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Abouts;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Abouts;->b()Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Abouts;->b()Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->b()Ljava/lang/String;

    move-result-object v3

    const-string v4, "contact"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 239
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Abouts;->d()Ljava/lang/String;

    move-result-object v3

    .line 242
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 243
    new-instance v4, Lcom/google/android/gms/smart_profile/card/view/h;

    invoke-direct {v4}, Lcom/google/android/gms/smart_profile/card/view/h;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v5, Lcom/google/android/gms/p;->wb:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/smart_profile/card/view/h;->c(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    .line 249
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 251
    :cond_1
    invoke-static {p2, v1}, Lcom/google/android/gms/smart_profile/card/view/g;->a(Ljava/util/List;Ljava/util/List;)V

    .line 253
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/google/android/gms/smart_profile/card/a/b;->a(Landroid/os/Bundle;)V

    .line 69
    const-string v0, "is_expanded"

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/a;->f:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 70
    return-void
.end method

.method public final a(Lcom/google/android/gms/smart_profile/card/a/c;Landroid/os/Bundle;Lcom/google/android/gms/smart_profile/bg;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/smart_profile/card/a/b;->a(Lcom/google/android/gms/smart_profile/card/a/c;Landroid/os/Bundle;Lcom/google/android/gms/smart_profile/bg;)V

    .line 56
    if-nez p3, :cond_1

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/a;->f:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/a;->f:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    sget-object v2, Lcom/google/android/gms/smart_profile/c;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 61
    if-nez p2, :cond_2

    .line 63
    :goto_1
    invoke-virtual {p3}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/smart_profile/card/a/a;->a(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;)V

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/smart_profile/card/a/a;->b(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;)V

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/smart_profile/card/a/a;->c(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;)V

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/smart_profile/card/a/a;->d(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;)V

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/smart_profile/card/a/a;->e(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;)V

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/smart_profile/card/a/a;->e:Z

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/card/a/a;->f:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p3}, Lcom/google/android/gms/smart_profile/bg;->q()I

    move-result v4

    invoke-virtual {v3, v2, v0, v1, v4}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Ljava/util/List;IZI)V

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/a;->f:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/b;->c:Lcom/google/android/gms/smart_profile/card/h;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Lcom/google/android/gms/smart_profile/card/h;)V

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    sget v1, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->vX:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p3}, Lcom/google/android/gms/smart_profile/bg;->q()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 61
    :cond_2
    const-string v0, "is_expanded"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    move v1, v0

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Lcom/google/android/gms/smart_profile/card/a/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/card/a/a;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/google/android/gms/smart_profile/c;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method
