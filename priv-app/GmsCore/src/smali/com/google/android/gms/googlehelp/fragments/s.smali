.class final Lcom/google/android/gms/googlehelp/fragments/s;
.super Landroid/webkit/WebViewClient;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;


# direct methods
.method constructor <init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/fragments/s;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/s;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    .line 176
    invoke-static {p2, v0}, Lcom/google/android/gms/googlehelp/common/k;->a(Ljava/lang/String;Lcom/google/android/gms/googlehelp/common/HelpConfig;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v2

    .line 177
    if-nez v2, :cond_1

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/s;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 179
    if-nez v0, :cond_0

    move v0, v6

    .line 190
    :goto_0
    return v0

    .line 182
    :cond_0
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/googlehelp/common/q;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    .line 190
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 184
    :cond_1
    new-instance v0, Lcom/google/android/gms/googlehelp/f/f;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/s;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    const-string v3, "ARTICLE_HELP_LINK_CLICKED"

    const/4 v4, -0x1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/googlehelp/f/f;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Lcom/google/android/gms/googlehelp/common/k;Ljava/lang/String;ILjava/util/Calendar;)V

    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/f/f;->a([Ljava/lang/Object;)V

    goto :goto_1
.end method
