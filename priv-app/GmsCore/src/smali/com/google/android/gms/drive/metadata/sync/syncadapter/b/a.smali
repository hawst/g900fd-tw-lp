.class public final Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/f;


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/r;

.field private final b:Lcom/google/android/gms/drive/database/model/a;

.field private final c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;Lcom/google/android/gms/drive/database/model/a;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/a;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/a;->b:Lcom/google/android/gms/drive/database/model/a;

    .line 34
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/a;->a:Lcom/google/android/gms/drive/database/r;

    .line 35
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/a;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;

    .line 36
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/SyncResult;Z)V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;Landroid/content/SyncResult;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/a;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/a;->b:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, p2, v1, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;->a(Landroid/content/SyncResult;Lcom/google/android/gms/drive/database/model/a;Ljava/lang/Boolean;)Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/a;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/a;->b:Lcom/google/android/gms/drive/database/model/a;

    new-instance v3, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b;

    iget-object v1, v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;->a:Lcom/google/android/gms/drive/g/aw;

    invoke-direct {v3, v1, v2, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b;-><init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;)V

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/a;->a:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/a;->b:Lcom/google/android/gms/drive/database/model/a;

    iget-object v1, v1, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/database/r;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/c;

    move-result-object v0

    .line 49
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/a;->a:Lcom/google/android/gms/drive/database/r;

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/a;->b:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v1, v2, v4}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/Boolean;)Ljava/util/Set;

    move-result-object v1

    .line 50
    new-instance v2, Lcom/google/android/gms/drive/metadata/sync/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/c;->g()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    invoke-direct {v2, v4, v5, v1}, Lcom/google/android/gms/drive/metadata/sync/a/b;-><init>(JLjava/util/Set;)V

    .line 51
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    invoke-static {v2, v1, v4, v5}, Lcom/google/android/gms/drive/metadata/sync/a/d;->a(Lcom/google/android/gms/drive/metadata/sync/a/c;Ljava/lang/String;J)Lcom/google/android/gms/drive/metadata/sync/a/d;

    move-result-object v1

    .line 52
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/c;->h()Ljava/lang/String;

    move-result-object v0

    const v2, 0x7fffffff

    invoke-virtual {p1, v1, v0, v2, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->a(Lcom/google/android/gms/drive/metadata/sync/a/d;Ljava/lang/String;ILcom/google/android/gms/drive/metadata/sync/syncadapter/e;)V

    .line 54
    return-void
.end method
