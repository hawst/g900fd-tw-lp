.class final Lcom/google/android/gms/plus/internal/cb;
.super Lcom/google/android/gms/plus/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/internal/ay;

.field private final b:Lcom/google/android/gms/plus/internal/ar;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/ar;)V
    .locals 0

    .prologue
    .line 285
    iput-object p1, p0, Lcom/google/android/gms/plus/internal/cb;->a:Lcom/google/android/gms/plus/internal/ay;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/b;-><init>()V

    .line 286
    iput-object p2, p0, Lcom/google/android/gms/plus/internal/cb;->b:Lcom/google/android/gms/plus/internal/ar;

    .line 287
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 291
    .line 292
    if-eqz p2, :cond_1

    .line 293
    const-string v0, "pendingIntent"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 295
    :goto_0
    new-instance v2, Lcom/google/android/gms/common/c;

    invoke-direct {v2, p1, v0}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V

    .line 296
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cb;->a:Lcom/google/android/gms/plus/internal/ay;

    new-instance v3, Lcom/google/android/gms/plus/internal/cc;

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/cb;->a:Lcom/google/android/gms/plus/internal/ay;

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/cb;->b:Lcom/google/android/gms/plus/internal/ar;

    if-eqz p3, :cond_0

    new-instance v1, Lcom/google/android/gms/plus/data/a/a;

    invoke-direct {v1, p3}, Lcom/google/android/gms/plus/data/a/a;-><init>(Landroid/os/Bundle;)V

    :cond_0
    invoke-direct {v3, v4, v5, v2, v1}, Lcom/google/android/gms/plus/internal/cc;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/ar;Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/data/a/a;)V

    invoke-virtual {v0, v3}, Lcom/google/android/gms/plus/internal/ay;->a(Lcom/google/android/gms/common/internal/al;)V

    .line 298
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
