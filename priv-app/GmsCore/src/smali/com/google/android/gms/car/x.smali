.class public Lcom/google/android/gms/car/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/LayoutInflater$Factory;


# static fields
.field public static a:Landroid/util/SparseArray;


# instance fields
.field public b:Landroid/view/LayoutInflater;

.field private c:Landroid/content/Context;

.field private d:Landroid/view/Window;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 89
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v3}, Landroid/util/SparseArray;-><init>(I)V

    .line 91
    sput-object v0, Lcom/google/android/gms/car/x;->a:Landroid/util/SparseArray;

    const/4 v1, 0x0

    const-string v2, "STATE_INITIALIZING"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 92
    sget-object v0, Lcom/google/android/gms/car/x;->a:Landroid/util/SparseArray;

    const/4 v1, 0x1

    const-string v2, "STATE_CREATED"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 93
    sget-object v0, Lcom/google/android/gms/car/x;->a:Landroid/util/SparseArray;

    const/4 v1, 0x2

    const-string v2, "STATE_STOPPED"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 94
    sget-object v0, Lcom/google/android/gms/car/x;->a:Landroid/util/SparseArray;

    const/4 v1, 0x3

    const-string v2, "STATE_STARTED"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 95
    sget-object v0, Lcom/google/android/gms/car/x;->a:Landroid/util/SparseArray;

    const/4 v1, 0x4

    const-string v2, "STATE_PAUSED"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 96
    sget-object v0, Lcom/google/android/gms/car/x;->a:Landroid/util/SparseArray;

    const/4 v1, 0x5

    const-string v2, "STATE_RESUMED"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 97
    sget-object v0, Lcom/google/android/gms/car/x;->a:Landroid/util/SparseArray;

    const-string v1, "STATE_FINISHED"

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 98
    return-void
.end method

.method public static d()V
    .locals 0

    .prologue
    .line 561
    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Context;
    .locals 3

    .prologue
    .line 138
    const-string v0, "CAR.PROJECTION"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    const-string v0, "CAR.PROJECTION"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Context DPI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/x;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->densityDpi:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/x;->c:Landroid/content/Context;

    return-object v0
.end method

.method public final b()Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/gms/car/x;->b:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method public final c()Landroid/content/res/Resources;
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/gms/car/x;->c:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 200
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarProjectionActivity not initialized with attach()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/x;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/ClassLoader;
    .locals 1

    .prologue
    .line 564
    iget-object v0, p0, Lcom/google/android/gms/car/x;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    return-object v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 578
    iget-boolean v0, p0, Lcom/google/android/gms/car/x;->e:Z

    return v0
.end method

.method public final g()Landroid/view/Window;
    .locals 1

    .prologue
    .line 582
    iget-object v0, p0, Lcom/google/android/gms/car/x;->d:Landroid/view/Window;

    return-object v0
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    .prologue
    .line 555
    const/4 v0, 0x0

    return-object v0
.end method
