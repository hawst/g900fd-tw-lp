.class public final Lcom/google/android/gms/smart_profile/b/e;
.super Landroid/support/v4/a/a;
.source "SourceFile"


# instance fields
.field private f:Ljava/lang/ref/WeakReference;

.field private g:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/smart_profile/b/f;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/support/v4/a/a;-><init>(Landroid/content/Context;)V

    .line 38
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/b/e;->f:Ljava/lang/ref/WeakReference;

    .line 39
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/b/e;->g:Ljava/lang/Object;

    .line 76
    iget-boolean v0, p0, Landroid/support/v4/a/j;->p:Z

    if-eqz v0, :cond_0

    .line 77
    invoke-super {p0, p1}, Landroid/support/v4/a/a;->b(Ljava/lang/Object;)V

    .line 79
    :cond_0
    return-void
.end method

.method public final d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 83
    monitor-enter p0

    .line 85
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/e;->f:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/e;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    .line 88
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    const/4 v0, 0x0

    return-object v0

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final e()V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0}, Landroid/support/v4/a/a;->e()V

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/e;->g:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/e;->g:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/b/e;->b(Ljava/lang/Object;)V

    .line 53
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/b/e;->i()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/e;->g:Ljava/lang/Object;

    if-nez v0, :cond_2

    .line 54
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/a/j;->a()V

    .line 56
    :cond_2
    return-void
.end method

.method protected final f()V
    .locals 0

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/b/e;->b()Z

    .line 62
    return-void
.end method

.method protected final g()V
    .locals 1

    .prologue
    .line 66
    invoke-super {p0}, Landroid/support/v4/a/a;->g()V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/b/e;->b()Z

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/b/e;->g:Ljava/lang/Object;

    .line 71
    return-void
.end method
