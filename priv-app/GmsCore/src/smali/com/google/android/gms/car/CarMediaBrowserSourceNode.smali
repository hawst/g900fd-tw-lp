.class public Lcom/google/android/gms/car/CarMediaBrowserSourceNode;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field public b:Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

.field public c:I

.field public d:I

.field public e:[Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/google/android/gms/car/fb;

    invoke-direct {v0}, Lcom/google/android/gms/car/fb;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->a:I

    .line 112
    new-instance v0, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

    invoke-direct {v0}, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->b:Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

    .line 113
    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;II[Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;)V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput p1, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->a:I

    .line 104
    iput-object p2, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->b:Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

    .line 105
    iput p3, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->c:I

    .line 106
    iput p4, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->d:I

    .line 107
    iput-object p5, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->e:[Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    .line 108
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->a:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 122
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/car/fb;->a(Lcom/google/android/gms/car/CarMediaBrowserSourceNode;Landroid/os/Parcel;I)V

    .line 123
    return-void
.end method
