.class public Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;
.super Lcom/google/android/gms/wallet/common/ui/FormEditText;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/wallet/common/ui/ak;

.field private d:I

.field private e:Landroid/text/TextWatcher;

.field private f:Lcom/google/android/gms/wallet/common/ui/validator/g;

.field private g:Lcom/google/android/gms/wallet/common/ui/validator/g;

.field private h:Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;

.field private i:Ljava/lang/String;

.field private j:[I

.field private k:Landroid/content/res/ColorStateList;

.field private l:I

.field private final m:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;-><init>(Landroid/content/Context;)V

    .line 37
    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->d:I

    .line 45
    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->l:I

    .line 222
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/aj;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/aj;-><init>(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->m:Landroid/text/TextWatcher;

    .line 51
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->h()V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->d:I

    .line 45
    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->l:I

    .line 222
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/aj;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/aj;-><init>(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->m:Landroid/text/TextWatcher;

    .line 56
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->h()V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->d:I

    .line 45
    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->l:I

    .line 222
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/aj;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/aj;-><init>(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->m:Landroid/text/TextWatcher;

    .line 61
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->h()V

    .line 62
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;I)I
    .locals 0

    .prologue
    .line 25
    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->l:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 25
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->i:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->i:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/common/w;->b(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v0, :cond_1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->aG:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/content/Context;Landroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->k:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->l:I

    return v0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 209
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->i()V

    .line 215
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->m:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Landroid/text/TextWatcher;)V

    .line 216
    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->k:Landroid/content/res/ColorStateList;

    .line 220
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->d:I

    return v0
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/ak;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a:Lcom/google/android/gms/wallet/common/ui/ak;

    .line 135
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->h:Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;

    if-nez v0, :cond_0

    .line 139
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->h:Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->h:Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->b(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->h:Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c:Landroid/text/TextWatcher;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, v0, v3}, Landroid/text/TextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 144
    :goto_0
    return-void

    .line 143
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f()Z

    goto :goto_0
.end method

.method public final a([I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, -0x1

    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 99
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->f:Lcom/google/android/gms/wallet/common/ui/validator/g;

    if-eqz v1, :cond_0

    .line 100
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->f:Lcom/google/android/gms/wallet/common/ui/validator/g;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 101
    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->f:Lcom/google/android/gms/wallet/common/ui/validator/g;

    .line 103
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->g:Lcom/google/android/gms/wallet/common/ui/validator/g;

    if-eqz v1, :cond_1

    .line 104
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->g:Lcom/google/android/gms/wallet/common/ui/validator/g;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 105
    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->g:Lcom/google/android/gms/wallet/common/ui/validator/g;

    .line 107
    :cond_1
    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->d:I

    if-ne v1, v6, :cond_2

    if-eqz p1, :cond_2

    .line 109
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->j:[I

    .line 114
    if-eqz p1, :cond_4

    array-length v4, p1

    move v1, v3

    :goto_0
    if-ge v1, v4, :cond_4

    aget v5, p1, v1

    if-nez v5, :cond_3

    .line 116
    :goto_1
    if-eq v1, v2, :cond_6

    .line 117
    new-instance v2, Lcom/google/android/gms/wallet/common/ui/validator/g;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getContext()Landroid/content/Context;

    move-result-object v4

    new-array v5, v6, [I

    aput v3, v5, v3

    invoke-direct {v2, v4, v5}, Lcom/google/android/gms/wallet/common/ui/validator/g;-><init>(Landroid/content/Context;[I)V

    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->g:Lcom/google/android/gms/wallet/common/ui/validator/g;

    .line 120
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->g:Lcom/google/android/gms/wallet/common/ui/validator/g;

    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 122
    if-nez p1, :cond_5

    .line 126
    :goto_2
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/validator/g;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/wallet/common/ui/validator/g;-><init>(Landroid/content/Context;[I)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->f:Lcom/google/android/gms/wallet/common/ui/validator/g;

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->f:Lcom/google/android/gms/wallet/common/ui/validator/g;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->b(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 131
    :cond_2
    return-void

    .line 114
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 122
    :cond_5
    array-length v2, p1

    add-int/lit8 v0, v2, -0x1

    new-array v0, v0, [I

    invoke-static {p1, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v3, v1, 0x1

    sub-int/2addr v2, v1

    add-int/lit8 v2, v2, -0x1

    invoke-static {p1, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2

    :cond_6
    move-object v0, p1

    goto :goto_2
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 78
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->d:I

    if-ne v0, v1, :cond_0

    .line 92
    :goto_0
    return-void

    .line 82
    :cond_0
    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->d:I

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->e:Landroid/text/TextWatcher;

    if-eqz v0, :cond_1

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->e:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->e:Landroid/text/TextWatcher;

    .line 88
    :cond_1
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/a/a;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->e:Landroid/text/TextWatcher;

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->e:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->i:Ljava/lang/String;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 204
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->onFinishInflate()V

    .line 205
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->i()V

    .line 206
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 189
    instance-of v0, p1, Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 190
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 200
    :goto_0
    return-void

    .line 194
    :cond_0
    check-cast p1, Landroid/os/Bundle;

    .line 195
    const-string v0, "superInstanceState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 197
    const-string v0, "instrumentType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->d:I

    .line 198
    const-string v0, "cardNumber"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->i:Ljava/lang/String;

    .line 199
    const-string v0, "disallowedCardTypes"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a([I)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 179
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 180
    const-string v1, "superInstanceState"

    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 181
    const-string v1, "instrumentType"

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 182
    const-string v1, "cardNumber"

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v1, "disallowedCardTypes"

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->j:[I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 184
    return-object v0
.end method
