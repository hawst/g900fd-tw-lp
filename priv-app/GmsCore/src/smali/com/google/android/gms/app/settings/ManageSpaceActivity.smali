.class public Lcom/google/android/gms/app/settings/ManageSpaceActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/Button;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/Button;

.field private g:Ljava/lang/CharSequence;

.field private h:Landroid/widget/Button;

.field private i:Lcom/google/android/gms/app/settings/e;

.field private j:Lcom/google/android/gms/app/settings/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 177
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->g:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 45
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->clearApplicationUserData()Z

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->b:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->d:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->c:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 110
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->startActivity(Landroid/content/Intent;)V

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->f:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    .line 112
    new-instance v0, Lcom/google/android/gms/app/settings/d;

    invoke-direct {v0}, Lcom/google/android/gms/app/settings/d;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "clearDataDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/app/settings/d;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    goto :goto_0

    .line 113
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->h:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 114
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 63
    sget v0, Lcom/google/android/gms/l;->cK:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->setContentView(I)V

    .line 65
    sget v0, Lcom/google/android/gms/p;->xK:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->g:Ljava/lang/CharSequence;

    .line 67
    sget v0, Lcom/google/android/gms/j;->jS:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->b:Landroid/widget/TextView;

    .line 68
    sget v0, Lcom/google/android/gms/j;->lJ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->c:Landroid/widget/Button;

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    sget v0, Lcom/google/android/gms/j;->lL:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->h:Landroid/widget/Button;

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->h:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    sget v0, Lcom/google/android/gms/j;->ud:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->d:Landroid/widget/TextView;

    .line 74
    sget v0, Lcom/google/android/gms/j;->cZ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->a:Landroid/view/View;

    .line 75
    const/16 v0, 0x13

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    sget v0, Lcom/google/android/gms/j;->tk:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->e:Landroid/widget/TextView;

    .line 77
    sget v0, Lcom/google/android/gms/j;->cY:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->f:Landroid/widget/Button;

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->i:Lcom/google/android/gms/app/settings/e;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/app/settings/e;->cancel(Z)Z

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->j:Lcom/google/android/gms/app/settings/f;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/app/settings/f;->cancel(Z)Z

    .line 101
    iput-object v2, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->i:Lcom/google/android/gms/app/settings/e;

    .line 102
    iput-object v2, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->j:Lcom/google/android/gms/app/settings/f;

    .line 104
    invoke-super {p0}, Landroid/support/v7/app/d;->onPause()V

    .line 105
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 86
    invoke-super {p0}, Landroid/support/v7/app/d;->onResume()V

    .line 87
    new-instance v0, Lcom/google/android/gms/app/settings/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/app/settings/e;-><init>(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->i:Lcom/google/android/gms/app/settings/e;

    .line 88
    new-instance v0, Lcom/google/android/gms/app/settings/f;

    invoke-direct {v0, p0, v3}, Lcom/google/android/gms/app/settings/f;-><init>(Lcom/google/android/gms/app/settings/ManageSpaceActivity;B)V

    iput-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->j:Lcom/google/android/gms/app/settings/f;

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->i:Lcom/google/android/gms/app/settings/e;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/app/settings/e;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->j:Lcom/google/android/gms/app/settings/f;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/app/settings/f;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 95
    return-void
.end method
