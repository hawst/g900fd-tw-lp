.class public final Lcom/google/android/gms/udc/e/d;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[I

.field public apiHeader:Lcom/google/android/gms/udc/e/a;

.field public b:I

.field public c:I

.field public d:Lcom/google/android/gms/udc/e/u;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 46
    iput-object v1, p0, Lcom/google/android/gms/udc/e/d;->apiHeader:Lcom/google/android/gms/udc/e/a;

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    iput v2, p0, Lcom/google/android/gms/udc/e/d;->b:I

    iput v2, p0, Lcom/google/android/gms/udc/e/d;->c:I

    iput-object v1, p0, Lcom/google/android/gms/udc/e/d;->d:Lcom/google/android/gms/udc/e/u;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/d;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/udc/e/d;->cachedSize:I

    .line 47
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 140
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v2

    .line 141
    iget-object v1, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    array-length v1, v1

    if-lez v1, :cond_5

    move v1, v0

    .line 143
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 144
    iget-object v3, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    aget v3, v3, v0

    .line 145
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 143
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148
    :cond_0
    add-int v0, v2, v1

    .line 149
    iget-object v1, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 151
    :goto_1
    iget v1, p0, Lcom/google/android/gms/udc/e/d;->b:I

    if-eqz v1, :cond_1

    .line 152
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/udc/e/d;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 155
    :cond_1
    iget v1, p0, Lcom/google/android/gms/udc/e/d;->c:I

    if-eqz v1, :cond_2

    .line 156
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/udc/e/d;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 159
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/udc/e/d;->d:Lcom/google/android/gms/udc/e/u;

    if-eqz v1, :cond_3

    .line 160
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/udc/e/d;->d:Lcom/google/android/gms/udc/e/u;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 163
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/udc/e/d;->apiHeader:Lcom/google/android/gms/udc/e/a;

    if-eqz v1, :cond_4

    .line 164
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/udc/e/d;->apiHeader:Lcom/google/android/gms/udc/e/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    :cond_4
    return v0

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 62
    if-ne p1, p0, :cond_1

    .line 63
    const/4 v0, 0x1

    .line 97
    :cond_0
    :goto_0
    return v0

    .line 65
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/udc/e/d;

    if-eqz v1, :cond_0

    .line 68
    check-cast p1, Lcom/google/android/gms/udc/e/d;

    .line 69
    iget-object v1, p0, Lcom/google/android/gms/udc/e/d;->apiHeader:Lcom/google/android/gms/udc/e/a;

    if-nez v1, :cond_4

    .line 70
    iget-object v1, p1, Lcom/google/android/gms/udc/e/d;->apiHeader:Lcom/google/android/gms/udc/e/a;

    if-nez v1, :cond_0

    .line 78
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    iget-object v2, p1, Lcom/google/android/gms/udc/e/d;->a:[I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    iget v1, p0, Lcom/google/android/gms/udc/e/d;->b:I

    iget v2, p1, Lcom/google/android/gms/udc/e/d;->b:I

    if-ne v1, v2, :cond_0

    .line 85
    iget v1, p0, Lcom/google/android/gms/udc/e/d;->c:I

    iget v2, p1, Lcom/google/android/gms/udc/e/d;->c:I

    if-ne v1, v2, :cond_0

    .line 88
    iget-object v1, p0, Lcom/google/android/gms/udc/e/d;->d:Lcom/google/android/gms/udc/e/u;

    if-nez v1, :cond_5

    .line 89
    iget-object v1, p1, Lcom/google/android/gms/udc/e/d;->d:Lcom/google/android/gms/udc/e/u;

    if-nez v1, :cond_0

    .line 97
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/gms/udc/e/d;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 74
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/udc/e/d;->apiHeader:Lcom/google/android/gms/udc/e/a;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/d;->apiHeader:Lcom/google/android/gms/udc/e/a;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 93
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/udc/e/d;->d:Lcom/google/android/gms/udc/e/u;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/d;->d:Lcom/google/android/gms/udc/e/u;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/u;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/udc/e/d;->apiHeader:Lcom/google/android/gms/udc/e/a;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 105
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v2

    add-int/2addr v0, v2

    .line 107
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/udc/e/d;->b:I

    add-int/2addr v0, v2

    .line 108
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/udc/e/d;->c:I

    add-int/2addr v0, v2

    .line 109
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/udc/e/d;->d:Lcom/google/android/gms/udc/e/u;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 111
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/udc/e/d;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 112
    return v0

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/d;->apiHeader:Lcom/google/android/gms/udc/e/a;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/a;->hashCode()I

    move-result v0

    goto :goto_0

    .line 109
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/udc/e/d;->d:Lcom/google/android/gms/udc/e/u;

    invoke-virtual {v1}, Lcom/google/android/gms/udc/e/u;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/udc/e/d;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/udc/e/d;->b:I

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/android/gms/udc/e/d;->c:I

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/udc/e/d;->d:Lcom/google/android/gms/udc/e/u;

    if-nez v0, :cond_8

    new-instance v0, Lcom/google/android/gms/udc/e/u;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/u;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/d;->d:Lcom/google/android/gms/udc/e/u;

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/udc/e/d;->d:Lcom/google/android/gms/udc/e/u;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/android/gms/udc/e/d;->apiHeader:Lcom/google/android/gms/udc/e/a;

    if-nez v0, :cond_9

    new-instance v0, Lcom/google/android/gms/udc/e/a;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/d;->apiHeader:Lcom/google/android/gms/udc/e/a;

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/udc/e/d;->apiHeader:Lcom/google/android/gms/udc/e/a;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x10 -> :sswitch_3
        0x18 -> :sswitch_4
        0x22 -> :sswitch_5
        0x2a -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 119
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 120
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/udc/e/d;->a:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    :cond_0
    iget v0, p0, Lcom/google/android/gms/udc/e/d;->b:I

    if-eqz v0, :cond_1

    .line 124
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/udc/e/d;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 126
    :cond_1
    iget v0, p0, Lcom/google/android/gms/udc/e/d;->c:I

    if-eqz v0, :cond_2

    .line 127
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/udc/e/d;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 129
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/d;->d:Lcom/google/android/gms/udc/e/u;

    if-eqz v0, :cond_3

    .line 130
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/udc/e/d;->d:Lcom/google/android/gms/udc/e/u;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 132
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/udc/e/d;->apiHeader:Lcom/google/android/gms/udc/e/a;

    if-eqz v0, :cond_4

    .line 133
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/udc/e/d;->apiHeader:Lcom/google/android/gms/udc/e/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 135
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 136
    return-void
.end method
