.class public final Lcom/google/android/gms/plus/apps/az;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/plus/apps/az;


# instance fields
.field private final b:Landroid/support/v4/g/h;

.field private final c:Landroid/content/res/Resources;

.field private d:Landroid/graphics/drawable/Drawable;


# direct methods
.method private constructor <init>(Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/az;->c:Landroid/content/res/Resources;

    .line 32
    new-instance v0, Landroid/support/v4/g/h;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Landroid/support/v4/g/h;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/az;->b:Landroid/support/v4/g/h;

    .line 33
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/az;
    .locals 2

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/gms/plus/apps/az;->a:Lcom/google/android/gms/plus/apps/az;

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Lcom/google/android/gms/plus/apps/az;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/apps/az;-><init>(Landroid/content/res/Resources;)V

    sput-object v0, Lcom/google/android/gms/plus/apps/az;->a:Lcom/google/android/gms/plus/apps/az;

    .line 42
    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/apps/az;->a:Lcom/google/android/gms/plus/apps/az;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 49
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/az;->b:Landroid/support/v4/g/h;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/az;->d:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/az;->c:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/gms/h;->cl:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/az;->d:Landroid/graphics/drawable/Drawable;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/az;->d:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/az;->b:Landroid/support/v4/g/h;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    return-void
.end method
