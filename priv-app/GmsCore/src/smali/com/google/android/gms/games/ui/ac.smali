.class public abstract Lcom/google/android/gms/games/ui/ac;
.super Landroid/support/v7/widget/bv;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/bq;


# static fields
.field private static final c:D


# instance fields
.field private d:Z

.field private e:Z

.field private f:Landroid/support/v7/widget/by;

.field g:Lcom/google/android/gms/games/ui/ac;

.field private final h:Landroid/support/v7/widget/ap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    const-wide v0, 0x4046800000000000L    # 45.0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/games/ui/ac;->c:D

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Landroid/support/v7/widget/bv;-><init>()V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/ac;->d:Z

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/ac;->e:Z

    .line 34
    iput-object v1, p0, Lcom/google/android/gms/games/ui/ac;->g:Lcom/google/android/gms/games/ui/ac;

    .line 39
    iput-object v1, p0, Lcom/google/android/gms/games/ui/ac;->f:Landroid/support/v7/widget/by;

    .line 115
    new-instance v0, Lcom/google/android/gms/games/ui/ad;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/ad;-><init>(Lcom/google/android/gms/games/ui/ac;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/ac;->h:Landroid/support/v7/widget/ap;

    .line 204
    return-void
.end method

.method public static s()V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method static synthetic v()D
    .locals 2

    .prologue
    .line 17
    sget-wide v0, Lcom/google/android/gms/games/ui/ac;->c:D

    return-wide v0
.end method


# virtual methods
.method protected a(Lcom/google/android/gms/games/ui/ac;)I
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x0

    return v0
.end method

.method final a(Landroid/support/v7/widget/by;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/gms/games/ui/ac;->f:Landroid/support/v7/widget/by;

    .line 100
    return-void
.end method

.method public final a(Landroid/support/v7/widget/cs;)V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/support/v7/widget/bv;->a(Landroid/support/v7/widget/cs;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ac;->f:Landroid/support/v7/widget/by;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ac;->f:Landroid/support/v7/widget/by;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/by;->c(Landroid/support/v7/widget/cs;)V

    .line 108
    :cond_0
    return-void
.end method

.method public final a(ZZ)V
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/ac;->d:Z

    if-eq v0, p1, :cond_0

    .line 64
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/ac;->d:Z

    .line 65
    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/ac;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/ac;->p()V

    .line 69
    :cond_0
    return-void
.end method

.method final b(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 148
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/ac;->a()I

    move-result v0

    .line 149
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/ac;->g()Z

    move-result v1

    .line 150
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/ac;->e:Z

    .line 151
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/ac;->g()Z

    move-result v2

    .line 153
    if-eq v1, v2, :cond_0

    .line 154
    if-eqz v2, :cond_1

    .line 156
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/ac;->a()I

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/google/android/gms/games/ui/ac;->c(II)V

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    invoke-virtual {p0, v3, v0}, Lcom/google/android/gms/games/ui/ac;->d(II)V

    goto :goto_0
.end method

.method protected g(I)I
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x1

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/ac;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract l()Z
.end method

.method public final m()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-virtual {p0, v0, v0}, Lcom/google/android/gms/games/ui/ac;->a(ZZ)V

    .line 49
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 53
    invoke-virtual {p0, v0, v0}, Lcom/google/android/gms/games/ui/ac;->a(ZZ)V

    .line 54
    return-void
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/ac;->d:Z

    return v0
.end method

.method public p()V
    .locals 2

    .prologue
    .line 87
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/ac;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/ac;->a(II)V

    .line 88
    return-void
.end method

.method public q()Landroid/support/v7/widget/ap;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ac;->h:Landroid/support/v7/widget/ap;

    return-object v0
.end method

.method public abstract r()I
.end method

.method protected final t()I
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ac;->g:Lcom/google/android/gms/games/ui/ac;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ac;->g:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/ac;->a(Lcom/google/android/gms/games/ui/ac;)I

    move-result v0

    .line 187
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u()I
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    return v0
.end method
