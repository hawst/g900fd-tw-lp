.class public final Lcom/google/android/gms/icing/c/a/r;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/icing/c/a/s;

.field public b:Lcom/google/android/gms/icing/c/a/t;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5036
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 5037
    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/r;->a:Lcom/google/android/gms/icing/c/a/s;

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/r;->b:Lcom/google/android/gms/icing/c/a/t;

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/r;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/r;->cachedSize:I

    .line 5038
    return-void
.end method

.method public static a([B)Lcom/google/android/gms/icing/c/a/r;
    .locals 1

    .prologue
    .line 5150
    new-instance v0, Lcom/google/android/gms/icing/c/a/r;

    invoke-direct {v0}, Lcom/google/android/gms/icing/c/a/r;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/c/a/r;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 5103
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 5104
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/r;->a:Lcom/google/android/gms/icing/c/a/s;

    if-eqz v1, :cond_0

    .line 5105
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/r;->a:Lcom/google/android/gms/icing/c/a/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5108
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/r;->b:Lcom/google/android/gms/icing/c/a/t;

    if-eqz v1, :cond_1

    .line 5109
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/r;->b:Lcom/google/android/gms/icing/c/a/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5112
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 5050
    if-ne p1, p0, :cond_1

    .line 5051
    const/4 v0, 0x1

    .line 5075
    :cond_0
    :goto_0
    return v0

    .line 5053
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/r;

    if-eqz v1, :cond_0

    .line 5056
    check-cast p1, Lcom/google/android/gms/icing/c/a/r;

    .line 5057
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/r;->a:Lcom/google/android/gms/icing/c/a/s;

    if-nez v1, :cond_4

    .line 5058
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/r;->a:Lcom/google/android/gms/icing/c/a/s;

    if-nez v1, :cond_0

    .line 5066
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/r;->b:Lcom/google/android/gms/icing/c/a/t;

    if-nez v1, :cond_5

    .line 5067
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/r;->b:Lcom/google/android/gms/icing/c/a/t;

    if-nez v1, :cond_0

    .line 5075
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/r;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 5062
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/r;->a:Lcom/google/android/gms/icing/c/a/s;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/r;->a:Lcom/google/android/gms/icing/c/a/s;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/c/a/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 5071
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/r;->b:Lcom/google/android/gms/icing/c/a/t;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/r;->b:Lcom/google/android/gms/icing/c/a/t;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/c/a/t;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5080
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/r;->a:Lcom/google/android/gms/icing/c/a/s;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 5083
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/r;->b:Lcom/google/android/gms/icing/c/a/t;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 5085
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/r;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 5086
    return v0

    .line 5080
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/r;->a:Lcom/google/android/gms/icing/c/a/s;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/c/a/s;->hashCode()I

    move-result v0

    goto :goto_0

    .line 5083
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/r;->b:Lcom/google/android/gms/icing/c/a/t;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/c/a/t;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 4676
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/r;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/r;->a:Lcom/google/android/gms/icing/c/a/s;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/icing/c/a/s;

    invoke-direct {v0}, Lcom/google/android/gms/icing/c/a/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/r;->a:Lcom/google/android/gms/icing/c/a/s;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/r;->a:Lcom/google/android/gms/icing/c/a/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/r;->b:Lcom/google/android/gms/icing/c/a/t;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/icing/c/a/t;

    invoke-direct {v0}, Lcom/google/android/gms/icing/c/a/t;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/r;->b:Lcom/google/android/gms/icing/c/a/t;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/r;->b:Lcom/google/android/gms/icing/c/a/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 5092
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/r;->a:Lcom/google/android/gms/icing/c/a/s;

    if-eqz v0, :cond_0

    .line 5093
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/r;->a:Lcom/google/android/gms/icing/c/a/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5095
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/r;->b:Lcom/google/android/gms/icing/c/a/t;

    if-eqz v0, :cond_1

    .line 5096
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/r;->b:Lcom/google/android/gms/icing/c/a/t;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5098
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5099
    return-void
.end method
