.class public Lcom/google/android/gms/people/service/a/a/b;
.super Lcom/google/android/gms/people/service/a/a/c;
.source "SourceFile"


# instance fields
.field public final g:Ljava/lang/String;

.field private final h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/service/d;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/google/android/gms/people/service/a/a/c;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;)V

    .line 18
    iput-object p4, p0, Lcom/google/android/gms/people/service/a/a/b;->g:Ljava/lang/String;

    .line 19
    iput-boolean p6, p0, Lcom/google/android/gms/people/service/a/a/b;->h:Z

    .line 20
    return-void
.end method


# virtual methods
.method protected final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[url="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/a/b;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final g()[B
    .locals 3

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/a/b;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/service/m;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/m;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/a/b;->g:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/gms/people/service/a/a/b;->h:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/service/m;->a(Ljava/lang/String;Z)[B

    move-result-object v0

    return-object v0
.end method
