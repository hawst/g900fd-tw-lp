.class public final Lcom/google/android/gms/smart_profile/header/HeaderUtil;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method

.method private static a(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Ljava/util/List;I)I
    .locals 3

    .prologue
    .line 249
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 250
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/z;

    .line 251
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/z;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 252
    invoke-virtual {p0, p2}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/z;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 253
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/z;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a(ILjava/lang/String;)V

    .line 255
    :cond_0
    add-int/lit8 p2, p2, 0x1

    .line 259
    :cond_1
    return p2

    .line 249
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Ljava/util/List;ZI)I
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 279
    move v1, v2

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 280
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/q;

    .line 281
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->d()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v4

    .line 282
    :goto_1
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->m()Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "work"

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v5, v4

    :goto_2
    if-eqz v5, :cond_6

    if-ne p2, v3, :cond_6

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->g()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 285
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 287
    if-eqz p2, :cond_5

    .line 288
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->k()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 290
    sget v3, Lcom/google/android/gms/p;->wT:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->h()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v2

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->f()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 305
    :goto_3
    invoke-virtual {p0, p3}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 306
    invoke-virtual {p0, p3, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a(ILjava/lang/String;)V

    .line 308
    :cond_0
    add-int/lit8 p3, p3, 0x1

    .line 312
    :cond_1
    return p3

    :cond_2
    move v3, v2

    .line 281
    goto :goto_1

    :cond_3
    move v5, v2

    .line 282
    goto :goto_2

    .line 295
    :cond_4
    sget v3, Lcom/google/android/gms/p;->wU:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v4, [Ljava/lang/Object;

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->f()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 301
    :cond_5
    sget v3, Lcom/google/android/gms/p;->wV:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v4, [Ljava/lang/Object;

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->f()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 279
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0
.end method

.method public static a(Lcom/google/android/gms/smart_profile/header/view/HeaderView;I)V
    .locals 2

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a()Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 45
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 46
    sget v1, Lcom/google/android/gms/p;->wB:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 47
    return-void
.end method

.method public static a(Lcom/google/android/gms/smart_profile/header/view/HeaderView;IZ)V
    .locals 7

    .prologue
    .line 367
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 369
    sget v0, Lcom/google/android/gms/h;->cV:I

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 373
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 374
    if-eqz p2, :cond_0

    .line 376
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    div-int/lit8 v0, v0, 0x3

    .line 378
    :cond_0
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 381
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 383
    invoke-virtual {v3, p1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 385
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 386
    const/16 v5, 0x55

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 387
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    const/4 v6, 0x0

    invoke-virtual {v3, v2, v5, v6, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 391
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, p1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 392
    if-eqz p2, :cond_1

    .line 394
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, v1, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v2}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->b(Landroid/graphics/drawable/Drawable;)V

    .line 401
    :goto_0
    return-void

    .line 397
    :cond_1
    invoke-static {v0}, Lcom/google/android/gms/common/images/internal/f;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 398
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 399
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 205
    if-nez p1, :cond_0

    .line 245
    :goto_0
    return-void

    .line 209
    :cond_0
    const/4 v0, 0x0

    .line 211
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->g()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v2

    .line 212
    if-eqz v2, :cond_1

    invoke-interface {v2}, Lcom/google/android/gms/people/identity/models/Person;->t()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Lcom/google/android/gms/people/identity/models/Person;->s()Lcom/google/android/gms/people/identity/models/r;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/people/identity/models/r;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 214
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/p;->wq:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    move v0, v1

    .line 233
    :goto_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 235
    invoke-virtual {p0, v2}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 216
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->u()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 218
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->u()Ljava/util/List;

    move-result-object v3

    .line 220
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 221
    if-ne v2, v4, :cond_2

    .line 222
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    :cond_2
    move v5, v2

    move-object v2, v0

    move v0, v5

    .line 224
    goto :goto_1

    :cond_3
    if-eqz v2, :cond_8

    invoke-interface {v2}, Lcom/google/android/gms/people/identity/models/Person;->t()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v2}, Lcom/google/android/gms/people/identity/models/Person;->s()Lcom/google/android/gms/people/identity/models/r;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/people/identity/models/r;->d()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 226
    invoke-interface {v2}, Lcom/google/android/gms/people/identity/models/Person;->s()Lcom/google/android/gms/people/identity/models/r;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/people/identity/models/r;->c()Ljava/util/List;

    move-result-object v3

    .line 227
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 228
    if-ne v2, v4, :cond_7

    .line 229
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v5, v2

    move-object v2, v0

    move v0, v5

    goto :goto_1

    .line 236
    :cond_4
    if-le v0, v4, :cond_5

    .line 238
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->wp:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 242
    :cond_5
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->t()Z

    move-result v0

    if-eqz v0, :cond_6

    sget v0, Lcom/google/android/gms/p;->xc:I

    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a(I)V

    goto/16 :goto_0

    :cond_6
    sget v0, Lcom/google/android/gms/p;->wZ:I

    goto :goto_2

    :cond_7
    move v5, v2

    move-object v2, v0

    move v0, v5

    goto/16 :goto_1

    :cond_8
    move-object v2, v0

    move v0, v1

    goto/16 :goto_1
.end method

.method public static a(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Lcom/google/android/gms/smart_profile/IdentityPersonUtil;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 15

    .prologue
    .line 69
    if-eqz p1, :cond_2

    .line 71
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->g()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v6

    .line 72
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->q()Z

    move-result v2

    .line 73
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->p()Z

    move-result v3

    .line 74
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n()Z

    move-result v5

    .line 75
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->r()Z

    .line 76
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->o()Z

    move-result v4

    .line 77
    if-eqz v2, :cond_1

    if-nez v3, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->i()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    .line 81
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->c()Ljava/lang/String;

    move-result-object v3

    move-object v14, v3

    move v3, v2

    move-object v2, v14

    .line 94
    :goto_1
    move-object/from16 v0, p2

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p0, v5}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->b(Z)V

    .line 97
    invoke-virtual {p0, v3}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->c(Z)V

    .line 98
    invoke-static/range {p0 .. p1}, Lcom/google/android/gms/smart_profile/header/HeaderUtil;->a(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V

    .line 103
    const/4 v5, 0x0

    .line 104
    const/4 v7, 0x0

    .line 105
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->c()I

    move-result v8

    .line 106
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 108
    const-string v2, "\n"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 109
    array-length v2, v4

    invoke-static {v2, v8}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 110
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v5, :cond_3

    .line 111
    invoke-virtual {p0, v2}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->b(I)Ljava/lang/String;

    move-result-object v6

    aget-object v7, v4, v2

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 112
    aget-object v6, v4, v2

    invoke-virtual {p0, v2, v6}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a(ILjava/lang/String;)V

    .line 110
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 77
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 84
    :cond_2
    const/4 v6, 0x0

    .line 85
    const/4 v5, 0x0

    .line 88
    const/4 v4, 0x0

    .line 90
    const/4 v3, 0x0

    .line 91
    const/4 v2, 0x0

    goto :goto_1

    .line 115
    :cond_3
    if-lez v5, :cond_4

    const/4 v2, 0x1

    .line 187
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->c()I

    move-result v4

    if-ge v5, v4, :cond_1a

    .line 188
    const/4 v4, 0x0

    invoke-virtual {p0, v5, v4}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a(ILjava/lang/String;)V

    .line 187
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 115
    :cond_4
    const/4 v2, 0x0

    goto :goto_3

    .line 116
    :cond_5
    if-eqz v6, :cond_20

    .line 118
    if-eqz v4, :cond_e

    .line 120
    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->H()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 121
    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->G()Ljava/util/List;

    move-result-object v2

    const/4 v4, 0x0

    invoke-static {p0, v2, v4}, Lcom/google/android/gms/smart_profile/header/HeaderUtil;->a(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Ljava/util/List;I)I

    move-result v2

    .line 124
    :goto_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->t()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->s()Lcom/google/android/gms/people/identity/models/r;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/people/identity/models/r;->k()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->s()Lcom/google/android/gms/people/identity/models/r;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/people/identity/models/r;->l()Lcom/google/android/gms/people/identity/models/u;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/people/identity/models/u;->c()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-interface {v4}, Lcom/google/android/gms/people/identity/models/u;->b()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v4, v10, v12

    if-lez v4, :cond_a

    const/4 v4, 0x1

    :goto_5
    if-eqz v4, :cond_6

    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->s()Lcom/google/android/gms/people/identity/models/r;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/people/identity/models/r;->l()Lcom/google/android/gms/people/identity/models/u;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/people/identity/models/u;->b()J

    move-result-wide v10

    long-to-int v4, v10

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v9, Lcom/google/android/gms/n;->w:I

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v7, v9, v4, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->t()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->s()Lcom/google/android/gms/people/identity/models/r;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/people/identity/models/r;->k()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->s()Lcom/google/android/gms/people/identity/models/r;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/people/identity/models/r;->l()Lcom/google/android/gms/people/identity/models/u;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/people/identity/models/u;->e()Z

    move-result v7

    if-eqz v7, :cond_c

    invoke-interface {v4}, Lcom/google/android/gms/people/identity/models/u;->d()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v4, v10, v12

    if-lez v4, :cond_c

    const/4 v4, 0x1

    :goto_6
    if-eqz v4, :cond_8

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_7

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v7, Lcom/google/android/gms/p;->wY:I

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->s()Lcom/google/android/gms/people/identity/models/r;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/people/identity/models/r;->l()Lcom/google/android/gms/people/identity/models/u;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/people/identity/models/u;->d()J

    move-result-wide v10

    long-to-int v4, v10

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v9, Lcom/google/android/gms/n;->x:I

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v7, v9, v4, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_9

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v2, v4}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a(ILjava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    .line 126
    :cond_9
    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->J()Z

    move-result v4

    if-eqz v4, :cond_1d

    if-ge v2, v8, :cond_1d

    .line 127
    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->I()Ljava/util/List;

    move-result-object v4

    invoke-static {p0, v4, v2}, Lcom/google/android/gms/smart_profile/header/HeaderUtil;->b(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Ljava/util/List;I)I

    move-result v2

    move v5, v2

    .line 185
    :goto_7
    if-lez v5, :cond_19

    const/4 v2, 0x1

    goto/16 :goto_3

    .line 124
    :cond_a
    const/4 v4, 0x0

    goto/16 :goto_5

    :cond_b
    const/4 v4, 0x0

    goto/16 :goto_5

    :cond_c
    const/4 v4, 0x0

    goto :goto_6

    :cond_d
    const/4 v4, 0x0

    goto :goto_6

    .line 132
    :cond_e
    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->t()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->s()Lcom/google/android/gms/people/identity/models/r;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/people/identity/models/r;->e()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_10

    .line 134
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_f

    .line 135
    const/4 v4, 0x0

    invoke-virtual {p0, v4, v2}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a(ILjava/lang/String;)V

    .line 136
    sget v2, Lcom/google/android/gms/h;->cN:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->c(I)V

    .line 139
    :cond_f
    const/4 v5, 0x1

    .line 142
    :cond_10
    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->H()Z

    move-result v2

    if-eqz v2, :cond_11

    if-ge v5, v8, :cond_11

    .line 143
    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->G()Ljava/util/List;

    move-result-object v2

    invoke-static {p0, v2, v5}, Lcom/google/android/gms/smart_profile/header/HeaderUtil;->a(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Ljava/util/List;I)I

    move-result v5

    .line 146
    :cond_11
    const/4 v2, 0x0

    .line 147
    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->z()Z

    move-result v4

    if-eqz v4, :cond_12

    if-ge v5, v8, :cond_12

    .line 148
    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->y()Ljava/util/List;

    move-result-object v2

    const/4 v4, 0x1

    invoke-static {p0, v2, v4, v5}, Lcom/google/android/gms/smart_profile/header/HeaderUtil;->a(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Ljava/util/List;ZI)I

    move-result v4

    .line 150
    if-eq v4, v5, :cond_16

    const/4 v2, 0x1

    :goto_8
    move v5, v4

    .line 154
    :cond_12
    const/4 v4, 0x0

    .line 155
    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->D()Z

    move-result v7

    if-eqz v7, :cond_13

    if-ge v5, v8, :cond_13

    .line 156
    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->C()Ljava/util/List;

    move-result-object v4

    const/4 v7, 0x1

    invoke-static {p0, v4, v7, v5}, Lcom/google/android/gms/smart_profile/header/HeaderUtil;->c(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Ljava/util/List;ZI)I

    move-result v7

    .line 158
    if-eq v7, v5, :cond_17

    const/4 v4, 0x1

    :goto_9
    move v5, v7

    .line 162
    :cond_13
    const/4 v7, 0x0

    .line 163
    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->z()Z

    move-result v9

    if-eqz v9, :cond_14

    if-ge v5, v8, :cond_14

    .line 164
    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->y()Ljava/util/List;

    move-result-object v7

    const/4 v9, 0x1

    invoke-static {p0, v7, v9, v5}, Lcom/google/android/gms/smart_profile/header/HeaderUtil;->b(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Ljava/util/List;ZI)I

    move-result v7

    .line 166
    if-eq v7, v5, :cond_18

    const/4 v5, 0x1

    :goto_a
    move v14, v5

    move v5, v7

    move v7, v14

    .line 170
    :cond_14
    if-nez v2, :cond_1e

    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->z()Z

    move-result v2

    if-eqz v2, :cond_1e

    if-ge v5, v8, :cond_1e

    .line 171
    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->y()Ljava/util/List;

    move-result-object v2

    const/4 v9, 0x0

    invoke-static {p0, v2, v9, v5}, Lcom/google/android/gms/smart_profile/header/HeaderUtil;->a(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Ljava/util/List;ZI)I

    move-result v2

    .line 175
    :goto_b
    if-nez v7, :cond_15

    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->z()Z

    move-result v5

    if-eqz v5, :cond_15

    if-ge v2, v8, :cond_15

    .line 176
    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->y()Ljava/util/List;

    move-result-object v5

    const/4 v7, 0x0

    invoke-static {p0, v5, v7, v2}, Lcom/google/android/gms/smart_profile/header/HeaderUtil;->b(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Ljava/util/List;ZI)I

    move-result v2

    .line 180
    :cond_15
    if-nez v4, :cond_1d

    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->D()Z

    move-result v4

    if-eqz v4, :cond_1d

    if-ge v2, v8, :cond_1d

    .line 181
    invoke-interface {v6}, Lcom/google/android/gms/people/identity/models/Person;->C()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {p0, v4, v5, v2}, Lcom/google/android/gms/smart_profile/header/HeaderUtil;->c(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Ljava/util/List;ZI)I

    move-result v2

    move v5, v2

    goto/16 :goto_7

    .line 150
    :cond_16
    const/4 v2, 0x0

    goto :goto_8

    .line 158
    :cond_17
    const/4 v4, 0x0

    goto :goto_9

    .line 166
    :cond_18
    const/4 v5, 0x0

    goto :goto_a

    .line 185
    :cond_19
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 192
    :cond_1a
    move/from16 v0, p4

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a(Z)V

    .line 194
    if-nez v3, :cond_1b

    if-eqz v2, :cond_1c

    :cond_1b
    const/4 v2, 0x1

    :goto_c
    invoke-virtual {p0, v2}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->d(Z)V

    .line 195
    return-void

    .line 194
    :cond_1c
    const/4 v2, 0x0

    goto :goto_c

    :cond_1d
    move v5, v2

    goto/16 :goto_7

    :cond_1e
    move v2, v5

    goto :goto_b

    :cond_1f
    move v2, v5

    goto/16 :goto_4

    :cond_20
    move v2, v7

    goto/16 :goto_3
.end method

.method private static b(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Ljava/util/List;I)I
    .locals 3

    .prologue
    .line 264
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 265
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/aa;

    .line 266
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/aa;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 267
    invoke-virtual {p0, p2}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/aa;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 268
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/aa;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a(ILjava/lang/String;)V

    .line 270
    :cond_0
    add-int/lit8 p2, p2, 0x1

    .line 274
    :cond_1
    return p2

    .line 264
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private static b(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Ljava/util/List;ZI)I
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 317
    move v1, v2

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 318
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/q;

    .line 319
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->d()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v4

    .line 320
    :goto_1
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->m()Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "school"

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v5, v4

    :goto_2
    if-eqz v5, :cond_5

    if-ne p2, v3, :cond_5

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->g()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 323
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-eqz p2, :cond_4

    sget v1, Lcom/google/android/gms/p;->wQ:I

    :goto_3
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 326
    invoke-virtual {p0, p3}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 327
    new-array v3, v4, [Ljava/lang/Object;

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/q;->f()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p3, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a(ILjava/lang/String;)V

    .line 330
    :cond_0
    add-int/lit8 p3, p3, 0x1

    .line 334
    :cond_1
    return p3

    :cond_2
    move v3, v2

    .line 319
    goto :goto_1

    :cond_3
    move v5, v2

    .line 320
    goto :goto_2

    .line 323
    :cond_4
    sget v1, Lcom/google/android/gms/p;->wR:I

    goto :goto_3

    .line 317
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private static c(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Ljava/util/List;ZI)I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 339
    move v1, v2

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 340
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/t;

    .line 341
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/t;->d()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/t;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v4

    .line 342
    :goto_1
    if-ne p2, v3, :cond_4

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/t;->g()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/t;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 344
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-eqz p2, :cond_3

    sget v1, Lcom/google/android/gms/p;->xe:I

    :goto_2
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 347
    invoke-virtual {p0, p3}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/t;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 348
    new-array v3, v4, [Ljava/lang/Object;

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/t;->f()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p3, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a(ILjava/lang/String;)V

    .line 351
    :cond_0
    add-int/lit8 p3, p3, 0x1

    .line 355
    :cond_1
    return p3

    :cond_2
    move v3, v2

    .line 341
    goto :goto_1

    .line 344
    :cond_3
    sget v1, Lcom/google/android/gms/p;->xf:I

    goto :goto_2

    .line 339
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
