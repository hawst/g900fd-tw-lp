.class public abstract Lcom/google/android/gms/kids/common/c/a;
.super Lcom/google/android/gms/kids/common/c/b;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/gms/kids/common/c/b;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 24
    check-cast p1, Lcom/google/android/gms/kids/common/service/a;

    invoke-static {}, Lcom/google/android/gms/kids/common/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/kids/common/d;->a(Landroid/content/Context;)Lcom/google/android/gms/kids/account/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/kids/account/b;->b()Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "AbstractKidsOperation"

    const-string v1, "No account found."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "AbstractKidsOperation"

    const-string v1, "Invoking performAction"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0, p1}, Lcom/google/android/gms/kids/common/c/a;->a(Lcom/google/android/gms/kids/common/service/a;)V

    goto :goto_0
.end method

.method public abstract a(Lcom/google/android/gms/kids/common/service/a;)V
.end method
