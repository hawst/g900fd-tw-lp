.class final Lcom/google/android/gms/cast/c/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/c/h;

.field private final b:Z

.field private final c:Lcom/google/android/gms/cast/CastDevice;

.field private final d:Ljava/util/Set;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/c/h;ZLcom/google/android/gms/cast/CastDevice;Ljava/util/Set;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 554
    iput-object p1, p0, Lcom/google/android/gms/cast/c/l;->a:Lcom/google/android/gms/cast/c/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 555
    iput-boolean p2, p0, Lcom/google/android/gms/cast/c/l;->b:Z

    .line 556
    iput-object p3, p0, Lcom/google/android/gms/cast/c/l;->c:Lcom/google/android/gms/cast/CastDevice;

    .line 557
    iput-object p4, p0, Lcom/google/android/gms/cast/c/l;->d:Ljava/util/Set;

    .line 558
    iput-object p5, p0, Lcom/google/android/gms/cast/c/l;->e:Ljava/lang/String;

    .line 559
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 563
    iget-boolean v0, p0, Lcom/google/android/gms/cast/c/l;->b:Z

    if-eqz v0, :cond_0

    .line 564
    iget-object v0, p0, Lcom/google/android/gms/cast/c/l;->a:Lcom/google/android/gms/cast/c/h;

    invoke-static {v0}, Lcom/google/android/gms/cast/c/h;->h(Lcom/google/android/gms/cast/c/h;)Lcom/google/android/gms/cast/c/m;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/c/l;->c:Lcom/google/android/gms/cast/CastDevice;

    iget-object v2, p0, Lcom/google/android/gms/cast/c/l;->d:Ljava/util/Set;

    iget-object v3, p0, Lcom/google/android/gms/cast/c/l;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/cast/c/m;->a(Lcom/google/android/gms/cast/CastDevice;Ljava/util/Set;Ljava/lang/String;)V

    .line 569
    :goto_0
    return-void

    .line 567
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/l;->a:Lcom/google/android/gms/cast/c/h;

    invoke-static {v0}, Lcom/google/android/gms/cast/c/h;->h(Lcom/google/android/gms/cast/c/h;)Lcom/google/android/gms/cast/c/m;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/c/l;->c:Lcom/google/android/gms/cast/CastDevice;

    invoke-interface {v0, v1}, Lcom/google/android/gms/cast/c/m;->a(Lcom/google/android/gms/cast/CastDevice;)V

    goto :goto_0
.end method
