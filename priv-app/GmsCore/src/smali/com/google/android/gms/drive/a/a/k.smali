.class public final Lcom/google/android/gms/drive/a/a/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/drive/database/model/be;

.field final b:Lcom/google/android/gms/drive/a/g;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/database/model/be;Lcom/google/android/gms/drive/a/g;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/be;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/k;->a:Lcom/google/android/gms/drive/database/model/be;

    .line 27
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/g;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/k;->b:Lcom/google/android/gms/drive/a/g;

    .line 28
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 48
    const-string v0, "ExecutingAction[%s, %s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/drive/a/a/k;->a:Lcom/google/android/gms/drive/database/model/be;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/drive/a/a/k;->b:Lcom/google/android/gms/drive/a/g;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
