.class public final Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/List;

.field public final b:Ljava/util/Set;

.field private c:Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientAclDetailsEntity;

.field private d:Ljava/util/List;

.field private e:Ljava/util/List;

.field private f:Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientLoggedRhsComponentEntity;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->b:Ljava/util/Set;

    .line 86
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bw;
    .locals 7

    .prologue
    .line 147
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->b:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->c:Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientAclDetailsEntity;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->d:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->e:Ljava/util/List;

    iget-object v6, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->f:Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientLoggedRhsComponentEntity;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;-><init>(Ljava/util/Set;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientAclDetailsEntity;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientLoggedRhsComponentEntity;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/bt;)Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;
    .locals 2

    .prologue
    .line 94
    check-cast p1, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientAclDetailsEntity;

    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->c:Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientAclDetailsEntity;

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->b:Ljava/util/Set;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 96
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/cf;)Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;
    .locals 2

    .prologue
    .line 138
    check-cast p1, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientLoggedRhsComponentEntity;

    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->f:Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientLoggedRhsComponentEntity;

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->b:Ljava/util/Set;

    const/16 v1, 0x15

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 140
    return-object p0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;
    .locals 2

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->d:Ljava/util/List;

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->b:Ljava/util/Set;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 108
    return-object p0
.end method

.method public final b(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;
    .locals 2

    .prologue
    .line 128
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->e:Ljava/util/List;

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->b:Ljava/util/Set;

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 130
    return-object p0
.end method
