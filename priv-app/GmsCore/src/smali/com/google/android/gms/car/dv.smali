.class final Lcom/google/android/gms/car/dv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/dt;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/dt;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/gms/car/dv;->a:Lcom/google/android/gms/car/dt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 98
    const-string v0, "CAR.CLIENT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    const-string v0, "CAR.CLIENT"

    const-string v1, "DefaultConnectionCallbacks#onConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/dv;->a:Lcom/google/android/gms/car/dt;

    invoke-virtual {v0}, Lcom/google/android/gms/car/dt;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/jx;

    iget-object v1, p0, Lcom/google/android/gms/car/dv;->a:Lcom/google/android/gms/car/dt;

    invoke-static {v1}, Lcom/google/android/gms/car/dt;->a(Lcom/google/android/gms/car/dt;)Lcom/google/android/gms/car/dx;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/jx;->a(Lcom/google/android/gms/car/lh;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/car/dv;->a:Lcom/google/android/gms/car/dt;

    invoke-static {v0}, Lcom/google/android/gms/car/dt;->b(Lcom/google/android/gms/car/dt;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 112
    :cond_1
    :goto_0
    return-void

    .line 106
    :catch_0
    move-exception v0

    const-string v0, "CAR.CLIENT"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 107
    const-string v0, "CAR.CLIENT"

    const-string v1, "service disconnected while onConnected is called"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 109
    :catch_1
    move-exception v0

    .line 110
    iget-object v1, p0, Lcom/google/android/gms/car/dv;->a:Lcom/google/android/gms/car/dt;

    invoke-static {v1, v0}, Lcom/google/android/gms/car/dt;->a(Lcom/google/android/gms/car/dt;Landroid/os/RemoteException;)V

    goto :goto_0
.end method

.method public final f_(I)V
    .locals 2

    .prologue
    .line 116
    const-string v0, "CAR.CLIENT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    const-string v0, "CAR.CLIENT"

    const-string v1, "DefaultConnectionCallbacks#onConnectionSuspended"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_0
    return-void
.end method
