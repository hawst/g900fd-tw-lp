.class public abstract Lcom/google/android/gms/cast_mirroring/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Ljava/lang/String;


# instance fields
.field protected b:Lcom/google/android/gms/cast/media/CastMirroringProvider;

.field protected c:Lcom/google/android/gms/cast_mirroring/b/a;

.field protected d:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "CastMirrorOperation"

    sput-object v0, Lcom/google/android/gms/cast_mirroring/e;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/cast/media/CastMirroringProvider;Lcom/google/android/gms/cast_mirroring/b/a;)V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/cast_mirroring/e;->d:Ljava/util/concurrent/CountDownLatch;

    .line 47
    iput-object p1, p0, Lcom/google/android/gms/cast_mirroring/e;->b:Lcom/google/android/gms/cast/media/CastMirroringProvider;

    .line 48
    iput-object p2, p0, Lcom/google/android/gms/cast_mirroring/e;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    .line 49
    return-void
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;)V
.end method
