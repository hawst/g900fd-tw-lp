.class final Lcom/google/android/gms/location/copresence/internal/a;
.super Lcom/google/android/gms/location/copresence/k;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/location/copresence/internal/BatchImpl;


# direct methods
.method constructor <init>(Lcom/google/android/gms/location/copresence/internal/BatchImpl;Lcom/google/android/gms/common/api/v;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/google/android/gms/location/copresence/internal/a;->a:Lcom/google/android/gms/location/copresence/internal/BatchImpl;

    invoke-direct {p0, p2}, Lcom/google/android/gms/location/copresence/k;-><init>(Lcom/google/android/gms/common/api/v;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/common/api/h;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 168
    check-cast p1, Lcom/google/android/gms/location/internal/w;

    invoke-virtual {p1}, Lcom/google/android/gms/location/internal/w;->g()Lcom/google/android/gms/location/copresence/internal/c;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/location/copresence/internal/a;->a:Lcom/google/android/gms/location/copresence/internal/BatchImpl;

    invoke-virtual {v4}, Lcom/google/android/gms/location/copresence/internal/c;->a()Lcom/google/android/gms/location/copresence/internal/l;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0xd

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {p0, v0}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, v4, Lcom/google/android/gms/location/copresence/internal/c;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.google.android.gms"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v5, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/internal/Operation;

    iget v3, v0, Lcom/google/android/gms/location/copresence/internal/Operation;->a:I

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    :pswitch_1
    iget-object v3, v0, Lcom/google/android/gms/location/copresence/internal/Operation;->c:Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;

    if-eqz v3, :cond_1

    iget-object v0, v0, Lcom/google/android/gms/location/copresence/internal/Operation;->c:Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;

    iget v0, v0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->a:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_2
    if-nez v0, :cond_2

    const-string v0, "0P users of copresence may not unpublishAll or unsubscribeAll."

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0xa

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {p0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    :cond_2
    iget-object v0, v4, Lcom/google/android/gms/location/copresence/internal/c;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {p0, v0}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_2
    iget-object v3, v0, Lcom/google/android/gms/location/copresence/internal/Operation;->e:Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

    if-eqz v3, :cond_1

    iget-object v0, v0, Lcom/google/android/gms/location/copresence/internal/Operation;->e:Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

    iget v0, v0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    move v0, v1

    goto :goto_2

    :cond_3
    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    invoke-virtual {v4}, Lcom/google/android/gms/location/copresence/internal/c;->a()Lcom/google/android/gms/location/copresence/internal/l;

    move-result-object v0

    iget-object v1, v4, Lcom/google/android/gms/location/copresence/internal/c;->d:Landroid/os/IBinder;

    if-nez v1, :cond_5

    new-instance v1, Landroid/os/Binder;

    invoke-direct {v1}, Landroid/os/Binder;-><init>()V

    iput-object v1, v4, Lcom/google/android/gms/location/copresence/internal/c;->d:Landroid/os/IBinder;

    :cond_5
    iget-object v1, v4, Lcom/google/android/gms/location/copresence/internal/c;->d:Landroid/os/IBinder;

    iget-object v2, v4, Lcom/google/android/gms/location/copresence/internal/c;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v4, Lcom/google/android/gms/location/copresence/internal/c;->b:Ljava/lang/String;

    iget-object v4, v4, Lcom/google/android/gms/location/copresence/internal/c;->c:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/gms/location/copresence/internal/d;->a(Lcom/google/android/gms/common/api/m;)Lcom/google/android/gms/location/copresence/internal/d;

    move-result-object v6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/location/copresence/internal/l;->a(Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/BatchImpl;Lcom/google/android/gms/location/copresence/internal/i;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
