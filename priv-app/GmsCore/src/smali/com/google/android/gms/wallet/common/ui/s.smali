.class public final Lcom/google/android/gms/wallet/common/ui/s;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final synthetic a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;)V
    .locals 0

    .prologue
    .line 306
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/s;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 307
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;B)V
    .locals 0

    .prologue
    .line 301
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/s;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;)V

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/wallet/common/ui/s;
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/s;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iput p1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->g:I

    .line 388
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/s;
    .locals 2

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/s;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    invoke-static {p1}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->b(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->c:I

    .line 351
    return-object p0
.end method

.method public final a(Ljava/util/ArrayList;)Lcom/google/android/gms/wallet/common/ui/s;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/s;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iput-object p1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->h:Ljava/util/ArrayList;

    .line 397
    return-object p0
.end method

.method public final a(Ljava/util/Collection;)Lcom/google/android/gms/wallet/common/ui/s;
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/s;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iput-object p1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->i:Ljava/util/Collection;

    .line 406
    return-object p0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/gms/wallet/common/ui/s;
    .locals 2

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/s;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    invoke-static {p1}, Lcom/google/android/gms/wallet/common/a/e;->a(Ljava/util/List;)[I

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->b:[I

    .line 333
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/wallet/common/ui/s;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/s;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a:Z

    .line 314
    return-object p0
.end method

.method public final b(Z)Lcom/google/android/gms/wallet/common/ui/s;
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/s;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->j:Z

    .line 415
    return-object p0
.end method

.method public final c(Z)Lcom/google/android/gms/wallet/common/ui/s;
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/s;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->k:Z

    .line 424
    return-object p0
.end method
