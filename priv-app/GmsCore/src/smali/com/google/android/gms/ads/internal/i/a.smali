.class public final Lcom/google/android/gms/ads/internal/i/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field final a:Ljava/lang/Object;

.field b:Lcom/google/android/gms/ads/internal/util/a/e;

.field c:Lcom/google/android/gms/ads/internal/util/a/a;

.field d:Lcom/google/android/gms/ads/internal/a/p;

.field e:I

.field private final f:Landroid/content/Context;

.field private final g:Ljava/lang/String;

.field private final h:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

.field private final i:Ljava/util/WeakHashMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->a:Ljava/lang/Object;

    .line 45
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/ads/internal/i/a;->e:I

    .line 49
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/i/a;->g:Ljava/lang/String;

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->f:Landroid/content/Context;

    .line 51
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/i/a;->h:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    .line 52
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->i:Ljava/util/WeakHashMap;

    .line 53
    new-instance v0, Lcom/google/android/gms/ads/internal/util/a/d;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/util/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->b:Lcom/google/android/gms/ads/internal/util/a/e;

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Ljava/lang/String;Lcom/google/android/gms/ads/internal/util/a/e;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/ads/internal/i/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Ljava/lang/String;)V

    .line 60
    iput-object p4, p0, Lcom/google/android/gms/ads/internal/i/a;->b:Lcom/google/android/gms/ads/internal/util/a/e;

    .line 61
    return-void
.end method

.method private d(Lcom/google/android/gms/ads/internal/util/a/a;)V
    .locals 4

    .prologue
    .line 68
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/ads/internal/i/a;->e:I

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/i/a;->h:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    new-instance v2, Lcom/google/android/gms/ads/internal/a/u;

    invoke-direct {v2, v0, v1}, Lcom/google/android/gms/ads/internal/a/u;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V

    iput-object v2, p0, Lcom/google/android/gms/ads/internal/i/a;->d:Lcom/google/android/gms/ads/internal/a/p;

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->d:Lcom/google/android/gms/ads/internal/a/p;

    new-instance v1, Lcom/google/android/gms/ads/internal/i/b;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/ads/internal/i/b;-><init>(Lcom/google/android/gms/ads/internal/i/a;Lcom/google/android/gms/ads/internal/util/a/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/ads/internal/a/p;->a(Lcom/google/android/gms/ads/internal/a/q;)V

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->d:Lcom/google/android/gms/ads/internal/a/p;

    const-string v1, "/jsLoaded"

    new-instance v2, Lcom/google/android/gms/ads/internal/i/d;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/ads/internal/i/d;-><init>(Lcom/google/android/gms/ads/internal/i/a;Lcom/google/android/gms/ads/internal/util/a/a;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/ads/internal/a/p;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 115
    new-instance v0, Lcom/google/android/gms/ads/internal/util/u;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/util/u;-><init>()V

    .line 117
    new-instance v1, Lcom/google/android/gms/ads/internal/i/f;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/ads/internal/i/f;-><init>(Lcom/google/android/gms/ads/internal/i/a;Lcom/google/android/gms/ads/internal/util/u;)V

    .line 128
    iput-object v1, v0, Lcom/google/android/gms/ads/internal/util/u;->a:Ljava/lang/Object;

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->d:Lcom/google/android/gms/ads/internal/a/p;

    const-string v2, "/requestReload"

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/ads/internal/a/p;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 131
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->g:Ljava/lang/String;

    const-string v1, ".js"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->d:Lcom/google/android/gms/ads/internal/a/p;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/i/a;->g:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/gms/ads/internal/a/p;->a(Ljava/lang/String;)V

    .line 137
    :goto_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 138
    new-instance v1, Lcom/google/android/gms/ads/internal/i/g;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/ads/internal/i/g;-><init>(Lcom/google/android/gms/ads/internal/i/a;Lcom/google/android/gms/ads/internal/util/a/a;)V

    sget v2, Lcom/google/android/gms/ads/internal/i/i;->a:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 151
    return-void

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->d:Lcom/google/android/gms/ads/internal/a/p;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/i/a;->g:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/gms/ads/internal/a/p;->b(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/ads/internal/util/a/a;
    .locals 3

    .prologue
    .line 154
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/i/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 155
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->c:Lcom/google/android/gms/ads/internal/util/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->c:Lcom/google/android/gms/ads/internal/util/a/a;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/util/a/a;->b()I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 157
    :cond_0
    new-instance v0, Lcom/google/android/gms/ads/internal/util/a/f;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/util/a/f;-><init>()V

    .line 158
    iput-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->c:Lcom/google/android/gms/ads/internal/util/a/a;

    .line 159
    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/internal/i/a;->d(Lcom/google/android/gms/ads/internal/util/a/a;)V

    .line 160
    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/i/a;->b(Lcom/google/android/gms/ads/internal/util/a/a;)V

    .line 161
    monitor-exit v1

    .line 175
    :goto_0
    return-object v0

    .line 162
    :cond_1
    iget v0, p0, Lcom/google/android/gms/ads/internal/i/a;->e:I

    if-nez v0, :cond_2

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->c:Lcom/google/android/gms/ads/internal/util/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/i/a;->b(Lcom/google/android/gms/ads/internal/util/a/a;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->c:Lcom/google/android/gms/ads/internal/util/a/a;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 177
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 165
    :cond_2
    :try_start_1
    iget v0, p0, Lcom/google/android/gms/ads/internal/i/a;->e:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 166
    new-instance v0, Lcom/google/android/gms/ads/internal/util/a/f;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/util/a/f;-><init>()V

    .line 167
    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/internal/i/a;->d(Lcom/google/android/gms/ads/internal/util/a/a;)V

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->c:Lcom/google/android/gms/ads/internal/util/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/i/a;->b(Lcom/google/android/gms/ads/internal/util/a/a;)V

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->c:Lcom/google/android/gms/ads/internal/util/a/a;

    monitor-exit v1

    goto :goto_0

    .line 170
    :cond_3
    iget v0, p0, Lcom/google/android/gms/ads/internal/i/a;->e:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_4

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->c:Lcom/google/android/gms/ads/internal/util/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/i/a;->b(Lcom/google/android/gms/ads/internal/util/a/a;)V

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->c:Lcom/google/android/gms/ads/internal/util/a/a;

    monitor-exit v1

    goto :goto_0

    .line 174
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->c:Lcom/google/android/gms/ads/internal/util/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/i/a;->b(Lcom/google/android/gms/ads/internal/util/a/a;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->c:Lcom/google/android/gms/ads/internal/util/a/a;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/ads/internal/util/a/a;)V
    .locals 2

    .prologue
    .line 180
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/i/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 181
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/ads/internal/i/a;->c(Lcom/google/android/gms/ads/internal/util/a/a;)V

    .line 182
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final b(Lcom/google/android/gms/ads/internal/util/a/a;)V
    .locals 3

    .prologue
    .line 186
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/i/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 187
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->i:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 188
    if-nez v0, :cond_0

    .line 189
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 191
    :cond_0
    const-string v2, "Incremented use-counter for js engine."

    invoke-static {v2}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;)V

    .line 192
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 193
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/i/a;->i:Ljava/util/WeakHashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final c(Lcom/google/android/gms/ads/internal/util/a/a;)V
    .locals 3

    .prologue
    .line 198
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/i/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 199
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->i:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 200
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_1

    .line 202
    :cond_0
    monitor-exit v1

    .line 218
    :goto_0
    return-void

    .line 204
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 205
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz v2, :cond_2

    .line 206
    const-string v2, "Decremented use-counter for js engine."

    invoke-static {v2}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;)V

    .line 207
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/i/a;->i:Ljava/util/WeakHashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 209
    :cond_2
    :try_start_1
    const-string v0, "Removing js engine."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;)V

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/a;->i:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    new-instance v0, Lcom/google/android/gms/ads/internal/i/h;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/internal/i/h;-><init>(Lcom/google/android/gms/ads/internal/i/a;)V

    new-instance v2, Lcom/google/android/gms/ads/internal/util/a/c;

    invoke-direct {v2}, Lcom/google/android/gms/ads/internal/util/a/c;-><init>()V

    invoke-interface {p1, v0, v2}, Lcom/google/android/gms/ads/internal/util/a/a;->a(Lcom/google/android/gms/ads/internal/util/a/e;Lcom/google/android/gms/ads/internal/util/a/b;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
