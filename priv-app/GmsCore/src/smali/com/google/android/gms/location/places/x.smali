.class public Lcom/google/android/gms/location/places/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/location/places/u;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/LinkedHashSet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 212
    const-class v0, Lcom/google/android/gms/location/places/x;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/x;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/location/places/x;->b:Ljava/util/LinkedHashSet;

    return-void
.end method

.method private static a(Ljava/lang/Iterable;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 215
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 216
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 217
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "<br>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 219
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/location/places/i;)V
    .locals 5

    .prologue
    .line 35
    iget-object v1, p0, Lcom/google/android/gms/location/places/x;->b:Ljava/util/LinkedHashSet;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/location/places/i;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/PlaceLikelihood;

    iget-object v3, p0, Lcom/google/android/gms/location/places/x;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceLikelihood;->b()Lcom/google/android/gms/location/places/f;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/internal/PlaceImpl;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->m()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/location/places/x;->a:Ljava/lang/String;

    const/4 v3, 0x3

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/location/places/x;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Attributions: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/location/places/x;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/gms/location/places/x;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;Landroid/app/PendingIntent;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 312
    new-instance v0, Lcom/google/android/gms/location/places/ae;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/location/places/ae;-><init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/common/api/v;Landroid/app/PendingIntent;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/places/NearbyAlertRequest;Landroid/app/PendingIntent;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 344
    new-instance v0, Lcom/google/android/gms/location/places/af;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/location/places/af;-><init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/places/NearbyAlertRequest;Landroid/app/PendingIntent;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/places/PlaceFilter;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 255
    new-instance v0, Lcom/google/android/gms/location/places/aa;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/location/places/aa;-><init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/places/PlaceFilter;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/places/UserAddedPlace;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 234
    new-instance v0, Lcom/google/android/gms/location/places/y;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/location/places/y;-><init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/places/UserAddedPlace;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/places/UserDataType;)Lcom/google/android/gms/common/api/am;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 300
    new-instance v0, Lcom/google/android/gms/location/places/ad;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/places/ad;-><init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/places/UserDataType;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/util/List;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;I)Lcom/google/android/gms/common/api/am;
    .locals 7

    .prologue
    .line 367
    new-instance v0, Lcom/google/android/gms/location/places/z;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p5

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/location/places/z;-><init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/lang/String;ILcom/google/android/gms/location/places/PlaceFilter;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;)Lcom/google/android/gms/common/api/am;
    .locals 6

    .prologue
    .line 287
    new-instance v0, Lcom/google/android/gms/location/places/ac;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/places/ac;-><init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 267
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    new-instance v0, Lcom/google/android/gms/location/places/ab;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/location/places/ab;-><init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/common/api/v;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 246
    iget-object v1, p0, Lcom/google/android/gms/location/places/x;->b:Ljava/util/LinkedHashSet;

    monitor-enter v1

    .line 247
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/location/places/x;->b:Ljava/util/LinkedHashSet;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 248
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    invoke-static {v0}, Lcom/google/android/gms/location/places/x;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 248
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
