.class public abstract Lcom/google/android/gms/games/ui/q;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;
.implements Lcom/google/android/gms/games/multiplayer/g;
.implements Lcom/google/android/gms/games/multiplayer/turnbased/b;
.implements Lcom/google/android/gms/games/quest/d;
.implements Lcom/google/android/gms/games/request/c;
.implements Lcom/google/android/gms/games/ui/a/b;
.implements Lcom/google/android/gms/games/ui/b/a/g;
.implements Lcom/google/android/gms/games/ui/bj;
.implements Lcom/google/android/gms/games/ui/cn;
.implements Lcom/google/android/gms/games/ui/common/players/i;
.implements Lcom/google/android/gms/games/ui/d/k;


# static fields
.field static f:Landroid/graphics/Bitmap;

.field private static final i:Landroid/content/IntentFilter;


# instance fields
.field protected a:Z

.field public b:Lcom/google/android/gms/games/ui/bh;

.field public c:Z

.field public d:Ljava/lang/String;

.field protected e:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field protected g:I

.field protected h:Z

.field private final j:I

.field private k:I

.field private l:Lcom/google/android/gms/common/api/v;

.field private m:Z

.field private n:Landroid/widget/ProgressBar;

.field private o:Z

.field private p:Landroid/app/Dialog;

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Landroid/support/v4/app/Fragment;

.field private w:Lcom/google/android/gms/games/ui/a/d;

.field private x:Lcom/google/android/gms/games/ui/r;

.field private y:Lcom/google/android/gms/games/ui/common/players/h;

.field private z:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 92
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 95
    sput-object v0, Lcom/google/android/gms/games/ui/q;->i:Landroid/content/IntentFilter;

    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 96
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 6

    .prologue
    .line 185
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/ui/q;-><init>(IIIIZ)V

    .line 187
    return-void
.end method

.method public constructor <init>(IIIIZ)V
    .locals 7

    .prologue
    .line 205
    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/ui/q;-><init>(IIIIZZ)V

    .line 207
    return-void
.end method

.method public constructor <init>(IIIIZZ)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 228
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 166
    const/16 v1, 0xff

    iput v1, p0, Lcom/google/android/gms/games/ui/q;->g:I

    .line 168
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->h:Z

    .line 229
    new-instance v1, Lcom/google/android/gms/games/ui/bh;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/games/ui/bh;-><init>(Landroid/app/Activity;II)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    .line 230
    iput-boolean p5, p0, Lcom/google/android/gms/games/ui/q;->m:Z

    .line 232
    if-gtz p3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->f()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->u()I

    move-result v1

    if-lez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 235
    iput p3, p0, Lcom/google/android/gms/games/ui/q;->j:I

    .line 236
    iput p4, p0, Lcom/google/android/gms/games/ui/q;->k:I

    .line 237
    iput-boolean p6, p0, Lcom/google/android/gms/games/ui/q;->q:Z

    .line 238
    return-void
.end method

.method private V()V
    .locals 1

    .prologue
    .line 1108
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->v:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/ch;

    if-eqz v0, :cond_1

    .line 1109
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->v:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/ch;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ch;->K_()V

    .line 1114
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->F()V

    .line 1115
    return-void

    .line 1110
    :cond_1
    instance-of v0, p0, Lcom/google/android/gms/games/ui/ch;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 1112
    check-cast v0, Lcom/google/android/gms/games/ui/ch;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ch;->K_()V

    goto :goto_0
.end method

.method private W()V
    .locals 1

    .prologue
    .line 1122
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->v:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/cj;

    if-eqz v0, :cond_1

    .line 1123
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->v:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/cj;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/cj;->M_()V

    .line 1128
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->F()V

    .line 1129
    return-void

    .line 1124
    :cond_1
    instance-of v0, p0, Lcom/google/android/gms/games/ui/cj;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 1126
    check-cast v0, Lcom/google/android/gms/games/ui/cj;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/cj;->M_()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/q;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/q;)Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->c:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/q;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/q;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final A()V
    .locals 2

    .prologue
    .line 915
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->m:Z

    const-string v1, "This method can only be called if we have a progressbar in the actionbar"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 917
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->o:Z

    .line 918
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->n:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 919
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->n:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 921
    :cond_0
    return-void
.end method

.method public final B()Lcom/google/android/gms/games/ui/a/a;
    .locals 1

    .prologue
    .line 930
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->w:Lcom/google/android/gms/games/ui/a/d;

    return-object v0
.end method

.method public final C()Lcom/google/android/gms/games/ui/b/a/f;
    .locals 1

    .prologue
    .line 935
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->w:Lcom/google/android/gms/games/ui/a/d;

    return-object v0
.end method

.method public D()V
    .locals 0

    .prologue
    .line 991
    return-void
.end method

.method public final D_()V
    .locals 0

    .prologue
    .line 1156
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/q;->V()V

    .line 1157
    return-void
.end method

.method public E()V
    .locals 0

    .prologue
    .line 1000
    return-void
.end method

.method public final E_()V
    .locals 0

    .prologue
    .line 1170
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/q;->V()V

    .line 1171
    return-void
.end method

.method public final F()V
    .locals 1

    .prologue
    .line 1013
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->v:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/d/n;

    if-eqz v0, :cond_1

    .line 1014
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->v:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/d/n;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/d/n;->O()V

    .line 1019
    :cond_0
    :goto_0
    return-void

    .line 1015
    :cond_1
    instance-of v0, p0, Lcom/google/android/gms/games/ui/d/n;

    if-eqz v0, :cond_0

    .line 1017
    check-cast p0, Lcom/google/android/gms/games/ui/d/n;

    invoke-interface {p0}, Lcom/google/android/gms/games/ui/d/n;->O()V

    goto :goto_0
.end method

.method public final F_()V
    .locals 0

    .prologue
    .line 1190
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/q;->W()V

    .line 1191
    return-void
.end method

.method public final G()V
    .locals 3

    .prologue
    .line 1028
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 1029
    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1030
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->r:Z

    .line 1031
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1032
    sget-object v0, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {v0, v1, p0}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/multiplayer/turnbased/b;)V

    .line 1040
    :cond_0
    :goto_0
    return-void

    .line 1034
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 1036
    :goto_1
    sget-object v2, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {v2, v1, p0, v0}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/multiplayer/turnbased/b;Ljava/lang/String;)V

    goto :goto_0

    .line 1034
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final H()V
    .locals 3

    .prologue
    .line 1049
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 1050
    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1051
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->s:Z

    .line 1052
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1053
    sget-object v0, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    invoke-interface {v0, v1, p0}, Lcom/google/android/gms/games/multiplayer/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/multiplayer/g;)V

    .line 1061
    :cond_0
    :goto_0
    return-void

    .line 1055
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 1057
    :goto_1
    sget-object v2, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    invoke-interface {v2, v1, p0, v0}, Lcom/google/android/gms/games/multiplayer/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/multiplayer/g;Ljava/lang/String;)V

    goto :goto_0

    .line 1055
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final I()V
    .locals 3

    .prologue
    .line 1070
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 1071
    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1072
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->t:Z

    .line 1073
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1074
    sget-object v0, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    invoke-interface {v0, v1, p0}, Lcom/google/android/gms/games/request/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/request/c;)V

    .line 1081
    :cond_0
    :goto_0
    return-void

    .line 1076
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 1078
    :goto_1
    sget-object v2, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    invoke-interface {v2, v1, p0, v0}, Lcom/google/android/gms/games/request/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/request/c;Ljava/lang/String;)V

    goto :goto_0

    .line 1076
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final J()V
    .locals 3

    .prologue
    .line 1090
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 1091
    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1092
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->u:Z

    .line 1093
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1094
    sget-object v0, Lcom/google/android/gms/games/d;->q:Lcom/google/android/gms/games/quest/e;

    invoke-interface {v0, v1, p0}, Lcom/google/android/gms/games/quest/e;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/quest/d;)V

    .line 1101
    :cond_0
    :goto_0
    return-void

    .line 1096
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 1098
    :goto_1
    sget-object v2, Lcom/google/android/gms/games/d;->q:Lcom/google/android/gms/games/quest/e;

    invoke-interface {v2, v1, p0, v0}, Lcom/google/android/gms/games/quest/e;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/quest/d;Ljava/lang/String;)V

    goto :goto_0

    .line 1096
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public K()V
    .locals 4

    .prologue
    const/16 v3, 0xff

    .line 1215
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->e:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v0, :cond_0

    .line 1216
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->e:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e()V

    .line 1220
    :cond_0
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    .line 1221
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    .line 1222
    iget-object v1, p0, Lcom/google/android/gms/games/ui/q;->z:Ljava/lang/CharSequence;

    if-eqz v1, :cond_1

    .line 1223
    iget-object v1, p0, Lcom/google/android/gms/games/ui/q;->z:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    .line 1225
    :cond_1
    iput v3, p0, Lcom/google/android/gms/games/ui/q;->g:I

    .line 1226
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->X:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->getWindow()Landroid/view/Window;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/games/ui/q;->g:I

    if-eq v2, v3, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/f;->U:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 1227
    :cond_2
    :goto_0
    return-void

    .line 1226
    :cond_3
    invoke-virtual {v1, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    goto :goto_0
.end method

.method public final L()Lcom/google/android/gms/games/ui/common/players/h;
    .locals 1

    .prologue
    .line 1454
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->y:Lcom/google/android/gms/games/ui/common/players/h;

    return-object v0
.end method

.method public final M()Z
    .locals 1

    .prologue
    .line 1459
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->v:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/d/k;

    if-eqz v0, :cond_0

    .line 1460
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->v:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/d/k;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/d/k;->M()Z

    move-result v0

    .line 1462
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final R_()V
    .locals 2

    .prologue
    .line 1161
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1162
    sget v0, Lcom/google/android/gms/p;->iI:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1165
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/q;->V()V

    .line 1166
    return-void
.end method

.method public final S_()V
    .locals 1

    .prologue
    .line 1195
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->v:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/ci;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->v:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/ci;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ci;->L_()V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->F()V

    .line 1196
    return-void

    .line 1195
    :cond_1
    instance-of v0, p0, Lcom/google/android/gms/games/ui/ci;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/google/android/gms/games/ui/ci;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ci;->L_()V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 1378
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->k()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/content/Context;II)I

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1294
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->u()I

    move-result v0

    .line 1295
    if-eqz v0, :cond_0

    .line 1296
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->u()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 1298
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getWrappableContentResId() can\'t return 0 when hasPlayHeader() returns true."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 1311
    return-void
.end method

.method public a(Lcom/google/android/gms/common/api/w;)V
    .locals 0

    .prologue
    .line 271
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/x;)V
    .locals 2

    .prologue
    .line 657
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->l:Lcom/google/android/gms/common/api/v;

    if-nez v0, :cond_0

    .line 659
    const-string v0, "GamesFragmentActivity"

    const-string v1, "Attempting to register a listener without a GoogleApiClient"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    :goto_0
    return-void

    .line 662
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->l:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/x;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/y;)V
    .locals 2

    .prologue
    .line 691
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->l:Lcom/google/android/gms/common/api/v;

    if-nez v0, :cond_0

    .line 693
    const-string v0, "GamesFragmentActivity"

    const-string v1, "Attempting to register a listener without a GoogleApiClient"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    :goto_0
    return-void

    .line 696
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->l:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/y;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/common/c;)V
    .locals 5

    .prologue
    const/16 v4, 0x385

    .line 524
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v0

    .line 525
    const-string v1, "GamesFragmentActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Connection to service apk failed with error "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 529
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->a:Z

    .line 530
    const/16 v0, 0x385

    invoke-virtual {p1, p0, v0}, Lcom/google/android/gms/common/c;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 548
    :cond_0
    :goto_0
    return-void

    .line 532
    :catch_0
    move-exception v0

    const-string v0, "GamesFragmentActivity"

    const-string v1, "Unable to recover from a connection failure."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->finish()V

    goto :goto_0

    .line 537
    :cond_1
    const/4 v1, 0x0

    invoke-static {v0, p0, v4, v1}, Lcom/google/android/gms/common/ew;->a(ILandroid/app/Activity;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/q;->p:Landroid/app/Dialog;

    .line 539
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->p:Landroid/app/Dialog;

    if-nez v0, :cond_2

    .line 540
    const-string v0, "GamesFragmentActivity"

    const-string v1, "Unable to recover from a connection failure."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->finish()V

    goto :goto_0

    .line 544
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 545
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->p:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/games/i;)V
    .locals 0

    .prologue
    .line 281
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1175
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1176
    invoke-interface {p1}, Lcom/google/android/gms/games/request/GameRequest;->i()I

    move-result v0

    .line 1177
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1178
    sget v0, Lcom/google/android/gms/p;->iH:I

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1185
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/q;->W()V

    .line 1186
    return-void

    .line 1180
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1181
    sget v0, Lcom/google/android/gms/p;->iK:I

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 0

    .prologue
    .line 1374
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 807
    iput-object p1, p0, Lcom/google/android/gms/games/ui/q;->z:Ljava/lang/CharSequence;

    .line 808
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    .line 809
    if-eqz v0, :cond_0

    .line 810
    invoke-virtual {v0, p1}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    .line 812
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 300
    const-string v0, "GPG_shareGame"

    if-nez p1, :cond_0

    const-string v0, "UiUtils"

    const-string v1, "shareGame: null game name"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :goto_0
    return-void

    .line 300
    :cond_0
    if-nez p2, :cond_1

    const-string v0, "UiUtils"

    const-string v1, "shareGame: null game package name"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {p2}, Lcom/google/android/gms/common/internal/ay;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "pcampaignid"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "UiUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "shareGame: couldn\'t get shareGame for game: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget v1, Lcom/google/android/gms/p;->lC:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "android.intent.extra.SUBJECT"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "text/plain"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v0, 0x80000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    sget v0, Lcom/google/android/gms/p;->lD:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1468
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->v:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/d/k;

    if-eqz v0, :cond_0

    .line 1469
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->v:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/d/k;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/d/k;->a(Z)V

    .line 1472
    :cond_0
    return-void
.end method

.method public b(Landroid/content/Context;)Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
    .locals 1

    .prologue
    .line 1384
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/x;)V
    .locals 2

    .prologue
    .line 674
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->l:Lcom/google/android/gms/common/api/v;

    if-nez v0, :cond_0

    .line 676
    const-string v0, "GamesFragmentActivity"

    const-string v1, "Attempting to unregister a listener without a GoogleApiClient"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    :goto_0
    return-void

    .line 679
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->l:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/x;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/api/y;)V
    .locals 2

    .prologue
    .line 730
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->l:Lcom/google/android/gms/common/api/v;

    if-nez v0, :cond_0

    .line 732
    const-string v0, "GamesFragmentActivity"

    const-string v1, "Attempting to unregister a listener without a GoogleApiClient"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    :goto_0
    return-void

    .line 735
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->l:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/y;)V

    goto :goto_0
.end method

.method public b(I)Z
    .locals 1

    .prologue
    .line 713
    const/4 v0, 0x1

    return v0
.end method

.method public b_(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 511
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->q:Z

    if-eqz v0, :cond_0

    .line 512
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->G()V

    .line 513
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->H()V

    .line 514
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->I()V

    .line 515
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->J()V

    .line 517
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1147
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1148
    sget v0, Lcom/google/android/gms/p;->iJ:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1151
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/q;->V()V

    .line 1152
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 1284
    const/4 v0, 0x0

    return v0
.end method

.method public final f_(I)V
    .locals 0

    .prologue
    .line 520
    return-void
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 1289
    const/4 v0, 0x0

    return v0
.end method

.method public final h()F
    .locals 1

    .prologue
    .line 1316
    const v0, 0x3f333333    # 0.7f

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1321
    const/4 v0, 0x1

    return v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 1331
    const/4 v0, 0x1

    return v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 1341
    const/4 v0, 0x2

    return v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 1347
    const/4 v0, 0x1

    return v0
.end method

.method public final m()Landroid/app/Activity;
    .locals 0

    .prologue
    .line 1358
    return-object p0
.end method

.method public final n()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1389
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-static {p0}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/games/ui/cn;)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    return-object v0
.end method

.method public final o()V
    .locals 0

    .prologue
    .line 1404
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 564
    sparse-switch p1, :sswitch_data_0

    .line 603
    :cond_0
    :goto_0
    const-string v0, "GamesFragmentActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onActivityResult: unhandled request code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/d;->onActivityResult(IILandroid/content/Intent;)V

    .line 605
    :goto_1
    :sswitch_0
    return-void

    .line 566
    :sswitch_1
    if-ne p2, v0, :cond_1

    .line 567
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/q;->a:Z

    .line 568
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->l:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    goto :goto_1

    .line 569
    :cond_1
    const/16 v0, 0x2712

    if-ne p2, v0, :cond_2

    .line 572
    const-string v0, "GamesFragmentActivity"

    const-string v1, "REQUEST_RESOLVE_FAILURE resulted in SIGN_IN_FAILED"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/q;->a:Z

    .line 574
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->finish()V

    goto :goto_1

    .line 579
    :cond_2
    const-string v0, "GamesFragmentActivity"

    const-string v1, "REQUEST_RESOLVE_FAILURE failed, bailing out..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->finish()V

    goto :goto_1

    .line 589
    :sswitch_2
    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 590
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->d:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/d/aa;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 591
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->d:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->E()V

    .line 592
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/q;->d:Ljava/lang/String;

    goto :goto_0

    .line 594
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->c:Z

    .line 595
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->d:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->D()V

    goto :goto_0

    .line 564
    nop

    :sswitch_data_0
    .sparse-switch
        0x384 -> :sswitch_0
        0x385 -> :sswitch_1
        0x7d0 -> :sswitch_2
    .end sparse-switch
.end method

.method public onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 488
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 489
    instance-of v0, p1, Landroid/support/v4/app/m;

    if-nez v0, :cond_0

    .line 493
    iput-object p1, p0, Lcom/google/android/gms/games/ui/q;->v:Landroid/support/v4/app/Fragment;

    .line 495
    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/games/ui/b/b;

    if-eqz v0, :cond_1

    .line 497
    check-cast p1, Lcom/google/android/gms/games/ui/b/b;

    invoke-virtual {p1, p0}, Lcom/google/android/gms/games/ui/b/b;->a(Landroid/content/Context;)V

    .line 499
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 316
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 318
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 321
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    new-instance v0, Lcom/google/android/gms/games/ui/ck;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/ck;-><init>(Lcom/google/android/gms/games/ui/cn;)V

    .line 323
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ck;->a(Landroid/view/LayoutInflater;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/games/ui/q;->e:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 324
    iget-object v2, p0, Lcom/google/android/gms/games/ui/q;->e:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/ck;->a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    .line 325
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->K()V

    .line 329
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->u()I

    move-result v2

    .line 330
    iget v0, p0, Lcom/google/android/gms/games/ui/q;->j:I

    if-eqz v0, :cond_4

    .line 332
    iget v0, p0, Lcom/google/android/gms/games/ui/q;->j:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/q;->setContentView(I)V

    .line 333
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 338
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 341
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->f()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 342
    iget-object v1, p0, Lcom/google/android/gms/games/ui/q;->e:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0, v1, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 355
    :cond_1
    :goto_0
    new-instance v0, Lcom/google/android/gms/games/ui/a/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/a/d;-><init>(Lcom/google/android/gms/games/ui/q;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/q;->w:Lcom/google/android/gms/games/ui/a/d;

    .line 356
    new-instance v0, Lcom/google/android/gms/games/ui/d/j;

    invoke-direct {v0, p0, p0}, Lcom/google/android/gms/games/ui/d/j;-><init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/d/k;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/q;->y:Lcom/google/android/gms/games/ui/common/players/h;

    .line 358
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->v()V

    .line 360
    if-eqz p1, :cond_2

    .line 361
    const-string v0, "savedStateResolutionInProgress"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->a:Z

    .line 363
    const-string v0, "savedStateWaitingForInstall"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->c:Z

    .line 366
    :cond_2
    return-void

    .line 343
    :cond_3
    if-eqz v2, :cond_1

    .line 344
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    goto :goto_0

    .line 346
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 347
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->e:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/q;->setContentView(Landroid/view/View;)V

    goto :goto_0

    .line 348
    :cond_5
    if-eqz v2, :cond_6

    .line 349
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/q;->setContentView(I)V

    goto :goto_0

    .line 351
    :cond_6
    const-string v0, "We need to either have a layout res id, play header, or a wrappable content res id to ensure we have a content view."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 851
    iget v0, p0, Lcom/google/android/gms/games/ui/q;->k:I

    if-nez v0, :cond_0

    .line 852
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 900
    :goto_0
    return v0

    .line 861
    :cond_0
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 863
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 864
    iget v2, p0, Lcom/google/android/gms/games/ui/q;->k:I

    invoke-virtual {v0, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 874
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 876
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->m:Z

    if-eqz v0, :cond_1

    .line 879
    sget v0, Lcom/google/android/gms/j;->lU:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 880
    const-string v0, "You need an item menu_progress_bar in your menu if you are enabling the ProgressBar in the ActionBar"

    invoke-static {v2, v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 889
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v3, Lcom/google/android/gms/l;->c:I

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 891
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x1

    invoke-direct {v0, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 894
    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 895
    sget v0, Lcom/google/android/gms/j;->pR:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/q;->n:Landroid/widget/ProgressBar;

    .line 896
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->o:Z

    const/4 v4, 0x4

    new-array v5, v1, [Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/gms/games/ui/q;->n:Landroid/widget/ProgressBar;

    aput-object v6, v5, v7

    invoke-static {v0, v4, v5}, Lcom/google/android/gms/games/ui/d/al;->a(ZI[Landroid/view/View;)V

    .line 897
    invoke-static {v2, v3}, Landroid/support/v4/view/ai;->a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    :cond_1
    move v0, v1

    .line 900
    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 425
    invoke-super {p0}, Landroid/support/v7/app/d;->onPause()V

    .line 427
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 392
    invoke-super {p0}, Landroid/support/v7/app/d;->onResume()V

    .line 394
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->a:Z

    if-eqz v0, :cond_0

    .line 397
    const-string v0, "GamesFragmentActivity"

    const-string v1, "onResume with a resolutionIntentInProgress. This should never have happened ..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/q;->a:Z

    .line 400
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->l:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 405
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->d:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/d/aa;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 406
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/q;->c:Z

    if-eqz v1, :cond_1

    .line 407
    if-eqz v0, :cond_3

    .line 408
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/q;->c:Z

    .line 409
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->d:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->E()V

    .line 410
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/q;->d:Ljava/lang/String;

    .line 416
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->x:Lcom/google/android/gms/games/ui/r;

    if-nez v0, :cond_2

    .line 417
    new-instance v0, Lcom/google/android/gms/games/ui/r;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/games/ui/r;-><init>(Lcom/google/android/gms/games/ui/q;B)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/q;->x:Lcom/google/android/gms/games/ui/r;

    .line 418
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->x:Lcom/google/android/gms/games/ui/r;

    sget-object v1, Lcom/google/android/gms/games/ui/q;->i:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/q;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 421
    :cond_2
    return-void

    .line 412
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->d:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->D()V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 503
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 505
    const-string v0, "savedStateResolutionInProgress"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/q;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 506
    const-string v0, "savedStateWaitingForInstall"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/q;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 507
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 384
    invoke-super {p0}, Landroid/support/v7/app/d;->onStart()V

    .line 385
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->a:Z

    if-nez v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->l:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 388
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 431
    invoke-super {p0}, Landroid/support/v7/app/d;->onStop()V

    .line 433
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v2

    .line 434
    invoke-interface {v2}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 435
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->r:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->q:Z

    if-eqz v0, :cond_1

    .line 436
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->d()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 437
    sget-object v0, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {v0, v2}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->a(Lcom/google/android/gms/common/api/v;)V

    .line 445
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->s:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->q:Z

    if-eqz v0, :cond_3

    .line 446
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->d()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 447
    sget-object v0, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    invoke-interface {v0, v2}, Lcom/google/android/gms/games/multiplayer/d;->a(Lcom/google/android/gms/common/api/v;)V

    .line 455
    :cond_3
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->t:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->q:Z

    if-eqz v0, :cond_5

    .line 456
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->d()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 457
    sget-object v0, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    invoke-interface {v0, v2}, Lcom/google/android/gms/games/request/d;->a(Lcom/google/android/gms/common/api/v;)V

    .line 464
    :cond_5
    :goto_2
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->u:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->q:Z

    if-eqz v0, :cond_7

    .line 465
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->d()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 466
    sget-object v0, Lcom/google/android/gms/games/d;->q:Lcom/google/android/gms/games/quest/e;

    invoke-interface {v0, v2}, Lcom/google/android/gms/games/quest/e;->a(Lcom/google/android/gms/common/api/v;)V

    .line 474
    :cond_7
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->l:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 476
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->p:Landroid/app/Dialog;

    if-eqz v0, :cond_8

    .line 477
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->p:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 480
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->x:Lcom/google/android/gms/games/ui/r;

    if-eqz v0, :cond_9

    .line 481
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->x:Lcom/google/android/gms/games/ui/r;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/q;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 482
    iput-object v1, p0, Lcom/google/android/gms/games/ui/q;->x:Lcom/google/android/gms/games/ui/r;

    .line 484
    :cond_9
    return-void

    .line 439
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_b

    move-object v0, v1

    .line 441
    :goto_4
    sget-object v3, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {v3, v2, v0}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->f(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V

    goto :goto_0

    .line 439
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 449
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_d

    move-object v0, v1

    .line 451
    :goto_5
    sget-object v3, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    invoke-interface {v3, v2, v0}, Lcom/google/android/gms/games/multiplayer/d;->b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V

    goto :goto_1

    .line 449
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 459
    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_f

    move-object v0, v1

    .line 461
    :goto_6
    sget-object v3, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    invoke-interface {v3, v2, v0}, Lcom/google/android/gms/games/request/d;->b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V

    goto :goto_2

    .line 459
    :cond_f
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 468
    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_11

    move-object v0, v1

    .line 470
    :goto_7
    sget-object v3, Lcom/google/android/gms/games/d;->q:Lcom/google/android/gms/games/quest/e;

    invoke-interface {v3, v2, v0}, Lcom/google/android/gms/games/quest/e;->b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V

    goto :goto_3

    .line 468
    :cond_11
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_7
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 1408
    const/4 v0, 0x0

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 1413
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->h:Z

    return v0
.end method

.method protected abstract r()Lcom/google/android/gms/common/api/v;
.end method

.method public final s()F
    .locals 1

    .prologue
    .line 1441
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->e:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v0, :cond_0

    .line 1442
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->e:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d()F

    move-result v0

    .line 1444
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTitle(I)V
    .locals 1

    .prologue
    .line 756
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/q;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/q;->setTitle(Ljava/lang/CharSequence;)V

    .line 757
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 748
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->setTitle(Ljava/lang/CharSequence;)V

    .line 749
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    .line 750
    if-eqz v0, :cond_0

    .line 751
    invoke-virtual {v0, p1}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    .line 753
    :cond_0
    return-void
.end method

.method public supportInvalidateOptionsMenu()V
    .locals 1

    .prologue
    .line 841
    invoke-super {p0}, Landroid/support/v7/app/d;->supportInvalidateOptionsMenu()V

    .line 842
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/q;->n:Landroid/widget/ProgressBar;

    .line 843
    return-void
.end method

.method public t()V
    .locals 0

    .prologue
    .line 292
    return-void
.end method

.method protected u()I
    .locals 1

    .prologue
    .line 379
    const/4 v0, 0x0

    return v0
.end method

.method protected final v()V
    .locals 2

    .prologue
    .line 612
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->r()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/q;->l:Lcom/google/android/gms/common/api/v;

    .line 613
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->l:Lcom/google/android/gms/common/api/v;

    if-nez v0, :cond_0

    .line 614
    const-string v0, "GamesFragmentActivity"

    const-string v1, "Unable to instantiate GoogleApiClient; bailing out..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->finish()V

    .line 619
    :cond_0
    return-void
.end method

.method public final w()Lcom/google/android/gms/common/api/v;
    .locals 2

    .prologue
    .line 638
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->l:Lcom/google/android/gms/common/api/v;

    if-nez v0, :cond_0

    .line 639
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GoogleApiClient instance not created yet"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 641
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->l:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method public final x()Z
    .locals 1

    .prologue
    .line 645
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->l:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final y()Lcom/google/android/gms/games/ui/bh;
    .locals 1

    .prologue
    .line 718
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    return-object v0
.end method

.method public final z()V
    .locals 2

    .prologue
    .line 905
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->m:Z

    const-string v1, "This method can only be called if we have a progressbar in the actionbar"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 907
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/q;->o:Z

    .line 908
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->n:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 909
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->n:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 911
    :cond_0
    return-void
.end method
