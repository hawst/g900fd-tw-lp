.class public final Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/ac;

.field private static final k:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$And;

.field d:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Collection;

.field e:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Email;

.field f:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Location;

.field g:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Name;

.field h:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Not;

.field i:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Or;

.field j:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Organization;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/ac;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/ac;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/ac;

    .line 75
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 78
    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->k:Ljava/util/HashMap;

    const-string v1, "and"

    const-string v2, "and"

    const/4 v3, 0x2

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$And;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->k:Ljava/util/HashMap;

    const-string v1, "collection"

    const-string v2, "collection"

    const/4 v3, 0x3

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Collection;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->k:Ljava/util/HashMap;

    const-string v1, "email"

    const-string v2, "email"

    const/4 v3, 0x4

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Email;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->k:Ljava/util/HashMap;

    const-string v1, "location"

    const-string v2, "location"

    const/4 v3, 0x6

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Location;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->k:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    const/4 v3, 0x7

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Name;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->k:Ljava/util/HashMap;

    const-string v1, "not"

    const-string v2, "not"

    const/16 v3, 0x8

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Not;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->k:Ljava/util/HashMap;

    const-string v1, "or"

    const-string v2, "or"

    const/16 v3, 0x9

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Or;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->k:Ljava/util/HashMap;

    const-string v1, "organization"

    const-string v2, "organization"

    const/16 v3, 0xa

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Organization;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 172
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->b:I

    .line 173
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->a:Ljava/util/Set;

    .line 174
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$And;Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Collection;Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Email;Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Location;Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Name;Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Not;Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Or;Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Organization;)V
    .locals 0

    .prologue
    .line 188
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 189
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->a:Ljava/util/Set;

    .line 190
    iput p2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->b:I

    .line 191
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->c:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$And;

    .line 192
    iput-object p4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Collection;

    .line 193
    iput-object p5, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->e:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Email;

    .line 194
    iput-object p6, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->f:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Location;

    .line 195
    iput-object p7, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->g:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Name;

    .line 196
    iput-object p8, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->h:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Not;

    .line 197
    iput-object p9, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->i:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Or;

    .line 198
    iput-object p10, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->j:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Organization;

    .line 199
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->k:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 4

    .prologue
    .line 2524
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 2525
    packed-switch v0, :pswitch_data_0

    .line 2551
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2527
    :pswitch_1
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$And;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->c:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$And;

    .line 2555
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2556
    return-void

    .line 2530
    :pswitch_2
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Collection;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Collection;

    goto :goto_0

    .line 2533
    :pswitch_3
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Email;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->e:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Email;

    goto :goto_0

    .line 2536
    :pswitch_4
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Location;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->f:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Location;

    goto :goto_0

    .line 2539
    :pswitch_5
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Name;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->g:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Name;

    goto :goto_0

    .line 2542
    :pswitch_6
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Not;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->h:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Not;

    goto :goto_0

    .line 2545
    :pswitch_7
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Or;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->i:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Or;

    goto :goto_0

    .line 2548
    :pswitch_8
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Organization;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->j:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Organization;

    goto :goto_0

    .line 2525
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 2481
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2486
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2504
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2488
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->c:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$And;

    .line 2502
    :goto_0
    return-object v0

    .line 2490
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Collection;

    goto :goto_0

    .line 2492
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->e:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Email;

    goto :goto_0

    .line 2494
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->f:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Location;

    goto :goto_0

    .line 2496
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->g:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Name;

    goto :goto_0

    .line 2498
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->h:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Not;

    goto :goto_0

    .line 2500
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->i:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Or;

    goto :goto_0

    .line 2502
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->j:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate$Organization;

    goto :goto_0

    .line 2486
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2471
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/ac;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2573
    instance-of v0, p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;

    if-nez v0, :cond_0

    move v0, v1

    .line 2604
    :goto_0
    return v0

    .line 2578
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 2579
    goto :goto_0

    .line 2582
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;

    .line 2583
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->k:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 2584
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2585
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2587
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2589
    goto :goto_0

    :cond_3
    move v0, v1

    .line 2594
    goto :goto_0

    .line 2597
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 2599
    goto :goto_0

    :cond_5
    move v0, v2

    .line 2604
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 2560
    const/4 v0, 0x0

    .line 2561
    sget-object v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->k:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 2562
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2563
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 2564
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 2566
    goto :goto_0

    .line 2567
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2476
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/ac;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/ac;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleSearchPredicate;Landroid/os/Parcel;I)V

    .line 2477
    return-void
.end method
