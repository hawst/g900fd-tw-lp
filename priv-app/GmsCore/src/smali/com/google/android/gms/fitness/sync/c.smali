.class public final Lcom/google/android/gms/fitness/sync/c;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 49
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "fitness_sync_account_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/accounts/Account;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 145
    sget-object v0, Lcom/google/android/gms/fitness/g/c;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    .line 146
    const-string v0, "com.google.android.gms.fitness"

    invoke-static {p0, v0}, Landroid/content/ContentResolver;->getPeriodicSyncs(Landroid/accounts/Account;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 149
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/PeriodicSync;

    .line 150
    iget-wide v8, v0, Landroid/content/PeriodicSync;->period:J

    cmp-long v7, v8, v4

    if-nez v7, :cond_0

    move v1, v3

    .line 151
    goto :goto_0

    .line 153
    :cond_0
    const-string v7, "com.google.android.gms.fitness"

    iget-object v0, v0, Landroid/content/PeriodicSync;->extras:Landroid/os/Bundle;

    invoke-static {p0, v7, v0}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 158
    :cond_1
    if-nez v1, :cond_2

    .line 159
    const-string v0, "set periodic sync to: %s"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 160
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 161
    const-string v1, "sync_periodic"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 162
    const-string v1, "com.google.android.gms.fitness"

    invoke-static {p0, v1, v0, v4, v5}, Landroid/content/ContentResolver;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    .line 165
    :cond_2
    return-void
.end method

.method private b(Landroid/accounts/Account;)Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/sync/c;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/sync/c;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 10

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/sync/c;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/g;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    :goto_0
    return-void

    .line 62
    :cond_0
    const-string v0, "initialize"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    const-string v0, "Initializing account \'%s\' so it\'s ready to sync settings."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 65
    const-string v0, "com.google.android.gms.fitness"

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 69
    const-string v0, "com.google.android.gms.fitness"

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 71
    invoke-static {p1}, Lcom/google/android/gms/fitness/sync/c;->a(Landroid/accounts/Account;)V

    goto :goto_0

    .line 76
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 77
    const-string v0, "force"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/sync/c;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_3

    .line 78
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/sync/c;->b(Landroid/accounts/Account;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "sync_time"

    const-wide/16 v4, 0x0

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 79
    sub-long v0, v2, v0

    .line 81
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v0, v1}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->l:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    cmp-long v0, v4, v0

    if-gez v0, :cond_3

    .line 82
    const-string v0, "Skipping Fitness sync under bad network conditions"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_0

    .line 77
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 88
    :cond_3
    const-string v0, "sync: %s, extras: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v1, v4

    const/4 v4, 0x1

    aput-object p2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 91
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/sync/c;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/h/d;->a(Landroid/content/Context;)Lcom/google/android/gms/fitness/h/f;

    move-result-object v0

    .line 93
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/sync/c;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, v4}, Lcom/google/android/gms/fitness/h/c;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/fitness/h/a;

    move-result-object v1

    .line 95
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/h/f;->a()Ljava/util/List;

    move-result-object v4

    .line 97
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/h/f;->b()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/fitness/h/a;->a(Landroid/os/Handler;)Lcom/google/android/gms/fitness/sensors/a/a;

    move-result-object v0

    .line 100
    invoke-virtual {v1, v4, v0}, Lcom/google/android/gms/fitness/h/a;->a(Ljava/util/List;Lcom/google/android/gms/fitness/sensors/a/a;)Lcom/google/android/gms/fitness/sensors/a;

    move-result-object v0

    .line 104
    invoke-virtual {v1, v0}, Lcom/google/android/gms/fitness/h/a;->a(Lcom/google/android/gms/fitness/sensors/a;)Lcom/google/android/gms/fitness/k/a;

    move-result-object v0

    .line 106
    invoke-virtual {v1}, Lcom/google/android/gms/fitness/h/a;->a()Lcom/google/android/gms/fitness/l/z;

    move-result-object v1

    .line 108
    new-instance v4, Lcom/google/android/gms/fitness/apiary/m;

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/sync/c;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v4, v1, v5, v6}, Lcom/google/android/gms/fitness/apiary/m;-><init>(Lcom/google/android/gms/fitness/l/z;Landroid/content/Context;Ljava/lang/String;)V

    .line 110
    new-instance v5, Lcom/google/android/gms/fitness/sync/b;

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/sync/c;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/gms/fitness/sync/b;-><init>(Landroid/content/Context;)V

    .line 111
    new-instance v6, Lcom/google/android/gms/fitness/sync/f;

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/sync/c;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/google/android/gms/fitness/sync/f;-><init>(Landroid/content/Context;)V

    .line 113
    :try_start_0
    invoke-virtual {v5, v4, v1, v0}, Lcom/google/android/gms/fitness/sync/b;->a(Lcom/google/android/gms/fitness/sync/d;Lcom/google/android/gms/fitness/l/z;Lcom/google/android/gms/fitness/k/a;)V

    .line 114
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_0
    .catch Lcom/google/android/gms/fitness/sync/g; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/fitness/sync/h; {:try_start_0 .. :try_end_0} :catch_2

    move-result-wide v8

    :try_start_1
    invoke-interface {v1, v8, v9}, Lcom/google/android/gms/fitness/l/z;->a(J)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    iput v5, v6, Lcom/google/android/gms/fitness/sync/f;->a:I

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_4
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/l/c;

    invoke-static {v4, v0}, Lcom/google/android/gms/fitness/sync/f;->a(Lcom/google/android/gms/fitness/sync/d;Lcom/google/android/gms/fitness/l/c;)Z

    move-result v8

    if-eqz v8, :cond_4

    iget-wide v8, v0, Lcom/google/android/gms/fitness/l/c;->a:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/fitness/sync/g; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/gms/fitness/sync/h; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v4, Lcom/google/android/gms/fitness/sync/g;

    const/4 v5, 0x0

    invoke-direct {v4, v5, v0}, Lcom/google/android/gms/fitness/sync/g;-><init>(ZLjava/lang/Throwable;)V

    throw v4
    :try_end_2
    .catch Lcom/google/android/gms/fitness/sync/g; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/gms/fitness/sync/h; {:try_start_2 .. :try_end_2} :catch_2

    .line 115
    :catch_1
    move-exception v0

    .line 116
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/sync/g;->b()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 117
    const-string v0, "Sync thread interrupted."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 119
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto/16 :goto_0

    .line 114
    :cond_5
    :try_start_3
    invoke-interface {v1, v5}, Lcom/google/android/gms/fitness/l/z;->a(Ljava/util/List;)I
    :try_end_3
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/google/android/gms/fitness/sync/g; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/android/gms/fitness/sync/h; {:try_start_3 .. :try_end_3} :catch_2

    :try_start_4
    invoke-virtual {v6, v4, v1}, Lcom/google/android/gms/fitness/sync/f;->a(Lcom/google/android/gms/fitness/sync/d;Lcom/google/android/gms/fitness/l/z;)V
    :try_end_4
    .catch Lcom/google/android/gms/fitness/sync/g; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/google/android/gms/fitness/sync/h; {:try_start_4 .. :try_end_4} :catch_2

    .line 128
    :goto_3
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/sync/c;->b(Landroid/accounts/Account;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "sync_time"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 129
    invoke-static {p1}, Lcom/google/android/gms/fitness/sync/c;->a(Landroid/accounts/Account;)V

    .line 135
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 136
    const-string v1, "com.google.android.gms.fitness.syncadapter.SYNC_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 137
    const-string v1, "TimestampMillis"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 138
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/sync/c;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 122
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "sync error: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v4}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_3

    .line 124
    :catch_2
    move-exception v0

    const-string v0, "user requested delete history"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v4}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 125
    invoke-interface {v1}, Lcom/google/android/gms/fitness/l/z;->e()Z

    goto :goto_3
.end method
