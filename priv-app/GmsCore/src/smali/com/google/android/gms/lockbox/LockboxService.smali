.class public Lcom/google/android/gms/lockbox/LockboxService;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/lockbox/e/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 91
    const-string v0, "LockboxService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method private a(J)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 147
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/lockbox/LockboxService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 148
    invoke-static {p0}, Lcom/google/android/gms/lockbox/LockboxAlarmReceiver;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 149
    const/4 v2, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    add-long/2addr v4, p1

    invoke-static {p0, v3, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v2, v4, v5, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 151
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v12, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 96
    invoke-static {p0}, Lcom/google/android/gms/lockbox/e/a;->a(Landroid/content/Context;)Lcom/google/android/gms/lockbox/e/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/lockbox/LockboxService;->a:Lcom/google/android/gms/lockbox/e/a;

    .line 97
    new-instance v7, Lcom/google/android/gms/lockbox/j;

    invoke-direct {v7, p0}, Lcom/google/android/gms/lockbox/j;-><init>(Lcom/google/android/gms/lockbox/LockboxService;)V

    iget-object v5, v7, Lcom/google/android/gms/lockbox/j;->a:Lcom/google/android/gms/lockbox/LockboxService;

    invoke-virtual {v5}, Lcom/google/android/gms/lockbox/LockboxService;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/google/android/gms/common/util/a;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v8, Lcom/google/android/gms/lockbox/c/o;->b:[I

    array-length v9, v8

    move v4, v2

    :goto_0
    if-ge v4, v9, :cond_0

    aget v10, v8, v4

    iget-object v11, v5, Lcom/google/android/gms/lockbox/LockboxService;->a:Lcom/google/android/gms/lockbox/e/a;

    invoke-virtual {v11, v0, v10}, Lcom/google/android/gms/lockbox/e/a;->a(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_2

    move-object v6, v0

    :goto_1
    if-eqz v6, :cond_7

    iget-object v0, v7, Lcom/google/android/gms/lockbox/j;->a:Lcom/google/android/gms/lockbox/LockboxService;

    sget-object v0, Lcom/google/android/gms/lockbox/b/c;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/lockbox/d/b;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    if-nez v0, :cond_6

    iget-object v4, v7, Lcom/google/android/gms/lockbox/j;->a:Lcom/google/android/gms/lockbox/LockboxService;

    new-array v8, v12, [Lcom/google/android/gms/lockbox/d/c;

    new-instance v5, Lcom/google/android/gms/lockbox/d/d;

    invoke-direct {v5, v4}, Lcom/google/android/gms/lockbox/d/d;-><init>(Landroid/content/Context;)V

    aput-object v5, v8, v2

    new-instance v5, Lcom/google/android/gms/lockbox/d/a;

    invoke-direct {v5, v4}, Lcom/google/android/gms/lockbox/d/a;-><init>(Landroid/content/Context;)V

    aput-object v5, v8, v1

    array-length v9, v8

    move v4, v2

    :goto_3
    if-ge v4, v9, :cond_5

    aget-object v5, v8, v4

    invoke-interface {v5}, Lcom/google/android/gms/lockbox/d/c;->a()Ljava/lang/String;

    move-result-object v5

    if-nez v3, :cond_1

    move-object v3, v5

    :cond_1
    if-nez v3, :cond_5

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    move-object v6, v3

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    if-eqz v3, :cond_6

    iget-object v0, v7, Lcom/google/android/gms/lockbox/j;->a:Lcom/google/android/gms/lockbox/LockboxService;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v12, :cond_9

    sget-object v0, Lcom/google/android/gms/lockbox/b/c;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/lockbox/d/b;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_8

    move v0, v1

    :goto_4
    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    if-eqz v0, :cond_a

    move v0, v1

    :cond_6
    :goto_6
    if-nez v0, :cond_b

    iget-object v0, v7, Lcom/google/android/gms/lockbox/j;->a:Lcom/google/android/gms/lockbox/LockboxService;

    const-wide/32 v2, 0x5265c00

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/lockbox/LockboxService;->a(J)V

    .line 98
    :cond_7
    return-void

    :cond_8
    move v0, v2

    .line 97
    goto :goto_4

    :cond_9
    move v0, v2

    goto :goto_5

    :cond_a
    move v0, v2

    goto :goto_6

    :cond_b
    iget-object v3, v7, Lcom/google/android/gms/lockbox/j;->a:Lcom/google/android/gms/lockbox/LockboxService;

    const-wide/32 v4, 0xea60

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/lockbox/LockboxService;->a(J)V

    if-eqz v0, :cond_7

    iget-object v0, v7, Lcom/google/android/gms/lockbox/j;->a:Lcom/google/android/gms/lockbox/LockboxService;

    iget-object v0, v0, Lcom/google/android/gms/lockbox/LockboxService;->a:Lcom/google/android/gms/lockbox/e/a;

    invoke-virtual {v0, v6, v1}, Lcom/google/android/gms/lockbox/e/a;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, v7, Lcom/google/android/gms/lockbox/j;->a:Lcom/google/android/gms/lockbox/LockboxService;

    iget-object v3, v7, Lcom/google/android/gms/lockbox/j;->a:Lcom/google/android/gms/lockbox/LockboxService;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/4 v3, 0x3

    new-array v3, v3, [Lcom/google/android/gms/lockbox/l;

    new-instance v7, Lcom/google/android/gms/lockbox/a/e;

    invoke-direct {v7, v0}, Lcom/google/android/gms/lockbox/a/e;-><init>(Landroid/content/Context;)V

    new-instance v8, Lcom/google/android/gms/lockbox/k;

    const-string v9, "AppUsageWatcher"

    const/4 v10, 0x6

    invoke-direct {v8, v9, v0, v10}, Lcom/google/android/gms/lockbox/k;-><init>(Ljava/lang/String;Landroid/content/Context;I)V

    new-instance v9, Lcom/google/android/gms/lockbox/l;

    const-string v10, "AppUsageWatcher"

    invoke-direct {v9, v10, v7, v8}, Lcom/google/android/gms/lockbox/l;-><init>(Ljava/lang/String;Lcom/google/android/gms/lockbox/m;Lcom/google/android/gms/lockbox/n;)V

    aput-object v9, v3, v2

    new-instance v7, Lcom/google/android/gms/lockbox/a/g;

    invoke-direct {v7, v0}, Lcom/google/android/gms/lockbox/a/g;-><init>(Landroid/content/Context;)V

    new-instance v8, Lcom/google/android/gms/lockbox/k;

    const-string v9, "TaskInfoWatcher"

    const/16 v10, 0x15

    invoke-direct {v8, v9, v0, v10}, Lcom/google/android/gms/lockbox/k;-><init>(Ljava/lang/String;Landroid/content/Context;I)V

    new-instance v9, Lcom/google/android/gms/lockbox/l;

    const-string v10, "TaskInfoWatcher"

    invoke-direct {v9, v10, v7, v8}, Lcom/google/android/gms/lockbox/l;-><init>(Ljava/lang/String;Lcom/google/android/gms/lockbox/m;Lcom/google/android/gms/lockbox/n;)V

    aput-object v9, v3, v1

    new-instance v1, Lcom/google/android/gms/lockbox/f/c;

    invoke-direct {v1, v0}, Lcom/google/android/gms/lockbox/f/c;-><init>(Landroid/content/Context;)V

    new-instance v7, Lcom/google/android/gms/lockbox/k;

    const-string v8, "SystemStateWatcher"

    const/16 v9, 0x13

    invoke-direct {v7, v8, v0, v9}, Lcom/google/android/gms/lockbox/k;-><init>(Ljava/lang/String;Landroid/content/Context;I)V

    new-instance v0, Lcom/google/android/gms/lockbox/l;

    const-string v8, "SystemStateWatcher"

    invoke-direct {v0, v8, v1, v7}, Lcom/google/android/gms/lockbox/l;-><init>(Ljava/lang/String;Lcom/google/android/gms/lockbox/m;Lcom/google/android/gms/lockbox/n;)V

    aput-object v0, v3, v12

    array-length v7, v3

    move v1, v2

    :goto_7
    if-ge v1, v7, :cond_d

    aget-object v8, v3, v1

    iget-object v0, v8, Lcom/google/android/gms/lockbox/l;->b:Lcom/google/android/gms/lockbox/n;

    invoke-interface {v0, v6}, Lcom/google/android/gms/lockbox/n;->a(Ljava/lang/String;)V

    iget-object v0, v8, Lcom/google/android/gms/lockbox/l;->a:Lcom/google/android/gms/lockbox/m;

    invoke-interface {v0, v4, v5}, Lcom/google/android/gms/lockbox/m;->a(J)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_8
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iget-object v10, v8, Lcom/google/android/gms/lockbox/l;->b:Lcom/google/android/gms/lockbox/n;

    invoke-interface {v10, v0}, Lcom/google/android/gms/lockbox/n;->a([B)V

    goto :goto_8

    :cond_c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    :cond_d
    array-length v0, v3

    :goto_9
    if-ge v2, v0, :cond_7

    aget-object v1, v3, v2

    iget-object v1, v1, Lcom/google/android/gms/lockbox/l;->b:Lcom/google/android/gms/lockbox/n;

    invoke-interface {v1}, Lcom/google/android/gms/lockbox/n;->a()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_9
.end method
