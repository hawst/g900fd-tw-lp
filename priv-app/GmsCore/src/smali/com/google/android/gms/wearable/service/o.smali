.class public final Lcom/google/android/gms/wearable/service/o;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/gms/wearable/service/WearableService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wearable/service/WearableService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 336
    iput-object p1, p0, Lcom/google/android/gms/wearable/service/o;->a:Lcom/google/android/gms/wearable/service/WearableService;

    .line 337
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 338
    return-void
.end method


# virtual methods
.method public final e(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;)V
    .locals 8

    .prologue
    const/16 v3, 0x10

    const/16 v2, 0x8

    const/4 v7, 0x0

    .line 343
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_0

    .line 344
    invoke-interface {p1, v3, v7, v7}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    .line 396
    :goto_0
    return-void

    .line 349
    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 350
    invoke-interface {p1, v2, v7, v7}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    goto :goto_0

    .line 353
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/o;->a:Lcom/google/android/gms/wearable/service/WearableService;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 359
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/o;->a:Lcom/google/android/gms/wearable/service/WearableService;

    invoke-static {v0}, Lcom/google/android/gms/wearable/service/i;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 360
    invoke-interface {p1, v3, v7, v7}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    goto :goto_0

    .line 366
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/o;->a:Lcom/google/android/gms/wearable/service/WearableService;

    invoke-static {v0, p3}, Lcom/google/android/gms/wearable/node/b;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/wearable/node/a;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 373
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/o;->a:Lcom/google/android/gms/wearable/service/WearableService;

    invoke-static {v0}, Lcom/google/android/gms/wearable/service/WearableService;->b(Lcom/google/android/gms/wearable/service/WearableService;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 374
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/service/z;

    .line 375
    :goto_1
    if-nez v0, :cond_4

    .line 377
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    const/16 v1, 0x15

    if-lt v0, v1, :cond_3

    .line 379
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/o;->a:Lcom/google/android/gms/wearable/service/WearableService;

    const-string v1, "telecom"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/service/WearableService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;
    :try_end_2
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v7, v0

    .line 386
    :cond_3
    :goto_2
    :try_start_3
    new-instance v0, Lcom/google/android/gms/wearable/service/z;

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/o;->a:Lcom/google/android/gms/wearable/service/WearableService;

    invoke-virtual {v1}, Lcom/google/android/gms/wearable/service/WearableService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/wearable/node/o;->a:Lcom/google/android/gms/wearable/node/o;

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/o;->a:Lcom/google/android/gms/wearable/service/WearableService;

    invoke-static {v4}, Lcom/google/android/gms/wearable/service/WearableService;->c(Lcom/google/android/gms/wearable/service/WearableService;)Lcom/google/android/gms/wearable/service/e;

    move-result-object v4

    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->c()Lcom/google/android/gms/wearable/node/a/a;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/wearable/node/au;->a:Lcom/google/android/gms/wearable/node/au;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/wearable/service/z;-><init>(Landroid/content/pm/PackageManager;Lcom/google/android/gms/wearable/node/o;Lcom/google/android/gms/wearable/node/a;Lcom/google/android/gms/wearable/service/e;Lcom/google/android/gms/wearable/node/a/a;Lcom/google/android/gms/wearable/node/au;Landroid/telecom/TelecomManager;)V

    .line 390
    iget-object v1, p0, Lcom/google/android/gms/wearable/service/o;->a:Lcom/google/android/gms/wearable/service/WearableService;

    invoke-static {v1}, Lcom/google/android/gms/wearable/service/WearableService;->b(Lcom/google/android/gms/wearable/service/WearableService;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v3, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 392
    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {p1, v1, v0, v2}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 394
    :catch_0
    move-exception v0

    const-string v0, "WearableService"

    const-string v1, "Client died while brokering wearable service"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 368
    :catch_1
    move-exception v0

    invoke-interface {p1, v2, v7, v7}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :cond_5
    move-object v0, v7

    .line 374
    goto :goto_1

    .line 382
    :catch_2
    move-exception v0

    :try_start_4
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not get TELECOM_SERVICE in SDK "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2
.end method
