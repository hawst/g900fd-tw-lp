.class public final Lcom/google/android/gms/appdatasearch/be;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final d:Lcom/google/android/gms/appdatasearch/be;


# instance fields
.field private final a:I

.field private final b:J

.field private final c:J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide/16 v2, -0x1

    .line 47
    new-instance v0, Lcom/google/android/gms/appdatasearch/be;

    const/4 v1, -0x1

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/appdatasearch/be;-><init>(IJJ)V

    sput-object v0, Lcom/google/android/gms/appdatasearch/be;->d:Lcom/google/android/gms/appdatasearch/be;

    return-void
.end method

.method private constructor <init>(IJJ)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    if-eqz p1, :cond_0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 53
    iput p1, p0, Lcom/google/android/gms/appdatasearch/be;->a:I

    .line 54
    iput-wide p2, p0, Lcom/google/android/gms/appdatasearch/be;->b:J

    .line 55
    iput-wide p4, p0, Lcom/google/android/gms/appdatasearch/be;->c:J

    .line 56
    return-void

    .line 51
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a([Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/be;
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 129
    if-eqz p0, :cond_0

    array-length v2, p0

    const/4 v3, 0x3

    if-ge v2, v3, :cond_1

    .line 130
    :cond_0
    sget-object v0, Lcom/google/android/gms/appdatasearch/be;->d:Lcom/google/android/gms/appdatasearch/be;

    .line 152
    :goto_0
    return-object v0

    .line 134
    :cond_1
    const-string v2, "documents"

    aget-object v3, p0, v1

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 143
    :goto_1
    const/4 v0, 0x1

    :try_start_0
    aget-object v0, p0, v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 144
    const/4 v0, 0x2

    aget-object v0, p0, v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 146
    const-wide/16 v6, 0x0

    cmp-long v0, v2, v6

    if-ltz v0, :cond_2

    const-wide/16 v6, 0x1

    cmp-long v0, v4, v6

    if-gez v0, :cond_5

    .line 147
    :cond_2
    sget-object v0, Lcom/google/android/gms/appdatasearch/be;->d:Lcom/google/android/gms/appdatasearch/be;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 136
    :cond_3
    const-string v2, "tags"

    aget-object v1, p0, v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v0

    .line 137
    goto :goto_1

    .line 139
    :cond_4
    sget-object v0, Lcom/google/android/gms/appdatasearch/be;->d:Lcom/google/android/gms/appdatasearch/be;

    goto :goto_0

    .line 150
    :cond_5
    :try_start_1
    new-instance v0, Lcom/google/android/gms/appdatasearch/be;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/appdatasearch/be;-><init>(IJJ)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 152
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/gms/appdatasearch/be;->d:Lcom/google/android/gms/appdatasearch/be;

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/google/android/gms/appdatasearch/be;->a:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 80
    iget-wide v0, p0, Lcom/google/android/gms/appdatasearch/be;->b:J

    return-wide v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 101
    iget-wide v0, p0, Lcom/google/android/gms/appdatasearch/be;->c:J

    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 106
    iget v0, p0, Lcom/google/android/gms/appdatasearch/be;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 107
    const-string v0, "SyncQuery[type=Unrecognized]"

    .line 110
    :goto_0
    return-object v0

    .line 109
    :cond_0
    iget v0, p0, Lcom/google/android/gms/appdatasearch/be;->a:I

    if-nez v0, :cond_1

    const-string v0, "Documents"

    .line 110
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SyncQuery[type="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lastSeqNo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/appdatasearch/be;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", limit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/appdatasearch/be;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 109
    :cond_1
    const-string v0, "Tags"

    goto :goto_1
.end method
