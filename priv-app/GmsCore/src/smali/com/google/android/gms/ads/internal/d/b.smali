.class public final Lcom/google/android/gms/ads/internal/d/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# static fields
.field public static a:Lcom/google/android/gms/common/a/d;

.field public static b:Lcom/google/android/gms/common/a/d;

.field public static c:Lcom/google/android/gms/common/a/d;

.field public static d:Lcom/google/android/gms/common/a/d;

.field public static e:Lcom/google/android/gms/common/a/d;

.field public static f:Lcom/google/android/gms/common/a/d;

.field public static g:Lcom/google/android/gms/common/a/d;

.field public static h:Lcom/google/android/gms/common/a/d;

.field public static i:Lcom/google/android/gms/common/a/d;

.field public static j:Lcom/google/android/gms/common/a/d;

.field public static k:Lcom/google/android/gms/common/a/d;

.field public static l:Lcom/google/android/gms/common/a/d;

.field public static m:Lcom/google/android/gms/common/a/d;

.field public static n:Lcom/google/android/gms/common/a/d;

.field public static o:Lcom/google/android/gms/common/a/d;

.field public static p:Lcom/google/android/gms/common/a/d;

.field public static q:Lcom/google/android/gms/common/a/d;

.field private static final r:Landroid/os/Bundle;

.field private static s:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0xa

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 56
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Lcom/google/android/gms/ads/internal/d/b;->r:Landroid/os/Bundle;

    .line 57
    sput-boolean v2, Lcom/google/android/gms/ads/internal/d/b;->s:Z

    .line 63
    const-string v0, "gads:sdk_core_location"

    const-string v1, "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/sdk-core-v40.html"

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/d/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/internal/d/b;->a:Lcom/google/android/gms/common/a/d;

    .line 65
    const-string v0, "gads:sdk_core_experiment_id"

    invoke-static {v0, v4}, Lcom/google/android/gms/ads/internal/d/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/internal/d/b;->b:Lcom/google/android/gms/common/a/d;

    .line 66
    const-string v0, "gads:sdk_crash_report_enabled"

    invoke-static {v0, v2}, Lcom/google/android/gms/ads/internal/d/b;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/internal/d/b;->c:Lcom/google/android/gms/common/a/d;

    .line 67
    const-string v0, "gads:sdk_crash_report_full_stacktrace"

    invoke-static {v0, v2}, Lcom/google/android/gms/ads/internal/d/b;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/internal/d/b;->d:Lcom/google/android/gms/common/a/d;

    .line 69
    const-string v0, "gads:block_autoclicks"

    invoke-static {v0, v2}, Lcom/google/android/gms/ads/internal/d/b;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/internal/d/b;->e:Lcom/google/android/gms/common/a/d;

    .line 72
    const-string v0, "gads:block_autoclicks_experiment_id"

    invoke-static {v0, v4}, Lcom/google/android/gms/ads/internal/d/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/internal/d/b;->f:Lcom/google/android/gms/common/a/d;

    .line 75
    sput-boolean v3, Lcom/google/android/gms/ads/internal/d/b;->s:Z

    .line 78
    const-string v0, "gads:enable_content_fetching"

    invoke-static {v0, v2}, Lcom/google/android/gms/ads/internal/d/b;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/internal/d/b;->k:Lcom/google/android/gms/common/a/d;

    .line 80
    const-string v0, "gads:content_length_weight"

    invoke-static {v0, v3}, Lcom/google/android/gms/ads/internal/d/b;->a(Ljava/lang/String;I)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/internal/d/b;->l:Lcom/google/android/gms/common/a/d;

    .line 82
    const-string v0, "gads:content_age_weight"

    invoke-static {v0, v3}, Lcom/google/android/gms/ads/internal/d/b;->a(Ljava/lang/String;I)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/internal/d/b;->m:Lcom/google/android/gms/common/a/d;

    .line 84
    const-string v0, "gads:min_content_len"

    const/16 v1, 0xb

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/d/b;->a(Ljava/lang/String;I)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/internal/d/b;->n:Lcom/google/android/gms/common/a/d;

    .line 86
    const-string v0, "gads:fingerprint_number"

    invoke-static {v0, v5}, Lcom/google/android/gms/ads/internal/d/b;->a(Ljava/lang/String;I)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/internal/d/b;->o:Lcom/google/android/gms/common/a/d;

    .line 88
    const-string v0, "gads:sleep_sec"

    invoke-static {v0, v5}, Lcom/google/android/gms/ads/internal/d/b;->a(Ljava/lang/String;I)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/internal/d/b;->p:Lcom/google/android/gms/common/a/d;

    .line 92
    const-string v0, "gads:spam_app_context:enabled"

    invoke-static {v0, v2}, Lcom/google/android/gms/ads/internal/d/b;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/internal/d/b;->g:Lcom/google/android/gms/common/a/d;

    .line 94
    const-string v0, "gads:spam_app_context:experiment_id"

    invoke-static {v0, v4}, Lcom/google/android/gms/ads/internal/d/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/internal/d/b;->h:Lcom/google/android/gms/common/a/d;

    .line 97
    const-string v0, "gads:request_builder:singleton_webview"

    invoke-static {v0, v2}, Lcom/google/android/gms/ads/internal/d/b;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/internal/d/b;->i:Lcom/google/android/gms/common/a/d;

    .line 99
    const-string v0, "gads:request_builder:singleton_webview_experiment_id"

    invoke-static {v0, v4}, Lcom/google/android/gms/ads/internal/d/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/internal/d/b;->j:Lcom/google/android/gms/common/a/d;

    .line 102
    const-string v0, "gads:interstitial_follow_url"

    invoke-static {v0, v3}, Lcom/google/android/gms/ads/internal/d/b;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/internal/d/b;->q:Lcom/google/android/gms/common/a/d;

    .line 104
    return-void
.end method

.method public static a()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->r:Landroid/os/Bundle;

    return-object v0
.end method

.method private static a(Ljava/lang/String;I)Lcom/google/android/gms/common/a/d;
    .locals 1

    .prologue
    .line 175
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->r:Landroid/os/Bundle;

    invoke-virtual {v0, p0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 176
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;
    .locals 1

    .prologue
    .line 158
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->r:Landroid/os/Bundle;

    invoke-virtual {v0, p0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    invoke-static {p0, p1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;
    .locals 1

    .prologue
    .line 164
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->r:Landroid/os/Bundle;

    invoke-virtual {v0, p0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 165
    invoke-static {p0, p1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    return-object v0
.end method

.method public static b()Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 111
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 112
    sget-boolean v0, Lcom/google/android/gms/ads/internal/d/b;->s:Z

    if-nez v0, :cond_0

    .line 113
    const-string v0, "not initialized"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    move-object v0, v1

    .line 152
    :goto_0
    return-object v0

    .line 117
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 119
    :try_start_0
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 124
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 127
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 129
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->l:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->l:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 130
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 131
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->n:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->n:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 132
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 133
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->p:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->p:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 134
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 136
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 143
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    move-object v0, v1

    .line 152
    goto/16 :goto_0

    .line 149
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method
