.class public final Lcom/google/android/gms/people/sync/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final A:Ljava/lang/String;

.field private static final B:Ljava/lang/String;

.field private static final C:Ljava/lang/String;

.field private static final D:Ljava/lang/String;

.field private static final P:Landroid/net/Uri;

.field private static final Q:Landroid/net/Uri;

.field private static final R:[Ljava/lang/String;

.field private static final S:[Ljava/lang/String;

.field private static final T:[Ljava/lang/String;

.field private static final U:[Ljava/lang/String;

.field private static final V:[Ljava/lang/String;

.field private static final W:[Ljava/lang/String;

.field private static final a:Z

.field private static final aa:[Ljava/lang/String;

.field private static final ac:[Ljava/lang/String;

.field private static final ad:[Ljava/lang/String;

.field private static final ae:[Ljava/lang/String;

.field private static final af:[Ljava/lang/String;

.field private static final ag:[Ljava/lang/String;

.field private static final ah:[Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/String;

.field private static final e:Ljava/lang/String;

.field private static final f:Ljava/lang/String;

.field private static final g:Ljava/lang/String;

.field private static final h:Ljava/lang/String;

.field private static final i:Ljava/lang/String;

.field private static final j:Ljava/lang/String;

.field private static final k:Ljava/lang/String;

.field private static final l:Ljava/lang/String;

.field private static final m:Ljava/lang/String;

.field private static final n:Ljava/lang/String;

.field private static final o:Ljava/lang/String;

.field private static final p:Ljava/lang/String;

.field private static final q:Ljava/lang/String;

.field private static final r:Ljava/lang/String;

.field private static final s:Ljava/lang/String;

.field private static final t:Ljava/lang/String;

.field private static final u:Ljava/lang/String;

.field private static final v:Ljava/lang/String;

.field private static final w:Ljava/lang/String;

.field private static final x:Ljava/lang/String;

.field private static final y:Ljava/lang/String;

.field private static final z:Ljava/lang/String;


# instance fields
.field private final E:Landroid/content/Context;

.field private final F:Landroid/content/ContentResolver;

.field private final G:Lcom/google/android/gms/people/c/e;

.field private final H:Ljava/lang/String;

.field private final I:J

.field private final J:Ljava/lang/String;

.field private final K:Landroid/net/Uri;

.field private final L:Landroid/net/Uri;

.field private final M:Landroid/net/Uri;

.field private final N:Landroid/net/Uri;

.field private final O:Landroid/net/Uri;

.field private final X:Lcom/google/android/gms/people/sync/a/j;

.field private final Y:Lcom/google/android/gms/people/sync/a/j;

.field private final Z:Lcom/google/android/gms/people/sync/a/j;

.field private final ab:Lcom/google/android/gms/people/sync/a/o;

.field private final ai:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 105
    invoke-static {v6}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/people/sync/a/a;->a:Z

    .line 202
    const-string v0, "0"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->b:Ljava/lang/String;

    .line 203
    const-string v0, "1"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->c:Ljava/lang/String;

    .line 204
    const-string v0, "2"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->d:Ljava/lang/String;

    .line 205
    const-string v0, "3"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->e:Ljava/lang/String;

    .line 207
    const-string v0, "0"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->f:Ljava/lang/String;

    .line 208
    const-string v0, "1"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->g:Ljava/lang/String;

    .line 209
    const-string v0, "2"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->h:Ljava/lang/String;

    .line 210
    const-string v0, "3"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->i:Ljava/lang/String;

    .line 211
    const-string v0, "4"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->j:Ljava/lang/String;

    .line 212
    const-string v0, "5"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->k:Ljava/lang/String;

    .line 213
    const-string v0, "6"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->l:Ljava/lang/String;

    .line 214
    const-string v0, "7"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->m:Ljava/lang/String;

    .line 215
    const-string v0, "8"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->n:Ljava/lang/String;

    .line 216
    const-string v0, "9"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->o:Ljava/lang/String;

    .line 217
    const-string v0, "10"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->p:Ljava/lang/String;

    .line 218
    const-string v0, "11"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->q:Ljava/lang/String;

    .line 219
    const-string v0, "12"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->r:Ljava/lang/String;

    .line 220
    const-string v0, "13"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->s:Ljava/lang/String;

    .line 221
    const-string v0, "14"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->t:Ljava/lang/String;

    .line 222
    const-string v0, "15"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->u:Ljava/lang/String;

    .line 223
    const-string v0, "16"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->v:Ljava/lang/String;

    .line 224
    const-string v0, "17"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->w:Ljava/lang/String;

    .line 225
    const-string v0, "18"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->x:Ljava/lang/String;

    .line 226
    const-string v0, "19"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->y:Ljava/lang/String;

    .line 227
    const-string v0, "20"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->z:Ljava/lang/String;

    .line 229
    const-string v0, "0"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->A:Ljava/lang/String;

    .line 230
    const-string v0, "1"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->B:Ljava/lang/String;

    .line 231
    const-string v0, "2"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->C:Ljava/lang/String;

    .line 232
    const-string v0, "3"

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->D:Ljava/lang/String;

    .line 308
    invoke-static {}, Lcom/google/android/gms/people/sync/a/a;->e()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->P:Landroid/net/Uri;

    .line 309
    invoke-static {}, Lcom/google/android/gms/people/sync/a/a;->f()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->Q:Landroid/net/Uri;

    .line 585
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "sync1"

    aput-object v1, v0, v4

    const-string v1, "sync2"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->R:[Ljava/lang/String;

    .line 596
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "mimetype"

    aput-object v1, v0, v3

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "data1"

    aput-object v1, v0, v5

    const-string v1, "data2"

    aput-object v1, v0, v6

    const-string v1, "data3"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->S:[Ljava/lang/String;

    .line 836
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "sourceid"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->T:[Ljava/lang/String;

    .line 900
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "sourceid"

    aput-object v1, v0, v4

    const-string v1, "sync1"

    aput-object v1, v0, v5

    const-string v1, "sync3"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->U:[Ljava/lang/String;

    .line 1170
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "account_name"

    aput-object v1, v0, v3

    const-string v1, "account_type"

    aput-object v1, v0, v4

    const-string v1, "data_set"

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, "sourceid"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "sync2"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->V:[Ljava/lang/String;

    .line 1243
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "avatar"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->W:[Ljava/lang/String;

    .line 1567
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "sourceid"

    aput-object v1, v0, v3

    const-string v1, "mimetype"

    aput-object v1, v0, v4

    const-string v1, "data_id"

    aput-object v1, v0, v5

    const-string v1, "data1"

    aput-object v1, v0, v6

    const-string v1, "data2"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "data3"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->aa:[Ljava/lang/String;

    .line 2024
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->ac:[Ljava/lang/String;

    .line 2212
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "sourceid"

    aput-object v1, v0, v4

    const-string v1, "data_id"

    aput-object v1, v0, v5

    const-string v1, "mimetype"

    aput-object v1, v0, v6

    const-string v1, "sync3"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->ad:[Ljava/lang/String;

    .line 2268
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "sourceid"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->ae:[Ljava/lang/String;

    .line 2323
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "qualified_id"

    aput-object v1, v0, v3

    const-string v1, "avatar"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->af:[Ljava/lang/String;

    .line 2499
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "raw_contact_id"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->ag:[Ljava/lang/String;

    .line 2943
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "raw_contact_id"

    aput-object v1, v0, v4

    const-string v1, "data5"

    aput-object v1, v0, v5

    const-string v1, "data4"

    aput-object v1, v0, v6

    const-string v1, "data3"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/gms/people/sync/a/a;->ah:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1470
    new-instance v0, Lcom/google/android/gms/people/sync/a/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/sync/a/b;-><init>(Lcom/google/android/gms/people/sync/a/a;)V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->X:Lcom/google/android/gms/people/sync/a/j;

    .line 1498
    new-instance v0, Lcom/google/android/gms/people/sync/a/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/sync/a/c;-><init>(Lcom/google/android/gms/people/sync/a/a;)V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->Y:Lcom/google/android/gms/people/sync/a/j;

    .line 1526
    new-instance v0, Lcom/google/android/gms/people/sync/a/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/sync/a/d;-><init>(Lcom/google/android/gms/people/sync/a/a;)V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->Z:Lcom/google/android/gms/people/sync/a/j;

    .line 1583
    new-instance v0, Lcom/google/android/gms/people/sync/a/o;

    invoke-direct {v0}, Lcom/google/android/gms/people/sync/a/o;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->ab:Lcom/google/android/gms/people/sync/a/o;

    .line 3121
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->ai:Ljava/util/Map;

    .line 312
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    .line 313
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->F:Landroid/content/ContentResolver;

    .line 314
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->G:Lcom/google/android/gms/people/c/e;

    .line 315
    iput-object p2, p0, Lcom/google/android/gms/people/sync/a/a;->H:Ljava/lang/String;

    .line 316
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lcom/google/android/gms/people/c/b;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/people/sync/a/a;->I:J

    .line 317
    iget-wide v0, p0, Lcom/google/android/gms/people/sync/a/a;->I:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 318
    new-instance v0, Lcom/google/android/gms/people/sync/a/i;

    invoke-direct {v0}, Lcom/google/android/gms/people/sync/a/i;-><init>()V

    throw v0

    .line 320
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/people/sync/a/a;->I:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->J:Ljava/lang/String;

    .line 322
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->H:Ljava/lang/String;

    sget-object v1, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_type"

    const-string v3, "com.google"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "data_set"

    const-string v3, "plus"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->K:Landroid/net/Uri;

    .line 323
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->H:Ljava/lang/String;

    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_type"

    const-string v3, "com.google"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "data_set"

    const-string v3, "plus"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->L:Landroid/net/Uri;

    .line 324
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->H:Ljava/lang/String;

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_type"

    const-string v3, "com.google"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "data_set"

    const-string v3, "plus"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->M:Landroid/net/Uri;

    .line 325
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->H:Ljava/lang/String;

    sget-object v1, Landroid/provider/ContactsContract$RawContactsEntity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_type"

    const-string v3, "com.google"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "data_set"

    const-string v3, "plus"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->N:Landroid/net/Uri;

    .line 326
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->H:Ljava/lang/String;

    sget-object v1, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "profile"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "raw_contacts"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_type"

    const-string v3, "com.google"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "data_set"

    const-string v3, "plus"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->O:Landroid/net/Uri;

    .line 327
    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 3148
    if-nez p0, :cond_1

    move v0, v1

    .line 3160
    :cond_0
    :goto_0
    return v0

    .line 3151
    :cond_1
    sget-boolean v0, Lcom/google/android/gms/people/sync/a/a;->a:Z

    if-eqz v0, :cond_2

    .line 3152
    const-string v0, "http"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "Avatar URL is compressed"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3155
    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 3156
    if-eqz v0, :cond_3

    if-ne v0, v1, :cond_0

    .line 3157
    :cond_3
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private a(JLjava/lang/String;IZLcom/google/android/gms/people/sync/v;)Lcom/google/android/gms/people/sync/a/h;
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1940
    invoke-virtual {p6}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 1942
    new-instance v1, Lcom/google/android/gms/people/sync/a/h;

    invoke-direct {v1, v3}, Lcom/google/android/gms/people/sync/a/h;-><init>(B)V

    .line 1943
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->L:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 1944
    const/4 v5, 0x3

    invoke-static {v5}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1945
    const-string v5, "PeopleContactsSync"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "downloadLargeAvatar: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1950
    :cond_0
    if-eqz p3, :cond_1

    .line 1951
    :try_start_0
    iget-object v5, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/gms/people/sync/a/a;->e(Landroid/content/Context;)I

    move-result v5

    .line 1954
    iget-object v6, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    const/4 v7, 0x0

    invoke-static {v6, p3, v5, v7}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/d/p;)[B

    move-result-object v5

    .line 1955
    const-string v6, "display_photo"

    invoke-static {v0, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 1957
    iget v0, v1, Lcom/google/android/gms/people/sync/a/h;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/google/android/gms/people/sync/a/h;->a:I

    .line 1958
    if-eqz v5, :cond_7

    array-length v7, v5

    sget-object v0, Lcom/google/android/gms/people/a/a;->as:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v7, v0, :cond_7

    .line 1959
    const-string v0, "PeopleContactsSync"

    const-string v4, "Avatar too big: %d bytes, %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    array-length v5, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v7, v8

    const/4 v5, 0x1

    aput-object p3, v7, v5

    invoke-static {v4, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1963
    iget v0, v1, Lcom/google/android/gms/people/sync/a/h;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/google/android/gms/people/sync/a/h;->c:I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2

    move-object v0, v2

    move v2, v3

    .line 1965
    :goto_0
    if-eqz v0, :cond_6

    .line 1967
    :try_start_1
    iget-object v4, p0, Lcom/google/android/gms/people/sync/a/a;->F:Landroid/content/ContentResolver;

    invoke-virtual {v4, v6}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v4

    .line 1968
    if-nez v4, :cond_3

    .line 1969
    new-instance v0, Lcom/google/android/gms/people/sync/a/g;

    const-string v4, "Cannot open stream to write image to Contacts"

    invoke-direct {v0, v4}, Lcom/google/android/gms/people/sync/a/g;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2

    .line 1992
    :catch_0
    move-exception v0

    move v4, v2

    move-object v2, v0

    .line 1981
    :goto_1
    :try_start_2
    const-string v0, "PeopleContactsSync"

    const-string v5, "IOException from CP2"

    invoke-static {v0, v5, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1983
    sget-object v0, Lcom/google/android/gms/people/a/a;->at:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_1
    move v3, v4

    .line 1995
    :goto_2
    :pswitch_0
    if-eqz v3, :cond_2

    .line 1997
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1998
    const-string v0, "data15"

    invoke-virtual {v2, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1999
    if-eqz p5, :cond_4

    invoke-static {}, Lcom/google/android/gms/people/sync/a/a;->f()Landroid/net/Uri;

    move-result-object v0

    .line 2001
    :goto_3
    iget-object v3, p0, Lcom/google/android/gms/people/sync/a/a;->F:Landroid/content/ContentResolver;

    const-string v4, "(mimetype =\'vnd.android.cursor.item/photo\') AND raw_contact_id=?"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2007
    :cond_2
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2012
    const-string v0, "sync2"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2013
    const-string v0, "sync3"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2014
    iget-object v3, p0, Lcom/google/android/gms/people/sync/a/a;->F:Landroid/content/ContentResolver;

    if-eqz p5, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->O:Landroid/net/Uri;

    :goto_4
    const-string v4, "_id=?"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_5
    move-object v0, v1

    .line 2021
    :goto_6
    return-object v0

    .line 1974
    :cond_3
    :try_start_3
    invoke-virtual {v4, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1976
    :try_start_4
    iget v0, v1, Lcom/google/android/gms/people/sync/a/h;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/google/android/gms/people/sync/a/h;->b:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1978
    :try_start_5
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    .line 1992
    :catch_1
    move-exception v0

    move-object v2, v0

    move v4, v3

    goto :goto_1

    .line 1978
    :catchall_0
    move-exception v0

    :goto_7
    :try_start_6
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    throw v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_2

    .line 2017
    :catch_2
    move-exception v0

    .line 2018
    const-string v2, "PeopleContactsSync"

    const-string v3, "Cannot download avatar: "

    invoke-static {v2, v3, p3, v0}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 1985
    :pswitch_1
    :try_start_7
    new-instance v0, Lcom/google/android/gms/people/sync/a/g;

    const-string v3, "IOException from CP2"

    invoke-direct {v0, v3, v2}, Lcom/google/android/gms/people/sync/a/g;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :pswitch_2
    move-object v0, v1

    .line 1987
    goto :goto_6

    .line 1999
    :cond_4
    invoke-static {}, Lcom/google/android/gms/people/sync/a/a;->e()Landroid/net/Uri;

    move-result-object v0

    goto :goto_3

    .line 2014
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->L:Landroid/net/Uri;
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_4

    .line 1978
    :catchall_1
    move-exception v0

    move v2, v3

    goto :goto_7

    :cond_6
    move v3, v2

    goto/16 :goto_2

    :cond_7
    move-object v0, v5

    move v2, v4

    goto/16 :goto_0

    .line 1983
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/gms/people/sync/a/n;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/people/sync/a/l;
    .locals 1

    .prologue
    .line 101
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/people/sync/a/a;->b(Lcom/google/android/gms/people/sync/a/n;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/people/sync/a/l;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/people/sync/a/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->J:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 362
    invoke-static {}, Lcom/google/android/gms/people/sync/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 387
    :cond_0
    :goto_0
    return-void

    .line 364
    :cond_1
    invoke-static {}, Lcom/google/android/gms/people/sync/a/a;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 365
    const-string v0, "PeopleContactsSync"

    const-string v1, "CP2 sync not supported."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 368
    :cond_2
    const-string v0, "PeopleContactsSync"

    const-string v1, "CP2 sync enabled."

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/people/debug/c;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    invoke-static {p0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    .line 373
    const-string v1, "gplusInstalled"

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Z)V

    .line 375
    const-string v1, "stopContactsSyncAfterCleanup"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Z)V

    .line 378
    const-string v1, "contactStreamsCleanupPending"

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Z)V

    .line 379
    const-string v1, "babelActionsRewritePending"

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Z)V

    .line 380
    const-string v1, "contactsCleanupPending"

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Z)V

    .line 383
    invoke-static {p0}, Lcom/google/android/gms/people/service/o;->a(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 384
    invoke-static {p0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v2

    const-string v3, "CP2 sync enabled"

    invoke-virtual {v2, v0, v3}, Lcom/google/android/gms/people/sync/u;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 14

    .prologue
    .line 1192
    invoke-static {}, Lcom/google/android/gms/people/sync/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1240
    :cond_0
    :goto_0
    return-void

    .line 1194
    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/people/sync/a/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1198
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1199
    const-string v0, "PeopleContactsSync"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "syncRawContact: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1201
    :cond_2
    const/4 v12, 0x0

    .line 1202
    const/4 v11, 0x0

    .line 1203
    const/4 v10, 0x0

    .line 1204
    const-wide/16 v8, 0x0

    .line 1205
    const/4 v7, 0x0

    .line 1206
    const/4 v6, 0x0

    .line 1208
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1209
    sget-object v2, Lcom/google/android/gms/people/sync/a/a;->V:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 1211
    if-eqz v13, :cond_7

    .line 1213
    :try_start_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1214
    const/4 v0, 0x0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1215
    const/4 v0, 0x1

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1216
    const/4 v0, 0x2

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1217
    const/4 v0, 0x3

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1218
    const/4 v0, 0x4

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1219
    const/4 v0, 0x5

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1222
    :goto_1
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    move-object v8, v1

    move-object v12, v6

    .line 1226
    :goto_2
    const-string v1, "com.google"

    invoke-static {v1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "plus"

    invoke-static {v1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    if-nez v8, :cond_4

    .line 1229
    :cond_3
    const-string v0, "PeopleContactsSync"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot sync raw contact: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1222
    :catchall_0
    move-exception v0

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1234
    :cond_4
    :try_start_1
    new-instance v1, Lcom/google/android/gms/people/sync/a/a;

    invoke-direct {v1, p0, v12}, Lcom/google/android/gms/people/sync/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 1235
    iget-object v4, v1, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v4

    const-string v5, "people"

    sget-object v6, Lcom/google/android/gms/people/sync/a/a;->W:[Ljava/lang/String;

    const-string v7, "owner_id=? AND qualified_id=?"

    iget-object v9, v1, Lcom/google/android/gms/people/sync/a/a;->J:Ljava/lang/String;

    invoke-static {v9, v8}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v5, v6, v7, v9}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Lcom/google/android/gms/people/sync/a/i; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v6

    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_5

    const-string v0, "PeopleContactsSync"

    const-string v1, "Cannot find person by qualified ID: "

    const/4 v2, 0x0

    invoke-static {v0, v1, v8, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Lcom/google/android/gms/people/sync/a/i; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 1237
    :catch_0
    move-exception v0

    const-string v0, "PeopleContactsSync"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot sync raw contact: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    invoke-static {v1, v12}, Lcom/google/android/gms/people/internal/as;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1235
    :cond_5
    const/4 v4, 0x0

    :try_start_4
    invoke-interface {v6, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/people/f/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/people/sync/a/a;->a(Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v5

    :try_start_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Lcom/google/android/gms/people/sync/a/i; {:try_start_5 .. :try_end_5} :catch_0

    if-eq v5, v0, :cond_0

    const/4 v6, 0x0

    :try_start_6
    sget-object v7, Lcom/google/android/gms/people/sync/v;->a:Lcom/google/android/gms/people/sync/v;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/people/sync/a/a;->a(JLjava/lang/String;IZLcom/google/android/gms/people/sync/v;)Lcom/google/android/gms/people/sync/a/h;
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lcom/google/android/gms/people/sync/a/i; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_0

    :catch_1
    move-exception v0

    :try_start_7
    const-string v1, "PeopleContactsSync"

    const-string v2, "Cannot download large avatar for person: "

    invoke-static {v1, v2, v8, v0}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_7
    .catch Lcom/google/android/gms/people/sync/a/i; {:try_start_7 .. :try_end_7} :catch_0

    :cond_6
    move v0, v6

    move-object v1, v7

    move-wide v2, v8

    move-object v4, v10

    move-object v5, v11

    move-object v6, v12

    goto/16 :goto_1

    :cond_7
    move v0, v6

    move-wide v2, v8

    move-object v4, v10

    move-object v5, v11

    move-object v8, v7

    goto/16 :goto_2
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/people/sync/v;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 2675
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676
    const-string v0, "PeopleContactsSync"

    const-string v1, "  removeContactsForNonSyncAccounts"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2678
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 2680
    invoke-static {p0}, Lcom/google/android/gms/people/sync/a/a;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 2682
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 2684
    const-string v3, "data_set=\'plus\' AND account_type=\'com.google\'"

    if-eqz v6, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND account_name NOT IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Profile;->CONTENT_RAW_CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "caller_is_syncadapter"

    const-string v5, "true"

    invoke-virtual {v1, v2, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v7

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_2

    new-instance v0, Lcom/google/android/gms/people/sync/a/g;

    const-string v1, "Cannot query profiles"

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/sync/a/g;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-string v3, "_id=?"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 2685
    invoke-virtual {p1}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 2687
    invoke-static {p0, v6}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2688
    invoke-virtual {p1}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 2690
    invoke-static {p0, v6, p1}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/people/sync/v;)V

    .line 2691
    invoke-virtual {p1}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 2693
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 2695
    const-string v2, "PeopleContactsSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "CP2 cleanup done, kept="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " duration="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long/2addr v0, v8

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v2, v0}, Lcom/google/android/gms/people/debug/c;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2697
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/people/sync/v;Landroid/database/Cursor;)V
    .locals 6

    .prologue
    .line 3259
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3262
    :cond_0
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3263
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 3264
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3266
    sget-object v4, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "stream_items"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "account_name"

    invoke-virtual {v2, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "account_type"

    const-string v3, "com.google"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "data_set"

    const-string v3, "plus"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "caller_is_syncadapter"

    const-string v3, "true"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3282
    sget-object v0, Lcom/google/android/gms/people/a/a;->ak:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3285
    invoke-virtual {p1}, Lcom/google/android/gms/people/sync/v;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3290
    :catchall_0
    move-exception v0

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 3288
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    invoke-static {p0, v1, v0}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3290
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 3291
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2767
    :try_start_0
    const-string v3, "data_set=\'plus\' AND account_type=\'com.google\'"

    .line 2771
    if-eqz p1, :cond_0

    .line 2772
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND account_name NOT IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2775
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2777
    sget-object v1, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "caller_is_syncadapter"

    const-string v4, "true"

    invoke-virtual {v1, v2, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 2780
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2782
    if-nez v2, :cond_1

    .line 2783
    new-instance v0, Lcom/google/android/gms/people/sync/a/g;

    const-string v1, "Cannot query groups"

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/sync/a/g;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2798
    :catch_0
    move-exception v0

    const-string v0, "PeopleContactsSync"

    const-string v1, "removeGroupsFromOtherAccounts failed."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2800
    :goto_0
    return-void

    .line 2787
    :cond_1
    :goto_1
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2788
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 2789
    const-string v3, "_id=?"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2794
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/people/sync/v;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2807
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2809
    const-string v3, "data_set=\'plus\' AND account_type=\'com.google\'"

    .line 2813
    if-eqz p1, :cond_0

    .line 2814
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND account_name NOT IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2821
    :cond_0
    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 2824
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-array v2, v8, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v7

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2826
    if-nez v2, :cond_1

    .line 2827
    new-instance v0, Lcom/google/android/gms/people/sync/a/g;

    const-string v1, "Cannot query raw contacts"

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/sync/a/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2831
    :cond_1
    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2832
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2835
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 2838
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2839
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2840
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v6, v8, [Ljava/lang/String;

    aput-object v0, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2843
    invoke-static {p0, v2, v7}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2845
    invoke-virtual {p2}, Lcom/google/android/gms/people/sync/v;->c()V

    goto :goto_1

    .line 2849
    :cond_4
    invoke-static {p0, v2, v8}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    .line 2850
    return-void
.end method

.method private a(Landroid/net/Uri;Ljava/util/List;)V
    .locals 9

    .prologue
    const/4 v1, 0x3

    const/4 v0, 0x0

    .line 1590
    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1591
    const-string v1, "PeopleContactsSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  reconcileContacts: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1593
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/a;->ab:Lcom/google/android/gms/people/sync/a/o;

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/a/o;->clear()V

    .line 1594
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    .line 1595
    new-array v4, v2, [Ljava/lang/String;

    .line 1596
    invoke-static {}, Lcom/google/android/gms/people/internal/at;->a()Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1597
    const-string v1, "_id IN ("

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v0

    .line 1598
    :goto_0
    if-ge v1, v2, :cond_2

    .line 1599
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/sync/a/n;

    .line 1600
    iget-object v5, p0, Lcom/google/android/gms/people/sync/a/a;->ab:Lcom/google/android/gms/people/sync/a/o;

    iget-object v6, v0, Lcom/google/android/gms/people/sync/a/n;->b:Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Lcom/google/android/gms/people/sync/a/o;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1601
    iget-wide v6, v0, Lcom/google/android/gms/people/sync/a/n;->a:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 1602
    if-eqz v1, :cond_1

    .line 1603
    const/16 v0, 0x2c

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1605
    :cond_1
    const/16 v0, 0x3f

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1598
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1607
    :cond_2
    const-string v0, ") AND mimetype IN (\'vnd.android.cursor.item/identity\',\'vnd.android.cursor.item/email_v2\',\'vnd.android.cursor.item/phone_v2\',\'vnd.android.cursor.item/postal-address_v2\')"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1609
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->F:Landroid/content/ContentResolver;

    sget-object v2, Lcom/google/android/gms/people/sync/a/a;->aa:[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1611
    if-nez v8, :cond_3

    .line 1612
    new-instance v0, Lcom/google/android/gms/people/sync/a/g;

    const-string v1, "Cannot query raw contact entities"

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/sync/a/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1616
    :cond_3
    :goto_1
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1617
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1618
    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/a;->ab:Lcom/google/android/gms/people/sync/a/o;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/sync/a/o;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/people/sync/a/n;

    .line 1619
    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x3

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x4

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v0, 0x5

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v1 .. v7}, Lcom/google/android/gms/people/sync/a/a;->a(Lcom/google/android/gms/people/sync/a/n;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1627
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1628
    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/a;->ab:Lcom/google/android/gms/people/sync/a/o;

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/a/o;->clear()V

    throw v0

    .line 1627
    :cond_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1628
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->ab:Lcom/google/android/gms/people/sync/a/o;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/a/o;->clear()V

    .line 1629
    return-void
.end method

.method private a(Lcom/google/android/gms/people/sync/a/k;Lcom/google/android/gms/people/sync/v;)V
    .locals 14

    .prologue
    .line 613
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614
    const-string v0, "PeopleContactsSync"

    const-string v1, "  computeMeProfileDiffs"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 618
    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/a;->O:Landroid/net/Uri;

    .line 621
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->F:Landroid/content/ContentResolver;

    sget-object v2, Lcom/google/android/gms/people/sync/a/a;->R:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 623
    if-nez v3, :cond_1

    .line 624
    new-instance v0, Lcom/google/android/gms/people/sync/a/g;

    const-string v1, "Error reading profile data"

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/sync/a/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 627
    :cond_1
    const-wide/16 v4, 0x0

    .line 628
    const/4 v2, 0x0

    .line 629
    const-wide/16 v0, 0x0

    .line 631
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 632
    const/4 v0, 0x0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 633
    const/4 v0, 0x1

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 634
    const/4 v0, 0x2

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 638
    :cond_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 640
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_3

    .line 641
    const/4 v3, 0x3

    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 642
    const-string v3, "PeopleContactsSync"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "    ME already exists in CP2.  rid="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    :cond_3
    iget-object v3, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v6

    .line 654
    const-string v3, "SELECT display_name,avatar,etag,gaia_id FROM owners WHERE _id=?"

    iget-object v7, p0, Lcom/google/android/gms/people/sync/a/a;->J:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v3, v7}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 664
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-nez v7, :cond_4

    .line 665
    const-string v0, "PeopleContactsSync"

    const-string v1, "Owner not found.  Account removed?  Stopping sync."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    new-instance v0, Lcom/google/android/gms/people/sync/x;

    const-string v1, "[cp2sync] Owner not found"

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/sync/x;-><init>(Ljava/lang/String;)V

    throw v0

    .line 638
    :catchall_0
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 669
    :cond_4
    const/4 v7, 0x0

    :try_start_1
    invoke-interface {v3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 670
    const/4 v8, 0x1

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/gms/people/f/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 671
    const/4 v9, 0x2

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 672
    const/4 v10, 0x3

    invoke-interface {v3, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v10

    .line 674
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 680
    const-wide/16 v12, 0x0

    cmp-long v3, v4, v12

    if-nez v3, :cond_6

    .line 681
    const/4 v3, 0x1

    .line 689
    :goto_0
    const/4 v11, 0x3

    invoke-static {v11}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 690
    const-string v11, "PeopleContactsSync"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "    ME: last-etag="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v12, " new-etag="

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v12, " lastAvatarFP="

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v11, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    :cond_5
    if-nez v3, :cond_9

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_9

    .line 695
    const-string v0, "PeopleContactsSync"

    const-string v1, "    ME unchanged."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 833
    :goto_1
    return-void

    .line 674
    :catchall_1
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 683
    :cond_6
    if-nez v2, :cond_7

    .line 684
    const/4 v3, 0x1

    goto :goto_0

    .line 686
    :cond_7
    invoke-static {v2, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    const/4 v3, 0x1

    goto :goto_0

    :cond_8
    const/4 v3, 0x0

    goto :goto_0

    .line 701
    :cond_9
    new-instance v11, Lcom/google/android/gms/people/sync/a/n;

    invoke-direct {v11}, Lcom/google/android/gms/people/sync/a/n;-><init>()V

    .line 702
    const/4 v2, 0x1

    iput-boolean v2, v11, Lcom/google/android/gms/people/sync/a/n;->f:Z

    .line 703
    iput-wide v4, v11, Lcom/google/android/gms/people/sync/a/n;->a:J

    .line 704
    iput-object v9, v11, Lcom/google/android/gms/people/sync/a/n;->d:Ljava/lang/String;

    .line 705
    invoke-static {v10}, Lcom/google/android/gms/people/internal/at;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v11, Lcom/google/android/gms/people/sync/a/n;->b:Ljava/lang/String;

    .line 706
    iput-object v7, v11, Lcom/google/android/gms/people/sync/a/n;->g:Ljava/lang/String;

    .line 708
    const/4 v2, 0x0

    .line 709
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 710
    iput-object v8, v11, Lcom/google/android/gms/people/sync/a/n;->h:Ljava/lang/String;

    .line 711
    iget-object v2, v11, Lcom/google/android/gms/people/sync/a/n;->h:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/people/sync/a/a;->a(Ljava/lang/String;)I

    move-result v2

    .line 714
    :cond_a
    int-to-long v2, v2

    cmp-long v2, v2, v0

    if-eqz v2, :cond_b

    .line 715
    const/4 v2, 0x1

    iput-boolean v2, v11, Lcom/google/android/gms/people/sync/a/n;->j:Z

    .line 717
    :cond_b
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 718
    const-string v2, "PeopleContactsSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "    ME: name="

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v11, Lcom/google/android/gms/people/sync/a/n;->g:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " avatar="

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v7, v11, Lcom/google/android/gms/people/sync/a/n;->h:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " avatarFP="

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 725
    :cond_c
    const-string v0, "SELECT email,type,custom_label FROM owner_emails WHERE owner_id=?"

    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/a;->J:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 735
    const/4 v0, 0x2

    :try_start_2
    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 736
    const-string v0, "PeopleContactsSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    Emails found: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    :cond_d
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 740
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 741
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 742
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 743
    const/4 v3, 0x2

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 745
    const-string v7, "vnd.android.cursor.item/email_v2"

    invoke-static {v11, v7, v0}, Lcom/google/android/gms/people/sync/a/a;->b(Lcom/google/android/gms/people/sync/a/n;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/people/sync/a/l;

    move-result-object v0

    .line 746
    invoke-static {v0, v2, v3}, Lcom/google/android/gms/people/sync/a/a;->a(Lcom/google/android/gms/people/sync/a/l;ILjava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_2

    .line 749
    :catchall_2
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_e
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 755
    const-string v0, "SELECT phone,type,custom_label FROM owner_phones WHERE owner_id=?"

    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/a;->J:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 765
    const/4 v0, 0x2

    :try_start_3
    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 766
    const-string v0, "PeopleContactsSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    Phones found: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    :cond_f
    :goto_3
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 769
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 770
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 771
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 772
    const/4 v3, 0x2

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 774
    const-string v7, "vnd.android.cursor.item/phone_v2"

    invoke-static {v11, v7, v0}, Lcom/google/android/gms/people/sync/a/a;->b(Lcom/google/android/gms/people/sync/a/n;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/people/sync/a/l;

    move-result-object v0

    .line 775
    iget-object v7, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    invoke-static {v0, v2, v3, v7}, Lcom/google/android/gms/people/sync/a/a;->a(Lcom/google/android/gms/people/sync/a/l;ILjava/lang/String;Landroid/content/Context;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_3

    .line 778
    :catchall_3
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_10
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 784
    const-string v0, "SELECT postal_address,type,custom_label FROM owner_postal_address WHERE owner_id=?"

    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/a;->J:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 794
    const/4 v0, 0x2

    :try_start_4
    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 795
    const-string v0, "PeopleContactsSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    Addresses found: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 797
    :cond_11
    :goto_4
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 798
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 799
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 800
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 801
    const/4 v3, 0x2

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 803
    const-string v6, "vnd.android.cursor.item/postal-address_v2"

    invoke-static {v11, v6, v0}, Lcom/google/android/gms/people/sync/a/a;->b(Lcom/google/android/gms/people/sync/a/n;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/people/sync/a/l;

    move-result-object v0

    .line 804
    invoke-static {v0, v2, v3}, Lcom/google/android/gms/people/sync/a/a;->b(Lcom/google/android/gms/people/sync/a/l;ILjava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    goto :goto_4

    .line 807
    :catchall_4
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_12
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 812
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-eqz v0, :cond_16

    .line 813
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->F:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/a;->ai:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    if-nez v1, :cond_13

    sget-object v1, Landroid/provider/ContactsContract$Profile;->CONTENT_RAW_CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "data"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_type"

    const-string v3, "com.google"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "data_set"

    const-string v3, "plus"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_name"

    iget-object v3, p0, Lcom/google/android/gms/people/sync/a/a;->H:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "caller_is_syncadapter"

    const-string v3, "true"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/people/sync/a/a;->ai:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_13
    sget-object v2, Lcom/google/android/gms/people/sync/a/a;->S:[Ljava/lang/String;

    const-string v3, "mimetype IN (\'vnd.android.cursor.item/identity\',\'vnd.android.cursor.item/email_v2\',\'vnd.android.cursor.item/phone_v2\',\'vnd.android.cursor.item/postal-address_v2\')"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 815
    if-nez v8, :cond_14

    .line 816
    new-instance v0, Lcom/google/android/gms/people/sync/a/g;

    const-string v1, "Cannot query profile data"

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/sync/a/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 820
    :cond_14
    :goto_5
    :try_start_5
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 821
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x3

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v0, 0x4

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v1, v11

    invoke-static/range {v1 .. v7}, Lcom/google/android/gms/people/sync/a/a;->a(Lcom/google/android/gms/people/sync/a/n;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    goto :goto_5

    .line 828
    :catchall_5
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_15
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 832
    :cond_16
    iput-object v11, p1, Lcom/google/android/gms/people/sync/a/k;->f:Lcom/google/android/gms/people/sync/a/n;

    goto/16 :goto_1
.end method

.method static a(Lcom/google/android/gms/people/sync/a/l;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 2528
    packed-switch p1, :pswitch_data_0

    .line 2532
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    .line 2533
    iput-object p2, p0, Lcom/google/android/gms/people/sync/a/l;->e:Ljava/lang/CharSequence;

    .line 2536
    :goto_0
    return-void

    .line 2529
    :pswitch_0
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2530
    :pswitch_1
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2528
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static a(Lcom/google/android/gms/people/sync/a/l;ILjava/lang/String;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2544
    packed-switch p1, :pswitch_data_0

    .line 2571
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    .line 2572
    iput-object p2, p0, Lcom/google/android/gms/people/sync/a/l;->e:Ljava/lang/CharSequence;

    .line 2575
    :goto_0
    return-void

    .line 2545
    :pswitch_0
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2546
    :pswitch_1
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2547
    :pswitch_2
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2548
    :pswitch_3
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2549
    :pswitch_4
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2550
    :pswitch_5
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->s:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2551
    :pswitch_6
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2552
    :pswitch_7
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2553
    :pswitch_8
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->y:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2554
    :pswitch_9
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2555
    :pswitch_a
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->t:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2556
    :pswitch_b
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2557
    :pswitch_c
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2558
    :pswitch_d
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->u:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2559
    :pswitch_e
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2560
    :pswitch_f
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->w:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2561
    :pswitch_10
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->x:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2562
    :pswitch_11
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2565
    :pswitch_12
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    .line 2566
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->rQ:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->e:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2544
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method

.method private static a(Lcom/google/android/gms/people/sync/a/n;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 1637
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1638
    const-string v1, "PeopleContactsSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    reconcileData: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1641
    :cond_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object p4, v0

    .line 1644
    :cond_1
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object p5, v0

    .line 1647
    :cond_2
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object p6, v0

    .line 1651
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/n;->l:Lcom/google/android/gms/people/sync/a/m;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/a/m;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/sync/a/l;

    .line 1652
    iget-object v2, v0, Lcom/google/android/gms/people/sync/a/l;->a:Ljava/lang/String;

    invoke-static {v2, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v0, Lcom/google/android/gms/people/sync/a/l;->c:Ljava/lang/CharSequence;

    invoke-static {v2, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-wide v2, v0, Lcom/google/android/gms/people/sync/a/l;->b:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    .line 1654
    iput-wide p1, v0, Lcom/google/android/gms/people/sync/a/l;->b:J

    .line 1657
    iget-object v1, v0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    invoke-static {v1, p5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/google/android/gms/people/sync/a/l;->e:Ljava/lang/CharSequence;

    invoke-static {v1, p6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1659
    :cond_5
    iput-boolean v6, v0, Lcom/google/android/gms/people/sync/a/l;->g:Z

    .line 1670
    :cond_6
    :goto_0
    return-void

    .line 1666
    :cond_7
    new-instance v0, Lcom/google/android/gms/people/sync/a/l;

    invoke-direct {v0}, Lcom/google/android/gms/people/sync/a/l;-><init>()V

    .line 1667
    iput-wide p1, v0, Lcom/google/android/gms/people/sync/a/l;->b:J

    .line 1668
    iput-boolean v6, v0, Lcom/google/android/gms/people/sync/a/l;->f:Z

    .line 1669
    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/n;->l:Lcom/google/android/gms/people/sync/a/m;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/sync/a/m;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/people/sync/a/o;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 918
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->F:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/a;->L:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/gms/people/sync/a/a;->U:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 920
    if-nez v1, :cond_0

    .line 921
    new-instance v0, Lcom/google/android/gms/people/sync/a/g;

    const-string v1, "Cannot query raw contacts"

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/sync/a/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 924
    :cond_0
    const/4 v0, 0x3

    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 925
    const-string v0, "PeopleContactsSync"

    const-string v2, "    %d people found in CP2"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    :cond_1
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 929
    new-instance v0, Lcom/google/android/gms/people/sync/a/n;

    invoke-direct {v0}, Lcom/google/android/gms/people/sync/a/n;-><init>()V

    .line 930
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/people/sync/a/n;->a:J

    .line 931
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/people/sync/a/n;->b:Ljava/lang/String;

    .line 932
    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/people/sync/a/n;->d:Ljava/lang/String;

    .line 933
    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/google/android/gms/people/sync/a/n;->i:I

    .line 936
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/gms/people/sync/a/n;->e:Z

    .line 937
    iget-object v2, v0, Lcom/google/android/gms/people/sync/a/n;->b:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Lcom/google/android/gms/people/sync/a/o;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 940
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 941
    return-void
.end method

.method private a(Lcom/google/android/gms/people/sync/v;Ljava/util/List;Lcom/google/android/gms/people/d/q;)V
    .locals 11

    .prologue
    .line 2173
    const-string v0, "PeopleContactsSync"

    const-string v1, "    Syncing large avatars..."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2175
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 2177
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/sync/a/e;

    .line 2178
    iget-boolean v1, v0, Lcom/google/android/gms/people/sync/a/e;->e:Z

    if-eqz v1, :cond_0

    .line 2179
    iget-wide v2, v0, Lcom/google/android/gms/people/sync/a/e;->a:J

    iget-object v4, v0, Lcom/google/android/gms/people/sync/a/e;->b:Ljava/lang/String;

    iget v5, v0, Lcom/google/android/gms/people/sync/a/e;->c:I

    const/4 v6, 0x0

    move-object v1, p0

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/people/sync/a/a;->a(JLjava/lang/String;IZLcom/google/android/gms/people/sync/v;)Lcom/google/android/gms/people/sync/a/h;

    move-result-object v0

    .line 2182
    iget v1, p3, Lcom/google/android/gms/people/d/q;->b:I

    iget v2, v0, Lcom/google/android/gms/people/sync/a/h;->a:I

    add-int/2addr v1, v2

    iput v1, p3, Lcom/google/android/gms/people/d/q;->b:I

    .line 2183
    iget v1, p3, Lcom/google/android/gms/people/d/q;->c:I

    iget v2, v0, Lcom/google/android/gms/people/sync/a/h;->b:I

    add-int/2addr v1, v2

    iput v1, p3, Lcom/google/android/gms/people/d/q;->c:I

    .line 2184
    iget v1, p3, Lcom/google/android/gms/people/d/q;->d:I

    iget v0, v0, Lcom/google/android/gms/people/sync/a/h;->c:I

    add-int/2addr v0, v1

    iput v0, p3, Lcom/google/android/gms/people/d/q;->d:I

    .line 2185
    invoke-virtual {p1}, Lcom/google/android/gms/people/sync/v;->c()V

    goto :goto_0

    .line 2188
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, v8

    iput-wide v0, p3, Lcom/google/android/gms/people/d/q;->a:J

    .line 2189
    return-void
.end method

.method private a(Lcom/google/android/gms/people/sync/v;Ljava/util/List;Lcom/google/android/gms/people/d/q;Lcom/google/android/gms/people/d/p;)V
    .locals 15

    .prologue
    .line 2120
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 2124
    sget-object v2, Lcom/google/android/gms/people/a/a;->aq:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 2125
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 2126
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v5

    .line 2127
    const/4 v3, 0x0

    .line 2128
    :goto_0
    if-ge v3, v5, :cond_4

    .line 2129
    sget-object v2, Lcom/google/android/gms/people/a/a;->ao:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v2, v3

    .line 2130
    if-le v2, v5, :cond_6

    move v4, v5

    :goto_1
    move v7, v3

    .line 2134
    :goto_2
    if-ge v7, v4, :cond_3

    .line 2135
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 2137
    move-object/from16 v0, p2

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/people/sync/a/e;

    .line 2138
    iget-object v3, v2, Lcom/google/android/gms/people/sync/a/e;->b:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 2140
    const/4 v3, 0x0

    invoke-direct {p0, v11, v2, v3}, Lcom/google/android/gms/people/sync/a/a;->a(Ljava/util/ArrayList;Lcom/google/android/gms/people/sync/a/e;[B)V

    .line 2134
    :cond_0
    :goto_3
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_2

    .line 2141
    :cond_1
    iget-boolean v3, v2, Lcom/google/android/gms/people/sync/a/e;->e:Z

    if-nez v3, :cond_0

    .line 2144
    iget-object v3, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    iget-object v6, v2, Lcom/google/android/gms/people/sync/a/e;->b:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-static {v3, v6, v10, v0}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/d/p;)[B

    move-result-object v6

    .line 2145
    move-object/from16 v0, p3

    iget v3, v0, Lcom/google/android/gms/people/d/q;->e:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p3

    iput v3, v0, Lcom/google/android/gms/people/d/q;->e:I

    .line 2146
    if-eqz v6, :cond_5

    array-length v12, v6

    sget-object v3, Lcom/google/android/gms/people/a/a;->ar:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-le v12, v3, :cond_5

    .line 2147
    const-string v3, "PeopleContactsSync"

    const-string v12, "Thumbnail too big: %d bytes, %s"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    array-length v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v13, v14

    const/4 v6, 0x1

    iget-object v14, v2, Lcom/google/android/gms/people/sync/a/e;->b:Ljava/lang/String;

    aput-object v14, v13, v6

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2149
    const/4 v3, 0x0

    .line 2150
    move-object/from16 v0, p3

    iget v6, v0, Lcom/google/android/gms/people/d/q;->g:I

    add-int/lit8 v6, v6, 0x1

    move-object/from16 v0, p3

    iput v6, v0, Lcom/google/android/gms/people/d/q;->g:I

    .line 2152
    :goto_4
    if-eqz v3, :cond_2

    .line 2153
    move-object/from16 v0, p3

    iget v6, v0, Lcom/google/android/gms/people/d/q;->f:I

    add-int/lit8 v6, v6, 0x1

    move-object/from16 v0, p3

    iput v6, v0, Lcom/google/android/gms/people/d/q;->f:I

    .line 2155
    :cond_2
    invoke-direct {p0, v11, v2, v3}, Lcom/google/android/gms/people/sync/a/a;->a(Ljava/util/ArrayList;Lcom/google/android/gms/people/sync/a/e;[B)V

    goto :goto_3

    .line 2159
    :cond_3
    iget-object v3, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    sget-object v2, Lcom/google/android/gms/people/a/a;->ap:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v6, 0x0

    invoke-static {v3, v11, v2, v6}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;

    move v3, v4

    .line 2162
    goto/16 :goto_0

    .line 2164
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    const/4 v3, 0x1

    invoke-static {v2, v11, v3}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    .line 2165
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, v8

    move-object/from16 v0, p3

    iput-wide v2, v0, Lcom/google/android/gms/people/d/q;->h:J

    .line 2166
    return-void

    :cond_5
    move-object v3, v6

    goto :goto_4

    :cond_6
    move v4, v2

    goto/16 :goto_1
.end method

.method private a(Ljava/util/ArrayList;Lcom/google/android/gms/people/sync/a/e;[B)V
    .locals 4

    .prologue
    .line 2367
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2368
    const-string v0, "PeopleContactsSync"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "buildThumbnailContentProviderOperations: rid="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p2, Lcom/google/android/gms/people/sync/a/e;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " url="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/google/android/gms/people/sync/a/e;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p2, Lcom/google/android/gms/people/sync/a/e;->d:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " image="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2372
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->L:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "_id=?"

    iget-wide v2, p2, Lcom/google/android/gms/people/sync/a/e;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "sync3"

    iget v2, p2, Lcom/google/android/gms/people/sync/a/e;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "sync2"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2383
    iget-wide v0, p2, Lcom/google/android/gms/people/sync/a/e;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 2384
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->P:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "_id=?"

    iget-wide v2, p2, Lcom/google/android/gms/people/sync/a/e;->d:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data15"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2396
    :cond_1
    :goto_0
    return-void

    .line 2389
    :cond_2
    if-eqz p3, :cond_1

    .line 2390
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->P:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "raw_contact_id"

    iget-wide v2, p2, Lcom/google/android/gms/people/sync/a/e;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/photo"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data15"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;Lcom/google/android/gms/people/sync/a/n;ZZ)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1694
    iget-object v0, p2, Lcom/google/android/gms/people/sync/a/n;->l:Lcom/google/android/gms/people/sync/a/m;

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 1700
    iget-boolean v0, p2, Lcom/google/android/gms/people/sync/a/n;->f:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->O:Landroid/net/Uri;

    .line 1701
    :goto_2
    iget-boolean v3, p2, Lcom/google/android/gms/people/sync/a/n;->f:Z

    if-eqz v3, :cond_7

    sget-object v3, Lcom/google/android/gms/people/sync/a/a;->Q:Landroid/net/Uri;

    .line 1703
    :goto_3
    iget-object v4, p2, Lcom/google/android/gms/people/sync/a/n;->b:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/people/internal/at;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1704
    iget-object v5, p2, Lcom/google/android/gms/people/sync/a/n;->c:Ljava/lang/String;

    .line 1707
    iget-object v5, p2, Lcom/google/android/gms/people/sync/a/n;->g:Ljava/lang/String;

    if-nez v5, :cond_0

    .line 1708
    const-string v5, ""

    iput-object v5, p2, Lcom/google/android/gms/people/sync/a/n;->g:Ljava/lang/String;

    .line 1711
    :cond_0
    if-eqz p3, :cond_9

    .line 1712
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1715
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "sourceid"

    iget-object v6, p2, Lcom/google/android/gms/people/sync/a/n;->b:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "sync1"

    iget-object v6, p2, Lcom/google/android/gms/people/sync/a/n;->d:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "raw_contact_is_read_only"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1723
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "raw_contact_id"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "mimetype"

    const-string v5, "vnd.android.cursor.item/name"

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data1"

    iget-object v5, p2, Lcom/google/android/gms/people/sync/a/n;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1729
    iget-boolean v0, p2, Lcom/google/android/gms/people/sync/a/n;->f:Z

    if-nez v0, :cond_2

    .line 1732
    if-eqz v4, :cond_1

    .line 1735
    if-eqz p4, :cond_8

    const-string v0, "vnd.android.cursor.item/vnd.googleplus.profile.comm"

    .line 1739
    :goto_4
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v4, "raw_contact_id"

    invoke-virtual {v1, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v4, "mimetype"

    invoke-virtual {v1, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v4, "data4"

    const/16 v5, 0xa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v4, "data5"

    const-string v5, "conversation"

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v4, "data3"

    iget-object v5, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    sget v6, Lcom/google/android/gms/p;->rY:I

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1752
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v4, "raw_contact_id"

    invoke-virtual {v1, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v4, "mimetype"

    invoke-virtual {v1, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data4"

    const/16 v4, 0xe

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data5"

    const-string v4, "hangout"

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data3"

    iget-object v4, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    sget v5, Lcom/google/android/gms/p;->rZ:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1764
    :cond_1
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "raw_contact_id"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "mimetype"

    const-string v4, "vnd.android.cursor.item/vnd.googleplus.profile"

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data4"

    const/16 v4, 0x14

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data5"

    const-string v4, "addtocircle"

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data3"

    iget-object v4, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    sget v5, Lcom/google/android/gms/p;->sa:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1777
    :cond_2
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "raw_contact_id"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "mimetype"

    const-string v4, "vnd.android.cursor.item/vnd.googleplus.profile"

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data4"

    const/16 v4, 0x1e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data5"

    const-string v4, "view"

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data3"

    iget-object v4, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    sget v5, Lcom/google/android/gms/p;->sb:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1829
    :goto_5
    iget-object v0, p2, Lcom/google/android/gms/people/sync/a/n;->l:Lcom/google/android/gms/people/sync/a/m;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/a/m;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/sync/a/l;

    .line 1831
    iget-boolean v1, v0, Lcom/google/android/gms/people/sync/a/l;->f:Z

    if-eqz v1, :cond_c

    .line 1832
    iget-wide v0, v0, Lcom/google/android/gms/people/sync/a/l;->b:J

    invoke-static {v3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    .line 1861
    :goto_7
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_4
    move v0, v2

    .line 1694
    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto/16 :goto_1

    .line 1700
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->L:Landroid/net/Uri;

    goto/16 :goto_2

    .line 1701
    :cond_7
    sget-object v3, Lcom/google/android/gms/people/sync/a/a;->P:Landroid/net/Uri;

    goto/16 :goto_3

    .line 1735
    :cond_8
    const-string v0, "vnd.android.cursor.item/vnd.googleplus.profile"

    goto/16 :goto_4

    .line 1786
    :cond_9
    iget-wide v4, p2, Lcom/google/android/gms/people/sync/a/n;->a:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 1788
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "_id=?"

    invoke-static {v4}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "sync1"

    iget-object v5, p2, Lcom/google/android/gms/people/sync/a/n;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1795
    iget-boolean v1, p2, Lcom/google/android/gms/people/sync/a/n;->j:Z

    if-eqz v1, :cond_a

    .line 1798
    const-string v1, "sync2"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v5, "sync3"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1802
    :cond_a
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1804
    iget-boolean v0, p2, Lcom/google/android/gms/people/sync/a/n;->j:Z

    if-eqz v0, :cond_b

    .line 1806
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "mimetype=\'vnd.android.cursor.item/photo\' AND raw_contact_id=?"

    invoke-static {v4}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1817
    :cond_b
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "mimetype=\'vnd.android.cursor.item/name\' AND raw_contact_id=? AND data1!=?"

    iget-object v5, p2, Lcom/google/android/gms/people/sync/a/n;->g:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "data1"

    iget-object v4, p2, Lcom/google/android/gms/people/sync/a/n;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 1836
    :cond_c
    iget-wide v6, v0, Lcom/google/android/gms/people/sync/a/l;->b:J

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-nez v1, :cond_e

    .line 1837
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    .line 1838
    const-string v5, "mimetype"

    iget-object v6, v0, Lcom/google/android/gms/people/sync/a/l;->a:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1839
    if-eqz p3, :cond_d

    .line 1840
    const-string v5, "raw_contact_id"

    invoke-virtual {v1, v5, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 1855
    :goto_8
    const-string v5, "data1"

    iget-object v6, v0, Lcom/google/android/gms/people/sync/a/l;->c:Ljava/lang/CharSequence;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1856
    const-string v5, "data2"

    iget-object v6, v0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1857
    const-string v5, "data3"

    iget-object v0, v0, Lcom/google/android/gms/people/sync/a/l;->e:Ljava/lang/CharSequence;

    invoke-virtual {v1, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1858
    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    goto/16 :goto_7

    .line 1843
    :cond_d
    const-string v5, "raw_contact_id"

    iget-wide v6, p2, Lcom/google/android/gms/people/sync/a/n;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto :goto_8

    .line 1845
    :cond_e
    iget-boolean v1, v0, Lcom/google/android/gms/people/sync/a/l;->g:Z

    if-eqz v1, :cond_3

    .line 1849
    sget-object v1, Lcom/google/android/gms/people/sync/a/a;->P:Landroid/net/Uri;

    iget-wide v6, v0, Lcom/google/android/gms/people/sync/a/l;->b:J

    invoke-static {v1, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    goto :goto_8

    .line 1864
    :cond_f
    const/4 v0, 0x0

    iput-object v0, p2, Lcom/google/android/gms/people/sync/a/n;->l:Lcom/google/android/gms/people/sync/a/m;

    .line 1865
    return-void
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/List;Lcom/google/android/gms/people/sync/v;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 1343
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1344
    const-string v0, "PeopleContactsSync"

    const-string v2, "  deleteDeletedContacts"

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1346
    :cond_0
    invoke-virtual {p3}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 1348
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    move v0, v1

    .line 1349
    :goto_0
    if-ge v0, v2, :cond_1

    .line 1350
    iget-object v3, p0, Lcom/google/android/gms/people/sync/a/a;->L:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "_id=?"

    new-array v5, v7, [Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1349
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1357
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    .line 1358
    return-void
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/List;ZZ)V
    .locals 3

    .prologue
    .line 1679
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    .line 1680
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1681
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/sync/a/n;

    invoke-direct {p0, p1, v0, p3, p4}, Lcom/google/android/gms/people/sync/a/a;->a(Ljava/util/ArrayList;Lcom/google/android/gms/people/sync/a/n;ZZ)V

    .line 1680
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1683
    :cond_0
    return-void
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/List;ZZLcom/google/android/gms/people/sync/v;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 1367
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1368
    const-string v0, "PeopleContactsSync"

    const-string v1, "  updateContacts"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1370
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    move v3, v4

    .line 1372
    :goto_0
    if-ge v3, v2, :cond_5

    .line 1373
    invoke-virtual {p5}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 1375
    sget-object v0, Lcom/google/android/gms/people/a/a;->al:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v3

    .line 1376
    if-le v0, v2, :cond_6

    move v1, v2

    .line 1380
    :goto_1
    invoke-interface {p2, v3, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    .line 1382
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    move v3, v4

    :goto_2
    if-ge v3, v6, :cond_3

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/sync/a/n;

    iget-boolean v7, v0, Lcom/google/android/gms/people/sync/a/n;->k:Z

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/gms/people/sync/a/a;->X:Lcom/google/android/gms/people/sync/a/j;

    invoke-virtual {v7, v0}, Lcom/google/android/gms/people/sync/a/j;->a(Lcom/google/android/gms/people/sync/a/n;)V

    iget-object v7, p0, Lcom/google/android/gms/people/sync/a/a;->Y:Lcom/google/android/gms/people/sync/a/j;

    invoke-virtual {v7, v0}, Lcom/google/android/gms/people/sync/a/j;->a(Lcom/google/android/gms/people/sync/a/n;)V

    iget-object v7, p0, Lcom/google/android/gms/people/sync/a/a;->Z:Lcom/google/android/gms/people/sync/a/j;

    invoke-virtual {v7, v0}, Lcom/google/android/gms/people/sync/a/j;->a(Lcom/google/android/gms/people/sync/a/n;)V

    :cond_1
    iget-object v7, v0, Lcom/google/android/gms/people/sync/a/n;->c:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "gprofile:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Lcom/google/android/gms/people/internal/at;->m(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "vnd.android.cursor.item/identity"

    invoke-static {v0, v8, v7}, Lcom/google/android/gms/people/sync/a/a;->b(Lcom/google/android/gms/people/sync/a/n;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/people/sync/a/l;

    move-result-object v0

    const-string v7, "com.google"

    iput-object v7, v0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 1384
    :cond_3
    if-nez p3, :cond_4

    .line 1385
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->N:Landroid/net/Uri;

    invoke-direct {p0, v0, v5}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/net/Uri;Ljava/util/List;)V

    .line 1388
    :cond_4
    invoke-direct {p0, p1, v5, p3, p4}, Lcom/google/android/gms/people/sync/a/a;->a(Ljava/util/ArrayList;Ljava/util/List;ZZ)V

    .line 1389
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    invoke-static {v0, p1, v4}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    move v3, v1

    .line 1391
    goto :goto_0

    .line 1392
    :cond_5
    return-void

    :cond_6
    move v1, v0

    goto :goto_1
.end method

.method private a(Ljava/util/HashSet;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2510
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->F:Landroid/content/ContentResolver;

    sget-object v2, Lcom/google/android/gms/people/sync/a/a;->ag:[Ljava/lang/String;

    const/4 v4, 0x0

    move-object v1, p2

    move-object v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2512
    if-eqz v1, :cond_1

    .line 2514
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2515
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2518
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2521
    :cond_1
    return-void
.end method

.method private a(Ljava/util/List;Lcom/google/android/gms/people/sync/v;)V
    .locals 8

    .prologue
    .line 1320
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1321
    const-string v0, "PeopleContactsSync"

    const-string v1, "  deleteExtraneousGroups"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1323
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 1325
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 1326
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1327
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1328
    iget-object v3, p0, Lcom/google/android/gms/people/sync/a/a;->F:Landroid/content/ContentResolver;

    iget-object v4, p0, Lcom/google/android/gms/people/sync/a/a;->K:Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1326
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1332
    :catch_0
    move-exception v0

    const-string v0, "PeopleContactsSync"

    const-string v1, "deleteExtraneousGroups failed."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1334
    :cond_1
    return-void
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 331
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/people/sync/v;)Z
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v4, 0x0

    const/4 v11, 0x0

    .line 2033
    invoke-virtual {p1}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 2036
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->F:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/a;->L:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/gms/people/sync/a/a;->ac:[Ljava/lang/String;

    const-string v3, "sync3=0 OR sync3 IS NULL"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2040
    if-nez v0, :cond_0

    .line 2041
    new-instance v0, Lcom/google/android/gms/people/sync/a/g;

    const-string v1, "Cannot obtain count of raw contacts requiring avatars"

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/sync/a/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2046
    :cond_0
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_2

    .line 2048
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move v11, v12

    .line 2085
    :cond_1
    :goto_0
    return v11

    .line 2048
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2054
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/a/a;->d()[Ljava/lang/String;

    move-result-object v9

    .line 2055
    array-length v0, v9

    if-eqz v0, :cond_1

    .line 2061
    invoke-static {}, Lcom/google/android/gms/people/internal/at;->a()Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2062
    const-string v0, "_id IN ("

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v11

    .line 2063
    :goto_1
    array-length v2, v9

    if-ge v0, v2, :cond_4

    .line 2064
    if-eqz v0, :cond_3

    .line 2065
    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2067
    :cond_3
    const/16 v2, 0x3f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2063
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2051
    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1

    .line 2069
    :cond_4
    const-string v0, ") AND (sync2=0 OR sync2 IS NULL)"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2073
    iget-object v5, p0, Lcom/google/android/gms/people/sync/a/a;->F:Landroid/content/ContentResolver;

    iget-object v6, p0, Lcom/google/android/gms/people/sync/a/a;->L:Landroid/net/Uri;

    sget-object v7, Lcom/google/android/gms/people/sync/a/a;->ac:[Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v10, v4

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2076
    if-nez v0, :cond_5

    .line 2077
    new-instance v0, Lcom/google/android/gms/people/sync/a/g;

    const-string v1, "Cannot obtain count of raw contacts requiring large avatars"

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/sync/a/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2083
    :cond_5
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-lez v1, :cond_6

    move v11, v12

    .line 2085
    :cond_6
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_1
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/d/p;)[B
    .locals 8

    .prologue
    .line 2404
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2408
    new-instance v0, Lcom/google/android/gms/common/internal/bs;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/internal/bs;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/common/internal/bs;->d:Z

    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/internal/bs;->a(I)Lcom/google/android/gms/common/internal/br;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/br;->a()Ljava/lang/String;

    move-result-object v0

    .line 2413
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 2415
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/people/service/m;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/m;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v1, v0, v4}, Lcom/google/android/gms/people/service/m;->b(Ljava/lang/String;Z)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2418
    if-eqz p3, :cond_0

    .line 2419
    iget-wide v4, p3, Lcom/google/android/gms/people/d/p;->b:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v2, v6, v2

    add-long/2addr v2, v4

    iput-wide v2, p3, Lcom/google/android/gms/people/d/p;->b:J

    .line 2423
    :cond_0
    sget-object v1, Lcom/google/android/gms/people/service/a/a/e;->i:[B

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    :cond_1
    return-object v0

    .line 2418
    :catchall_0
    move-exception v0

    if-eqz p3, :cond_2

    .line 2419
    iget-wide v4, p3, Lcom/google/android/gms/people/d/p;->b:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v2, v6, v2

    add-long/2addr v2, v4

    iput-wide v2, p3, Lcom/google/android/gms/people/d/p;->b:J

    :cond_2
    throw v0
.end method

.method private static a(Landroid/content/Context;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x3

    .line 1884
    invoke-static {v6}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1885
    const-string v0, "PeopleContactsSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "flushBatch: force="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " minsize="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1888
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1889
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderOperation;

    .line 1890
    const-string v3, "PeopleContactsSync"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "  "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1895
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1896
    if-nez v0, :cond_1

    move-object v0, v1

    .line 1916
    :goto_1
    return-object v0

    .line 1900
    :cond_1
    if-nez p3, :cond_2

    if-ge v0, p2, :cond_2

    move-object v0, v1

    .line 1902
    goto :goto_1

    .line 1904
    :cond_2
    invoke-static {v6}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1905
    const-string v0, "PeopleContactsSync"

    const-string v1, "  applying batch"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1908
    :cond_3
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com.android.contacts"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1915
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    .line 1911
    :catch_0
    move-exception v0

    .line 1912
    new-instance v1, Lcom/google/android/gms/people/sync/a/g;

    const-string v2, "Error appying batch of CPOs"

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/people/sync/a/g;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;
    .locals 1

    .prologue
    .line 1874
    sget-object v0, Lcom/google/android/gms/people/a/a;->ak:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0, p1, v0, p2}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/people/sync/a/a;)Lcom/google/android/gms/people/c/e;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->G:Lcom/google/android/gms/people/c/e;

    return-object v0
.end method

.method private static b(Lcom/google/android/gms/people/sync/a/n;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/people/sync/a/l;
    .locals 2

    .prologue
    .line 1559
    new-instance v0, Lcom/google/android/gms/people/sync/a/l;

    invoke-direct {v0}, Lcom/google/android/gms/people/sync/a/l;-><init>()V

    .line 1560
    iput-object p1, v0, Lcom/google/android/gms/people/sync/a/l;->a:Ljava/lang/String;

    .line 1561
    iput-object p2, v0, Lcom/google/android/gms/people/sync/a/l;->c:Ljava/lang/CharSequence;

    .line 1562
    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/n;->l:Lcom/google/android/gms/people/sync/a/m;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/sync/a/m;->add(Ljava/lang/Object;)Z

    .line 1563
    return-object v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2597
    invoke-static {}, Lcom/google/android/gms/people/sync/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2623
    :cond_0
    :goto_0
    return-void

    .line 2599
    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/people/sync/a/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2604
    const-string v0, "PeopleContactsSync"

    const-string v3, "triggerPendingContactsCleanup"

    invoke-static {v0, v3}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2612
    invoke-static {p0}, Lcom/google/android/gms/people/service/o;->b(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v3

    .line 2613
    array-length v0, v3

    if-nez v0, :cond_2

    .line 2614
    const-string v0, "PeopleContactsSync"

    const-string v1, "triggerPendingContactsCleanup: no accounts"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2617
    :cond_2
    invoke-static {p0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    const-string v4, "contactStreamsCleanupPending"

    invoke-virtual {v0, v4, v2}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "contactsCleanupPending"

    invoke-virtual {v0, v4, v2}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "babelActionsRewritePending"

    invoke-virtual {v0, v4, v2}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    move v0, v2

    :goto_1
    if-eqz v0, :cond_5

    .line 2618
    const-string v0, "PeopleContactsSync"

    const-string v4, "  Starting sync..."

    invoke-static {v0, v4}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2619
    invoke-static {p0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    aget-object v1, v3, v1

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    const-string v4, "PeopleSync"

    const-string v5, "requestContactSyncCleanup"

    invoke-static {v3, v4, v1, v5}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2, v2}, Lcom/google/android/gms/people/sync/u;->a(ZZ)Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "gms.people.contacts_sync"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "gms.people.skip_main_sync"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v2, v0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    const-string v2, "com.google.android.gms.people"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 2617
    goto :goto_1

    .line 2621
    :cond_5
    const-string v0, "PeopleContactsSync"

    const-string v1, "  No pending cleanups."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private b(Lcom/google/android/gms/people/sync/a/k;Lcom/google/android/gms/people/sync/v;)V
    .locals 6

    .prologue
    .line 846
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 847
    const-string v0, "PeopleContactsSync"

    const-string v1, "  computeGroupDiffs"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 854
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->F:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/a;->K:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/gms/people/sync/a/a;->T:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 855
    if-nez v1, :cond_1

    .line 856
    new-instance v0, Lcom/google/android/gms/people/sync/a/g;

    const-string v1, "Cannot query groups"

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/sync/a/g;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 869
    :catch_0
    move-exception v0

    const-string v0, "PeopleContactsSync"

    const-string v1, "computeGroupDiffs failed."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 871
    :goto_0
    return-void

    .line 860
    :cond_1
    :goto_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 861
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 862
    iget-object v0, p1, Lcom/google/android/gms/people/sync/a/k;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p1, Lcom/google/android/gms/people/sync/a/k;->b:Ljava/util/ArrayList;

    :cond_2
    iget-object v0, p1, Lcom/google/android/gms/people/sync/a/k;->b:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 865
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0
.end method

.method static b(Lcom/google/android/gms/people/sync/a/l;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 2582
    packed-switch p1, :pswitch_data_0

    .line 2586
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->A:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    .line 2587
    iput-object p2, p0, Lcom/google/android/gms/people/sync/a/l;->e:Ljava/lang/CharSequence;

    .line 2590
    :goto_0
    return-void

    .line 2583
    :pswitch_0
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->B:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2584
    :pswitch_1
    sget-object v0, Lcom/google/android/gms/people/sync/a/a;->C:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/sync/a/l;->d:Ljava/lang/CharSequence;

    goto :goto_0

    .line 2582
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(Lcom/google/android/gms/people/sync/a/o;)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1017
    iget-object v3, p0, Lcom/google/android/gms/people/sync/a/a;->G:Lcom/google/android/gms/people/c/e;

    const-string v4, "SELECT qualified_id,etag,name,in_circle,avatar,mobile_owner_id FROM people P WHERE owner_id=?1 AND name IS NOT NULL AND profile_type=1 AND (( EXISTS (SELECT 1 FROM owners WHERE _id=?1 AND sync_circles_to_contacts=1) AND qualified_id IN (SELECT qualified_id FROM circle_members WHERE owner_id=?1 AND circle_id IN (SELECT circle_id FROM circles WHERE owner_id=?1 AND type=-1 AND for_sharing=1)))OR( EXISTS (SELECT 1 FROM owners WHERE _id=?1 AND sync_evergreen_to_contacts=1) AND in_contacts=1 AND (?2 OR EXISTS (SELECT 1 FROM gaia_id_map G  WHERE G.owner_id=P.owner_id AND G.type!=2 AND G.gaia_id=P.gaia_id))))"

    iget-object v5, p0, Lcom/google/android/gms/people/sync/a/a;->J:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/people/a/a;->ae:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "1"

    :goto_0
    invoke-static {v5, v0}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 1030
    const/4 v0, 0x3

    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1031
    const-string v0, "PeopleContactsSync"

    const-string v3, "    %d people found in db"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1034
    :cond_0
    :goto_1
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1035
    const/4 v0, 0x0

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1038
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x1

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ":1"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1039
    const/4 v0, 0x2

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1040
    const/4 v0, 0x3

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_2

    move v4, v1

    .line 1041
    :goto_2
    const/4 v0, 0x4

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/f/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1043
    const/4 v0, 0x5

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1045
    invoke-virtual {p1, v3}, Lcom/google/android/gms/people/sync/a/o;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/sync/a/n;

    .line 1046
    if-nez v0, :cond_3

    .line 1048
    new-instance v0, Lcom/google/android/gms/people/sync/a/n;

    invoke-direct {v0}, Lcom/google/android/gms/people/sync/a/n;-><init>()V

    .line 1049
    iput-object v3, v0, Lcom/google/android/gms/people/sync/a/n;->b:Ljava/lang/String;

    .line 1050
    iget-object v3, v0, Lcom/google/android/gms/people/sync/a/n;->b:Ljava/lang/String;

    invoke-virtual {p1, v3, v0}, Lcom/google/android/gms/people/sync/a/o;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v3, v0

    .line 1058
    :goto_3
    iput-object v6, v3, Lcom/google/android/gms/people/sync/a/n;->d:Ljava/lang/String;

    .line 1059
    iput-object v7, v3, Lcom/google/android/gms/people/sync/a/n;->g:Ljava/lang/String;

    .line 1060
    const/4 v0, 0x0

    iput-boolean v0, v3, Lcom/google/android/gms/people/sync/a/n;->e:Z

    .line 1061
    iget v0, v3, Lcom/google/android/gms/people/sync/a/n;->i:I

    invoke-static {v8}, Lcom/google/android/gms/people/sync/a/a;->a(Ljava/lang/String;)I

    move-result v6

    if-eq v0, v6, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, v3, Lcom/google/android/gms/people/sync/a/n;->j:Z

    .line 1063
    iput-boolean v4, v3, Lcom/google/android/gms/people/sync/a/n;->k:Z

    .line 1064
    iput-object v9, v3, Lcom/google/android/gms/people/sync/a/n;->c:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1067
    :catchall_0
    move-exception v0

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1017
    :cond_1
    const-string v0, "0"

    goto/16 :goto_0

    :cond_2
    move v4, v2

    .line 1040
    goto :goto_2

    .line 1051
    :cond_3
    :try_start_1
    iget-object v10, v0, Lcom/google/android/gms/people/sync/a/n;->d:Ljava/lang/String;

    invoke-static {v10, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1054
    invoke-virtual {p1, v3}, Lcom/google/android/gms/people/sync/a/o;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_4
    move v0, v2

    .line 1061
    goto :goto_4

    .line 1067
    :cond_5
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 1068
    return-void

    :cond_6
    move-object v3, v0

    goto :goto_3
.end method

.method private static b()Z
    .locals 2

    .prologue
    .line 346
    sget-object v0, Lcom/google/android/gms/people/a/a;->aj:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->a(Lcom/google/android/gms/common/a/d;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 347
    const-string v0, "PeopleContactsSync"

    const-string v1, "CP2 sync disabled by gservices."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    const/4 v0, 0x1

    .line 350
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/people/sync/a/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    return-object v0
.end method

.method private c()Lcom/google/android/gms/people/sync/a/f;
    .locals 10

    .prologue
    .line 2196
    new-instance v9, Lcom/google/android/gms/people/sync/a/f;

    invoke-direct {v9}, Lcom/google/android/gms/people/sync/a/f;-><init>()V

    .line 2198
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->F:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/a;->N:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/gms/people/sync/a/a;->ad:[Ljava/lang/String;

    const-string v3, "(mimetype=\'vnd.android.cursor.item/photo\' OR mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\')AND(sync3=0 OR sync3 IS NULL)"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/android/gms/people/sync/a/g;

    const-string v1, "Cannot obtain a list of missing thumbnails"

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/sync/a/g;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Lcom/google/android/gms/people/sync/a/f;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/sync/a/e;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/people/sync/a/e;

    invoke-direct {v0}, Lcom/google/android/gms/people/sync/a/e;-><init>()V

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v0, Lcom/google/android/gms/people/sync/a/e;->a:J

    const/4 v3, 0x4

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v0, Lcom/google/android/gms/people/sync/a/e;->c:I

    invoke-virtual {v9, v2, v0}, Lcom/google/android/gms/people/sync/a/f;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "vnd.android.cursor.item/photo"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/people/sync/a/e;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2199
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/a/a;->d()[Ljava/lang/String;

    move-result-object v4

    array-length v0, v4

    if-eqz v0, :cond_8

    invoke-static {}, Lcom/google/android/gms/people/internal/at;->a()Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v0, "_id IN ("

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    :goto_1
    array-length v1, v4

    if-ge v0, v1, :cond_4

    if-eqz v0, :cond_3

    const/16 v1, 0x2c

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_3
    const/16 v1, 0x3f

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    const-string v0, ") AND (sync2=0 OR sync2 IS NULL)"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->F:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/a;->L:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/gms/people/sync/a/a;->ae:[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_5

    new-instance v0, Lcom/google/android/gms/people/sync/a/g;

    const-string v1, "Cannot obtain raw contacts w/o large avatars"

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/sync/a/g;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :goto_2
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Lcom/google/android/gms/people/sync/a/f;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/sync/a/e;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/android/gms/people/sync/a/e;

    invoke-direct {v0}, Lcom/google/android/gms/people/sync/a/e;-><init>()V

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v0, Lcom/google/android/gms/people/sync/a/e;->a:J

    invoke-virtual {v9, v2, v0}, Lcom/google/android/gms/people/sync/a/f;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/gms/people/sync/a/e;->e:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_7
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2200
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v0

    const-string v1, "people"

    sget-object v2, Lcom/google/android/gms/people/sync/a/a;->af:[Ljava/lang/String;

    const-string v3, "owner_id=?"

    iget-object v4, p0, Lcom/google/android/gms/people/sync/a/a;->J:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    iget-object v0, v0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :cond_9
    :goto_3
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/f/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/people/sync/a/a;->a(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v9, v2}, Lcom/google/android/gms/people/sync/a/f;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/sync/a/e;

    if-eqz v0, :cond_9

    iget v5, v0, Lcom/google/android/gms/people/sync/a/e;->c:I

    if-ne v5, v4, :cond_a

    invoke-virtual {v9, v2}, Lcom/google/android/gms/people/sync/a/f;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_3

    :catchall_2
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_a
    :try_start_3
    iput v4, v0, Lcom/google/android/gms/people/sync/a/e;->c:I

    iput-object v3, v0, Lcom/google/android/gms/people/sync/a/e;->b:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_3

    :cond_b
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2202
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2203
    const-string v0, "PeopleContactsSync"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "    Avatar state map: size="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/google/android/gms/people/sync/a/f;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2204
    invoke-virtual {v9}, Lcom/google/android/gms/people/sync/a/f;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2205
    const-string v2, "PeopleContactsSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "      "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 2209
    :cond_c
    return-object v9
.end method

.method public static c(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2857
    const-string v2, "PeopleContactsSync"

    const-string v3, "onPackageChanged"

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2859
    invoke-static {}, Lcom/google/android/gms/people/sync/a/a;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2870
    :goto_0
    return-void

    .line 2861
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/people/sync/a/a;->d(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2862
    const-string v0, "PeopleContactsSync"

    const-string v1, "CP2 sync disabled"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2866
    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->a()V

    :try_start_0
    const-string v4, "babelInstalled"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {p0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/people/ah;->o()Lcom/google/android/gms/people/f/i;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/people/f/i;->a()Z

    move-result v5

    if-eq v4, v5, :cond_2

    const-string v4, "PeopleContactsSync"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Hangout changed: installed="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v4, v6}, Lcom/google/android/gms/people/debug/c;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "babelInstalled"

    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Z)V

    const-string v4, "babelActionsRewritePending"

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Z)V

    :cond_2
    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v3, v1}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 2867
    invoke-static {p0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->a()V

    :try_start_1
    const-string v4, "gplusInstalled"

    invoke-virtual {v2, v4}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {p0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/people/ah;->o()Lcom/google/android/gms/people/f/i;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/gms/people/f/i;->a:Landroid/content/Context;

    const-string v5, "com.google.android.apps.plus"

    const-string v6, "gcore_cp2_sync_supported"

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/people/f/i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    move v0, v1

    :goto_1
    :pswitch_0
    if-nez v0, :cond_3

    const-string v0, "PeopleContactsSync"

    const-string v4, "G+ app uninstalled."

    invoke-static {p0, v0, v4}, Lcom/google/android/gms/people/debug/c;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/gms/people/c/f;->p()Z

    const-string v0, "contactsCleanupPending"

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v4}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Z)V

    const-string v0, "stopContactsSyncAfterCleanup"

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v4}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Z)V

    const-string v0, "gplusInstalled"

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v4}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Z)V

    :cond_3
    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v3, v1}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 2869
    invoke-static {p0}, Lcom/google/android/gms/people/sync/a/a;->b(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2866
    :catchall_0
    move-exception v0

    invoke-virtual {v3, v1}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v0

    .line 2867
    :pswitch_1
    :try_start_2
    sget-object v0, Lcom/google/android/gms/people/a/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    goto :goto_1

    :catchall_1
    move-exception v0

    invoke-virtual {v3, v1}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static d(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 338
    invoke-static {}, Lcom/google/android/gms/people/sync/a/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->a()Lcom/google/android/gms/people/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/a/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()[Ljava/lang/String;
    .locals 5

    .prologue
    .line 2460
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 2462
    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/a;->M:Landroid/net/Uri;

    const-string v2, "starred!=0 AND mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\'"

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/people/sync/a/a;->a(Ljava/util/HashSet;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 2463
    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/a;->M:Landroid/net/Uri;

    const-string v2, "starred=0 AND times_contacted>0 AND mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\'"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "times_contacted DESC LIMIT "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/gms/people/a/a;->am:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/people/sync/a/a;->a(Ljava/util/HashSet;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 2464
    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/a;->M:Landroid/net/Uri;

    const-string v2, "starred=0 AND times_contacted>0 AND mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\'"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "last_time_contacted DESC LIMIT "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/gms/people/a/a;->an:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/people/sync/a/a;->a(Ljava/util/HashSet;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 2466
    sget-object v1, Lcom/google/android/gms/people/internal/at;->d:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method private static e(Landroid/content/Context;)I
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 2434
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$DisplayPhoto;->CONTENT_MAX_DIMENSIONS_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2437
    if-eqz v1, :cond_1

    .line 2439
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2440
    const-string v0, "display_max_dim"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 2443
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2452
    :goto_0
    return v0

    .line 2443
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2452
    :cond_1
    :goto_1
    const/16 v0, 0x100

    goto :goto_0

    .line 2443
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 2448
    :catch_0
    move-exception v0

    const-string v0, "PeopleContactsSync"

    const-string v1, "getPreferredAvatarSize failed, using the default size."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static e()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 3075
    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static f()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 3086
    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "profile"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "data"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static f(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2707
    invoke-static {p0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v0

    const-string v1, "SELECT account_name FROM owners WHERE page_gaia_id IS NULL AND (sync_to_contacts!=0) ORDER BY _id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2714
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/people/internal/at;->a()Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v0, ""

    const/4 v3, -0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    const-string v0, ","

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2716
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/people/sync/v;Lcom/google/android/gms/people/d/p;)Lcom/google/android/gms/people/d/p;
    .locals 21

    .prologue
    .line 453
    invoke-static {}, Lcom/google/android/gms/people/sync/a/a;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 p2, 0x0

    .line 520
    :goto_0
    return-object p2

    .line 455
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/people/sync/a/a;->d(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 457
    const/16 p2, 0x0

    goto :goto_0

    .line 459
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    const-string v5, "PeopleContactsSync"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/people/sync/a/a;->H:Ljava/lang/String;

    const-string v7, "CP2 sync start"

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    .line 461
    const/4 v14, 0x0

    .line 464
    :try_start_0
    const-string v4, "PeopleContactsSync"

    const-string v5, "  performPendingContactsCleanup"

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v11

    const-string v4, "contactStreamsCleanupPending"

    const/4 v5, 0x1

    invoke-virtual {v11, v4, v5}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    const-string v4, "PeopleContactsSync"

    const-string v5, "removeAllStreamItemsFromContacts"

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "stream_items"

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "_id"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "account_name"

    aput-object v8, v6, v7

    const-string v7, "account_type=\'com.google\' AND data_set=\'plus\'"

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    if-nez v4, :cond_2

    new-instance v4, Lcom/google/android/gms/people/sync/a/g;

    const-string v5, "Cannot select stream items"

    invoke-direct {v4, v5}, Lcom/google/android/gms/people/sync/a/g;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 486
    :catch_0
    move-exception v4

    move-object v5, v4

    .line 487
    :try_start_1
    sget-object v4, Lcom/google/android/gms/people/a/a;->aE:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_25

    .line 488
    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 492
    :catchall_0
    move-exception v4

    move-object v5, v4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long/2addr v6, v12

    move-object/from16 v0, p2

    iput-wide v6, v0, Lcom/google/android/gms/people/d/p;->a:J

    .line 494
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    if-eqz v4, :cond_27

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    .line 496
    :goto_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    const-string v7, "PeopleContactsSync"

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/people/sync/a/a;->H:Ljava/lang/String;

    const-string v9, "===CP2 sync finished, success=%s, time=%d,%d,%d,%d,%d,%d rc=%d,%d,%d,%d av=%d,%d,%d,%d,%d,%d"

    const/16 v10, 0x11

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/google/android/gms/people/d/p;->a:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/google/android/gms/people/d/p;->c:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/google/android/gms/people/d/p;->d:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x4

    iget-wide v12, v4, Lcom/google/android/gms/people/d/q;->h:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x5

    iget-wide v12, v4, Lcom/google/android/gms/people/d/q;->a:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x6

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/google/android/gms/people/d/p;->b:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x7

    move-object/from16 v0, p2

    iget v12, v0, Lcom/google/android/gms/people/d/p;->f:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/16 v11, 0x8

    move-object/from16 v0, p2

    iget v12, v0, Lcom/google/android/gms/people/d/p;->i:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/16 v11, 0x9

    move-object/from16 v0, p2

    iget v12, v0, Lcom/google/android/gms/people/d/p;->g:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/16 v11, 0xa

    move-object/from16 v0, p2

    iget v12, v0, Lcom/google/android/gms/people/d/p;->h:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/16 v11, 0xb

    iget v12, v4, Lcom/google/android/gms/people/d/q;->e:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/16 v11, 0xc

    iget v12, v4, Lcom/google/android/gms/people/d/q;->f:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/16 v11, 0xd

    iget v12, v4, Lcom/google/android/gms/people/d/q;->g:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/16 v11, 0xe

    iget v12, v4, Lcom/google/android/gms/people/d/q;->b:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/16 v11, 0xf

    iget v12, v4, Lcom/google/android/gms/people/d/q;->c:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/16 v11, 0x10

    iget v4, v4, Lcom/google/android/gms/people/d/q;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v7, v8, v4}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    throw v5

    .line 464
    :cond_2
    :try_start_2
    move-object/from16 v0, p1

    invoke-static {v10, v0, v4}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/people/sync/v;Landroid/database/Cursor;)V

    const-string v4, "PeopleContactsSync"

    const-string v5, "removeAllStreamItemsFromMeProfile"

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/ContactsContract$Profile;->CONTENT_RAW_CONTACTS_URI:Landroid/net/Uri;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "_id"

    aput-object v8, v6, v7

    const-string v7, "account_type=\'com.google\' AND data_set=\'plus\'"

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    if-nez v15, :cond_4

    new-instance v4, Lcom/google/android/gms/people/sync/a/g;

    const-string v5, "Cannot select profile raw contacts"

    invoke-direct {v4, v5}, Lcom/google/android/gms/people/sync/a/g;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    :try_start_3
    move-object/from16 v0, p1

    invoke-static {v10, v0, v4}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/people/sync/v;Landroid/database/Cursor;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_4
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x0

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "stream_items"

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "_id"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "account_name"

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    if-nez v4, :cond_3

    new-instance v4, Lcom/google/android/gms/people/sync/a/g;

    const-string v5, "Cannot select stream items"

    invoke-direct {v4, v5}, Lcom/google/android/gms/people/sync/a/g;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v4

    :try_start_5
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    throw v4
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_2
    move-exception v5

    :try_start_6
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :cond_5
    :try_start_7
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    const-string v4, "contactStreamsCleanupPending"

    const/4 v5, 0x0

    invoke-virtual {v11, v4, v5}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Z)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/people/sync/v;->c()V

    :cond_6
    const-string v4, "contactsCleanupPending"

    const/4 v5, 0x1

    invoke-virtual {v11, v4, v5}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/people/sync/v;)V

    const-string v4, "contactsCleanupPending"

    const/4 v5, 0x0

    invoke-virtual {v11, v4, v5}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Z)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/people/sync/v;->c()V

    :cond_7
    const-string v4, "babelActionsRewritePending"

    const/4 v5, 0x1

    invoke-virtual {v11, v4, v5}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_c

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    const-string v4, "PeopleContactsSync"

    const-string v5, "  updateBabelActionMimeType"

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v15}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/people/ah;->o()Lcom/google/android/gms/people/f/i;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/people/f/i;->a()Z

    move-result v4

    if-eqz v4, :cond_9

    const-string v4, "vnd.android.cursor.item/vnd.googleplus.profile.comm"

    move-object v10, v4

    :goto_2
    const/4 v4, 0x3

    invoke-static {v4}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v4

    if-eqz v4, :cond_8

    const-string v4, "PeopleContactsSync"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "  new mimetype="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v15}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/people/sync/a/a;->P:Landroid/net/Uri;

    sget-object v6, Lcom/google/android/gms/people/sync/a/a;->ah:[Ljava/lang/String;

    const-string v7, "account_type=\'com.google\' AND data_set=\'plus\' AND mimetype IN (\'vnd.android.cursor.item/vnd.googleplus.profile\',\'vnd.android.cursor.item/vnd.googleplus.profile.comm\') AND data5 IN (\'conversation\',\'hangout\')"

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    if-nez v5, :cond_a

    new-instance v4, Lcom/google/android/gms/people/sync/a/g;

    const-string v5, "Cannot obtain babel actions"

    invoke-direct {v4, v5}, Lcom/google/android/gms/people/sync/a/g;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_9
    const-string v4, "vnd.android.cursor.item/vnd.googleplus.profile"
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-object v10, v4

    goto :goto_2

    :cond_a
    :goto_3
    :try_start_8
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_b

    const/4 v4, 0x0

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/4 v4, 0x1

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const/4 v4, 0x2

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/16 v17, 0x3

    move/from16 v0, v17

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x4

    move/from16 v0, v18

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    sget-object v19, Lcom/google/android/gms/people/sync/a/a;->P:Landroid/net/Uri;

    invoke-static/range {v19 .. v19}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v19

    const-string v20, "_id=?"

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v6}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v6, Lcom/google/android/gms/people/sync/a/a;->P:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "raw_contact_id"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "mimetype"

    invoke-virtual {v6, v7, v10}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data5"

    invoke-virtual {v6, v7, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v6, "data4"

    move-object/from16 v0, v17

    invoke-virtual {v4, v6, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v6, "data3"

    move-object/from16 v0, v18

    invoke-virtual {v4, v6, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v4, Lcom/google/android/gms/people/a/a;->ak:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-static {v15, v0, v4, v6}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/people/sync/v;->c()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    goto/16 :goto_3

    :catchall_3
    move-exception v4

    :try_start_9
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v4

    :cond_b
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    const/4 v4, 0x1

    move-object/from16 v0, v16

    invoke-static {v15, v0, v4}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    const-string v4, "PeopleContactsSync"

    const-string v5, "Updated babel action mime type"

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "babelActionsRewritePending"

    const/4 v5, 0x0

    invoke-virtual {v11, v4, v5}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Z)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 467
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v4

    const-string v5, "stopContactsSyncAfterCleanup"

    invoke-virtual {v4, v5}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 470
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/people/ah;->a()Lcom/google/android/gms/people/a/c;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/gms/people/a/c;->a(Z)V

    .line 472
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    const-string v5, "PeopleContactsSync"

    const-string v6, "CP2 sync disabled.  (reverse-handover)"

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/people/debug/c;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 473
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v12

    move-object/from16 v0, p2

    iput-wide v4, v0, Lcom/google/android/gms/people/d/p;->a:J

    .line 494
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    if-eqz v4, :cond_d

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    .line 496
    :goto_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    const-string v6, "PeopleContactsSync"

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/people/sync/a/a;->H:Ljava/lang/String;

    const-string v8, "===CP2 sync finished, success=%s, time=%d,%d,%d,%d,%d,%d rc=%d,%d,%d,%d av=%d,%d,%d,%d,%d,%d"

    const/16 v9, 0x11

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/google/android/gms/people/d/p;->a:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/google/android/gms/people/d/p;->c:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x3

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/google/android/gms/people/d/p;->d:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x4

    iget-wide v12, v4, Lcom/google/android/gms/people/d/q;->h:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x5

    iget-wide v12, v4, Lcom/google/android/gms/people/d/q;->a:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x6

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/google/android/gms/people/d/p;->b:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x7

    move-object/from16 v0, p2

    iget v11, v0, Lcom/google/android/gms/people/d/p;->f:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0x8

    move-object/from16 v0, p2

    iget v11, v0, Lcom/google/android/gms/people/d/p;->i:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0x9

    move-object/from16 v0, p2

    iget v11, v0, Lcom/google/android/gms/people/d/p;->g:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xa

    move-object/from16 v0, p2

    iget v11, v0, Lcom/google/android/gms/people/d/p;->h:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xb

    iget v11, v4, Lcom/google/android/gms/people/d/q;->e:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xc

    iget v11, v4, Lcom/google/android/gms/people/d/q;->f:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xd

    iget v11, v4, Lcom/google/android/gms/people/d/q;->g:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xe

    iget v11, v4, Lcom/google/android/gms/people/d/q;->b:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xf

    iget v11, v4, Lcom/google/android/gms/people/d/q;->c:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0x10

    iget v4, v4, Lcom/google/android/gms/people/d/q;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v6, v7, v4}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    const/16 p2, 0x0

    goto/16 :goto_0

    .line 494
    :cond_d
    new-instance v4, Lcom/google/android/gms/people/d/q;

    invoke-direct {v4}, Lcom/google/android/gms/people/d/q;-><init>()V

    goto/16 :goto_4

    .line 477
    :cond_e
    const/4 v4, 0x3

    :try_start_a
    invoke-static {v4}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v4

    if-eqz v4, :cond_f

    const-string v4, "PeopleContactsSync"

    const-string v5, "computePeopleSyncDiffs"

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    new-instance v15, Lcom/google/android/gms/people/sync/a/k;

    invoke-direct {v15}, Lcom/google/android/gms/people/sync/a/k;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v4

    const-string v5, "SELECT sync_to_contacts FROM owners WHERE _id=?"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/people/sync/a/a;->J:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    const-wide/16 v8, 0x0

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_15

    const/4 v4, 0x1

    :goto_5
    iput-boolean v4, v15, Lcom/google/android/gms/people/sync/a/k;->a:Z

    iget-boolean v4, v15, Lcom/google/android/gms/people/sync/a/k;->a:Z

    if-eqz v4, :cond_12

    const/4 v4, 0x1

    move-object/from16 v0, p2

    iput-boolean v4, v0, Lcom/google/android/gms/people/d/p;->e:Z

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v15, v1}, Lcom/google/android/gms/people/sync/a/a;->a(Lcom/google/android/gms/people/sync/a/k;Lcom/google/android/gms/people/sync/v;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v15, v1}, Lcom/google/android/gms/people/sync/a/a;->b(Lcom/google/android/gms/people/sync/a/k;Lcom/google/android/gms/people/sync/v;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const/4 v6, 0x3

    invoke-static {v6}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v6

    if-eqz v6, :cond_10

    const-string v6, "PeopleContactsSync"

    const-string v7, "  computeRawContactDiffs"

    invoke-static {v6, v7}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/people/sync/v;->c()V

    new-instance v6, Lcom/google/android/gms/people/sync/a/o;

    invoke-direct {v6}, Lcom/google/android/gms/people/sync/a/o;-><init>()V

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/gms/people/sync/a/a;->a(Lcom/google/android/gms/people/sync/a/o;)V

    invoke-virtual {v6}, Lcom/google/android/gms/people/sync/a/o;->size()I

    move-result v7

    move-object/from16 v0, p2

    iput v7, v0, Lcom/google/android/gms/people/d/p;->f:I

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/people/sync/v;->c()V

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/gms/people/sync/a/a;->b(Lcom/google/android/gms/people/sync/a/o;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/people/sync/v;->c()V

    invoke-virtual {v6, v15}, Lcom/google/android/gms/people/sync/a/o;->a(Lcom/google/android/gms/people/sync/a/k;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v4, v6, v4

    move-object/from16 v0, p2

    iput-wide v4, v0, Lcom/google/android/gms/people/d/p;->c:J

    iget-object v4, v15, Lcom/google/android/gms/people/sync/a/k;->c:Ljava/util/ArrayList;

    if-nez v4, :cond_11

    iget-object v4, v15, Lcom/google/android/gms/people/sync/a/k;->d:Ljava/util/ArrayList;

    if-eqz v4, :cond_16

    :cond_11
    const-string v4, "PeopleContactsSync"

    const-string v5, "  People added or updated.  Need to update avatars."

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x1

    iput-boolean v4, v15, Lcom/google/android/gms/people/sync/a/k;->g:Z

    :cond_12
    :goto_6
    const/4 v4, 0x3

    invoke-static {v4}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v4

    if-eqz v4, :cond_13

    const-string v4, "PeopleContactsSync"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "computePeopleSyncDiffs done: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Lcom/google/android/gms/people/sync/a/k;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    :cond_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    const-string v5, "PeopleContactsSync"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/people/sync/a/a;->H:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "CP2 sync: diff="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Lcom/google/android/gms/people/sync/a/k;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/people/sync/v;->c()V

    const-string v4, "PeopleContactsSync"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Syncing people to contacts: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Lcom/google/android/gms/people/sync/a/k;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/people/sync/a/a;->H:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-boolean v4, v15, Lcom/google/android/gms/people/sync/a/k;->a:Z

    if-nez v4, :cond_18

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/people/sync/v;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 492
    :cond_14
    :goto_7
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v12

    move-object/from16 v0, p2

    iput-wide v4, v0, Lcom/google/android/gms/people/d/p;->a:J

    .line 494
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    if-eqz v4, :cond_24

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    .line 496
    :goto_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    const-string v6, "PeopleContactsSync"

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/people/sync/a/a;->H:Ljava/lang/String;

    const-string v8, "===CP2 sync finished, success=%s, time=%d,%d,%d,%d,%d,%d rc=%d,%d,%d,%d av=%d,%d,%d,%d,%d,%d"

    const/16 v9, 0x11

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/google/android/gms/people/d/p;->a:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/google/android/gms/people/d/p;->c:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x3

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/google/android/gms/people/d/p;->d:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x4

    iget-wide v12, v4, Lcom/google/android/gms/people/d/q;->h:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x5

    iget-wide v12, v4, Lcom/google/android/gms/people/d/q;->a:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x6

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/google/android/gms/people/d/p;->b:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x7

    move-object/from16 v0, p2

    iget v11, v0, Lcom/google/android/gms/people/d/p;->f:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0x8

    move-object/from16 v0, p2

    iget v11, v0, Lcom/google/android/gms/people/d/p;->i:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0x9

    move-object/from16 v0, p2

    iget v11, v0, Lcom/google/android/gms/people/d/p;->g:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xa

    move-object/from16 v0, p2

    iget v11, v0, Lcom/google/android/gms/people/d/p;->h:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xb

    iget v11, v4, Lcom/google/android/gms/people/d/q;->e:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xc

    iget v11, v4, Lcom/google/android/gms/people/d/q;->f:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xd

    iget v11, v4, Lcom/google/android/gms/people/d/q;->g:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xe

    iget v11, v4, Lcom/google/android/gms/people/d/q;->b:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xf

    iget v11, v4, Lcom/google/android/gms/people/d/q;->c:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0x10

    iget v4, v4, Lcom/google/android/gms/people/d/q;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v6, v7, v4}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 477
    :cond_15
    const/4 v4, 0x0

    goto/16 :goto_5

    :cond_16
    :try_start_b
    invoke-direct/range {p0 .. p1}, Lcom/google/android/gms/people/sync/a/a;->a(Lcom/google/android/gms/people/sync/v;)Z

    move-result v4

    if-eqz v4, :cond_17

    const-string v5, "PeopleContactsSync"

    const-string v6, "  Finishing incomplete avatar sync."

    invoke-static {v5, v6}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_9
    iput-boolean v4, v15, Lcom/google/android/gms/people/sync/a/k;->g:Z

    goto/16 :goto_6

    :cond_17
    const-string v5, "PeopleContactsSync"

    const-string v6, "  No need for avatar sync."

    invoke-static {v5, v6}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    .line 482
    :cond_18
    invoke-virtual {v15}, Lcom/google/android/gms/people/sync/a/k;->a()Z

    move-result v4

    if-nez v4, :cond_14

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/people/ah;->o()Lcom/google/android/gms/people/f/i;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/people/f/i;->a()Z

    move-result v16

    iget-object v5, v15, Lcom/google/android/gms/people/sync/a/k;->f:Lcom/google/android/gms/people/sync/a/n;

    if-eqz v5, :cond_1b

    const/4 v4, 0x3

    invoke-static {v4}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v4

    if-eqz v4, :cond_19

    const-string v4, "PeopleContactsSync"

    const-string v6, "  updateMeProfile"

    invoke-static {v4, v6}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_19
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/people/sync/v;->c()V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-wide v8, v5, Lcom/google/android/gms/people/sync/a/n;->a:J

    const-wide/16 v10, 0x0

    cmp-long v4, v8, v10

    if-nez v4, :cond_22

    const/4 v4, 0x1

    :goto_a
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v6, v5, v4, v1}, Lcom/google/android/gms/people/sync/a/a;->a(Ljava/util/ArrayList;Lcom/google/android/gms/people/sync/a/n;ZZ)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    const/4 v7, 0x1

    invoke-static {v4, v6, v7}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    move-result-object v4

    iget-wide v6, v5, Lcom/google/android/gms/people/sync/a/n;->a:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_1a

    if-eqz v4, :cond_1a

    array-length v6, v4

    if-lez v6, :cond_1a

    const/4 v6, 0x0

    aget-object v4, v4, v6

    iget-object v4, v4, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    iput-wide v6, v5, Lcom/google/android/gms/people/sync/a/n;->a:J

    :cond_1a
    iget-boolean v4, v5, Lcom/google/android/gms/people/sync/a/n;->j:Z

    if-eqz v4, :cond_1b

    iget-wide v6, v5, Lcom/google/android/gms/people/sync/a/n;->a:J

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-eqz v4, :cond_1b

    iget-wide v6, v5, Lcom/google/android/gms/people/sync/a/n;->a:J

    iget-object v8, v5, Lcom/google/android/gms/people/sync/a/n;->h:Ljava/lang/String;

    iget-object v4, v5, Lcom/google/android/gms/people/sync/a/n;->h:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/people/sync/a/a;->a(Ljava/lang/String;)I

    move-result v9

    const/4 v10, 0x1

    move-object/from16 v5, p0

    move-object/from16 v11, p1

    invoke-direct/range {v5 .. v11}, Lcom/google/android/gms/people/sync/a/a;->a(JLjava/lang/String;IZLcom/google/android/gms/people/sync/v;)Lcom/google/android/gms/people/sync/a/h;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/people/sync/v;->c()V

    :cond_1b
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/people/sync/v;->c()V

    const/4 v4, 0x3

    invoke-static {v4}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v4

    if-eqz v4, :cond_1c

    const-string v4, "PeopleContactsSync"

    const-string v5, "  syncPeopleToContacts"

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1c
    iget-object v4, v15, Lcom/google/android/gms/people/sync/a/k;->b:Ljava/util/ArrayList;

    if-eqz v4, :cond_1d

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/google/android/gms/people/sync/a/a;->a(Ljava/util/List;Lcom/google/android/gms/people/sync/v;)V

    :cond_1d
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, v15, Lcom/google/android/gms/people/sync/a/k;->e:Ljava/util/ArrayList;

    if-eqz v4, :cond_1e

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    move-object/from16 v0, p2

    iput v6, v0, Lcom/google/android/gms/people/d/p;->h:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v5, v4, v1}, Lcom/google/android/gms/people/sync/a/a;->a(Ljava/util/ArrayList;Ljava/util/List;Lcom/google/android/gms/people/sync/v;)V

    :cond_1e
    iget-object v6, v15, Lcom/google/android/gms/people/sync/a/k;->c:Ljava/util/ArrayList;

    if-eqz v6, :cond_1f

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    move-object/from16 v0, p2

    iput v4, v0, Lcom/google/android/gms/people/d/p;->i:I

    const/4 v7, 0x1

    move-object/from16 v4, p0

    move/from16 v8, v16

    move-object/from16 v9, p1

    invoke-direct/range {v4 .. v9}, Lcom/google/android/gms/people/sync/a/a;->a(Ljava/util/ArrayList;Ljava/util/List;ZZLcom/google/android/gms/people/sync/v;)V

    :cond_1f
    iget-object v6, v15, Lcom/google/android/gms/people/sync/a/k;->d:Ljava/util/ArrayList;

    if-eqz v6, :cond_20

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    move-object/from16 v0, p2

    iput v4, v0, Lcom/google/android/gms/people/d/p;->g:I

    const/4 v7, 0x0

    move-object/from16 v4, p0

    move/from16 v8, v16

    move-object/from16 v9, p1

    invoke-direct/range {v4 .. v9}, Lcom/google/android/gms/people/sync/a/a;->a(Ljava/util/ArrayList;Ljava/util/List;ZZLcom/google/android/gms/people/sync/v;)V

    :cond_20
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/people/sync/a/a;->a(Landroid/content/Context;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    iget-boolean v4, v15, Lcom/google/android/gms/people/sync/a/k;->g:Z

    if-eqz v4, :cond_21

    const-string v4, "PeopleContactsSync"

    const-string v5, "  syncAvatars"

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "PeopleContactsSync"

    const-string v5, "    Syncing thumbnails..."

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/people/sync/a/a;->c()Lcom/google/android/gms/people/sync/a/f;

    move-result-object v4

    if-eqz v4, :cond_21

    invoke-virtual {v4}, Lcom/google/android/gms/people/sync/a/f;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_23

    :cond_21
    :goto_b
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v10

    move-object/from16 v0, p2

    iput-wide v4, v0, Lcom/google/android/gms/people/d/p;->d:J

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/people/sync/v;->c()V

    goto/16 :goto_7

    :cond_22
    const/4 v4, 0x0

    goto/16 :goto_a

    :cond_23
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v4}, Lcom/google/android/gms/people/sync/a/f;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v4, Lcom/google/android/gms/people/d/q;

    invoke-direct {v4}, Lcom/google/android/gms/people/d/q;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v5, v4, v2}, Lcom/google/android/gms/people/sync/a/a;->a(Lcom/google/android/gms/people/sync/v;Ljava/util/List;Lcom/google/android/gms/people/d/q;Lcom/google/android/gms/people/d/p;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/people/sync/v;->c()V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5, v4}, Lcom/google/android/gms/people/sync/a/a;->a(Lcom/google/android/gms/people/sync/v;Ljava/util/List;Lcom/google/android/gms/people/d/q;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/people/sync/v;->c()V

    move-object/from16 v0, p2

    iput-object v4, v0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_b

    .line 494
    :cond_24
    new-instance v4, Lcom/google/android/gms/people/d/q;

    invoke-direct {v4}, Lcom/google/android/gms/people/d/q;-><init>()V

    goto/16 :goto_8

    .line 492
    :cond_25
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v12

    move-object/from16 v0, p2

    iput-wide v4, v0, Lcom/google/android/gms/people/d/p;->a:J

    .line 494
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    if-eqz v4, :cond_26

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    .line 496
    :goto_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    const-string v6, "PeopleContactsSync"

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/people/sync/a/a;->H:Ljava/lang/String;

    const-string v8, "===CP2 sync finished, success=%s, time=%d,%d,%d,%d,%d,%d rc=%d,%d,%d,%d av=%d,%d,%d,%d,%d,%d"

    const/16 v9, 0x11

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/google/android/gms/people/d/p;->a:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/google/android/gms/people/d/p;->c:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x3

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/google/android/gms/people/d/p;->d:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x4

    iget-wide v12, v4, Lcom/google/android/gms/people/d/q;->h:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x5

    iget-wide v12, v4, Lcom/google/android/gms/people/d/q;->a:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x6

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/google/android/gms/people/d/p;->b:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x7

    move-object/from16 v0, p2

    iget v11, v0, Lcom/google/android/gms/people/d/p;->f:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0x8

    move-object/from16 v0, p2

    iget v11, v0, Lcom/google/android/gms/people/d/p;->i:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0x9

    move-object/from16 v0, p2

    iget v11, v0, Lcom/google/android/gms/people/d/p;->g:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xa

    move-object/from16 v0, p2

    iget v11, v0, Lcom/google/android/gms/people/d/p;->h:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xb

    iget v11, v4, Lcom/google/android/gms/people/d/q;->e:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xc

    iget v11, v4, Lcom/google/android/gms/people/d/q;->f:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xd

    iget v11, v4, Lcom/google/android/gms/people/d/q;->g:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xe

    iget v11, v4, Lcom/google/android/gms/people/d/q;->b:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xf

    iget v11, v4, Lcom/google/android/gms/people/d/q;->c:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0x10

    iget v4, v4, Lcom/google/android/gms/people/d/q;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v6, v7, v4}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 494
    :cond_26
    new-instance v4, Lcom/google/android/gms/people/d/q;

    invoke-direct {v4}, Lcom/google/android/gms/people/d/q;-><init>()V

    goto/16 :goto_c

    :cond_27
    new-instance v4, Lcom/google/android/gms/people/d/q;

    invoke-direct {v4}, Lcom/google/android/gms/people/d/q;-><init>()V

    goto/16 :goto_1
.end method

.method public final a(Z[Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 395
    invoke-static {}, Lcom/google/android/gms/people/sync/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 441
    :cond_0
    :goto_0
    return-void

    .line 397
    :cond_1
    invoke-static {}, Lcom/google/android/gms/people/sync/a/a;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 398
    const-string v0, "PeopleContactsSync"

    const-string v1, "CP2 sync not supported."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 401
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    const-string v4, "PeopleContactsSync"

    iget-object v5, p0, Lcom/google/android/gms/people/sync/a/a;->H:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "Changing contact sync settings enabled:"

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " target:"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez p2, :cond_3

    const-string v0, "null"

    :goto_1
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v4, v5, v0}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v5

    .line 406
    invoke-virtual {v5}, Lcom/google/android/gms/people/c/e;->a()V

    .line 413
    :try_start_0
    array-length v6, p2

    move v4, v3

    move v0, v3

    move v1, v3

    :goto_2
    if-ge v4, v6, :cond_6

    aget-object v7, p2, v4

    .line 414
    const-string v8, "$$mycircles$$"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    if-eqz v8, :cond_4

    move v1, p1

    .line 413
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 401
    :cond_3
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 416
    :cond_4
    :try_start_1
    const-string v8, "$$everrgreen$$"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    move v0, p1

    .line 417
    goto :goto_3

    .line 419
    :cond_5
    const-string v8, "PeopleContactsSync"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Contacts sync settings.  Ignoring invalid target="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 433
    :catchall_0
    move-exception v0

    invoke-virtual {v5, v3}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v0

    .line 424
    :cond_6
    :try_start_2
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 425
    const-string v7, "sync_to_contacts"

    if-eqz p1, :cond_7

    move v4, v2

    :goto_4
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 426
    const-string v4, "sync_circles_to_contacts"

    if-eqz v1, :cond_8

    move v1, v2

    :goto_5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v6, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 427
    const-string v1, "sync_evergreen_to_contacts"

    if-eqz v0, :cond_9

    move v0, v2

    :goto_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 429
    const-string v0, "owners"

    const-string v1, "_id=?"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/a/a;->J:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v0, v6, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 431
    invoke-virtual {v5}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 433
    invoke-virtual {v5, v3}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 437
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/a/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 438
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/a;->E:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/a;->H:Ljava/lang/String;

    const-string v2, "CP2 sync settings changed"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/sync/u;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    move v4, v3

    .line 425
    goto :goto_4

    :cond_8
    move v1, v3

    .line 426
    goto :goto_5

    :cond_9
    move v0, v3

    .line 427
    goto :goto_6
.end method
