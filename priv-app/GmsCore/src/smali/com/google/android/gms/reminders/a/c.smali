.class final Lcom/google/android/gms/reminders/a/c;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/reminders/a/b;

.field private final b:Landroid/content/Context;

.field private final c:[Landroid/accounts/Account;


# direct methods
.method constructor <init>(Lcom/google/android/gms/reminders/a/b;Landroid/content/Context;[Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/gms/reminders/a/c;->a:Lcom/google/android/gms/reminders/a/b;

    .line 43
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 44
    iput-object p2, p0, Lcom/google/android/gms/reminders/a/c;->b:Landroid/content/Context;

    .line 45
    iput-object p3, p0, Lcom/google/android/gms/reminders/a/c;->c:[Landroid/accounts/Account;

    .line 46
    return-void
.end method

.method private static a([Landroid/accounts/Account;Lcom/google/android/gms/reminders/a/d;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 97
    iget-object v1, p1, Lcom/google/android/gms/reminders/a/d;->b:Landroid/accounts/Account;

    iget-object v2, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 98
    array-length v3, p0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, p0, v1

    .line 99
    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 103
    :goto_1
    return v0

    .line 98
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 103
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v1, 0x0

    .line 37
    iget-object v0, p0, Lcom/google/android/gms/reminders/a/c;->b:Landroid/content/Context;

    invoke-static {v0, v9, v9}, Lcom/google/android/gms/reminders/a/a;->a(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/reminders/a/c;->c:[Landroid/accounts/Account;

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_4

    aget-object v6, v4, v2

    invoke-static {v6}, Lcom/google/android/gms/reminders/a/a;->a(Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "account_name"

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/gms/reminders/a/c;->b:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/google/android/gms/reminders/internal/a/g;->a:Landroid/net/Uri;

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    iget-object v7, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/reminders/a/d;

    iget-object v0, v0, Lcom/google/android/gms/reminders/a/d;->b:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_1

    :cond_3
    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/reminders/a/d;

    iget-object v2, p0, Lcom/google/android/gms/reminders/a/c;->c:[Landroid/accounts/Account;

    invoke-static {v2, v0}, Lcom/google/android/gms/reminders/a/c;->a([Landroid/accounts/Account;Lcom/google/android/gms/reminders/a/d;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/reminders/a/c;->b:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/google/android/gms/reminders/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/reminders/a/d;)V

    goto :goto_2

    :cond_6
    return-object v9
.end method
