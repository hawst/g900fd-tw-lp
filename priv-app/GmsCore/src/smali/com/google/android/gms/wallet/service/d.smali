.class public final Lcom/google/android/gms/wallet/service/d;
.super Lcom/google/android/gms/wallet/service/b;
.source "SourceFile"


# instance fields
.field private final j:Lcom/google/android/gms/wallet/service/a/b;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/service/b;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;)V

    .line 68
    new-instance v0, Lcom/google/android/gms/wallet/service/a/b;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/wallet/service/a/a;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p3, v3, p4}, Lcom/google/android/gms/wallet/service/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/service/a/b;-><init>(Ljava/lang/String;Lcom/google/android/gms/wallet/service/a/a;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/d;->j:Lcom/google/android/gms/wallet/service/a/b;

    .line 70
    return-void
.end method

.method private static a(Lcom/google/t/a/b;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/a/b;
    .locals 2

    .prologue
    .line 437
    new-instance v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/b;-><init>()V

    .line 438
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    .line 439
    iput-object p0, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    .line 440
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 441
    iput-object p1, v0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    .line 443
    :cond_0
    return-object v0
.end method

.method private static a(Lcom/google/checkout/a/a/a/b;)Lcom/google/checkout/inapp/proto/j;
    .locals 4

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    iget-object v0, v0, Lcom/google/checkout/a/a/a/c;->a:Ljava/lang/String;

    .line 418
    new-instance v1, Lcom/google/checkout/inapp/proto/j;

    invoke-direct {v1}, Lcom/google/checkout/inapp/proto/j;-><init>()V

    .line 419
    iget-object v2, p0, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    iget-object v3, p0, Lcom/google/checkout/a/a/a/b;->g:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/wallet/service/d;->a(Lcom/google/t/a/b;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v2

    iput-object v2, v1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    .line 421
    iget v2, p0, Lcom/google/checkout/a/a/a/b;->b:I

    iput v2, v1, Lcom/google/checkout/inapp/proto/j;->j:I

    .line 422
    iget v2, p0, Lcom/google/checkout/a/a/a/b;->c:I

    iput v2, v1, Lcom/google/checkout/inapp/proto/j;->k:I

    .line 423
    iget v2, p0, Lcom/google/checkout/a/a/a/b;->f:I

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/z;->b(I)I

    move-result v2

    iput v2, v1, Lcom/google/checkout/inapp/proto/j;->c:I

    .line 425
    const/4 v2, 0x0

    iput v2, v1, Lcom/google/checkout/inapp/proto/j;->d:I

    .line 426
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x4

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    .line 427
    const/4 v0, 0x1

    iput v0, v1, Lcom/google/checkout/inapp/proto/j;->h:I

    .line 431
    invoke-static {v1}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/j;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    .line 432
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method public final a(Lcom/google/aa/b/a/a/a/a/g;)V
    .locals 2

    .prologue
    .line 378
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Create wallet objects requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/aa/b/a/a/a/a/i;Z)V
    .locals 2

    .prologue
    .line 321
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 323
    const/16 v1, 0xc

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/service/d;->b(ILjava/lang/Object;)V

    .line 324
    return-void
.end method

.method public final a(Lcom/google/aa/b/a/a/a/a/k;Lcom/google/android/gms/wallet/Cart;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 395
    const/16 v0, 0x8

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/service/d;->b(ILjava/lang/Object;)V

    .line 397
    return-void
.end method

.method public final a(Lcom/google/aa/b/a/a/a/a/o;Z)V
    .locals 1

    .prologue
    .line 304
    const/4 v0, 0x7

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/wallet/service/d;->b(ILjava/lang/Object;)V

    .line 305
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/aa;Z)V
    .locals 2

    .prologue
    .line 275
    const/4 v0, 0x3

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/service/d;->b(ILjava/lang/Object;)V

    .line 277
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/ab;)V
    .locals 2

    .prologue
    .line 340
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Create profile requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/checkout/inapp/proto/ac;)V
    .locals 2

    .prologue
    .line 352
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Enroll with broker requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/checkout/inapp/proto/af;)V
    .locals 2

    .prologue
    .line 346
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Get legal documents requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/checkout/inapp/proto/ah;)V
    .locals 2

    .prologue
    .line 384
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Get profile requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/checkout/inapp/proto/ak;)V
    .locals 2

    .prologue
    .line 328
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Purchase options requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/checkout/inapp/proto/am;)V
    .locals 2

    .prologue
    .line 334
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Purchase requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/checkout/inapp/proto/ao;Z)V
    .locals 2

    .prologue
    .line 296
    const/4 v0, 0x6

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/service/d;->b(ILjava/lang/Object;)V

    .line 298
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/ap;Z)V
    .locals 2

    .prologue
    .line 282
    const/4 v0, 0x4

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/service/d;->b(ILjava/lang/Object;)V

    .line 284
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/s;)V
    .locals 2

    .prologue
    .line 359
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Billing get payment options requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/checkout/inapp/proto/u;)V
    .locals 2

    .prologue
    .line 372
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Billing make payment requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/checkout/inapp/proto/w;)V
    .locals 2

    .prologue
    .line 366
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Billing update payment settings requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/checkout/inapp/proto/z;Z)V
    .locals 2

    .prologue
    .line 289
    const/4 v0, 0x5

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/service/d;->b(ILjava/lang/Object;)V

    .line 291
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 315
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "BIN requests not supported for local payments"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/aa/b/a/a/a/a/c;)V
    .locals 1

    .prologue
    .line 310
    const/16 v0, 0xa

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/wallet/service/d;->b(ILjava/lang/Object;)V

    .line 311
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 92
    return-void
.end method

.method protected final c(ILjava/lang/Object;)Landroid/util/Pair;
    .locals 10

    .prologue
    const/16 v9, 0x10

    const/16 v5, 0xc

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 98
    const/4 v0, 0x0

    .line 99
    packed-switch p1, :pswitch_data_0

    .line 266
    :goto_0
    :pswitch_0
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 102
    :pswitch_1
    check-cast p2, Landroid/util/Pair;

    .line 104
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/checkout/inapp/proto/aa;

    .line 105
    iget-object v1, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 107
    iget-object v0, v0, Lcom/google/checkout/inapp/proto/aa;->b:Lcom/google/checkout/a/a/a/d;

    iget-object v0, v0, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    .line 108
    invoke-static {v0}, Lcom/google/android/gms/wallet/service/d;->a(Lcom/google/checkout/a/a/a/b;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v2

    .line 111
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/d;->j:Lcom/google/android/gms/wallet/service/a/b;

    iget-object v0, v0, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    invoke-virtual {v3, v2, v0, v1}, Lcom/google/android/gms/wallet/service/a/b;->a(Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/a/a/a/c;Z)Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    .line 114
    new-instance v1, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;

    invoke-direct {v1}, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;-><init>()V

    .line 115
    iput-object v0, v1, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lcom/google/checkout/inapp/proto/j;

    .line 116
    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/4 v2, 0x7

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V

    goto :goto_0

    .line 121
    :pswitch_2
    check-cast p2, Landroid/util/Pair;

    .line 123
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/checkout/inapp/proto/ap;

    .line 124
    iget-object v1, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 126
    iget-object v3, v0, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    iget-object v3, v3, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    .line 127
    iget-object v0, v0, Lcom/google/checkout/inapp/proto/ap;->b:Ljava/lang/String;

    .line 130
    iget-object v4, p0, Lcom/google/android/gms/wallet/service/d;->j:Lcom/google/android/gms/wallet/service/a/b;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/wallet/service/a/b;->b(Ljava/lang/String;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v4

    .line 131
    iget-object v5, v3, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    iget-object v6, v3, Lcom/google/checkout/a/a/a/b;->g:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/android/gms/wallet/service/d;->a(Lcom/google/t/a/b;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v5

    iput-object v5, v4, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    .line 133
    iget v5, v3, Lcom/google/checkout/a/a/a/b;->b:I

    iput v5, v4, Lcom/google/checkout/inapp/proto/j;->j:I

    .line 134
    iget v3, v3, Lcom/google/checkout/a/a/a/b;->c:I

    iput v3, v4, Lcom/google/checkout/inapp/proto/j;->k:I

    .line 135
    iput v2, v4, Lcom/google/checkout/inapp/proto/j;->h:I

    .line 136
    sget-object v2, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v2, v4, Lcom/google/checkout/inapp/proto/j;->g:[I

    .line 139
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/d;->j:Lcom/google/android/gms/wallet/service/a/b;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/wallet/service/a/b;->a(Ljava/lang/String;)Lcom/google/checkout/a/a/a/c;

    move-result-object v0

    .line 141
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/d;->j:Lcom/google/android/gms/wallet/service/a/b;

    invoke-virtual {v2, v4, v0, v1}, Lcom/google/android/gms/wallet/service/a/b;->b(Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/a/a/a/c;Z)V

    .line 142
    new-instance v1, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;

    invoke-direct {v1}, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;-><init>()V

    .line 143
    iput-object v4, v1, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->a:Lcom/google/checkout/inapp/proto/j;

    .line 144
    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v2, 0x9

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    .line 149
    :pswitch_3
    check-cast p2, Landroid/util/Pair;

    .line 151
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/checkout/inapp/proto/z;

    .line 152
    iget-object v1, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 154
    iget-object v2, v0, Lcom/google/checkout/inapp/proto/z;->b:Lcom/google/t/a/b;

    iget-object v3, v0, Lcom/google/checkout/inapp/proto/z;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/wallet/service/d;->a(Lcom/google/t/a/b;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v2

    .line 156
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/d;->f:Landroid/content/Context;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/z;->b:Lcom/google/t/a/b;

    invoke-static {v3, v0}, Lcom/google/android/gms/wallet/service/a/a;->a(Landroid/content/Context;Lcom/google/t/a/b;)Z

    move-result v0

    iput-boolean v0, v2, Lcom/google/checkout/inapp/proto/a/b;->f:Z

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/d;->j:Lcom/google/android/gms/wallet/service/a/b;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/wallet/service/a/b;->a(Lcom/google/checkout/inapp/proto/a/b;Z)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v0

    .line 160
    new-instance v1, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;

    invoke-direct {v1}, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;-><init>()V

    .line 161
    iput-object v0, v1, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    .line 162
    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v2, 0xb

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    .line 167
    :pswitch_4
    check-cast p2, Landroid/util/Pair;

    .line 169
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/checkout/inapp/proto/ao;

    .line 170
    iget-object v1, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 172
    iget-object v2, v0, Lcom/google/checkout/inapp/proto/ao;->c:Lcom/google/t/a/b;

    iget-object v3, v0, Lcom/google/checkout/inapp/proto/ao;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/wallet/service/d;->a(Lcom/google/t/a/b;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v2

    .line 174
    iget-object v3, v0, Lcom/google/checkout/inapp/proto/ao;->b:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    .line 175
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/d;->f:Landroid/content/Context;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/ao;->c:Lcom/google/t/a/b;

    invoke-static {v3, v0}, Lcom/google/android/gms/wallet/service/a/a;->a(Landroid/content/Context;Lcom/google/t/a/b;)Z

    move-result v0

    iput-boolean v0, v2, Lcom/google/checkout/inapp/proto/a/b;->f:Z

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/d;->j:Lcom/google/android/gms/wallet/service/a/b;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/wallet/service/a/b;->b(Lcom/google/checkout/inapp/proto/a/b;Z)V

    .line 178
    new-instance v1, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;

    invoke-direct {v1}, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;-><init>()V

    .line 179
    iput-object v2, v1, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    .line 180
    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    invoke-direct {v0, v5, v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    .line 185
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/d;->j:Lcom/google/android/gms/wallet/service/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/a/b;->b()Ljava/util/ArrayList;

    move-result-object v0

    .line 186
    new-instance v1, Lcom/google/aa/a/a/a/f;

    invoke-direct {v1}, Lcom/google/aa/a/a/a/f;-><init>()V

    .line 187
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/checkout/inapp/proto/j;

    iput-object v2, v1, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    .line 188
    iget-object v2, v1, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/d;->j:Lcom/google/android/gms/wallet/service/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/a/b;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 191
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/checkout/inapp/proto/a/b;

    iput-object v2, v1, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    .line 192
    iget-object v2, v1, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 194
    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v2, 0x13

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    .line 199
    :pswitch_6
    check-cast p2, Landroid/util/Pair;

    .line 201
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/aa/b/a/a/a/a/i;

    .line 202
    iget-object v1, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 203
    iget-object v2, v0, Lcom/google/aa/b/a/a/a/a/i;->b:Ljava/lang/String;

    .line 204
    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/i;->c:Ljava/lang/String;

    .line 205
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/d;->j:Lcom/google/android/gms/wallet/service/a/b;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/wallet/service/a/b;->a(Ljava/lang/String;)Lcom/google/checkout/a/a/a/c;

    move-result-object v3

    .line 207
    iget-object v4, p0, Lcom/google/android/gms/wallet/service/d;->j:Lcom/google/android/gms/wallet/service/a/b;

    invoke-virtual {v4, v2}, Lcom/google/android/gms/wallet/service/a/b;->b(Ljava/lang/String;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v2

    .line 209
    new-instance v4, Lcom/google/aa/b/a/a/a/a/j;

    invoke-direct {v4}, Lcom/google/aa/b/a/a/a/a/j;-><init>()V

    .line 210
    iget-object v3, v3, Lcom/google/checkout/a/a/a/c;->a:Ljava/lang/String;

    iput-object v3, v4, Lcom/google/aa/b/a/a/a/a/j;->e:Ljava/lang/String;

    .line 211
    iget v3, v2, Lcom/google/checkout/inapp/proto/j;->j:I

    iput v3, v4, Lcom/google/aa/b/a/a/a/a/j;->c:I

    .line 212
    iget v3, v2, Lcom/google/checkout/inapp/proto/j;->k:I

    iput v3, v4, Lcom/google/aa/b/a/a/a/a/j;->d:I

    .line 213
    iget-object v2, v2, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iput-object v2, v4, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    .line 214
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 215
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/d;->j:Lcom/google/android/gms/wallet/service/a/b;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/wallet/service/a/b;->c(Ljava/lang/String;)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v0

    .line 216
    iput-object v0, v4, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    .line 219
    :cond_0
    if-eqz v1, :cond_1

    .line 220
    iget-object v0, v4, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    invoke-static {v0, v5}, Lcom/google/android/gms/common/util/h;->b([II)[I

    move-result-object v0

    iput-object v0, v4, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    .line 224
    :cond_1
    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    invoke-direct {v0, v9, v4}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    .line 229
    :pswitch_7
    new-instance v1, Lcom/google/aa/b/a/a/a/a/d;

    invoke-direct {v1}, Lcom/google/aa/b/a/a/a/a/d;-><init>()V

    .line 231
    iput v2, v1, Lcom/google/aa/b/a/a/a/a/d;->a:I

    .line 232
    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v2, 0x17

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    .line 237
    :pswitch_8
    check-cast p2, Landroid/util/Pair;

    .line 239
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/aa/b/a/a/a/a/k;

    .line 240
    iget-object v1, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 241
    iget-object v1, v0, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    iget-object v1, v1, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    .line 242
    iget-object v5, v1, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    .line 244
    invoke-static {v1}, Lcom/google/android/gms/wallet/service/d;->a(Lcom/google/checkout/a/a/a/b;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v1

    .line 245
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/d;->j:Lcom/google/android/gms/wallet/service/a/b;

    invoke-virtual {v6, v1, v5, v4}, Lcom/google/android/gms/wallet/service/a/b;->a(Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/a/a/a/c;Z)Lcom/google/checkout/inapp/proto/j;

    .line 246
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/d;->j:Lcom/google/android/gms/wallet/service/a/b;

    iget-object v7, v1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v6, v7, v4}, Lcom/google/android/gms/wallet/service/a/b;->a(Lcom/google/checkout/inapp/proto/a/b;Z)Lcom/google/checkout/inapp/proto/a/b;

    .line 248
    new-instance v6, Lcom/google/aa/b/a/a/a/a/j;

    invoke-direct {v6}, Lcom/google/aa/b/a/a/a/a/j;-><init>()V

    .line 249
    iget-object v7, v5, Lcom/google/checkout/a/a/a/c;->a:Ljava/lang/String;

    iput-object v7, v6, Lcom/google/aa/b/a/a/a/a/j;->e:Ljava/lang/String;

    .line 250
    iget v7, v1, Lcom/google/checkout/inapp/proto/j;->j:I

    iput v7, v6, Lcom/google/aa/b/a/a/a/a/j;->c:I

    .line 251
    iget v7, v1, Lcom/google/checkout/inapp/proto/j;->k:I

    iput v7, v6, Lcom/google/aa/b/a/a/a/a/j;->d:I

    .line 252
    iget-object v7, v1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iput-object v7, v6, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    .line 253
    iget-object v5, v5, Lcom/google/checkout/a/a/a/c;->b:Ljava/lang/String;

    iput-object v5, v6, Lcom/google/aa/b/a/a/a/a/j;->f:Ljava/lang/String;

    .line 255
    iget-object v5, v0, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v5, :cond_3

    .line 256
    iget-object v1, v1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v5, v0, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-ne v1, v5, :cond_4

    move v1, v2

    :goto_1
    if-nez v1, :cond_2

    .line 258
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/d;->j:Lcom/google/android/gms/wallet/service/a/b;

    iget-object v3, v0, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/wallet/service/a/b;->a(Lcom/google/checkout/inapp/proto/a/b;Z)Lcom/google/checkout/inapp/proto/a/b;

    .line 260
    :cond_2
    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    iput-object v0, v6, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    .line 261
    iget-object v0, v6, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    iput-boolean v2, v0, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    .line 264
    :cond_3
    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    invoke-direct {v0, v9, v6}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    .line 256
    :cond_4
    if-eqz v1, :cond_5

    if-nez v5, :cond_6

    :cond_5
    move v1, v3

    goto :goto_1

    :cond_6
    iget-object v7, v1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v8, v5, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    invoke-static {v7, v8}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)Z

    move-result v7

    if-eqz v7, :cond_7

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    iget-object v5, v5, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-static {v1, v5}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    move v1, v2

    goto :goto_1

    :cond_7
    move v1, v3

    goto :goto_1

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method
