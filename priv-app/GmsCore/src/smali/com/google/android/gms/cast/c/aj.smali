.class final Lcom/google/android/gms/cast/c/aj;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/cast/c/aq;

.field final synthetic b:Lcom/google/android/gms/cast/c/af;

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/c/af;Lcom/google/android/gms/cast/c/aq;)V
    .locals 0

    .prologue
    .line 440
    iput-object p1, p0, Lcom/google/android/gms/cast/c/aj;->b:Lcom/google/android/gms/cast/c/af;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 441
    iput-object p2, p0, Lcom/google/android/gms/cast/c/aj;->a:Lcom/google/android/gms/cast/c/aq;

    .line 442
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 449
    iget-object v2, p0, Lcom/google/android/gms/cast/c/aj;->a:Lcom/google/android/gms/cast/c/aq;

    iget-object v2, v2, Lcom/google/android/gms/cast/c/aq;->a:Lcom/google/android/gms/cast/c/ao;

    iget-object v2, v2, Lcom/google/android/gms/cast/c/ao;->a:[Ljava/lang/String;

    .line 451
    iget-object v3, p0, Lcom/google/android/gms/cast/c/aj;->a:Lcom/google/android/gms/cast/c/aq;

    invoke-virtual {v3}, Lcom/google/android/gms/cast/c/aq;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/cast/c/aj;->a:Lcom/google/android/gms/cast/c/aq;

    invoke-virtual {v3}, Lcom/google/android/gms/cast/c/aq;->b()Z

    move-result v3

    if-nez v3, :cond_3

    .line 452
    :cond_0
    iget-boolean v3, p0, Lcom/google/android/gms/cast/c/aj;->c:Z

    if-nez v3, :cond_2

    .line 454
    iget-object v3, p0, Lcom/google/android/gms/cast/c/aj;->b:Lcom/google/android/gms/cast/c/af;

    iget-object v3, v3, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v4, "doing resolve query for SRV/TXT for %s"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v2}, Lcom/google/android/gms/cast/c/ap;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 456
    iget-object v0, p0, Lcom/google/android/gms/cast/c/aj;->b:Lcom/google/android/gms/cast/c/af;

    invoke-static {}, Lcom/google/android/gms/cast/c/af;->c()[I

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/cast/c/af;->a(Lcom/google/android/gms/cast/c/af;[Ljava/lang/String;[I)V

    .line 457
    iput-boolean v1, p0, Lcom/google/android/gms/cast/c/aj;->c:Z

    :cond_1
    :goto_0
    move v0, v1

    .line 477
    :cond_2
    return v0

    .line 463
    :cond_3
    iget-object v3, p0, Lcom/google/android/gms/cast/c/aj;->a:Lcom/google/android/gms/cast/c/aq;

    invoke-virtual {v3}, Lcom/google/android/gms/cast/c/aq;->d()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/cast/c/aj;->a:Lcom/google/android/gms/cast/c/aq;

    invoke-virtual {v3}, Lcom/google/android/gms/cast/c/aq;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 464
    iget-boolean v3, p0, Lcom/google/android/gms/cast/c/aj;->d:Z

    if-nez v3, :cond_2

    .line 466
    iget-object v3, p0, Lcom/google/android/gms/cast/c/aj;->b:Lcom/google/android/gms/cast/c/af;

    iget-object v3, v3, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v4, "doing resolve query for A/AAAA for %s"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v2}, Lcom/google/android/gms/cast/c/ap;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 468
    iget-object v0, p0, Lcom/google/android/gms/cast/c/aj;->b:Lcom/google/android/gms/cast/c/af;

    iget-object v2, p0, Lcom/google/android/gms/cast/c/aj;->a:Lcom/google/android/gms/cast/c/aq;

    iget-object v2, v2, Lcom/google/android/gms/cast/c/aq;->b:Lcom/google/android/gms/cast/c/at;

    iget-object v2, v2, Lcom/google/android/gms/cast/c/at;->d:[Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/cast/c/af;->d()[I

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/cast/c/af;->a(Lcom/google/android/gms/cast/c/af;[Ljava/lang/String;[I)V

    .line 469
    iput-boolean v1, p0, Lcom/google/android/gms/cast/c/aj;->d:Z

    goto :goto_0
.end method
