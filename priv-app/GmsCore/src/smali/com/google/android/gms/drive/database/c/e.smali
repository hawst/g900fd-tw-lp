.class public final Lcom/google/android/gms/drive/database/c/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Lcom/google/android/gms/drive/database/c/d;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/c/e;->a:Z

    .line 8
    iput-object v1, p0, Lcom/google/android/gms/drive/database/c/e;->c:Ljava/lang/String;

    .line 9
    iput-object v1, p0, Lcom/google/android/gms/drive/database/c/e;->d:Ljava/lang/String;

    .line 10
    iput-object v1, p0, Lcom/google/android/gms/drive/database/c/e;->b:Lcom/google/android/gms/drive/database/c/d;

    .line 13
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/e;
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/gms/drive/database/c/e;

    invoke-direct {v0}, Lcom/google/android/gms/drive/database/c/e;-><init>()V

    iput-object p0, v0, Lcom/google/android/gms/drive/database/c/e;->c:Ljava/lang/String;

    .line 20
    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 57
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "CREATE VIEW "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/c/e;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "IF NOT EXISTS \'"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/drive/database/c/e;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/drive/database/c/e;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'.\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/c/e;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/c/e;->b:Lcom/google/android/gms/drive/database/c/d;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 57
    :cond_0
    const-string v0, "\'"

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1
.end method
