.class final Lcom/google/android/gms/droidguard/d/w;
.super Lorg/apache/http/entity/AbstractHttpEntity;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/droidguard/d;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/droidguard/d;)V
    .locals 1

    .prologue
    .line 188
    invoke-direct {p0}, Lorg/apache/http/entity/AbstractHttpEntity;-><init>()V

    .line 189
    iput-object p1, p0, Lcom/google/android/gms/droidguard/d/w;->a:Lcom/google/android/gms/droidguard/d;

    .line 190
    const-string v0, "application/x-protobuf"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/droidguard/d/w;->setContentType(Ljava/lang/String;)V

    .line 191
    return-void
.end method


# virtual methods
.method public final getContent()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 208
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getContent() not supported."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getContentLength()J
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/w;->a:Lcom/google/android/gms/droidguard/d;

    invoke-virtual {v0}, Lcom/google/android/gms/droidguard/d;->b()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final isRepeatable()Z
    .locals 1

    .prologue
    .line 218
    const/4 v0, 0x1

    return v0
.end method

.method public final isStreaming()Z
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x0

    return v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 195
    invoke-static {p1}, Lcom/google/protobuf/a/c;->a(Ljava/io/OutputStream;)Lcom/google/protobuf/a/c;

    move-result-object v0

    .line 196
    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/w;->a:Lcom/google/android/gms/droidguard/d;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/droidguard/d;->a(Lcom/google/protobuf/a/c;)V

    .line 197
    invoke-virtual {v0}, Lcom/google/protobuf/a/c;->a()V

    .line 198
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 199
    return-void
.end method
