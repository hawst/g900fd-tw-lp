.class public final Lcom/google/android/gms/googlehelp/f/h;
.super Lcom/google/android/gms/googlehelp/f/c;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/googlehelp/d/d;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/f/c;-><init>()V

    .line 40
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->g()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/h;->a:Landroid/content/Context;

    .line 41
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c()Lcom/google/android/gms/googlehelp/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/h;->b:Lcom/google/android/gms/googlehelp/d/d;

    .line 42
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/h;->b:Lcom/google/android/gms/googlehelp/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/d/d;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/h;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ak;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/h;->a:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/googlehelp/service/ConnectivityBroadcastReceiver;->a(Landroid/content/Context;Z)V

    :cond_0
    :goto_0
    const/4 v0, 0x0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/h;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/service/MetricsReportService;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
