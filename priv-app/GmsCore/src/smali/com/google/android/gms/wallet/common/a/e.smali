.class public final Lcom/google/android/gms/wallet/common/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/util/Comparator;

.field private static final b:Ljava/util/regex/Pattern;

.field private static final c:Ljava/util/regex/Pattern;

.field private static final d:[C

.field private static final e:Lorg/json/JSONObject;

.field private static f:[Ljava/lang/String;

.field private static final g:Ljava/util/regex/Pattern;

.field private static final h:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 79
    const-string v0, "/"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/a/e;->b:Ljava/util/regex/Pattern;

    .line 85
    const-string v0, "--"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/a/e;->c:Ljava/util/regex/Pattern;

    .line 103
    invoke-static {}, Lcom/google/android/gms/wallet/dynamite/common/a/a;->a()[C

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/a/e;->d:[C

    .line 118
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    const-string v1, "{\"upper\": \"C\", \"zip_name_type\": \"postal\", \"fmt\": \"%N%n%O%n%A%n%C\", \"require\": \"AC\", \"state_name_type\": \"province\", \"id\": \"data/ZZ\", \"dir\": \"ltr\"}"

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/wallet/common/a/e;->e:Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    new-instance v0, Lcom/google/android/gms/wallet/common/a/f;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/a/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/a/e;->a:Ljava/util/Comparator;

    .line 704
    const-string v0, "(\\\\d|\\d|[^\\^\\w])"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/a/e;->g:Ljava/util/regex/Pattern;

    .line 753
    const-string v0, "^[\\w \\-]+$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/a/e;->h:Ljava/util/regex/Pattern;

    return-void

    .line 119
    :catch_0
    move-exception v0

    .line 120
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static final a(C)I
    .locals 1

    .prologue
    .line 419
    sparse-switch p0, :sswitch_data_0

    .line 445
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 421
    :sswitch_0
    sget v0, Lcom/google/android/gms/j;->ag:I

    goto :goto_0

    .line 423
    :sswitch_1
    sget v0, Lcom/google/android/gms/j;->ah:I

    goto :goto_0

    .line 425
    :sswitch_2
    sget v0, Lcom/google/android/gms/j;->ai:I

    goto :goto_0

    .line 427
    :sswitch_3
    sget v0, Lcom/google/android/gms/j;->aj:I

    goto :goto_0

    .line 429
    :sswitch_4
    sget v0, Lcom/google/android/gms/j;->ak:I

    goto :goto_0

    .line 431
    :sswitch_5
    sget v0, Lcom/google/android/gms/j;->al:I

    goto :goto_0

    .line 433
    :sswitch_6
    sget v0, Lcom/google/android/gms/j;->am:I

    goto :goto_0

    .line 435
    :sswitch_7
    sget v0, Lcom/google/android/gms/j;->an:I

    goto :goto_0

    .line 437
    :sswitch_8
    sget v0, Lcom/google/android/gms/j;->ao:I

    goto :goto_0

    .line 439
    :sswitch_9
    sget v0, Lcom/google/android/gms/j;->ap:I

    goto :goto_0

    .line 441
    :sswitch_a
    sget v0, Lcom/google/android/gms/j;->aq:I

    goto :goto_0

    .line 443
    :sswitch_b
    sget v0, Lcom/google/android/gms/j;->ag:I

    goto :goto_0

    .line 419
    nop

    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_0
        0x32 -> :sswitch_1
        0x33 -> :sswitch_2
        0x41 -> :sswitch_b
        0x43 -> :sswitch_6
        0x44 -> :sswitch_5
        0x4e -> :sswitch_9
        0x4f -> :sswitch_7
        0x52 -> :sswitch_4
        0x53 -> :sswitch_3
        0x58 -> :sswitch_a
        0x5a -> :sswitch_8
    .end sparse-switch
.end method

.method private static a(Lcom/google/checkout/inapp/proto/a/b;)I
    .locals 1

    .prologue
    .line 1160
    if-nez p0, :cond_0

    .line 1161
    const/16 v0, 0x7b

    .line 1167
    :goto_0
    return v0

    .line 1162
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/wallet/common/w;->b(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1163
    const/16 v0, 0x7c

    goto :goto_0

    .line 1164
    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1165
    const/16 v0, 0x7d

    goto :goto_0

    .line 1167
    :cond_2
    const/16 v0, 0x7f

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 650
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_flag.png"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/wallet/common/a/e;->f:[Ljava/lang/String;

    if-nez v3, :cond_0

    const-string v3, "wallet/images/flags"

    invoke-virtual {v2, v3}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/google/android/gms/wallet/common/a/e;->f:[Ljava/lang/String;

    :cond_0
    sget-object v3, Lcom/google/android/gms/wallet/common/a/e;->f:[Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 653
    :goto_0
    return-object v0

    .line 650
    :cond_1
    new-instance v3, Ljava/io/File;

    const-string v4, "wallet/images/flags"

    invoke-direct {v3, v4, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v3, 0x0

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    const/16 v3, 0xa0

    iput v3, v2, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    const/16 v3, 0xa0

    iput v3, v2, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v3, v4, v1, v5, v2}, Landroid/graphics/drawable/Drawable;->createFromResourceStream(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 651
    :catch_0
    move-exception v1

    .line 652
    const-string v2, "AddressUtils"

    const-string v3, "Unable to load flag drawable"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static a(Ljava/util/Collection;)Landroid/util/Pair;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 932
    if-nez p0, :cond_0

    .line 933
    invoke-static {v0, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 954
    :goto_0
    return-object v0

    .line 936
    :cond_0
    new-instance v1, Ljava/util/TreeSet;

    sget-object v0, Lcom/google/android/gms/wallet/common/a/e;->a:Ljava/util/Comparator;

    invoke-direct {v1, v0}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 938
    new-instance v2, Ljava/util/TreeSet;

    invoke-direct {v2}, Ljava/util/TreeSet;-><init>()V

    .line 940
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 941
    if-eqz v0, :cond_1

    .line 942
    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->b(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 945
    iget-object v4, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    if-eqz v4, :cond_2

    .line 948
    iget-object v4, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    invoke-virtual {v1, v4}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 950
    :cond_2
    iget-object v4, v0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 951
    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 954
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0
.end method

.method public static final a(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 319
    invoke-static {p0}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 320
    invoke-static {v1}, Lcom/google/android/gms/wallet/dynamite/common/a/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 322
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 323
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 326
    :cond_0
    return-object v0
.end method

.method public static final a(Landroid/content/Context;CLorg/json/JSONObject;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 341
    sparse-switch p1, :sswitch_data_0

    .line 404
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 343
    :sswitch_0
    sget v0, Lcom/google/android/gms/p;->zz:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 345
    :sswitch_1
    sget v0, Lcom/google/android/gms/p;->zA:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 347
    :sswitch_2
    sget v0, Lcom/google/android/gms/p;->zB:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 349
    :sswitch_3
    const-string v0, "state_name_type"

    invoke-static {p2, v0}, Lcom/google/android/gms/wallet/common/a/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 351
    const-string v1, "province"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 352
    sget v0, Lcom/google/android/gms/p;->zL:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 353
    :cond_0
    const-string v1, "state"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 354
    sget v0, Lcom/google/android/gms/p;->zN:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 355
    :cond_1
    const-string v1, "area"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 356
    sget v0, Lcom/google/android/gms/p;->zC:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 357
    :cond_2
    const-string v1, "county"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 358
    sget v0, Lcom/google/android/gms/p;->zD:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 359
    :cond_3
    const-string v1, "department"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 360
    sget v0, Lcom/google/android/gms/p;->zE:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 361
    :cond_4
    const-string v1, "district"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 362
    sget v0, Lcom/google/android/gms/p;->zF:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 363
    :cond_5
    const-string v1, "do_si"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 364
    sget v0, Lcom/google/android/gms/p;->zG:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 365
    :cond_6
    const-string v1, "emirate"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 366
    sget v0, Lcom/google/android/gms/p;->zH:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 367
    :cond_7
    const-string v1, "island"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 368
    sget v0, Lcom/google/android/gms/p;->zI:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 369
    :cond_8
    const-string v1, "parish"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 370
    sget v0, Lcom/google/android/gms/p;->zJ:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 371
    :cond_9
    const-string v1, "prefecture"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 372
    sget v0, Lcom/google/android/gms/p;->zK:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 373
    :cond_a
    const-string v1, "region"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 374
    sget v0, Lcom/google/android/gms/p;->zM:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 377
    :cond_b
    sget v0, Lcom/google/android/gms/p;->zL:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 380
    :sswitch_4
    sget v0, Lcom/google/android/gms/p;->zO:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 382
    :sswitch_5
    sget v0, Lcom/google/android/gms/p;->zP:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 384
    :sswitch_6
    sget v0, Lcom/google/android/gms/p;->zQ:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 386
    :sswitch_7
    sget v0, Lcom/google/android/gms/p;->zR:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 388
    :sswitch_8
    const-string v0, "zip_name_type"

    invoke-static {p2, v0}, Lcom/google/android/gms/wallet/common/a/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 389
    const-string v1, "postal"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 390
    sget v0, Lcom/google/android/gms/p;->zS:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 391
    :cond_c
    const-string v1, "zip"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 392
    sget v0, Lcom/google/android/gms/p;->zW:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 395
    :cond_d
    sget v0, Lcom/google/android/gms/p;->zS:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 398
    :sswitch_9
    sget v0, Lcom/google/android/gms/p;->zT:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 400
    :sswitch_a
    sget v0, Lcom/google/android/gms/p;->zV:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 402
    :sswitch_b
    sget v0, Lcom/google/android/gms/p;->zz:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 341
    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_0
        0x32 -> :sswitch_1
        0x33 -> :sswitch_2
        0x41 -> :sswitch_b
        0x43 -> :sswitch_6
        0x44 -> :sswitch_5
        0x4e -> :sswitch_9
        0x4f -> :sswitch_7
        0x52 -> :sswitch_4
        0x53 -> :sswitch_3
        0x58 -> :sswitch_a
        0x5a -> :sswitch_8
    .end sparse-switch
.end method

.method public static final a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 205
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 209
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Collection;[C)Ljava/util/ArrayList;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 844
    if-nez p0, :cond_0

    .line 845
    const/4 v0, 0x0

    .line 874
    :goto_0
    return-object v0

    .line 848
    :cond_0
    if-nez p1, :cond_3

    .line 849
    sget-object p1, Lcom/google/android/gms/wallet/common/a/e;->d:[C

    .line 858
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 860
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_1
    if-ltz v4, :cond_6

    .line 861
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_2
    if-ltz v3, :cond_2

    .line 862
    if-eq v4, v3, :cond_5

    .line 863
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/t/a/b;

    .line 866
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/t/a/b;

    .line 867
    invoke-static {v0, v1, p1}, Lcom/google/android/gms/wallet/common/a/e;->a(Lcom/google/t/a/b;Lcom/google/t/a/b;[C)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 868
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 860
    :cond_2
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 851
    :goto_3
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 852
    aget-char v2, p1, v0

    invoke-static {v2}, Lcom/google/android/gms/wallet/dynamite/common/a/a;->a(C)Z

    move-result v2

    if-nez v2, :cond_4

    .line 853
    aput-char v1, p1, v0

    .line 851
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 861
    :cond_5
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_2

    :cond_6
    move-object v0, v2

    .line 874
    goto :goto_0
.end method

.method public static a(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 6

    .prologue
    .line 458
    if-nez p0, :cond_0

    .line 459
    new-instance p0, Lorg/json/JSONObject;

    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 463
    :cond_0
    sget-object v0, Lcom/google/android/gms/wallet/common/a/e;->e:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v2

    .line 464
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 465
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 467
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 469
    :try_start_0
    sget-object v1, Lcom/google/android/gms/wallet/common/a/e;->e:Lorg/json/JSONObject;

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 470
    :catch_0
    move-exception v1

    .line 471
    const-string v3, "AddressUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not set property \""

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\" on JSONObject instance"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 476
    :cond_2
    return-object p0
.end method

.method public static a(CLorg/json/JSONObject;)Z
    .locals 2

    .prologue
    .line 532
    const/16 v0, 0x4e

    if-ne p0, v0, :cond_0

    .line 533
    const/4 v0, 0x1

    .line 547
    :goto_0
    return v0

    .line 536
    :cond_0
    const-string v0, "require"

    invoke-static {p1, v0}, Lcom/google/android/gms/wallet/common/a/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 537
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 538
    const/4 v0, 0x0

    goto :goto_0

    .line 542
    :cond_1
    const/16 v1, 0x31

    if-ne p0, v1, :cond_2

    .line 544
    const/16 p0, 0x41

    .line 547
    :cond_2
    invoke-static {p0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(Lcom/google/checkout/inapp/proto/a/b;Lcom/google/checkout/inapp/proto/a/b;)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1065
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 1066
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v1

    .line 1083
    :cond_1
    :goto_0
    return v0

    .line 1068
    :cond_2
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/a/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/wallet/common/a/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1071
    iget-object v3, p0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    .line 1072
    iget-object v4, p1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    .line 1073
    sget-object v5, Lcom/google/android/gms/wallet/common/a/e;->d:[C

    array-length v6, v5

    move v2, v0

    :goto_1
    if-ge v2, v6, :cond_4

    aget-char v7, v5, v2

    .line 1074
    if-eqz v7, :cond_3

    .line 1075
    invoke-static {v3, v7}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;C)Ljava/lang/String;

    move-result-object v8

    .line 1078
    invoke-static {v4, v7}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;C)Ljava/lang/String;

    move-result-object v7

    .line 1079
    invoke-static {v8, v7}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1073
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    move v0, v1

    .line 1083
    goto :goto_0
.end method

.method public static a(Lcom/google/t/a/b;Lcom/google/t/a/b;)Z
    .locals 1

    .prologue
    .line 881
    sget-object v0, Lcom/google/android/gms/wallet/common/a/e;->d:[C

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/wallet/common/a/e;->a(Lcom/google/t/a/b;Lcom/google/t/a/b;[C)Z

    move-result v0

    return v0
.end method

.method private static a(Lcom/google/t/a/b;Lcom/google/t/a/b;[C)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 895
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    move v0, v1

    .line 915
    :cond_1
    :goto_0
    return v0

    .line 899
    :cond_2
    array-length v3, p2

    move v2, v1

    move v0, v1

    :goto_1
    if-ge v2, v3, :cond_1

    aget-char v4, p2, v2

    .line 900
    if-eqz v4, :cond_3

    .line 901
    invoke-static {p0, v4}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;C)Ljava/lang/String;

    move-result-object v5

    .line 905
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 906
    invoke-static {p1, v4}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;C)Ljava/lang/String;

    move-result-object v0

    .line 909
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 910
    const/4 v0, 0x1

    .line 899
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    move v0, v1

    .line 912
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)[C
    .locals 4

    .prologue
    .line 488
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489
    new-instance v0, Ljava/util/UnknownFormatConversionException;

    const-string v1, "Cannot convert null/empty formats"

    invoke-direct {v0, v1}, Ljava/util/UnknownFormatConversionException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 492
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 493
    invoke-static {p0}, Lcom/google/android/gms/wallet/common/a/e;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 495
    const-string v3, "%."

    invoke-virtual {v0, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "%n"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 496
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 500
    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 504
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 505
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    .line 506
    const/16 v3, 0x41

    if-ne v0, v3, :cond_3

    .line 507
    const/16 v0, 0x31

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 508
    const/16 v0, 0x32

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 509
    const/16 v0, 0x33

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 511
    :cond_3
    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 515
    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v3, v0, [C

    .line 516
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    array-length v0, v3

    if-ge v1, v0, :cond_5

    .line 517
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    aput-char v0, v3, v1

    .line 516
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 519
    :cond_5
    return-object v3
.end method

.method public static final a(Ljava/util/List;)[I
    .locals 4

    .prologue
    .line 184
    if-nez p0, :cond_0

    .line 185
    const/4 v0, 0x0

    .line 194
    :goto_0
    return-object v0

    .line 188
    :cond_0
    const/4 v0, 0x0

    .line 189
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    new-array v2, v1, [I

    .line 190
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 191
    invoke-static {v0}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->b(Ljava/lang/String;)I

    move-result v0

    aput v0, v2, v1

    .line 192
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 193
    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 194
    goto :goto_0
.end method

.method public static final a([I)[I
    .locals 9

    .prologue
    const/16 v5, 0x35a

    const/4 v3, 0x0

    .line 244
    if-nez p0, :cond_0

    .line 245
    const/4 v0, 0x0

    .line 308
    :goto_0
    return-object v0

    .line 249
    :cond_0
    new-instance v1, Landroid/util/SparseArray;

    const/16 v0, 0x111

    invoke-direct {v1, v0}, Landroid/util/SparseArray;-><init>(I)V

    .line 252
    const-string v0, ""

    invoke-virtual {v1, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 255
    new-instance v6, Ljava/util/ArrayList;

    array-length v0, p0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 256
    array-length v2, p0

    move v0, v3

    :goto_1
    if-ge v0, v2, :cond_2

    aget v4, p0, v0

    .line 257
    if-eqz v4, :cond_1

    if-eq v4, v5, :cond_1

    .line 258
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 256
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 262
    :cond_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 264
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 265
    invoke-static {v4}, Lcom/google/android/gms/wallet/common/a/e;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 266
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 267
    const-string v0, "AddressUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "Region code \'"

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "\' without label"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    const-string v0, ""

    .line 271
    :cond_3
    invoke-virtual {v1, v4, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_2

    .line 275
    :cond_4
    new-instance v0, Lcom/google/android/gms/wallet/common/a/g;

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/common/a/g;-><init>(Landroid/util/SparseArray;)V

    invoke-static {v6, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move v4, v3

    move v1, v3

    move v2, v3

    .line 289
    :goto_3
    if-ge v4, v7, :cond_5

    .line 290
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 291
    if-eq v0, v1, :cond_8

    .line 293
    add-int/lit8 v1, v2, 0x1

    .line 289
    :goto_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v1

    move v1, v0

    goto :goto_3

    .line 298
    :cond_5
    new-array v4, v2, [I

    move v1, v3

    move v5, v3

    .line 299
    :goto_5
    if-ge v5, v7, :cond_6

    .line 300
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 301
    if-eq v2, v3, :cond_7

    .line 302
    aput v2, v4, v1

    .line 304
    add-int/lit8 v0, v1, 0x1

    move v1, v2

    .line 299
    :goto_6
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v3, v1

    move v1, v0

    goto :goto_5

    :cond_6
    move-object v0, v4

    .line 308
    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v1, v3

    goto :goto_6

    :cond_8
    move v0, v1

    move v1, v2

    goto :goto_4
.end method

.method public static final a([Ljava/lang/String;)[I
    .locals 4

    .prologue
    .line 164
    if-nez p0, :cond_1

    .line 165
    const/4 v0, 0x0

    .line 173
    :cond_0
    return-object v0

    .line 168
    :cond_1
    array-length v2, p0

    .line 169
    new-array v0, v2, [I

    .line 170
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 171
    aget-object v3, p0, v1

    invoke-static {v3}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->b(Ljava/lang/String;)I

    move-result v3

    aput v3, v0, v1

    .line 170
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/google/aa/a/a/a/b;Lcom/google/android/gms/identity/intents/UserAddressRequest;)[Lcom/google/checkout/inapp/proto/a/b;
    .locals 11

    .prologue
    const/16 v10, 0x7f

    const/4 v2, 0x0

    .line 1088
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 1089
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/identity/intents/UserAddressRequest;->b()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1090
    invoke-virtual {p1}, Lcom/google/android/gms/identity/intents/UserAddressRequest;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/identity/intents/model/CountrySpecification;

    .line 1091
    invoke-virtual {v0}, Lcom/google/android/gms/identity/intents/model/CountrySpecification;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1094
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1095
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 1096
    const/4 v0, -0x1

    move v1, v2

    move v3, v0

    .line 1098
    :goto_1
    iget-object v0, p0, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 1099
    iget-object v0, p0, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/a/e;->a(Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v0

    if-ne v0, v10, :cond_2

    .line 1101
    new-instance v8, Lcom/google/android/gms/wallet/common/a/h;

    iget-object v0, p0, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {v8, v0, v2}, Lcom/google/android/gms/wallet/common/a/h;-><init>(Lcom/google/checkout/inapp/proto/a/b;B)V

    .line 1102
    invoke-virtual {v7, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1103
    invoke-virtual {v7, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1104
    invoke-virtual {v5}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, v8, Lcom/google/android/gms/wallet/common/a/h;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1106
    :cond_1
    iget-object v0, p0, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1107
    iget-object v0, p0, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    aget-object v0, v0, v1

    iget-boolean v0, v0, Lcom/google/checkout/inapp/proto/j;->f:Z

    if-eqz v0, :cond_2

    .line 1108
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v3, v0, -0x1

    .line 1098
    :cond_2
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1112
    :cond_3
    iget-object v0, p0, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    aget-object v0, v0, v1

    iget-boolean v0, v0, Lcom/google/checkout/inapp/proto/j;->f:Z

    if-eqz v0, :cond_2

    move v4, v2

    .line 1113
    :goto_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_2

    .line 1114
    new-instance v9, Lcom/google/android/gms/wallet/common/a/h;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {v9, v0, v2}, Lcom/google/android/gms/wallet/common/a/h;-><init>(Lcom/google/checkout/inapp/proto/a/b;B)V

    .line 1115
    invoke-virtual {v9, v8}, Lcom/google/android/gms/wallet/common/a/h;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v3, v4

    .line 1117
    goto :goto_2

    .line 1113
    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    .line 1124
    :cond_5
    if-lez v3, :cond_6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    .line 1125
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 1126
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v6, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1127
    invoke-virtual {v6, v3, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_6
    move v0, v2

    .line 1129
    :goto_4
    iget-object v1, p0, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v1, v1

    if-ge v0, v1, :cond_9

    .line 1130
    iget-object v1, p0, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    aget-object v1, v1, v0

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/a/e;->a(Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v1

    if-ne v1, v10, :cond_8

    .line 1131
    new-instance v1, Lcom/google/android/gms/wallet/common/a/h;

    iget-object v3, p0, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    aget-object v3, v3, v0

    invoke-direct {v1, v3, v2}, Lcom/google/android/gms/wallet/common/a/h;-><init>(Lcom/google/checkout/inapp/proto/a/b;B)V

    .line 1132
    invoke-virtual {v7, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 1133
    invoke-virtual {v7, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1134
    invoke-virtual {v5}, Ljava/util/HashSet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/a/h;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1136
    :cond_7
    iget-object v1, p0, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    aget-object v1, v1, v0

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1129
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1141
    :cond_9
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/checkout/inapp/proto/a/b;

    .line 1142
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/checkout/inapp/proto/a/b;

    return-object v0
.end method

.method public static b(Lorg/json/JSONObject;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 558
    if-nez p0, :cond_1

    .line 572
    :cond_0
    :goto_0
    return v0

    .line 562
    :cond_1
    const-string v1, "id"

    invoke-static {p0, v1}, Lcom/google/android/gms/wallet/common/a/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 563
    if-eqz v1, :cond_0

    .line 567
    sget-object v2, Lcom/google/android/gms/wallet/common/a/e;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v2

    .line 568
    array-length v3, v2

    packed-switch v3, :pswitch_data_0

    .line 574
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid address data id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 571
    :pswitch_0
    sget-object v1, Lcom/google/android/gms/wallet/common/a/e;->c:Ljava/util/regex/Pattern;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v1

    .line 572
    aget-object v0, v1, v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 568
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Ljava/lang/String;)Ljava/util/regex/Pattern;
    .locals 2

    .prologue
    .line 632
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633
    const/4 v0, 0x0

    .line 635
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ").*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(I)Z
    .locals 3

    .prologue
    .line 582
    invoke-static {p0}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 583
    sget-object v0, Lcom/google/android/gms/wallet/b/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public static final b(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;
    .locals 2

    .prologue
    .line 221
    invoke-static {p0, p1}, Lcom/google/android/gms/wallet/common/a/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 223
    if-nez v0, :cond_0

    .line 224
    const/4 v0, 0x0

    .line 228
    :goto_0
    return-object v0

    .line 227
    :cond_0
    const-string v1, "~"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    .prologue
    .line 771
    if-eqz p0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 772
    :cond_0
    const/4 v0, 0x0

    .line 775
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v6, v0

    array-length v0, v6

    new-array v5, v0, [Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/gms/wallet/common/a/e;->c(Lorg/json/JSONObject;)Ljava/util/regex/Pattern;

    move-result-object v7

    if-eqz v7, :cond_7

    const-string v0, "sub_zips"

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/a/e;->b(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_2

    array-length v0, v8

    if-nez v0, :cond_3

    :cond_2
    move-object v0, v5

    :goto_1
    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_2
    array-length v1, v6

    if-ge v0, v1, :cond_7

    aget-object v9, v6, v0

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {v7, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v3, -0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    array-length v10, v8

    :goto_3
    if-ge v4, v10, :cond_5

    aget-object v2, v8, v4

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/a/e;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v11

    if-eqz v11, :cond_8

    const/4 v11, 0x1

    invoke-virtual {v2, v11}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v11, -0x1

    if-eq v3, v11, :cond_4

    if-le v2, v1, :cond_8

    :cond_4
    move v1, v2

    move v2, v4

    :goto_4
    add-int/lit8 v4, v4, 0x1

    move v3, v2

    goto :goto_3

    :cond_5
    if-ltz v3, :cond_6

    const-string v1, "sub_keys"

    invoke-static {p0, v1}, Lcom/google/android/gms/wallet/common/a/e;->b(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    array-length v2, v1

    if-ge v3, v2, :cond_6

    aget-object v1, v1, v3

    aput-object v1, v5, v0

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    move-object v0, v5

    goto :goto_1

    :cond_8
    move v2, v3

    goto :goto_4
.end method

.method private static c(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1035
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1038
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v5, v4

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v5, :cond_4

    aget-char v6, v4, v2

    .line 1039
    if-eqz v0, :cond_2

    .line 1041
    const-string v0, "%n"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "%"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1042
    const-string v0, "%n"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 1038
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1044
    :cond_0
    invoke-static {v6}, Lcom/google/android/gms/wallet/dynamite/common/a/a;->a(C)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1045
    new-instance v0, Ljava/util/UnknownFormatConversionException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot determine AddressField for \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/UnknownFormatConversionException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "%"

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_1

    .line 1050
    :cond_2
    const/16 v7, 0x25

    if-ne v6, v7, :cond_3

    .line 1051
    const/4 v0, 0x1

    goto :goto_1

    .line 1053
    :cond_3
    invoke-static {v6}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1056
    :cond_4
    return-object v3
.end method

.method public static c(Lorg/json/JSONObject;)Ljava/util/regex/Pattern;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 602
    if-nez p0, :cond_1

    .line 623
    :cond_0
    :goto_0
    return-object v0

    .line 606
    :cond_1
    const-string v1, "zip"

    invoke-static {p0, v1}, Lcom/google/android/gms/wallet/common/a/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 607
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 611
    const-string v2, "id"

    invoke-static {p0, v2}, Lcom/google/android/gms/wallet/common/a/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 612
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 616
    sget-object v3, Lcom/google/android/gms/wallet/common/a/e;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v2}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    .line 617
    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 619
    :pswitch_0
    const/4 v0, 0x2

    invoke-static {v1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    goto :goto_0

    .line 621
    :pswitch_1
    invoke-static {v1}, Lcom/google/android/gms/wallet/common/a/e;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    goto :goto_0

    .line 617
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 1060
    if-nez p0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static d(Lorg/json/JSONObject;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 716
    if-nez p0, :cond_1

    .line 720
    :cond_0
    :goto_0
    return v0

    .line 719
    :cond_1
    const-string v1, "zip"

    invoke-static {p0, v1}, Lcom/google/android/gms/wallet/common/a/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 720
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/google/android/gms/wallet/common/a/e;->g:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static d(Lorg/json/JSONObject;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 968
    if-nez p0, :cond_1

    .line 993
    :cond_0
    :goto_0
    return v0

    .line 972
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 976
    const-string v2, "lname"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "sub_lnames"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "lfmt"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 978
    :cond_2
    const-string v2, "lang"

    invoke-static {p0, v2}, Lcom/google/android/gms/wallet/common/a/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 980
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 981
    invoke-static {p0}, Lcom/google/android/gms/wallet/common/a/e;->b(Lorg/json/JSONObject;)I

    move-result v0

    .line 982
    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/dynamite/common/a/b;->a(ILjava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 983
    :cond_3
    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/wallet/dynamite/common/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v0, v1

    .line 985
    goto :goto_0

    .line 986
    :cond_4
    invoke-static {v2, p1}, Lcom/google/android/gms/wallet/dynamite/common/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 989
    goto :goto_0
.end method

.method public static e(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 739
    if-nez p0, :cond_1

    .line 746
    :cond_0
    :goto_0
    return-object v0

    .line 742
    :cond_1
    const-string v1, "zip"

    invoke-static {p0, v1}, Lcom/google/android/gms/wallet/common/a/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 743
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_0

    move-object v0, v1

    .line 744
    goto :goto_0

    .line 743
    :cond_2
    sget-object v2, Lcom/google/android/gms/wallet/common/a/e;->h:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    goto :goto_1
.end method

.method public static e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1006
    const-string v1, "languages"

    invoke-static {p0, v1}, Lcom/google/android/gms/wallet/common/a/e;->b(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1007
    if-eqz v3, :cond_0

    array-length v1, v3

    const/4 v2, 0x1

    if-gt v1, v2, :cond_1

    .line 1025
    :cond_0
    :goto_0
    return-object v0

    .line 1011
    :cond_1
    const-string v1, "lang"

    invoke-static {p0, v1}, Lcom/google/android/gms/wallet/common/a/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1012
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1016
    invoke-static {v1, p1}, Lcom/google/android/gms/wallet/dynamite/common/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1020
    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 1021
    invoke-static {v1, p1}, Lcom/google/android/gms/wallet/dynamite/common/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v1

    .line 1022
    goto :goto_0

    .line 1020
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method
