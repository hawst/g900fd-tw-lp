.class final Lcom/google/android/gms/auth/frp/f;
.super Lcom/google/android/gms/auth/frp/h;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/frp/FrpService;

.field private final b:Lcom/google/android/gms/auth/a/k;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/auth/frp/FrpService;Lcom/google/android/gms/auth/a/k;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/gms/auth/frp/f;->a:Lcom/google/android/gms/auth/frp/FrpService;

    invoke-direct {p0}, Lcom/google/android/gms/auth/frp/h;-><init>()V

    .line 37
    iput-object p2, p0, Lcom/google/android/gms/auth/frp/f;->b:Lcom/google/android/gms/auth/a/k;

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionRequest;)Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionResponse;
    .locals 3

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/auth/frp/f;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->b(I)V

    .line 61
    new-instance v1, Lcom/google/android/gms/auth/firstparty/dataservice/w;

    iget-object v0, p0, Lcom/google/android/gms/auth/frp/f;->a:Lcom/google/android/gms/auth/frp/FrpService;

    invoke-direct {v1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/w;-><init>(Landroid/content/Context;)V

    .line 63
    new-instance v0, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    invoke-direct {v0}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;-><init>()V

    iget-object v2, p1, Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionRequest;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v0

    iget-object v2, p1, Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionRequest;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->c(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v0

    .line 66
    new-instance v2, Lcom/google/android/gms/auth/firstparty/dataservice/am;

    invoke-direct {v2, v1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/am;-><init>(Lcom/google/android/gms/auth/firstparty/dataservice/w;Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a(Lcom/google/android/gms/auth/firstparty/dataservice/ap;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/ValidateAccountCredentialsResponse;

    .line 68
    iget v2, v0, Lcom/google/android/gms/auth/firstparty/dataservice/ValidateAccountCredentialsResponse;->b:I

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/google/android/gms/auth/firstparty/dataservice/ValidateAccountCredentialsResponse;->c:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 70
    :cond_0
    const/4 v1, 0x1

    .line 71
    iget v0, v0, Lcom/google/android/gms/auth/firstparty/dataservice/ValidateAccountCredentialsResponse;->b:I

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 79
    :goto_0
    new-instance v1, Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionResponse;

    invoke-direct {v1, v0}, Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionResponse;-><init>(I)V

    move-object v0, v1

    .line 89
    :goto_1
    return-object v0

    .line 73
    :pswitch_0
    const/4 v0, 0x3

    .line 74
    goto :goto_0

    .line 76
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 81
    :cond_1
    iget-object v0, v0, Lcom/google/android/gms/auth/firstparty/dataservice/ValidateAccountCredentialsResponse;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/CheckFactoryResetPolicyComplianceRequest;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/CheckFactoryResetPolicyComplianceRequest;

    move-result-object v0

    .line 83
    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a(Lcom/google/android/gms/auth/firstparty/dataservice/CheckFactoryResetPolicyComplianceRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/CheckFactoryResetPolicyComplianceResponse;

    move-result-object v0

    .line 85
    iget-boolean v2, v0, Lcom/google/android/gms/auth/firstparty/dataservice/CheckFactoryResetPolicyComplianceResponse;->b:Z

    if-eqz v2, :cond_2

    .line 87
    invoke-virtual {v1}, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a()V

    .line 89
    :cond_2
    new-instance v1, Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionResponse;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/firstparty/dataservice/CheckFactoryResetPolicyComplianceResponse;->b:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    :goto_2
    invoke-direct {v1, v0}, Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionResponse;-><init>(I)V

    move-object v0, v1

    goto :goto_1

    :cond_3
    const/4 v0, 0x4

    goto :goto_2

    .line 71
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/auth/frp/f;->a:Lcom/google/android/gms/auth/frp/FrpService;

    invoke-static {v0}, Lcom/google/android/gms/auth/be/a/b;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/a/b;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/a/b;->a()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/auth/frp/f;->a:Lcom/google/android/gms/auth/frp/FrpService;

    .line 50
    invoke-static {v0}, Lcom/google/android/gms/auth/be/a/b;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/a/b;

    move-result-object v1

    .line 52
    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 53
    const-string v2, "com.google"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 54
    invoke-virtual {v1}, Lcom/google/android/gms/auth/be/a/b;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
