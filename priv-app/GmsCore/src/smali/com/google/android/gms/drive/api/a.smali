.class final Lcom/google/android/gms/drive/api/a;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/api/ApiService;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/drive/api/ApiService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/google/android/gms/drive/api/a;->a:Lcom/google/android/gms/drive/api/ApiService;

    .line 218
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 219
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/api/ApiService;Landroid/content/Context;B)V
    .locals 0

    .prologue
    .line 215
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/api/a;-><init>(Lcom/google/android/gms/drive/api/ApiService;Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 13

    .prologue
    .line 229
    iget-object v12, p0, Lcom/google/android/gms/drive/api/a;->a:Lcom/google/android/gms/drive/api/ApiService;

    .line 230
    invoke-virtual {v12}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 231
    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v2

    .line 233
    const-string v3, "proxy_package_name"

    move-object/from16 v0, p6

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 238
    if-eqz v2, :cond_0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 242
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, v5, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 243
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v1, Landroid/content/pm/ApplicationInfo;->uid:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v6, p3

    .line 262
    :goto_0
    new-instance v1, Lcom/google/android/gms/drive/api/a/j;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a;->a:Lcom/google/android/gms/drive/api/ApiService;

    invoke-static {v2}, Lcom/google/android/gms/drive/api/ApiService;->a(Lcom/google/android/gms/drive/api/ApiService;)Lcom/google/android/gms/drive/api/c;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/drive/api/a;->a:Lcom/google/android/gms/drive/api/ApiService;

    invoke-static {v3}, Lcom/google/android/gms/drive/api/ApiService;->b(Lcom/google/android/gms/drive/api/ApiService;)Lcom/google/android/gms/drive/api/h;

    move-result-object v3

    move-object/from16 v7, p5

    move-object/from16 v8, p4

    move-object v9, p1

    move v10, p2

    move-object/from16 v11, p6

    invoke-direct/range {v1 .. v11}, Lcom/google/android/gms/drive/api/a/j;-><init>(Lcom/google/android/gms/drive/api/c;Lcom/google/android/gms/drive/api/h;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/gms/common/internal/bg;ILandroid/os/Bundle;)V

    invoke-static {v12, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 272
    :goto_1
    return-void

    .line 246
    :catch_0
    move-exception v1

    .line 247
    const-string v2, "ApiService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Bad package name: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 248
    const/16 v1, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {p1, v1, v2, v3}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    goto :goto_1

    .line 253
    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    .line 255
    const/4 v6, 0x0

    .line 256
    move-object/from16 v0, p3

    invoke-static {v12, v4, v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 258
    new-instance v1, Ljava/lang/SecurityException;

    const-string v2, "Invalid calling package"

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    move-object/from16 v5, p3

    goto :goto_0
.end method
