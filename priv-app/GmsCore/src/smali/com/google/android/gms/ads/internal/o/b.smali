.class public final Lcom/google/android/gms/ads/internal/o/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field public final a:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

.field public final b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

.field public final c:Lcom/google/android/gms/ads/internal/k/d;

.field public final d:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

.field public final e:I

.field public final f:J

.field public final g:J

.field public final h:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Lcom/google/android/gms/ads/internal/request/AdResponseParcel;Lcom/google/android/gms/ads/internal/k/d;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;IJJLorg/json/JSONObject;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/o/b;->a:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    .line 52
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    .line 53
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/o/b;->c:Lcom/google/android/gms/ads/internal/k/d;

    .line 54
    iput-object p4, p0, Lcom/google/android/gms/ads/internal/o/b;->d:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    .line 55
    iput p5, p0, Lcom/google/android/gms/ads/internal/o/b;->e:I

    .line 56
    iput-wide p6, p0, Lcom/google/android/gms/ads/internal/o/b;->f:J

    .line 57
    iput-wide p8, p0, Lcom/google/android/gms/ads/internal/o/b;->g:J

    .line 58
    iput-object p10, p0, Lcom/google/android/gms/ads/internal/o/b;->h:Lorg/json/JSONObject;

    .line 59
    return-void
.end method
