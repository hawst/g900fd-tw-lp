.class public final Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/drive/d/f;

.field private final b:Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;

.field private final c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/d/f;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/i;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/d/f;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;->a:Lcom/google/android/gms/drive/d/f;

    .line 42
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;

    invoke-direct {v0, p2, p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;-><init>(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/i;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;->b:Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;

    .line 43
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;

    invoke-direct {v0, p2, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;-><init>(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/d/f;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;

    .line 44
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/g/aw;)V
    .locals 3

    .prologue
    .line 35
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->o()Lcom/google/android/gms/drive/d/f;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->b()Lcom/google/android/gms/drive/g/i;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;-><init>(Lcom/google/android/gms/drive/d/f;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/i;)V

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/d/d;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;->b:Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;

    iget-object v1, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iget-object v2, p1, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-static {}, Lcom/google/android/gms/drive/auth/AppIdentity;->a()Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v4

    if-ne v2, v4, :cond_0

    const/4 v6, 0x1

    :cond_0
    const-wide/16 v4, 0x0

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->a(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/d/d;Ljava/lang/Boolean;JZ)V

    .line 124
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;->b(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)Lcom/google/android/gms/drive/d/d;

    move-result-object v0

    .line 56
    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;->a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/d/d;)V

    .line 57
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;ZZ)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 158
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;->a:Lcom/google/android/gms/drive/d/f;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/auth/g;->b()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/drive/d/f;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 166
    :try_start_1
    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 174
    :goto_0
    return-void

    .line 160
    :catch_0
    move-exception v0

    .line 161
    const-string v1, "SingleItemSynchronizer"

    const-string v2, "Failed to retrieve resource ID for file with unique ID %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 163
    throw v0

    .line 167
    :catch_1
    move-exception v0

    .line 168
    if-nez p4, :cond_0

    .line 169
    throw v0

    .line 171
    :cond_0
    const-string v0, "SingleItemSynchronizer"

    const-string v1, "File with unique ID %s does not exist but that\'s cool."

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p2, v2, v4

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)Lcom/google/android/gms/drive/d/d;
    .locals 5

    .prologue
    .line 87
    invoke-virtual {p1}, Lcom/google/android/gms/drive/auth/g;->b()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    const-string v1, "https://www.googleapis.com/auth/drive.appdata"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;->a(Lcom/google/android/gms/drive/auth/g;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/auth/g;->b()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    .line 98
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 99
    iget-object v2, p1, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v3, Lcom/google/android/gms/drive/aa;->b:Lcom/google/android/gms/drive/aa;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 100
    iget-wide v2, p1, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 103
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;->a:Lcom/google/android/gms/drive/d/f;

    invoke-interface {v2, v0, p2, v1}, Lcom/google/android/gms/drive/d/f;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/gms/drive/d/d;
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    return-object v0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    const-string v1, "SingleItemSynchronizer"

    const-string v2, "Failed to retrieve app folder ID from the server so cannot sync %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 93
    throw v0

    .line 104
    :catch_1
    move-exception v0

    .line 105
    invoke-static {v0}, Lcom/google/android/gms/drive/d/a/c;->a(Lcom/android/volley/ac;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 106
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "File "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exist on the server"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_2
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
