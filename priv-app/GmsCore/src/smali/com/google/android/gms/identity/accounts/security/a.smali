.class public final Lcom/google/android/gms/identity/accounts/security/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/security/SecureRandom;

.field private final b:Ljavax/crypto/SecretKey;

.field private final c:Ljavax/crypto/SecretKey;

.field private final d:Lcom/google/android/gms/identity/accounts/security/d;

.field private final e:Lcom/google/android/gms/identity/accounts/security/e;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/identity/accounts/security/d;Ljavax/crypto/SecretKey;Lcom/google/android/gms/identity/accounts/security/e;Ljavax/crypto/SecretKey;Ljava/security/SecureRandom;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const-string v0, "Cipher must not be null."

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    const-string v0, "Message key must not be null."

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const-string v0, "MAC must not be null."

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    const-string v0, "MAC key must not be null."

    invoke-static {p4, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    const-string v0, "Random must not be null."

    invoke-static {p5, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    iput-object p1, p0, Lcom/google/android/gms/identity/accounts/security/a;->d:Lcom/google/android/gms/identity/accounts/security/d;

    .line 57
    iput-object p4, p0, Lcom/google/android/gms/identity/accounts/security/a;->c:Ljavax/crypto/SecretKey;

    .line 58
    iput-object p2, p0, Lcom/google/android/gms/identity/accounts/security/a;->b:Ljavax/crypto/SecretKey;

    .line 59
    iput-object p3, p0, Lcom/google/android/gms/identity/accounts/security/a;->e:Lcom/google/android/gms/identity/accounts/security/e;

    .line 60
    iput-object p5, p0, Lcom/google/android/gms/identity/accounts/security/a;->a:Ljava/security/SecureRandom;

    .line 61
    return-void
.end method

.method public static a([B)Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;
    .locals 3

    .prologue
    .line 147
    :try_start_0
    sget-object v0, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/d;->a([BLandroid/os/Parcelable$Creator;)Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;
    :try_end_0
    .catch Lcom/google/android/gms/common/internal/safeparcel/b; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    :goto_0
    return-object v0

    .line 151
    :catch_0
    move-exception v0

    const-string v0, "AccountDataUtil"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    const-string v0, "AccountDataUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t parse input: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;)Z
    .locals 4

    .prologue
    .line 159
    invoke-virtual {p1}, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;->c()[B

    move-result-object v0

    .line 160
    invoke-virtual {p1}, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;->b()[B

    move-result-object v1

    .line 162
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/identity/accounts/security/a;->e:Lcom/google/android/gms/identity/accounts/security/e;

    iget-object v3, p0, Lcom/google/android/gms/identity/accounts/security/a;->c:Ljavax/crypto/SecretKey;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/identity/accounts/security/e;->a(Ljavax/crypto/SecretKey;)V
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    iget-object v2, p0, Lcom/google/android/gms/identity/accounts/security/a;->e:Lcom/google/android/gms/identity/accounts/security/e;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/identity/accounts/security/e;->a([B)V

    .line 167
    invoke-virtual {p1}, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;->d()[B

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/identity/accounts/security/a;->e:Lcom/google/android/gms/identity/accounts/security/e;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/identity/accounts/security/e;->b([B)[B

    move-result-object v1

    invoke-static {v0, v1}, Ljava/security/MessageDigest;->isEqual([B[B)Z

    move-result v0

    return v0

    .line 163
    :catch_0
    move-exception v0

    .line 164
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "The key didn\'t match the MAC supplied."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/identity/accounts/api/AccountData;[B)[B
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x3

    .line 96
    invoke-static {p1}, Lcom/google/android/gms/common/internal/safeparcel/d;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;)[B

    move-result-object v1

    .line 98
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/identity/accounts/security/a;->d:Lcom/google/android/gms/identity/accounts/security/d;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/identity/accounts/security/a;->b:Ljavax/crypto/SecretKey;

    new-instance v5, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v5, p2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    iget-object v6, p0, Lcom/google/android/gms/identity/accounts/security/a;->a:Ljava/security/SecureRandom;

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/gms/identity/accounts/security/d;->a(ILjavax/crypto/SecretKey;Ljavax/crypto/spec/IvParameterSpec;Ljava/security/SecureRandom;)V
    :try_end_0
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1

    .line 111
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/identity/accounts/security/a;->d:Lcom/google/android/gms/identity/accounts/security/d;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/identity/accounts/security/d;->a([B)[B
    :try_end_1
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljavax/crypto/BadPaddingException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v0

    .line 120
    :cond_0
    :goto_0
    return-object v0

    .line 99
    :catch_0
    move-exception v1

    .line 101
    const-string v2, "AccountDataUtil"

    invoke-static {v2, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    const-string v2, "AccountDataUtil"

    const-string v3, "Unexpected algorithm parameter exception."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 106
    :catch_1
    move-exception v0

    .line 107
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "The key didn\'t match the cipher supplied."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 114
    :catch_2
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Your cipher algorithm should request padding."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :catch_3
    move-exception v1

    .line 117
    const-string v2, "AccountDataUtil"

    invoke-static {v2, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 118
    const-string v2, "AccountDataUtil"

    const-string v3, "Unexpected padding exception."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a([B[B)[B
    .locals 3

    .prologue
    .line 81
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/security/a;->e:Lcom/google/android/gms/identity/accounts/security/e;

    iget-object v1, p0, Lcom/google/android/gms/identity/accounts/security/a;->c:Ljavax/crypto/SecretKey;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/identity/accounts/security/e;->a(Ljavax/crypto/SecretKey;)V
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/security/a;->e:Lcom/google/android/gms/identity/accounts/security/e;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/identity/accounts/security/e;->a([B)V

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/security/a;->e:Lcom/google/android/gms/identity/accounts/security/e;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/identity/accounts/security/e;->b([B)[B

    move-result-object v0

    return-object v0

    .line 82
    :catch_0
    move-exception v0

    .line 83
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "The key didn\'t match the MAC supplied."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;)Lcom/google/android/gms/identity/accounts/api/AccountData;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x3

    .line 171
    invoke-virtual {p1}, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;->c()[B

    move-result-object v0

    .line 172
    invoke-virtual {p1}, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;->b()[B

    move-result-object v2

    .line 173
    new-instance v3, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v3, v0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 175
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/security/a;->d:Lcom/google/android/gms/identity/accounts/security/d;

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/android/gms/identity/accounts/security/a;->b:Ljavax/crypto/SecretKey;

    iget-object v6, p0, Lcom/google/android/gms/identity/accounts/security/a;->a:Ljava/security/SecureRandom;

    invoke-virtual {v0, v4, v5, v3, v6}, Lcom/google/android/gms/identity/accounts/security/d;->a(ILjavax/crypto/SecretKey;Ljavax/crypto/spec/IvParameterSpec;Ljava/security/SecureRandom;)V
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_1

    .line 187
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/security/a;->d:Lcom/google/android/gms/identity/accounts/security/d;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/identity/accounts/security/d;->a([B)[B

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/identity/accounts/api/AccountData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/safeparcel/d;->a([BLandroid/os/Parcelable$Creator;)Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/identity/accounts/api/AccountData;
    :try_end_1
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljavax/crypto/BadPaddingException; {:try_start_1 .. :try_end_1} :catch_3

    .line 197
    :goto_0
    return-object v0

    .line 176
    :catch_0
    move-exception v0

    .line 177
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "The key didn\'t match the cipher supplied."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 178
    :catch_1
    move-exception v0

    .line 180
    const-string v2, "AccountDataUtil"

    invoke-static {v2, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 181
    const-string v2, "AccountDataUtil"

    const-string v3, "Unexpected algorithm parameter exception."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    move-object v0, v1

    .line 184
    goto :goto_0

    .line 191
    :catch_2
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Your cipher algorithm should request unpadding."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 192
    :catch_3
    move-exception v0

    .line 194
    const-string v2, "AccountDataUtil"

    invoke-static {v2, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 195
    const-string v2, "AccountDataUtil"

    const-string v3, "Bad padding."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    move-object v0, v1

    .line 197
    goto :goto_0
.end method
