.class public final Lcom/google/android/gms/playlog/uploader/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/playlog/uploader/a;

.field private final c:Lcom/google/android/gms/playlog/store/f;

.field private final d:Lcom/google/android/gms/playlog/uploader/c;

.field private final e:Lcom/google/android/gms/http/GoogleHttpClient;

.field private final f:Lcom/google/android/d/a;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/playlog/uploader/a;Lcom/google/android/gms/playlog/store/f;Lcom/google/android/gms/playlog/uploader/c;Lcom/google/android/gms/http/GoogleHttpClient;)V
    .locals 1

    .prologue
    .line 171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/playlog/uploader/b;->a:Landroid/content/Context;

    .line 173
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/playlog/uploader/a;

    iput-object v0, p0, Lcom/google/android/gms/playlog/uploader/b;->b:Lcom/google/android/gms/playlog/uploader/a;

    .line 174
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/playlog/store/f;

    iput-object v0, p0, Lcom/google/android/gms/playlog/uploader/b;->c:Lcom/google/android/gms/playlog/store/f;

    .line 175
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/playlog/uploader/c;

    iput-object v0, p0, Lcom/google/android/gms/playlog/uploader/b;->d:Lcom/google/android/gms/playlog/uploader/c;

    .line 176
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/http/GoogleHttpClient;

    iput-object v0, p0, Lcom/google/android/gms/playlog/uploader/b;->e:Lcom/google/android/gms/http/GoogleHttpClient;

    .line 177
    new-instance v0, Lcom/google/android/gms/playlog/uploader/d;

    invoke-direct {v0, p1}, Lcom/google/android/gms/playlog/uploader/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/playlog/uploader/b;->f:Lcom/google/android/d/a;

    .line 178
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 421
    if-nez p1, :cond_0

    .line 422
    const-string v1, "Uploader"

    const-string v2, "No account for auth token provided"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    :goto_0
    return-object v0

    .line 428
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/playlog/uploader/b;->a:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/google/android/gms/common/util/a;->h(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 430
    const-string v1, "Uploader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " no longer exists, so no auth token."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 435
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/playlog/uploader/b;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/playlog/uploader/b;->d:Lcom/google/android/gms/playlog/uploader/c;

    iget-object v2, v2, Lcom/google/android/gms/playlog/uploader/c;->a:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v1, p1, v2, v3}, Lcom/google/android/gms/auth/r;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/af; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    goto :goto_0

    .line 437
    :catch_0
    move-exception v1

    .line 438
    const-string v2, "Uploader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to get auth token: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/auth/af;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 439
    :catch_1
    move-exception v1

    .line 440
    const-string v2, "Uploader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to get auth token: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 441
    :catch_2
    move-exception v1

    .line 442
    const-string v2, "Uploader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to get auth token: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/auth/q;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/playlog/store/h;Lcom/google/af/a/d/a/a/l;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 227
    :try_start_0
    iget-object v2, p1, Lcom/google/android/gms/playlog/store/h;->e:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/gms/playlog/store/h;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, p1, Lcom/google/android/gms/playlog/store/h;->i:Ljava/lang/String;

    const/4 v4, 0x5

    if-eq v0, v4, :cond_0

    const-string v0, "GAMES"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/google/android/gms/playlog/a/e;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/playlog/uploader/b;->d:Lcom/google/android/gms/playlog/uploader/c;

    iget v3, v3, Lcom/google/android/gms/playlog/uploader/c;->b:I

    invoke-direct {p0, v2, p2, v0, v3}, Lcom/google/android/gms/playlog/uploader/b;->a(Ljava/lang/String;Lcom/google/af/a/d/a/a/l;Ljava/lang/String;I)Z

    move-result v0

    .line 237
    :goto_1
    return v0

    .line 227
    :cond_1
    sget-object v0, Lcom/google/android/gms/playlog/a/e;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 232
    :catch_0
    move-exception v0

    .line 233
    const-string v2, "Uploader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Network request failed "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 234
    goto :goto_1

    .line 235
    :catch_1
    move-exception v0

    .line 236
    const-string v2, "Uploader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Network request failed "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 237
    goto :goto_1
.end method

.method private a(Ljava/lang/String;Lcom/google/af/a/d/a/a/l;Ljava/lang/String;I)Z
    .locals 11

    .prologue
    const/16 v10, 0x190

    const/16 v9, 0x12c

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 299
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    new-instance v3, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v3, p3}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 303
    sget-object v0, Lcom/google/android/gms/playlog/a/e;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https"

    invoke-virtual {v3}, Lorg/apache/http/client/methods/HttpPost;->getURI()Ljava/net/URI;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 304
    const-string v0, "Uploader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Abort attempt to upload logs in plaintext: requestUrl="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    :goto_0
    return v2

    .line 308
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/playlog/uploader/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 309
    if-eqz v0, :cond_1

    .line 310
    const-string v4, "Authorization"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "GoogleLogin auth="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :cond_1
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 314
    new-instance v5, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v5, v4}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 315
    invoke-static {p2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v6

    .line 316
    invoke-virtual {v5, v6}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    .line 318
    invoke-virtual {v5}, Ljava/util/zip/GZIPOutputStream;->close()V

    .line 319
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    .line 320
    new-instance v5, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v5, v4}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 322
    const-string v7, "gzip"

    invoke-virtual {v5, v7}, Lorg/apache/http/entity/ByteArrayEntity;->setContentEncoding(Ljava/lang/String;)V

    .line 323
    const-string v7, "application/x-gzip"

    invoke-virtual {v5, v7}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    .line 325
    invoke-virtual {v3, v5}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 327
    invoke-static {}, Lcom/google/android/gms/playlog/uploader/b;->b()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 328
    const-string v5, "Uploader"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Compressed log request from ["

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v6, v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] to ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v4, v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "]"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    :cond_2
    iget-object v4, p0, Lcom/google/android/gms/playlog/uploader/b;->e:Lcom/google/android/gms/http/GoogleHttpClient;

    iget-object v5, p0, Lcom/google/android/gms/playlog/uploader/b;->f:Lcom/google/android/d/a;

    invoke-static {v3, v5}, Lcom/google/android/d/b;->a(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/d/a;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/http/GoogleHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/playlog/uploader/b;->f:Lcom/google/android/d/a;

    invoke-static {v3, v4, v5}, Lcom/google/android/d/b;->a(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;Lcom/google/android/d/a;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 337
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    .line 338
    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    .line 342
    const/16 v6, 0xc8

    if-gt v6, v5, :cond_6

    if-ge v5, v9, :cond_6

    .line 343
    invoke-static {}, Lcom/google/android/gms/playlog/uploader/b;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 344
    const-string v0, "Uploader"

    const-string v2, "Successfully uploaded logs."

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    :cond_3
    sget-object v0, Lcom/google/android/gms/playlog/a/e;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_5

    :try_start_0
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/playlog/c/a;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/af/a/d/a/a/m;->a([B)Lcom/google/af/a/d/a/a/m;

    move-result-object v0

    iget-wide v2, v0, Lcom/google/af/a/d/a/a/m;->a:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_5

    iget-wide v2, v0, Lcom/google/af/a/d/a/a/m;->a:J

    invoke-static {}, Lcom/google/android/gms/playlog/uploader/b;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "Uploader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "LogResponse: wait time in millis = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/playlog/uploader/b;->b:Lcom/google/android/gms/playlog/uploader/a;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/playlog/uploader/a;->a(J)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    :cond_5
    :goto_1
    move v2, v1

    .line 417
    goto/16 :goto_0

    .line 349
    :catch_0
    move-exception v0

    const-string v2, "Uploader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error parsing content: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/protobuf/nano/i;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v2, "Uploader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error getting the content of the response body: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v2, "Uploader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error reading the content of the response body: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 350
    :cond_6
    if-gt v9, v5, :cond_9

    if-ge v5, v10, :cond_9

    .line 351
    if-lez p4, :cond_8

    .line 352
    const-string v0, "Location"

    invoke-interface {v3, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 353
    if-nez v0, :cond_7

    .line 354
    const-string v0, "Uploader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Status "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "... redirect: no location header"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 357
    :cond_7
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 358
    add-int/lit8 v1, p4, -0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/playlog/uploader/b;->a(Ljava/lang/String;Lcom/google/af/a/d/a/a/l;Ljava/lang/String;I)Z

    move-result v1

    goto/16 :goto_1

    .line 362
    :cond_8
    const-string v0, "Uploader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Server returned "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "... redirect, but no more redirects allowed."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 365
    goto/16 :goto_1

    .line 367
    :cond_9
    if-ne v5, v10, :cond_a

    .line 368
    const-string v0, "Uploader"

    const-string v2, "Server returned 400... deleting local malformed logs"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 370
    :cond_a
    const/16 v6, 0x191

    if-ne v5, v6, :cond_b

    .line 371
    const-string v1, "Uploader"

    const-string v3, "Server returned 401... invalidating auth token"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    iget-object v1, p0, Lcom/google/android/gms/playlog/uploader/b;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;)V

    move v1, v2

    .line 373
    goto/16 :goto_1

    .line 374
    :cond_b
    const/16 v0, 0x1f4

    if-ne v5, v0, :cond_c

    .line 375
    const-string v0, "Uploader"

    const-string v1, "Server returned 500... server crashed"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 377
    goto/16 :goto_1

    .line 378
    :cond_c
    const/16 v0, 0x1f5

    if-ne v5, v0, :cond_d

    .line 379
    const-string v0, "Uploader"

    const-string v1, "Server returned 501... service doesn\'t seem to exist"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 381
    goto/16 :goto_1

    .line 382
    :cond_d
    const/16 v0, 0x1f6

    if-ne v5, v0, :cond_e

    .line 383
    const-string v0, "Uploader"

    const-string v1, "Server returned 502... servers are down"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 385
    goto/16 :goto_1

    .line 386
    :cond_e
    const/16 v0, 0x1f7

    if-ne v5, v0, :cond_11

    .line 387
    const-string v0, "Retry-After"

    invoke-interface {v3, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 388
    if-eqz v0, :cond_10

    .line 390
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 392
    :try_start_1
    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 393
    const-string v3, "Uploader"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Server said to retry after "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " seconds"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    iget-object v3, p0, Lcom/google/android/gms/playlog/uploader/b;->b:Lcom/google/android/gms/playlog/uploader/a;

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/playlog/uploader/a;->a(J)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_3

    move v0, v1

    .line 400
    :goto_2
    if-nez v0, :cond_f

    move v0, v1

    :goto_3
    move v1, v0

    .line 401
    goto/16 :goto_1

    .line 397
    :catch_3
    move-exception v3

    const-string v3, "Uploader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown retry value: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    goto :goto_2

    :cond_f
    move v0, v2

    .line 400
    goto :goto_3

    .line 402
    :cond_10
    const-string v0, "Uploader"

    const-string v2, "Status 503 without retry-after header"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 406
    :cond_11
    const/16 v0, 0x1f8

    if-ne v5, v0, :cond_12

    .line 407
    const-string v0, "Uploader"

    const-string v1, "Server returned 504... timeout"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 409
    goto/16 :goto_1

    .line 411
    :cond_12
    const-string v0, "Uploader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected error received from server: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private static b()Z
    .locals 1

    .prologue
    .line 477
    sget-object v0, Lcom/google/android/gms/playlog/a/e;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private static c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 489
    sget-object v0, Lcom/google/android/gms/playlog/a/d;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 11

    .prologue
    .line 181
    const/4 v0, 0x1

    .line 182
    iget-object v1, p0, Lcom/google/android/gms/playlog/uploader/b;->c:Lcom/google/android/gms/playlog/store/f;

    invoke-virtual {v1}, Lcom/google/android/gms/playlog/store/f;->d()Lcom/google/android/gms/playlog/store/e;

    move-result-object v3

    .line 185
    :cond_0
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/playlog/uploader/b;->d:Lcom/google/android/gms/playlog/uploader/c;

    iget-wide v4, v1, Lcom/google/android/gms/playlog/uploader/c;->c:J

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/playlog/store/e;->a(J)Lcom/google/android/gms/playlog/store/b;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 194
    if-eqz v4, :cond_4

    .line 195
    :try_start_1
    iget-object v5, v4, Lcom/google/android/gms/playlog/store/b;->b:Lcom/google/android/gms/playlog/store/h;

    iget-object v1, v4, Lcom/google/android/gms/playlog/store/b;->c:[[B

    new-instance v6, Lcom/google/af/a/d/a/a/l;

    invoke-direct {v6}, Lcom/google/af/a/d/a/a/l;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iput-wide v8, v6, Lcom/google/af/a/d/a/a/l;->a:J

    iget-object v2, p0, Lcom/google/android/gms/playlog/uploader/b;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/common/util/e;->f(Landroid/content/Context;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/4 v8, -0x1

    if-ne v7, v8, :cond_1

    const/4 v2, 0x0

    :cond_1
    iget-object v7, p0, Lcom/google/android/gms/playlog/uploader/b;->a:Landroid/content/Context;

    iget-object v8, v5, Lcom/google/android/gms/playlog/store/h;->g:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    iget-object v9, v5, Lcom/google/android/gms/playlog/store/h;->f:Ljava/lang/String;

    iget-object v10, v5, Lcom/google/android/gms/playlog/store/h;->c:Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-static {v7, v8, v9, v10, v2}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;ZLjava/lang/String;ILjava/lang/Integer;)Lcom/google/af/a/d/a/a/c;

    move-result-object v2

    new-instance v7, Lcom/google/af/a/d/a/a/e;

    invoke-direct {v7}, Lcom/google/af/a/d/a/a/e;-><init>()V

    const/4 v8, 0x4

    iput v8, v7, Lcom/google/af/a/d/a/a/e;->a:I

    iput-object v2, v7, Lcom/google/af/a/d/a/a/e;->b:Lcom/google/af/a/d/a/a/c;

    iput-object v7, v6, Lcom/google/af/a/d/a/a/l;->b:Lcom/google/af/a/d/a/a/e;

    iget-object v2, v5, Lcom/google/android/gms/playlog/store/h;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v6, Lcom/google/af/a/d/a/a/l;->c:I

    iget-object v2, v5, Lcom/google/android/gms/playlog/store/h;->i:Ljava/lang/String;

    if-nez v2, :cond_5

    const-string v2, ""

    :goto_1
    iput-object v2, v6, Lcom/google/af/a/d/a/a/l;->d:Ljava/lang/String;

    iget-object v2, v5, Lcom/google/android/gms/playlog/store/h;->h:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, v5, Lcom/google/android/gms/playlog/store/h;->h:Ljava/lang/String;

    iput-object v2, v6, Lcom/google/af/a/d/a/a/l;->e:Ljava/lang/String;

    :cond_2
    invoke-static {}, Lcom/google/android/gms/playlog/uploader/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {}, Lcom/google/android/gms/playlog/uploader/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/gms/playlog/b/a;->a(Ljava/lang/String;[[B)[[B

    move-result-object v1

    :cond_3
    iput-object v1, v6, Lcom/google/af/a/d/a/a/l;->g:[[B

    .line 199
    iget-object v1, v4, Lcom/google/android/gms/playlog/store/b;->b:Lcom/google/android/gms/playlog/store/h;

    invoke-direct {p0, v1, v6}, Lcom/google/android/gms/playlog/uploader/b;->a(Lcom/google/android/gms/playlog/store/h;Lcom/google/af/a/d/a/a/l;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 200
    if-eqz v1, :cond_6

    .line 202
    :try_start_2
    iget-object v1, v4, Lcom/google/android/gms/playlog/store/b;->a:Lcom/google/android/gms/playlog/store/d;

    invoke-virtual {v1}, Lcom/google/android/gms/playlog/store/d;->a()I
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    :try_start_3
    iget-object v2, v4, Lcom/google/android/gms/playlog/store/b;->c:[[B

    array-length v2, v2

    if-eq v1, v2, :cond_0

    const-string v2, "LogEventSet"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could not delete all rows: numDeleted="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " != numRows="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, v4, Lcom/google/android/gms/playlog/store/b;->c:[[B

    array-length v4, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 203
    :catch_0
    move-exception v1

    .line 206
    :try_start_4
    const-string v2, "Uploader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not delete logs: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 216
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/android/gms/playlog/store/e;->close()V

    throw v0

    .line 188
    :catch_1
    move-exception v1

    .line 191
    :try_start_5
    const-string v2, "Uploader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not read logs: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 212
    :cond_4
    :goto_2
    invoke-virtual {v3}, Lcom/google/android/gms/playlog/store/e;->close()V

    .line 218
    return v0

    .line 195
    :cond_5
    :try_start_6
    iget-object v2, v5, Lcom/google/android/gms/playlog/store/h;->i:Ljava/lang/String;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_1

    .line 202
    :catch_2
    move-exception v1

    :try_start_7
    new-instance v2, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not delete logs: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 211
    :cond_6
    const/4 v0, 0x0

    goto :goto_2
.end method
