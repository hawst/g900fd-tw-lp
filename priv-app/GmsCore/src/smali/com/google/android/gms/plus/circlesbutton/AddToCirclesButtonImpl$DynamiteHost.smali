.class public Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl$DynamiteHost;
.super Lcom/google/android/gms/plus/d/b;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/DynamiteApi;
.end annotation

.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private a:Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 449
    invoke-direct {p0}, Lcom/google/android/gms/plus/d/b;-><init>()V

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 505
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "call initialize() first"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 506
    return-void

    .line 505
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/b/l;
    .locals 1

    .prologue
    .line 470
    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl$DynamiteHost;->c()V

    .line 471
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 476
    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl$DynamiteHost;->c()V

    .line 477
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->a(IZ)V

    .line 478
    return-void
.end method

.method public final a(Lcom/google/android/gms/b/l;Lcom/google/android/gms/b/l;Lcom/google/android/gms/b/l;)V
    .locals 4

    .prologue
    .line 455
    new-instance v3, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;

    invoke-static {p1}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p3}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/AttributeSet;

    invoke-direct {v3, v0, v1, v2}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;-><init>(Landroid/content/Context;Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;

    .line 459
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 500
    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl$DynamiteHost;->c()V

    .line 501
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->a(Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;Ljava/lang/String;I)V

    .line 502
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/people/data/AudienceMember;Ljava/lang/String;Lcom/google/android/gms/plus/d/c;)V
    .locals 6

    .prologue
    .line 464
    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl$DynamiteHost;->c()V

    .line 465
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/people/data/AudienceMember;Ljava/lang/String;Lcom/google/android/gms/plus/d/c;)V

    .line 466
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 494
    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl$DynamiteHost;->c()V

    .line 495
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->a(Z)Z

    .line 496
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 488
    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl$DynamiteHost;->c()V

    .line 489
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;

    invoke-static {v0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->b(Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;)V

    .line 490
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 482
    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl$DynamiteHost;->c()V

    .line 483
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;

    invoke-static {v0, p1}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->a(Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;I)V

    .line 484
    return-void
.end method
