.class final Lcom/google/android/gms/wallet/common/ui/e;
.super Lcom/google/android/gms/wallet/service/l;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V
    .locals 0

    .prologue
    .line 452
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 544
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(ILandroid/content/Intent;)V

    .line 545
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 489
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->c(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V

    .line 493
    iget-object v1, p1, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    array-length v1, v1

    if-lez v1, :cond_2

    .line 494
    iget-object v5, p1, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    array-length v6, v5

    move v4, v0

    move v1, v0

    move v3, v0

    :goto_0
    if-ge v4, v6, :cond_1

    aget v0, v5, v4

    .line 495
    packed-switch v0, :pswitch_data_0

    .line 525
    :pswitch_0
    const-string v1, "AddInstrumentActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Unexpected error code: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    move v1, v3

    .line 494
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v1

    move v1, v0

    goto :goto_0

    .line 498
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/cb;

    instance-of v0, v0, Lcom/google/android/gms/wallet/common/ui/f;

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/cb;

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/f;->b()V

    .line 502
    :cond_0
    if-nez v3, :cond_4

    .line 503
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/cb;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/cb;->g()Z

    move v0, v1

    move v1, v2

    .line 504
    goto :goto_1

    .line 508
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    sget v7, Lcom/google/android/gms/p;->Bo:I

    invoke-static {v0, v7}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;I)V

    .line 511
    if-nez v3, :cond_4

    .line 512
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    move v0, v1

    move v1, v2

    .line 513
    goto :goto_1

    .line 517
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    sget v7, Lcom/google/android/gms/p;->Bn:I

    invoke-static {v0, v7}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;I)V

    .line 519
    if-nez v3, :cond_4

    .line 520
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    move v0, v1

    move v1, v2

    .line 521
    goto :goto_1

    .line 530
    :cond_1
    if-eqz v1, :cond_3

    .line 531
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->d(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V

    .line 540
    :goto_2
    return-void

    .line 535
    :cond_2
    const-string v0, "AddInstrumentActivity"

    const-string v1, "Unexpected create instrument response."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->d(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V

    goto :goto_2

    .line 539
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V

    goto :goto_2

    :cond_4
    move v0, v1

    move v1, v3

    goto :goto_1

    .line 495
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/google/checkout/inapp/proto/j;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 3

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.disallowedCardCategories"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v0

    .line 462
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p1, Lcom/google/checkout/inapp/proto/j;->l:I

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/wallet/common/w;->a(Landroid/content/res/Resources;I[I)Ljava/lang/String;

    move-result-object v1

    .line 464
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/cb;

    instance-of v0, v0, Lcom/google/android/gms/wallet/common/ui/f;

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/cb;

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/f;->a(Ljava/lang/String;)V

    .line 468
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/cb;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/cb;->g()Z

    .line 469
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V

    .line 484
    :goto_0
    return-void

    .line 473
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 474
    const-string v1, "com.google.android.gms.wallet.instrument"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 476
    const-string v1, "com.google.android.gms.wallet.instrumentId"

    iget-object v2, p1, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 480
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->b(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 481
    const-string v1, "serverResponse"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 483
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 549
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(ILandroid/content/Intent;)V

    .line 550
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 554
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->e(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V

    .line 555
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/e;->a:Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->d(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V

    .line 560
    return-void
.end method
