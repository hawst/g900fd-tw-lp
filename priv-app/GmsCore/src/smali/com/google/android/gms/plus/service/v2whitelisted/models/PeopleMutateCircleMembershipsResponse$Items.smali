.class public final Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/x;

.field private static final f:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items$CircleMemberId;

.field d:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

.field e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 109
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/x;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/x;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/x;

    .line 126
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 129
    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->f:Ljava/util/HashMap;

    const-string v1, "circleMemberId"

    const-string v2, "circleMemberId"

    const/4 v3, 0x2

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items$CircleMemberId;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->f:Ljava/util/HashMap;

    const-string v1, "person"

    const-string v2, "person"

    const/4 v3, 0x3

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->f:Ljava/util/HashMap;

    const-string v1, "result"

    const-string v2, "result"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 170
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 171
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->b:I

    .line 172
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->a:Ljava/util/Set;

    .line 173
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items$CircleMemberId;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 183
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->a:Ljava/util/Set;

    .line 184
    iput p2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->b:I

    .line 185
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->c:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items$CircleMemberId;

    .line 186
    iput-object p4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    .line 187
    iput-object p5, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->e:Ljava/lang/String;

    .line 188
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->f:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 4

    .prologue
    .line 537
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 538
    packed-switch v0, :pswitch_data_0

    .line 546
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 540
    :pswitch_0
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items$CircleMemberId;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->c:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items$CircleMemberId;

    .line 550
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 551
    return-void

    .line 543
    :pswitch_1
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    goto :goto_0

    .line 538
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 522
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 523
    packed-switch v0, :pswitch_data_0

    .line 528
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 525
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->e:Ljava/lang/String;

    .line 531
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 532
    return-void

    .line 523
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 489
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    return-object v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 494
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 502
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 496
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->c:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items$CircleMemberId;

    .line 500
    :goto_0
    return-object v0

    .line 498
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    goto :goto_0

    .line 500
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->e:Ljava/lang/String;

    goto :goto_0

    .line 494
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->a:Ljava/util/Set;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 479
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/x;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 568
    instance-of v0, p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;

    if-nez v0, :cond_0

    move v0, v1

    .line 599
    :goto_0
    return v0

    .line 573
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 574
    goto :goto_0

    .line 577
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;

    .line 578
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 579
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 580
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 582
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 584
    goto :goto_0

    :cond_3
    move v0, v1

    .line 589
    goto :goto_0

    .line 592
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 594
    goto :goto_0

    :cond_5
    move v0, v2

    .line 599
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 555
    const/4 v0, 0x0

    .line 556
    sget-object v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->f:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 557
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 558
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 559
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 561
    goto :goto_0

    .line 562
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 484
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/x;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/x;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;Landroid/os/Parcel;I)V

    .line 485
    return-void
.end method
