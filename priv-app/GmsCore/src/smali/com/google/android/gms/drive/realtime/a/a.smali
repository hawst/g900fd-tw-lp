.class public final Lcom/google/android/gms/drive/realtime/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/c/a/a/b/b/a/b;Ljava/lang/Iterable;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/drive/realtime/a/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/realtime/a/b;-><init>(Lcom/google/c/a/a/b/b/a/b;)V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/realtime/a/b;->a(Ljava/lang/Iterable;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/data/DataHolder;ILcom/google/c/a/a/b/b/a/b;)Lcom/google/c/a/a/b/b/a/r;
    .locals 4

    .prologue
    .line 86
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v0

    .line 87
    const-string v1, "valueType"

    invoke-virtual {p0, v1, p1, v0}, Lcom/google/android/gms/common/data/DataHolder;->b(Ljava/lang/String;II)I

    move-result v1

    .line 88
    packed-switch v1, :pswitch_data_0

    .line 99
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown object type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :pswitch_0
    const/4 v0, 0x0

    .line 97
    :goto_0
    return-object v0

    .line 92
    :pswitch_1
    const-string v1, "value"

    invoke-virtual {p0, v1, p1, v0}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    .line 94
    invoke-static {v0}, Lcom/google/c/a/a/b/b/a/n;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/n;

    move-result-object v0

    goto :goto_0

    .line 96
    :pswitch_2
    const-string v1, "value"

    invoke-virtual {p0, v1, p1, v0}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    .line 97
    invoke-interface {p2, v0}, Lcom/google/c/a/a/b/b/a/b;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/g;->e()Lcom/google/c/a/a/b/b/a/p;

    move-result-object v0

    goto :goto_0

    .line 88
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Landroid/database/CursorWindow;ILjava/lang/Object;Lcom/google/c/a/a/b/b/a/b;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 52
    if-nez p2, :cond_0

    .line 54
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1, p1, v4}, Landroid/database/CursorWindow;->putLong(JII)Z

    move-result v0

    or-int/lit8 v0, v0, 0x1

    .line 76
    :goto_0
    return v0

    .line 56
    :cond_0
    instance-of v0, p2, Lcom/google/c/a/a/b/b/a/n;

    if-eqz v0, :cond_1

    .line 57
    const-wide/16 v0, 0x2

    invoke-virtual {p0, v0, v1, p1, v4}, Landroid/database/CursorWindow;->putLong(JII)Z

    move-result v0

    or-int/lit8 v0, v0, 0x1

    .line 59
    check-cast p2, Lcom/google/c/a/a/b/b/a/n;

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/n;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p1, v5}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    move-result v1

    or-int/2addr v0, v1

    goto :goto_0

    .line 61
    :cond_1
    instance-of v0, p2, Lcom/google/c/a/a/b/b/a/p;

    if-eqz v0, :cond_2

    .line 62
    check-cast p2, Lcom/google/c/a/a/b/b/a/p;

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/p;->a()Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-interface {p3, v0}, Lcom/google/c/a/a/b/b/a/b;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v0

    .line 64
    const-wide/16 v2, 0x1

    invoke-virtual {p0, v2, v3, p1, v4}, Landroid/database/CursorWindow;->putLong(JII)Z

    move-result v1

    or-int/lit8 v1, v1, 0x1

    .line 66
    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/g;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, p1, v5}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    move-result v2

    or-int/2addr v1, v2

    .line 67
    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/g;->f()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {p0, v0, p1, v2}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    move-result v0

    or-int/2addr v0, v1

    .line 69
    goto :goto_0

    :cond_2
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 70
    const-wide/16 v0, 0x3

    invoke-virtual {p0, v0, v1, p1, v4}, Landroid/database/CursorWindow;->putLong(JII)Z

    move-result v0

    or-int/lit8 v0, v0, 0x1

    .line 72
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p2, p1, v5}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    move-result v1

    or-int/2addr v0, v1

    goto :goto_0

    .line 74
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown value type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
