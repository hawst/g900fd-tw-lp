.class public final Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;
.super Lcom/google/android/gms/common/server/response/FastJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/lso/c;

.field private static final a:Ljava/util/HashMap;


# instance fields
.field private final b:Ljava/util/Set;

.field private final c:I

.field private d:Ljava/util/List;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 450
    new-instance v0, Lcom/google/android/gms/plus/service/lso/c;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/lso/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->CREATOR:Lcom/google/android/gms/plus/service/lso/c;

    .line 467
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 470
    sput-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->a:Ljava/util/HashMap;

    const-string v1, "clients"

    const-string v2, "clients"

    const/4 v3, 0x2

    const-class v4, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 472
    sget-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->a:Ljava/util/HashMap;

    const-string v1, "display_name"

    const-string v2, "display_name"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 473
    sget-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->a:Ljava/util/HashMap;

    const-string v1, "icon_url"

    const-string v2, "icon_url"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 474
    sget-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->a:Ljava/util/HashMap;

    const-string v1, "project_id"

    const-string v2, "project_id"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    sget-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->a:Ljava/util/HashMap;

    const-string v1, "revocation_handle"

    const-string v2, "revocation_handle"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 476
    sget-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->a:Ljava/util/HashMap;

    const-string v1, "scope_ids"

    const-string v2, "scope_ids"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 477
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 514
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 515
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->c:I

    .line 516
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->b:Ljava/util/Set;

    .line 517
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 529
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 530
    iput-object p1, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->b:Ljava/util/Set;

    .line 531
    iput p2, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->c:I

    .line 532
    iput-object p3, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->d:Ljava/util/List;

    .line 533
    iput-object p4, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->e:Ljava/lang/String;

    .line 534
    iput-object p5, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->f:Ljava/lang/String;

    .line 535
    iput-object p6, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->g:Ljava/lang/String;

    .line 536
    iput-object p7, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->h:Ljava/lang/String;

    .line 537
    iput-object p8, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->i:Ljava/util/List;

    .line 538
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 481
    sget-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->a:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1085
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 1086
    packed-switch v0, :pswitch_data_0

    .line 1100
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1088
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->e:Ljava/lang/String;

    .line 1103
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->b:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1104
    return-void

    .line 1091
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->f:Ljava/lang/String;

    goto :goto_0

    .line 1094
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->g:Ljava/lang/String;

    goto :goto_0

    .line 1097
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->h:Ljava/lang/String;

    goto :goto_0

    .line 1086
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 1109
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 1110
    packed-switch v0, :pswitch_data_0

    .line 1115
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known array of custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1112
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->d:Ljava/util/List;

    .line 1119
    iget-object v1, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->b:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1120
    return-void

    .line 1110
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 1020
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->b:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1025
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1039
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1027
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->d:Ljava/util/List;

    .line 1037
    :goto_0
    return-object v0

    .line 1029
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->e:Ljava/lang/String;

    goto :goto_0

    .line 1031
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->f:Ljava/lang/String;

    goto :goto_0

    .line 1033
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->g:Ljava/lang/String;

    goto :goto_0

    .line 1035
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->h:Ljava/lang/String;

    goto :goto_0

    .line 1037
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->i:Ljava/util/List;

    goto :goto_0

    .line 1025
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected final b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1010
    const/4 v0, 0x0

    return-object v0
.end method

.method final b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 564
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->b:Ljava/util/Set;

    return-object v0
.end method

.method final c()I
    .locals 1

    .prologue
    .line 571
    iget v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->c:I

    return v0
.end method

.method protected final c(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 1070
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 1071
    packed-switch v0, :pswitch_data_0

    .line 1076
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be an array of ints."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1073
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->i:Ljava/util/List;

    .line 1079
    iget-object v1, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->b:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1080
    return-void

    .line 1071
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
    .end packed-switch
.end method

.method protected final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1015
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Ljava/util/List;
    .locals 1

    .prologue
    .line 576
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->d:Ljava/util/List;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 999
    sget-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->CREATOR:Lcom/google/android/gms/plus/service/lso/c;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 588
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1137
    instance-of v0, p1, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;

    if-nez v0, :cond_0

    move v0, v1

    .line 1168
    :goto_0
    return v0

    .line 1142
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 1143
    goto :goto_0

    .line 1146
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;

    .line 1147
    sget-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 1148
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1149
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1151
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 1153
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1158
    goto :goto_0

    .line 1161
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 1163
    goto :goto_0

    :cond_5
    move v0, v2

    .line 1168
    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 600
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 612
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 624
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1124
    const/4 v0, 0x0

    .line 1125
    sget-object v1, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->a:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 1126
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1127
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 1128
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 1130
    goto :goto_0

    .line 1131
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final k()Ljava/util/List;
    .locals 1

    .prologue
    .line 636
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->i:Ljava/util/List;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1004
    sget-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->CREATOR:Lcom/google/android/gms/plus/service/lso/c;

    invoke-static {p0, p1}, Lcom/google/android/gms/plus/service/lso/c;->a(Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;Landroid/os/Parcel;)V

    .line 1005
    return-void
.end method
