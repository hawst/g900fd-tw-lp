.class public abstract Lcom/google/android/gms/games/ui/common/leaderboards/g;
.super Lcom/google/android/gms/games/ui/s;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;
.implements Lcom/google/android/gms/games/ui/common/leaderboards/b;


# instance fields
.field private l:Lcom/google/android/gms/games/ui/common/leaderboards/h;

.field private m:Lcom/google/android/gms/games/ui/common/leaderboards/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/s;-><init>()V

    return-void
.end method


# virtual methods
.method public final B()V
    .locals 3

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/g;->b()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 117
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->d:Lcom/google/android/gms/games/ui/q;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    const-string v0, "LeaderboardFrag"

    const-string v1, "onRetry: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    :goto_0
    return-void

    .line 123
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v1

    .line 124
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/bh;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 125
    sget-object v1, Lcom/google/android/gms/games/d;->j:Lcom/google/android/gms/games/e/n;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/e/n;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 132
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->h:Lcom/google/android/gms/games/ui/d/p;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/d/p;->b(I)V

    goto :goto_0

    .line 128
    :cond_1
    sget-object v2, Lcom/google/android/gms/games/d;->j:Lcom/google/android/gms/games/e/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/bh;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Lcom/google/android/gms/games/e/n;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;)I
    .locals 3

    .prologue
    .line 138
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 139
    sget v1, Lcom/google/android/gms/g;->S:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sget v2, Lcom/google/android/gms/g;->R:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sub-int v0, v1, v0

    .line 142
    return v0
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 5

    .prologue
    .line 26
    check-cast p1, Lcom/google/android/gms/games/e/o;

    invoke-interface {p1}, Lcom/google/android/gms/games/e/o;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/gms/games/e/o;->c()Lcom/google/android/gms/games/e/b;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/g;->d()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/games/e/b;->w_()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/ui/q;->b(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/games/e/b;->w_()V

    goto :goto_0

    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->m:Lcom/google/android/gms/games/ui/common/leaderboards/a;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/a;->a(Lcom/google/android/gms/common/data/d;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->h:Lcom/google/android/gms/games/ui/d/p;

    invoke-virtual {v1}, Lcom/google/android/gms/games/e/b;->c()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/games/ui/d/p;->a(IIZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/e/b;->w_()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    sget-object v0, Lcom/google/android/gms/games/d;->j:Lcom/google/android/gms/games/e/n;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/e/n;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 73
    :goto_0
    return-void

    .line 69
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->j:Lcom/google/android/gms/games/e/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->e()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Lcom/google/android/gms/games/e/n;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method protected abstract a(Lcom/google/android/gms/games/e/a;)V
.end method

.method public final b(Lcom/google/android/gms/games/e/a;)V
    .locals 0

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/common/leaderboards/g;->a(Lcom/google/android/gms/games/e/a;)V

    .line 106
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/s;->onActivityCreated(Landroid/os/Bundle;)V

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->a()Z

    move-result v1

    .line 41
    if-eqz v1, :cond_2

    sget v0, Lcom/google/android/gms/h;->ay:I

    :goto_0
    sget v2, Lcom/google/android/gms/p;->jw:I

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/games/ui/common/leaderboards/g;->a(III)V

    .line 44
    if-eqz v1, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 46
    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget v2, Lcom/google/android/gms/f;->W:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/g;->a(II)V

    .line 50
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/common/leaderboards/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->d:Lcom/google/android/gms/games/ui/q;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/common/leaderboards/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/leaderboards/b;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->m:Lcom/google/android/gms/games/ui/common/leaderboards/a;

    .line 52
    new-instance v0, Lcom/google/android/gms/games/ui/by;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/by;-><init>()V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/g;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 54
    new-instance v1, Lcom/google/android/gms/games/ui/common/leaderboards/h;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->d:Lcom/google/android/gms/games/ui/q;

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/ui/common/leaderboards/h;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->l:Lcom/google/android/gms/games/ui/common/leaderboards/h;

    .line 55
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->l:Lcom/google/android/gms/games/ui/common/leaderboards/h;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 57
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/g;->m:Lcom/google/android/gms/games/ui/common/leaderboards/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 59
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/by;->a()Lcom/google/android/gms/games/ui/bw;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/g;->a(Landroid/support/v7/widget/bv;)V

    .line 60
    return-void

    .line 41
    :cond_2
    sget v0, Lcom/google/android/gms/h;->ax:I

    goto :goto_0
.end method
