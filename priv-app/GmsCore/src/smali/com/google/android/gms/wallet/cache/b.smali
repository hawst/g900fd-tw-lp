.class public final Lcom/google/android/gms/wallet/cache/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/cache/e;
.implements Ljava/lang/Runnable;


# instance fields
.field final a:I

.field final b:I

.field private final c:Lcom/google/android/gms/wallet/cache/CacheUpdateService;

.field private final d:Ljava/lang/String;

.field private e:Lcom/google/android/gms/wallet/cache/j;

.field private f:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/cache/CacheUpdateService;Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/cache/b;->f:I

    .line 43
    iput-object p1, p0, Lcom/google/android/gms/wallet/cache/b;->c:Lcom/google/android/gms/wallet/cache/CacheUpdateService;

    .line 44
    iput p3, p0, Lcom/google/android/gms/wallet/cache/b;->b:I

    .line 45
    const-string v0, "com.google.android.gms.wallet.cache.CacheUpdateConstants.EXTRA_TRIGGER"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/cache/b;->a:I

    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/gms/wallet/cache/b;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/cache/b;->d:Ljava/lang/String;

    .line 47
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 131
    iget v0, p0, Lcom/google/android/gms/wallet/cache/b;->f:I

    if-nez v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/b;->c:Lcom/google/android/gms/wallet/cache/CacheUpdateService;

    iget v1, p0, Lcom/google/android/gms/wallet/cache/b;->b:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->a(I)V

    .line 134
    :cond_0
    return-void
.end method

.method private a([Landroid/accounts/Account;)V
    .locals 10

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/b;->e:Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/cache/j;->b([Landroid/accounts/Account;)Ljava/util/ArrayList;

    move-result-object v8

    .line 94
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/b;->c:Lcom/google/android/gms/wallet/cache/CacheUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/common/d/b;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/cache/b;->f:I

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/b;->c:Lcom/google/android/gms/wallet/cache/CacheUpdateService;

    invoke-static {}, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->a()Lcom/android/volley/s;

    move-result-object v2

    .line 98
    const/4 v0, 0x0

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v7, v0

    :goto_0
    if-ge v7, v9, :cond_1

    .line 99
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/util/Pair;

    .line 100
    new-instance v0, Lcom/google/android/gms/wallet/cache/c;

    iget-object v1, p0, Lcom/google/android/gms/wallet/cache/b;->c:Lcom/google/android/gms/wallet/cache/CacheUpdateService;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Landroid/accounts/Account;

    iget-object v4, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v6

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/wallet/cache/c;-><init>(Landroid/content/Context;Lcom/android/volley/s;Landroid/accounts/Account;ILcom/google/android/gms/wallet/cache/e;Landroid/os/Looper;)V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/cache/c;->run()V

    .line 98
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 104
    :cond_0
    const-string v0, "CacheUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/wallet/cache/b;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "no network connection, retry later..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/b;->c:Lcom/google/android/gms/wallet/cache/CacheUpdateService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/receiver/ConnectivityChangeBroadcastReceiver;->a(Landroid/content/Context;Z)V

    .line 108
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;IILcom/google/checkout/inapp/proto/ai;)V
    .locals 3

    .prologue
    .line 113
    packed-switch p3, :pswitch_data_0

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/b;->e:Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/cache/j;->c(Landroid/accounts/Account;I)V

    .line 125
    :goto_0
    iget v0, p0, Lcom/google/android/gms/wallet/cache/b;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/cache/b;->f:I

    .line 126
    const-string v0, "CacheUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/wallet/cache/b;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "task finished, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/wallet/cache/b;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "remain"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    invoke-direct {p0}, Lcom/google/android/gms/wallet/cache/b;->a()V

    .line 128
    return-void

    .line 115
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/b;->e:Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual {v0, p1, p2, p4}, Lcom/google/android/gms/wallet/cache/j;->a(Landroid/accounts/Account;ILcom/google/checkout/inapp/proto/ai;)V

    goto :goto_0

    .line 119
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/b;->c:Lcom/google/android/gms/wallet/cache/CacheUpdateService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/receiver/ConnectivityChangeBroadcastReceiver;->a(Landroid/content/Context;Z)V

    goto :goto_0

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final run()V
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 51
    const-string v2, "CacheUpdater"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/gms/wallet/cache/b;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "CacheUpdater started"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    iget-object v2, p0, Lcom/google/android/gms/wallet/cache/b;->c:Lcom/google/android/gms/wallet/cache/CacheUpdateService;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->b()Lcom/google/android/gms/wallet/cache/j;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/wallet/cache/b;->e:Lcom/google/android/gms/wallet/cache/j;

    .line 53
    iget-object v2, p0, Lcom/google/android/gms/wallet/cache/b;->c:Lcom/google/android/gms/wallet/cache/CacheUpdateService;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->c()Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "com.google"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    .line 56
    iget v2, p0, Lcom/google/android/gms/wallet/cache/b;->a:I

    const/4 v4, 0x2

    if-ne v2, v4, :cond_1

    .line 57
    iget-object v2, p0, Lcom/google/android/gms/wallet/cache/b;->e:Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wallet/cache/j;->a([Landroid/accounts/Account;)V

    .line 58
    iget-object v2, p0, Lcom/google/android/gms/wallet/cache/b;->c:Lcom/google/android/gms/wallet/cache/CacheUpdateService;

    invoke-static {v2, v3}, Lcom/google/android/gms/wallet/cache/h;->a(Landroid/content/Context;[Landroid/accounts/Account;)V

    .line 60
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 61
    iget-object v6, p0, Lcom/google/android/gms/wallet/cache/b;->e:Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual {v6, v5, v0}, Lcom/google/android/gms/wallet/cache/j;->b(Landroid/accounts/Account;I)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-nez v5, :cond_0

    .line 67
    :goto_1
    if-nez v0, :cond_1

    .line 68
    invoke-direct {p0}, Lcom/google/android/gms/wallet/cache/b;->a()V

    .line 86
    :goto_2
    return-void

    .line 60
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/b;->c:Lcom/google/android/gms/wallet/cache/CacheUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/b;->c:Lcom/google/android/gms/wallet/cache/CacheUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/wallet/receiver/PackageAddedBroadcastReceiver;->a(Landroid/content/Context;)V

    .line 76
    const-string v0, "US"

    iget-object v1, p0, Lcom/google/android/gms/wallet/cache/b;->c:Lcom/google/android/gms/wallet/cache/CacheUpdateService;

    invoke-static {v1}, Lcom/google/android/gms/common/util/ag;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 77
    invoke-direct {p0, v3}, Lcom/google/android/gms/wallet/cache/b;->a([Landroid/accounts/Account;)V

    .line 85
    :goto_3
    invoke-direct {p0}, Lcom/google/android/gms/wallet/cache/b;->a()V

    goto :goto_2

    .line 79
    :cond_2
    const-string v0, "CacheUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/wallet/cache/b;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "wallet probably not supported at location"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 82
    :cond_3
    const-string v0, "CacheUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/wallet/cache/b;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "no app requested wallet prefetch"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/b;->c:Lcom/google/android/gms/wallet/cache/CacheUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/wallet/receiver/PackageAddedBroadcastReceiver;->a(Landroid/content/Context;)V

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_1
.end method
