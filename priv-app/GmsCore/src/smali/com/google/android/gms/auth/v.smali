.class public final Lcom/google/android/gms/auth/v;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/auth/u;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/auth/t;)V
    .locals 2

    .prologue
    .line 487
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 488
    new-instance v0, Lcom/google/android/gms/auth/u;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/u;-><init>(B)V

    iput-object v0, p0, Lcom/google/android/gms/auth/v;->a:Lcom/google/android/gms/auth/u;

    .line 489
    iget-object v0, p0, Lcom/google/android/gms/auth/v;->a:Lcom/google/android/gms/auth/u;

    iget-object v1, p1, Lcom/google/android/gms/auth/t;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    iput v1, v0, Lcom/google/android/gms/auth/u;->b:I

    .line 490
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/auth/t;B)V
    .locals 0

    .prologue
    .line 483
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/v;-><init>(Lcom/google/android/gms/auth/t;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/auth/x;)Lcom/google/android/gms/auth/u;
    .locals 2

    .prologue
    .line 508
    iget-object v0, p0, Lcom/google/android/gms/auth/v;->a:Lcom/google/android/gms/auth/u;

    iput-object p1, v0, Lcom/google/android/gms/auth/u;->d:Lcom/google/android/gms/auth/x;

    .line 509
    iget-object v0, p0, Lcom/google/android/gms/auth/v;->a:Lcom/google/android/gms/auth/u;

    iget-object v1, v0, Lcom/google/android/gms/auth/u;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget v1, v0, Lcom/google/android/gms/auth/u;->b:I

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/auth/u;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/auth/u;->d:Lcom/google/android/gms/auth/x;

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lcom/google/android/gms/auth/w;

    invoke-direct {v0}, Lcom/google/android/gms/auth/w;-><init>()V

    throw v0

    .line 510
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/v;->a:Lcom/google/android/gms/auth/u;

    return-object v0
.end method
