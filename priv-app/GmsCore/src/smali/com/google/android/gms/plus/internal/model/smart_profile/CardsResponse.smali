.class public Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/internal/model/smart_profile/b;


# instance fields
.field private final a:I

.field private final b:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/plus/internal/model/smart_profile/b;

    invoke-direct {v0}, Lcom/google/android/gms/plus/internal/model/smart_profile/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;->CREATOR:Lcom/google/android/gms/plus/internal/model/smart_profile/b;

    return-void
.end method

.method public constructor <init>(I[B)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput p1, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;->a:I

    .line 35
    iput-object p2, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;->b:[B

    .line 36
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;-><init>(I[B)V

    .line 40
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;->a:I

    return v0
.end method

.method public final b()[B
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;->b:[B

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 72
    instance-of v1, p1, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;

    if-nez v1, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v0

    .line 75
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;

    .line 76
    iget v1, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;->a:I

    iget v2, p1, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;->a:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;->b:[B

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;->b:[B

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 67
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;->b:[B

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 55
    const-string v1, "%s<bytes=%s>"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    const-class v3, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;->b:[B

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;->b:[B

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 62
    invoke-static {p0, p1}, Lcom/google/android/gms/plus/internal/model/smart_profile/b;->a(Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;Landroid/os/Parcel;)V

    .line 63
    return-void
.end method
