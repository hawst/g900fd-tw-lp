.class public Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

.field private c:Lcom/google/android/gms/googlehelp/common/k;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private a(Z)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 111
    if-eqz p1, :cond_0

    .line 112
    invoke-virtual {v0, p0}, Landroid/support/v4/app/aj;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 116
    :goto_0
    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 117
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->b()I

    .line 121
    :goto_1
    return-void

    .line 114
    :cond_0
    invoke-virtual {v0, p0}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    goto :goto_0

    .line 119
    :cond_1
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->G()Ljava/lang/String;

    move-result-object v1

    .line 68
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->c:Lcom/google/android/gms/googlehelp/common/k;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->c:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/k;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 88
    :goto_0
    return-void

    .line 73
    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->c:Lcom/google/android/gms/googlehelp/common/k;

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->c:Lcom/google/android/gms/googlehelp/common/k;

    if-nez v0, :cond_2

    .line 78
    invoke-direct {p0, v4}, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->a(Z)V

    goto :goto_0

    .line 73
    :cond_1
    invoke-static {v1, v0}, Lcom/google/android/gms/googlehelp/common/k;->a(Ljava/lang/String;Lcom/google/android/gms/googlehelp/common/HelpConfig;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    goto :goto_1

    .line 80
    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->a(Z)V

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/view/View;Landroid/view/View$OnClickListener;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    const-string v1, "SHOWN_BROWSE_ALL_ARTICLES"

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->c:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/k;->n()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-static {v0, v1, v2, v4, v3}, Lcom/google/android/gms/googlehelp/f/l;->a(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->c:Lcom/google/android/gms/googlehelp/common/k;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->c:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/k;->a(Z)V

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->c:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/k;->b(Z)V

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->c:Lcom/google/android/gms/googlehelp/common/k;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/k;->d(Ljava/lang/String;)V

    .line 106
    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    .line 61
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->a()V

    .line 62
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 92
    new-instance v0, Lcom/google/android/gms/googlehelp/f/f;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->c:Lcom/google/android/gms/googlehelp/common/k;

    const-string v3, "BROWSE_ALL_ARTICLES_CLICKED"

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/googlehelp/f/f;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Lcom/google/android/gms/googlehelp/common/k;Ljava/lang/String;ILjava/util/Calendar;)V

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/f/f;->a([Ljava/lang/Object;)V

    .line 97
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 51
    sget v0, Lcom/google/android/gms/l;->cb:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 52
    sget v0, Lcom/google/android/gms/j;->hF:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->a:Landroid/widget/TextView;

    .line 53
    return-object v1
.end method
