.class public final Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/gencode/authzen/server/api/aw;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/ay;

.field private static final i:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:I

.field e:Ljava/lang/String;

.field f:I

.field g:Ljava/util/List;

.field h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ay;

    invoke-direct {v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/ay;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/ay;

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 66
    sput-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->i:Ljava/util/HashMap;

    const-string v1, "applicationId"

    const-string v2, "applicationId"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->i:Ljava/util/HashMap;

    const-string v1, "invocationReason"

    const-string v2, "invocationReason"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->i:Ljava/util/HashMap;

    const-string v1, "origin"

    const-string v2, "origin"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->i:Ljava/util/HashMap;

    const-string v1, "retryCount"

    const-string v2, "retryCount"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->i:Ljava/util/HashMap;

    const-string v1, "types"

    const-string v2, "types"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->i:Ljava/util/HashMap;

    const-string v1, "useLegacyCrypto"

    const-string v2, "useLegacyCrypto"

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 128
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->b:I

    .line 129
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->a:Ljava/util/Set;

    .line 130
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;ILjava/lang/String;ILjava/util/List;Z)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 143
    iput-object p1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->a:Ljava/util/Set;

    .line 144
    iput p2, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->b:I

    .line 145
    iput-object p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->c:Ljava/lang/String;

    .line 146
    iput p4, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->d:I

    .line 147
    iput-object p5, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->e:Ljava/lang/String;

    .line 148
    iput p6, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->f:I

    .line 149
    iput-object p7, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->g:Ljava/util/List;

    .line 150
    iput-boolean p8, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->h:Z

    .line 151
    return-void
.end method

.method public constructor <init>(Ljava/util/Set;Ljava/lang/String;ILjava/lang/String;ILjava/util/List;Z)V
    .locals 1

    .prologue
    .line 162
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 163
    iput-object p1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->a:Ljava/util/Set;

    .line 164
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->b:I

    .line 165
    iput-object p2, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->c:Ljava/lang/String;

    .line 166
    iput p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->d:I

    .line 167
    iput-object p4, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->e:Ljava/lang/String;

    .line 168
    iput p5, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->f:I

    .line 169
    iput-object p6, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->g:Ljava/util/List;

    .line 170
    iput-boolean p7, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->h:Z

    .line 171
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->i:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 329
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 330
    packed-switch v0, :pswitch_data_0

    .line 338
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be an int."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 332
    :pswitch_1
    iput p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->d:I

    .line 341
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 342
    return-void

    .line 335
    :pswitch_2
    iput p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->f:I

    goto :goto_0

    .line 330
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 362
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 363
    packed-switch v0, :pswitch_data_0

    .line 371
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 365
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->c:Ljava/lang/String;

    .line 374
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 375
    return-void

    .line 368
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->e:Ljava/lang/String;

    goto :goto_0

    .line 363
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 347
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 348
    packed-switch v0, :pswitch_data_0

    .line 353
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a boolean."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 350
    :pswitch_0
    iput-boolean p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->h:Z

    .line 356
    iget-object v1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 357
    return-void

    .line 348
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 295
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 309
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 297
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->c:Ljava/lang/String;

    .line 307
    :goto_0
    return-object v0

    .line 299
    :pswitch_1
    iget v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 301
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->e:Ljava/lang/String;

    goto :goto_0

    .line 303
    :pswitch_3
    iget v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 305
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->g:Ljava/util/List;

    goto :goto_0

    .line 307
    :pswitch_5
    iget-boolean v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 295
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 380
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 381
    packed-switch v0, :pswitch_data_0

    .line 386
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be an array of String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 383
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->g:Ljava/util/List;

    .line 390
    iget-object v1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 391
    return-void

    .line 381
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method public final bridge synthetic c()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 24
    return-object p0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 280
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/ay;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 419
    instance-of v0, p1, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;

    if-nez v0, :cond_0

    move v0, v1

    .line 450
    :goto_0
    return v0

    .line 424
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 425
    goto :goto_0

    .line 428
    :cond_1
    check-cast p1, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;

    .line 429
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 430
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 431
    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 433
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 435
    goto :goto_0

    :cond_3
    move v0, v1

    .line 440
    goto :goto_0

    .line 443
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 445
    goto :goto_0

    :cond_5
    move v0, v2

    .line 450
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 406
    const/4 v0, 0x0

    .line 407
    sget-object v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->i:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 408
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 409
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 410
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 412
    goto :goto_0

    .line 413
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 285
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;->CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/ay;

    invoke-static {p0, p1}, Lcom/google/android/gms/auth/gencode/authzen/server/api/ay;->a(Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;Landroid/os/Parcel;)V

    .line 286
    return-void
.end method

.method public final z_()Z
    .locals 1

    .prologue
    .line 401
    const/4 v0, 0x1

    return v0
.end method
