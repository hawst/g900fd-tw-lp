.class public final Lcom/google/android/gms/identity/service/a;
.super Lcom/google/android/gms/identity/intents/a/e;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/google/android/gms/identity/intents/a/e;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/google/android/gms/identity/service/a;->a:Landroid/content/Context;

    .line 78
    iput-object p2, p0, Lcom/google/android/gms/identity/service/a;->b:Ljava/lang/String;

    .line 79
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/identity/service/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/identity/service/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/identity/service/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/identity/service/a;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/identity/intents/a/a;Lcom/google/android/gms/identity/intents/UserAddressRequest;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 86
    new-instance v1, Lcom/google/android/gms/wallet/address/a;

    iget-object v0, p0, Lcom/google/android/gms/identity/service/a;->a:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/wallet/cache/j;

    iget-object v3, p0, Lcom/google/android/gms/identity/service/a;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/google/android/gms/wallet/cache/j;-><init>(Landroid/content/Context;)V

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/wallet/address/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/cache/j;)V

    .line 88
    const/4 v0, 0x0

    .line 89
    const-string v2, "com.google.android.gms.identity.intents.EXTRA_ACCOUNT"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 90
    const-string v0, "com.google.android.gms.identity.intents.EXTRA_ACCOUNT"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 93
    :cond_0
    new-instance v2, Lcom/google/android/gms/identity/service/b;

    invoke-direct {v2, p0, p3, p2, p1}, Lcom/google/android/gms/identity/service/b;-><init>(Lcom/google/android/gms/identity/service/a;Landroid/os/Bundle;Lcom/google/android/gms/identity/intents/UserAddressRequest;Lcom/google/android/gms/identity/intents/a/a;)V

    .line 123
    if-nez v0, :cond_3

    .line 124
    iget-object v0, v1, Lcom/google/android/gms/wallet/address/a;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v3, "com.google"

    invoke-virtual {v0, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_1

    array-length v3, v0

    if-nez v3, :cond_2

    :cond_1
    new-array v0, v4, [Lcom/google/checkout/inapp/proto/a/b;

    invoke-interface {v2, v0}, Lcom/google/android/gms/wallet/address/c;->a([Lcom/google/checkout/inapp/proto/a/b;)V

    .line 128
    :goto_0
    return-void

    .line 124
    :cond_2
    aget-object v0, v0, v4

    invoke-virtual {v1, v0, p2, v2}, Lcom/google/android/gms/wallet/address/a;->a(Landroid/accounts/Account;Lcom/google/android/gms/identity/intents/UserAddressRequest;Lcom/google/android/gms/wallet/address/c;)V

    goto :goto_0

    .line 126
    :cond_3
    invoke-virtual {v1, v0, p2, v2}, Lcom/google/android/gms/wallet/address/a;->a(Landroid/accounts/Account;Lcom/google/android/gms/identity/intents/UserAddressRequest;Lcom/google/android/gms/wallet/address/c;)V

    goto :goto_0
.end method
