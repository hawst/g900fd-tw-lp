.class public Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;
.super Lcom/google/android/gms/wallet/common/ui/dq;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/google/android/gms/wallet/common/ui/bf;


# static fields
.field private static final j:Ljava/lang/String;


# instance fields
.field protected a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field protected b:Landroid/accounts/Account;

.field c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

.field d:Landroid/widget/TextView;

.field e:Landroid/widget/ProgressBar;

.field f:Landroid/widget/CheckBox;

.field g:Lcom/google/android/gms/wallet/common/ui/cb;

.field h:Lcom/google/android/gms/wallet/common/ui/bb;

.field i:Lcom/google/android/gms/wallet/common/ui/bb;

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:Lcom/google/checkout/inapp/proto/q;

.field private n:Z

.field private o:Lcom/google/android/gms/wallet/common/ui/dh;

.field private p:I

.field private q:I

.field private r:Ljava/lang/String;

.field private final s:Lcom/google/android/gms/wallet/service/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-string v0, "addInstrument"

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->j:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/dq;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->k:Z

    .line 78
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->q:I

    .line 451
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/e;-><init>(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->s:Lcom/google/android/gms/wallet/service/l;

    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 433
    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->p:I

    .line 434
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 435
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 436
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;I)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(I)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 364
    if-eqz p1, :cond_1

    .line 365
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 369
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    if-nez p1, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 370
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/cb;

    if-nez p1, :cond_3

    move v0, v1

    :goto_2
    invoke-interface {v3, v0}, Lcom/google/android/gms/wallet/common/ui/cb;->a(Z)V

    .line 371
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->f:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->f:Landroid/widget/CheckBox;

    if-nez p1, :cond_4

    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 374
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->n:Z

    .line 375
    return-void

    .line 367
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->e:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 369
    goto :goto_1

    :cond_3
    move v0, v2

    .line 370
    goto :goto_2

    :cond_4
    move v1, v2

    .line 372
    goto :goto_3
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->l:Ljava/lang/String;

    return-object v0
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 401
    if-eqz p1, :cond_0

    .line 402
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    sget v1, Lcom/google/android/gms/p;->Cw:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Ljava/lang/CharSequence;)V

    .line 406
    :goto_0
    return-void

    .line 404
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    sget v1, Lcom/google/android/gms/p;->Au:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->f()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V
    .locals 3

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/bb;->b()Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "AddInstrumentActivity.PossiblyRecoverableErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method private e()Lcom/google/android/gms/wallet/common/ui/dh;
    .locals 2

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->o:Lcom/google/android/gms/wallet/common/ui/dh;

    if-nez v0, :cond_0

    .line 380
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dh;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->o:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->o:Lcom/google/android/gms/wallet/common/ui/dh;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;)V
    .locals 3

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->h:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->h:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->h:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->h:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->h:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "AddInstrumentActivity.NetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 439
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->p:I

    .line 440
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 441
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    .prologue
    .line 308
    packed-switch p2, :pswitch_data_0

    .line 314
    const-string v0, "AddInstrumentActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown error dialog error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    :goto_0
    return-void

    .line 311
    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(Z)V

    goto :goto_0

    .line 308
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(ILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 445
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/dq;->a(ILandroid/content/Intent;)V

    .line 447
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/ui/dr;->a(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->r:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryActivityClosedEvent;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    .line 449
    return-void
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 388
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/dq;->finish()V

    .line 389
    const/4 v0, 0x0

    sget v1, Lcom/google/android/gms/b;->D:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->overridePendingTransition(II)V

    .line 390
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 348
    const/high16 v0, -0x10000

    and-int/2addr v0, p1

    if-eqz v0, :cond_1

    .line 349
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/dq;->onActivityResult(IILandroid/content/Intent;)V

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->gr:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 357
    if-eqz v0, :cond_0

    .line 358
    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .prologue
    .line 395
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->k:Z

    if-eqz v0, :cond_0

    .line 396
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->b(Z)V

    .line 398
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 322
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/cb;

    invoke-interface {v1}, Lcom/google/android/gms/wallet/common/ui/cb;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 323
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(Z)V

    .line 325
    new-instance v1, Lcom/google/checkout/inapp/proto/aa;

    invoke-direct {v1}, Lcom/google/checkout/inapp/proto/aa;-><init>()V

    .line 326
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/cb;

    invoke-interface {v2}, Lcom/google/android/gms/wallet/common/ui/cb;->a()Lcom/google/checkout/a/a/a/d;

    move-result-object v2

    iput-object v2, v1, Lcom/google/checkout/inapp/proto/aa;->b:Lcom/google/checkout/a/a/a/d;

    .line 327
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->l:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 328
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->l:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/checkout/inapp/proto/aa;->c:Ljava/lang/String;

    .line 330
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->m:Lcom/google/checkout/inapp/proto/q;

    if-eqz v2, :cond_1

    .line 331
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->m:Lcom/google/checkout/inapp/proto/q;

    iput-object v2, v1, Lcom/google/checkout/inapp/proto/aa;->a:Lcom/google/checkout/inapp/proto/q;

    .line 334
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->f:Landroid/widget/CheckBox;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->f:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->f:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 337
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->e()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/checkout/inapp/proto/aa;Z)V

    .line 342
    :goto_1
    return-void

    .line 334
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 340
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/cb;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/cb;->g()Z

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 100
    :try_start_0
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dq;->onCreate(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dg;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/dg;

    .line 108
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ck;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/ck;

    .line 110
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v11

    .line 111
    const-string v0, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v11, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 112
    const-string v0, "com.google.android.gms.wallet.account"

    invoke-virtual {v11, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->b:Landroid/accounts/Account;

    .line 113
    const-string v0, "com.google.android.gms.wallet.unadjustedCartId"

    invoke-virtual {v11, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->l:Ljava/lang/String;

    .line 115
    const-string v0, "com.google.android.gms.wallet.accountReference"

    const-class v1, Lcom/google/checkout/inapp/proto/q;

    invoke-static {v11, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/q;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->m:Lcom/google/checkout/inapp/proto/q;

    .line 118
    const-string v0, "com.google.android.gms.wallet.localMode"

    invoke-virtual {v11, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->k:Z

    .line 120
    sget v0, Lcom/google/android/gms/l;->fX:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->setContentView(I)V

    .line 122
    if-eqz p1, :cond_2

    .line 123
    const-string v0, "errorMessageResourceId"

    invoke-virtual {p1, v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->p:I

    .line 124
    const-string v0, "analyticsSessionId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->r:Ljava/lang/String;

    .line 131
    :goto_0
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v3

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->k:Z

    if-nez v0, :cond_4

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Landroid/support/v7/app/a;->a()V

    sget v0, Lcom/google/android/gms/p;->zv:I

    invoke-virtual {v3, v0}, Landroid/support/v7/app/a;->c(I)V

    move v1, v2

    :goto_1
    sget v0, Lcom/google/android/gms/j;->pN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->e:Landroid/widget/ProgressBar;

    sget v0, Lcom/google/android/gms/j;->cj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->b(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/gms/j;->ch:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->d:Landroid/widget/TextView;

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->p:I

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->p:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(I)V

    :goto_2
    const-string v0, "com.google.android.gms.wallet.disallowedCreditCardTypes"

    invoke-virtual {v11, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->gr:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/cb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/cb;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->k:Z

    if-nez v0, :cond_9

    const-string v0, "com.google.android.gms.wallet.defaultCountryCode"

    invoke-virtual {v11, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "com.google.android.gms.wallet.allowedCountryCodes"

    invoke-virtual {v11, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    const-string v0, "com.google.android.gms.wallet.requiresCreditCardFullAddress"

    invoke-virtual {v11, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    const-string v0, "com.google.android.gms.wallet.disallowedCardCategories"

    invoke-virtual {v11, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v8

    const-string v0, "com.google.android.gms.wallet.addressHints"

    const-class v1, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v11, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v9

    const-string v0, "com.google.android.gms.wallet.phoneNumberRequired"

    invoke-virtual {v11, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->b:Landroid/accounts/Account;

    iget-object v10, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->r:Ljava/lang/String;

    invoke-static/range {v0 .. v10}, Lcom/google/android/gms/wallet/common/ui/f;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;ILjava/util/ArrayList;Ljava/lang/String;ZZ[I[ILjava/util/Collection;Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/cb;

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    sget v3, Lcom/google/android/gms/j;->gr:I

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/cb;

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v3, v0}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    sget v0, Lcom/google/android/gms/j;->ua:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;)V

    .line 135
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->e()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    if-nez v0, :cond_1

    .line 136
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->k:Z

    if-nez v0, :cond_a

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->b:Landroid/accounts/Account;

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/wallet/common/ui/dh;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->o:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 144
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->o:Lcom/google/android/gms/wallet/common/ui/dh;

    sget-object v2, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 148
    :cond_1
    :goto_5
    return-void

    .line 101
    :catch_0
    move-exception v0

    .line 102
    const-string v1, "AddInstrumentActivity"

    const-string v3, "Exception creating fragment"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 103
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(ILandroid/content/Intent;)V

    goto :goto_5

    .line 126
    :cond_2
    const-string v0, "addInstrument"

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->b:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v0, v1, v3}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 131
    :cond_3
    sget v0, Lcom/google/android/gms/j;->tc:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setVisibility(I)V

    sget v1, Lcom/google/android/gms/p;->zv:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(I)V

    move v1, v2

    goto/16 :goto_1

    :cond_4
    sget v0, Lcom/google/android/gms/j;->qz:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->f:Landroid/widget/CheckBox;

    const-string v0, "com.google.android.gms.wallet.allowSaveToChromeOption"

    invoke-virtual {v11, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    move v1, v2

    :goto_6
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Landroid/support/v7/app/a;->a()V

    sget v0, Lcom/google/android/gms/p;->BR:I

    invoke-virtual {v3, v0}, Landroid/support/v7/app/a;->c(I)V

    goto/16 :goto_1

    :cond_5
    move v1, v6

    goto :goto_6

    :cond_6
    sget v0, Lcom/google/android/gms/j;->nK:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->setVisibility(I)V

    sget v3, Lcom/google/android/gms/p;->BR:I

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(I)V

    goto/16 :goto_1

    :cond_7
    const-string v0, "com.google.android.gms.wallet.errorMessageResourceId"

    invoke-virtual {v11, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_8

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(I)V

    goto/16 :goto_2

    :cond_8
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->f()V

    goto/16 :goto_2

    :cond_9
    invoke-static {v7}, Lcom/google/android/gms/wallet/common/ui/ci;->a([I)Lcom/google/android/gms/wallet/common/ui/ci;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/cb;

    goto/16 :goto_3

    .line 140
    :cond_a
    const-string v0, "com.google.android.gms.wallet.sessionId"

    invoke-virtual {v11, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 141
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->b:Landroid/accounts/Account;

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->o:Lcom/google/android/gms/wallet/common/ui/dh;

    goto/16 :goto_4
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 287
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/dq;->onPause()V

    .line 289
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->e()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->s:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->b(Lcom/google/android/gms/wallet/service/l;)V

    .line 291
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 152
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dq;->onPostCreate(Landroid/os/Bundle;)V

    .line 154
    if-eqz p1, :cond_1

    .line 155
    const-string v0, "pendingRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->n:Z

    .line 156
    const-string v0, "serviceConnectionSavePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->q:I

    .line 164
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->n:Z

    if-eqz v0, :cond_0

    .line 165
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a(Z)V

    .line 167
    :cond_0
    return-void

    .line 160
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "add_instrument"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 267
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/dq;->onResume()V

    .line 269
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "AddInstrumentActivity.NetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->h:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 271
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->h:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->h:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 274
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "AddInstrumentActivity.PossiblyRecoverableErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 276
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_1

    .line 277
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 280
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->e()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->s:Lcom/google/android/gms/wallet/service/l;

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->q:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/service/k;->b(Lcom/google/android/gms/wallet/service/l;I)V

    .line 283
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 295
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dq;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 297
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->e()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->s:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->c(Lcom/google/android/gms/wallet/service/l;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->q:I

    .line 300
    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->q:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 301
    const-string v0, "pendingRequest"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->n:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 302
    const-string v0, "errorMessageResourceId"

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->p:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 303
    const-string v0, "analyticsSessionId"

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddInstrumentActivity;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    return-void
.end method
