.class final Lcom/google/android/gms/wallet/common/ui/d;
.super Lcom/google/android/gms/wallet/service/l;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)V
    .locals 0

    .prologue
    .line 421
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/d;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/d;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->a(ILandroid/content/Intent;)V

    .line 472
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 440
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->c:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 442
    iget-object v3, p1, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->c:[I

    array-length v4, v3

    move v1, v2

    move v0, v2

    :goto_0
    if-ge v1, v4, :cond_0

    aget v5, v3, v1

    .line 443
    packed-switch v5, :pswitch_data_0

    .line 452
    const-string v0, "AddAddressActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unexpected error code: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    const/4 v0, 0x1

    .line 442
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 448
    :pswitch_0
    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/d;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    iget-object v5, v5, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b()V

    .line 449
    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/d;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    iget-object v5, v5, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g()Z

    goto :goto_1

    .line 457
    :cond_0
    if-eqz v0, :cond_2

    .line 458
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/d;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->e(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)V

    .line 467
    :goto_2
    return-void

    .line 462
    :cond_1
    const-string v0, "AddAddressActivity"

    const-string v1, "Unexpected create address response."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/d;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->e(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)V

    goto :goto_2

    .line 466
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/d;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->a(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;Z)V

    goto :goto_2

    .line 443
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/checkout/inapp/proto/a/b;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 3

    .prologue
    .line 426
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 427
    const-string v1, "com.google.android.gms.wallet.address"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 431
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/d;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->b(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 432
    const-string v1, "serverResponse"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 434
    :cond_0
    const-string v1, "com.google.android.gms.wallet.addressId"

    iget-object v2, p1, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 435
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/d;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->a(ILandroid/content/Intent;)V

    .line 436
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 476
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/d;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->a(ILandroid/content/Intent;)V

    .line 477
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/d;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->f(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)V

    .line 482
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/d;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->e(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)V

    .line 487
    return-void
.end method
