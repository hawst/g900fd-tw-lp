.class public final Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;
.super Landroid/widget/MultiAutoCompleteTextView;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/plus/sharebox/u;

.field private b:Lcom/google/android/gms/plus/audience/bg;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 94
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->c()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-void
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 5

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->b:Lcom/google/android/gms/plus/audience/bg;

    if-nez v0, :cond_1

    .line 218
    :cond_0
    return-void

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->b:Lcom/google/android/gms/plus/audience/bg;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->b:Lcom/google/android/gms/plus/audience/bg;

    iget-object v1, v1, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2, p2}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    new-instance v3, Lcom/google/android/gms/common/people/data/a;

    invoke-direct {v3, v1}, Lcom/google/android/gms/common/people/data/a;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    invoke-virtual {v3, v2}, Lcom/google/android/gms/common/people/data/a;->a(Ljava/util/Collection;)Lcom/google/android/gms/common/people/data/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/a;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/google/android/gms/plus/audience/bg;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    .line 210
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 211
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 212
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 213
    invoke-static {v0, p2}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Lcom/google/android/gms/common/people/data/AudienceMember;Ljava/util/ArrayList;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 214
    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->b:Lcom/google/android/gms/plus/audience/bg;

    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->b:Lcom/google/android/gms/plus/audience/bg;

    iget-object v4, v4, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v4, v0}, Lcom/google/android/gms/common/people/data/g;->b(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-virtual {v3, v0, p0}, Lcom/google/android/gms/plus/audience/bg;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    .line 211
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/common/people/data/AudienceMember;Ljava/util/ArrayList;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 222
    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    move v2, v1

    .line 223
    :goto_1
    if-ge v2, v0, :cond_0

    .line 224
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/gms/common/people/data/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 225
    const/4 v1, 0x1

    .line 228
    :cond_0
    return v1

    .line 222
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0

    .line 223
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private c()Ljava/util/ArrayList;
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 158
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    .line 159
    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v6

    .line 160
    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v1, Lcom/google/android/gms/plus/sharebox/MentionSpan;

    invoke-interface {v5, v3, v0, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/plus/sharebox/MentionSpan;

    .line 163
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 164
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 168
    array-length v9, v0

    move v4, v3

    :goto_0
    if-ge v4, v9, :cond_3

    .line 169
    aget-object v1, v0, v4

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/MentionSpan;->a()Ljava/lang/String;

    move-result-object v10

    .line 171
    invoke-virtual {v8, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 172
    invoke-virtual {v8, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 176
    aget-object v1, v0, v4

    invoke-interface {v5, v1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    .line 177
    aget-object v11, v0, v4

    invoke-interface {v5, v11}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v11

    .line 178
    add-int/lit8 v11, v11, 0x1

    invoke-static {v6, v11}, Ljava/lang/Math;->min(II)I

    move-result v11

    invoke-interface {v5, v1, v11}, Landroid/text/Spannable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 180
    const-string v11, "+"

    invoke-virtual {v1, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    .line 181
    if-eqz v11, :cond_0

    .line 182
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 185
    :cond_0
    const/4 v12, 0x0

    invoke-static {v10, v1, v12}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v10

    .line 187
    invoke-virtual {v10}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "checkboxEnabled"

    if-nez v11, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {v12, v13, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 189
    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    :cond_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    :cond_2
    move v1, v3

    .line 187
    goto :goto_1

    .line 192
    :cond_3
    return-object v7
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 246
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 247
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getSelectionEnd()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineBaseline(I)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/support/v4/app/au;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/audience/bg;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 234
    iput-object p6, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->b:Lcom/google/android/gms/plus/audience/bg;

    .line 235
    new-instance v0, Lcom/google/android/gms/plus/sharebox/u;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v7, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->b:Lcom/google/android/gms/plus/audience/bg;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/sharebox/u;-><init>(Landroid/content/Context;Landroid/support/v4/app/au;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/audience/bg;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a:Lcom/google/android/gms/plus/sharebox/u;

    .line 237
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a:Lcom/google/android/gms/plus/sharebox/u;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 238
    new-instance v0, Lcom/google/android/gms/plus/sharebox/aa;

    invoke-direct {v0}, Lcom/google/android/gms/plus/sharebox/aa;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setTokenizer(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;)V

    .line 239
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setThreshold(I)V

    .line 240
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a:Lcom/google/android/gms/plus/sharebox/u;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/u;->j()V

    .line 241
    new-instance v0, Lcom/google/android/gms/plus/sharebox/z;

    invoke-direct {v0, p0, v8}, Lcom/google/android/gms/plus/sharebox/z;-><init>(Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;B)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 242
    invoke-virtual {p0, v8}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Z)V

    .line 243
    return-void
.end method

.method public final a(Z)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const v9, 0x44098000    # 550.0f

    const/4 v1, 0x1

    .line 266
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getInputType()I

    move-result v3

    .line 267
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/common/util/at;->a:Lcom/google/android/gms/common/util/at;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/gms/common/util/at;->a:Lcom/google/android/gms/common/util/at;

    .line 269
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-eq v2, v1, :cond_0

    iget v0, v0, Lcom/google/android/gms/common/util/at;->b:I

    if-eq v0, v1, :cond_0

    if-nez p1, :cond_6

    .line 274
    :cond_0
    const v0, -0x10001

    and-int/2addr v0, v3

    .line 279
    :goto_1
    if-eq v3, v0, :cond_1

    .line 280
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setRawInputType(I)V

    .line 281
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/ae;->a(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    .line 283
    :cond_1
    return-void

    .line 267
    :cond_2
    const-string v0, "window"

    invoke-virtual {v4, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    new-instance v5, Landroid/util/DisplayMetrics;

    invoke-direct {v5}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v6, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v7, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    if-eqz v6, :cond_3

    if-eqz v7, :cond_3

    iget v0, v5, Landroid/util/DisplayMetrics;->density:F

    const/4 v8, 0x0

    cmpl-float v0, v0, v8

    if-nez v0, :cond_4

    :cond_3
    move v0, v2

    :goto_2
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v4, Lcom/google/android/gms/g;->p:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v2, v4

    float-to-int v4, v2

    new-instance v2, Lcom/google/android/gms/common/util/at;

    invoke-static {v7, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-direct {v2, v5, v6, v0, v4}, Lcom/google/android/gms/common/util/at;-><init>(IIII)V

    sput-object v2, Lcom/google/android/gms/common/util/at;->a:Lcom/google/android/gms/common/util/at;

    move-object v0, v2

    goto :goto_0

    :cond_4
    int-to-float v0, v6

    iget v8, v5, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v8

    int-to-float v8, v7

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    div-float v5, v8, v5

    cmpl-float v0, v0, v9

    if-ltz v0, :cond_5

    cmpl-float v0, v5, v9

    if-ltz v0, :cond_5

    move v0, v1

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_2

    .line 276
    :cond_6
    const/high16 v0, 0x10000

    or-int/2addr v0, v3

    goto :goto_1
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 253
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getSelectionEnd()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineTop(I)I

    move-result v0

    goto :goto_0
.end method

.method protected final convertSelectionToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 124
    instance-of v0, p1, Lcom/google/android/gms/people/model/j;

    if-nez v0, :cond_1

    .line 125
    invoke-super {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;->convertSelectionToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 141
    :cond_0
    :goto_0
    return-object v0

    .line 127
    :cond_1
    check-cast p1, Lcom/google/android/gms/people/model/j;

    .line 129
    :try_start_0
    new-instance v0, Landroid/text/SpannableString;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gms/people/model/j;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 130
    invoke-interface {p1}, Lcom/google/android/gms/people/model/j;->a()Ljava/lang/String;

    move-result-object v1

    .line 131
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 132
    new-instance v2, Lcom/google/android/gms/plus/sharebox/MentionSpan;

    invoke-direct {v2, v1}, Lcom/google/android/gms/plus/sharebox/MentionSpan;-><init>(Ljava/lang/String;)V

    .line 133
    const/4 v1, 0x0

    invoke-interface {v0}, Landroid/text/Spannable;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-interface {v0, v2, v1, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 136
    :catch_0
    move-exception v0

    .line 138
    const-string v1, "ShareBox"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 139
    const-string v1, "ShareBox"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to convert +mention selection to String: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final onAttachedToWindow()V
    .locals 1

    .prologue
    .line 287
    invoke-super {p0}, Landroid/widget/MultiAutoCompleteTextView;->onAttachedToWindow()V

    .line 288
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a:Lcom/google/android/gms/plus/sharebox/u;

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a:Lcom/google/android/gms/plus/sharebox/u;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/u;->j()V

    .line 291
    :cond_0
    return-void
.end method

.method protected final onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 295
    invoke-super {p0}, Landroid/widget/MultiAutoCompleteTextView;->onDetachedFromWindow()V

    .line 296
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a:Lcom/google/android/gms/plus/sharebox/u;

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a:Lcom/google/android/gms/plus/sharebox/u;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/u;->k()V

    .line 299
    :cond_0
    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 98
    invoke-super {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 103
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v2

    .line 104
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v0

    .line 106
    const-class v3, Landroid/text/style/URLSpan;

    invoke-interface {v2, v1, v0, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 107
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v0, v1

    .line 108
    invoke-static {v4}, Lcom/google/android/gms/plus/sharebox/MentionSpan;->a(Landroid/text/style/URLSpan;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 109
    new-instance v5, Lcom/google/android/gms/plus/sharebox/MentionSpan;

    invoke-direct {v5, v4}, Lcom/google/android/gms/plus/sharebox/MentionSpan;-><init>(Landroid/text/style/URLSpan;)V

    invoke-interface {v2, v4}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    invoke-interface {v2, v4}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    invoke-interface {v2, v4}, Landroid/text/Editable;->getSpanFlags(Ljava/lang/Object;)I

    move-result v8

    invoke-interface {v2, v4}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    invoke-interface {v2, v5, v6, v7, v8}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 107
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 112
    :cond_1
    return-void
.end method

.method protected final replaceText(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->c()Ljava/util/ArrayList;

    move-result-object v0

    .line 148
    invoke-super {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;->replaceText(Ljava/lang/CharSequence;)V

    .line 149
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->c()Ljava/util/ArrayList;

    move-result-object v1

    .line 150
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 151
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Z)V

    .line 152
    return-void
.end method
