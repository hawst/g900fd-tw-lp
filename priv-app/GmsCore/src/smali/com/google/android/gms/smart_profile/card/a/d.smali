.class public final Lcom/google/android/gms/smart_profile/card/a/d;
.super Lcom/google/android/gms/smart_profile/card/a/b;
.source "SourceFile"


# instance fields
.field private e:Lcom/google/android/gms/smart_profile/bg;

.field private f:Z

.field private g:Ljava/util/List;

.field private h:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/a/b;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 88
    invoke-super {p0, p1}, Lcom/google/android/gms/smart_profile/card/a/b;->a(Landroid/os/Bundle;)V

    .line 89
    const-string v0, "is_expanded"

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/d;->h:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 90
    return-void
.end method

.method public final a(Lcom/google/android/gms/smart_profile/card/a/c;Landroid/os/Bundle;Lcom/google/android/gms/smart_profile/bg;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 38
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/smart_profile/card/a/b;->a(Lcom/google/android/gms/smart_profile/card/a/c;Landroid/os/Bundle;Lcom/google/android/gms/smart_profile/bg;)V

    .line 40
    if-nez p3, :cond_0

    .line 84
    :goto_0
    return-void

    .line 43
    :cond_0
    iput-object p3, p0, Lcom/google/android/gms/smart_profile/card/a/d;->e:Lcom/google/android/gms/smart_profile/bg;

    .line 45
    new-instance v1, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/card/a/d;->e:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v4}, Lcom/google/android/gms/smart_profile/bg;->c()Lcom/google/android/gms/smart_profile/ak;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/gms/smart_profile/ak;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v0, v4}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;-><init>(Landroid/content/Context;Landroid/content/Context;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/d;->e:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/a/d;->e:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->a(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v2

    .line 52
    iget-object v1, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/util/List;

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/d;->h:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/d;->h:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    sget-object v4, Lcom/google/android/gms/smart_profile/c;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 55
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/smart_profile/card/a/d;->f:Z

    goto :goto_0

    .line 58
    :cond_1
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/d;->g:Ljava/util/List;

    .line 61
    if-nez p2, :cond_2

    .line 63
    :goto_1
    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/d;->e:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->a(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Landroid/content/ContentResolver;)Landroid/net/Uri;

    move-result-object v4

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/d;->h:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    sget-object v2, Lcom/google/android/gms/smart_profile/a/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/card/a/d;->e:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v5}, Lcom/google/android/gms/smart_profile/bg;->q()I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Ljava/util/List;IZLandroid/net/Uri;I)V

    .line 76
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/d;->h:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/b;->c:Lcom/google/android/gms/smart_profile/card/h;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Lcom/google/android/gms/smart_profile/card/h;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    sget v1, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 80
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v1, Landroid/support/v7/widget/CardView;

    invoke-virtual {v1}, Landroid/support/v7/widget/CardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->wB:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/d;->e:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/bg;->q()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 61
    :cond_2
    const-string v0, "is_expanded"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    goto :goto_1

    .line 73
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/a/d;->h:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/card/a/d;->e:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v4}, Lcom/google/android/gms/smart_profile/bg;->q()I

    move-result v4

    invoke-virtual {v2, v1, v0, v3, v4}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Ljava/util/List;IZI)V

    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/card/a/d;->f:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/google/android/gms/smart_profile/c;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/d;->g:Ljava/util/List;

    return-object v0
.end method
