.class public final Lcom/google/android/gms/wallet/ia/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/ia/f;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/wallet/ia/CartDetailsView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 41
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "price"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "start_time"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "duration"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/wallet/ia/l;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/wallet/ia/CartDetailsView;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/l;->b:Landroid/content/Context;

    .line 49
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/l;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    .line 50
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/l;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    sget v1, Lcom/google/android/gms/l;->ha:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 175
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/l;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    sget v1, Lcom/google/android/gms/l;->gZ:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 182
    sget v1, Lcom/google/android/gms/j;->kE:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    sget v1, Lcom/google/android/gms/j;->kH:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 185
    sget v1, Lcom/google/android/gms/j;->kI:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 187
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 190
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    .line 364
    new-instance v1, Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/wallet/ia/l;->a:[Ljava/lang/String;

    array-length v0, v0

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 366
    sget-object v2, Lcom/google/android/gms/wallet/ia/l;->a:[Ljava/lang/String;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 367
    new-instance v5, Landroid/text/style/TextAppearanceSpan;

    iget-object v6, p0, Lcom/google/android/gms/wallet/ia/l;->b:Landroid/content/Context;

    sget v7, Lcom/google/android/gms/q;->w:I

    invoke-direct {v5, v6, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 370
    :cond_0
    invoke-static {p1, v1}, Lcom/google/android/gms/wallet/common/ae;->a(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/checkout/inapp/proto/d;)V
    .locals 17

    .prologue
    .line 54
    const/4 v5, 0x0

    .line 55
    const/4 v6, 0x0

    .line 57
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/ia/l;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c()V

    .line 58
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    array-length v2, v2

    if-nez v2, :cond_0

    .line 87
    :goto_0
    return-void

    .line 61
    :cond_0
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    array-length v9, v8

    const/4 v2, 0x0

    move v7, v2

    :goto_1
    if-ge v7, v9, :cond_14

    aget-object v10, v8, v7

    .line 62
    iget-object v2, v10, Lcom/google/checkout/inapp/proto/f;->f:Lcom/google/checkout/inapp/proto/p;

    if-eqz v2, :cond_10

    .line 63
    iget-object v2, v10, Lcom/google/checkout/inapp/proto/f;->a:Ljava/lang/String;

    iget-object v3, v10, Lcom/google/checkout/inapp/proto/f;->g:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v3}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/d;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/gms/wallet/ia/l;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/ia/l;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    sget v3, Lcom/google/android/gms/l;->hb:I

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sget v3, Lcom/google/android/gms/j;->sh:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    iget-object v3, v10, Lcom/google/checkout/inapp/proto/f;->f:Lcom/google/checkout/inapp/proto/p;

    iget v4, v3, Lcom/google/checkout/inapp/proto/p;->b:I

    iget v3, v3, Lcom/google/checkout/inapp/proto/p;->a:I

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/wallet/ia/l;->b:Landroid/content/Context;

    const/4 v12, 0x1

    if-ne v3, v12, :cond_1

    packed-switch v4, :pswitch_data_0

    const-string v3, "DefaultCartDetailsRenderer"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Unknown recurrence frequency units: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    sget v3, Lcom/google/android/gms/p;->CF:I

    :goto_2
    invoke-virtual {v11, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/wallet/ia/l;->b:Landroid/content/Context;

    sget v12, Lcom/google/android/gms/q;->w:I

    invoke-direct {v4, v11, v12}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/4 v11, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v12, 0x21

    invoke-virtual {v5, v4, v11, v3, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget v3, v10, Lcom/google/checkout/inapp/proto/f;->h:I

    if-eqz v3, :cond_2

    iget v3, v10, Lcom/google/checkout/inapp/proto/f;->h:I

    packed-switch v3, :pswitch_data_1

    const-string v4, "DefaultCartDetailsRenderer"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Unknown initial payment type: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    :goto_3
    if-lez v3, :cond_2

    const-string v4, " - "

    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/wallet/ia/l;->b:Landroid/content/Context;

    invoke-virtual {v11, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_2
    const-string v3, "\n"

    invoke-virtual {v5, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v11, v10, Lcom/google/checkout/inapp/proto/f;->f:Lcom/google/checkout/inapp/proto/p;

    iget v12, v11, Lcom/google/checkout/inapp/proto/p;->a:I

    iget-object v3, v10, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v3}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/d;)Ljava/lang/String;

    move-result-object v13

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v3

    iget-wide v14, v11, Lcom/google/checkout/inapp/proto/p;->c:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iget v3, v11, Lcom/google/checkout/inapp/proto/p;->d:I

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    :goto_4
    iget-object v4, v10, Lcom/google/checkout/inapp/proto/f;->f:Lcom/google/checkout/inapp/proto/p;

    iget v15, v4, Lcom/google/checkout/inapp/proto/p;->b:I

    iget-boolean v10, v10, Lcom/google/checkout/inapp/proto/f;->i:Z

    iget v4, v4, Lcom/google/checkout/inapp/proto/p;->d:I

    if-eqz v4, :cond_4

    const/4 v4, 0x1

    :goto_5
    packed-switch v15, :pswitch_data_2

    const-string v4, "DefaultCartDetailsRenderer"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v16, "Unknown subscription frequency units: "

    move-object/from16 v0, v16

    invoke-direct {v10, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v4, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    :goto_6
    if-lez v4, :cond_f

    if-eqz v3, :cond_e

    iget v3, v11, Lcom/google/checkout/inapp/proto/p;->d:I

    mul-int/2addr v3, v12

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/wallet/ia/l;->b:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const/4 v11, 0x4

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v13, v11, v15

    const/4 v13, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v11, v13

    const/4 v13, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v11, v13

    const/4 v3, 0x3

    aput-object v14, v11, v3

    invoke-virtual {v10, v4, v12, v11}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_7
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/gms/wallet/ia/l;->b(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    :goto_8
    invoke-virtual {v5, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    const/4 v2, 0x1

    .line 61
    :goto_9
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move v5, v2

    goto/16 :goto_1

    .line 63
    :pswitch_0
    sget v3, Lcom/google/android/gms/p;->Av:I

    goto/16 :goto_2

    :pswitch_1
    sget v3, Lcom/google/android/gms/p;->Cc:I

    goto/16 :goto_2

    :pswitch_2
    sget v3, Lcom/google/android/gms/p;->Dg:I

    goto/16 :goto_2

    :pswitch_3
    sget v3, Lcom/google/android/gms/p;->BC:I

    goto/16 :goto_3

    :pswitch_4
    sget v3, Lcom/google/android/gms/p;->Ct:I

    goto/16 :goto_3

    :cond_3
    const/4 v3, 0x0

    goto :goto_4

    :cond_4
    const/4 v4, 0x0

    goto :goto_5

    :pswitch_5
    if-eqz v10, :cond_6

    if-eqz v4, :cond_5

    sget v4, Lcom/google/android/gms/n;->H:I

    goto :goto_6

    :cond_5
    sget v4, Lcom/google/android/gms/n;->G:I

    goto :goto_6

    :cond_6
    if-eqz v4, :cond_7

    sget v4, Lcom/google/android/gms/n;->F:I

    goto :goto_6

    :cond_7
    sget v4, Lcom/google/android/gms/n;->E:I

    goto :goto_6

    :pswitch_6
    if-eqz v10, :cond_9

    if-eqz v4, :cond_8

    sget v4, Lcom/google/android/gms/n;->L:I

    goto :goto_6

    :cond_8
    sget v4, Lcom/google/android/gms/n;->K:I

    goto :goto_6

    :cond_9
    if-eqz v4, :cond_a

    sget v4, Lcom/google/android/gms/n;->J:I

    goto :goto_6

    :cond_a
    sget v4, Lcom/google/android/gms/n;->I:I

    goto :goto_6

    :pswitch_7
    if-eqz v10, :cond_c

    if-eqz v4, :cond_b

    sget v4, Lcom/google/android/gms/n;->P:I

    goto :goto_6

    :cond_b
    sget v4, Lcom/google/android/gms/n;->O:I

    goto/16 :goto_6

    :cond_c
    if-eqz v4, :cond_d

    sget v4, Lcom/google/android/gms/n;->N:I

    goto/16 :goto_6

    :cond_d
    sget v4, Lcom/google/android/gms/n;->M:I

    goto/16 :goto_6

    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/ia/l;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v13, v10, v11

    const/4 v11, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v10, v11

    const/4 v11, 0x2

    aput-object v14, v10, v11

    invoke-virtual {v3, v4, v12, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_7

    :cond_f
    const-string v3, ""

    goto :goto_8

    .line 66
    :cond_10
    iget-object v2, v10, Lcom/google/checkout/inapp/proto/f;->a:Ljava/lang/String;

    iget-object v3, v10, Lcom/google/checkout/inapp/proto/f;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_11

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v10, Lcom/google/checkout/inapp/proto/f;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_11
    iget v3, v10, Lcom/google/checkout/inapp/proto/f;->e:I

    const/4 v4, 0x0

    const/4 v11, 0x1

    if-le v3, v11, :cond_13

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v3, v10, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v3, :cond_12

    iget-object v3, v10, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v3}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/d;)Ljava/lang/String;

    move-result-object v3

    :goto_a
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/gms/wallet/ia/l;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;)V

    move v2, v5

    goto/16 :goto_9

    :cond_12
    const-string v3, ""

    goto :goto_a

    :cond_13
    iget-object v3, v10, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v3}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/d;)Ljava/lang/String;

    move-result-object v3

    goto :goto_a

    .line 70
    :cond_14
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/d;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v2, :cond_18

    .line 71
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/d;->d:Ljava/lang/String;

    .line 72
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 73
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/ia/l;->b:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->Bc:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 75
    :cond_15
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/ia/l;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/checkout/inapp/proto/d;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v4}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/d;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 77
    const/4 v2, 0x1

    .line 80
    :goto_b
    if-eqz v5, :cond_17

    .line 81
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/ia/l;->b:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->AA:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gms/wallet/ia/l;->a(Ljava/lang/String;)V

    .line 86
    :cond_16
    :goto_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/ia/l;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a()V

    goto/16 :goto_0

    .line 82
    :cond_17
    if-eqz v2, :cond_16

    .line 83
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/ia/l;->b:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->Az:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gms/wallet/ia/l;->a(Ljava/lang/String;)V

    goto :goto_c

    :cond_18
    move v2, v6

    goto :goto_b

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final b(Lcom/google/checkout/inapp/proto/d;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 91
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/d;->e:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/d;->e:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/d;)Ljava/lang/String;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/l;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/l;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b(Ljava/lang/CharSequence;)V

    .line 97
    :cond_0
    iget-object v1, p1, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    .line 98
    if-eqz v1, :cond_1

    iget-object v0, v1, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v0, :cond_1

    .line 99
    iget-boolean v0, v1, Lcom/google/checkout/inapp/proto/at;->b:Z

    if-eqz v0, :cond_3

    .line 101
    iget v0, v1, Lcom/google/checkout/inapp/proto/at;->c:I

    packed-switch v0, :pswitch_data_0

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/l;->b:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->BH:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 112
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/l;->b:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->BF:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    iget-object v0, v1, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/d;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 114
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/l;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c(Ljava/lang/CharSequence;)V

    .line 134
    :cond_1
    :goto_1
    iget-object v1, p1, Lcom/google/checkout/inapp/proto/d;->g:Lcom/google/checkout/inapp/proto/ar;

    .line 135
    if-eqz v1, :cond_2

    iget-object v0, v1, Lcom/google/checkout/inapp/proto/ar;->a:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v0, :cond_2

    .line 136
    iget-object v0, v1, Lcom/google/checkout/inapp/proto/ar;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/l;->b:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->CB:I

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, v1, Lcom/google/checkout/inapp/proto/ar;->b:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 143
    :goto_2
    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/l;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/ar;->a:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/d;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 147
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/l;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a()V

    .line 148
    return-void

    .line 103
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/l;->b:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->BG:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 106
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/l;->b:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->BI:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 117
    :cond_3
    iget v0, v1, Lcom/google/checkout/inapp/proto/at;->c:I

    packed-switch v0, :pswitch_data_1

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/l;->b:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->CG:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 129
    :goto_3
    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/l;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/d;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 119
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/l;->b:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->BG:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 122
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/l;->b:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->BI:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 141
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/l;->b:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->CA:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 101
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 117
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
