.class public final Lcom/google/android/gms/lockbox/a/d;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:Z

.field public c:J

.field public d:I

.field public e:Lcom/google/android/gms/lockbox/a/c;

.field public f:Ljava/lang/String;

.field public g:I

.field public h:I

.field public i:I

.field public j:Lcom/google/android/gms/lockbox/a/c;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 750
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 751
    iput-wide v4, p0, Lcom/google/android/gms/lockbox/a/d;->a:J

    iput-boolean v1, p0, Lcom/google/android/gms/lockbox/a/d;->b:Z

    iput-wide v4, p0, Lcom/google/android/gms/lockbox/a/d;->c:J

    iput v1, p0, Lcom/google/android/gms/lockbox/a/d;->d:I

    iput-object v2, p0, Lcom/google/android/gms/lockbox/a/d;->e:Lcom/google/android/gms/lockbox/a/c;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/lockbox/a/d;->f:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/lockbox/a/d;->g:I

    iput v1, p0, Lcom/google/android/gms/lockbox/a/d;->h:I

    iput v1, p0, Lcom/google/android/gms/lockbox/a/d;->i:I

    iput-object v2, p0, Lcom/google/android/gms/lockbox/a/d;->j:Lcom/google/android/gms/lockbox/a/c;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/lockbox/a/d;->cachedSize:I

    .line 752
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 886
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 887
    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/d;->a:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 888
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/d;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 891
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/lockbox/a/d;->b:Z

    if-eqz v1, :cond_1

    .line 892
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/lockbox/a/d;->b:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 895
    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/d;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 896
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/d;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 899
    :cond_2
    iget v1, p0, Lcom/google/android/gms/lockbox/a/d;->d:I

    if-eqz v1, :cond_3

    .line 900
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/lockbox/a/d;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 903
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/d;->e:Lcom/google/android/gms/lockbox/a/c;

    if-eqz v1, :cond_4

    .line 904
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/d;->e:Lcom/google/android/gms/lockbox/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 907
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/d;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 908
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/d;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 911
    :cond_5
    iget v1, p0, Lcom/google/android/gms/lockbox/a/d;->g:I

    if-eqz v1, :cond_6

    .line 912
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/gms/lockbox/a/d;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 915
    :cond_6
    iget v1, p0, Lcom/google/android/gms/lockbox/a/d;->h:I

    if-eqz v1, :cond_7

    .line 916
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/gms/lockbox/a/d;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 919
    :cond_7
    iget v1, p0, Lcom/google/android/gms/lockbox/a/d;->i:I

    if-eqz v1, :cond_8

    .line 920
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/gms/lockbox/a/d;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 923
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/d;->j:Lcom/google/android/gms/lockbox/a/c;

    if-eqz v1, :cond_9

    .line 924
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/d;->j:Lcom/google/android/gms/lockbox/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 927
    :cond_9
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 771
    if-ne p1, p0, :cond_1

    .line 824
    :cond_0
    :goto_0
    return v0

    .line 774
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/lockbox/a/d;

    if-nez v2, :cond_2

    move v0, v1

    .line 775
    goto :goto_0

    .line 777
    :cond_2
    check-cast p1, Lcom/google/android/gms/lockbox/a/d;

    .line 778
    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/d;->a:J

    iget-wide v4, p1, Lcom/google/android/gms/lockbox/a/d;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 779
    goto :goto_0

    .line 781
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/gms/lockbox/a/d;->b:Z

    iget-boolean v3, p1, Lcom/google/android/gms/lockbox/a/d;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 782
    goto :goto_0

    .line 784
    :cond_4
    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/d;->c:J

    iget-wide v4, p1, Lcom/google/android/gms/lockbox/a/d;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    .line 785
    goto :goto_0

    .line 787
    :cond_5
    iget v2, p0, Lcom/google/android/gms/lockbox/a/d;->d:I

    iget v3, p1, Lcom/google/android/gms/lockbox/a/d;->d:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 788
    goto :goto_0

    .line 790
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/d;->e:Lcom/google/android/gms/lockbox/a/c;

    if-nez v2, :cond_7

    .line 791
    iget-object v2, p1, Lcom/google/android/gms/lockbox/a/d;->e:Lcom/google/android/gms/lockbox/a/c;

    if-eqz v2, :cond_8

    move v0, v1

    .line 792
    goto :goto_0

    .line 795
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/d;->e:Lcom/google/android/gms/lockbox/a/c;

    iget-object v3, p1, Lcom/google/android/gms/lockbox/a/d;->e:Lcom/google/android/gms/lockbox/a/c;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/lockbox/a/c;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 796
    goto :goto_0

    .line 799
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/d;->f:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 800
    iget-object v2, p1, Lcom/google/android/gms/lockbox/a/d;->f:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 801
    goto :goto_0

    .line 803
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/d;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/lockbox/a/d;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 804
    goto :goto_0

    .line 806
    :cond_a
    iget v2, p0, Lcom/google/android/gms/lockbox/a/d;->g:I

    iget v3, p1, Lcom/google/android/gms/lockbox/a/d;->g:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 807
    goto :goto_0

    .line 809
    :cond_b
    iget v2, p0, Lcom/google/android/gms/lockbox/a/d;->h:I

    iget v3, p1, Lcom/google/android/gms/lockbox/a/d;->h:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 810
    goto :goto_0

    .line 812
    :cond_c
    iget v2, p0, Lcom/google/android/gms/lockbox/a/d;->i:I

    iget v3, p1, Lcom/google/android/gms/lockbox/a/d;->i:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 813
    goto :goto_0

    .line 815
    :cond_d
    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/d;->j:Lcom/google/android/gms/lockbox/a/c;

    if-nez v2, :cond_e

    .line 816
    iget-object v2, p1, Lcom/google/android/gms/lockbox/a/d;->j:Lcom/google/android/gms/lockbox/a/c;

    if-eqz v2, :cond_0

    move v0, v1

    .line 817
    goto :goto_0

    .line 820
    :cond_e
    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/d;->j:Lcom/google/android/gms/lockbox/a/c;

    iget-object v3, p1, Lcom/google/android/gms/lockbox/a/d;->j:Lcom/google/android/gms/lockbox/a/c;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/lockbox/a/c;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 821
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 829
    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/d;->a:J

    iget-wide v4, p0, Lcom/google/android/gms/lockbox/a/d;->a:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/lit16 v0, v0, 0x20f

    .line 832
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/lockbox/a/d;->b:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v2

    .line 833
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/d;->c:J

    iget-wide v4, p0, Lcom/google/android/gms/lockbox/a/d;->c:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 835
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/lockbox/a/d;->d:I

    add-int/2addr v0, v2

    .line 836
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/d;->e:Lcom/google/android/gms/lockbox/a/c;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 838
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/d;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 840
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/lockbox/a/d;->g:I

    add-int/2addr v0, v2

    .line 841
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/lockbox/a/d;->h:I

    add-int/2addr v0, v2

    .line 842
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/lockbox/a/d;->i:I

    add-int/2addr v0, v2

    .line 843
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/d;->j:Lcom/google/android/gms/lockbox/a/c;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 845
    return v0

    .line 832
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0

    .line 836
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/d;->e:Lcom/google/android/gms/lockbox/a/c;

    invoke-virtual {v0}, Lcom/google/android/gms/lockbox/a/c;->hashCode()I

    move-result v0

    goto :goto_1

    .line 838
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/d;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 843
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/d;->j:Lcom/google/android/gms/lockbox/a/c;

    invoke-virtual {v1}, Lcom/google/android/gms/lockbox/a/c;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 703
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/lockbox/a/d;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/lockbox/a/d;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/lockbox/a/d;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/lockbox/a/d;->d:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/d;->e:Lcom/google/android/gms/lockbox/a/c;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/lockbox/a/c;

    invoke-direct {v0}, Lcom/google/android/gms/lockbox/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/lockbox/a/d;->e:Lcom/google/android/gms/lockbox/a/c;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/d;->e:Lcom/google/android/gms/lockbox/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/lockbox/a/d;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/lockbox/a/d;->g:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/lockbox/a/d;->h:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/lockbox/a/d;->i:I

    goto :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/d;->j:Lcom/google/android/gms/lockbox/a/c;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/lockbox/a/c;

    invoke-direct {v0}, Lcom/google/android/gms/lockbox/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/lockbox/a/d;->j:Lcom/google/android/gms/lockbox/a/c;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/d;->j:Lcom/google/android/gms/lockbox/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 851
    iget-wide v0, p0, Lcom/google/android/gms/lockbox/a/d;->a:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 852
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/d;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 854
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/lockbox/a/d;->b:Z

    if-eqz v0, :cond_1

    .line 855
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/gms/lockbox/a/d;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 857
    :cond_1
    iget-wide v0, p0, Lcom/google/android/gms/lockbox/a/d;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 858
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/d;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 860
    :cond_2
    iget v0, p0, Lcom/google/android/gms/lockbox/a/d;->d:I

    if-eqz v0, :cond_3

    .line 861
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/lockbox/a/d;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 863
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/d;->e:Lcom/google/android/gms/lockbox/a/c;

    if-eqz v0, :cond_4

    .line 864
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/d;->e:Lcom/google/android/gms/lockbox/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 866
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/d;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 867
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/d;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 869
    :cond_5
    iget v0, p0, Lcom/google/android/gms/lockbox/a/d;->g:I

    if-eqz v0, :cond_6

    .line 870
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/gms/lockbox/a/d;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 872
    :cond_6
    iget v0, p0, Lcom/google/android/gms/lockbox/a/d;->h:I

    if-eqz v0, :cond_7

    .line 873
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/gms/lockbox/a/d;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 875
    :cond_7
    iget v0, p0, Lcom/google/android/gms/lockbox/a/d;->i:I

    if-eqz v0, :cond_8

    .line 876
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/android/gms/lockbox/a/d;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 878
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/d;->j:Lcom/google/android/gms/lockbox/a/c;

    if-eqz v0, :cond_9

    .line 879
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/d;->j:Lcom/google/android/gms/lockbox/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 881
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 882
    return-void
.end method
