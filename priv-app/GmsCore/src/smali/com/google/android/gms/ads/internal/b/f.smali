.class final Lcom/google/android/gms/ads/internal/b/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Landroid/webkit/ValueCallback;

.field final synthetic b:Lcom/google/android/gms/ads/internal/b/a;

.field final synthetic c:Landroid/webkit/WebView;

.field final synthetic d:Lcom/google/android/gms/ads/internal/b/d;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/b/d;Lcom/google/android/gms/ads/internal/b/a;Landroid/webkit/WebView;)V
    .locals 1

    .prologue
    .line 286
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/b/f;->d:Lcom/google/android/gms/ads/internal/b/d;

    iput-object p2, p0, Lcom/google/android/gms/ads/internal/b/f;->b:Lcom/google/android/gms/ads/internal/b/a;

    iput-object p3, p0, Lcom/google/android/gms/ads/internal/b/f;->c:Landroid/webkit/WebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 287
    new-instance v0, Lcom/google/android/gms/ads/internal/b/g;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/internal/b/g;-><init>(Lcom/google/android/gms/ads/internal/b/f;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/b/f;->a:Landroid/webkit/ValueCallback;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b/f;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getJavaScriptEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    invoke-static {}, Lcom/google/android/gms/ads/internal/util/g;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 299
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b/f;->c:Landroid/webkit/WebView;

    const-string v1, "(function() { return  {text:document.body.innerText}})();"

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b/f;->a:Landroid/webkit/ValueCallback;

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->evaluateJavascript(Ljava/lang/String;Landroid/webkit/ValueCallback;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 307
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    .line 301
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b/f;->a:Landroid/webkit/ValueCallback;

    const-string v1, ""

    invoke-interface {v0, v1}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    goto :goto_0
.end method
