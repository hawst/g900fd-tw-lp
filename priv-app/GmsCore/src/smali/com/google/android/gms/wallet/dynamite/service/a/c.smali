.class public final Lcom/google/android/gms/wallet/dynamite/service/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;


# static fields
.field public static a:Lcom/google/android/gms/common/internal/a/b;


# instance fields
.field final b:Ljava/lang/String;

.field final c:[Lcom/google/android/gms/wallet/dynamite/service/a/g;

.field final d:[Landroid/os/Message;

.field public final e:Lcom/google/android/gms/wallet/dynamite/service/a/b;

.field public f:Ljava/lang/ref/WeakReference;

.field public g:Landroid/os/Handler;

.field private h:Lcom/google/android/gms/wallet/c/i;

.field private i:Lcom/google/android/gms/wallet/c/f;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    new-instance v0, Lcom/google/android/gms/wallet/dynamite/service/a/d;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/wallet/dynamite/service/a/d;-><init>(Lcom/google/android/gms/wallet/dynamite/service/a/c;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->g:Landroid/os/Handler;

    .line 245
    new-instance v0, Lcom/google/android/gms/wallet/dynamite/service/a/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/dynamite/service/a/e;-><init>(Lcom/google/android/gms/wallet/dynamite/service/a/c;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->h:Lcom/google/android/gms/wallet/c/i;

    .line 274
    new-instance v0, Lcom/google/android/gms/wallet/dynamite/service/a/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/dynamite/service/a/f;-><init>(Lcom/google/android/gms/wallet/dynamite/service/a/c;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->i:Lcom/google/android/gms/wallet/c/f;

    .line 79
    iput-object p1, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->b:Ljava/lang/String;

    .line 80
    new-array v0, v2, [Lcom/google/android/gms/wallet/dynamite/service/a/g;

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->c:[Lcom/google/android/gms/wallet/dynamite/service/a/g;

    .line 81
    new-array v0, v2, [Landroid/os/Message;

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->d:[Landroid/os/Message;

    .line 82
    new-instance v0, Lcom/google/android/gms/wallet/dynamite/service/a/b;

    invoke-direct {v0, p2, p0, p0}, Lcom/google/android/gms/wallet/dynamite/service/a/b;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->e:Lcom/google/android/gms/wallet/dynamite/service/a/b;

    .line 83
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 111
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    .line 112
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->d:[Landroid/os/Message;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 113
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->g:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->d:[Landroid/os/Message;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 114
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->d:[Landroid/os/Message;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 111
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 117
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 0

    .prologue
    .line 303
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->e:Lcom/google/android/gms/wallet/dynamite/service/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/dynamite/service/a/b;->c_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->e:Lcom/google/android/gms/wallet/dynamite/service/a/b;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWallet;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWallet;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->h:Lcom/google/android/gms/wallet/c/i;

    invoke-virtual {v0, v1, v2, p2, v3}, Lcom/google/android/gms/wallet/dynamite/service/a/b;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V

    .line 172
    :goto_0
    return-void

    .line 167
    :cond_0
    new-instance v0, Lcom/google/android/gms/wallet/dynamite/service/a/g;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/dynamite/service/a/g;-><init>()V

    .line 168
    iput-object p1, v0, Lcom/google/android/gms/wallet/dynamite/service/a/g;->a:Ljava/lang/Object;

    .line 169
    iput-object p2, v0, Lcom/google/android/gms/wallet/dynamite/service/a/g;->b:Landroid/os/Bundle;

    .line 170
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->c:[Lcom/google/android/gms/wallet/dynamite/service/a/g;

    const/4 v2, 0x2

    aput-object v0, v1, v2

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->e:Lcom/google/android/gms/wallet/dynamite/service/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/dynamite/service/a/b;->c_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->e:Lcom/google/android/gms/wallet/dynamite/service/a/b;

    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->h:Lcom/google/android/gms/wallet/c/i;

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/gms/wallet/dynamite/service/a/b;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V

    .line 149
    :goto_0
    return-void

    .line 144
    :cond_0
    new-instance v0, Lcom/google/android/gms/wallet/dynamite/service/a/g;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/dynamite/service/a/g;-><init>()V

    .line 145
    iput-object p1, v0, Lcom/google/android/gms/wallet/dynamite/service/a/g;->a:Ljava/lang/Object;

    .line 146
    iput-object p2, v0, Lcom/google/android/gms/wallet/dynamite/service/a/g;->b:Landroid/os/Bundle;

    .line 147
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->c:[Lcom/google/android/gms/wallet/dynamite/service/a/g;

    const/4 v2, 0x1

    aput-object v0, v1, v2

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->e:Lcom/google/android/gms/wallet/dynamite/service/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/dynamite/service/a/b;->c_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->e:Lcom/google/android/gms/wallet/dynamite/service/a/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/dynamite/service/a/b;->a(Landroid/os/Bundle;)V

    .line 127
    :goto_0
    return-void

    .line 123
    :cond_0
    new-instance v0, Lcom/google/android/gms/wallet/dynamite/service/a/g;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/dynamite/service/a/g;-><init>()V

    .line 124
    iput-object p1, v0, Lcom/google/android/gms/wallet/dynamite/service/a/g;->b:Landroid/os/Bundle;

    .line 125
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->c:[Lcom/google/android/gms/wallet/dynamite/service/a/g;

    const/4 v2, 0x3

    aput-object v0, v1, v2

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->e:Lcom/google/android/gms/wallet/dynamite/service/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/dynamite/service/a/b;->c_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->e:Lcom/google/android/gms/wallet/dynamite/service/a/b;

    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->i:Lcom/google/android/gms/wallet/c/f;

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/gms/wallet/dynamite/service/a/b;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/f;)V

    .line 160
    :goto_0
    return-void

    .line 155
    :cond_0
    new-instance v0, Lcom/google/android/gms/wallet/dynamite/service/a/g;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/dynamite/service/a/g;-><init>()V

    .line 156
    iput-object p1, v0, Lcom/google/android/gms/wallet/dynamite/service/a/g;->a:Ljava/lang/Object;

    .line 157
    iput-object p2, v0, Lcom/google/android/gms/wallet/dynamite/service/a/g;->b:Landroid/os/Bundle;

    .line 158
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->c:[Lcom/google/android/gms/wallet/dynamite/service/a/g;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    goto :goto_0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 294
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/4 v0, 0x5

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->c:[Lcom/google/android/gms/wallet/dynamite/service/a/g;

    aget-object v2, v0, v1

    if-eqz v2, :cond_0

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported service type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, v2, Lcom/google/android/gms/wallet/dynamite/service/a/g;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iget-object v2, v2, Lcom/google/android/gms/wallet/dynamite/service/a/g;->b:Landroid/os/Bundle;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/wallet/dynamite/service/a/c;->b(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->c:[Lcom/google/android/gms/wallet/dynamite/service/a/g;

    const/4 v2, 0x0

    aput-object v2, v0, v1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :pswitch_1
    iget-object v0, v2, Lcom/google/android/gms/wallet/dynamite/service/a/g;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iget-object v2, v2, Lcom/google/android/gms/wallet/dynamite/service/a/g;->b:Landroid/os/Bundle;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/wallet/dynamite/service/a/c;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;)V

    goto :goto_1

    :pswitch_2
    iget-object v0, v2, Lcom/google/android/gms/wallet/dynamite/service/a/g;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/wallet/MaskedWallet;

    iget-object v2, v2, Lcom/google/android/gms/wallet/dynamite/service/a/g;->b:Landroid/os/Bundle;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/wallet/dynamite/service/a/c;->a(Lcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    goto :goto_1

    :pswitch_3
    iget-object v0, v2, Lcom/google/android/gms/wallet/dynamite/service/a/g;->b:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/dynamite/service/a/c;->b(Landroid/os/Bundle;)V

    goto :goto_1

    :pswitch_4
    iget-object v0, v2, Lcom/google/android/gms/wallet/dynamite/service/a/g;->b:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/dynamite/service/a/c;->c(Landroid/os/Bundle;)V

    goto :goto_1

    .line 295
    :cond_1
    return-void

    .line 294
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->e:Lcom/google/android/gms/wallet/dynamite/service/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/dynamite/service/a/b;->c_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->e:Lcom/google/android/gms/wallet/dynamite/service/a/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/dynamite/service/a/b;->b(Landroid/os/Bundle;)V

    .line 137
    :goto_0
    return-void

    .line 133
    :cond_0
    new-instance v0, Lcom/google/android/gms/wallet/dynamite/service/a/g;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/dynamite/service/a/g;-><init>()V

    .line 134
    iput-object p1, v0, Lcom/google/android/gms/wallet/dynamite/service/a/g;->b:Landroid/os/Bundle;

    .line 135
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->c:[Lcom/google/android/gms/wallet/dynamite/service/a/g;

    const/4 v2, 0x4

    aput-object v0, v1, v2

    goto :goto_0
.end method

.method public final f_(I)V
    .locals 0

    .prologue
    .line 299
    return-void
.end method
