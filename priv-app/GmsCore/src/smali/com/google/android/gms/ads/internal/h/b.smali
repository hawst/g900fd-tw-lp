.class public final Lcom/google/android/gms/ads/internal/h/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;


# instance fields
.field private final a:Lcom/google/android/gms/ads/internal/h/c;

.field private final b:Lcom/google/android/gms/ads/internal/h/d;

.field private final c:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/h/c;)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/ads/internal/h/b;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/h/c;B)V

    .line 53
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/h/c;B)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/h/b;->c:Ljava/lang/Object;

    .line 58
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/h/b;->a:Lcom/google/android/gms/ads/internal/h/c;

    .line 59
    new-instance v0, Lcom/google/android/gms/ads/internal/h/d;

    invoke-direct {v0, p1, p0, p0}, Lcom/google/android/gms/ads/internal/h/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/h/b;->b:Lcom/google/android/gms/ads/internal/h/d;

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/h/b;->b:Lcom/google/android/gms/ads/internal/h/d;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/h/d;->a()V

    .line 64
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/h/b;->a:Lcom/google/android/gms/ads/internal/h/c;

    invoke-static {}, Lcom/google/android/gms/ads/internal/d/b;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/ads/internal/h/c;->a(Landroid/os/Bundle;)V

    .line 70
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 74
    invoke-static {}, Lcom/google/android/gms/ads/internal/d/b;->a()Landroid/os/Bundle;

    move-result-object v1

    .line 75
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/h/b;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 77
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/h/b;->b:Lcom/google/android/gms/ads/internal/h/d;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/h/d;->c()Lcom/google/android/gms/ads/internal/h/e;

    move-result-object v0

    .line 78
    if-eqz v0, :cond_7

    .line 79
    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/h/e;->a()Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 86
    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/h/b;->b:Lcom/google/android/gms/ads/internal/h/d;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/h/d;->c_()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/h/b;->b:Lcom/google/android/gms/ads/internal/h/d;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/h/d;->n_()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 87
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/h/b;->b:Lcom/google/android/gms/ads/internal/h/d;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/h/d;->b()V

    .line 90
    :cond_1
    :goto_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 91
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/h/b;->a:Lcom/google/android/gms/ads/internal/h/c;

    invoke-interface {v1, v0}, Lcom/google/android/gms/ads/internal/h/c;->a(Landroid/os/Bundle;)V

    .line 92
    return-void

    .line 81
    :catch_0
    move-exception v0

    .line 82
    :try_start_2
    const-string v3, "Error when get Gservice values"

    invoke-static {v3, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 86
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/h/b;->b:Lcom/google/android/gms/ads/internal/h/d;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/h/d;->c_()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/h/b;->b:Lcom/google/android/gms/ads/internal/h/d;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/h/d;->n_()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 87
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/h/b;->b:Lcom/google/android/gms/ads/internal/h/d;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/h/d;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v0, v1

    goto :goto_1

    .line 83
    :catch_1
    move-exception v0

    .line 84
    :try_start_4
    const-string v3, "Error when get Gservice values"

    invoke-static {v3, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 86
    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/h/b;->b:Lcom/google/android/gms/ads/internal/h/d;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/h/d;->c_()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/h/b;->b:Lcom/google/android/gms/ads/internal/h/d;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/h/d;->n_()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 87
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/h/b;->b:Lcom/google/android/gms/ads/internal/h/d;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/h/d;->b()V

    move-object v0, v1

    goto :goto_1

    .line 86
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/h/b;->b:Lcom/google/android/gms/ads/internal/h/d;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/h/d;->c_()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/h/b;->b:Lcom/google/android/gms/ads/internal/h/d;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/h/d;->n_()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 87
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/h/b;->b:Lcom/google/android/gms/ads/internal/h/d;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/h/d;->b()V

    :cond_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 90
    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_6
    move-object v0, v1

    goto :goto_1

    :cond_7
    move-object v0, v1

    goto :goto_0
.end method

.method public final f_(I)V
    .locals 1

    .prologue
    .line 96
    const-string v0, "Disconnected from remote ad request service."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 97
    return-void
.end method
