.class final Lcom/google/android/gms/droidguard/j;
.super Lcom/google/android/gms/droidguard/c/q;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/droidguard/DroidGuardService;

.field private final b:Ljava/lang/String;

.field private final c:Landroid/os/ConditionVariable;

.field private d:Ljava/lang/String;

.field private e:Lcom/google/android/gms/droidguard/d/ah;

.field private f:Ljava/lang/Throwable;


# direct methods
.method constructor <init>(Lcom/google/android/gms/droidguard/DroidGuardService;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 149
    iput-object p1, p0, Lcom/google/android/gms/droidguard/j;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-direct {p0}, Lcom/google/android/gms/droidguard/c/q;-><init>()V

    .line 144
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/droidguard/j;->c:Landroid/os/ConditionVariable;

    .line 150
    iput-object p2, p0, Lcom/google/android/gms/droidguard/j;->b:Ljava/lang/String;

    .line 151
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/droidguard/j;)Landroid/os/ConditionVariable;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/gms/droidguard/j;->c:Landroid/os/ConditionVariable;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/droidguard/j;Lcom/google/android/gms/droidguard/d/ah;)Lcom/google/android/gms/droidguard/d/ah;
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/google/android/gms/droidguard/j;->e:Lcom/google/android/gms/droidguard/d/ah;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/droidguard/j;Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/google/android/gms/droidguard/j;->f:Ljava/lang/Throwable;

    return-object p1
.end method

.method private a(Ljava/util/Map;Ljava/lang/Throwable;)[B
    .locals 3

    .prologue
    .line 172
    new-instance v0, Lcom/google/android/gms/droidguard/b/f;

    iget-object v1, p0, Lcom/google/android/gms/droidguard/j;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/droidguard/j;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-direct {v0, v1, v2, p2}, Lcom/google/android/gms/droidguard/b/f;-><init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/Throwable;)V

    .line 173
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/droidguard/b/f;->a(Ljava/util/Map;[B)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/droidguard/j;->c:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/droidguard/j;->e:Lcom/google/android/gms/droidguard/d/ah;

    if-eqz v0, :cond_0

    .line 197
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/droidguard/j;->e:Lcom/google/android/gms/droidguard/d/ah;

    iget-object v1, v0, Lcom/google/android/gms/droidguard/d/ah;->a:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "close"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/droidguard/d/ah;->a:Ljava/lang/Object;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    :cond_0
    :goto_0
    iput-object v4, p0, Lcom/google/android/gms/droidguard/j;->e:Lcom/google/android/gms/droidguard/d/ah;

    .line 203
    iput-object v4, p0, Lcom/google/android/gms/droidguard/j;->f:Ljava/lang/Throwable;

    .line 204
    return-void

    .line 198
    :catch_0
    move-exception v0

    .line 199
    const-string v1, "DroidGuardService"

    const-string v2, "Close failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 154
    iput-object p1, p0, Lcom/google/android/gms/droidguard/j;->d:Ljava/lang/String;

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/droidguard/j;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/DroidGuardService;->b(Lcom/google/android/gms/droidguard/DroidGuardService;)Lcom/google/android/gms/droidguard/d/e;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/droidguard/j;->b:Ljava/lang/String;

    new-instance v2, Lcom/google/android/gms/droidguard/n;

    iget-object v3, p0, Lcom/google/android/gms/droidguard/j;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    iget-object v4, p0, Lcom/google/android/gms/droidguard/j;->b:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/droidguard/n;-><init>(Lcom/google/android/gms/droidguard/DroidGuardService;Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/gms/droidguard/d/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/droidguard/d/k;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/droidguard/d/g;

    invoke-direct {v2, v0}, Lcom/google/android/gms/droidguard/d/g;-><init>(Lcom/google/android/gms/droidguard/d/e;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/droidguard/d/k;->a(Lcom/google/android/gms/droidguard/d/n;)Lcom/google/android/gms/droidguard/d/k;

    move-result-object v0

    .line 157
    new-instance v1, Lcom/google/android/gms/droidguard/k;

    invoke-direct {v1, p0}, Lcom/google/android/gms/droidguard/k;-><init>(Lcom/google/android/gms/droidguard/j;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/droidguard/d/k;->a(Lcom/google/android/gms/droidguard/d/m;)Lcom/google/android/gms/droidguard/d/k;

    .line 169
    return-void
.end method

.method public final a(Ljava/util/Map;)[B
    .locals 6

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/gms/droidguard/j;->c:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/droidguard/j;->f:Ljava/lang/Throwable;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/droidguard/j;->f:Ljava/lang/Throwable;

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/droidguard/j;->a(Ljava/util/Map;Ljava/lang/Throwable;)[B

    move-result-object v0

    .line 188
    :goto_0
    return-object v0

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/droidguard/j;->e:Lcom/google/android/gms/droidguard/d/ah;

    if-eqz v0, :cond_1

    .line 183
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/droidguard/j;->e:Lcom/google/android/gms/droidguard/d/ah;

    iget-object v1, v0, Lcom/google/android/gms/droidguard/d/ah;->a:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "ss"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/util/Map;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/droidguard/d/ah;->a:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 184
    :catch_0
    move-exception v0

    .line 185
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/droidguard/j;->a(Ljava/util/Map;Ljava/lang/Throwable;)[B

    move-result-object v0

    goto :goto_0

    .line 188
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/droidguard/j;->a(Ljava/util/Map;Ljava/lang/Throwable;)[B

    move-result-object v0

    goto :goto_0
.end method
