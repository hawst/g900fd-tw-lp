.class public Lcom/google/android/gms/kids/account/receiver/UpdateProfileOwnerReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 55
    return-void
.end method


# virtual methods
.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 27
    monitor-enter p0

    :try_start_0
    const-string v0, "UpdateProfileOwnerReceiver"

    const-string v1, "onReceive: action=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    const-string v0, "is_kid_account_removed"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 30
    const-string v1, "account_to_operate_on"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 32
    if-nez v0, :cond_1

    .line 33
    const-class v0, Lcom/google/android/gms/kids/restrictions/receiver/RestrictionChangedReceiver;

    invoke-static {p1, v0}, Lcom/google/android/gms/kids/common/a;->a(Landroid/content/Context;Ljava/lang/Class;)V

    const-class v0, Lcom/google/android/gms/kids/restrictions/receiver/ManualSyncReceiver;

    invoke-static {p1, v0}, Lcom/google/android/gms/kids/common/a;->a(Landroid/content/Context;Ljava/lang/Class;)V

    const-class v0, Lcom/google/android/gms/kids/account/receiver/ProfileOwnerSetupCompleteReceiver;

    invoke-static {p1, v0}, Lcom/google/android/gms/kids/common/a;->a(Landroid/content/Context;Ljava/lang/Class;)V

    .line 34
    const-string v0, "device_policy"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/admin/DevicePolicyManager;->isProfileOwnerApp(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "UpdateProfileOwnerReceiver"

    const-string v2, "Firing intent for RegisterProfileOwnerActivity"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gms/kids/account/activities/RegisterProfileOwnerActivity;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "account_to_operate_on"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    :goto_0
    monitor-exit p0

    return-void

    .line 34
    :cond_0
    :try_start_1
    const-string v0, "UpdateProfileOwnerReceiver"

    const-string v1, "Profile owner already set"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 27
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 36
    :cond_1
    :try_start_2
    new-instance v0, Lcom/google/android/gms/kids/account/receiver/b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/kids/account/receiver/b;-><init>(B)V

    invoke-static {p1, v0}, Lcom/google/android/gms/kids/common/service/KidsApiService;->a(Landroid/content/Context;Lcom/google/android/gms/kids/common/c/b;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
