.class public Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Landroid/accounts/Account;

.field private final b:Ljava/lang/String;

.field private c:Lcom/google/aa/b/a/a/a/a/c;

.field private d:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/c;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/service/ow/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/aa/b/a/a/a/a/c;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->a:Landroid/accounts/Account;

    .line 26
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->b:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->c:Lcom/google/aa/b/a/a/a/a/c;

    .line 28
    return-void
.end method

.method private constructor <init>(Landroid/accounts/Account;Ljava/lang/String;[B)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->a:Landroid/accounts/Account;

    .line 33
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->b:Ljava/lang/String;

    .line 34
    iput-object p3, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->d:[B

    .line 35
    return-void
.end method

.method synthetic constructor <init>(Landroid/accounts/Account;Ljava/lang/String;[BB)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;-><init>(Landroid/accounts/Account;Ljava/lang/String;[B)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->a:Landroid/accounts/Account;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/google/aa/b/a/a/a/a/c;
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->c:Lcom/google/aa/b/a/a/a/a/c;

    if-nez v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->d:[B

    const-class v1, Lcom/google/aa/b/a/a/a/a/c;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/c;

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->c:Lcom/google/aa/b/a/a/a/a/c;

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->c:Lcom/google/aa/b/a/a/a/a/c;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->a:Landroid/accounts/Account;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->d:[B

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->c:Lcom/google/aa/b/a/a/a/a/c;

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->d:[B

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->d:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 88
    return-void
.end method
