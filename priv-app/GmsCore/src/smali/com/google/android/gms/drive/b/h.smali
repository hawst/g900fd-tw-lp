.class public final Lcom/google/android/gms/drive/b/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/b/g;


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/model/a;

.field private final b:Lcom/google/android/gms/drive/auth/AppIdentity;

.field private final c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

.field private final d:J

.field private final e:Lcom/google/android/gms/drive/DriveId;

.field private final f:Lcom/google/android/gms/drive/a/a/a;

.field private final g:Lcom/google/android/gms/drive/a/a/l;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/a/a/a;Lcom/google/android/gms/drive/a/a/l;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/gms/drive/b/h;->a:Lcom/google/android/gms/drive/database/model/a;

    .line 36
    iput-object p2, p0, Lcom/google/android/gms/drive/b/h;->b:Lcom/google/android/gms/drive/auth/AppIdentity;

    .line 37
    iput-object p3, p0, Lcom/google/android/gms/drive/b/h;->c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 38
    iput-wide p4, p0, Lcom/google/android/gms/drive/b/h;->d:J

    .line 39
    iput-object p6, p0, Lcom/google/android/gms/drive/b/h;->e:Lcom/google/android/gms/drive/DriveId;

    .line 40
    iput-object p7, p0, Lcom/google/android/gms/drive/b/h;->f:Lcom/google/android/gms/drive/a/a/a;

    .line 41
    iput-object p8, p0, Lcom/google/android/gms/drive/b/h;->g:Lcom/google/android/gms/drive/a/a/l;

    .line 42
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 21
    new-instance v1, Lcom/google/android/gms/drive/a/l;

    iget-object v2, p0, Lcom/google/android/gms/drive/b/h;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v3, p0, Lcom/google/android/gms/drive/b/h;->b:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-object v5, p0, Lcom/google/android/gms/drive/b/h;->c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iget-wide v6, p0, Lcom/google/android/gms/drive/b/h;->d:J

    iget-object v8, p0, Lcom/google/android/gms/drive/b/h;->e:Lcom/google/android/gms/drive/DriveId;

    iget-object v9, p0, Lcom/google/android/gms/drive/b/h;->g:Lcom/google/android/gms/drive/a/a/l;

    move-object v4, p1

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/drive/a/l;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/a/a/l;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/b/h;->f:Lcom/google/android/gms/drive/a/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/a/a/a;->a(Lcom/google/android/gms/drive/a/c;)I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    new-instance v2, Lcom/google/android/gms/drive/b/a;

    invoke-direct {v2, v0, v1}, Lcom/google/android/gms/drive/b/a;-><init>(ILcom/google/android/gms/drive/a/c;)V

    return-object v2

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
