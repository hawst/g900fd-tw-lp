.class public final Lcom/google/android/gms/reminders/service/a/b;
.super Lcom/google/android/gms/reminders/service/a/a;
.source "SourceFile"


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/reminders/internal/b;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/gms/reminders/service/a/a;-><init>(Lcom/google/android/gms/reminders/internal/b;)V

    .line 19
    const-string v0, "RemindersService"

    iput-object v0, p0, Lcom/google/android/gms/reminders/service/a/b;->b:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 3

    .prologue
    .line 18
    invoke-static {}, Lcom/google/android/gms/reminders/service/b;->a()Lcom/google/android/gms/reminders/service/b;

    invoke-static {}, Lcom/google/android/gms/reminders/service/b;->a()Lcom/google/android/gms/reminders/service/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/reminders/service/a/b;->a:Lcom/google/android/gms/reminders/internal/b;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/reminders/service/b;->a(Lcom/google/android/gms/reminders/internal/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/reminders/service/a/b;->a:Lcom/google/android/gms/reminders/internal/b;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/reminders/internal/b;->a(Lcom/google/android/gms/common/api/Status;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "RemindersService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Duplicate listener "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/reminders/service/a/b;->a:Lcom/google/android/gms/reminders/internal/b;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/reminders/service/a/b;->a:Lcom/google/android/gms/reminders/internal/b;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0xa

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/reminders/internal/b;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0
.end method
