.class public final Lcom/google/android/gms/wallet/common/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:[Ljava/lang/String;

.field private final b:I

.field private c:I


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    if-eqz p1, :cond_0

    .line 172
    invoke-static {}, Lcom/google/android/gms/wallet/common/af;->b()Ljava/util/regex/Pattern;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ag;->a:[Ljava/lang/String;

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ag;->a:[Ljava/lang/String;

    array-length v0, v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ag;->b:I

    .line 178
    :goto_0
    return-void

    .line 175
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ag;->a:[Ljava/lang/String;

    .line 176
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ag;->b:I

    goto :goto_0
.end method

.method synthetic constructor <init>(Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 164
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ag;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ag;->c()Ljava/lang/String;

    move-result-object v0

    .line 210
    if-eqz v0, :cond_0

    .line 212
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 217
    :goto_0
    return v0

    .line 214
    :catch_0
    move-exception v0

    const-string v0, "StringSerializer"

    const-string v1, "Unable to parse int"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(J)J
    .locals 3

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ag;->c()Ljava/lang/String;

    move-result-object v0

    .line 226
    if-eqz v0, :cond_0

    .line 228
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p1

    .line 233
    :cond_0
    :goto_0
    return-wide p1

    .line 230
    :catch_0
    move-exception v0

    const-string v0, "StringSerializer"

    const-string v1, "Unable to parse long"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Ljava/lang/Class;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;
    .locals 3

    .prologue
    .line 281
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ag;->c()Ljava/lang/String;

    move-result-object v0

    .line 282
    if-eqz v0, :cond_0

    .line 284
    :try_start_0
    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    .line 289
    :cond_0
    :goto_0
    return-object p2

    .line 286
    :catch_0
    move-exception v0

    const-string v0, "StringSerializer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to parse proto "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Z)Z
    .locals 2

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ag;->c()Ljava/lang/String;

    move-result-object v0

    .line 194
    if-eqz v0, :cond_0

    .line 196
    :try_start_0
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 201
    :cond_0
    :goto_0
    return p1

    .line 198
    :catch_0
    move-exception v0

    const-string v0, "StringSerializer"

    const-string v1, "Unable to parse boolean"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Ljava/lang/Class;[Lcom/google/protobuf/nano/j;)[Lcom/google/protobuf/nano/j;
    .locals 5

    .prologue
    .line 297
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ag;->c()Ljava/lang/String;

    move-result-object v0

    .line 298
    if-eqz v0, :cond_1

    .line 299
    invoke-static {}, Lcom/google/android/gms/wallet/common/af;->c()Ljava/util/regex/Pattern;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;I)[Ljava/lang/String;

    move-result-object v2

    .line 300
    array-length v3, v2

    .line 302
    invoke-static {p1, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/protobuf/nano/j;

    .line 303
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    .line 304
    aget-object v4, v2, v1

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 306
    :try_start_0
    aget-object v4, v2, v1

    invoke-static {v4, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v4

    aput-object v4, v0, v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 303
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 308
    :catch_0
    move-exception v0

    const-string v0, "StringSerializer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to parse proto in array "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    :cond_1
    :goto_1
    return-object p2

    :cond_2
    move-object p2, v0

    .line 313
    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ag;->c()Ljava/lang/String;

    move-result-object v0

    .line 258
    if-eqz v0, :cond_0

    .line 261
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 319
    iget v0, p0, Lcom/google/android/gms/wallet/common/ag;->c:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/gms/wallet/common/ag;->c:I

    .line 320
    iget v1, p0, Lcom/google/android/gms/wallet/common/ag;->b:I

    if-ge v0, v1, :cond_0

    .line 321
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ag;->a:[Ljava/lang/String;

    aget-object v0, v1, v0

    .line 322
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 326
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
