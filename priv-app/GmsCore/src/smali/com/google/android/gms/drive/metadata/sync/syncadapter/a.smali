.class public final Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/r;

.field private final b:Lcom/google/android/gms/drive/d/f;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/d/f;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/r;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;->a:Lcom/google/android/gms/drive/database/r;

    .line 30
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/d/f;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;->b:Lcom/google/android/gms/drive/d/f;

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/g/aw;)V
    .locals 2

    .prologue
    .line 24
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->o()Lcom/google/android/gms/drive/d/f;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;-><init>(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/d/f;)V

    .line 25
    return-void
.end method

.method private b(Lcom/google/android/gms/drive/auth/g;)V
    .locals 3

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;->b:Lcom/google/android/gms/drive/d/f;

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/d/f;->a(Lcom/google/android/gms/drive/auth/g;)Ljava/lang/String;

    move-result-object v0

    .line 64
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->b()V

    .line 68
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1, p1}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v1

    .line 69
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/database/model/ah;->k(Ljava/lang/String;)V

    .line 71
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/i;

    move-result-object v0

    .line 74
    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/google/android/gms/drive/database/model/i;->b:Z

    .line 75
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/i;->i()V

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->e()V

    .line 79
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ah;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->c()V

    .line 82
    return-void

    .line 81
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->c()V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/auth/g;)V
    .locals 4

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;->a:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/database/r;->d(Lcom/google/android/gms/drive/database/model/a;)Ljava/util/Set;

    move-result-object v0

    .line 57
    iget-wide v2, p1, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;->b(Lcom/google/android/gms/drive/auth/g;)V

    .line 60
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/a;)V
    .locals 6

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/database/r;->d(Lcom/google/android/gms/drive/database/model/a;)Ljava/util/Set;

    move-result-object v0

    .line 40
    iget-wide v2, p1, Lcom/google/android/gms/drive/database/model/a;->b:J

    .line 41
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0, v2, v3, v4, v5}, Lcom/google/android/gms/drive/database/r;->a(JJ)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    .line 44
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;->b(Lcom/google/android/gms/drive/auth/g;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 45
    :catch_0
    move-exception v0

    .line 46
    const-string v4, "AppDataFolderIdUpdater"

    const-string v5, "Failed to get real appData folder from server"

    invoke-static {v4, v0, v5}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0

    .line 49
    :cond_0
    return-void
.end method
