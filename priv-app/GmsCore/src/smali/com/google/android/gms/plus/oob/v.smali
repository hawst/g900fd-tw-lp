.class public final Lcom/google/android/gms/plus/oob/v;
.super Lcom/google/android/gms/plus/oob/y;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private e:Lcom/google/android/gms/plus/oob/w;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/y;-><init>()V

    .line 27
    return-void
.end method

.method public static a(ZLcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;Ljava/lang/String;)Lcom/google/android/gms/plus/oob/v;
    .locals 3

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/gms/plus/oob/v;

    invoke-direct {v0}, Lcom/google/android/gms/plus/oob/v;-><init>()V

    .line 49
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 50
    const-string v2, "is_setup_wizard_theme"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 51
    const-string v2, "upgrade_account"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 52
    const-string v2, "error_id"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/v;->setArguments(Landroid/os/Bundle;)V

    .line 54
    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 128
    return-void
.end method

.method protected final a(Landroid/view/LayoutInflater;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 137
    sget v1, Lcom/google/android/gms/l;->dW:I

    sget v0, Lcom/google/android/gms/j;->cn:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 138
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 4

    .prologue
    .line 105
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/oob/y;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/v;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    .line 109
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/v;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 111
    instance-of v3, v0, Lcom/google/android/gms/plus/oob/k;

    if-eqz v3, :cond_0

    .line 112
    check-cast v0, Lcom/google/android/gms/plus/oob/k;

    .line 113
    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/k;->m()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 114
    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/k;->k()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 115
    iget-object v3, p0, Lcom/google/android/gms/plus/oob/v;->c:Landroid/widget/Button;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/k;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 109
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 116
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/k;->l()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 117
    iget-object v3, p0, Lcom/google/android/gms/plus/oob/v;->d:Landroid/widget/Button;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/k;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 122
    :cond_2
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 133
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 59
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/oob/y;->onAttach(Landroid/app/Activity;)V

    .line 60
    instance-of v0, p1, Lcom/google/android/gms/plus/oob/w;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/gms/plus/oob/w;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_0
    check-cast p1, Lcom/google/android/gms/plus/oob/w;

    iput-object p1, p0, Lcom/google/android/gms/plus/oob/v;->e:Lcom/google/android/gms/plus/oob/w;

    .line 65
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 89
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 90
    sget v1, Lcom/google/android/gms/j;->bO:I

    if-ne v0, v1, :cond_1

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/v;->a:Lcom/google/android/gms/plus/oob/e;

    sget-object v1, Lcom/google/android/gms/common/analytics/m;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/v;->a:Lcom/google/android/gms/plus/oob/e;

    invoke-interface {v2}, Lcom/google/android/gms/plus/oob/e;->b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/oob/e;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/v;->e:Lcom/google/android/gms/plus/oob/w;

    invoke-interface {v0}, Lcom/google/android/gms/plus/oob/w;->d()V

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    sget v1, Lcom/google/android/gms/j;->mw:I

    if-ne v0, v1, :cond_0

    .line 95
    const-string v0, "invalidNameHardFail"

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/v;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/v;->e:Lcom/google/android/gms/plus/oob/w;

    invoke-interface {v0}, Lcom/google/android/gms/plus/oob/w;->c()V

    goto :goto_0

    .line 99
    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/oob/y;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 70
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/plus/oob/y;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/v;->d:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 74
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/v;->d:Landroid/widget/Button;

    sget v2, Lcom/google/android/gms/p;->td:I

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 77
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/v;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "error_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/v;->f:Ljava/lang/String;

    .line 78
    const-string v1, "invalidNameHardFail"

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/v;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/v;->c:Landroid/widget/Button;

    sget v2, Lcom/google/android/gms/p;->te:I

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 84
    :goto_0
    return-object v0

    .line 81
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/v;->c:Landroid/widget/Button;

    sget v2, Lcom/google/android/gms/p;->tc:I

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    goto :goto_0
.end method
