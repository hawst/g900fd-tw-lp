.class public final Lcom/google/android/gms/location/copresence/internal/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field d:Landroid/os/IBinder;

.field private final e:Lcom/google/android/gms/location/internal/z;

.field private f:Lcom/google/android/gms/location/copresence/internal/l;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/internal/z;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/android/gms/location/copresence/internal/c;->a:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lcom/google/android/gms/location/copresence/internal/c;->b:Ljava/lang/String;

    .line 62
    iput-object p4, p0, Lcom/google/android/gms/location/copresence/internal/c;->e:Lcom/google/android/gms/location/internal/z;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/location/copresence/internal/c;->f:Lcom/google/android/gms/location/copresence/internal/l;

    .line 64
    iput-object p3, p0, Lcom/google/android/gms/location/copresence/internal/c;->c:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/internal/z;)Lcom/google/android/gms/location/copresence/internal/c;
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/c;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/location/copresence/internal/c;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/internal/z;)V

    return-object v0
.end method


# virtual methods
.method final a()Lcom/google/android/gms/location/copresence/internal/l;
    .locals 3

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/internal/c;->e:Lcom/google/android/gms/location/internal/z;

    invoke-interface {v0}, Lcom/google/android/gms/location/internal/z;->a()V

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/internal/c;->f:Lcom/google/android/gms/location/copresence/internal/l;

    if-nez v0, :cond_0

    .line 71
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/internal/c;->e:Lcom/google/android/gms/location/internal/z;

    invoke-interface {v0}, Lcom/google/android/gms/location/internal/z;->b()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/q;

    invoke-interface {v0}, Lcom/google/android/gms/location/internal/q;->b()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/location/copresence/internal/m;->a(Landroid/os/IBinder;)Lcom/google/android/gms/location/copresence/internal/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/location/copresence/internal/c;->f:Lcom/google/android/gms/location/copresence/internal/l;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/internal/c;->f:Lcom/google/android/gms/location/copresence/internal/l;

    return-object v0

    .line 73
    :catch_0
    move-exception v0

    .line 75
    const-string v1, "CopresenceClientImpl"

    const-string v2, "Could not get copresence service interface."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
