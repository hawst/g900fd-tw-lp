.class final Lcom/google/android/gms/wallet/common/ui/di;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/di;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/di;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/di;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->a(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;Z)V

    .line 191
    new-instance v0, Lcom/google/checkout/inapp/proto/ao;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/ao;-><init>()V

    .line 192
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/di;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->a(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v1

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/ao;->b:Ljava/lang/String;

    .line 193
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/di;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->d:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h()Lcom/google/t/a/b;

    move-result-object v1

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/ao;->c:Lcom/google/t/a/b;

    .line 194
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/di;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->b(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/di;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->d:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 196
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/di;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->d:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/ao;->d:Ljava/lang/String;

    .line 198
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/di;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->c(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 199
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/di;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->c(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/ao;->e:Ljava/lang/String;

    .line 201
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/di;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->d(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Lcom/google/checkout/inapp/proto/q;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 202
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/di;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->d(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Lcom/google/checkout/inapp/proto/q;

    move-result-object v1

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/ao;->a:Lcom/google/checkout/inapp/proto/q;

    .line 204
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/di;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->e(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 205
    iget-object v1, v0, Lcom/google/checkout/inapp/proto/ao;->c:Lcom/google/t/a/b;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/di;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/t/a/b;->t:Ljava/lang/String;

    .line 208
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/di;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->h()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/di;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->e(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Z

    move-result v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/checkout/inapp/proto/ao;Z)V

    .line 213
    :goto_0
    return-void

    .line 211
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/di;->a:Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->g()Z

    goto :goto_0
.end method
