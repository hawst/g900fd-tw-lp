.class public Lcom/google/android/gms/drive/api/a/x;
.super Lcom/google/android/gms/drive/api/a/b;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/internal/ListParentsRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ListParentsRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0xc

    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;I)V

    .line 29
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/x;->e:Lcom/google/android/gms/drive/internal/ListParentsRequest;

    .line 30
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/x;->e:Lcom/google/android/gms/drive/internal/ListParentsRequest;

    const-string v1, "Invalid getParents request: request must be provided"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/x;->e:Lcom/google/android/gms/drive/internal/ListParentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/ListParentsRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    const-string v1, "Invalid getParents request: DriveId must be provided"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/x;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/x;->e:Lcom/google/android/gms/drive/internal/ListParentsRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/ListParentsRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/x;->c:Lcom/google/android/gms/drive/c/a;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/api/d;->c(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/c/a;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    .line 41
    new-instance v0, Lcom/google/android/gms/drive/internal/OnListParentsResponse;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/internal/OnListParentsResponse;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 44
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/x;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-interface {v2, v0}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/drive/internal/OnListParentsResponse;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/OnListParentsResponse;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 51
    :cond_0
    return-void

    .line 47
    :catchall_0
    move-exception v0

    .line 48
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method
