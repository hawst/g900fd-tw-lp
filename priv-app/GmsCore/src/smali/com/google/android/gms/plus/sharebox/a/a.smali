.class public final Lcom/google/android/gms/plus/sharebox/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/os/Bundle;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "At least one of url or deepLinkId is required."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/a/a;->a:Landroid/os/Bundle;

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/a/a;->a:Landroid/os/Bundle;

    const-string v1, "label"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/a/a;->a:Landroid/os/Bundle;

    const-string v1, "url"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/a/a;->a:Landroid/os/Bundle;

    const-string v1, "deepLinkId"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/google/android/gms/plus/sharebox/a/a;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 49
    if-nez p0, :cond_1

    .line 62
    :cond_0
    :goto_0
    return-object v0

    .line 53
    :cond_1
    const-string v1, "label"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 54
    const-string v2, "url"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 55
    const-string v3, "deepLinkId"

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 58
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 62
    :cond_2
    new-instance v0, Lcom/google/android/gms/plus/sharebox/a/a;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/plus/sharebox/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/an;)Lcom/google/android/gms/plus/service/v1whitelisted/models/an;
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 105
    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/am;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/am;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/a/a;->a:Landroid/os/Bundle;

    const-string v2, "deepLinkId"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/am;->a:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/am;->c:Ljava/util/Set;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/a/a;->a:Landroid/os/Bundle;

    const-string v2, "url"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/am;->b:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/am;->c:Ljava/util/Set;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ActionEntity$DeepLinkEntity;

    iget-object v2, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/am;->c:Ljava/util/Set;

    iget-object v3, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/am;->a:Ljava/lang/String;

    iget-object v1, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/am;->b:Ljava/lang/String;

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ActionEntity$DeepLinkEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/ak;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ak;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/a/a;->a:Landroid/os/Bundle;

    const-string v3, "label"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/ak;->c:Ljava/lang/String;

    iget-object v2, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/ak;->d:Ljava/util/Set;

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ActionEntity$DeepLinkEntity;

    iput-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/ak;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ActionEntity$DeepLinkEntity;

    iget-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/ak;->d:Ljava/util/Set;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ActionEntity;

    iget-object v2, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/ak;->d:Ljava/util/Set;

    iget-object v3, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/ak;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ActionEntity$DeepLinkEntity;

    iget-object v4, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/ak;->b:Ljava/lang/String;

    iget-object v1, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/ak;->c:Ljava/lang/String;

    invoke-direct {v0, v2, v3, v4, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ActionEntity;-><init>(Ljava/util/Set;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ActionEntity$DeepLinkEntity;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ActionEntity;

    iput-object v0, p1, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ActionEntity;

    iget-object v0, p1, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->g:Ljava/util/Set;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 115
    const-string v0, "action"

    iput-object v0, p1, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->f:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->g:Ljava/util/Set;

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 116
    return-object p1
.end method
