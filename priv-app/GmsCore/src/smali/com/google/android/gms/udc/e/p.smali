.class public final Lcom/google/android/gms/udc/e/p;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/android/gms/udc/e/p;


# instance fields
.field public a:I

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 44
    iput v0, p0, Lcom/google/android/gms/udc/e/p;->a:I

    iput v0, p0, Lcom/google/android/gms/udc/e/p;->b:I

    iput v0, p0, Lcom/google/android/gms/udc/e/p;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/udc/e/p;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/udc/e/p;->cachedSize:I

    .line 45
    return-void
.end method

.method public static a()[Lcom/google/android/gms/udc/e/p;
    .locals 2

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/gms/udc/e/p;->d:[Lcom/google/android/gms/udc/e/p;

    if-nez v0, :cond_1

    .line 24
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 26
    :try_start_0
    sget-object v0, Lcom/google/android/gms/udc/e/p;->d:[Lcom/google/android/gms/udc/e/p;

    if-nez v0, :cond_0

    .line 27
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/udc/e/p;

    sput-object v0, Lcom/google/android/gms/udc/e/p;->d:[Lcom/google/android/gms/udc/e/p;

    .line 29
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31
    :cond_1
    sget-object v0, Lcom/google/android/gms/udc/e/p;->d:[Lcom/google/android/gms/udc/e/p;

    return-object v0

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 104
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 105
    iget v1, p0, Lcom/google/android/gms/udc/e/p;->a:I

    if-eqz v1, :cond_0

    .line 106
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/udc/e/p;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    :cond_0
    iget v1, p0, Lcom/google/android/gms/udc/e/p;->b:I

    if-eqz v1, :cond_1

    .line 110
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/udc/e/p;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 113
    :cond_1
    iget v1, p0, Lcom/google/android/gms/udc/e/p;->c:I

    if-eqz v1, :cond_2

    .line 114
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/udc/e/p;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 117
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 58
    if-ne p1, p0, :cond_1

    .line 59
    const/4 v0, 0x1

    .line 74
    :cond_0
    :goto_0
    return v0

    .line 61
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/udc/e/p;

    if-eqz v1, :cond_0

    .line 64
    check-cast p1, Lcom/google/android/gms/udc/e/p;

    .line 65
    iget v1, p0, Lcom/google/android/gms/udc/e/p;->a:I

    iget v2, p1, Lcom/google/android/gms/udc/e/p;->a:I

    if-ne v1, v2, :cond_0

    .line 68
    iget v1, p0, Lcom/google/android/gms/udc/e/p;->b:I

    iget v2, p1, Lcom/google/android/gms/udc/e/p;->b:I

    if-ne v1, v2, :cond_0

    .line 71
    iget v1, p0, Lcom/google/android/gms/udc/e/p;->c:I

    iget v2, p1, Lcom/google/android/gms/udc/e/p;->c:I

    if-ne v1, v2, :cond_0

    .line 74
    invoke-virtual {p0, p1}, Lcom/google/android/gms/udc/e/p;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 79
    iget v0, p0, Lcom/google/android/gms/udc/e/p;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 81
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/udc/e/p;->b:I

    add-int/2addr v0, v1

    .line 82
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/udc/e/p;->c:I

    add-int/2addr v0, v1

    .line 83
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/udc/e/p;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/udc/e/p;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/udc/e/p;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/udc/e/p;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/android/gms/udc/e/p;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 90
    iget v0, p0, Lcom/google/android/gms/udc/e/p;->a:I

    if-eqz v0, :cond_0

    .line 91
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/udc/e/p;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 93
    :cond_0
    iget v0, p0, Lcom/google/android/gms/udc/e/p;->b:I

    if-eqz v0, :cond_1

    .line 94
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/udc/e/p;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 96
    :cond_1
    iget v0, p0, Lcom/google/android/gms/udc/e/p;->c:I

    if-eqz v0, :cond_2

    .line 97
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/udc/e/p;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 99
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 100
    return-void
.end method
