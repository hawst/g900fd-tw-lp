.class final Lcom/google/android/gms/people/service/a/a/m;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;Ljava/lang/String;Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;)Lcom/google/android/gms/people/service/a/a/e;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 88
    invoke-static {p0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v0

    invoke-virtual {v0, p4, v6}, Lcom/google/android/gms/people/c/b;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 96
    :cond_0
    :goto_0
    return-object v6

    .line 92
    :cond_1
    new-instance v0, Lcom/google/android/gms/people/sync/ap;

    invoke-direct {v0, p0, p4, v6}, Lcom/google/android/gms/people/sync/ap;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 96
    new-instance v0, Lcom/google/android/gms/people/service/a/a/n;

    invoke-virtual {p5}, Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;->d()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v7, 0x4

    :goto_1
    const/4 v8, 0x1

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/people/service/a/a/n;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;Ljava/lang/String;Ljava/lang/String;II)V

    move-object v6, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p5}, Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;->b()I

    move-result v7

    goto :goto_1
.end method
