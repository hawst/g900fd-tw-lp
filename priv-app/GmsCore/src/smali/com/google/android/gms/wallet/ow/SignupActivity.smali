.class public Lcom/google/android/gms/wallet/ow/SignupActivity;
.super Lcom/google/android/gms/wallet/common/ui/dq;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/google/android/gms/wallet/common/a/m;
.implements Lcom/google/android/gms/wallet/common/a/q;
.implements Lcom/google/android/gms/wallet/common/ui/ah;
.implements Lcom/google/android/gms/wallet/common/ui/bf;
.implements Lcom/google/android/gms/wallet/common/ui/cq;
.implements Lcom/google/android/gms/wallet/common/ui/cr;
.implements Lcom/google/android/gms/wallet/common/ui/dp;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field A:Z

.field private B:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private C:Lcom/google/aa/b/a/a/a/a/u;

.field private D:Lcom/google/android/gms/wallet/Cart;

.field private E:Landroid/accounts/Account;

.field private F:Z

.field private G:Lcom/google/android/gms/wallet/common/ui/dh;

.field private H:Ljava/util/ArrayList;

.field private I:Z

.field private J:Lcom/google/android/apps/common/a/a/i;

.field private K:Lcom/google/android/apps/common/a/a/h;

.field private L:Lcom/google/android/gms/wallet/common/a/n;

.field private M:Lcom/google/android/gms/wallet/common/a/k;

.field private N:Z

.field private O:Z

.field private P:Ljava/util/List;

.field private Q:I

.field private R:Z

.field private S:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

.field private T:Z

.field private U:Lcom/google/checkout/inapp/proto/a/b;

.field private V:Ljava/lang/String;

.field private W:Z

.field private final X:Lcom/google/android/gms/wallet/service/l;

.field b:Landroid/support/v7/app/a;

.field c:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

.field d:Lcom/google/android/gms/wallet/common/ui/cs;

.field e:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

.field f:Landroid/widget/ProgressBar;

.field g:Lcom/google/android/gms/wallet/common/ui/f;

.field h:Landroid/widget/CheckBox;

.field i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

.field j:Landroid/widget/CheckBox;

.field k:Landroid/view/View;

.field l:Landroid/widget/TextView;

.field m:Lcom/google/android/gms/wallet/common/ui/bb;

.field n:Lcom/google/android/gms/wallet/common/ui/ca;

.field o:Lcom/google/android/gms/wallet/common/ui/bb;

.field p:Landroid/widget/TextView;

.field q:Landroid/view/ViewGroup;

.field r:Landroid/widget/CheckBox;

.field s:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field t:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field u:Landroid/widget/CheckBox;

.field v:Landroid/widget/CheckBox;

.field w:I

.field x:Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;

.field y:Ljava/lang/String;

.field z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 114
    const-string v0, "signup"

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 107
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/dq;-><init>()V

    .line 289
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Z

    .line 294
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->N:Z

    .line 295
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->O:Z

    .line 297
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->y:Ljava/lang/String;

    .line 299
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->Q:I

    .line 300
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->R:Z

    .line 301
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->z:Z

    .line 303
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->A:Z

    .line 306
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->T:Z

    .line 1163
    new-instance v0, Lcom/google/android/gms/wallet/ow/y;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ow/y;-><init>(Lcom/google/android/gms/wallet/ow/SignupActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->X:Lcom/google/android/gms/wallet/service/l;

    return-void
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/aa/b/a/a/a/a/u;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;ZLcom/google/android/gms/wallet/ImmediateFullWalletRequest;Z)Landroid/content/Intent;
    .locals 15

    .prologue
    .line 395
    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x1

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v7, p5

    move-object/from16 v13, p6

    move/from16 v14, p7

    invoke-static/range {v0 .. v14}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/aa/b/a/a/a/a/u;Lcom/google/android/gms/wallet/Cart;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z[I[ILjava/util/Collection;ZZLcom/google/android/gms/wallet/ImmediateFullWalletRequest;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/aa/b/a/a/a/a/u;Lcom/google/android/gms/wallet/Cart;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z[I[ILjava/util/Collection;ZLcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Landroid/content/Intent;
    .locals 15

    .prologue
    .line 370
    const/4 v12, 0x0

    const/4 v14, 0x1

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move/from16 v11, p11

    move-object/from16 v13, p12

    invoke-static/range {v0 .. v14}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/aa/b/a/a/a/a/u;Lcom/google/android/gms/wallet/Cart;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z[I[ILjava/util/Collection;ZZLcom/google/android/gms/wallet/ImmediateFullWalletRequest;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/aa/b/a/a/a/a/u;Lcom/google/android/gms/wallet/Cart;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z[I[ILjava/util/Collection;ZZLcom/google/android/gms/wallet/ImmediateFullWalletRequest;Z)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 327
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 328
    const-string v2, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 329
    const-string v2, "merchantMaskedWalletRequest"

    invoke-static {v1, v2, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 331
    const-string v2, "cart"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 332
    const-string v2, "account"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 333
    const-string v2, "allowedBillingCountryCodes"

    invoke-virtual {v1, v2, p5}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 335
    const-string v2, "defaultCountryCode"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 336
    const-string v2, "com.google.android.gms"

    const-class v3, Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 338
    const-string v2, "legalDocsForCountry"

    invoke-virtual {v1, v2, p6}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 339
    const-string v2, "requiresCreditCardFullAddress"

    invoke-virtual {v1, v2, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 340
    const-string v2, "disallowedCreditCardTypes"

    invoke-virtual {v1, v2, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 341
    const-string v2, "disallowedCardCategories"

    invoke-virtual {v1, v2, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 342
    const-string v2, "addressHints"

    invoke-static {v1, v2, p10}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V

    .line 343
    const-string v2, "showPreauthorizationPrompt"

    invoke-virtual {v1, v2, p11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 344
    const-string v2, "localMode"

    move/from16 v0, p12

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 345
    const-string v2, "immediateFullWalletRequest"

    move-object/from16 v0, p13

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 346
    const-string v2, "allowChangeAccounts"

    move/from16 v0, p14

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 347
    return-object v1
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/SignupActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->B:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/SignupActivity;Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->U:Lcom/google/checkout/inapp/proto/a/b;

    return-object p1
.end method

.method private a(Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/16 v11, 0x8

    const/4 v3, 0x0

    .line 569
    sget v0, Lcom/google/android/gms/j;->rk:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Landroid/widget/CheckBox;

    .line 570
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Landroid/widget/CheckBox;

    sget-object v0, Lcom/google/android/gms/wallet/b/a;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 571
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->b:Landroid/support/v7/app/a;

    .line 573
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->R:Z

    if-nez v0, :cond_6

    .line 574
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->b:Landroid/support/v7/app/a;

    if-eqz v0, :cond_5

    .line 575
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->b:Landroid/support/v7/app/a;

    invoke-virtual {v0}, Landroid/support/v7/app/a;->a()V

    .line 577
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->b:Landroid/support/v7/app/a;

    sget v1, Lcom/google/android/gms/p;->CD:I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->c(I)V

    move v1, v3

    .line 618
    :cond_0
    :goto_0
    sget v0, Lcom/google/android/gms/j;->cM:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->q:Landroid/view/ViewGroup;

    .line 619
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->q:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/gms/j;->nv:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->r:Landroid/widget/CheckBox;

    .line 621
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 622
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->R:Z

    if-eqz v0, :cond_a

    .line 623
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->q:Landroid/view/ViewGroup;

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->A:Z

    if-eqz v0, :cond_9

    move v0, v11

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 625
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->E:Landroid/accounts/Account;

    invoke-static {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->E:Landroid/accounts/Account;

    sget-object v2, Lcom/google/android/gms/wallet/w;->a:Landroid/accounts/Account;

    invoke-virtual {v0, v2}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 627
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 633
    :cond_2
    :goto_2
    sget v0, Lcom/google/android/gms/j;->pN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->f:Landroid/widget/ProgressBar;

    .line 634
    sget v0, Lcom/google/android/gms/j;->tg:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->l:Landroid/widget/TextView;

    .line 636
    sget v0, Lcom/google/android/gms/j;->ch:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->p:Landroid/widget/TextView;

    .line 638
    sget v0, Lcom/google/android/gms/j;->cj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->e:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    .line 639
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->e:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 640
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Z)V

    .line 641
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->e:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/view/View$OnClickListener;)V

    .line 643
    const-string v0, "disallowedCreditCardTypes"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v7

    .line 646
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->kx:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/f;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    .line 648
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    if-nez v0, :cond_b

    .line 649
    const-string v0, "addressHints"

    const-class v1, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v9

    .line 652
    const-string v0, "allowedBillingCountryCodes"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 654
    const-string v0, "disallowedCardCategories"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v8

    .line 656
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->B:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->E:Landroid/accounts/Account;

    const/4 v2, 0x2

    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->y:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->O:Z

    iget-boolean v6, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->N:Z

    iget-object v10, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->V:Ljava/lang/String;

    invoke-static/range {v0 .. v10}, Lcom/google/android/gms/wallet/common/ui/f;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;ILjava/util/ArrayList;Ljava/lang/String;ZZ[I[ILjava/util/Collection;Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    .line 667
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->kx:I

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 672
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/f;->a(Lcom/google/android/gms/wallet/common/ui/cr;)V

    .line 673
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/f;->a(Lcom/google/android/gms/wallet/common/ui/ah;)V

    .line 675
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->rD:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    .line 678
    sget v0, Lcom/google/android/gms/j;->tH:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->h:Landroid/widget/CheckBox;

    .line 681
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->P:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->y:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 683
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 688
    :goto_4
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Z

    if-eqz v0, :cond_3

    .line 691
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 693
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->i()V

    .line 698
    :goto_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, v11}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 701
    :cond_3
    sget v0, Lcom/google/android/gms/j;->lE:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->j:Landroid/widget/CheckBox;

    .line 702
    sget v0, Lcom/google/android/gms/j;->lF:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->k:Landroid/view/View;

    .line 704
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->W:Z

    if-eqz v0, :cond_4

    .line 705
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->l()V

    .line 708
    :cond_4
    sget v0, Lcom/google/android/gms/j;->ua:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;)V

    .line 709
    return-void

    .line 579
    :cond_5
    sget v0, Lcom/google/android/gms/j;->tc:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    .line 580
    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setVisibility(I)V

    .line 581
    sget v1, Lcom/google/android/gms/p;->CD:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(I)V

    move v1, v3

    .line 582
    goto/16 :goto_0

    .line 584
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->b:Landroid/support/v7/app/a;

    if-eqz v0, :cond_7

    .line 585
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->b:Landroid/support/v7/app/a;

    invoke-virtual {v0}, Landroid/support/v7/app/a;->a()V

    .line 587
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->b:Landroid/support/v7/app/a;

    sget v1, Lcom/google/android/gms/p;->BS:I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->c(I)V

    .line 596
    :goto_6
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->z:Z

    if-eqz v0, :cond_f

    .line 597
    sget v0, Lcom/google/android/gms/j;->qz:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->u:Landroid/widget/CheckBox;

    .line 598
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->S:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->i()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 599
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->u:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 600
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->u:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    move v1, v2

    .line 603
    :goto_7
    sget v0, Lcom/google/android/gms/j;->bX:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->s:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 604
    sget v0, Lcom/google/android/gms/j;->rC:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 605
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Landroid/widget/CheckBox;

    invoke-virtual {v0, v11}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 607
    :goto_8
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->T:Z

    if-nez v0, :cond_0

    .line 608
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->b:Landroid/support/v7/app/a;

    if-eqz v0, :cond_8

    .line 609
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->d:Lcom/google/android/gms/wallet/common/ui/cs;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/common/ui/cs;->a(Z)V

    .line 614
    :goto_9
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->A:Z

    goto/16 :goto_0

    .line 589
    :cond_7
    sget v0, Lcom/google/android/gms/j;->nK:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->c:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    .line 591
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->c:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->E:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Landroid/accounts/Account;)V

    .line 592
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->c:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Lcom/google/android/gms/wallet/common/ui/cq;)V

    .line 593
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->c:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->setVisibility(I)V

    goto :goto_6

    .line 611
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->c:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Z)V

    .line 612
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->c:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->b()V

    goto :goto_9

    :cond_9
    move v0, v3

    .line 623
    goto/16 :goto_1

    .line 630
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->q:Landroid/view/ViewGroup;

    invoke-virtual {v0, v11}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_2

    .line 670
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/wallet/common/ui/f;->a([I)V

    goto/16 :goto_3

    .line 685
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, v11}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_4

    .line 695
    :cond_d
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->h()V

    goto/16 :goto_5

    :cond_e
    move v1, v3

    goto :goto_7

    :cond_f
    move v1, v3

    goto :goto_8
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/SignupActivity;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->p:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setFocusable(Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/SignupActivity;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->o:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->o:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    invoke-static {p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->o:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->o:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->o:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "SignupActivity.OwErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 913
    if-eqz p1, :cond_0

    .line 914
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->e:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    sget v1, Lcom/google/android/gms/p;->Cw:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Ljava/lang/CharSequence;)V

    .line 918
    :goto_0
    return-void

    .line 916
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->e:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    sget v1, Lcom/google/android/gms/p;->Au:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private b(Landroid/accounts/Account;)V
    .locals 1

    .prologue
    .line 1137
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->b:Landroid/support/v7/app/a;

    if-eqz v0, :cond_1

    .line 1139
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->d:Lcom/google/android/gms/wallet/common/ui/cs;

    if-eqz v0, :cond_0

    .line 1140
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->d:Lcom/google/android/gms/wallet/common/ui/cs;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/cs;->a(Landroid/accounts/Account;)V

    .line 1145
    :cond_0
    :goto_0
    return-void

    .line 1143
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->c:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Landroid/accounts/Account;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ow/SignupActivity;)V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->p:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ow/SignupActivity;I)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->d(I)V

    return-void
.end method

.method private b(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 958
    if-eqz p1, :cond_5

    .line 959
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 963
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->e:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    if-nez p1, :cond_6

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 964
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    if-nez p1, :cond_7

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/f;->a(Z)V

    .line 965
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->h:Landroid/widget/CheckBox;

    if-nez p1, :cond_8

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 966
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-eqz v0, :cond_0

    .line 967
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-nez p1, :cond_9

    move v0, v1

    :goto_4
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Z)V

    .line 969
    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->j:Landroid/widget/CheckBox;

    if-nez p1, :cond_a

    move v0, v1

    :goto_5
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 970
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->k:Landroid/view/View;

    if-nez p1, :cond_b

    move v0, v1

    :goto_6
    invoke-virtual {v3, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 971
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Landroid/widget/CheckBox;

    if-nez p1, :cond_c

    move v0, v1

    :goto_7
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 972
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->l:Landroid/widget/TextView;

    if-nez p1, :cond_d

    move v0, v1

    :goto_8
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 973
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->r:Landroid/widget/CheckBox;

    if-nez p1, :cond_e

    move v0, v1

    :goto_9
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 974
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->s:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_1

    .line 975
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->s:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-nez p1, :cond_f

    move v0, v1

    :goto_a
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 977
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_2

    .line 978
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-nez p1, :cond_10

    move v0, v1

    :goto_b
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 980
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->u:Landroid/widget/CheckBox;

    if-eqz v0, :cond_3

    .line 981
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->u:Landroid/widget/CheckBox;

    if-nez p1, :cond_11

    move v0, v1

    :goto_c
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 983
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->z:Z

    if-eqz v0, :cond_4

    .line 984
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->b:Landroid/support/v7/app/a;

    if-eqz v0, :cond_13

    .line 985
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->d:Lcom/google/android/gms/wallet/common/ui/cs;

    if-nez p1, :cond_12

    :goto_d
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cs;->a(Z)V

    .line 990
    :cond_4
    :goto_e
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->F:Z

    .line 991
    return-void

    .line 961
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->f:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 963
    goto/16 :goto_1

    :cond_7
    move v0, v2

    .line 964
    goto/16 :goto_2

    :cond_8
    move v0, v2

    .line 965
    goto :goto_3

    :cond_9
    move v0, v2

    .line 967
    goto :goto_4

    :cond_a
    move v0, v2

    .line 969
    goto :goto_5

    :cond_b
    move v0, v2

    .line 970
    goto :goto_6

    :cond_c
    move v0, v2

    .line 971
    goto :goto_7

    :cond_d
    move v0, v2

    .line 972
    goto :goto_8

    :cond_e
    move v0, v2

    .line 973
    goto :goto_9

    :cond_f
    move v0, v2

    .line 975
    goto :goto_a

    :cond_10
    move v0, v2

    .line 978
    goto :goto_b

    :cond_11
    move v0, v2

    .line 981
    goto :goto_c

    :cond_12
    move v1, v2

    .line 985
    goto :goto_d

    .line 987
    :cond_13
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->c:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    if-nez p1, :cond_14

    :goto_f
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Z)V

    goto :goto_e

    :cond_14
    move v1, v2

    goto :goto_f
.end method

.method private c(I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 933
    invoke-static {p1}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->a(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->H:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->H:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->x:Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;

    .line 935
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->x:Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;

    if-nez v0, :cond_3

    .line 936
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->z:Z

    if-nez v0, :cond_1

    .line 937
    const-string v0, "SignupActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to find legal docs for region "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 939
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    .line 955
    :cond_1
    :goto_1
    return-void

    :cond_2
    move-object v0, v1

    .line 933
    goto :goto_0

    .line 944
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->l:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->CN:I

    const-string v2, "wallet_tos_activity"

    invoke-static {v2}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/wallet/ow/x;

    invoke-direct {v3, p0}, Lcom/google/android/gms/wallet/ow/x;-><init>(Lcom/google/android/gms/wallet/ow/SignupActivity;)V

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/ClickSpan;->a(Landroid/widget/TextView;ILjava/util/Set;Lcom/google/android/gms/wallet/common/ui/al;)V

    goto :goto_1
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/ow/SignupActivity;)V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->b(Z)V

    return-void
.end method

.method private c(Z)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1005
    .line 1007
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Z

    if-eqz v1, :cond_4

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->j()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1008
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    .line 1009
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 1012
    :goto_0
    const/4 v4, 0x4

    new-array v4, v4, [Lcom/google/android/gms/wallet/common/ui/dn;

    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    aput-object v5, v4, v3

    aput-object v1, v4, v2

    const/4 v1, 0x2

    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->s:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v5, v4, v1

    const/4 v1, 0x3

    aput-object v0, v4, v1

    .line 1015
    array-length v5, v4

    move v1, v3

    move v0, v2

    :goto_1
    if-ge v1, v5, :cond_3

    aget-object v6, v4, v1

    .line 1017
    if-eqz v6, :cond_0

    .line 1018
    if-eqz p1, :cond_2

    .line 1021
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->f()Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v0, :cond_1

    move v0, v2

    .line 1015
    :cond_0
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move v0, v3

    .line 1021
    goto :goto_2

    .line 1022
    :cond_2
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->e()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1027
    :goto_3
    return v3

    :cond_3
    move v3, v0

    goto :goto_3

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method private d(I)V
    .locals 2

    .prologue
    .line 1068
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1069
    const-string v1, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1070
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    .line 1071
    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/ow/SignupActivity;)Z
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/ow/SignupActivity;)V
    .locals 3

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->m:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->m:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->m:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->m:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->m:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "SignupActivity.NetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/wallet/ow/SignupActivity;)Z
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->R:Z

    return v0
.end method

.method private h()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 807
    sget v0, Lcom/google/android/gms/j;->rE:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 808
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 809
    sget v1, Lcom/google/android/gms/j;->rF:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 810
    sget v1, Lcom/google/android/gms/j;->rD:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 811
    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 812
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->Bg:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 815
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->z:Z

    if-eqz v0, :cond_1

    .line 816
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    .line 819
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-eqz v0, :cond_2

    .line 820
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 825
    :goto_0
    return-void

    .line 823
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->y:Ljava/lang/String;

    new-instance v2, Lcom/google/t/a/b;

    invoke-direct {v2}, Lcom/google/t/a/b;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/ui/f;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/ui/f;->a()Lcom/google/checkout/a/a/a/d;

    move-result-object v3

    iget v3, v3, Lcom/google/checkout/a/a/a/d;->a:I

    packed-switch v3, :pswitch_data_0

    :cond_3
    :goto_1
    const-string v3, "addressHints"

    const-class v4, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/a/e;->a(Ljava/util/Collection;)Landroid/util/Pair;

    move-result-object v3

    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a()Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->P:Ljava/util/List;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/List;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v4

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/ArrayList;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/Collection;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->N:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/s;->b(Z)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/s;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;)Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/t/a/b;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->rD:I

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/f;->a()Lcom/google/checkout/a/a/a/d;

    move-result-object v0

    iget-object v0, v0, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iget-object v3, v0, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    iget-object v0, v3, Lcom/google/t/a/b;->a:Ljava/lang/String;

    iget-object v3, v3, Lcom/google/t/a/b;->s:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/t/a/b;->s:Ljava/lang/String;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private i()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 828
    sget v0, Lcom/google/android/gms/j;->rE:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 829
    sget v0, Lcom/google/android/gms/j;->rF:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 830
    sget v0, Lcom/google/android/gms/j;->rD:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 831
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->z:Z

    if-eqz v0, :cond_0

    .line 832
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    .line 834
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-eqz v0, :cond_1

    .line 835
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 838
    :cond_1
    return-void
.end method

.method private j()Z
    .locals 1

    .prologue
    .line 1054
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()Lcom/google/android/gms/wallet/common/ui/dh;
    .locals 2

    .prologue
    .line 1060
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->G:Lcom/google/android/gms/wallet/common/ui/dh;

    if-nez v0, :cond_0

    .line 1061
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/wallet/ow/SignupActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dh;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->G:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 1064
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->G:Lcom/google/android/gms/wallet/common/ui/dh;

    return-object v0
.end method

.method private l()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1572
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 1573
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->z:Z

    if-eqz v0, :cond_2

    .line 1574
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->s:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    .line 1585
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->e:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 1586
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Z

    if-eqz v0, :cond_1

    .line 1587
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 1589
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->W:Z

    .line 1590
    return-void

    .line 1576
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1577
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->C:Lcom/google/aa/b/a/a/a/a/u;

    iget-boolean v0, v0, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "showPreauthorizationPrompt"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 1579
    :goto_1
    if-eqz v0, :cond_0

    .line 1580
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->k:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1581
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1582
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 1577
    goto :goto_1
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/wallet/common/a/k;
    .locals 1

    .prologue
    .line 1157
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->M:Lcom/google/android/gms/wallet/common/a/k;

    if-nez v0, :cond_0

    .line 1158
    new-instance v0, Lcom/google/android/gms/wallet/common/a/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/a/k;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->M:Lcom/google/android/gms/wallet/common/a/k;

    .line 1160
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->M:Lcom/google/android/gms/wallet/common/a/k;

    return-object v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 1568
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->l()V

    .line 1569
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 903
    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_1

    .line 904
    if-nez p1, :cond_0

    .line 905
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->b(Z)V

    .line 910
    :cond_0
    :goto_0
    return-void

    .line 908
    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->d(I)V

    goto :goto_0
.end method

.method protected final a(ILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1075
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/dq;->a(ILandroid/content/Intent;)V

    .line 1077
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/ui/dr;->a(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->V:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryActivityClosedEvent;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    .line 1080
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->J:Lcom/google/android/apps/common/a/a/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->K:Lcom/google/android/apps/common/a/a/h;

    if-eqz v0, :cond_0

    .line 1081
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->J:Lcom/google/android/apps/common/a/a/i;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->K:Lcom/google/android/apps/common/a/a/h;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "click_to_activity_result"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/common/a/a/i;->a(Lcom/google/android/apps/common/a/a/h;[Ljava/lang/String;)Z

    .line 1083
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->J:Lcom/google/android/apps/common/a/a/i;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/apps/common/a/a/i;)V

    .line 1084
    iput-object v5, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->J:Lcom/google/android/apps/common/a/a/i;

    .line 1085
    iput-object v5, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->K:Lcom/google/android/apps/common/a/a/h;

    .line 1087
    :cond_0
    return-void
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 2

    .prologue
    .line 1445
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->E:Landroid/accounts/Account;

    invoke-virtual {v0, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1446
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1447
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1448
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    .line 1450
    :cond_0
    return-void
.end method

.method public final b()Lcom/google/android/gms/wallet/common/a/n;
    .locals 1

    .prologue
    .line 1149
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->L:Lcom/google/android/gms/wallet/common/a/n;

    if-nez v0, :cond_0

    .line 1150
    new-instance v0, Lcom/google/android/gms/wallet/common/a/n;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/a/n;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->L:Lcom/google/android/gms/wallet/common/a/n;

    .line 1152
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->L:Lcom/google/android/gms/wallet/common/a/n;

    return-object v0
.end method

.method public final b(I)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 767
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->R:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->y:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->a(Ljava/lang/String;)I

    move-result v0

    if-eq p1, v0, :cond_1

    move v0, v1

    .line 770
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->z:Z

    if-eqz v2, :cond_2

    .line 804
    :cond_0
    :goto_1
    return-void

    .line 767
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 773
    :cond_2
    if-eqz v0, :cond_3

    .line 774
    invoke-static {p1}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->y:Ljava/lang/String;

    .line 775
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->z:Z

    .line 776
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->B:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->C:Lcom/google/aa/b/a/a/a/a/u;

    invoke-static {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->y:Ljava/lang/String;

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->O:Z

    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->S:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iget-boolean v7, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->T:Z

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/aa/b/a/a/a/a/u;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;ZLcom/google/android/gms/wallet/ImmediateFullWalletRequest;Z)Landroid/content/Intent;

    move-result-object v0

    .line 780
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->setIntent(Landroid/content/Intent;)V

    .line 781
    invoke-static {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->E:Landroid/accounts/Account;

    .line 782
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->E:Landroid/accounts/Account;

    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->b(Landroid/accounts/Account;)V

    .line 783
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Landroid/content/Intent;)V

    .line 784
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->S:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h()Ljava/lang/String;

    move-result-object v0

    .line 785
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->k()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/gms/wallet/service/d;

    if-nez v1, :cond_0

    .line 788
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->G:Lcom/google/android/gms/wallet/common/ui/dh;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/aj;->a()I

    .line 790
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->B:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->E:Landroid/accounts/Account;

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->G:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 792
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->G:Lcom/google/android/gms/wallet/common/ui/dh;

    sget-object v2, Lcom/google/android/gms/wallet/ow/SignupActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 795
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->G:Lcom/google/android/gms/wallet/common/ui/dh;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/dh;->onAttach(Landroid/app/Activity;)V

    .line 796
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->k()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->X:Lcom/google/android/gms/wallet/service/l;

    iget v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->Q:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/service/k;->b(Lcom/google/android/gms/wallet/service/l;I)V

    goto/16 :goto_1

    .line 801
    :cond_3
    iput p1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->w:I

    .line 802
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->c(I)V

    goto/16 :goto_1
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 995
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->c(Z)Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1000
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->c(Z)Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1032
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/f;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1050
    :cond_0
    :goto_0
    return v0

    .line 1035
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->s:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->s:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1036
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->s:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->clearFocus()V

    .line 1037
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->s:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    goto :goto_0

    .line 1040
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Z

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1041
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1044
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1045
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->clearFocus()V

    .line 1046
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    goto :goto_0

    .line 1050
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 715
    const/high16 v0, -0x10000

    and-int/2addr v0, p1

    if-eqz v0, :cond_1

    .line 716
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/dq;->onActivityResult(IILandroid/content/Intent;)V

    .line 728
    :cond_0
    :goto_0
    return-void

    .line 722
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->kx:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 724
    if-eqz v0, :cond_0

    .line 725
    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 1092
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    .line 1093
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .prologue
    .line 879
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->r:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_2

    .line 880
    if-nez p2, :cond_1

    .line 881
    invoke-static {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->b(Landroid/accounts/Account;)V

    .line 898
    :cond_0
    :goto_0
    return-void

    .line 883
    :cond_1
    invoke-static {}, Lcom/google/android/gms/wallet/a/a;->a()Lcom/google/android/gms/wallet/a/a;

    invoke-static {p0}, Lcom/google/android/gms/wallet/a/a;->b(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->b(Landroid/accounts/Account;)V

    goto :goto_0

    .line 885
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->u:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_3

    .line 886
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->z:Z

    if-eqz v0, :cond_0

    .line 887
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Z)V

    goto :goto_0

    .line 889
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->h:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_0

    .line 890
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Z

    if-eqz v0, :cond_0

    .line 891
    if-eqz p2, :cond_4

    .line 892
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->i()V

    goto :goto_0

    .line 894
    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->h()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 748
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 749
    sget v1, Lcom/google/android/gms/j;->pB:I

    if-ne v0, v1, :cond_10

    .line 750
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->c(Z)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 751
    new-instance v0, Lcom/google/android/apps/common/a/a/i;

    const-string v1, "get_masked_wallet_signup"

    invoke-direct {v0, v1}, Lcom/google/android/apps/common/a/a/i;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->J:Lcom/google/android/apps/common/a/a/i;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->J:Lcom/google/android/apps/common/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/common/a/a/i;->a()Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->K:Lcom/google/android/apps/common/a/a/h;

    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->b(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/f;->a()Lcom/google/checkout/a/a/a/d;

    move-result-object v0

    new-instance v4, Lcom/google/aa/b/a/a/a/a/k;

    invoke-direct {v4}, Lcom/google/aa/b/a/a/a/a/k;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->C:Lcom/google/aa/b/a/a/a/a/u;

    iput-object v1, v4, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    iput-object v0, v4, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Z

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->j()Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, v0, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iget-object v1, v1, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    iget-object v0, v0, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iget-object v0, v0, Lcom/google/checkout/a/a/a/b;->g:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    :goto_0
    new-instance v5, Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {v5}, Lcom/google/checkout/inapp/proto/a/b;-><init>()V

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/t/a/b;

    iput-object v0, v5, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->N:Z

    if-eqz v0, :cond_0

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->U:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->U:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v0, v5}, Lcom/google/android/gms/wallet/common/a/e;->a(Lcom/google/checkout/inapp/proto/a/b;Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->U:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/aa/b/a/a/a/a/k;->d:Ljava/lang/String;

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->x:Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->x:Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->b()[Lcom/google/aa/b/a/a/a/a/s;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/aa/b/a/a/a/a/s;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->R:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->B:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    iput-boolean v0, v4, Lcom/google/aa/b/a/a/a/a/k;->i:Z

    iget-boolean v0, v4, Lcom/google/aa/b/a/a/a/a/k;->i:Z

    if-eqz v0, :cond_9

    const-string v0, "requested"

    :goto_2
    const-string v5, "ow_signup"

    const-string v6, "preauth"

    invoke-static {v1, v5, v6, v0, v7}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->v:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_b

    iput v2, v4, Lcom/google/aa/b/a/a/a/a/k;->n:I

    :goto_4
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->z:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->s:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, v4, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    iget-object v1, v1, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iget-object v1, v1, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    iput-object v0, v1, Lcom/google/t/a/b;->t:Ljava/lang/String;

    :cond_4
    iget-object v1, v4, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Z

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_d

    iget-object v1, v4, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iput-object v0, v1, Lcom/google/t/a/b;->t:Ljava/lang/String;

    :cond_5
    :goto_5
    iput-boolean v3, v4, Lcom/google/aa/b/a/a/a/a/k;->e:Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->u:Landroid/widget/CheckBox;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->u:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->u:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_e

    move v0, v2

    :goto_6
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->k()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->D:Lcom/google/android/gms/wallet/Cart;

    invoke-interface {v1, v4, v2, v7, v0}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/aa/b/a/a/a/a/k;Lcom/google/android/gms/wallet/Cart;Ljava/lang/String;Z)V

    .line 758
    :cond_6
    :goto_7
    return-void

    .line 751
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h()Lcom/google/t/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0

    :cond_8
    iput-object v5, v4, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    goto/16 :goto_1

    :cond_9
    const-string v0, "not_requested"

    goto/16 :goto_2

    :cond_a
    const-string v0, "ow_signup"

    const-string v5, "preauth"

    const-string v6, "not_shown"

    invoke-static {v1, v0, v5, v6, v7}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_3

    :cond_b
    const/4 v0, 0x2

    iput v0, v4, Lcom/google/aa/b/a/a/a/a/k;->n:I

    goto :goto_4

    :cond_c
    iput v3, v4, Lcom/google/aa/b/a/a/a/a/k;->n:I

    goto/16 :goto_4

    :cond_d
    iget-object v0, v4, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->t:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/t/a/b;->t:Ljava/lang/String;

    goto :goto_5

    :cond_e
    move v0, v3

    goto :goto_6

    .line 753
    :cond_f
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->g()Z

    goto :goto_7

    .line 755
    :cond_10
    sget v1, Lcom/google/android/gms/j;->lF:I

    if-ne v0, v1, :cond_6

    .line 756
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->n:Lcom/google/android/gms/wallet/common/ui/ca;

    if-eqz v0, :cond_11

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->n:Lcom/google/android/gms/wallet/common/ui/ca;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_11
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/ca;->b()Lcom/google/android/gms/wallet/common/ui/ca;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->n:Lcom/google/android/gms/wallet/common/ui/ca;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->n:Lcom/google/android/gms/wallet/common/ui/ca;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "SignupActivity.InfoDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/ca;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    goto :goto_7
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 405
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 406
    const-string v0, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v4, "Activity requires buyFlowConfig extra!"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 408
    const-string v0, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->B:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 409
    const-string v0, "merchantMaskedWalletRequest"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v4, "Activity requires maskedWalletRequest extra!"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 411
    const-string v0, "merchantMaskedWalletRequest"

    const-class v4, Lcom/google/aa/b/a/a/a/a/u;

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/u;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->C:Lcom/google/aa/b/a/a/a/a/u;

    .line 413
    const-string v0, "cart"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/Cart;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->D:Lcom/google/android/gms/wallet/Cart;

    .line 414
    const-string v0, "account"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v4, "Activity requires account extra!"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 416
    const-string v0, "account"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->E:Landroid/accounts/Account;

    .line 417
    const-string v0, "allowedBillingCountryCodes"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v4, "Activity requires allowedCountryCodes"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 419
    const-string v0, "defaultCountryCode"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v4, "Activity requires defaultCountryCode"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 421
    const-string v0, "defaultCountryCode"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->y:Ljava/lang/String;

    .line 422
    const-string v0, "legalDocsForCountry"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v4, "Activity requires legalDocsForCountry"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 424
    const-string v0, "legalDocsForCountry"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->H:Ljava/util/ArrayList;

    .line 426
    const-string v0, "requiresCreditCardFullAddress"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->O:Z

    .line 429
    const-string v0, "localMode"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->z:Z

    .line 430
    const-string v0, "immediateFullWalletRequest"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->S:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    .line 432
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->z:Z

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->S:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 435
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->S:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->R:Z

    .line 436
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->C:Lcom/google/aa/b/a/a/a/a/u;

    iget-boolean v0, v0, Lcom/google/aa/b/a/a/a/a/u;->g:Z

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Z

    .line 437
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->C:Lcom/google/aa/b/a/a/a/a/u;

    iget-boolean v0, v0, Lcom/google/aa/b/a/a/a/a/u;->i:Z

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->N:Z

    .line 438
    const-string v0, "allowChangeAccounts"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->T:Z

    .line 440
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->I:Z

    if-eqz v0, :cond_3

    .line 441
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->C:Lcom/google/aa/b/a/a/a/a/u;

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    .line 443
    array-length v4, v0

    .line 444
    if-eqz v4, :cond_2

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 445
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->P:Ljava/util/List;

    .line 446
    :goto_2
    if-ge v2, v4, :cond_3

    .line 447
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->P:Ljava/util/List;

    aget-object v5, v0, v2

    iget-object v5, v5, Lcom/google/aa/b/a/a/a/a/x;->a:Ljava/lang/String;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 446
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    move v0, v2

    .line 435
    goto :goto_0

    :cond_2
    move v1, v2

    .line 444
    goto :goto_1

    .line 451
    :cond_3
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dq;->onCreate(Landroid/os/Bundle;)V

    .line 452
    sget v0, Lcom/google/android/gms/l;->gf:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->setContentView(I)V

    .line 454
    if-nez p1, :cond_5

    .line 455
    const-string v0, "signup"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->B:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->E:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->V:Ljava/lang/String;

    .line 463
    :goto_3
    invoke-direct {p0, v3}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Landroid/content/Intent;)V

    .line 467
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->k()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    if-nez v0, :cond_4

    .line 468
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->z:Z

    if-nez v0, :cond_6

    .line 469
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->B:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->E:Landroid/accounts/Account;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/dh;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->G:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 477
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->G:Lcom/google/android/gms/wallet/common/ui/dh;

    sget-object v2, Lcom/google/android/gms/wallet/ow/SignupActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 481
    :cond_4
    return-void

    .line 459
    :cond_5
    const-string v0, "analyticsSessionId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->V:Ljava/lang/String;

    .line 460
    const-string v0, "expandedMode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->W:Z

    goto :goto_3

    .line 473
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->S:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h()Ljava/lang/String;

    move-result-object v0

    .line 474
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->B:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->E:Landroid/accounts/Account;

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->G:Lcom/google/android/gms/wallet/common/ui/dh;

    goto :goto_4
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 732
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->R:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->T:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->b:Landroid/support/v7/app/a;

    if-eqz v0, :cond_0

    .line 733
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/m;->G:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 735
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/cs;

    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/app/a;->g()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/wallet/common/ui/cs;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/cq;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->d:Lcom/google/android/gms/wallet/common/ui/cs;

    .line 737
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->d:Lcom/google/android/gms/wallet/common/ui/cs;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->E:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cs;->a(Landroid/accounts/Account;)V

    .line 738
    sget v0, Lcom/google/android/gms/j;->nI:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 739
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->d:Lcom/google/android/gms/wallet/common/ui/cs;

    invoke-static {v0, v1}, Landroid/support/v4/view/ai;->a(Landroid/view/MenuItem;Landroid/support/v4/view/n;)Landroid/view/MenuItem;

    .line 740
    const/4 v0, 0x1

    .line 742
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 535
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/dq;->onPause()V

    .line 537
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->k()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->X:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->b(Lcom/google/android/gms/wallet/service/l;)V

    .line 539
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 485
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dq;->onPostCreate(Landroid/os/Bundle;)V

    .line 487
    if-eqz p1, :cond_2

    .line 488
    const-string v0, "pendingRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->F:Z

    .line 489
    const-string v0, "regionCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->w:I

    .line 490
    const-string v0, "serviceConnectionSavePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->Q:I

    .line 492
    const-string v0, "existingSelectedAddress"

    const-class v1, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->U:Lcom/google/checkout/inapp/proto/a/b;

    .line 500
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->z:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->w:I

    if-eqz v0, :cond_0

    .line 501
    iget v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->w:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->c(I)V

    .line 504
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->F:Z

    if-eqz v0, :cond_1

    .line 505
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->b(Z)V

    .line 507
    :cond_1
    return-void

    .line 496
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->B:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->B:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onlinewallet_signup"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 561
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dq;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 563
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 564
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->p:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 566
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 511
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/dq;->onResume()V

    .line 513
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "SignupActivity.NetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->m:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 515
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->m:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->m:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 519
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "SignupActivity.OwErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->o:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 521
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->o:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_1

    .line 522
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->o:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 525
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "SignupActivity.InfoDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ca;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->n:Lcom/google/android/gms/wallet/common/ui/ca;

    .line 528
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->k()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->X:Lcom/google/android/gms/wallet/service/l;

    iget v2, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->Q:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/service/k;->b(Lcom/google/android/gms/wallet/service/l;I)V

    .line 531
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 543
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dq;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 545
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->k()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->X:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->c(Lcom/google/android/gms/wallet/service/l;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->Q:I

    .line 548
    const-string v0, "pendingRequest"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->F:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 549
    const-string v0, "regionCode"

    iget v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->w:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 550
    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->Q:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 551
    const-string v0, "analyticsSessionId"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->V:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->U:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_0

    .line 553
    const-string v0, "existingSelectedAddress"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->U:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 556
    :cond_0
    const-string v0, "expandedMode"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/SignupActivity;->W:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 557
    return-void
.end method
