.class public final Lcom/google/android/gms/auth/setup/d2d/aq;
.super Landroid/app/Fragment;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;


# instance fields
.field private final b:Ljava/lang/Object;

.field private c:Lcom/google/android/gms/auth/setup/d2d/aw;

.field private d:Landroid/content/Context;

.field private e:Landroid/nfc/NfcAdapter;

.field private f:Landroid/bluetooth/BluetoothAdapter;

.field private g:Z

.field private h:Z

.field private i:Landroid/content/BroadcastReceiver;

.field private j:Lcom/google/k/k/a/ag;

.field private k:Landroid/os/AsyncTask;

.field private l:Landroid/os/AsyncTask;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 53
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "D2D"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "TargetResourcesFragment"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/setup/d2d/aq;->a:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 57
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->b:Ljava/lang/Object;

    .line 63
    iput-boolean v1, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->g:Z

    .line 64
    iput-boolean v1, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->h:Z

    .line 71
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/setup/d2d/aq;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->i:Landroid/content/BroadcastReceiver;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/auth/setup/d2d/aq;)Z
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->f:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->e:Landroid/nfc/NfcAdapter;

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->c:Lcom/google/android/gms/auth/setup/d2d/aw;

    invoke-interface {v0}, Lcom/google/android/gms/auth/setup/d2d/aw;->i()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b()Lcom/google/android/gms/auth/d/a;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/aq;->a:Lcom/google/android/gms/auth/d/a;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/auth/setup/d2d/aq;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/aq;->c()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/auth/setup/d2d/aq;)Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->i:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 205
    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 206
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->i:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 208
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->i:Landroid/content/BroadcastReceiver;

    .line 210
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic d(Lcom/google/android/gms/auth/setup/d2d/aq;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/auth/setup/d2d/aq;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->f:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/auth/setup/d2d/aq;)Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->h:Z

    return v0
.end method

.method static synthetic g(Lcom/google/android/gms/auth/setup/d2d/aq;)Lcom/google/android/gms/auth/setup/d2d/aw;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->c:Lcom/google/android/gms/auth/setup/d2d/aw;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/auth/setup/d2d/aq;)Landroid/nfc/NfcAdapter;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->e:Landroid/nfc/NfcAdapter;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/auth/setup/d2d/aq;)Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->g:Z

    return v0
.end method

.method static synthetic j(Lcom/google/android/gms/auth/setup/d2d/aq;)Lcom/google/k/k/a/ag;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->j:Lcom/google/k/k/a/ag;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/droidguard/b;
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->j:Lcom/google/k/k/a/ag;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->j:Lcom/google/k/k/a/ag;

    invoke-virtual {v0}, Lcom/google/k/k/a/ag;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/droidguard/b;

    return-object v0

    .line 192
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "TargetResourcesFragment has already been destroyed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 75
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 76
    check-cast p1, Lcom/google/android/gms/auth/setup/d2d/aw;

    iput-object p1, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->c:Lcom/google/android/gms/auth/setup/d2d/aw;

    .line 77
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 81
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 82
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/aq;->setRetainInstance(Z)V

    .line 84
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/aq;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->d:Landroid/content/Context;

    .line 85
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->f:Landroid/bluetooth/BluetoothAdapter;

    .line 86
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/aq;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->e:Landroid/nfc/NfcAdapter;

    .line 87
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/ar;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/setup/d2d/ar;-><init>(Lcom/google/android/gms/auth/setup/d2d/aq;)V

    invoke-static {v0}, Lcom/google/k/k/a/ag;->a(Ljava/util/concurrent/Callable;)Lcom/google/k/k/a/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->j:Lcom/google/k/k/a/ag;

    .line 95
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/as;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/setup/d2d/as;-><init>(Lcom/google/android/gms/auth/setup/d2d/aq;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/setup/d2d/as;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->k:Landroid/os/AsyncTask;

    .line 138
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/au;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/setup/d2d/au;-><init>(Lcom/google/android/gms/auth/setup/d2d/aq;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/setup/d2d/au;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->l:Landroid/os/AsyncTask;

    .line 145
    return-void
.end method

.method public final onDestroy()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 149
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->k:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->k:Landroid/os/AsyncTask;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 153
    iput-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->k:Landroid/os/AsyncTask;

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->l:Landroid/os/AsyncTask;

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->l:Landroid/os/AsyncTask;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 158
    iput-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->l:Landroid/os/AsyncTask;

    .line 161
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/aq;->c()V

    .line 163
    iget-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->h:Z

    if-eqz v0, :cond_2

    .line 164
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/aq;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Disabling Bluetooth."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->f:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    .line 168
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->g:Z

    if-eqz v0, :cond_3

    .line 169
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/aq;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Disabling NFC."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->e:Landroid/nfc/NfcAdapter;

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->disable()Z

    .line 173
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->j:Lcom/google/k/k/a/ag;

    if-eqz v0, :cond_4

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->j:Lcom/google/k/k/a/ag;

    new-instance v1, Lcom/google/android/gms/auth/setup/d2d/av;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/setup/d2d/av;-><init>(Lcom/google/android/gms/auth/setup/d2d/aq;)V

    invoke-static {v0, v1}, Lcom/google/k/k/a/n;->a(Lcom/google/k/k/a/af;Lcom/google/k/k/a/m;)V

    .line 184
    iput-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/aq;->j:Lcom/google/k/k/a/ag;

    .line 186
    :cond_4
    return-void
.end method
