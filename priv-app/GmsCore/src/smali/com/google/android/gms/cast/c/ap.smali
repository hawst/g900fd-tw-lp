.class abstract Lcom/google/android/gms/cast/c/ap;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field protected final b:[Ljava/lang/String;

.field final c:J

.field private final d:I

.field private final e:J


# direct methods
.method protected constructor <init>([Ljava/lang/String;ILcom/google/android/gms/cast/c/am;)V
    .locals 4

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/gms/cast/c/ap;->b:[Ljava/lang/String;

    .line 40
    iput p2, p0, Lcom/google/android/gms/cast/c/ap;->a:I

    .line 41
    invoke-virtual {p3}, Lcom/google/android/gms/cast/c/am;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/cast/c/ap;->d:I

    .line 42
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p3}, Lcom/google/android/gms/cast/c/am;->d()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/cast/c/ap;->c:J

    .line 43
    invoke-virtual {p3}, Lcom/google/android/gms/cast/c/am;->c()I

    move-result v0

    .line 45
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/cast/c/ap;->e:J

    .line 47
    invoke-virtual {p3, v0}, Lcom/google/android/gms/cast/c/am;->a(I)V

    .line 48
    invoke-virtual {p0, p3}, Lcom/google/android/gms/cast/c/ap;->a(Lcom/google/android/gms/cast/c/am;)V

    .line 49
    invoke-virtual {p3}, Lcom/google/android/gms/cast/c/am;->a()V

    .line 50
    return-void
.end method

.method public static a([Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    if-nez p0, :cond_0

    .line 154
    const/4 v0, 0x0

    .line 156
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "."

    invoke-static {v0, p0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(Lcom/google/android/gms/cast/c/am;)V
.end method

.method protected abstract a(Lcom/google/android/gms/cast/c/an;)V
.end method

.method public final a(Lcom/google/android/gms/cast/c/an;J)V
    .locals 8

    .prologue
    const-wide/16 v6, 0xff

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ap;->b:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/c/an;->a([Ljava/lang/String;)V

    .line 105
    iget v0, p0, Lcom/google/android/gms/cast/c/ap;->a:I

    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/c/an;->b(I)V

    .line 106
    iget v0, p0, Lcom/google/android/gms/cast/c/ap;->d:I

    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/c/an;->b(I)V

    .line 108
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v0, p0, Lcom/google/android/gms/cast/c/ap;->e:J

    sub-long v0, p2, v0

    iget-wide v4, p0, Lcom/google/android/gms/cast/c/ap;->c:J

    cmp-long v3, v0, v4

    if-lez v3, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    const/4 v2, 0x4

    invoke-virtual {p1, v2}, Lcom/google/android/gms/cast/c/an;->c(I)V

    iget-object v2, p1, Lcom/google/android/gms/cast/c/an;->a:[B

    iget v3, p1, Lcom/google/android/gms/cast/c/an;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p1, Lcom/google/android/gms/cast/c/an;->b:I

    const/16 v4, 0x18

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    iget-object v2, p1, Lcom/google/android/gms/cast/c/an;->a:[B

    iget v3, p1, Lcom/google/android/gms/cast/c/an;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p1, Lcom/google/android/gms/cast/c/an;->b:I

    const/16 v4, 0x10

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    iget-object v2, p1, Lcom/google/android/gms/cast/c/an;->a:[B

    iget v3, p1, Lcom/google/android/gms/cast/c/an;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p1, Lcom/google/android/gms/cast/c/an;->b:I

    const/16 v4, 0x8

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    iget-object v2, p1, Lcom/google/android/gms/cast/c/an;->a:[B

    iget v3, p1, Lcom/google/android/gms/cast/c/an;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p1, Lcom/google/android/gms/cast/c/an;->b:I

    and-long/2addr v0, v6

    long-to-int v0, v0

    int-to-byte v0, v0

    aput-byte v0, v2, v3

    .line 110
    iget v0, p1, Lcom/google/android/gms/cast/c/an;->b:I

    .line 111
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/google/android/gms/cast/c/an;->b(I)V

    .line 112
    iget v1, p1, Lcom/google/android/gms/cast/c/an;->b:I

    .line 114
    invoke-virtual {p0, p1}, Lcom/google/android/gms/cast/c/ap;->a(Lcom/google/android/gms/cast/c/an;)V

    .line 117
    iget v2, p1, Lcom/google/android/gms/cast/c/an;->b:I

    .line 118
    sub-int v1, v2, v1

    .line 119
    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/c/an;->a(I)V

    .line 120
    invoke-virtual {p1, v1}, Lcom/google/android/gms/cast/c/an;->b(I)V

    .line 121
    invoke-virtual {p1}, Lcom/google/android/gms/cast/c/an;->a()V

    .line 122
    return-void

    .line 108
    :cond_0
    iget-wide v4, p0, Lcom/google/android/gms/cast/c/ap;->c:J

    sub-long v0, v4, v0

    goto :goto_0
.end method

.method public final a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ap;->b:[Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 134
    instance-of v1, p1, Lcom/google/android/gms/cast/c/ap;

    if-nez v1, :cond_1

    .line 140
    :cond_0
    :goto_0
    return v0

    .line 138
    :cond_1
    check-cast p1, Lcom/google/android/gms/cast/c/ap;

    .line 140
    iget-object v1, p0, Lcom/google/android/gms/cast/c/ap;->b:[Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/cast/c/ap;->b:[Ljava/lang/String;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/gms/cast/c/ap;->a:I

    iget v2, p1, Lcom/google/android/gms/cast/c/ap;->a:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 145
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/cast/c/ap;->b:[Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/cast/c/ap;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
