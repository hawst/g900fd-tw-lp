.class public final Lcom/google/android/gms/drive/database/model/ax;
.super Lcom/google/android/gms/drive/database/model/ad;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljavax/crypto/SecretKey;

.field public g:J

.field public final h:J


# direct methods
.method private constructor <init>(Lcom/google/android/gms/drive/database/model/ay;)V
    .locals 3

    .prologue
    .line 71
    iget-object v0, p1, Lcom/google/android/gms/drive/database/model/ay;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->a()Lcom/google/android/gms/drive/database/model/az;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/drive/database/model/ad;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)V

    .line 72
    iget-object v0, p1, Lcom/google/android/gms/drive/database/model/ay;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ax;->a:Ljava/lang/String;

    .line 73
    iget-wide v0, p1, Lcom/google/android/gms/drive/database/model/ay;->f:J

    iput-wide v0, p0, Lcom/google/android/gms/drive/database/model/ax;->g:J

    .line 74
    iget-wide v0, p1, Lcom/google/android/gms/drive/database/model/ay;->g:J

    iput-wide v0, p0, Lcom/google/android/gms/drive/database/model/ax;->h:J

    .line 75
    iget-object v0, p1, Lcom/google/android/gms/drive/database/model/ay;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ax;->b:Ljava/lang/String;

    .line 76
    iget-object v0, p1, Lcom/google/android/gms/drive/database/model/ay;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ax;->c:Ljava/lang/String;

    .line 77
    iget-object v0, p1, Lcom/google/android/gms/drive/database/model/ay;->e:Ljavax/crypto/SecretKey;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ax;->d:Ljavax/crypto/SecretKey;

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ax;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ax;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "At least one of internalFilename or sharedFilename must be set."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 80
    return-void

    .line 78
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/database/model/ay;B)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/database/model/ax;-><init>(Lcom/google/android/gms/drive/database/model/ay;)V

    return-void
.end method

.method public static a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/ax;
    .locals 12

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 226
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->a:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 227
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->b:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 228
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->c:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 229
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->d:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v9

    .line 231
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->e:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v10

    .line 233
    const/4 v0, 0x0

    .line 234
    if-eqz v10, :cond_3

    .line 235
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->f:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    .line 237
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->g:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v11

    .line 239
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v0, v6, v11}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    move-object v6, v0

    :goto_0
    move-object v0, p0

    .line 241
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/drive/database/model/ax;->a(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;JJ)Lcom/google/android/gms/drive/database/model/ay;

    move-result-object v2

    iput-object v9, v2, Lcom/google/android/gms/drive/database/model/ay;->c:Ljava/lang/String;

    if-nez v6, :cond_0

    move v1, v7

    :goto_1
    if-nez v10, :cond_1

    move v0, v7

    :goto_2
    if-ne v1, v0, :cond_2

    move v0, v7

    :goto_3
    const-string v1, "encryptionKey must be set if and only if sharedFilename is set."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    iput-object v10, v2, Lcom/google/android/gms/drive/database/model/ay;->d:Ljava/lang/String;

    iput-object v6, v2, Lcom/google/android/gms/drive/database/model/ay;->e:Ljavax/crypto/SecretKey;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ay;->a()Lcom/google/android/gms/drive/database/model/ax;

    move-result-object v0

    .line 245
    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->a()Lcom/google/android/gms/drive/database/model/az;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/az;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/drive/database/model/ax;->d(J)V

    .line 247
    return-object v0

    :cond_0
    move v1, v8

    .line 241
    goto :goto_1

    :cond_1
    move v0, v8

    goto :goto_2

    :cond_2
    move v0, v8

    goto :goto_3

    :cond_3
    move-object v6, v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;JJ)Lcom/google/android/gms/drive/database/model/ay;
    .locals 10

    .prologue
    .line 87
    new-instance v1, Lcom/google/android/gms/drive/database/model/ay;

    const/4 v8, 0x0

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/drive/database/model/ay;-><init>(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;JJB)V

    return-object v1
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 197
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->a:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ax;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->b:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/ax;->g:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 199
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->c:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/ax;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 200
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ax;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 201
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->d:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ax;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ax;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 206
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->e:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ax;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ax;->d:Ljavax/crypto/SecretKey;

    if-eqz v0, :cond_2

    .line 211
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->f:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ax;->d:Ljavax/crypto/SecretKey;

    invoke-interface {v1}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 213
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->g:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ax;->d:Ljavax/crypto/SecretKey;

    invoke-interface {v1}, Ljavax/crypto/SecretKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    :goto_2
    return-void

    .line 203
    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->d:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0

    .line 208
    :cond_1
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->e:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_1

    .line 217
    :cond_2
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->f:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 218
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->g:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 127
    if-nez p1, :cond_0

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ax;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "internal and shared filenames cannot both be null"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 131
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ax;->b:Ljava/lang/String;

    .line 132
    return-void

    .line 128
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 252
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FileContent [contentHash="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ax;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", internalFilename="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ax;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sharedFilename="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ax;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", encryptionKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ax;->d:Ljavax/crypto/SecretKey;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lastAccessedTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/ax;->g:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/ax;->h:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
