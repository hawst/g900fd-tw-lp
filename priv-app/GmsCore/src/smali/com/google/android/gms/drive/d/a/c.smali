.class public final Lcom/google/android/gms/drive/d/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/d/f;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/util/Map;

.field private static final c:Ljava/util/Map;

.field private static final d:Ljava/util/Set;


# instance fields
.field private e:Lcom/google/android/gms/drive/d/a/g;

.field private f:Lcom/google/android/gms/drive/d/a/g;

.field private final g:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 76
    const-string v0, "application/vnd.google-apps.folder"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "mimeType = \'%s\'"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/d/a/c;->a:Ljava/lang/String;

    .line 92
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/d/a/c;->b:Ljava/util/Map;

    .line 95
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/d/a/c;->c:Ljava/util/Map;

    .line 97
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "authorizedAppIds"

    aput-object v2, v1, v3

    const-string v2, "properties"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string v3, "creatorAppId"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/google/android/gms/drive/d/a/c;->d:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object p1, p0, Lcom/google/android/gms/drive/d/a/c;->g:Landroid/content/Context;

    .line 111
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/drive/d/a/g;
    .locals 7

    .prologue
    .line 587
    invoke-static {}, Lcom/google/android/gms/drive/d/a/e;->a()Ljava/lang/String;

    move-result-object v6

    .line 588
    new-instance v0, Lcom/google/android/gms/drive/d/a/g;

    sget-object v1, Lcom/google/android/gms/drive/ai;->T:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v4, 0x1

    sget-object v1, Lcom/google/android/gms/drive/ai;->ao:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/d/a/g;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)V

    .line 596
    if-eqz v6, :cond_0

    sget-object v1, Lcom/google/android/gms/drive/ai;->w:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 597
    const-string v1, "trace.deb"

    const-string v2, "genoa:3:all,driveService:3:all"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/d/a/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    :cond_0
    return-object v0
.end method

.method private a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/internal/model/File;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/d/d;
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 322
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/d/a/c;->c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/drive/d/a/g;

    move-result-object v0

    .line 323
    new-instance v4, Lcom/google/android/gms/drive/f/g;

    invoke-direct {v4, v0}, Lcom/google/android/gms/drive/f/g;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 324
    new-instance v3, Lcom/google/android/gms/drive/f/l;

    invoke-direct {v3}, Lcom/google/android/gms/drive/f/l;-><init>()V

    const-class v5, Lcom/google/android/gms/drive/internal/model/File;

    invoke-static {p1}, Lcom/google/android/gms/drive/d/a/c;->b(Lcom/google/android/gms/common/server/ClientContext;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    :goto_0
    invoke-static {v5, v0}, Lcom/google/android/gms/drive/d/a/c;->b(Ljava/lang/Class;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/drive/f/l;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/f/l;

    .line 327
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v3, "files"

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/f/l;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz v5, :cond_1

    const-string v0, "convert"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v0, v5}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz v6, :cond_2

    const-string v0, "ocr"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v0, v5}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    if-eqz v7, :cond_3

    const-string v0, "pinned"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v0, v5}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_3
    if-eqz v1, :cond_4

    const-string v0, "useContentAsIndexableText"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_4
    iget-object v0, v4, Lcom/google/android/gms/drive/f/g;->a:Lcom/google/android/gms/common/server/n;

    const-class v5, Lcom/google/android/gms/drive/internal/model/File;

    move-object v1, p1

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/File;

    .line 340
    new-instance v1, Lcom/google/android/gms/drive/d/a/b;

    invoke-direct {v1, v0, p3, p1}, Lcom/google/android/gms/drive/d/a/b;-><init>(Lcom/google/android/gms/drive/internal/model/File;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/common/server/ClientContext;)V

    return-object v1

    :cond_5
    move v0, v1

    .line 324
    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/d/g;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 189
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/d/a/c;->c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/drive/d/a/g;

    move-result-object v2

    .line 190
    new-instance v4, Lcom/google/android/gms/drive/f/g;

    invoke-direct {v4, v2}, Lcom/google/android/gms/drive/f/g;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 193
    :try_start_0
    new-instance v2, Lcom/google/android/gms/drive/f/m;

    invoke-direct {v2}, Lcom/google/android/gms/drive/f/m;-><init>()V

    const-class v3, Lcom/google/android/gms/drive/internal/model/FileList;

    invoke-static {p1}, Lcom/google/android/gms/drive/d/a/c;->b(Lcom/google/android/gms/common/server/ClientContext;)Z

    move-result v5

    if-nez v5, :cond_6

    :goto_0
    invoke-static {v3, v0}, Lcom/google/android/gms/drive/d/a/c;->b(Ljava/lang/Class;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/drive/f/m;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/f/m;

    .line 196
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/gms/drive/d/a/c;->a(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x64

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v3, "files"

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/f/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz v1, :cond_1

    const-string v0, "allProperties"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz v2, :cond_2

    const-string v0, "fileScopeAppIds"

    invoke-static {v2}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    if-eqz v5, :cond_3

    const-string v0, "maxResults"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_3
    if-eqz p3, :cond_4

    const-string v0, "pageToken"

    invoke-static {p3}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_4
    if-eqz p4, :cond_5

    const-string v0, "q"

    invoke-static {p4}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_5
    iget-object v0, v4, Lcom/google/android/gms/drive/f/g;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/drive/internal/model/FileList;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/FileList;

    .line 210
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/FileList;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/internal/model/File;

    new-instance v4, Lcom/google/android/gms/drive/d/a/b;

    const/4 v5, 0x0

    invoke-direct {v4, v1, v5, p1}, Lcom/google/android/gms/drive/d/a/b;-><init>(Lcom/google/android/gms/drive/internal/model/File;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/common/server/ClientContext;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 211
    :catch_0
    move-exception v0

    .line 212
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_6
    move v0, v1

    .line 193
    goto/16 :goto_0

    .line 210
    :cond_7
    :try_start_1
    new-instance v1, Lcom/google/android/gms/drive/d/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/FileList;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/gms/drive/d/a/d;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_1

    return-object v1

    .line 213
    :catch_1
    move-exception v0

    .line 214
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Ljava/lang/String;Z)Lcom/google/android/gms/drive/internal/model/FileLocalId;
    .locals 3

    .prologue
    .line 691
    new-instance v0, Lcom/google/android/gms/drive/internal/model/FileLocalId;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/model/FileLocalId;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/gms/drive/internal/model/FileLocalId;->f(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/FileLocalId;

    move-result-object v1

    if-eqz p1, :cond_1

    const-string v0, "APPDATA"

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/internal/model/FileLocalId;->e(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/FileLocalId;

    move-result-object v1

    .line 697
    sget-object v0, Lcom/google/android/gms/drive/ai;->U:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 698
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 699
    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/internal/model/FileLocalId;->g(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/FileLocalId;

    .line 702
    :cond_0
    return-object v1

    .line 691
    :cond_1
    const-string v0, "DRIVE"

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Z)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 612
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 614
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;

    .line 616
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 617
    if-eqz p1, :cond_1

    .line 618
    sget-object v1, Lcom/google/android/gms/drive/d/a/c;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 619
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-eqz v1, :cond_2

    .line 623
    const-string v1, ","

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 625
    :cond_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 626
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->j()Ljava/lang/Class;

    move-result-object v0

    .line 628
    if-eqz v0, :cond_0

    .line 629
    invoke-static {v0, p1}, Lcom/google/android/gms/drive/d/a/c;->b(Ljava/lang/Class;Z)Ljava/lang/String;

    move-result-object v0

    .line 631
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 632
    const-string v1, "("

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 633
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 634
    const-string v0, ")"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 638
    :catch_0
    move-exception v0

    .line 639
    const-string v1, "ApiaryRemoteResourceAccessor"

    const-string v3, "Unable to create instance: %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p0, v4, v5

    invoke-static {v1, v0, v3, v4}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 643
    :cond_3
    :goto_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 640
    :catch_1
    move-exception v0

    .line 641
    const-string v1, "ApiaryRemoteResourceAccessor"

    const-string v3, "Unable to create instance: %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p0, v4, v5

    invoke-static {v1, v0, v3, v4}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1
.end method

.method private static a(Ljava/util/Set;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 181
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    invoke-interface {p0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ","

    invoke-interface {p0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/android/volley/ac;)Z
    .locals 2

    .prologue
    .line 711
    iget-object v0, p0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v0, v0, Lcom/android/volley/m;->a:I

    const/16 v1, 0x194

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/lang/Class;Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 656
    if-eqz p1, :cond_1

    sget-object v0, Lcom/google/android/gms/drive/d/a/c;->c:Ljava/util/Map;

    move-object v1, v0

    .line 658
    :goto_0
    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 659
    if-nez v0, :cond_0

    .line 660
    invoke-static {p0, p1}, Lcom/google/android/gms/drive/d/a/c;->a(Ljava/lang/Class;Z)Ljava/lang/String;

    move-result-object v0

    .line 661
    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 663
    :cond_0
    return-object v0

    .line 656
    :cond_1
    sget-object v0, Lcom/google/android/gms/drive/d/a/c;->b:Ljava/util/Map;

    move-object v1, v0

    goto :goto_0
.end method

.method public static b(Lcom/google/android/gms/common/server/ClientContext;)Z
    .locals 2

    .prologue
    .line 105
    const-string v0, "com.google.android.gms"

    invoke-virtual {p0}, Lcom/google/android/gms/common/server/ClientContext;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/drive/d/a/g;
    .locals 2

    .prologue
    .line 573
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/c;->e:Lcom/google/android/gms/drive/d/a/g;

    if-nez v0, :cond_0

    .line 574
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/c;->g:Landroid/content/Context;

    const-string v1, "/drive/v2beta/"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/d/a/c;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/drive/d/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/d/a/c;->e:Lcom/google/android/gms/drive/d/a/g;

    .line 577
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/c;->f:Lcom/google/android/gms/drive/d/a/g;

    if-nez v0, :cond_1

    .line 578
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/c;->g:Landroid/content/Context;

    const-string v1, "/drive/v2internal/"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/d/a/c;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/drive/d/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/d/a/c;->f:Lcom/google/android/gms/drive/d/a/g;

    .line 582
    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/drive/d/a/c;->b(Lcom/google/android/gms/common/server/ClientContext;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/c;->f:Lcom/google/android/gms/drive/d/a/g;

    :goto_0
    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/c;->e:Lcom/google/android/gms/drive/d/a/g;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/ClientContext;)J
    .locals 6

    .prologue
    .line 511
    :try_start_0
    new-instance v0, Lcom/google/android/gms/drive/f/c;

    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/d/a/c;->c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/drive/d/a/g;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/f/c;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 512
    new-instance v1, Lcom/google/android/gms/drive/f/d;

    invoke-direct {v1}, Lcom/google/android/gms/drive/f/d;-><init>()V

    .line 513
    const-string v2, "self"

    const-string v3, "apps/%1$s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2}, Lcom/google/android/gms/drive/f/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/f/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/gms/drive/f/c;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/drive/internal/model/App;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/App;

    .line 514
    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/App;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    return-wide v0

    .line 515
    :catch_0
    move-exception v0

    .line 516
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;JJ)Lcom/google/android/gms/drive/d/a;
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 118
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/d/a/c;->c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/drive/d/a/g;

    move-result-object v0

    .line 119
    new-instance v1, Lcom/google/android/gms/drive/f/a;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/f/a;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 121
    new-instance v0, Lcom/google/android/gms/drive/f/b;

    invoke-direct {v0}, Lcom/google/android/gms/drive/f/b;-><init>()V

    const-class v2, Lcom/google/android/gms/drive/internal/model/About;

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/d/a/c;->b(Ljava/lang/Class;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/f/b;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/f/b;

    .line 125
    const/4 v2, 0x1

    :try_start_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const-string v3, "about"

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/f/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz v2, :cond_1

    const-string v0, "includeSubscribed"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/drive/f/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz v4, :cond_2

    const-string v0, "maxChangeIdCount"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/drive/f/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    if-eqz v5, :cond_3

    const-string v0, "startChangeId"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/drive/f/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_3
    iget-object v0, v1, Lcom/google/android/gms/drive/f/a;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/drive/internal/model/About;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/About;

    .line 132
    new-instance v1, Lcom/google/android/gms/drive/d/a/a;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/d/a/a;-><init>(Lcom/google/android/gms/drive/internal/model/About;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    return-object v1

    .line 133
    :catch_0
    move-exception v0

    .line 134
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 135
    :catch_1
    move-exception v0

    .line 136
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/d/d;
    .locals 2

    .prologue
    .line 311
    :try_start_0
    invoke-static {p2}, Lcom/google/android/gms/drive/metadata/a/ax;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v0

    .line 312
    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/gms/drive/d/a/c;->a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/internal/model/File;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/d/d;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 313
    :catch_0
    move-exception v0

    .line 314
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 315
    :catch_1
    move-exception v0

    .line 316
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/auth/g;Z)Lcom/google/android/gms/drive/d/d;
    .locals 2

    .prologue
    .line 294
    :try_start_0
    invoke-static {p2}, Lcom/google/android/gms/drive/metadata/a/ax;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v1

    .line 295
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, p4}, Lcom/google/android/gms/drive/d/a/c;->a(Ljava/lang/String;Z)Lcom/google/android/gms/drive/internal/model/FileLocalId;

    move-result-object v0

    .line 299
    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/internal/model/File;->a(Lcom/google/android/gms/drive/internal/model/FileLocalId;)Lcom/google/android/gms/drive/internal/model/File;

    .line 301
    :cond_0
    invoke-direct {p0, p1, v1, p3}, Lcom/google/android/gms/drive/d/a/c;->a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/internal/model/File;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/d/d;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 302
    :catch_0
    move-exception v0

    .line 303
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/d/d;
    .locals 6

    .prologue
    .line 275
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/d/a/c;->c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/drive/d/a/g;

    move-result-object v0

    .line 276
    new-instance v2, Lcom/google/android/gms/drive/f/g;

    invoke-direct {v2, v0}, Lcom/google/android/gms/drive/f/g;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 279
    :try_start_0
    new-instance v0, Lcom/google/android/gms/drive/f/h;

    invoke-direct {v0}, Lcom/google/android/gms/drive/f/h;-><init>()V

    const-class v1, Lcom/google/android/gms/drive/internal/model/File;

    const/4 v3, 0x0

    invoke-static {v1, v3}, Lcom/google/android/gms/drive/d/a/c;->b(Ljava/lang/Class;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/f/h;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/f/h;

    .line 281
    const-string v1, "files/%1$s/authorize"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p3}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/f/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v1, "appId"

    invoke-static {p2}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v2, Lcom/google/android/gms/drive/f/g;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/drive/internal/model/File;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/File;

    .line 282
    new-instance v1, Lcom/google/android/gms/drive/d/a/b;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2, p1}, Lcom/google/android/gms/drive/d/a/b;-><init>(Lcom/google/android/gms/drive/internal/model/File;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/common/server/ClientContext;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    return-object v1

    .line 283
    :catch_0
    move-exception v0

    .line 284
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 285
    :catch_1
    move-exception v0

    .line 286
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/gms/drive/d/d;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 249
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/d/a/c;->c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/drive/d/a/g;

    move-result-object v3

    .line 250
    new-instance v0, Lcom/google/android/gms/drive/f/g;

    invoke-direct {v0, v3}, Lcom/google/android/gms/drive/f/g;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 253
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/drive/d/a/c;->b(Lcom/google/android/gms/common/server/ClientContext;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 254
    :goto_0
    new-instance v2, Lcom/google/android/gms/drive/f/k;

    invoke-direct {v2}, Lcom/google/android/gms/drive/f/k;-><init>()V

    const-class v3, Lcom/google/android/gms/drive/internal/model/File;

    invoke-static {v3, v1}, Lcom/google/android/gms/drive/d/a/c;->b(Ljava/lang/Class;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/gms/drive/f/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v7

    check-cast v7, Lcom/google/android/gms/drive/f/k;

    .line 256
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {p3}, Lcom/google/android/gms/drive/d/a/c;->a(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v5

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/drive/f/g;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Lcom/google/android/gms/drive/f/k;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v0

    .line 266
    new-instance v1, Lcom/google/android/gms/drive/d/a/b;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2, p1}, Lcom/google/android/gms/drive/d/a/b;-><init>(Lcom/google/android/gms/drive/internal/model/File;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/common/server/ClientContext;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :cond_0
    move v1, v2

    .line 253
    goto :goto_0

    .line 267
    :catch_0
    move-exception v0

    .line 268
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/d/e;
    .locals 14

    .prologue
    .line 474
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/d/a/c;->c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/drive/d/a/g;

    move-result-object v0

    .line 475
    new-instance v1, Lcom/google/android/gms/drive/f/g;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/f/g;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 478
    :try_start_0
    new-instance v0, Lcom/google/android/gms/drive/f/q;

    invoke-direct {v0}, Lcom/google/android/gms/drive/f/q;-><init>()V

    .line 479
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/drive/metadata/a/ax;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v4

    const-string v3, "files/%1$s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v3, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/f/q;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_0

    const-string v0, "convert"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz v5, :cond_1

    const-string v0, "newRevision"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz v6, :cond_2

    const-string v0, "ocr"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    if-eqz v7, :cond_3

    const-string v0, "pinned"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_3
    if-eqz v8, :cond_4

    const-string v0, "setModifiedDate"

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_4
    if-eqz v9, :cond_5

    const-string v0, "updateViewedDate"

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_5
    if-eqz v10, :cond_6

    const-string v0, "useContentAsIndexableText"

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_6
    iget-object v0, v1, Lcom/google/android/gms/drive/f/g;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x2

    const-class v5, Lcom/google/android/gms/drive/internal/model/File;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/File;

    .line 499
    new-instance v1, Lcom/google/android/gms/drive/d/e;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/drive/d/e;-><init>(Lcom/google/android/gms/drive/internal/model/File;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    return-object v1

    .line 500
    :catch_0
    move-exception v0

    .line 501
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 502
    :catch_1
    move-exception v0

    .line 503
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;J)Lcom/google/android/gms/drive/d/g;
    .locals 10

    .prologue
    .line 221
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/d/a/c;->c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/drive/d/a/g;

    move-result-object v0

    .line 222
    new-instance v1, Lcom/google/android/gms/drive/f/e;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/f/e;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 224
    :try_start_0
    new-instance v0, Lcom/google/android/gms/drive/f/f;

    invoke-direct {v0}, Lcom/google/android/gms/drive/f/f;-><init>()V

    const-class v2, Lcom/google/android/gms/drive/internal/model/ChangeList;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/d/a/c;->b(Ljava/lang/Class;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/f/f;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/f/f;

    .line 227
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {p2}, Lcom/google/android/gms/drive/d/a/c;->a(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const/16 v3, 0x64

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    const-string v3, "changes"

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/f/f;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz v2, :cond_1

    const-string v0, "allProperties"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/drive/f/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz v4, :cond_2

    const-string v0, "fileScopeAppIds"

    invoke-static {v4}, Lcom/google/android/gms/drive/f/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/drive/f/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    if-eqz v5, :cond_3

    const-string v0, "includeDeleted"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/drive/f/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_3
    if-eqz v6, :cond_4

    const-string v0, "includeSubscribed"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/drive/f/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_4
    if-eqz v7, :cond_5

    const-string v0, "maxResults"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/drive/f/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_5
    if-eqz p3, :cond_6

    const-string v0, "pageToken"

    invoke-static {p3}, Lcom/google/android/gms/drive/f/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/drive/f/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_6
    if-eqz v8, :cond_7

    const-string v0, "startChangeId"

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/drive/f/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_7
    iget-object v0, v1, Lcom/google/android/gms/drive/f/e;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/drive/internal/model/ChangeList;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/ChangeList;

    .line 238
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/ChangeList;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/internal/model/Change;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/model/Change;->b()Z

    move-result v2

    if-eqz v2, :cond_8

    new-instance v2, Lcom/google/android/gms/drive/d/a/f;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/model/Change;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/google/android/gms/drive/d/a/f;-><init>(Ljava/lang/String;)V

    move-object v1, v2

    :goto_1
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 239
    :catch_0
    move-exception v0

    .line 240
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 238
    :cond_8
    :try_start_1
    new-instance v2, Lcom/google/android/gms/drive/d/a/b;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/model/Change;->c()Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v1

    const/4 v5, 0x0

    invoke-direct {v2, v1, v5, p1}, Lcom/google/android/gms/drive/d/a/b;-><init>(Lcom/google/android/gms/drive/internal/model/File;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/common/server/ClientContext;)V

    move-object v1, v2

    goto :goto_1

    :cond_9
    new-instance v1, Lcom/google/android/gms/drive/d/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/ChangeList;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/ChangeList;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {v1, v3, v2, v0}, Lcom/google/android/gms/drive/d/a/d;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_1

    return-object v1

    .line 241
    :catch_1
    move-exception v0

    .line 242
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/d/g;
    .locals 6

    .prologue
    .line 177
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/d/a/c;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/d/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;Z)Lcom/google/android/gms/drive/d/g;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 169
    if-eqz p4, :cond_0

    sget-object v4, Lcom/google/android/gms/drive/d/a/c;->a:Ljava/lang/String;

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/d/a/c;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/d/g;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v4, v5

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 6

    .prologue
    .line 669
    new-instance v0, Lcom/google/android/gms/drive/internal/model/File;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/model/File;-><init>()V

    invoke-static {p2, p3}, Lcom/google/android/gms/drive/d/a/c;->a(Ljava/lang/String;Z)Lcom/google/android/gms/drive/internal/model/FileLocalId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/internal/model/File;->a(Lcom/google/android/gms/drive/internal/model/FileLocalId;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v4

    .line 672
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/d/a/c;->c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/drive/d/a/g;

    move-result-object v0

    .line 673
    new-instance v1, Lcom/google/android/gms/drive/f/g;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/f/g;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 674
    new-instance v0, Lcom/google/android/gms/drive/f/j;

    invoke-direct {v0}, Lcom/google/android/gms/drive/f/j;-><init>()V

    .line 676
    :try_start_0
    const-string v2, "files/generateId"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/f/j;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Lcom/google/android/gms/drive/f/g;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const-class v5, Lcom/google/android/gms/drive/internal/model/File;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/File;

    .line 678
    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->s()Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 679
    :catch_0
    move-exception v0

    .line 680
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 681
    :catch_1
    move-exception v0

    .line 682
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/drive/auth/g;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 142
    invoke-virtual {p1}, Lcom/google/android/gms/drive/auth/g;->b()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    .line 143
    invoke-direct {p0, v1}, Lcom/google/android/gms/drive/d/a/c;->c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/drive/d/a/g;

    move-result-object v2

    .line 144
    new-instance v0, Lcom/google/android/gms/drive/f/g;

    invoke-direct {v0, v2}, Lcom/google/android/gms/drive/f/g;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 147
    :try_start_0
    new-instance v2, Lcom/google/android/gms/drive/f/k;

    invoke-direct {v2}, Lcom/google/android/gms/drive/f/k;-><init>()V

    const-string v3, "id"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/f/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v7

    check-cast v7, Lcom/google/android/gms/drive/f/k;

    .line 148
    const-string v2, "appdata"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/drive/f/g;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Lcom/google/android/gms/drive/f/k;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v0

    .line 158
    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->s()Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 159
    :catch_0
    move-exception v0

    .line 160
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 161
    :catch_1
    move-exception v0

    .line 162
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 346
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/d/a/c;->c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/drive/d/a/g;

    move-result-object v0

    .line 347
    new-instance v1, Lcom/google/android/gms/drive/f/g;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/f/g;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 350
    :try_start_0
    new-instance v0, Lcom/google/android/gms/drive/f/o;

    invoke-direct {v0}, Lcom/google/android/gms/drive/f/o;-><init>()V

    .line 351
    const-string v2, "files/%1$s/trash"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/f/o;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Lcom/google/android/gms/drive/f/g;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/drive/internal/model/File;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    .line 356
    return-void

    .line 352
    :catch_0
    move-exception v0

    .line 353
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 354
    :catch_1
    move-exception v0

    .line 355
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 362
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/d/a/c;->c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/drive/d/a/g;

    move-result-object v0

    .line 363
    new-instance v1, Lcom/google/android/gms/drive/f/g;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/f/g;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 366
    :try_start_0
    new-instance v0, Lcom/google/android/gms/drive/f/p;

    invoke-direct {v0}, Lcom/google/android/gms/drive/f/p;-><init>()V

    .line 367
    const-string v2, "files/%1$s/untrash"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/f/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Lcom/google/android/gms/drive/f/g;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/drive/internal/model/File;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    .line 372
    return-void

    .line 368
    :catch_0
    move-exception v0

    .line 369
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 370
    :catch_1
    move-exception v0

    .line 371
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/Set;)V
    .locals 7

    .prologue
    .line 399
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/d/a/c;->c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/drive/d/a/g;

    move-result-object v0

    .line 400
    new-instance v1, Lcom/google/android/gms/drive/f/g;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/f/g;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 403
    :try_start_0
    new-instance v4, Lcom/google/android/gms/drive/internal/model/File;

    invoke-direct {v4}, Lcom/google/android/gms/drive/internal/model/File;-><init>()V

    .line 404
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 405
    invoke-interface {p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 406
    new-instance v5, Lcom/google/android/gms/drive/internal/model/ParentReference;

    invoke-direct {v5}, Lcom/google/android/gms/drive/internal/model/ParentReference;-><init>()V

    .line 407
    invoke-virtual {v5, v0}, Lcom/google/android/gms/drive/internal/model/ParentReference;->e(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/ParentReference;

    .line 408
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 430
    :catch_0
    move-exception v0

    .line 431
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 410
    :cond_0
    :try_start_1
    invoke-virtual {v4, v2}, Lcom/google/android/gms/drive/internal/model/File;->a(Ljava/util/List;)Lcom/google/android/gms/drive/internal/model/File;

    .line 411
    new-instance v0, Lcom/google/android/gms/drive/f/n;

    invoke-direct {v0}, Lcom/google/android/gms/drive/f/n;-><init>()V

    .line 412
    const-string v2, "files/%1$s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/f/n;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Lcom/google/android/gms/drive/f/g;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x7

    const-class v5, Lcom/google/android/gms/drive/internal/model/File;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_1

    .line 434
    return-void

    .line 432
    :catch_1
    move-exception v0

    .line 433
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 378
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/d/a/c;->c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/drive/d/a/g;

    move-result-object v0

    .line 379
    new-instance v1, Lcom/google/android/gms/drive/f/g;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/f/g;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 382
    :try_start_0
    new-instance v0, Lcom/google/android/gms/drive/f/i;

    invoke-direct {v0}, Lcom/google/android/gms/drive/f/i;-><init>()V

    .line 383
    const-string v2, "files/%1$s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Lcom/google/android/gms/drive/f/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/f/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/gms/drive/f/g;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v0, v3}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    .line 394
    :goto_0
    return-void

    .line 384
    :catch_0
    move-exception v0

    .line 385
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 386
    :catch_1
    move-exception v0

    .line 389
    invoke-static {v0}, Lcom/google/android/gms/drive/d/a/c;->a(Lcom/android/volley/ac;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 390
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 392
    :cond_0
    const-string v0, "ApiaryRemoteResourceAccessor"

    const-string v1, "Not found, assuming already deleted"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
