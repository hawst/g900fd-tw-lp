.class public final Lcom/google/android/gms/wallet/common/aa;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/identity/intents/model/CountrySpecification;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/identity/intents/model/CountrySpecification;

    const-string v1, "US"

    invoke-direct {v0, v1}, Lcom/google/android/gms/identity/intents/model/CountrySpecification;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/wallet/common/aa;->a:Lcom/google/android/gms/identity/intents/model/CountrySpecification;

    return-void
.end method

.method public static a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 74
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->j()[Lcom/google/android/gms/wallet/CountrySpecification;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->j()[Lcom/google/android/gms/wallet/CountrySpecification;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_2

    .line 78
    invoke-static {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Lcom/google/android/gms/wallet/i;

    move-result-object v0

    .line 80
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->j()[Lcom/google/android/gms/wallet/CountrySpecification;

    move-result-object v2

    array-length v2, v2

    .line 81
    :goto_0
    if-ge v1, v2, :cond_0

    .line 82
    new-instance v3, Lcom/google/android/gms/identity/intents/model/CountrySpecification;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->j()[Lcom/google/android/gms/wallet/CountrySpecification;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/CountrySpecification;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/gms/identity/intents/model/CountrySpecification;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/i;->a(Lcom/google/android/gms/identity/intents/model/CountrySpecification;)Lcom/google/android/gms/wallet/i;

    .line 81
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 85
    :cond_0
    iget-object p0, v0, Lcom/google/android/gms/wallet/i;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    .line 104
    :cond_1
    :goto_1
    return-object p0

    .line 89
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    :cond_3
    const-string v0, "RequestCompat"

    const-string v2, "Adding all supported countries to allowed shipping country codes"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    sget-object v0, Lcom/google/android/gms/wallet/b/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 94
    array-length v3, v2

    .line 95
    invoke-static {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Lcom/google/android/gms/wallet/i;

    move-result-object v4

    move v0, v1

    .line 97
    :goto_2
    if-ge v0, v3, :cond_4

    .line 98
    new-instance v1, Lcom/google/android/gms/identity/intents/model/CountrySpecification;

    aget-object v5, v2, v0

    invoke-direct {v1, v5}, Lcom/google/android/gms/identity/intents/model/CountrySpecification;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Lcom/google/android/gms/wallet/i;->a(Lcom/google/android/gms/identity/intents/model/CountrySpecification;)Lcom/google/android/gms/wallet/i;

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 101
    :cond_4
    iget-object p0, v4, Lcom/google/android/gms/wallet/i;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    goto :goto_1
.end method

.method public static a(Lcom/google/android/gms/wallet/MaskedWalletRequest;)Lcom/google/android/gms/wallet/MaskedWalletRequest;
    .locals 5

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 37
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->m()[Lcom/google/android/gms/wallet/CountrySpecification;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->m()[Lcom/google/android/gms/wallet/CountrySpecification;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_2

    .line 39
    invoke-static {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;)Lcom/google/android/gms/wallet/q;

    move-result-object v1

    .line 40
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->m()[Lcom/google/android/gms/wallet/CountrySpecification;

    move-result-object v2

    array-length v2, v2

    .line 41
    :goto_0
    if-ge v0, v2, :cond_0

    .line 42
    new-instance v3, Lcom/google/android/gms/identity/intents/model/CountrySpecification;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->m()[Lcom/google/android/gms/wallet/CountrySpecification;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/CountrySpecification;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/gms/identity/intents/model/CountrySpecification;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Lcom/google/android/gms/wallet/q;->a(Lcom/google/android/gms/identity/intents/model/CountrySpecification;)Lcom/google/android/gms/wallet/q;

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45
    :cond_0
    iget-object p0, v1, Lcom/google/android/gms/wallet/q;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    .line 59
    :cond_1
    :goto_1
    return-object p0

    .line 49
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->p()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->p()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    :cond_3
    const-string v0, "RequestCompat"

    const-string v1, "Adding US allowed shipping country code"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-static {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;)Lcom/google/android/gms/wallet/q;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/wallet/common/aa;->a:Lcom/google/android/gms/identity/intents/model/CountrySpecification;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/q;->a(Lcom/google/android/gms/identity/intents/model/CountrySpecification;)Lcom/google/android/gms/wallet/q;

    move-result-object v0

    iget-object p0, v0, Lcom/google/android/gms/wallet/q;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    goto :goto_1
.end method
