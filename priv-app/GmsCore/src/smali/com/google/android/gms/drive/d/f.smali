.class public interface abstract Lcom/google/android/gms/drive/d/f;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract a(Lcom/google/android/gms/common/server/ClientContext;)J
.end method

.method public abstract a(Lcom/google/android/gms/common/server/ClientContext;JJ)Lcom/google/android/gms/drive/d/a;
.end method

.method public abstract a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/d/d;
.end method

.method public abstract a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/auth/g;Z)Lcom/google/android/gms/drive/d/d;
.end method

.method public abstract a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/d/d;
.end method

.method public abstract a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/gms/drive/d/d;
.end method

.method public abstract a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/d/e;
.end method

.method public abstract a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;J)Lcom/google/android/gms/drive/d/g;
.end method

.method public abstract a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/d/g;
.end method

.method public abstract a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;Z)Lcom/google/android/gms/drive/d/g;
.end method

.method public abstract a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Ljava/lang/String;
.end method

.method public abstract a(Lcom/google/android/gms/drive/auth/g;)Ljava/lang/String;
.end method

.method public abstract a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
.end method

.method public abstract b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
.end method

.method public abstract b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/Set;)V
.end method

.method public abstract c(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
.end method
