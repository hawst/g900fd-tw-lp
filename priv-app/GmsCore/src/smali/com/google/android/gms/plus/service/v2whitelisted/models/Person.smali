.class public final Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/an;

.field private static final L:Ljava/util/HashMap;


# instance fields
.field A:Ljava/util/List;

.field B:Ljava/util/List;

.field C:Ljava/util/List;

.field D:Ljava/lang/String;

.field E:Ljava/util/List;

.field F:Ljava/util/List;

.field G:Ljava/util/List;

.field H:Ljava/util/List;

.field I:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

.field J:Ljava/util/List;

.field K:Ljava/util/List;

.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/util/List;

.field d:Ljava/util/List;

.field e:Ljava/lang/String;

.field f:Ljava/util/List;

.field g:Ljava/util/List;

.field h:Ljava/util/List;

.field i:Ljava/util/List;

.field j:Ljava/util/List;

.field k:Ljava/lang/String;

.field l:Ljava/util/List;

.field m:Ljava/util/List;

.field n:Ljava/util/List;

.field o:Ljava/lang/String;

.field p:Ljava/util/List;

.field q:Ljava/util/List;

.field r:Ljava/lang/String;

.field s:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;

.field t:Ljava/util/List;

.field u:Ljava/util/List;

.field v:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

.field w:Ljava/util/List;

.field x:Ljava/util/List;

.field y:Ljava/util/List;

.field z:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/an;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/an;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/an;

    .line 218
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 221
    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "abouts"

    const-string v2, "abouts"

    const/4 v3, 0x2

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Abouts;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "addresses"

    const-string v2, "addresses"

    const/4 v3, 0x3

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "ageRange"

    const-string v2, "ageRange"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "birthdays"

    const-string v2, "birthdays"

    const/4 v3, 0x5

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "braggingRights"

    const-string v2, "braggingRights"

    const/4 v3, 0x6

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$BraggingRights;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "coverPhotos"

    const-string v2, "coverPhotos"

    const/4 v3, 0x7

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$CoverPhotos;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "customFields"

    const-string v2, "customFields"

    const/16 v3, 0x8

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$CustomFields;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "emails"

    const-string v2, "emails"

    const/16 v3, 0x9

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "etag"

    const-string v2, "etag"

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "events"

    const-string v2, "events"

    const/16 v3, 0xb

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Events;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "fileAs"

    const-string v2, "fileAs"

    const/16 v3, 0xc

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$FileAs;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "genders"

    const-string v2, "genders"

    const/16 v3, 0xd

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Genders;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "id"

    const-string v2, "id"

    const/16 v3, 0xe

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "images"

    const-string v2, "images"

    const/16 v3, 0xf

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "instantMessaging"

    const-string v2, "instantMessaging"

    const/16 v3, 0x10

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "language"

    const-string v2, "language"

    const/16 v3, 0x12

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "legacyFields"

    const-string v2, "legacyFields"

    const/16 v3, 0x13

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "linkedPeople"

    const-string v2, "linkedPeople"

    const/16 v3, 0x14

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "memberships"

    const-string v2, "memberships"

    const/16 v3, 0x15

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Memberships;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "metadata"

    const-string v2, "metadata"

    const/16 v3, 0x16

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "names"

    const-string v2, "names"

    const/16 v3, 0x17

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "nicknames"

    const-string v2, "nicknames"

    const/16 v3, 0x18

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Nicknames;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "occupations"

    const-string v2, "occupations"

    const/16 v3, 0x19

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Occupations;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "organizations"

    const-string v2, "organizations"

    const/16 v3, 0x1a

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Organizations;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "peopleInCommon"

    const-string v2, "peopleInCommon"

    const/16 v3, 0x1b

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "phoneNumbers"

    const-string v2, "phoneNumbers"

    const/16 v3, 0x1c

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "placesLived"

    const-string v2, "placesLived"

    const/16 v3, 0x1d

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PlacesLived;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "profileUrl"

    const-string v2, "profileUrl"

    const/16 v3, 0x1e

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "relations"

    const-string v2, "relations"

    const/16 v3, 0x1f

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Relations;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "relationshipInterests"

    const-string v2, "relationshipInterests"

    const/16 v3, 0x20

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$RelationshipInterests;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "relationshipStatuses"

    const-string v2, "relationshipStatuses"

    const/16 v3, 0x21

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$RelationshipStatuses;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "skills"

    const-string v2, "skills"

    const/16 v3, 0x22

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Skills;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "sortKeys"

    const-string v2, "sortKeys"

    const/16 v3, 0x23

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "taglines"

    const-string v2, "taglines"

    const/16 v3, 0x24

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Taglines;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    const-string v1, "urls"

    const-string v2, "urls"

    const/16 v3, 0x25

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Urls;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 523
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 524
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->b:I

    .line 525
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/Set;

    .line 526
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 567
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 568
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/Set;

    .line 569
    iput p2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->b:I

    .line 570
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->c:Ljava/util/List;

    .line 571
    iput-object p4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->d:Ljava/util/List;

    .line 572
    iput-object p5, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e:Ljava/lang/String;

    .line 573
    iput-object p6, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->f:Ljava/util/List;

    .line 574
    iput-object p7, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->g:Ljava/util/List;

    .line 575
    iput-object p8, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h:Ljava/util/List;

    .line 576
    iput-object p9, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->i:Ljava/util/List;

    .line 577
    iput-object p10, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->j:Ljava/util/List;

    .line 578
    iput-object p11, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->k:Ljava/lang/String;

    .line 579
    iput-object p12, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->l:Ljava/util/List;

    .line 580
    iput-object p13, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->m:Ljava/util/List;

    .line 581
    iput-object p14, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n:Ljava/util/List;

    .line 582
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->o:Ljava/lang/String;

    .line 583
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->p:Ljava/util/List;

    .line 584
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->q:Ljava/util/List;

    .line 585
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->r:Ljava/lang/String;

    .line 586
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->s:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;

    .line 587
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->t:Ljava/util/List;

    .line 588
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->u:Ljava/util/List;

    .line 589
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    .line 590
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->w:Ljava/util/List;

    .line 591
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->x:Ljava/util/List;

    .line 592
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->y:Ljava/util/List;

    .line 593
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->z:Ljava/util/List;

    .line 594
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->A:Ljava/util/List;

    .line 595
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->B:Ljava/util/List;

    .line 596
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->C:Ljava/util/List;

    .line 597
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->D:Ljava/lang/String;

    .line 598
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->E:Ljava/util/List;

    .line 599
    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->F:Ljava/util/List;

    .line 600
    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->G:Ljava/util/List;

    .line 601
    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->H:Ljava/util/List;

    .line 602
    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->I:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    .line 603
    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->J:Ljava/util/List;

    .line 604
    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->K:Ljava/util/List;

    .line 605
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 293
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 4

    .prologue
    .line 12277
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 12278
    sparse-switch v0, :sswitch_data_0

    .line 12289
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 12280
    :sswitch_0
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->s:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;

    .line 12293
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 12294
    return-void

    .line 12283
    :sswitch_1
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    goto :goto_0

    .line 12286
    :sswitch_2
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->I:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    goto :goto_0

    .line 12278
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x16 -> :sswitch_1
        0x23 -> :sswitch_2
    .end sparse-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 12250
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 12251
    sparse-switch v0, :sswitch_data_0

    .line 12268
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 12253
    :sswitch_0
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e:Ljava/lang/String;

    .line 12271
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 12272
    return-void

    .line 12256
    :sswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->k:Ljava/lang/String;

    goto :goto_0

    .line 12259
    :sswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->o:Ljava/lang/String;

    goto :goto_0

    .line 12262
    :sswitch_3
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->r:Ljava/lang/String;

    goto :goto_0

    .line 12265
    :sswitch_4
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->D:Ljava/lang/String;

    goto :goto_0

    .line 12251
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0xa -> :sswitch_1
        0xe -> :sswitch_2
        0x12 -> :sswitch_3
        0x1e -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 12299
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 12300
    packed-switch v0, :pswitch_data_0

    .line 12383
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known array of custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 12302
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->c:Ljava/util/List;

    .line 12387
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 12388
    return-void

    .line 12305
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->d:Ljava/util/List;

    goto :goto_0

    .line 12308
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->f:Ljava/util/List;

    goto :goto_0

    .line 12311
    :pswitch_4
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->g:Ljava/util/List;

    goto :goto_0

    .line 12314
    :pswitch_5
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h:Ljava/util/List;

    goto :goto_0

    .line 12317
    :pswitch_6
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->i:Ljava/util/List;

    goto :goto_0

    .line 12320
    :pswitch_7
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->j:Ljava/util/List;

    goto :goto_0

    .line 12323
    :pswitch_8
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->l:Ljava/util/List;

    goto :goto_0

    .line 12326
    :pswitch_9
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->m:Ljava/util/List;

    goto :goto_0

    .line 12329
    :pswitch_a
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n:Ljava/util/List;

    goto :goto_0

    .line 12332
    :pswitch_b
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->p:Ljava/util/List;

    goto :goto_0

    .line 12335
    :pswitch_c
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->q:Ljava/util/List;

    goto :goto_0

    .line 12338
    :pswitch_d
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->t:Ljava/util/List;

    goto :goto_0

    .line 12341
    :pswitch_e
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->u:Ljava/util/List;

    goto :goto_0

    .line 12344
    :pswitch_f
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->w:Ljava/util/List;

    goto :goto_0

    .line 12347
    :pswitch_10
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->x:Ljava/util/List;

    goto :goto_0

    .line 12350
    :pswitch_11
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->y:Ljava/util/List;

    goto :goto_0

    .line 12353
    :pswitch_12
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->z:Ljava/util/List;

    goto :goto_0

    .line 12356
    :pswitch_13
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->A:Ljava/util/List;

    goto :goto_0

    .line 12359
    :pswitch_14
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->B:Ljava/util/List;

    goto :goto_0

    .line 12362
    :pswitch_15
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->C:Ljava/util/List;

    goto :goto_0

    .line 12365
    :pswitch_16
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->E:Ljava/util/List;

    goto :goto_0

    .line 12368
    :pswitch_17
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->F:Ljava/util/List;

    goto :goto_0

    .line 12371
    :pswitch_18
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->G:Ljava/util/List;

    goto :goto_0

    .line 12374
    :pswitch_19
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->H:Ljava/util/List;

    goto :goto_0

    .line 12377
    :pswitch_1a
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->J:Ljava/util/List;

    goto :goto_0

    .line 12380
    :pswitch_1b
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->K:Ljava/util/List;

    goto :goto_0

    .line 12300
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_0
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_0
        :pswitch_1a
        :pswitch_1b
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 12153
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 12158
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 12230
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 12160
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->c:Ljava/util/List;

    .line 12228
    :goto_0
    return-object v0

    .line 12162
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->d:Ljava/util/List;

    goto :goto_0

    .line 12164
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e:Ljava/lang/String;

    goto :goto_0

    .line 12166
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->f:Ljava/util/List;

    goto :goto_0

    .line 12168
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->g:Ljava/util/List;

    goto :goto_0

    .line 12170
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h:Ljava/util/List;

    goto :goto_0

    .line 12172
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->i:Ljava/util/List;

    goto :goto_0

    .line 12174
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->j:Ljava/util/List;

    goto :goto_0

    .line 12176
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->k:Ljava/lang/String;

    goto :goto_0

    .line 12178
    :pswitch_a
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->l:Ljava/util/List;

    goto :goto_0

    .line 12180
    :pswitch_b
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->m:Ljava/util/List;

    goto :goto_0

    .line 12182
    :pswitch_c
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n:Ljava/util/List;

    goto :goto_0

    .line 12184
    :pswitch_d
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->o:Ljava/lang/String;

    goto :goto_0

    .line 12186
    :pswitch_e
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->p:Ljava/util/List;

    goto :goto_0

    .line 12188
    :pswitch_f
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->q:Ljava/util/List;

    goto :goto_0

    .line 12190
    :pswitch_10
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->r:Ljava/lang/String;

    goto :goto_0

    .line 12192
    :pswitch_11
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->s:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;

    goto :goto_0

    .line 12194
    :pswitch_12
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->t:Ljava/util/List;

    goto :goto_0

    .line 12196
    :pswitch_13
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->u:Ljava/util/List;

    goto :goto_0

    .line 12198
    :pswitch_14
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    goto :goto_0

    .line 12200
    :pswitch_15
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->w:Ljava/util/List;

    goto :goto_0

    .line 12202
    :pswitch_16
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->x:Ljava/util/List;

    goto :goto_0

    .line 12204
    :pswitch_17
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->y:Ljava/util/List;

    goto :goto_0

    .line 12206
    :pswitch_18
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->z:Ljava/util/List;

    goto :goto_0

    .line 12208
    :pswitch_19
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->A:Ljava/util/List;

    goto :goto_0

    .line 12210
    :pswitch_1a
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->B:Ljava/util/List;

    goto :goto_0

    .line 12212
    :pswitch_1b
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->C:Ljava/util/List;

    goto :goto_0

    .line 12214
    :pswitch_1c
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->D:Ljava/lang/String;

    goto :goto_0

    .line 12216
    :pswitch_1d
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->E:Ljava/util/List;

    goto :goto_0

    .line 12218
    :pswitch_1e
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->F:Ljava/util/List;

    goto :goto_0

    .line 12220
    :pswitch_1f
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->G:Ljava/util/List;

    goto :goto_0

    .line 12222
    :pswitch_20
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->H:Ljava/util/List;

    goto :goto_0

    .line 12224
    :pswitch_21
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->I:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    goto :goto_0

    .line 12226
    :pswitch_22
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->J:Ljava/util/List;

    goto :goto_0

    .line 12228
    :pswitch_23
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->K:Ljava/util/List;

    goto :goto_0

    .line 12158
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
    .end packed-switch
.end method

.method public final b()Ljava/util/List;
    .locals 1

    .prologue
    .line 707
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->d:Ljava/util/List;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1

    .prologue
    .line 772
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h:Ljava/util/List;

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1

    .prologue
    .line 802
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->j:Ljava/util/List;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 12143
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/an;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 12405
    instance-of v0, p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    if-nez v0, :cond_0

    move v0, v1

    .line 12436
    :goto_0
    return v0

    .line 12410
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 12411
    goto :goto_0

    .line 12414
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    .line 12415
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 12416
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 12417
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 12419
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 12421
    goto :goto_0

    :cond_3
    move v0, v1

    .line 12426
    goto :goto_0

    .line 12429
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 12431
    goto :goto_0

    :cond_5
    move v0, v2

    .line 12436
    goto :goto_0
.end method

.method public final f()Ljava/util/List;
    .locals 1

    .prologue
    .line 859
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n:Ljava/util/List;

    return-object v0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 866
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/Set;

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 875
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 12392
    const/4 v0, 0x0

    .line 12393
    sget-object v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->L:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 12394
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 12395
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 12396
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 12398
    goto :goto_0

    .line 12399
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final l()Ljava/util/List;
    .locals 1

    .prologue
    .line 890
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->p:Ljava/util/List;

    return-object v0
.end method

.method public final m()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;
    .locals 1

    .prologue
    .line 935
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->s:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;

    return-object v0
.end method

.method public final n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;
    .locals 1

    .prologue
    .line 981
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->v:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 988
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->a:Ljava/util/Set;

    const/16 v1, 0x16

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final p()Ljava/util/List;
    .locals 1

    .prologue
    .line 996
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->w:Ljava/util/List;

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 1011
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->x:Ljava/util/List;

    return-object v0
.end method

.method public final r()Ljava/util/List;
    .locals 1

    .prologue
    .line 1072
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->B:Ljava/util/List;

    return-object v0
.end method

.method public final s()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;
    .locals 1

    .prologue
    .line 1177
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->I:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    return-object v0
.end method

.method public final t()Ljava/util/List;
    .locals 1

    .prologue
    .line 1192
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->J:Ljava/util/List;

    return-object v0
.end method

.method public final u()Ljava/util/List;
    .locals 1

    .prologue
    .line 1207
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->K:Ljava/util/List;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 12148
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/an;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/an;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Landroid/os/Parcel;I)V

    .line 12149
    return-void
.end method
