.class final Lcom/google/android/gms/drive/realtime/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/Runnable;

.field final synthetic b:Ljava/util/concurrent/CountDownLatch;

.field final synthetic c:Lcom/google/android/gms/drive/realtime/l;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/realtime/l;Ljava/lang/Runnable;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lcom/google/android/gms/drive/realtime/n;->c:Lcom/google/android/gms/drive/realtime/l;

    iput-object p2, p0, Lcom/google/android/gms/drive/realtime/n;->a:Ljava/lang/Runnable;

    iput-object p3, p0, Lcom/google/android/gms/drive/realtime/n;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 285
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/n;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 289
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/n;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 290
    return-void

    .line 286
    :catch_0
    move-exception v0

    .line 287
    const-string v1, "RealtimeLoader"

    const-string v2, "Error while firing event."

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method
