.class public final Lcom/google/android/gms/location/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:I

.field private final b:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/location/h;->b:Ljava/util/List;

    .line 140
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/gms/location/h;->a:I

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/location/GeofencingRequest;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/location/h;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "No geofence has been added to this request."

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 234
    new-instance v0, Lcom/google/android/gms/location/GeofencingRequest;

    iget-object v2, p0, Lcom/google/android/gms/location/h;->b:Ljava/util/List;

    iget v3, p0, Lcom/google/android/gms/location/h;->a:I

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/gms/location/GeofencingRequest;-><init>(Ljava/util/List;IB)V

    return-object v0

    :cond_0
    move v0, v1

    .line 232
    goto :goto_0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/gms/location/h;
    .locals 4

    .prologue
    .line 187
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 196
    :cond_0
    return-object p0

    .line 191
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/ParcelableGeofence;

    .line 192
    if-eqz v0, :cond_2

    .line 193
    const-string v2, "geofence can\'t be null."

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    instance-of v2, v0, Lcom/google/android/gms/location/internal/ParcelableGeofence;

    const-string v3, "Geofence must be created using Geofence.Builder."

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/gms/location/h;->b:Ljava/util/List;

    check-cast v0, Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
