.class final Lcom/google/android/gms/fitness/sensors/a/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/k/k/a/aq;

.field final synthetic b:Lcom/google/android/gms/fitness/sensors/a/e;


# direct methods
.method constructor <init>(Lcom/google/android/gms/fitness/sensors/a/e;Lcom/google/k/k/a/aq;)V
    .locals 0

    .prologue
    .line 313
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/a/g;->b:Lcom/google/android/gms/fitness/sensors/a/e;

    iput-object p2, p0, Lcom/google/android/gms/fitness/sensors/a/g;->a:Lcom/google/k/k/a/aq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 316
    const-string v0, "Checking if disconnect future is still in progress."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 317
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/g;->b:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/g;->a:Lcom/google/k/k/a/aq;

    iget-object v2, v0, Lcom/google/android/gms/fitness/sensors/a/e;->h:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v3, v0, Lcom/google/android/gms/fitness/sensors/a/e;->i:Lcom/google/k/k/a/aq;

    if-ne v3, v1, :cond_0

    const-string v1, "Disconnect is still in progress. Resolving it to failure"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v3}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v1, v0, Lcom/google/android/gms/fitness/sensors/a/e;->i:Lcom/google/k/k/a/aq;

    new-instance v3, Lcom/google/android/gms/fitness/sensors/a/l;

    const-string v4, "Disconnection timeout %s secs is exceeded"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    sget-object v7, Lcom/google/android/gms/fitness/g/c;->ah:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v7}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lcom/google/android/gms/fitness/sensors/a/l;-><init>(Lcom/google/android/gms/fitness/sensors/a/e;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Lcom/google/k/k/a/aq;->a(Ljava/lang/Throwable;)Z

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/fitness/sensors/a/e;->i:Lcom/google/k/k/a/aq;

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method
