.class public final Lcom/google/android/gms/wallet/service/ow/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/support/v4/g/h;

.field private final b:Landroid/content/Context;

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/service/ow/a;-><init>(Landroid/content/Context;B)V

    .line 50
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/a;->b:Landroid/content/Context;

    .line 54
    const v0, 0x493e0

    iput v0, p0, Lcom/google/android/gms/wallet/service/ow/a;->c:I

    .line 55
    new-instance v0, Landroid/support/v4/g/h;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/support/v4/g/h;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/a;->a:Landroid/support/v4/g/h;

    .line 56
    return-void
.end method

.method private static a(Landroid/content/pm/PackageInfo;)Lcom/google/aa/b/a/a/a/a/b;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 110
    new-instance v3, Lcom/google/aa/b/a/a/a/a/b;

    invoke-direct {v3}, Lcom/google/aa/b/a/a/a/a/b;-><init>()V

    .line 111
    iput v8, v3, Lcom/google/aa/b/a/a/a/a/b;->b:I

    .line 112
    iget v1, p0, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v1, v3, Lcom/google/aa/b/a/a/a/a/b;->d:I

    .line 113
    iget-object v1, p0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 114
    iget-object v1, p0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iput-object v1, v3, Lcom/google/aa/b/a/a/a/a/b;->a:Ljava/lang/String;

    .line 116
    :cond_0
    iget-object v1, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v1, v1

    if-nez v1, :cond_2

    .line 117
    :cond_1
    const-string v0, "AndroidAppIdCache"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "No package certificates found for "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    .line 155
    :goto_0
    return-object v0

    .line 120
    :cond_2
    iget-object v1, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v5, v1

    .line 121
    new-array v1, v5, [Ljava/lang/String;

    iput-object v1, v3, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    move v4, v0

    move v1, v0

    .line 123
    :goto_1
    if-ge v4, v5, :cond_4

    .line 124
    iget-object v0, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    aget-object v0, v0, v4

    const-string v6, "SHA-1"

    invoke-static {v0, v6}, Lcom/google/android/gms/wallet/service/a;->a(Landroid/content/pm/Signature;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 127
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 128
    iget-object v7, v3, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    add-int/lit8 v0, v1, 0x1

    aput-object v6, v7, v1

    .line 123
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto :goto_1

    .line 132
    :cond_3
    const-string v0, "AndroidAppIdCache"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "At least one of the package certificates obtained from PackageManagerfor "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is invalid"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_2

    .line 137
    :cond_4
    iget-object v0, v3, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 138
    iget-object v0, v3, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v3, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    .line 141
    :cond_5
    iget-object v0, v3, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_6

    .line 142
    const-string v0, "AndroidAppIdCache"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "No valid package certificate found for "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    .line 143
    goto :goto_0

    .line 146
    :cond_6
    iget-object v0, p0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    const-string v1, "SHA-1"

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/service/ow/a;->a(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 148
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 149
    const-string v0, "AndroidAppIdCache"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unable to calculate application fingerprint for "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    .line 150
    goto/16 :goto_0

    .line 152
    :cond_7
    iput v8, v3, Lcom/google/aa/b/a/a/a/a/b;->e:I

    .line 153
    iput-object v0, v3, Lcom/google/aa/b/a/a/a/a/b;->f:Ljava/lang/String;

    move-object v0, v3

    .line 155
    goto/16 :goto_0
.end method

.method private static a(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 160
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 161
    invoke-static {p1}, Lcom/google/android/gms/common/util/e;->b(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v4

    .line 162
    if-nez v4, :cond_0

    move-object v0, v1

    .line 198
    :goto_0
    return-object v0

    .line 168
    :cond_0
    :try_start_0
    new-instance v2, Ljava/io/BufferedInputStream;

    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    const/high16 v0, 0x10000

    :try_start_1
    new-array v5, v0, [B

    .line 174
    sget-object v0, Lcom/google/android/gms/wallet/b/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 175
    if-ltz v6, :cond_3

    array-length v0, v5

    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 177
    :cond_1
    :goto_1
    const/4 v7, 0x0

    invoke-virtual {v2, v5, v7, v0}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v7

    if-ltz v7, :cond_2

    .line 178
    const/4 v8, 0x0

    invoke-virtual {v4, v5, v8, v7}, Ljava/security/MessageDigest;->update([BII)V

    .line 179
    if-ltz v6, :cond_1

    .line 180
    add-int/2addr v3, v7

    .line 181
    if-lt v3, v6, :cond_1

    .line 182
    :cond_2
    invoke-virtual {v4}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->a([B)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 190
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    .line 175
    :cond_3
    :try_start_3
    array-length v0, v5
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 187
    :catch_1
    move-exception v0

    move-object v2, v1

    .line 188
    :goto_2
    :try_start_4
    const-string v3, "AndroidAppIdCache"

    const-string v4, "Failed to read 3rd party APK for hashing"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 190
    if-eqz v2, :cond_4

    .line 192
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :cond_4
    :goto_3
    move-object v0, v1

    .line 198
    goto :goto_0

    .line 190
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_4
    if-eqz v2, :cond_5

    .line 192
    :try_start_6
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 195
    :cond_5
    :goto_5
    throw v0

    :catch_2
    move-exception v0

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_5

    .line 190
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 187
    :catch_4
    move-exception v0

    goto :goto_2
.end method

.method private a(Landroid/content/pm/PackageInfo;Lcom/google/android/gms/wallet/service/ow/b;J)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 204
    if-eqz p2, :cond_0

    iget-object v1, p1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v1, :cond_0

    iget-wide v2, p2, Lcom/google/android/gms/wallet/service/ow/b;->a:J

    iget v1, p0, Lcom/google/android/gms/wallet/service/ow/a;->c:I

    int-to-long v4, v1

    sub-long v4, p3, v4

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 232
    :cond_0
    :goto_0
    return v0

    .line 211
    :cond_1
    iget-object v1, p2, Lcom/google/android/gms/wallet/service/ow/b;->c:[Landroid/content/pm/Signature;

    array-length v3, v1

    .line 213
    iget-object v1, p1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v4, v1

    .line 214
    iget-object v1, p1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v1, v1

    if-ne v3, v1, :cond_0

    move v2, v0

    .line 218
    :goto_1
    if-ge v2, v3, :cond_3

    move v1, v0

    .line 220
    :goto_2
    if-ge v1, v4, :cond_2

    .line 221
    iget-object v5, p2, Lcom/google/android/gms/wallet/service/ow/b;->c:[Landroid/content/pm/Signature;

    aget-object v5, v5, v2

    iget-object v6, p1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 223
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 227
    :cond_2
    if-eq v1, v4, :cond_0

    .line 218
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 232
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/aa/b/a/a/a/a/b;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 70
    if-nez p1, :cond_0

    .line 71
    const-string v0, "AndroidAppIdCache"

    const-string v2, "getAndroidAppId called with null packageName"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 100
    :goto_0
    return-object v0

    .line 75
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/a;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/ow/b;

    .line 78
    iget-object v4, p0, Lcom/google/android/gms/wallet/service/ow/a;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 79
    const/16 v5, 0x40

    :try_start_0
    invoke-virtual {v4, p1, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 87
    invoke-direct {p0, v4, v0, v2, v3}, Lcom/google/android/gms/wallet/service/ow/a;->a(Landroid/content/pm/PackageInfo;Lcom/google/android/gms/wallet/service/ow/b;J)Z

    move-result v5

    if-nez v5, :cond_2

    .line 88
    invoke-static {v4}, Lcom/google/android/gms/wallet/service/ow/a;->a(Landroid/content/pm/PackageInfo;)Lcom/google/aa/b/a/a/a/a/b;

    move-result-object v0

    .line 89
    if-nez v0, :cond_1

    .line 90
    const-string v0, "AndroidAppIdCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to construct AndroidAppId for packageName="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 91
    goto :goto_0

    .line 83
    :catch_0
    move-exception v0

    const-string v0, "AndroidAppIdCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to get package info from PackageManager for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 84
    goto :goto_0

    .line 94
    :cond_1
    new-instance v1, Lcom/google/android/gms/wallet/service/ow/b;

    iget-object v4, v4, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/google/android/gms/wallet/service/ow/b;-><init>(JLcom/google/aa/b/a/a/a/a/b;[Landroid/content/pm/Signature;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/a;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    iget-object v0, v1, Lcom/google/android/gms/wallet/service/ow/b;->b:Lcom/google/aa/b/a/a/a/a/b;

    goto :goto_0

    .line 100
    :cond_2
    iget-object v0, v0, Lcom/google/android/gms/wallet/service/ow/b;->b:Lcom/google/aa/b/a/a/a/a/b;

    goto :goto_0
.end method
