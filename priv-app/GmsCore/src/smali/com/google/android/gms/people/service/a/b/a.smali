.class public final Lcom/google/android/gms/people/service/a/b/a;
.super Lcom/google/android/gms/people/service/a/b/c;
.source "SourceFile"


# instance fields
.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 33
    move-object v1, p0

    move-object v2, p1

    move-object v3, p6

    move-object v4, p2

    move v5, p4

    move-object v6, p5

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/people/service/a/b/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/b/a;->i:Ljava/lang/String;

    .line 36
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/b/a;->j:Ljava/lang/String;

    .line 37
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    iput-object p2, p0, Lcom/google/android/gms/people/service/a/b/a;->k:Ljava/lang/String;

    .line 38
    return-void

    :cond_0
    move-object p2, p3

    .line 37
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/people/service/e;Lcom/google/android/gms/people/e/a;)Landroid/util/Pair;
    .locals 7

    .prologue
    const/4 v3, 0x1

    .line 46
    invoke-static {p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/u;->b()V

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/a;->i:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/b/a;->j:Ljava/lang/String;

    invoke-virtual {p2, p3, v0, v1}, Lcom/google/android/gms/people/service/e;->a(Lcom/google/android/gms/people/e/a;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;

    move-result-object v6

    .line 53
    invoke-static {p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/u;->b()V

    .line 55
    if-eqz v6, :cond_0

    .line 56
    iget-object v1, p0, Lcom/google/android/gms/people/service/a/b/a;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/a;->g:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;->e()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/people/service/a/b/a;->h:Ljava/lang/String;

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/people/service/a/b/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 61
    :cond_0
    if-nez v6, :cond_1

    const/4 v0, 0x0

    .line 67
    :goto_0
    if-ne v0, v3, :cond_2

    .line 68
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.people.BROADCAST_CIRCLES_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.permission.INTERNAL_BROADCAST"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 71
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 72
    const-string v0, "circle_id"

    invoke-virtual {v6}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v0, "circle_name"

    invoke-virtual {v6}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    new-instance v0, Landroid/util/Pair;

    sget-object v2, Lcom/google/android/gms/people/service/b;->c:Lcom/google/android/gms/people/service/b;

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 76
    :goto_1
    return-object v0

    .line 61
    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/b/a;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/a;->h:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 76
    :cond_2
    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lcom/google/android/gms/people/service/b;->f:Lcom/google/android/gms/people/service/b;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1
.end method
