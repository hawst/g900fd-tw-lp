.class public final Lcom/google/android/gms/auth/f/b;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:I

.field private c:Z

.field private d:Lcom/google/android/gms/auth/f/h;

.field private e:Z

.field private f:Lcom/google/android/gms/auth/f/i;

.field private g:Z

.field private h:Lcom/google/android/gms/auth/f/d;

.field private i:Z

.field private j:Lcom/google/android/gms/auth/f/k;

.field private k:Z

.field private l:Lcom/google/android/gms/auth/f/c;

.field private m:Z

.field private n:Lcom/google/android/gms/auth/f/e;

.field private o:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2435
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 2449
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/auth/f/b;->b:I

    .line 2466
    iput-object v1, p0, Lcom/google/android/gms/auth/f/b;->d:Lcom/google/android/gms/auth/f/h;

    .line 2486
    iput-object v1, p0, Lcom/google/android/gms/auth/f/b;->f:Lcom/google/android/gms/auth/f/i;

    .line 2506
    iput-object v1, p0, Lcom/google/android/gms/auth/f/b;->h:Lcom/google/android/gms/auth/f/d;

    .line 2526
    iput-object v1, p0, Lcom/google/android/gms/auth/f/b;->j:Lcom/google/android/gms/auth/f/k;

    .line 2546
    iput-object v1, p0, Lcom/google/android/gms/auth/f/b;->l:Lcom/google/android/gms/auth/f/c;

    .line 2566
    iput-object v1, p0, Lcom/google/android/gms/auth/f/b;->n:Lcom/google/android/gms/auth/f/e;

    .line 2625
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/auth/f/b;->o:I

    .line 2435
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2628
    iget v0, p0, Lcom/google/android/gms/auth/f/b;->o:I

    if-gez v0, :cond_0

    .line 2630
    invoke-virtual {p0}, Lcom/google/android/gms/auth/f/b;->b()I

    .line 2632
    :cond_0
    iget v0, p0, Lcom/google/android/gms/auth/f/b;->o:I

    return v0
.end method

.method public final a(I)Lcom/google/android/gms/auth/f/b;
    .locals 1

    .prologue
    .line 2453
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/b;->a:Z

    .line 2454
    iput p1, p0, Lcom/google/android/gms/auth/f/b;->b:I

    .line 2455
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/auth/f/c;)Lcom/google/android/gms/auth/f/b;
    .locals 1

    .prologue
    .line 2550
    if-nez p1, :cond_0

    .line 2551
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2553
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/b;->k:Z

    .line 2554
    iput-object p1, p0, Lcom/google/android/gms/auth/f/b;->l:Lcom/google/android/gms/auth/f/c;

    .line 2555
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/auth/f/d;)Lcom/google/android/gms/auth/f/b;
    .locals 1

    .prologue
    .line 2510
    if-nez p1, :cond_0

    .line 2511
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2513
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/b;->g:Z

    .line 2514
    iput-object p1, p0, Lcom/google/android/gms/auth/f/b;->h:Lcom/google/android/gms/auth/f/d;

    .line 2515
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/auth/f/e;)Lcom/google/android/gms/auth/f/b;
    .locals 1

    .prologue
    .line 2570
    if-nez p1, :cond_0

    .line 2571
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2573
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/b;->m:Z

    .line 2574
    iput-object p1, p0, Lcom/google/android/gms/auth/f/b;->n:Lcom/google/android/gms/auth/f/e;

    .line 2575
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/auth/f/h;)Lcom/google/android/gms/auth/f/b;
    .locals 1

    .prologue
    .line 2470
    if-nez p1, :cond_0

    .line 2471
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2473
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/b;->c:Z

    .line 2474
    iput-object p1, p0, Lcom/google/android/gms/auth/f/b;->d:Lcom/google/android/gms/auth/f/h;

    .line 2475
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/auth/f/i;)Lcom/google/android/gms/auth/f/b;
    .locals 1

    .prologue
    .line 2490
    if-nez p1, :cond_0

    .line 2491
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2493
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/b;->e:Z

    .line 2494
    iput-object p1, p0, Lcom/google/android/gms/auth/f/b;->f:Lcom/google/android/gms/auth/f/i;

    .line 2495
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/auth/f/k;)Lcom/google/android/gms/auth/f/b;
    .locals 1

    .prologue
    .line 2530
    if-nez p1, :cond_0

    .line 2531
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2533
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/b;->i:Z

    .line 2534
    iput-object p1, p0, Lcom/google/android/gms/auth/f/b;->j:Lcom/google/android/gms/auth/f/k;

    .line 2535
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 1

    .prologue
    .line 2432
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/b;->a(I)Lcom/google/android/gms/auth/f/b;

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/android/gms/auth/f/h;

    invoke-direct {v0}, Lcom/google/android/gms/auth/f/h;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/b;->a(Lcom/google/android/gms/auth/f/h;)Lcom/google/android/gms/auth/f/b;

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/android/gms/auth/f/i;

    invoke-direct {v0}, Lcom/google/android/gms/auth/f/i;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/b;->a(Lcom/google/android/gms/auth/f/i;)Lcom/google/android/gms/auth/f/b;

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/android/gms/auth/f/d;

    invoke-direct {v0}, Lcom/google/android/gms/auth/f/d;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/b;->a(Lcom/google/android/gms/auth/f/d;)Lcom/google/android/gms/auth/f/b;

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/android/gms/auth/f/k;

    invoke-direct {v0}, Lcom/google/android/gms/auth/f/k;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/b;->a(Lcom/google/android/gms/auth/f/k;)Lcom/google/android/gms/auth/f/b;

    goto :goto_0

    :sswitch_6
    new-instance v0, Lcom/google/android/gms/auth/f/c;

    invoke-direct {v0}, Lcom/google/android/gms/auth/f/c;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/b;->a(Lcom/google/android/gms/auth/f/c;)Lcom/google/android/gms/auth/f/b;

    goto :goto_0

    :sswitch_7
    new-instance v0, Lcom/google/android/gms/auth/f/e;

    invoke-direct {v0}, Lcom/google/android/gms/auth/f/e;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/b;->a(Lcom/google/android/gms/auth/f/e;)Lcom/google/android/gms/auth/f/b;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 2602
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/b;->a:Z

    if-eqz v0, :cond_0

    .line 2603
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/auth/f/b;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 2605
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/b;->c:Z

    if-eqz v0, :cond_1

    .line 2606
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/auth/f/b;->d:Lcom/google/android/gms/auth/f/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 2608
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/b;->e:Z

    if-eqz v0, :cond_2

    .line 2609
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/auth/f/b;->f:Lcom/google/android/gms/auth/f/i;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 2611
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/b;->g:Z

    if-eqz v0, :cond_3

    .line 2612
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/auth/f/b;->h:Lcom/google/android/gms/auth/f/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 2614
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/b;->i:Z

    if-eqz v0, :cond_4

    .line 2615
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/auth/f/b;->j:Lcom/google/android/gms/auth/f/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 2617
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/b;->k:Z

    if-eqz v0, :cond_5

    .line 2618
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/gms/auth/f/b;->l:Lcom/google/android/gms/auth/f/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 2620
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/b;->m:Z

    if-eqz v0, :cond_6

    .line 2621
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/gms/auth/f/b;->n:Lcom/google/android/gms/auth/f/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 2623
    :cond_6
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 2637
    const/4 v0, 0x0

    .line 2638
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/b;->a:Z

    if-eqz v1, :cond_0

    .line 2639
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/auth/f/b;->b:I

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2642
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/b;->c:Z

    if-eqz v1, :cond_1

    .line 2643
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/auth/f/b;->d:Lcom/google/android/gms/auth/f/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2646
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/b;->e:Z

    if-eqz v1, :cond_2

    .line 2647
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/auth/f/b;->f:Lcom/google/android/gms/auth/f/i;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2650
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/b;->g:Z

    if-eqz v1, :cond_3

    .line 2651
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/auth/f/b;->h:Lcom/google/android/gms/auth/f/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2654
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/b;->i:Z

    if-eqz v1, :cond_4

    .line 2655
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/auth/f/b;->j:Lcom/google/android/gms/auth/f/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2658
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/b;->k:Z

    if-eqz v1, :cond_5

    .line 2659
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/auth/f/b;->l:Lcom/google/android/gms/auth/f/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2662
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/b;->m:Z

    if-eqz v1, :cond_6

    .line 2663
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/auth/f/b;->n:Lcom/google/android/gms/auth/f/e;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2666
    :cond_6
    iput v0, p0, Lcom/google/android/gms/auth/f/b;->o:I

    .line 2667
    return v0
.end method
