.class public Lcom/google/android/gms/audiomodem/aq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected d:Landroid/os/IInterface;


# direct methods
.method public constructor <init>(Landroid/os/IInterface;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/aq;->d:Landroid/os/IInterface;

    .line 19
    return-void
.end method


# virtual methods
.method public final c()Landroid/os/IInterface;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aq;->d:Landroid/os/IInterface;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 26
    if-eqz p1, :cond_0

    instance-of v2, p1, Lcom/google/android/gms/audiomodem/aq;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 34
    :cond_1
    :goto_0
    return v0

    .line 31
    :cond_2
    :try_start_0
    check-cast p1, Lcom/google/android/gms/audiomodem/aq;

    .line 32
    iget-object v2, p0, Lcom/google/android/gms/audiomodem/aq;->d:Landroid/os/IInterface;

    iget-object v3, p1, Lcom/google/android/gms/audiomodem/aq;->d:Landroid/os/IInterface;

    if-eq v2, v3, :cond_1

    if-eqz v2, :cond_3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Landroid/os/IInterface;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-interface {v3}, Landroid/os/IInterface;->asBinder()Landroid/os/IBinder;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    if-eq v2, v3, :cond_1

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 34
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 48
    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aq;->d:Landroid/os/IInterface;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    aput-object v0, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aq;->d:Landroid/os/IInterface;

    invoke-interface {v0}, Landroid/os/IInterface;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto :goto_0
.end method
