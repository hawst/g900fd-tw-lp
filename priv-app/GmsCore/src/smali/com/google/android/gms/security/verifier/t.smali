.class public final Lcom/google/android/gms/security/verifier/t;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/android/gms/security/verifier/t;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;

.field public c:[B

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 196
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 197
    iput-object v0, p0, Lcom/google/android/gms/security/verifier/t;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/t;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/t;->c:[B

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/t;->d:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/security/verifier/t;->cachedSize:I

    .line 198
    return-void
.end method

.method public static a()[Lcom/google/android/gms/security/verifier/t;
    .locals 2

    .prologue
    .line 173
    sget-object v0, Lcom/google/android/gms/security/verifier/t;->e:[Lcom/google/android/gms/security/verifier/t;

    if-nez v0, :cond_1

    .line 174
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 176
    :try_start_0
    sget-object v0, Lcom/google/android/gms/security/verifier/t;->e:[Lcom/google/android/gms/security/verifier/t;

    if-nez v0, :cond_0

    .line 177
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/security/verifier/t;

    sput-object v0, Lcom/google/android/gms/security/verifier/t;->e:[Lcom/google/android/gms/security/verifier/t;

    .line 179
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    :cond_1
    sget-object v0, Lcom/google/android/gms/security/verifier/t;->e:[Lcom/google/android/gms/security/verifier/t;

    return-object v0

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 272
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 273
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/t;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 275
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/t;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 277
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/t;->c:[B

    if-eqz v1, :cond_0

    .line 278
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/t;->c:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 281
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/t;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 282
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/t;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 285
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 211
    if-ne p1, p0, :cond_1

    .line 241
    :cond_0
    :goto_0
    return v0

    .line 214
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/security/verifier/t;

    if-nez v2, :cond_2

    move v0, v1

    .line 215
    goto :goto_0

    .line 217
    :cond_2
    check-cast p1, Lcom/google/android/gms/security/verifier/t;

    .line 218
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/t;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 219
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/t;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 220
    goto :goto_0

    .line 222
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/t;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/t;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 223
    goto :goto_0

    .line 225
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/t;->b:Ljava/lang/Integer;

    if-nez v2, :cond_5

    .line 226
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/t;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    move v0, v1

    .line 227
    goto :goto_0

    .line 229
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/t;->b:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/t;->b:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 230
    goto :goto_0

    .line 231
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/t;->c:[B

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/t;->c:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 232
    goto :goto_0

    .line 234
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/t;->d:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 235
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/t;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 236
    goto :goto_0

    .line 238
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/t;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/t;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 239
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 246
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/t;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 249
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/t;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 250
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/t;->c:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    .line 251
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/t;->d:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 253
    return v0

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/t;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 249
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/t;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1

    .line 251
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/t;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 167
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/t;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/t;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/t;->c:[B

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/t;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 259
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/t;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 260
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/t;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 261
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/t;->c:[B

    if-eqz v0, :cond_0

    .line 262
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/t;->c:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 264
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/t;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 265
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/t;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 267
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 268
    return-void
.end method
