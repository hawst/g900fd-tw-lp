.class final Lcom/google/android/gms/games/ui/widget/finsky/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/google/android/gms/games/ui/widget/finsky/b;->a:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/b;->a:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/finsky/b;->a:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a(Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a(Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;I)V

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/b;->a:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->b(Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;)Z

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/finsky/b;->a:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->c(Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;)Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabStrip;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 177
    return-void
.end method
