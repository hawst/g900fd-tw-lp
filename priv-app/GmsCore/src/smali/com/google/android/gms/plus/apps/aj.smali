.class public final Lcom/google/android/gms/plus/apps/aj;
.super Landroid/support/v4/app/m;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private j:Lcom/google/android/gms/plus/apps/ak;

.field private k:Landroid/accounts/Account;

.field private l:Lcom/google/android/gms/plus/model/a/a;

.field private m:Z

.field private n:Lcom/google/android/gms/plus/apps/b;

.field private o:Ljava/lang/String;

.field private p:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    .line 58
    return-void
.end method

.method public static a(Landroid/accounts/Account;Lcom/google/android/gms/plus/model/a/a;Ljava/lang/String;Z)Lcom/google/android/gms/plus/apps/aj;
    .locals 3

    .prologue
    .line 70
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 71
    const-string v1, "account"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 72
    const-string v1, "application"

    invoke-static {p1}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a(Lcom/google/android/gms/plus/model/a/a;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 73
    const-string v1, "calling_package_name"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v1, "signed_up"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 75
    new-instance v1, Lcom/google/android/gms/plus/apps/aj;

    invoke-direct {v1}, Lcom/google/android/gms/plus/apps/aj;-><init>()V

    .line 76
    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/aj;->setArguments(Landroid/os/Bundle;)V

    .line 77
    return-object v1
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 101
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/aj;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 102
    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 103
    sget v0, Lcom/google/android/gms/p;->ts:I

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 104
    sget v0, Lcom/google/android/gms/p;->tr:I

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 105
    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    .line 107
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/aj;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/aj;->l:Lcom/google/android/gms/plus/model/a/a;

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/a/a;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    sget v0, Lcom/google/android/gms/p;->tt:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/aj;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/aj;->n:Lcom/google/android/gms/plus/apps/b;

    iget-object v2, v0, Lcom/google/android/gms/plus/apps/b;->a:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/aj;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v3, Lcom/google/android/gms/l;->dJ:I

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    sget v0, Lcom/google/android/gms/j;->mb:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v4, Lcom/google/android/gms/p;->tq:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    aput-object v2, v5, v7

    invoke-virtual {p0, v4, v5}, Lcom/google/android/gms/plus/apps/aj;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v0, Lcom/google/android/gms/j;->cE:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    sget v4, Lcom/google/android/gms/p;->tp:I

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/google/android/gms/plus/apps/aj;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v2, p0, Lcom/google/android/gms/plus/apps/aj;->m:Z

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 113
    :goto_0
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 111
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/aj;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/google/android/gms/l;->dK:I

    invoke-virtual {v0, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    sget v0, Lcom/google/android/gms/j;->mb:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v3, Lcom/google/android/gms/p;->tv:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/apps/aj;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 151
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onAttach(Landroid/app/Activity;)V

    .line 152
    instance-of v0, p1, Lcom/google/android/gms/plus/apps/ak;

    if-eqz v0, :cond_0

    .line 153
    check-cast p1, Lcom/google/android/gms/plus/apps/ak;

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/aj;->j:Lcom/google/android/gms/plus/apps/ak;

    return-void

    .line 155
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DisconnectSourceDialog has to be hosted by an Activity that implements OnDisconnectSourceAcceptedListener."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 6

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/aj;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/aj;->k:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/gms/common/analytics/c;->u:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/common/analytics/d;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/apps/aj;->o:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 197
    return-void
.end method

.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 144
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->cE:I

    if-ne v0, v1, :cond_0

    .line 145
    iput-boolean p2, p0, Lcom/google/android/gms/plus/apps/aj;->m:Z

    .line 147
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 168
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/aj;->j:Lcom/google/android/gms/plus/apps/ak;

    if-nez v0, :cond_0

    .line 190
    :goto_0
    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/aj;->j:Lcom/google/android/gms/plus/apps/ak;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/aj;->k:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/aj;->l:Lcom/google/android/gms/plus/model/a/a;

    iget-boolean v3, p0, Lcom/google/android/gms/plus/apps/aj;->m:Z

    invoke-interface {v0, v1, v3}, Lcom/google/android/gms/plus/apps/ak;->a(Lcom/google/android/gms/plus/model/a/a;Z)V

    .line 175
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/aj;->m:Z

    if-eqz v0, :cond_1

    .line 176
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/aj;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/aj;->k:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/common/analytics/c;->t:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/common/analytics/d;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/apps/aj;->o:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 189
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/aj;->dismiss()V

    goto :goto_0

    .line 180
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/aj;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/aj;->k:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/common/analytics/c;->s:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/common/analytics/d;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/apps/aj;->o:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_1

    .line 185
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/aj;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/aj;->k:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/common/analytics/c;->u:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/common/analytics/d;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/apps/aj;->o:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onCreate(Landroid/os/Bundle;)V

    .line 83
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/aj;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 84
    const-string v0, "account"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/aj;->k:Landroid/accounts/Account;

    .line 85
    const-string v0, "application"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/model/a/a;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/aj;->l:Lcom/google/android/gms/plus/model/a/a;

    .line 86
    if-eqz p1, :cond_0

    const-string v0, "delete_all_frames"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/aj;->m:Z

    .line 88
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/aj;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/a;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/a;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/aj;->l:Lcom/google/android/gms/plus/model/a/a;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/apps/a;->a(Lcom/google/android/gms/plus/model/a/a;)Lcom/google/android/gms/plus/apps/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/aj;->n:Lcom/google/android/gms/plus/apps/b;

    .line 89
    const-string v0, "calling_package_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/aj;->o:Ljava/lang/String;

    .line 90
    const-string v0, "signed_up"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/aj;->p:Z

    .line 91
    return-void

    .line 86
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDetach()V
    .locals 1

    .prologue
    .line 162
    invoke-super {p0}, Landroid/support/v4/app/m;->onDetach()V

    .line 163
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/aj;->j:Lcom/google/android/gms/plus/apps/ak;

    .line 164
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 95
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 96
    const-string v0, "delete_all_frames"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/apps/aj;->m:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 97
    return-void
.end method
