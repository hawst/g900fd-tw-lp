.class public Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/social/location/am;
.implements Lcom/google/android/gms/social/location/b;
.implements Lcom/google/android/gms/social/location/h;
.implements Lcom/google/android/gms/social/location/q;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

.field private c:Lcom/google/android/gms/social/location/DeviceLocationSettings;

.field private d:Z

.field private e:Lcom/google/android/gms/social/location/SwitchBar;

.field private f:Landroid/support/v7/widget/RecyclerView;

.field private g:Lcom/google/android/gms/social/location/aa;

.field private h:Lcom/google/android/gms/social/location/x;

.field private i:Lcom/google/android/gms/social/location/z;

.field private j:Lcom/google/android/gms/social/location/y;

.field private k:Lcom/google/android/gms/social/location/TimeShare;

.field private l:Z

.field private m:Lcom/google/android/gms/common/internal/a/b;

.field private n:Lcom/google/android/gms/social/location/j;

.field private o:Lcom/google/android/gms/social/location/n;

.field private p:Lcom/google/android/gms/social/location/ai;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 621
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;Lcom/google/android/gms/social/location/DeviceLocationSettings;)Lcom/google/android/gms/social/location/DeviceLocationSettings;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->e()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;I)V
    .locals 3

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "req_pending"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/m;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/social/location/ag;->a(Ljava/lang/String;)Lcom/google/android/gms/social/location/ag;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/social/location/ag;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;Z)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->a(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 363
    invoke-static {p1, p2}, Lcom/google/android/gms/social/location/e;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/social/location/e;

    move-result-object v0

    .line 365
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "help_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/social/location/e;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 366
    return-void
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 407
    new-instance v0, Lcom/google/android/gms/social/location/DeviceLocationSettings;

    iget-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v1}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->c()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v2}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v3}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->e()Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/gms/social/location/DeviceLocationSettings;-><init>(ZLcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/Audience;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    .line 412
    new-instance v0, Lcom/google/android/gms/social/location/z;

    invoke-direct {v0, p0, v4}, Lcom/google/android/gms/social/location/z;-><init>(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;B)V

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->i:Lcom/google/android/gms/social/location/z;

    .line 413
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->i:Lcom/google/android/gms/social/location/z;

    new-array v1, v4, [Lcom/google/android/gms/social/location/af;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/social/location/z;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 414
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;Z)Z
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->d:Z

    return p1
.end method

.method static synthetic c(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Lcom/google/android/gms/social/location/DeviceLocationSettings;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    return-object v0
.end method

.method private c(Lcom/google/android/gms/social/location/af;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 402
    new-instance v0, Lcom/google/android/gms/social/location/z;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/social/location/z;-><init>(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;B)V

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->i:Lcom/google/android/gms/social/location/z;

    .line 403
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->i:Lcom/google/android/gms/social/location/z;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/social/location/af;

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/social/location/z;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 404
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 518
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/m;

    .line 520
    if-eqz v0, :cond_0

    .line 521
    invoke-virtual {v0}, Landroid/support/v4/app/m;->dismiss()V

    .line 523
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->c:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->f()V

    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Lcom/google/android/gms/social/location/DeviceLocationSettings;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->c:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    return-object v0
.end method

.method private e()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/16 v6, 0x8

    const/4 v2, 0x0

    .line 179
    const v0, 0x1020004

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 180
    sget v0, Lcom/google/android/gms/j;->lr:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 181
    sget v0, Lcom/google/android/gms/j;->mL:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 182
    iget-boolean v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->d:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->n:Lcom/google/android/gms/social/location/j;

    invoke-virtual {v0}, Lcom/google/android/gms/social/location/j;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->n:Lcom/google/android/gms/social/location/j;

    iget v0, v0, Lcom/google/android/gms/social/location/j;->f:I

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_5

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->e:Lcom/google/android/gms/social/location/SwitchBar;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/social/location/SwitchBar;->setVisibility(I)V

    .line 185
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 186
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->e:Lcom/google/android/gms/social/location/SwitchBar;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/social/location/SwitchBar;->a(Lcom/google/android/gms/social/location/ai;)V

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->e:Lcom/google/android/gms/social/location/SwitchBar;

    iget-object v3, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v3}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->b()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/social/location/SwitchBar;->setChecked(Z)V

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->e:Lcom/google/android/gms/social/location/SwitchBar;

    iget-object v3, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->p:Lcom/google/android/gms/social/location/ai;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/social/location/SwitchBar;->a(Lcom/google/android/gms/social/location/ai;)V

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->g:Lcom/google/android/gms/social/location/aa;

    iget-object v3, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    iput-object v3, v0, Lcom/google/android/gms/social/location/aa;->e:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    iget-object v3, v0, Lcom/google/android/gms/social/location/aa;->e:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/google/android/gms/social/location/aa;->e:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v3}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->e()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v0, Lcom/google/android/gms/social/location/aa;->e:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v3}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->e()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x2

    if-gt v3, v4, :cond_1

    iput-boolean v1, v0, Lcom/google/android/gms/social/location/aa;->f:Z

    :cond_1
    iget-object v0, v0, Landroid/support/v7/widget/bv;->a:Landroid/support/v7/widget/bw;

    invoke-virtual {v0}, Landroid/support/v7/widget/bw;->a()V

    .line 196
    invoke-direct {p0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->f()V

    .line 198
    iget-boolean v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->l:Z

    if-eqz v0, :cond_2

    .line 199
    iput-boolean v2, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->l:Z

    .line 200
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->n:Lcom/google/android/gms/social/location/j;

    invoke-virtual {v0}, Lcom/google/android/gms/social/location/j;->a()V

    .line 227
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v2

    .line 182
    goto :goto_0

    .line 205
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "start_wizard"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 206
    if-eqz v0, :cond_2

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->e:Lcom/google/android/gms/social/location/SwitchBar;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/social/location/SwitchBar;->setChecked(Z)V

    goto :goto_1

    .line 212
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->e:Lcom/google/android/gms/social/location/SwitchBar;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/social/location/SwitchBar;->setVisibility(I)V

    .line 213
    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 214
    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 215
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 216
    sget v0, Lcom/google/android/gms/j;->li:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 217
    sget v1, Lcom/google/android/gms/j;->lj:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 218
    iget-boolean v3, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->d:Z

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->h:Lcom/google/android/gms/social/location/x;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->h:Lcom/google/android/gms/social/location/x;

    invoke-virtual {v3}, Lcom/google/android/gms/social/location/x;->a()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 220
    :cond_6
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 221
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 223
    :cond_7
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 224
    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private f()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 417
    sget v0, Lcom/google/android/gms/j;->mL:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 418
    sget v1, Lcom/google/android/gms/j;->lr:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 419
    iget-object v2, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v2}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->b()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 420
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 421
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 426
    :goto_0
    return-void

    .line 423
    :cond_0
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 424
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)V
    .locals 4

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->n:Lcom/google/android/gms/social/location/j;

    invoke-virtual {v0}, Lcom/google/android/gms/social/location/j;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/p;->fy:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    sget v1, Lcom/google/android/gms/p;->fA:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->fz:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x104000a

    invoke-virtual {p0, v3}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/gms/social/location/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/social/location/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/social/location/a;->a(Lcom/google/android/gms/social/location/b;)V

    invoke-virtual {p0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "reporting_reminder"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/social/location/a;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic g(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Lcom/google/android/gms/social/location/j;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->n:Lcom/google/android/gms/social/location/j;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Lcom/google/android/gms/social/location/aa;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->g:Lcom/google/android/gms/social/location/aa;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Lcom/google/android/gms/social/location/TimeShare;
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->k:Lcom/google/android/gms/social/location/TimeShare;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->n:Lcom/google/android/gms/social/location/j;

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->n:Lcom/google/android/gms/social/location/j;

    invoke-virtual {v0}, Lcom/google/android/gms/social/location/j;->d()V

    .line 455
    :cond_0
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 459
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 460
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 461
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 462
    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 463
    return-void
.end method

.method public final a(Lcom/google/android/gms/social/location/TimeShare;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 467
    iput-object p1, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->k:Lcom/google/android/gms/social/location/TimeShare;

    .line 468
    new-instance v0, Lcom/google/android/gms/social/location/y;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/social/location/y;-><init>(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;B)V

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->j:Lcom/google/android/gms/social/location/y;

    .line 469
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->j:Lcom/google/android/gms/social/location/y;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/social/location/TimeShare;

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/social/location/y;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 470
    return-void
.end method

.method public final a(Lcom/google/android/gms/social/location/af;)V
    .locals 4

    .prologue
    .line 330
    const/4 v0, 0x0

    .line 331
    invoke-static {}, Lcom/google/android/gms/common/audience/a/a;->a()Lcom/google/android/gms/common/audience/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/gms/common/audience/a/b;->a(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/b;

    move-result-object v1

    .line 332
    sget-object v2, Lcom/google/android/gms/social/location/w;->a:[I

    invoke-virtual {p1}, Lcom/google/android/gms/social/location/af;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 344
    :goto_0
    invoke-interface {v1}, Lcom/google/android/gms/common/audience/a/b;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 345
    return-void

    .line 334
    :pswitch_0
    const/4 v0, 0x1

    .line 335
    invoke-interface {v1}, Lcom/google/android/gms/common/audience/a/b;->b()Lcom/google/android/gms/common/audience/a/b;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v3}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->c()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/gms/common/audience/a/b;->a(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/android/gms/common/audience/a/b;

    goto :goto_0

    .line 339
    :pswitch_1
    const/4 v0, 0x2

    .line 340
    invoke-interface {v1}, Lcom/google/android/gms/common/audience/a/b;->b()Lcom/google/android/gms/common/audience/a/b;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v3}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/gms/common/audience/a/b;->a(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/android/gms/common/audience/a/b;

    goto :goto_0

    .line 332
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 528
    return-void
.end method

.method public final b(Lcom/google/android/gms/social/location/af;)V
    .locals 2

    .prologue
    .line 349
    sget-object v0, Lcom/google/android/gms/social/location/w;->a:[I

    invoke-virtual {p1}, Lcom/google/android/gms/social/location/af;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 360
    :goto_0
    return-void

    .line 351
    :pswitch_0
    sget v0, Lcom/google/android/gms/p;->qQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->qP:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 356
    :pswitch_1
    sget v0, Lcom/google/android/gms/p;->qM:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->qL:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 349
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 532
    const-string v0, "reporting_reminder"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 533
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->n:Lcom/google/android/gms/social/location/j;

    invoke-virtual {v0}, Lcom/google/android/gms/social/location/j;->d()V

    .line 535
    :cond_0
    return-void
.end method

.method public final i_()Z
    .locals 1

    .prologue
    .line 446
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->finish()V

    .line 447
    const/4 v0, 0x1

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 370
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/d;->onActivityResult(IILandroid/content/Intent;)V

    .line 371
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-nez p3, :cond_1

    .line 399
    :cond_0
    :goto_0
    return-void

    .line 374
    :cond_1
    invoke-static {p3}, Lcom/google/android/gms/common/audience/a/a;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/audience/a/c;

    move-result-object v0

    .line 375
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 377
    :pswitch_0
    new-instance v1, Lcom/google/android/gms/social/location/DeviceLocationSettings;

    iget-object v2, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v2}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->b()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0}, Lcom/google/android/gms/common/audience/a/c;->c()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v3}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v4}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->e()Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/google/android/gms/social/location/DeviceLocationSettings;-><init>(ZLjava/util/List;Ljava/util/List;Ljava/util/List;)V

    iput-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    .line 383
    sget-object v0, Lcom/google/android/gms/social/location/af;->a:Lcom/google/android/gms/social/location/af;

    invoke-direct {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->c(Lcom/google/android/gms/social/location/af;)V

    goto :goto_0

    .line 388
    :pswitch_1
    new-instance v1, Lcom/google/android/gms/social/location/DeviceLocationSettings;

    iget-object v2, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v2}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->b()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v3}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->c()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v0}, Lcom/google/android/gms/common/audience/a/c;->c()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v4}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->e()Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/google/android/gms/social/location/DeviceLocationSettings;-><init>(ZLjava/util/List;Ljava/util/List;Ljava/util/List;)V

    iput-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    .line 395
    sget-object v0, Lcom/google/android/gms/social/location/af;->b:Lcom/google/android/gms/social/location/af;

    invoke-direct {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->c(Lcom/google/android/gms/social/location/af;)V

    goto :goto_0

    .line 375
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 94
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 95
    sget v0, Lcom/google/android/gms/l;->cC:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->setContentView(I)V

    .line 96
    new-instance v0, Lcom/google/android/gms/social/location/s;

    invoke-direct {v0, p0}, Lcom/google/android/gms/social/location/s;-><init>(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->o:Lcom/google/android/gms/social/location/n;

    .line 102
    new-instance v0, Lcom/google/android/gms/social/location/t;

    invoke-direct {v0, p0}, Lcom/google/android/gms/social/location/t;-><init>(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->p:Lcom/google/android/gms/social/location/ai;

    .line 109
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->a:Ljava/lang/String;

    .line 111
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/social/location/r;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/social/location/r;

    move-result-object v0

    .line 113
    iget-object v1, v0, Lcom/google/android/gms/social/location/r;->a:Lcom/google/android/gms/common/internal/a/b;

    iput-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->m:Lcom/google/android/gms/common/internal/a/b;

    .line 114
    iget-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->m:Lcom/google/android/gms/common/internal/a/b;

    if-nez v1, :cond_0

    .line 115
    new-instance v1, Lcom/google/android/gms/common/internal/a/b;

    const/16 v2, 0x14

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/internal/a/b;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->m:Lcom/google/android/gms/common/internal/a/b;

    .line 117
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->m:Lcom/google/android/gms/common/internal/a/b;

    iput-object v1, v0, Lcom/google/android/gms/social/location/r;->a:Lcom/google/android/gms/common/internal/a/b;

    .line 119
    new-instance v0, Lcom/google/android/gms/social/location/j;

    iget-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->o:Lcom/google/android/gms/social/location/n;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/gms/social/location/j;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/support/v4/app/v;Lcom/google/android/gms/social/location/n;)V

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->n:Lcom/google/android/gms/social/location/j;

    .line 122
    if-eqz p1, :cond_2

    .line 123
    const-string v0, "settings_loaded"

    invoke-virtual {p1, v0, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->d:Z

    .line 124
    iget-boolean v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->d:Z

    if-eqz v0, :cond_1

    .line 125
    const-string v0, "location_settings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/social/location/DeviceLocationSettings;

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    .line 126
    const-string v0, "last_saved_location_settings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/social/location/DeviceLocationSettings;

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->c:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    .line 133
    :cond_1
    :goto_0
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/support/v7/app/a;->a(Z)V

    invoke-virtual {v0, v7}, Landroid/support/v7/app/a;->b(Z)V

    iget-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    sget v0, Lcom/google/android/gms/j;->sn:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/social/location/SwitchBar;

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->e:Lcom/google/android/gms/social/location/SwitchBar;

    sget v0, Lcom/google/android/gms/j;->lr:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->f:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>()V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/ce;)V

    new-instance v0, Lcom/google/android/gms/social/location/aa;

    iget-object v4, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->m:Lcom/google/android/gms/common/internal/a/b;

    invoke-virtual {p0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v5

    move-object v1, p0

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/social/location/aa;-><init>(Landroid/content/Context;Lcom/google/android/gms/social/location/q;Lcom/google/android/gms/social/location/am;Lcom/google/android/gms/common/internal/a/b;Landroid/support/v4/app/v;)V

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->g:Lcom/google/android/gms/social/location/aa;

    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->g:Lcom/google/android/gms/social/location/aa;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/bv;)V

    sget v0, Lcom/google/android/gms/j;->mL:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v1, Lcom/google/android/gms/p;->zi:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/gms/social/location/o;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->pK:I

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    sget v2, Lcom/google/android/gms/p;->qN:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v4, v5, v6

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v7

    invoke-virtual {v3, v2, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Spanned;->length()I

    move-result v1

    const-class v2, Landroid/text/style/URLSpan;

    invoke-interface {v5, v6, v1, v2}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/style/URLSpan;

    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-direct {v7, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->clearSpans()V

    array-length v8, v1

    move v2, v6

    :goto_1
    if-ge v2, v8, :cond_3

    aget-object v9, v1, v2

    invoke-virtual {v9}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v5, v9}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v11

    invoke-interface {v5, v9}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v9

    new-instance v12, Lcom/google/android/gms/social/location/u;

    invoke-direct {v12, p0, v10, v4}, Lcom/google/android/gms/social/location/u;-><init>(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v10, 0x21

    invoke-virtual {v7, v12, v11, v9, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 130
    :cond_2
    iput-boolean v7, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->l:Z

    goto/16 :goto_0

    .line 133
    :cond_3
    sget v1, Lcom/google/android/gms/f;->k:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLinkTextColor(I)V

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-direct {p0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->e()V

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    if-nez v0, :cond_5

    .line 136
    iput-boolean v6, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->d:Z

    .line 137
    new-instance v0, Lcom/google/android/gms/social/location/x;

    invoke-direct {v0, p0, v6}, Lcom/google/android/gms/social/location/x;-><init>(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;B)V

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->h:Lcom/google/android/gms/social/location/x;

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->h:Lcom/google/android/gms/social/location/x;

    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/social/location/x;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 152
    :cond_4
    :goto_2
    return-void

    .line 139
    :cond_5
    const-string v0, "settings_pending"

    invoke-virtual {p1, v0, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 142
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->a(Z)V

    goto :goto_2

    .line 144
    :cond_6
    const-string v0, "time_share_removal"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/social/location/TimeShare;

    .line 146
    if-eqz v0, :cond_4

    .line 149
    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->a(Lcom/google/android/gms/social/location/TimeShare;)V

    goto :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 311
    sget v0, Lcom/google/android/gms/p;->qp:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 312
    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/support/v4/view/ai;->a(Landroid/view/MenuItem;I)V

    .line 313
    sget v1, Lcom/google/android/gms/h;->bK:I

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 314
    new-instance v1, Lcom/google/android/gms/social/location/v;

    invoke-direct {v1, p0}, Lcom/google/android/gms/social/location/v;-><init>(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 325
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 278
    invoke-super {p0}, Landroid/support/v7/app/d;->onPause()V

    .line 279
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->n:Lcom/google/android/gms/social/location/j;

    iget-boolean v1, v0, Lcom/google/android/gms/social/location/j;->g:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/gms/social/location/j;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/social/location/j;->g:Z

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/social/location/j;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 280
    const-string v0, "req_pending"

    invoke-direct {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->c(Ljava/lang/String;)V

    .line 281
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 272
    invoke-super {p0}, Landroid/support/v7/app/d;->onResume()V

    .line 273
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->n:Lcom/google/android/gms/social/location/j;

    const-string v1, "enable_location_reporting_auto"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/social/location/j;->c(Ljava/lang/String;)V

    const-string v1, "enable_location_reporting_error"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/social/location/j;->c(Ljava/lang/String;)V

    const-string v1, "enable_location_reporting_manual "

    invoke-virtual {v0, v1}, Lcom/google/android/gms/social/location/j;->c(Ljava/lang/String;)V

    const-string v1, "enable_location_reporting_manual_multi_account"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/social/location/j;->c(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/social/location/j;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 274
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 285
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 286
    const-string v0, "location_settings"

    iget-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 287
    const-string v0, "last_saved_location_settings"

    iget-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->c:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 288
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->h:Lcom/google/android/gms/social/location/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->h:Lcom/google/android/gms/social/location/x;

    invoke-virtual {v0}, Lcom/google/android/gms/social/location/x;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    .line 289
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->h:Lcom/google/android/gms/social/location/x;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/social/location/x;->cancel(Z)Z

    .line 291
    :cond_0
    const-string v0, "settings_loaded"

    iget-boolean v1, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 293
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->i:Lcom/google/android/gms/social/location/z;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->i:Lcom/google/android/gms/social/location/z;

    invoke-virtual {v0}, Lcom/google/android/gms/social/location/z;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_1

    .line 297
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->i:Lcom/google/android/gms/social/location/z;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/social/location/z;->cancel(Z)Z

    .line 298
    const-string v0, "settings_pending"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 301
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->j:Lcom/google/android/gms/social/location/y;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->j:Lcom/google/android/gms/social/location/y;

    invoke-virtual {v0}, Lcom/google/android/gms/social/location/y;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_2

    .line 304
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->j:Lcom/google/android/gms/social/location/y;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/social/location/y;->cancel(Z)Z

    .line 305
    const-string v0, "time_share_removal"

    iget-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->k:Lcom/google/android/gms/social/location/TimeShare;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 307
    :cond_2
    return-void
.end method
