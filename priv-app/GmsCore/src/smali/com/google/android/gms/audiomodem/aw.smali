.class public final Lcom/google/android/gms/audiomodem/aw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Landroid/os/Handler;

.field c:Lcom/google/android/gms/audiomodem/ba;

.field d:Z

.field e:Z

.field final f:Landroid/content/IntentFilter;

.field final g:Landroid/content/BroadcastReceiver;

.field h:Z

.field private final i:Landroid/media/AudioManager;

.field private final j:Lcom/google/android/gms/audiomodem/ad;

.field private k:Landroid/telephony/TelephonyManager;

.field private final l:Landroid/telephony/PhoneStateListener;

.field private final m:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Lcom/google/android/gms/audiomodem/ax;

    invoke-direct {v0, p0}, Lcom/google/android/gms/audiomodem/ax;-><init>(Lcom/google/android/gms/audiomodem/aw;)V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/aw;->g:Landroid/content/BroadcastReceiver;

    .line 68
    new-instance v0, Lcom/google/android/gms/audiomodem/ay;

    invoke-direct {v0, p0}, Lcom/google/android/gms/audiomodem/ay;-><init>(Lcom/google/android/gms/audiomodem/aw;)V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/aw;->l:Landroid/telephony/PhoneStateListener;

    .line 85
    new-instance v0, Lcom/google/android/gms/audiomodem/az;

    invoke-direct {v0, p0}, Lcom/google/android/gms/audiomodem/az;-><init>(Lcom/google/android/gms/audiomodem/aw;)V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/aw;->m:Landroid/content/BroadcastReceiver;

    .line 103
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/aw;->a:Landroid/content/Context;

    .line 104
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/aw;->b:Landroid/os/Handler;

    .line 106
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/aw;->i:Landroid/media/AudioManager;

    .line 108
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/aw;->f:Landroid/content/IntentFilter;

    .line 109
    const/16 v0, 0xe

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "android.media.ACTION_SCO_AUDIO_STATE_UPDATED"

    .line 112
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/audiomodem/aw;->f:Landroid/content/IntentFilter;

    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 114
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/aw;->k:Landroid/telephony/TelephonyManager;

    .line 119
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.NEW_OUTGOING_CALL"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 120
    iget-object v1, p0, Lcom/google/android/gms/audiomodem/aw;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/aw;->m:Landroid/content/BroadcastReceiver;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/audiomodem/aw;->b:Landroid/os/Handler;

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aw;->k:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/google/android/gms/audiomodem/aw;->l:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 123
    new-instance v0, Lcom/google/android/gms/audiomodem/ad;

    invoke-direct {v0, p1}, Lcom/google/android/gms/audiomodem/ad;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/aw;->j:Lcom/google/android/gms/audiomodem/ad;

    .line 124
    return-void

    .line 109
    :cond_0
    const-string v0, "android.media.SCO_AUDIO_STATE_CHANGED"

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/audiomodem/aw;)V
    .locals 2

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/google/android/gms/audiomodem/aw;->d:Z

    invoke-virtual {p0}, Lcom/google/android/gms/audiomodem/aw;->a()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/audiomodem/aw;->d:Z

    iget-boolean v1, p0, Lcom/google/android/gms/audiomodem/aw;->d:Z

    if-eq v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aw;->c:Lcom/google/android/gms/audiomodem/ba;

    if-eqz v0, :cond_1

    const-string v0, "audioModem"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PlaybackCapabilityHelper: onPlaybackCapabilityUpdated called with canPlay = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/gms/audiomodem/aw;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aw;->c:Lcom/google/android/gms/audiomodem/ba;

    iget-boolean v1, p0, Lcom/google/android/gms/audiomodem/aw;->d:Z

    invoke-interface {v0, v1}, Lcom/google/android/gms/audiomodem/ba;->a(Z)V

    :cond_1
    return-void
.end method


# virtual methods
.method final a()Z
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 154
    iget-boolean v2, p0, Lcom/google/android/gms/audiomodem/aw;->h:Z

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/aw;->i:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "audioModem"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "PlaybackCapabilityHelper: Wired headset plugged in. Should not broadcast."

    const-string v3, "audioModem"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move v2, v1

    :goto_0
    if-eqz v2, :cond_9

    :goto_1
    return v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/audiomodem/aw;->j:Lcom/google/android/gms/audiomodem/ad;

    iget-object v3, v2, Lcom/google/android/gms/audiomodem/ad;->a:Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v2, "audioModem"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "BluetoothAudioChecker: Bluetooth A2dp on"

    const-string v3, "audioModem"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move v2, v0

    :goto_2
    if-eqz v2, :cond_8

    const-string v2, "audioModem"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "PlaybackCapabilityHelper: Bluetooth headset connected. Should not broadcast."

    const-string v3, "audioModem"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move v2, v1

    goto :goto_0

    :cond_4
    iget-object v3, v2, Lcom/google/android/gms/audiomodem/ad;->a:Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v2, "audioModem"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "BluetoothAudioChecker: Bluetooth Sco on"

    const-string v3, "audioModem"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    move v2, v0

    goto :goto_2

    :cond_6
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_7

    invoke-virtual {v2}, Lcom/google/android/gms/audiomodem/ad;->a()Z

    move-result v2

    if-eqz v2, :cond_7

    move v2, v0

    goto :goto_2

    :cond_7
    move v2, v1

    goto :goto_2

    :cond_8
    move v2, v0

    goto :goto_0

    :cond_9
    move v0, v1

    goto :goto_1
.end method
