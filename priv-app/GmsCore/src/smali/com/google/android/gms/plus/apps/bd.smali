.class public final Lcom/google/android/gms/plus/apps/bd;
.super Lcom/google/android/gms/plus/apps/bf;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/support/v4/app/av;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lcom/google/android/gms/plus/apps/ay;
.implements Lcom/google/android/gms/plus/apps/bc;
.implements Lcom/google/android/gms/plus/apps/bi;


# instance fields
.field private i:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/CharSequence;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Landroid/widget/TextView;

.field private final p:Ljava/util/ArrayList;

.field private q:Lcom/google/android/gms/plus/apps/ba;

.field private r:Lcom/google/android/gms/plus/apps/az;

.field private s:Lcom/google/android/gms/common/ui/widget/k;

.field private t:Lcom/google/android/gms/plus/apps/bg;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/bf;-><init>()V

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->p:Ljava/util/ArrayList;

    return-void
.end method

.method public static a(Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;Ljava/lang/String;)Lcom/google/android/gms/plus/apps/bd;
    .locals 2

    .prologue
    .line 82
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 83
    if-eqz p0, :cond_1

    .line 84
    const-string v1, "app_filter"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 88
    :cond_0
    :goto_0
    new-instance v1, Lcom/google/android/gms/plus/apps/bd;

    invoke-direct {v1}, Lcom/google/android/gms/plus/apps/bd;-><init>()V

    .line 89
    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/bd;->setArguments(Landroid/os/Bundle;)V

    .line 90
    return-object v1

    .line 85
    :cond_1
    if-eqz p1, :cond_0

    .line 86
    const-string v1, "collection_filter"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/widget/TextView;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 392
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->i:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-eqz v0, :cond_2

    .line 393
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->i:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d()Ljava/lang/String;

    move-result-object v1

    .line 394
    if-nez v1, :cond_4

    .line 395
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->j:Lcom/google/android/gms/plus/apps/c;

    iget-object v0, v0, Lcom/google/android/gms/plus/apps/c;->c:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/model/a/a;

    .line 396
    iget-object v4, p0, Lcom/google/android/gms/plus/apps/bd;->i:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->f()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/a/a;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 397
    invoke-static {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a(Lcom/google/android/gms/plus/model/a/a;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/apps/bd;->i:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    .line 398
    invoke-interface {v0}, Lcom/google/android/gms/plus/model/a/a;->d()Ljava/lang/String;

    move-result-object v0

    .line 403
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 404
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 407
    if-eqz v0, :cond_1

    move-object p1, v2

    :cond_1
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/bd;->o:Landroid/widget/TextView;

    .line 416
    :goto_1
    return-void

    .line 408
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->k:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 410
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/bd;->o:Landroid/widget/TextView;

    goto :goto_1

    .line 413
    :cond_3
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 414
    iput-object v2, p0, Lcom/google/android/gms/plus/apps/bd;->o:Landroid/widget/TextView;

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 142
    invoke-static {}, Lcom/google/android/gms/plus/apps/af;->a()Lcom/google/android/gms/plus/apps/af;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/apps/af;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 144
    invoke-super {p0}, Lcom/google/android/gms/plus/apps/bf;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/ax;

    if-eqz v0, :cond_0

    .line 145
    invoke-super {p0}, Lcom/google/android/gms/plus/apps/bf;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/ax;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/apps/ax;->c()V

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->j:Lcom/google/android/gms/plus/apps/c;

    iget-object v0, v0, Lcom/google/android/gms/plus/apps/c;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->m:Ljava/lang/String;

    .line 148
    iput-object v2, p0, Lcom/google/android/gms/plus/apps/bd;->n:Ljava/lang/String;

    .line 149
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Landroid/support/v4/app/ar;->a(ZZ)V

    .line 150
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/16 v1, 0xc8

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/au;->b(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 152
    :cond_1
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 379
    invoke-super {p0}, Lcom/google/android/gms/plus/apps/bf;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/ax;

    if-eqz v0, :cond_0

    .line 380
    invoke-super {p0}, Lcom/google/android/gms/plus/apps/bf;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/ax;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/apps/ax;->d()V

    .line 382
    :cond_0
    invoke-static {}, Lcom/google/android/gms/plus/apps/af;->a()Lcom/google/android/gms/plus/apps/af;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/android/gms/plus/apps/af;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 383
    invoke-static {}, Lcom/google/android/gms/plus/apps/af;->a()Lcom/google/android/gms/plus/apps/af;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/android/gms/plus/apps/af;->b(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/bd;->a(Ljava/lang/CharSequence;)V

    .line 388
    :goto_0
    invoke-super {p0, v1, v1}, Landroid/support/v4/app/ar;->a(ZZ)V

    .line 389
    return-void

    .line 386
    :cond_1
    sget v0, Lcom/google/android/gms/p;->tU:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/bd;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/bd;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final V_()V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->o:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->o:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/bd;->a(Landroid/widget/TextView;)V

    .line 281
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->r:Lcom/google/android/gms/plus/apps/az;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/apps/az;->a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 338
    if-nez v0, :cond_0

    .line 339
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bd;->q:Lcom/google/android/gms/plus/apps/ba;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/plus/apps/ba;->a(Ljava/lang/String;)V

    .line 341
    :cond_0
    return-object v0
.end method

.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 7

    .prologue
    .line 285
    const/16 v0, 0xc8

    if-ne p1, v0, :cond_1

    .line 286
    new-instance v0, Lcom/google/android/gms/plus/apps/be;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/bd;->j:Lcom/google/android/gms/plus/apps/c;

    iget-object v2, v2, Lcom/google/android/gms/plus/apps/c;->a:Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/bd;->k:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/apps/bd;->i:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/plus/apps/bd;->i:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->f()Ljava/lang/String;

    move-result-object v4

    :goto_0
    sget-object v5, Lcom/google/android/gms/plus/c/a;->H:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/gms/plus/apps/bd;->n:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/apps/be;-><init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    return-object v0

    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 291
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown loader ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 323
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/16 v5, 0xc8

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 46
    check-cast p2, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    iget v0, p1, Landroid/support/v4/a/j;->m:I

    if-ne v0, v5, :cond_2

    invoke-super {p0, v1, v1}, Landroid/support/v4/app/ar;->a(ZZ)V

    check-cast p1, Lcom/google/android/gms/plus/apps/be;

    iget-object v3, p1, Lcom/google/android/gms/plus/apps/be;->a:Lcom/google/android/gms/common/c;

    invoke-super {p0}, Lcom/google/android/gms/plus/apps/bf;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/ax;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/plus/apps/ax;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getActivity()Landroid/support/v4/app/q;

    move-result-object v4

    invoke-direct {v0, v4, p0}, Lcom/google/android/gms/plus/apps/ax;-><init>(Landroid/content/Context;Lcom/google/android/gms/plus/apps/ay;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/bd;->a(Landroid/widget/ListAdapter;)V

    :cond_0
    if-eqz p2, :cond_1

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/support/v4/app/au;->a(I)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/bd;->h()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->o:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->o:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->o:Landroid/widget/TextView;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->l:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/bd;->a(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->n:Ljava/lang/String;

    invoke-super {p0}, Lcom/google/android/gms/plus/apps/bf;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/ax;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;->b()Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/apps/bd;->n:Ljava/lang/String;

    if-eqz v4, :cond_5

    :goto_1
    invoke-virtual {v0, v3, v1}, Lcom/google/android/gms/plus/apps/ax;->a(Ljava/util/List;Z)V

    goto :goto_0

    :cond_5
    move v1, v2

    goto :goto_1
.end method

.method public final a(Landroid/support/v7/app/a;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 230
    sget v0, Lcom/google/android/gms/h;->cs:I

    invoke-virtual {p1, v0}, Landroid/support/v7/app/a;->b(I)V

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->j:Lcom/google/android/gms/plus/apps/c;

    iget-object v0, v0, Lcom/google/android/gms/plus/apps/c;->a:Landroid/accounts/Account;

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->i:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->k:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 235
    :cond_0
    sget v0, Lcom/google/android/gms/l;->eI:I

    invoke-virtual {p1, v0}, Landroid/support/v7/app/a;->a(I)V

    .line 236
    invoke-virtual {p1}, Landroid/support/v7/app/a;->b()Landroid/view/View;

    move-result-object v1

    .line 237
    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 238
    sget v2, Lcom/google/android/gms/p;->sK:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 239
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    sget v3, Lcom/google/android/gms/q;->I:I

    invoke-virtual {v0, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 241
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 242
    sget v0, Lcom/google/android/gms/j;->sf:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/bd;->a(Landroid/widget/TextView;)V

    .line 267
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->m:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 270
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/bd;->g()V

    .line 272
    :cond_2
    return-void

    .line 246
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/c;->a(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v5

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->s:Lcom/google/android/gms/common/ui/widget/k;

    if-nez v0, :cond_4

    .line 249
    new-instance v0, Lcom/google/android/gms/common/ui/widget/k;

    invoke-virtual {p1}, Landroid/support/v7/app/a;->g()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/gms/l;->eG:I

    sget v3, Lcom/google/android/gms/j;->rY:I

    sget v4, Lcom/google/android/gms/j;->f:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/ui/widget/k;-><init>(Landroid/content/Context;III[Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->s:Lcom/google/android/gms/common/ui/widget/k;

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->s:Lcom/google/android/gms/common/ui/widget/k;

    sget v1, Lcom/google/android/gms/p;->sK:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/ui/widget/k;->a(I)V

    .line 255
    :cond_4
    sget v0, Lcom/google/android/gms/l;->eH:I

    invoke-virtual {p1, v0}, Landroid/support/v7/app/a;->a(I)V

    .line 256
    invoke-virtual {p1}, Landroid/support/v7/app/a;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 257
    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 258
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bd;->s:Lcom/google/android/gms/common/ui/widget/k;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 259
    invoke-virtual {v0, v7}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 260
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bd;->s:Lcom/google/android/gms/common/ui/widget/k;

    invoke-virtual {v1, v6}, Lcom/google/android/gms/common/ui/widget/k;->getPosition(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 262
    array-length v1, v5

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 263
    invoke-virtual {v0, v7}, Landroid/widget/Spinner;->setBackgroundResource(I)V

    .line 264
    invoke-virtual {v0, v7}, Landroid/widget/Spinner;->setClickable(Z)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/apps/bh;)V
    .locals 4

    .prologue
    .line 437
    iget-object v0, p1, Lcom/google/android/gms/plus/apps/bh;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bd;->i:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 439
    new-instance v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iget-object v1, p1, Lcom/google/android/gms/plus/apps/bh;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/apps/bh;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/plus/apps/bh;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->i:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    .line 442
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->o:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->o:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/bd;->a(Landroid/widget/TextView;)V

    .line 446
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;)V
    .locals 3

    .prologue
    .line 356
    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;->ad_()Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;->ad_()Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;->c()Ljava/lang/String;

    move-result-object v0

    .line 357
    :goto_0
    sget v1, Lcom/google/android/gms/p;->tX:I

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-static {v0, v1, v2, p0}, Lcom/google/android/gms/plus/apps/an;->a(Ljava/lang/String;ILandroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)Z

    move-result v0

    .line 361
    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/common/analytics/c;->x:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    .line 363
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bd;->j:Lcom/google/android/gms/plus/apps/c;

    sget-object v2, Lcom/google/android/gms/common/analytics/d;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/plus/apps/c;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 364
    return-void

    .line 356
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 361
    :cond_1
    sget-object v0, Lcom/google/android/gms/common/analytics/c;->y:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 346
    if-eqz p2, :cond_0

    .line 347
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->r:Lcom/google/android/gms/plus/apps/az;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/apps/az;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    .line 348
    invoke-super {p0}, Lcom/google/android/gms/plus/apps/bf;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/ax;

    if-eqz v0, :cond_0

    .line 349
    invoke-super {p0}, Lcom/google/android/gms/plus/apps/bf;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/ax;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/apps/ax;->a()V

    .line 352
    :cond_0
    return-void
.end method

.method public final bridge synthetic b()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 46
    invoke-super {p0}, Lcom/google/android/gms/plus/apps/bf;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/ax;

    return-object v0
.end method

.method final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 373
    invoke-super {p0}, Lcom/google/android/gms/plus/apps/bf;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/ax;

    if-eqz v0, :cond_0

    .line 374
    invoke-super {p0}, Lcom/google/android/gms/plus/apps/bf;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/ax;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/apps/ax;->b()V

    .line 376
    :cond_0
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 220
    sget v0, Lcom/google/android/gms/p;->sK:I

    return v0
.end method

.method protected final d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    .prologue
    .line 225
    sget-object v0, Lcom/google/android/gms/common/analytics/d;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method public final e()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->p:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 332
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/16 v1, 0xc8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/au;->b(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 333
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 95
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/apps/bf;->onActivityCreated(Landroid/os/Bundle;)V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->i()V

    .line 97
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 98
    if-eqz p1, :cond_3

    const-string v0, "moment_list_app_filter"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 100
    const-string v0, "moment_list_app_filter"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->i:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    .line 104
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "collection_filter"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->k:Ljava/lang/String;

    .line 105
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/ba;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/ba;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->q:Lcom/google/android/gms/plus/apps/ba;

    .line 106
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/az;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/az;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->r:Lcom/google/android/gms/plus/apps/az;

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->q:Lcom/google/android/gms/plus/apps/ba;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/apps/ba;->a(Lcom/google/android/gms/plus/apps/bc;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->i:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->k:Ljava/lang/String;

    if-eqz v0, :cond_4

    :cond_0
    sget v0, Lcom/google/android/gms/p;->tV:I

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/bd;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->l:Ljava/lang/CharSequence;

    .line 111
    if-eqz p1, :cond_1

    const-string v0, "moment_list_deleted_moments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->p:Ljava/util/ArrayList;

    const-string v1, "moment_list_deleted_moments"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 115
    :cond_1
    invoke-static {}, Lcom/google/android/gms/plus/apps/af;->a()Lcom/google/android/gms/plus/apps/af;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/apps/af;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 116
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/bd;->h()V

    .line 123
    :cond_2
    :goto_2
    return-void

    .line 102
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "app_filter"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->i:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    goto :goto_0

    .line 108
    :cond_4
    sget v0, Lcom/google/android/gms/p;->tT:I

    goto :goto_1

    .line 117
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->i:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->i:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 119
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/bg;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->t:Lcom/google/android/gms/plus/apps/bg;

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->t:Lcom/google/android/gms/plus/apps/bg;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bd;->i:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/plus/apps/c;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/gms/plus/apps/bg;->a(Lcom/google/android/gms/plus/apps/bi;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    .prologue
    .line 368
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 369
    return-void
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 127
    invoke-super {p0}, Lcom/google/android/gms/plus/apps/bf;->onDestroy()V

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->q:Lcom/google/android/gms/plus/apps/ba;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/apps/ba;->b(Lcom/google/android/gms/plus/apps/bc;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->t:Lcom/google/android/gms/plus/apps/bg;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->t:Lcom/google/android/gms/plus/apps/bg;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/apps/bg;->a(Lcom/google/android/gms/plus/apps/bi;)V

    .line 132
    :cond_0
    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7

    .prologue
    .line 161
    invoke-super {p0}, Lcom/google/android/gms/plus/apps/bf;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/ax;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/plus/apps/ax;->a(I)Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;

    move-result-object v2

    .line 162
    if-eqz v2, :cond_1

    .line 163
    new-instance v3, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const-class v1, Lcom/google/android/gms/plus/apps/ManageMomentActivity;

    invoke-direct {v3, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bd;->j:Lcom/google/android/gms/plus/apps/c;

    iget-object v1, v1, Lcom/google/android/gms/plus/apps/c;->a:Landroid/accounts/Account;

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "app_activity"

    invoke-interface {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bd;->i:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-nez v1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->j:Lcom/google/android/gms/plus/apps/c;

    iget-object v0, v0, Lcom/google/android/gms/plus/apps/c;->c:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/model/a/a;

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/a/a;->f()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a(Lcom/google/android/gms/plus/model/a/a;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v0

    :goto_0
    const-string v1, "application"

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v3, v1}, Landroid/support/v4/app/q;->startActivityForResult(Landroid/content/Intent;I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->j:Lcom/google/android/gms/plus/apps/c;

    sget-object v1, Lcom/google/android/gms/common/analytics/d;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lcom/google/android/gms/common/analytics/d;->l:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/apps/c;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 165
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bd;->s:Lcom/google/android/gms/common/ui/widget/k;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/common/ui/widget/k;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 421
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bd;->j:Lcom/google/android/gms/plus/apps/c;

    iget-object v1, v1, Lcom/google/android/gms/plus/apps/c;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 422
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 423
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bd;->j:Lcom/google/android/gms/plus/apps/c;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/c;->b(Ljava/lang/String;)V

    .line 424
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bd;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 426
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/bd;->g()V

    .line 429
    :cond_0
    return-void
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 433
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 175
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 190
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/apps/bf;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 177
    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/plus/c/a;->E:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 178
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 183
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bd;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/common/activity/WebViewActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 184
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/apps/bd;->startActivityForResult(Landroid/content/Intent;I)V

    .line 188
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 186
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/bd;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 175
    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_0
    .end packed-switch
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 4

    .prologue
    .line 169
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/apps/bf;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 170
    const/4 v0, 0x0

    const/16 v1, 0xc8

    const/16 v2, 0x64

    sget v3, Lcom/google/android/gms/p;->eU:I

    invoke-interface {p1, v0, v1, v2, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 171
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 136
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/apps/bf;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 137
    const-string v0, "moment_list_app_filter"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bd;->i:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 138
    const-string v0, "moment_list_deleted_moments"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bd;->p:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 139
    return-void
.end method
