.class public Lcom/google/android/gms/reminders/packagemanager/PackageService;
.super Lcom/google/android/gms/common/app/d;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    const-string v0, "PackageService"

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/app/d;-><init>(Ljava/lang/String;)V

    .line 13
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 17
    const-string v0, "android.intent.extra.UID"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 18
    if-ne v0, v1, :cond_0

    .line 22
    :goto_0
    return-void

    .line 21
    :cond_0
    invoke-static {}, Lcom/google/android/gms/reminders/packagemanager/a;->a()Lcom/google/android/gms/reminders/packagemanager/a;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Lcom/google/android/gms/reminders/packagemanager/a;->a(Landroid/content/Context;I)V

    goto :goto_0
.end method
