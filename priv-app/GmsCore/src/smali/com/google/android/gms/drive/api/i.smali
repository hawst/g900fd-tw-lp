.class public final Lcom/google/android/gms/drive/api/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/api/k;


# instance fields
.field final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field final b:Lcom/google/android/gms/drive/g/i;

.field private final c:Landroid/util/SparseArray;

.field private final d:Lcom/google/android/gms/drive/a/a/a;

.field private final e:Lcom/google/android/gms/drive/b/f;

.field private final f:Lcom/google/android/gms/drive/b/d;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/a/a/a;Lcom/google/android/gms/drive/b/f;Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/drive/b/d;)V
    .locals 2

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/drive/api/i;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 69
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/api/i;->c:Landroid/util/SparseArray;

    .line 141
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/a/a;

    iput-object v0, p0, Lcom/google/android/gms/drive/api/i;->d:Lcom/google/android/gms/drive/a/a/a;

    .line 142
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/f;

    iput-object v0, p0, Lcom/google/android/gms/drive/api/i;->e:Lcom/google/android/gms/drive/b/f;

    .line 143
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/i;

    iput-object v0, p0, Lcom/google/android/gms/drive/api/i;->b:Lcom/google/android/gms/drive/g/i;

    .line 144
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/d;

    iput-object v0, p0, Lcom/google/android/gms/drive/api/i;->f:Lcom/google/android/gms/drive/b/d;

    .line 145
    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/api/j;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/a/a/l;)Lcom/google/android/gms/drive/DriveId;
    .locals 10

    .prologue
    .line 335
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/l;->c:Lcom/google/android/gms/drive/metadata/internal/a/p;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    .line 338
    iget-object v9, p2, Lcom/google/android/gms/drive/api/j;->e:Lcom/google/android/gms/drive/b/l;

    new-instance v0, Lcom/google/android/gms/drive/b/h;

    iget-object v1, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, p1, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-wide v4, p2, Lcom/google/android/gms/drive/api/j;->i:J

    iget-object v7, p0, Lcom/google/android/gms/drive/api/i;->d:Lcom/google/android/gms/drive/a/a/a;

    move-object v3, p3

    move-object v6, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/drive/b/h;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/a/a/a;Lcom/google/android/gms/drive/a/a/l;)V

    invoke-virtual {v9, v0}, Lcom/google/android/gms/drive/b/l;->a(Lcom/google/android/gms/drive/b/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/a;

    .line 344
    iget v1, v0, Lcom/google/android/gms/drive/b/a;->a:I

    .line 345
    iget-object v0, v0, Lcom/google/android/gms/drive/b/a;->b:Lcom/google/android/gms/drive/a/c;

    check-cast v0, Lcom/google/android/gms/drive/a/l;

    .line 346
    if-eqz v1, :cond_1

    .line 349
    const/4 v0, 0x3

    if-ne v1, v0, :cond_0

    const/16 v0, 0x5de

    .line 353
    :goto_0
    new-instance v1, Lcom/google/android/gms/common/service/h;

    const-string v2, "Failed to create the file."

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 358
    :catch_0
    move-exception v0

    :try_start_1
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0x8

    const-string v2, "Failed to commit file because of an I/O error."

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 361
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {p0, p2}, Lcom/google/android/gms/drive/api/i;->a(Lcom/google/android/gms/drive/api/j;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 335
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 349
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 355
    :cond_1
    :try_start_3
    iget-object v1, p0, Lcom/google/android/gms/drive/api/i;->f:Lcom/google/android/gms/drive/b/d;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/b/d;->b()V

    .line 356
    iget-object v0, v0, Lcom/google/android/gms/drive/a/l;->d:Lcom/google/android/gms/drive/DriveId;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 361
    :try_start_4
    invoke-virtual {p0, p2}, Lcom/google/android/gms/drive/api/i;->a(Lcom/google/android/gms/drive/api/j;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    monitor-exit p0

    return-object v0
.end method

.method private declared-synchronized a(I)Lcom/google/android/gms/drive/api/j;
    .locals 1

    .prologue
    .line 183
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/i;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/api/j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a()Ljava/util/Collection;
    .locals 4

    .prologue
    .line 151
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 152
    iget-object v0, p0, Lcom/google/android/gms/drive/api/i;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 153
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/drive/api/i;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/api/j;

    .line 155
    iget-object v0, v0, Lcom/google/android/gms/drive/api/j;->c:Ljava/lang/String;

    .line 156
    if-eqz v0, :cond_0

    .line 157
    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 160
    :cond_1
    monitor-exit p0

    return-object v2

    .line 151
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/api/j;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/a/a/l;)V
    .locals 12

    .prologue
    .line 371
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/l;->c:Lcom/google/android/gms/drive/metadata/internal/a/p;

    invoke-virtual {p3, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 372
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/l;->c:Lcom/google/android/gms/drive/metadata/internal/a/p;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    .line 375
    :cond_0
    iget-object v0, p2, Lcom/google/android/gms/drive/api/j;->e:Lcom/google/android/gms/drive/b/l;

    new-instance v1, Lcom/google/android/gms/drive/b/c;

    iget-object v2, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v3, p1, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-wide v6, p2, Lcom/google/android/gms/drive/api/j;->i:J

    iget-object v8, p0, Lcom/google/android/gms/drive/api/i;->d:Lcom/google/android/gms/drive/a/a/a;

    iget-object v9, p2, Lcom/google/android/gms/drive/api/j;->c:Ljava/lang/String;

    iget-object v10, p2, Lcom/google/android/gms/drive/api/j;->d:Ljava/lang/String;

    move-object/from16 v4, p4

    move-object v5, p3

    move-object/from16 v11, p5

    invoke-direct/range {v1 .. v11}, Lcom/google/android/gms/drive/b/c;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcom/google/android/gms/drive/a/a/a;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/a/a/l;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/b/l;->a(Lcom/google/android/gms/drive/b/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/a;

    .line 383
    iget v0, v0, Lcom/google/android/gms/drive/b/a;->a:I

    .line 384
    if-eqz v0, :cond_2

    .line 387
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/16 v0, 0x5de

    .line 391
    :goto_0
    new-instance v1, Lcom/google/android/gms/common/service/h;

    const-string v2, "Failed to commit changes."

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 395
    :catch_0
    move-exception v0

    :try_start_1
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0x8

    const-string v2, "Failed to commit file because of an I/O error."

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 398
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {p0, p2}, Lcom/google/android/gms/drive/api/i;->a(Lcom/google/android/gms/drive/api/j;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 371
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 387
    :cond_1
    const/16 v0, 0x8

    goto :goto_0

    .line 393
    :cond_2
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/drive/api/i;->f:Lcom/google/android/gms/drive/b/d;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/b/d;->b()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 398
    :try_start_4
    invoke-virtual {p0, p2}, Lcom/google/android/gms/drive/api/i;->a(Lcom/google/android/gms/drive/api/j;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 399
    monitor-exit p0

    return-void
.end method

.method private b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;IILandroid/os/IBinder;)Lcom/google/android/gms/drive/Contents;
    .locals 14

    .prologue
    .line 245
    if-eqz p2, :cond_3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    move-object v12, v1

    .line 246
    :goto_0
    if-nez v12, :cond_0

    const/high16 v1, 0x10000000

    move/from16 v0, p4

    if-eq v0, v1, :cond_4

    :cond_0
    const/4 v1, 0x1

    :goto_1
    const-string v2, "New files must not be created with MODE_READ_ONLY."

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 248
    if-eqz p3, :cond_1

    const/high16 v1, 0x20000000

    move/from16 v0, p4

    if-ne v0, v1, :cond_5

    :cond_1
    const/4 v1, 0x1

    :goto_2
    const-string v2, "baseRequestId must be used with MODE_WRITE_ONLY."

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 251
    const/high16 v1, 0x20000000

    move/from16 v0, p4

    if-eq v0, v1, :cond_2

    const/high16 v1, 0x10000000

    move/from16 v0, p4

    if-eq v0, v1, :cond_2

    const/high16 v1, 0x30000000

    move/from16 v0, p4

    if-ne v0, v1, :cond_6

    :cond_2
    const/4 v1, 0x1

    :goto_3
    const-string v2, "Invalid mode"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 255
    if-eqz p2, :cond_7

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v1

    move-object v10, v1

    .line 256
    :goto_4
    const/4 v4, 0x0

    .line 257
    const/4 v11, 0x0

    .line 261
    iget-object v1, p0, Lcom/google/android/gms/drive/api/i;->f:Lcom/google/android/gms/drive/b/d;

    iget-object v13, v1, Lcom/google/android/gms/drive/b/d;->f:Ljava/lang/Object;

    monitor-enter v13

    .line 264
    if-eqz p3, :cond_9

    .line 268
    :try_start_0
    iget-object v1, p1, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    move/from16 v0, p3

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/drive/api/i;->a(Lcom/google/android/gms/drive/auth/AppIdentity;I)V

    .line 269
    move/from16 v0, p3

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/api/i;->a(I)Lcom/google/android/gms/drive/api/j;

    move-result-object v1

    .line 270
    iget-object v2, v1, Lcom/google/android/gms/drive/api/j;->e:Lcom/google/android/gms/drive/b/l;

    if-eqz v2, :cond_8

    .line 271
    new-instance v1, Lcom/google/android/gms/common/service/h;

    const/16 v2, 0x8

    const-string v3, "Only READ_ONLY contents may be reopenForWrite()."

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 327
    :catchall_0
    move-exception v1

    monitor-exit v13

    throw v1

    .line 245
    :cond_3
    const/4 v1, 0x0

    move-object v12, v1

    goto :goto_0

    .line 246
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 248
    :cond_5
    const/4 v1, 0x0

    goto :goto_2

    .line 251
    :cond_6
    const/4 v1, 0x0

    goto :goto_3

    .line 255
    :cond_7
    const/4 v1, 0x0

    move-object v10, v1

    goto :goto_4

    .line 274
    :cond_8
    :try_start_1
    iget-object v8, v1, Lcom/google/android/gms/drive/api/j;->c:Ljava/lang/String;

    .line 275
    iget-object v9, v1, Lcom/google/android/gms/drive/api/j;->d:Ljava/lang/String;

    .line 276
    invoke-virtual {p0, v1}, Lcom/google/android/gms/drive/api/i;->a(Lcom/google/android/gms/drive/api/j;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 289
    :goto_5
    sparse-switch p4, :sswitch_data_0

    .line 308
    :try_start_2
    new-instance v1, Lcom/google/android/gms/common/service/h;

    const/16 v2, 0xa

    const-string v3, "Unrecognized mode."

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 311
    :catch_0
    move-exception v1

    .line 312
    :try_start_3
    const-string v2, "HashBasedOpenContentsStore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to open file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 313
    new-instance v1, Lcom/google/android/gms/common/service/h;

    const/16 v2, 0x8

    const-string v3, "Unable to open file."

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v1

    .line 277
    :cond_9
    if-eqz p2, :cond_a

    .line 280
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/drive/database/model/ah;->p()Ljava/lang/String;

    move-result-object v8

    .line 281
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/drive/database/model/ah;->m()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v9

    goto :goto_5

    .line 285
    :cond_a
    const/4 v8, 0x0

    .line 286
    const/4 v9, 0x0

    goto :goto_5

    .line 291
    :sswitch_0
    :try_start_4
    iget-object v1, p0, Lcom/google/android/gms/drive/api/i;->e:Lcom/google/android/gms/drive/b/f;

    invoke-virtual {v1, v8}, Lcom/google/android/gms/drive/b/f;->a(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v3

    .line 316
    :goto_6
    if-nez v3, :cond_e

    .line 317
    :try_start_5
    new-instance v1, Lcom/google/android/gms/common/service/h;

    const/16 v2, 0x8

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Content is not available locally: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 294
    :sswitch_1
    :try_start_6
    iget-object v1, p0, Lcom/google/android/gms/drive/api/i;->e:Lcom/google/android/gms/drive/b/f;

    const/high16 v2, 0x20000000

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/b/f;->a(I)Lcom/google/android/gms/drive/b/l;

    move-result-object v4

    .line 295
    invoke-virtual {v4}, Lcom/google/android/gms/drive/b/l;->a()Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    goto :goto_6

    .line 298
    :sswitch_2
    if-eqz v8, :cond_c

    .line 299
    iget-object v5, p0, Lcom/google/android/gms/drive/api/i;->e:Lcom/google/android/gms/drive/b/f;

    invoke-virtual {v5, v8}, Lcom/google/android/gms/drive/b/f;->a(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    if-nez v1, :cond_b

    const/4 v1, 0x0

    :goto_7
    move-object v4, v1

    .line 303
    :goto_8
    if-eqz v4, :cond_d

    .line 304
    invoke-virtual {v4}, Lcom/google/android/gms/drive/b/l;->a()Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    goto :goto_6

    .line 299
    :cond_b
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v2, v5, Lcom/google/android/gms/drive/b/f;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v2, v6}, Lcom/google/android/gms/drive/database/r;->d(Ljava/lang/String;)V

    new-instance v2, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;

    invoke-direct {v2, v1}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    new-instance v1, Ljava/io/FileOutputStream;

    const/4 v3, 0x0

    invoke-virtual {v5, v6, v3}, Lcom/google/android/gms/drive/b/f;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/4 v3, 0x1

    invoke-static {v2, v1, v3}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Z)J

    new-instance v1, Lcom/google/android/gms/drive/b/l;

    iget-object v2, v5, Lcom/google/android/gms/drive/b/f;->a:Lcom/google/android/gms/drive/database/i;

    iget-object v3, v5, Lcom/google/android/gms/drive/b/f;->b:Lcom/google/android/gms/drive/database/r;

    iget-object v4, v5, Lcom/google/android/gms/drive/b/f;->c:Lcom/google/android/gms/drive/g/i;

    const/high16 v7, 0x30000000

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/drive/b/l;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/drive/b/f;Ljava/lang/String;I)V

    goto :goto_7

    .line 301
    :cond_c
    iget-object v1, p0, Lcom/google/android/gms/drive/api/i;->e:Lcom/google/android/gms/drive/b/f;

    const/high16 v2, 0x30000000

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/b/f;->a(I)Lcom/google/android/gms/drive/b/l;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v4

    goto :goto_8

    :cond_d
    move-object v3, v11

    .line 315
    goto :goto_6

    .line 320
    :cond_e
    :try_start_7
    new-instance v1, Lcom/google/android/gms/drive/api/j;

    iget-object v6, p1, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    move-object v2, p0

    move-object v5, v10

    move-object/from16 v7, p5

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/drive/api/j;-><init>(Lcom/google/android/gms/drive/api/i;Landroid/os/ParcelFileDescriptor;Lcom/google/android/gms/drive/b/l;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    invoke-direct {p0, v1}, Lcom/google/android/gms/drive/api/i;->b(Lcom/google/android/gms/drive/api/j;)V

    .line 324
    const/high16 v2, 0x30000000

    move/from16 v0, p4

    if-eq v0, v2, :cond_f

    if-eqz p3, :cond_10

    :cond_f
    const/4 v7, 0x1

    .line 326
    :goto_9
    new-instance v2, Lcom/google/android/gms/drive/Contents;

    iget v4, v1, Lcom/google/android/gms/drive/api/j;->a:I

    move/from16 v5, p4

    move-object v6, v12

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/drive/Contents;-><init>(Landroid/os/ParcelFileDescriptor;IILcom/google/android/gms/drive/DriveId;Z)V

    monitor-exit v13
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    return-object v2

    .line 324
    :cond_10
    const/4 v7, 0x0

    goto :goto_9

    .line 289
    nop

    :sswitch_data_0
    .sparse-switch
        0x10000000 -> :sswitch_0
        0x20000000 -> :sswitch_1
        0x30000000 -> :sswitch_2
    .end sparse-switch
.end method

.method private declared-synchronized b(Lcom/google/android/gms/drive/api/j;)V
    .locals 4

    .prologue
    .line 164
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/drive/api/j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/drive/api/i;->c:Landroid/util/SparseArray;

    iget v1, p1, Lcom/google/android/gms/drive/api/j;->a:I

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/drive/api/i;->f:Lcom/google/android/gms/drive/b/d;

    invoke-direct {p0}, Lcom/google/android/gms/drive/api/i;->a()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/b/d;->a(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    .line 172
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0x8

    const-string v2, "Unable to link client"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/auth/g;ILandroid/os/IBinder;)Lcom/google/android/gms/drive/Contents;
    .locals 6

    .prologue
    .line 189
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/api/i;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;IILandroid/os/IBinder;)Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;IILandroid/os/IBinder;)Lcom/google/android/gms/drive/Contents;
    .locals 1

    .prologue
    .line 212
    invoke-direct/range {p0 .. p5}, Lcom/google/android/gms/drive/api/i;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;IILandroid/os/IBinder;)Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/drive/auth/g;ILcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/a/a/l;)Lcom/google/android/gms/drive/DriveId;
    .locals 6

    .prologue
    .line 197
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/drive/api/i;->a(Lcom/google/android/gms/drive/auth/AppIdentity;I)V

    .line 198
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    invoke-direct {p0, p2}, Lcom/google/android/gms/drive/api/i;->a(I)Lcom/google/android/gms/drive/api/j;

    move-result-object v2

    .line 200
    iget-object v0, v2, Lcom/google/android/gms/drive/api/j;->f:Lcom/google/android/gms/drive/database/model/EntrySpec;

    if-eqz v0, :cond_0

    .line 201
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0xa

    const-string v2, "Cannot create a new file using contents opened from an existing file.Use DriveApi.newDriveContents() to create the contents instead."

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 205
    :try_start_1
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/api/i;->a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/api/j;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/a/a/l;)Lcom/google/android/gms/drive/DriveId;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method final declared-synchronized a(Lcom/google/android/gms/drive/api/j;)V
    .locals 2

    .prologue
    .line 177
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/drive/api/j;->b()V

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/drive/api/i;->c:Landroid/util/SparseArray;

    iget v1, p1, Lcom/google/android/gms/drive/api/j;->a:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/drive/api/i;->f:Lcom/google/android/gms/drive/b/d;

    invoke-direct {p0}, Lcom/google/android/gms/drive/api/i;->a()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/b/d;->a(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    monitor-exit p0

    return-void

    .line 177
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/drive/auth/AppIdentity;I)V
    .locals 6

    .prologue
    const/16 v5, 0xa

    const/4 v4, 0x0

    .line 405
    invoke-direct {p0, p2}, Lcom/google/android/gms/drive/api/i;->a(I)Lcom/google/android/gms/drive/api/j;

    move-result-object v0

    .line 406
    if-nez v0, :cond_0

    .line 407
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Contents already closed."

    invoke-direct {v0, v5, v1, v4}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 410
    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/drive/api/j;->g:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 411
    new-instance v0, Lcom/google/android/gms/common/service/h;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "App %s cannot close this file because it was opened by different app."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v5, v1, v4}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 415
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/auth/g;ILcom/google/android/gms/drive/metadata/internal/MetadataBundle;ZLcom/google/android/gms/drive/a/a/l;)V
    .locals 6

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 218
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/drive/api/i;->a(Lcom/google/android/gms/drive/auth/AppIdentity;I)V

    .line 219
    invoke-direct {p0, p2}, Lcom/google/android/gms/drive/api/i;->a(I)Lcom/google/android/gms/drive/api/j;

    move-result-object v2

    .line 221
    invoke-virtual {p5}, Lcom/google/android/gms/drive/a/a/l;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 223
    if-nez p4, :cond_0

    .line 224
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Can\'t detect conflicts without saveResults"

    invoke-direct {v0, v4, v1, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 227
    :cond_0
    iget-object v0, v2, Lcom/google/android/gms/drive/api/j;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 228
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Can\'t detect conflicts without baseContentHash"

    invoke-direct {v0, v4, v1, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 232
    :cond_1
    if-eqz p4, :cond_2

    iget-object v0, v2, Lcom/google/android/gms/drive/api/j;->e:Lcom/google/android/gms/drive/b/l;

    if-nez v0, :cond_2

    .line 233
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Can\'t save contents opened for READ_ONLY."

    invoke-direct {v0, v4, v1, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 236
    :cond_2
    if-eqz p4, :cond_3

    iget-object v0, v2, Lcom/google/android/gms/drive/api/j;->f:Lcom/google/android/gms/drive/database/model/EntrySpec;

    if-eqz v0, :cond_3

    iget-object v0, v2, Lcom/google/android/gms/drive/api/j;->e:Lcom/google/android/gms/drive/b/l;

    if-eqz v0, :cond_3

    .line 237
    iget-object v4, v2, Lcom/google/android/gms/drive/api/j;->f:Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/api/i;->a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/api/j;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/a/a/l;)V

    .line 241
    :goto_0
    return-void

    .line 239
    :cond_3
    invoke-virtual {p0, v2}, Lcom/google/android/gms/drive/api/i;->a(Lcom/google/android/gms/drive/api/j;)V

    goto :goto_0
.end method
