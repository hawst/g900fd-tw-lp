.class public final Lcom/google/android/gms/wallet/dynamite/ui/g;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Landroid/widget/ImageView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/Button;

.field private g:Landroid/widget/TableRow;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/Button;

.field private k:Landroid/widget/TableLayout;

.field private l:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Context;Lcom/google/android/gms/wallet/dynamite/fragment/a;)V
    .locals 3

    .prologue
    .line 51
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 53
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 54
    sget v1, Lcom/google/android/gms/l;->gD:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 57
    sget v0, Lcom/google/android/gms/j;->lv:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->a:Landroid/widget/ImageView;

    .line 58
    sget v0, Lcom/google/android/gms/j;->lw:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->b:Landroid/widget/ImageView;

    .line 59
    sget v0, Lcom/google/android/gms/j;->sC:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->c:Landroid/widget/TextView;

    .line 60
    sget v0, Lcom/google/android/gms/j;->sB:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->d:Landroid/widget/TextView;

    .line 61
    sget v0, Lcom/google/android/gms/j;->sG:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->e:Landroid/widget/TextView;

    .line 62
    sget v0, Lcom/google/android/gms/j;->cf:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->f:Landroid/widget/Button;

    .line 65
    sget v0, Lcom/google/android/gms/j;->st:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->g:Landroid/widget/TableRow;

    .line 67
    sget v0, Lcom/google/android/gms/j;->sF:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->h:Landroid/widget/TextView;

    .line 69
    sget v0, Lcom/google/android/gms/j;->sE:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->i:Landroid/widget/TextView;

    .line 70
    sget v0, Lcom/google/android/gms/j;->cg:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->j:Landroid/widget/Button;

    .line 73
    sget v0, Lcom/google/android/gms/j;->su:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/dynamite/ui/g;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->k:Landroid/widget/TableLayout;

    .line 75
    sget v0, Lcom/google/android/gms/j;->sD:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/dynamite/ui/g;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->l:Landroid/widget/TextView;

    .line 78
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 81
    iget v2, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->g:I

    if-eqz v2, :cond_5

    .line 82
    iget v2, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->g:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 88
    :cond_0
    :goto_0
    iget v1, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->e:I

    if-eqz v1, :cond_1

    .line 89
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->d:Landroid/widget/TextView;

    iget v2, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->e:I

    invoke-virtual {v1, p2, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 91
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->e:Landroid/widget/TextView;

    iget v2, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->e:I

    invoke-virtual {v1, p2, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 93
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->i:Landroid/widget/TextView;

    iget v2, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->e:I

    invoke-virtual {v1, p2, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 95
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->l:Landroid/widget/TextView;

    iget v2, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->e:I

    invoke-virtual {v1, p2, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 98
    :cond_1
    iget v1, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->f:I

    if-eqz v1, :cond_2

    .line 100
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->c:Landroid/widget/TextView;

    iget v2, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->f:I

    invoke-virtual {v1, p2, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 102
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->h:Landroid/widget/TextView;

    iget v2, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->f:I

    invoke-virtual {v1, p2, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 105
    :cond_2
    iget v1, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->i:I

    if-eqz v1, :cond_3

    .line 107
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->f:Landroid/widget/Button;

    iget v2, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->i:I

    invoke-virtual {v1, p2, v2}, Landroid/widget/Button;->setTextAppearance(Landroid/content/Context;I)V

    .line 109
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->j:Landroid/widget/Button;

    iget v2, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->i:I

    invoke-virtual {v1, p2, v2}, Landroid/widget/Button;->setTextAppearance(Landroid/content/Context;I)V

    .line 112
    :cond_3
    iget v1, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->j:I

    if-eqz v1, :cond_6

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->f:Landroid/widget/Button;

    iget v1, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->j:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->j:Landroid/widget/Button;

    iget v1, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->j:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 124
    :cond_4
    :goto_1
    iget v0, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->l:I

    if-eqz v0, :cond_7

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->b:Landroid/widget/ImageView;

    iget v1, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->l:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 131
    :goto_2
    iget v0, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->m:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_8

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->a:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/gms/h;->dI:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 136
    :goto_3
    return-void

    .line 83
    :cond_5
    iget v2, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->h:I

    if-eqz v2, :cond_0

    .line 85
    iget v2, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->h:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 117
    :cond_6
    iget v1, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->k:I

    if-eqz v1, :cond_4

    .line 119
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->f:Landroid/widget/Button;

    iget v2, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->k:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 121
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->j:Landroid/widget/Button;

    iget v2, p3, Lcom/google/android/gms/wallet/dynamite/fragment/a;->k:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 129
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->b:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/dynamite/ui/g;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/f;->aH:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    goto :goto_2

    .line 134
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->a:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/gms/h;->dH:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->f:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->j:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/MaskedWallet;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/4 v5, 0x0

    .line 151
    if-eqz p1, :cond_5

    .line 152
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->k:Landroid/widget/TableLayout;

    invoke-virtual {v0, v5}, Landroid/widget/TableLayout;->setVisibility(I)V

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->e:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWallet;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->d:Landroid/widget/TextView;

    const-string v1, "\n"

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWallet;->e()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWallet;->g()Lcom/google/android/gms/identity/intents/model/UserAddress;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->g:Landroid/widget/TableRow;

    invoke-virtual {v0, v5}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 158
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->i:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWallet;->g()Lcom/google/android/gms/identity/intents/model/UserAddress;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v0, ""

    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    :goto_1
    return-void

    .line 158
    :cond_1
    if-nez v2, :cond_2

    const/4 v0, 0x0

    :goto_2
    const-string v3, "\n"

    invoke-static {v0, v3}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/gms/identity/intents/model/UserAddress;->n()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/google/android/gms/identity/intents/model/UserAddress;->n()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/t/a/b;

    invoke-direct {v0}, Lcom/google/t/a/b;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/identity/intents/model/UserAddress;->c()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/t/a/b;->s:Ljava/lang/String;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/identity/intents/model/UserAddress;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x1

    invoke-virtual {v2}, Lcom/google/android/gms/identity/intents/model/UserAddress;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {v2}, Lcom/google/android/gms/identity/intents/model/UserAddress;->f()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-virtual {v2}, Lcom/google/android/gms/identity/intents/model/UserAddress;->g()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-virtual {v2}, Lcom/google/android/gms/identity/intents/model/UserAddress;->h()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    iput-object v3, v0, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/identity/intents/model/UserAddress;->k()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/t/a/b;->a:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/identity/intents/model/UserAddress;->j()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/t/a/b;->f:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/identity/intents/model/UserAddress;->i()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/t/a/b;->d:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/identity/intents/model/UserAddress;->l()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/t/a/b;->k:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/identity/intents/model/UserAddress;->m()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/t/a/b;->m:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/identity/intents/model/UserAddress;->o()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/t/a/b;->r:Ljava/lang/String;

    goto/16 :goto_2

    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/gms/identity/intents/model/UserAddress;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 161
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->g:Landroid/widget/TableRow;

    invoke-virtual {v0, v3}, Landroid/widget/TableRow;->setVisibility(I)V

    goto/16 :goto_1

    .line 164
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->k:Landroid/widget/TableLayout;

    invoke-virtual {v0, v3}, Landroid/widget/TableLayout;->setVisibility(I)V

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method public final setEnabled(Z)V
    .locals 1

    .prologue
    .line 171
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->f:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/g;->j:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 174
    return-void
.end method
