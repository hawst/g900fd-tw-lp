.class public final Lcom/google/android/gms/plus/internal/cp;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# instance fields
.field private a:[Ljava/lang/String;

.field private final b:Landroid/widget/ImageView;

.field private final c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/plus/internal/cp;-><init>(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;)V

    .line 33
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;)V
    .locals 4

    .prologue
    const/16 v3, 0x11

    const/4 v2, -0x2

    .line 37
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/cp;->a:[Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/google/android/gms/plus/internal/cp;->b:Landroid/widget/ImageView;

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cp;->b:Landroid/widget/ImageView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/internal/cp;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 42
    iput-object p3, p0, Lcom/google/android/gms/plus/internal/cp;->c:Landroid/widget/TextView;

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cp;->c:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/internal/cp;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cp;->c:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/cp;->bringChildToFront(Landroid/view/View;)V

    .line 47
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cp;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 85
    return-void
.end method

.method public final a(F)V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cp;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 81
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cp;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 93
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cp;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 51
    return-void
.end method

.method public final a([Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/google/android/gms/plus/internal/cp;->a:[Ljava/lang/String;

    .line 97
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cp;->c:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 89
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 101
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 103
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cp;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextSize()F

    move-result v0

    invoke-virtual {v6, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cp;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 106
    const/4 v2, 0x0

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cp;->a:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cp;->a:[Ljava/lang/String;

    array-length v0, v0

    :goto_0
    move v4, v1

    move v3, v1

    move-object v1, v2

    .line 110
    :goto_1
    if-ge v4, v0, :cond_1

    .line 111
    iget-object v2, p0, Lcom/google/android/gms/plus/internal/cp;->a:[Ljava/lang/String;

    aget-object v2, v2, v4

    if-eqz v2, :cond_4

    .line 112
    iget-object v2, p0, Lcom/google/android/gms/plus/internal/cp;->a:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-int v2, v2

    .line 115
    if-gt v2, v5, :cond_4

    if-lt v2, v3, :cond_4

    .line 116
    iget-object v1, p0, Lcom/google/android/gms/plus/internal/cp;->a:[Ljava/lang/String;

    aget-object v1, v1, v4

    .line 110
    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    goto :goto_1

    :cond_0
    move v0, v1

    .line 109
    goto :goto_0

    .line 122
    :cond_1
    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cp;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 123
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cp;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    :cond_3
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 126
    return-void

    :cond_4
    move v2, v3

    goto :goto_2
.end method
