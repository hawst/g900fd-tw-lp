.class public Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/x;


# instance fields
.field final a:I

.field final b:[Lcom/google/android/gms/appdatasearch/CorpusId;

.field public final c:I

.field final d:[Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:Ljava/lang/String;

.field private final transient i:Ljava/util/Map;

.field private final transient j:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/gms/appdatasearch/x;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/x;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/x;

    return-void
.end method

.method constructor <init>(I[Lcom/google/android/gms/appdatasearch/CorpusId;I[Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;IIILjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput p1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->a:I

    .line 98
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->b:[Lcom/google/android/gms/appdatasearch/CorpusId;

    .line 99
    iput p3, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->c:I

    .line 100
    iput p5, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->e:I

    .line 101
    iput p6, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->f:I

    .line 102
    iput p7, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->g:I

    .line 103
    iput-object p8, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->h:Ljava/lang/String;

    .line 105
    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->d:[Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;

    .line 106
    if-eqz p2, :cond_0

    array-length v0, p2

    if-nez v0, :cond_4

    .line 107
    :cond_0
    iput-object v5, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->i:Ljava/util/Map;

    .line 122
    :cond_1
    if-eqz p4, :cond_2

    array-length v0, p4

    if-nez v0, :cond_7

    .line 123
    :cond_2
    iput-object v5, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->j:Ljava/util/Map;

    .line 131
    :cond_3
    return-void

    .line 109
    :cond_4
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->i:Ljava/util/Map;

    move v1, v2

    .line 110
    :goto_0
    array-length v0, p2

    if-ge v1, v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->i:Ljava/util/Map;

    aget-object v3, p2, v1

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/CorpusId;->b:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 112
    if-nez v0, :cond_5

    .line 113
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 114
    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->i:Ljava/util/Map;

    aget-object v4, p2, v1

    iget-object v4, v4, Lcom/google/android/gms/appdatasearch/CorpusId;->b:Ljava/lang/String;

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    :cond_5
    aget-object v3, p2, v1

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/CorpusId;->c:Ljava/lang/String;

    if-eqz v3, :cond_6

    .line 117
    aget-object v3, p2, v1

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/CorpusId;->c:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 110
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 125
    :cond_7
    new-instance v0, Ljava/util/HashMap;

    array-length v1, p4

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->j:Ljava/util/Map;

    .line 127
    :goto_1
    array-length v0, p4

    if-ge v2, v0, :cond_3

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->j:Ljava/util/Map;

    aget-object v1, p4, v2

    iget-object v1, v1, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->b:Lcom/google/android/gms/appdatasearch/CorpusId;

    aget-object v3, p4, v2

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->j:Ljava/util/Map;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 183
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->i:Ljava/util/Map;

    if-eqz v0, :cond_3

    move v1, v2

    .line 149
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->i:Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 151
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v1, v3

    .line 156
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_2

    iget v0, v0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->c:I

    if-eqz v0, :cond_4

    :cond_2
    move v0, v2

    .line 160
    :goto_1
    if-nez v1, :cond_5

    if-eqz v0, :cond_5

    :goto_2
    return v2

    :cond_3
    move v1, v3

    .line 148
    goto :goto_0

    :cond_4
    move v0, v3

    .line 158
    goto :goto_1

    :cond_5
    move v2, v3

    .line 160
    goto :goto_2
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->j:Ljava/util/Map;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 174
    :cond_0
    :goto_0
    return-object v0

    .line 170
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->j:Ljava/util/Map;

    new-instance v2, Lcom/google/android/gms/appdatasearch/CorpusId;

    invoke-direct {v2, p1, p2}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;

    .line 171
    if-nez v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->j:Ljava/util/Map;

    new-instance v2, Lcom/google/android/gms/appdatasearch/CorpusId;

    invoke-direct {v2, p1, v1}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 189
    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/x;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 195
    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/x;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/x;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;Landroid/os/Parcel;I)V

    .line 196
    return-void
.end method
