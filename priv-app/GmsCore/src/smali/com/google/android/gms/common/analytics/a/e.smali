.class public final Lcom/google/android/gms/common/analytics/a/e;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/common/analytics/a/c;

.field public b:Lcom/google/android/gms/f/a/f;

.field public c:Lcom/google/android/gms/common/analytics/a/b;

.field public d:[Lcom/google/android/gms/common/analytics/a/f;

.field public e:[Lcom/google/android/gms/common/analytics/a/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1094
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1095
    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->a:Lcom/google/android/gms/common/analytics/a/c;

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->b:Lcom/google/android/gms/f/a/f;

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->c:Lcom/google/android/gms/common/analytics/a/b;

    invoke-static {}, Lcom/google/android/gms/common/analytics/a/f;->a()[Lcom/google/android/gms/common/analytics/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->d:[Lcom/google/android/gms/common/analytics/a/f;

    invoke-static {}, Lcom/google/android/gms/common/analytics/a/d;->a()[Lcom/google/android/gms/common/analytics/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->e:[Lcom/google/android/gms/common/analytics/a/d;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/analytics/a/e;->cachedSize:I

    .line 1096
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1141
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1142
    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/e;->a:Lcom/google/android/gms/common/analytics/a/c;

    if-eqz v2, :cond_0

    .line 1143
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/common/analytics/a/e;->a:Lcom/google/android/gms/common/analytics/a/c;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1146
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/e;->b:Lcom/google/android/gms/f/a/f;

    if-eqz v2, :cond_1

    .line 1147
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/common/analytics/a/e;->b:Lcom/google/android/gms/f/a/f;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1150
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/e;->c:Lcom/google/android/gms/common/analytics/a/b;

    if-eqz v2, :cond_2

    .line 1151
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/gms/common/analytics/a/e;->c:Lcom/google/android/gms/common/analytics/a/b;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1154
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/e;->d:[Lcom/google/android/gms/common/analytics/a/f;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/e;->d:[Lcom/google/android/gms/common/analytics/a/f;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 1155
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/common/analytics/a/e;->d:[Lcom/google/android/gms/common/analytics/a/f;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 1156
    iget-object v3, p0, Lcom/google/android/gms/common/analytics/a/e;->d:[Lcom/google/android/gms/common/analytics/a/f;

    aget-object v3, v3, v0

    .line 1157
    if-eqz v3, :cond_3

    .line 1158
    const/4 v4, 0x4

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1155
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1163
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/e;->e:[Lcom/google/android/gms/common/analytics/a/d;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/e;->e:[Lcom/google/android/gms/common/analytics/a/d;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 1164
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/e;->e:[Lcom/google/android/gms/common/analytics/a/d;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 1165
    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/e;->e:[Lcom/google/android/gms/common/analytics/a/d;

    aget-object v2, v2, v1

    .line 1166
    if-eqz v2, :cond_6

    .line 1167
    const/4 v3, 0x5

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1164
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1172
    :cond_7
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1057
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->a:Lcom/google/android/gms/common/analytics/a/c;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/common/analytics/a/c;

    invoke-direct {v0}, Lcom/google/android/gms/common/analytics/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->a:Lcom/google/android/gms/common/analytics/a/c;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->a:Lcom/google/android/gms/common/analytics/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->b:Lcom/google/android/gms/f/a/f;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/f/a/f;

    invoke-direct {v0}, Lcom/google/android/gms/f/a/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->b:Lcom/google/android/gms/f/a/f;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->b:Lcom/google/android/gms/f/a/f;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->c:Lcom/google/android/gms/common/analytics/a/b;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/common/analytics/a/b;

    invoke-direct {v0}, Lcom/google/android/gms/common/analytics/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->c:Lcom/google/android/gms/common/analytics/a/b;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->c:Lcom/google/android/gms/common/analytics/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->d:[Lcom/google/android/gms/common/analytics/a/f;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/common/analytics/a/f;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/common/analytics/a/e;->d:[Lcom/google/android/gms/common/analytics/a/f;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/android/gms/common/analytics/a/f;

    invoke-direct {v3}, Lcom/google/android/gms/common/analytics/a/f;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->d:[Lcom/google/android/gms/common/analytics/a/f;

    array-length v0, v0

    goto :goto_1

    :cond_6
    new-instance v3, Lcom/google/android/gms/common/analytics/a/f;

    invoke-direct {v3}, Lcom/google/android/gms/common/analytics/a/f;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/common/analytics/a/e;->d:[Lcom/google/android/gms/common/analytics/a/f;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->e:[Lcom/google/android/gms/common/analytics/a/d;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/common/analytics/a/d;

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/android/gms/common/analytics/a/e;->e:[Lcom/google/android/gms/common/analytics/a/d;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Lcom/google/android/gms/common/analytics/a/d;

    invoke-direct {v3}, Lcom/google/android/gms/common/analytics/a/d;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->e:[Lcom/google/android/gms/common/analytics/a/d;

    array-length v0, v0

    goto :goto_3

    :cond_9
    new-instance v3, Lcom/google/android/gms/common/analytics/a/d;

    invoke-direct {v3}, Lcom/google/android/gms/common/analytics/a/d;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/common/analytics/a/e;->e:[Lcom/google/android/gms/common/analytics/a/d;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1111
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->a:Lcom/google/android/gms/common/analytics/a/c;

    if-eqz v0, :cond_0

    .line 1112
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/e;->a:Lcom/google/android/gms/common/analytics/a/c;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1114
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->b:Lcom/google/android/gms/f/a/f;

    if-eqz v0, :cond_1

    .line 1115
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/e;->b:Lcom/google/android/gms/f/a/f;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1117
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->c:Lcom/google/android/gms/common/analytics/a/b;

    if-eqz v0, :cond_2

    .line 1118
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/e;->c:Lcom/google/android/gms/common/analytics/a/b;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1120
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->d:[Lcom/google/android/gms/common/analytics/a/f;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->d:[Lcom/google/android/gms/common/analytics/a/f;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 1121
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/e;->d:[Lcom/google/android/gms/common/analytics/a/f;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 1122
    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/e;->d:[Lcom/google/android/gms/common/analytics/a/f;

    aget-object v2, v2, v0

    .line 1123
    if-eqz v2, :cond_3

    .line 1124
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1121
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1128
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->e:[Lcom/google/android/gms/common/analytics/a/d;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->e:[Lcom/google/android/gms/common/analytics/a/d;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 1129
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->e:[Lcom/google/android/gms/common/analytics/a/d;

    array-length v0, v0

    if-ge v1, v0, :cond_6

    .line 1130
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/e;->e:[Lcom/google/android/gms/common/analytics/a/d;

    aget-object v0, v0, v1

    .line 1131
    if-eqz v0, :cond_5

    .line 1132
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1129
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1136
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1137
    return-void
.end method
