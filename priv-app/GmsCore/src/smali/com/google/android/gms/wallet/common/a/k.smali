.class public final Lcom/google/android/gms/wallet/common/a/k;
.super Lcom/google/android/gms/wallet/common/a/s;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/regex/Pattern;

.field private static final c:Lcom/google/android/gms/common/util/ap;


# instance fields
.field private d:Z

.field private e:Z

.field private final f:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-string v0, "[\\r\\n]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/a/k;->b:Ljava/util/regex/Pattern;

    .line 41
    new-instance v0, Lcom/google/android/gms/wallet/common/a/l;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/a/l;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/a/k;->c:Lcom/google/android/gms/common/util/ap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    const-string v0, "DeviceAddressSource"

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/a/s;-><init>(Ljava/lang/String;)V

    .line 48
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/a/k;->d:Z

    .line 49
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/a/k;->e:Z

    .line 54
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/a/k;->f:Landroid/content/Context;

    .line 55
    return-void
.end method

.method private static a(II)I
    .locals 2

    .prologue
    .line 205
    sub-int v0, p0, p1

    .line 206
    if-gez v0, :cond_0

    .line 207
    new-instance v0, Ljava/lang/OutOfMemoryError;

    const-string v1, "Device data exceeds allowed storage for source"

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :cond_0
    return v0
.end method

.method private static a(Lcom/google/t/a/b;)I
    .locals 1

    .prologue
    .line 196
    if-nez p0, :cond_0

    .line 197
    const/4 v0, 0x0

    .line 200
    :goto_0
    return v0

    .line 199
    :cond_0
    invoke-virtual {p0}, Lcom/google/t/a/b;->getCachedSize()I

    move-result v0

    .line 200
    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x2d

    div-int/lit8 v0, v0, 0x8

    mul-int/lit8 v0, v0, 0x8

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized c()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 229
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/a/k;->e:Z

    if-nez v1, :cond_0

    .line 230
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/a/k;->e:Z

    .line 231
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/a/k;->f:Landroid/content/Context;

    const-string v2, "android.permission.READ_CONTACTS"

    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/a/k;->d:Z

    .line 235
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/a/k;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 231
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 229
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected final b()Ljava/util/ArrayList;
    .locals 20

    .prologue
    .line 73
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/wallet/common/a/k;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 74
    const/4 v1, 0x0

    .line 188
    :goto_0
    return-object v1

    .line 77
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/a/k;->f:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    invoke-virtual {v1}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v1

    div-int/lit8 v1, v1, 0x10

    mul-int/lit16 v1, v1, 0x400

    mul-int/lit16 v1, v1, 0x400

    if-nez v1, :cond_f

    const/high16 v1, 0x100000

    move v7, v1

    .line 79
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/a/k;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 84
    const/4 v2, 0x2

    new-array v3, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v4, "contact_id"

    aput-object v4, v3, v2

    const/4 v2, 0x1

    const-string v4, "data1"

    aput-object v4, v3, v2

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "in_visible_group=1 AND mimetype=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v8, "vnd.android.cursor.item/name"

    aput-object v8, v5, v6

    const-string v6, "contact_id"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 86
    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    :try_start_0
    new-instance v8, Landroid/util/SparseArray;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-direct {v8, v3}, Landroid/util/SparseArray;-><init>(I)V

    .line 89
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_2

    .line 90
    const-string v3, "contact_id"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 91
    const-string v4, "data1"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 93
    :cond_1
    :goto_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 94
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 95
    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 96
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 97
    invoke-virtual {v8, v5, v6}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 103
    :catchall_0
    move-exception v1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 104
    throw v1

    .line 103
    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 110
    const/16 v2, 0x8

    new-array v3, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v4, "contact_id"

    aput-object v4, v3, v2

    const/4 v2, 0x1

    const-string v4, "data4"

    aput-object v4, v3, v2

    const/4 v2, 0x2

    const-string v4, "data7"

    aput-object v4, v3, v2

    const/4 v2, 0x3

    const-string v4, "data6"

    aput-object v4, v3, v2

    const/4 v2, 0x4

    const-string v4, "data8"

    aput-object v4, v3, v2

    const/4 v2, 0x5

    const-string v4, "data9"

    aput-object v4, v3, v2

    const/4 v2, 0x6

    const-string v4, "data10"

    aput-object v4, v3, v2

    const/4 v2, 0x7

    const-string v4, "data5"

    aput-object v4, v3, v2

    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$StructuredPostal;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "in_visible_group=1"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 114
    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    :try_start_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 117
    new-instance v5, Landroid/util/SparseBooleanArray;

    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v1

    invoke-direct {v5, v1}, Landroid/util/SparseBooleanArray;-><init>(I)V

    .line 118
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_c

    .line 119
    const-string v1, "contact_id"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 120
    const-string v1, "data4"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 121
    const-string v1, "data7"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    .line 122
    const-string v1, "data6"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    .line 123
    const-string v1, "data8"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    .line 124
    const-string v1, "data9"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    .line 125
    const-string v1, "data10"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v13

    .line 126
    const-string v1, "data5"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    .line 127
    :goto_3
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 128
    new-instance v15, Lcom/google/t/a/b;

    invoke-direct {v15}, Lcom/google/t/a/b;-><init>()V

    .line 129
    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 130
    move/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/a/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 131
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_3

    .line 132
    iput-object v1, v15, Lcom/google/t/a/b;->s:Ljava/lang/String;

    .line 134
    :cond_3
    invoke-interface {v2, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_5

    .line 135
    sget-object v1, Lcom/google/android/gms/wallet/common/a/k;->b:Ljava/util/regex/Pattern;

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v17

    .line 136
    const/4 v1, 0x0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v18, v0

    :goto_4
    move/from16 v0, v18

    if-ge v1, v0, :cond_4

    .line 137
    aget-object v19, v17, v1

    invoke-static/range {v19 .. v19}, Lcom/google/android/gms/wallet/common/a/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v1

    .line 136
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 139
    :cond_4
    sget-object v1, Lcom/google/android/gms/wallet/common/a/k;->c:Lcom/google/android/gms/common/util/ap;

    move-object/from16 v0, v17

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/Object;Lcom/google/android/gms/common/util/ap;)I

    move-result v1

    move-object/from16 v0, v17

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iput-object v1, v15, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    .line 142
    :cond_5
    invoke-interface {v2, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/a/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 143
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_6

    .line 144
    iput-object v1, v15, Lcom/google/t/a/b;->f:Ljava/lang/String;

    .line 146
    :cond_6
    invoke-interface {v2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/a/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 147
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_7

    .line 148
    iput-object v1, v15, Lcom/google/t/a/b;->g:Ljava/lang/String;

    .line 150
    :cond_7
    invoke-interface {v2, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/a/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 151
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_8

    .line 152
    iput-object v1, v15, Lcom/google/t/a/b;->d:Ljava/lang/String;

    .line 154
    :cond_8
    invoke-interface {v2, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/a/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 155
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_9

    .line 156
    iput-object v1, v15, Lcom/google/t/a/b;->k:Ljava/lang/String;

    .line 158
    :cond_9
    invoke-interface {v2, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/a/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 159
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_a

    .line 160
    iput-object v1, v15, Lcom/google/t/a/b;->a:Ljava/lang/String;

    .line 162
    :cond_a
    invoke-interface {v2, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/a/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 163
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_b

    .line 164
    iput-object v1, v15, Lcom/google/t/a/b;->n:Ljava/lang/String;

    .line 166
    :cond_b
    invoke-static {v15}, Lcom/google/android/gms/wallet/common/a/k;->a(Lcom/google/t/a/b;)I

    move-result v1

    invoke-static {v7, v1}, Lcom/google/android/gms/wallet/common/a/k;->a(II)I

    move-result v7

    .line 168
    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    const/4 v1, 0x1

    move/from16 v0, v16

    invoke-virtual {v5, v0, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto/16 :goto_3

    .line 173
    :catchall_1
    move-exception v1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 174
    throw v1

    .line 173
    :cond_c
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 176
    const/4 v1, 0x0

    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v6

    move v4, v1

    move v2, v7

    :goto_5
    if-ge v4, v6, :cond_d

    .line 177
    invoke-virtual {v8, v4}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 178
    invoke-virtual {v5, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-nez v1, :cond_e

    .line 179
    invoke-virtual {v8, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 182
    new-instance v7, Lcom/google/t/a/b;

    invoke-direct {v7}, Lcom/google/t/a/b;-><init>()V

    .line 183
    iput-object v1, v7, Lcom/google/t/a/b;->s:Ljava/lang/String;

    .line 184
    invoke-static {v7}, Lcom/google/android/gms/wallet/common/a/k;->a(Lcom/google/t/a/b;)I

    move-result v1

    invoke-static {v2, v1}, Lcom/google/android/gms/wallet/common/a/k;->a(II)I

    move-result v1

    .line 186
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    :goto_6
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v1

    goto :goto_5

    :cond_d
    move-object v1, v3

    .line 188
    goto/16 :goto_0

    :cond_e
    move v1, v2

    goto :goto_6

    :cond_f
    move v7, v1

    goto/16 :goto_1
.end method
