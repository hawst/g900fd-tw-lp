.class public final Lcom/google/android/gms/plus/oob/a;
.super Landroid/text/style/ClickableSpan;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/gms/plus/oob/b;

.field private final c:Lcom/google/android/gms/plus/oob/c;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/gms/plus/oob/b;Lcom/google/android/gms/plus/oob/c;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/gms/plus/oob/a;->a:Landroid/app/Activity;

    .line 34
    iput-object p2, p0, Lcom/google/android/gms/plus/oob/a;->b:Lcom/google/android/gms/plus/oob/b;

    .line 35
    iput-object p3, p0, Lcom/google/android/gms/plus/oob/a;->c:Lcom/google/android/gms/plus/oob/c;

    .line 36
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/a;->c:Lcom/google/android/gms/plus/oob/c;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/a;->c:Lcom/google/android/gms/plus/oob/c;

    iget-object v1, v0, Lcom/google/android/gms/plus/oob/c;->a:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/gms/plus/oob/c;->b:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/gms/plus/oob/c;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, v0, Lcom/google/android/gms/plus/oob/c;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v0, v0, Lcom/google/android/gms/plus/oob/c;->e:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/a;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/a;->b:Lcom/google/android/gms/plus/oob/b;

    iget-object v1, v1, Lcom/google/android/gms/plus/oob/b;->a:Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/a;->b:Lcom/google/android/gms/plus/oob/b;

    iget v2, v2, Lcom/google/android/gms/plus/oob/b;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 78
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/a;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 87
    sget v1, Lcom/google/android/gms/e;->e:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    invoke-virtual {p1, v1}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 88
    sget v1, Lcom/google/android/gms/f;->ah:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 89
    return-void
.end method
