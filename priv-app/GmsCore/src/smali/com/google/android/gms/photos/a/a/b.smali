.class public final Lcom/google/android/gms/photos/a/a/b;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/photos/a/a/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 29
    iput-object v0, p0, Lcom/google/android/gms/photos/a/a/b;->a:Lcom/google/android/gms/photos/a/a/c;

    iput-object v0, p0, Lcom/google/android/gms/photos/a/a/b;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/photos/a/a/b;->cachedSize:I

    .line 30
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 50
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 51
    iget-object v1, p0, Lcom/google/android/gms/photos/a/a/b;->a:Lcom/google/android/gms/photos/a/a/c;

    if-eqz v1, :cond_0

    .line 52
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/photos/a/a/b;->a:Lcom/google/android/gms/photos/a/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    :cond_0
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/photos/a/a/b;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/photos/a/a/b;->a:Lcom/google/android/gms/photos/a/a/c;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/photos/a/a/c;

    invoke-direct {v0}, Lcom/google/android/gms/photos/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/photos/a/a/b;->a:Lcom/google/android/gms/photos/a/a/c;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/photos/a/a/b;->a:Lcom/google/android/gms/photos/a/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/photos/a/a/b;->a:Lcom/google/android/gms/photos/a/a/c;

    if-eqz v0, :cond_0

    .line 43
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/photos/a/a/b;->a:Lcom/google/android/gms/photos/a/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 45
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 46
    return-void
.end method
