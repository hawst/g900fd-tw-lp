.class public final Lcom/google/android/gms/drive/internal/model/FileLocalId;
.super Lcom/google/android/gms/common/server/response/FastJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/drive/internal/model/l;

.field private static final f:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/gms/drive/internal/model/l;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/model/l;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->CREATOR:Lcom/google/android/gms/drive/internal/model/l;

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 53
    sput-object v0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->f:Ljava/util/HashMap;

    const-string v1, "space"

    const-string v2, "space"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->f:Ljava/util/HashMap;

    const-string v1, "value"

    const-string v2, "value"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->f:Ljava/util/HashMap;

    const-string v1, "version"

    const-string v2, "version"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 94
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->b:I

    .line 95
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->a:Ljava/util/Set;

    .line 96
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 106
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->a:Ljava/util/Set;

    .line 107
    iput p2, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->b:I

    .line 108
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->c:Ljava/lang/String;

    .line 109
    iput-object p4, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->d:Ljava/lang/String;

    .line 110
    iput-object p5, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->e:Ljava/lang/String;

    .line 111
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->f:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 269
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 270
    packed-switch v0, :pswitch_data_0

    .line 281
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 272
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->c:Ljava/lang/String;

    .line 284
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 285
    return-void

    .line 275
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->d:Ljava/lang/String;

    goto :goto_0

    .line 278
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->e:Ljava/lang/String;

    goto :goto_0

    .line 270
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 230
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 238
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 232
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->c:Ljava/lang/String;

    .line 236
    :goto_0
    return-object v0

    .line 234
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->d:Ljava/lang/String;

    goto :goto_0

    .line 236
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->e:Ljava/lang/String;

    goto :goto_0

    .line 230
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected final b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->e:Ljava/lang/String;

    return-object v0
.end method

.method protected final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 204
    sget-object v0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->CREATOR:Lcom/google/android/gms/drive/internal/model/l;

    const/4 v0, 0x0

    return v0
.end method

.method public final e(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/FileLocalId;
    .locals 2

    .prologue
    .line 178
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->c:Ljava/lang/String;

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->a:Ljava/util/Set;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 180
    return-object p0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 302
    instance-of v0, p1, Lcom/google/android/gms/drive/internal/model/FileLocalId;

    if-nez v0, :cond_0

    move v0, v1

    .line 333
    :goto_0
    return v0

    .line 307
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 308
    goto :goto_0

    .line 311
    :cond_1
    check-cast p1, Lcom/google/android/gms/drive/internal/model/FileLocalId;

    .line 312
    sget-object v0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 313
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/FileLocalId;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 314
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/FileLocalId;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 316
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/FileLocalId;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/FileLocalId;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 318
    goto :goto_0

    :cond_3
    move v0, v1

    .line 323
    goto :goto_0

    .line 326
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/FileLocalId;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 328
    goto :goto_0

    :cond_5
    move v0, v2

    .line 333
    goto :goto_0
.end method

.method public final f(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/FileLocalId;
    .locals 2

    .prologue
    .line 187
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->d:Ljava/lang/String;

    .line 188
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->a:Ljava/util/Set;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 189
    return-object p0
.end method

.method public final g(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/FileLocalId;
    .locals 2

    .prologue
    .line 196
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->e:Ljava/lang/String;

    .line 197
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->a:Ljava/util/Set;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 198
    return-object p0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 289
    const/4 v0, 0x0

    .line 290
    sget-object v1, Lcom/google/android/gms/drive/internal/model/FileLocalId;->f:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 291
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/FileLocalId;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 292
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 293
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/FileLocalId;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 295
    goto :goto_0

    .line 296
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 209
    sget-object v0, Lcom/google/android/gms/drive/internal/model/FileLocalId;->CREATOR:Lcom/google/android/gms/drive/internal/model/l;

    invoke-static {p0, p1}, Lcom/google/android/gms/drive/internal/model/l;->a(Lcom/google/android/gms/drive/internal/model/FileLocalId;Landroid/os/Parcel;)V

    .line 210
    return-void
.end method
