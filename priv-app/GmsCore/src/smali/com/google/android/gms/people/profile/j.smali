.class final Lcom/google/android/gms/people/profile/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/google/android/gms/people/profile/AvatarView;

.field private b:F

.field private c:F

.field private d:Z

.field private e:F

.field private f:F

.field private g:F

.field private h:J

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/profile/AvatarView;)V
    .locals 0

    .prologue
    .line 998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 999
    iput-object p1, p0, Lcom/google/android/gms/people/profile/j;->a:Lcom/google/android/gms/people/profile/AvatarView;

    .line 1000
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1029
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/j;->i:Z

    .line 1030
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/j;->j:Z

    .line 1031
    return-void
.end method

.method public final a(FFFF)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1006
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/j;->i:Z

    if-eqz v0, :cond_0

    .line 1022
    :goto_0
    return v2

    .line 1010
    :cond_0
    iput p3, p0, Lcom/google/android/gms/people/profile/j;->b:F

    .line 1011
    iput p4, p0, Lcom/google/android/gms/people/profile/j;->c:F

    .line 1014
    iput p2, p0, Lcom/google/android/gms/people/profile/j;->e:F

    .line 1015
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/gms/people/profile/j;->h:J

    .line 1016
    iput p1, p0, Lcom/google/android/gms/people/profile/j;->f:F

    .line 1017
    iget v0, p0, Lcom/google/android/gms/people/profile/j;->e:F

    iget v3, p0, Lcom/google/android/gms/people/profile/j;->f:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/j;->d:Z

    .line 1018
    iget v0, p0, Lcom/google/android/gms/people/profile/j;->e:F

    iget v3, p0, Lcom/google/android/gms/people/profile/j;->f:F

    sub-float/2addr v0, v3

    const/high16 v3, 0x43960000    # 300.0f

    div-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/gms/people/profile/j;->g:F

    .line 1019
    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/j;->i:Z

    .line 1020
    iput-boolean v2, p0, Lcom/google/android/gms/people/profile/j;->j:Z

    .line 1021
    iget-object v0, p0, Lcom/google/android/gms/people/profile/j;->a:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/profile/AvatarView;->post(Ljava/lang/Runnable;)Z

    move v2, v1

    .line 1022
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1017
    goto :goto_1
.end method

.method public final run()V
    .locals 4

    .prologue
    .line 1035
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/j;->j:Z

    if-eqz v0, :cond_1

    .line 1054
    :cond_0
    :goto_0
    return-void

    .line 1040
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1041
    iget-wide v2, p0, Lcom/google/android/gms/people/profile/j;->h:J

    sub-long/2addr v0, v2

    .line 1042
    iget v2, p0, Lcom/google/android/gms/people/profile/j;->f:F

    iget v3, p0, Lcom/google/android/gms/people/profile/j;->g:F

    long-to-float v0, v0

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    .line 1043
    iget-object v1, p0, Lcom/google/android/gms/people/profile/j;->a:Lcom/google/android/gms/people/profile/AvatarView;

    iget v2, p0, Lcom/google/android/gms/people/profile/j;->b:F

    iget v3, p0, Lcom/google/android/gms/people/profile/j;->c:F

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/people/profile/AvatarView;->a(Lcom/google/android/gms/people/profile/AvatarView;FFF)V

    .line 1046
    iget v1, p0, Lcom/google/android/gms/people/profile/j;->e:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/gms/people/profile/j;->d:Z

    iget v2, p0, Lcom/google/android/gms/people/profile/j;->e:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    if-ne v1, v0, :cond_3

    .line 1047
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/profile/j;->a:Lcom/google/android/gms/people/profile/AvatarView;

    iget v1, p0, Lcom/google/android/gms/people/profile/j;->e:F

    iget v2, p0, Lcom/google/android/gms/people/profile/j;->b:F

    iget v3, p0, Lcom/google/android/gms/people/profile/j;->c:F

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/people/profile/AvatarView;->a(Lcom/google/android/gms/people/profile/AvatarView;FFF)V

    .line 1048
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/j;->a()V

    .line 1051
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/j;->j:Z

    if-nez v0, :cond_0

    .line 1052
    iget-object v0, p0, Lcom/google/android/gms/people/profile/j;->a:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/profile/AvatarView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1046
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method
