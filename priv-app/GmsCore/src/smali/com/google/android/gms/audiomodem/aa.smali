.class final Lcom/google/android/gms/audiomodem/aa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/io/PipedOutputStream;

.field final synthetic b:Lcom/google/android/gms/audiomodem/y;


# direct methods
.method constructor <init>(Lcom/google/android/gms/audiomodem/y;Ljava/io/PipedOutputStream;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/aa;->b:Lcom/google/android/gms/audiomodem/y;

    iput-object p2, p0, Lcom/google/android/gms/audiomodem/aa;->a:Ljava/io/PipedOutputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 219
    const-string v0, "audioModem"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    const-string v0, "AudioRecorder: Recorder thread started"

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    :cond_0
    const/16 v0, -0x13

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 225
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aa;->b:Lcom/google/android/gms/audiomodem/y;

    iget-boolean v0, v0, Lcom/google/android/gms/audiomodem/y;->g:Z

    if-nez v0, :cond_3

    .line 226
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aa;->b:Lcom/google/android/gms/audiomodem/y;

    iget-object v0, v0, Lcom/google/android/gms/audiomodem/y;->b:Lcom/google/android/gms/audiomodem/bb;

    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/bb;->a()Landroid/media/AudioRecord;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/audiomodem/aa;->b:Lcom/google/android/gms/audiomodem/y;

    iget-object v1, v1, Lcom/google/android/gms/audiomodem/y;->d:[B

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/aa;->b:Lcom/google/android/gms/audiomodem/y;

    iget-object v2, v2, Lcom/google/android/gms/audiomodem/y;->d:[B

    array-length v2, v2

    invoke-virtual {v0, v1, v4, v2}, Landroid/media/AudioRecord;->read([BII)I

    move-result v1

    .line 228
    if-lez v1, :cond_2

    .line 230
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aa;->a:Ljava/io/PipedOutputStream;

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/aa;->b:Lcom/google/android/gms/audiomodem/y;

    iget-object v2, v2, Lcom/google/android/gms/audiomodem/y;->d:[B

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Ljava/io/PipedOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 238
    :cond_2
    :goto_1
    const/4 v0, -0x3

    if-ne v1, v0, :cond_1

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aa;->b:Lcom/google/android/gms/audiomodem/y;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/audiomodem/y;->g:Z

    goto :goto_0

    .line 231
    :catch_0
    move-exception v0

    .line 232
    const-string v2, "audioModem"

    invoke-static {v2, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 233
    const-string v2, "AudioRecorder: Recorder failed to write to pipe"

    const-string v3, "audioModem"

    invoke-static {v3, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 251
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aa;->b:Lcom/google/android/gms/audiomodem/y;

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/google/android/gms/audiomodem/y;->h:J

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aa;->b:Lcom/google/android/gms/audiomodem/y;

    iget-object v0, v0, Lcom/google/android/gms/audiomodem/y;->b:Lcom/google/android/gms/audiomodem/bb;

    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/bb;->a()Landroid/media/AudioRecord;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    .line 254
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aa;->a:Ljava/io/PipedOutputStream;

    invoke-virtual {v0}, Ljava/io/PipedOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 260
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aa;->b:Lcom/google/android/gms/audiomodem/y;

    iput-boolean v4, v0, Lcom/google/android/gms/audiomodem/y;->g:Z

    .line 261
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aa;->b:Lcom/google/android/gms/audiomodem/y;

    iput-boolean v4, v0, Lcom/google/android/gms/audiomodem/y;->f:Z

    .line 262
    const-string v0, "audioModem"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 264
    const-string v0, "AudioRecorder: Recorder thread exiting"

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    :cond_5
    return-void

    .line 255
    :catch_1
    move-exception v0

    .line 256
    const-string v1, "audioModem"

    invoke-static {v1, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 257
    const-string v1, "AudioRecorder: Recorder failed to close pipe"

    const-string v2, "audioModem"

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method
