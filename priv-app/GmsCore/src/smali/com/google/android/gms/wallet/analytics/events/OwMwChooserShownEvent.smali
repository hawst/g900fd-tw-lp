.class public Lcom/google/android/gms/wallet/analytics/events/OwMwChooserShownEvent;
.super Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/n;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/analytics/events/n;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/analytics/events/OwMwChooserShownEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;-><init>(Landroid/os/Parcel;)V

    .line 34
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/OwMwChooserShownEvent;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/gms/wallet/analytics/events/OwMwChooserShownEvent;->j:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/OwMwChooserShownEvent;

    invoke-direct {v0, p1}, Lcom/google/android/gms/wallet/analytics/events/OwMwChooserShownEvent;-><init>(Ljava/lang/String;)V

    .line 39
    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 40
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/analytics/events/OwMwChooserShownEvent;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method
