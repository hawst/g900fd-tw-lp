.class public abstract Lcom/google/android/gms/games/ui/card/b;
.super Lcom/google/android/gms/games/ui/d;
.source "SourceFile"


# static fields
.field private static final h:I

.field private static final i:I

.field private static final j:I

.field private static final k:I

.field private static final l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    sget v0, Lcom/google/android/gms/l;->bk:I

    sput v0, Lcom/google/android/gms/games/ui/card/b;->h:I

    .line 31
    sget v0, Lcom/google/android/gms/l;->bl:I

    sput v0, Lcom/google/android/gms/games/ui/card/b;->i:I

    .line 32
    sget v0, Lcom/google/android/gms/l;->bi:I

    sput v0, Lcom/google/android/gms/games/ui/card/b;->j:I

    .line 33
    sget v0, Lcom/google/android/gms/l;->bj:I

    sput v0, Lcom/google/android/gms/games/ui/card/b;->k:I

    .line 34
    sget v0, Lcom/google/android/gms/l;->bm:I

    sput v0, Lcom/google/android/gms/games/ui/card/b;->l:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/d;-><init>(Landroid/content/Context;)V

    .line 47
    return-void
.end method


# virtual methods
.method protected abstract b(Landroid/view/View;)Lcom/google/android/gms/games/ui/card/c;
.end method

.method protected final b(Landroid/view/ViewGroup;I)Lcom/google/android/gms/games/ui/f;
    .locals 4

    .prologue
    .line 51
    shr-int/lit8 v0, p2, 0x10

    .line 53
    packed-switch v0, :pswitch_data_0

    .line 75
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown card type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 55
    :pswitch_0
    sget v0, Lcom/google/android/gms/games/ui/card/b;->h:I

    .line 77
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/b;->e:Landroid/view/LayoutInflater;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/b;->b(Landroid/view/View;)Lcom/google/android/gms/games/ui/card/c;

    move-result-object v0

    return-object v0

    .line 59
    :pswitch_1
    sget v0, Lcom/google/android/gms/games/ui/card/b;->i:I

    goto :goto_0

    .line 63
    :pswitch_2
    sget v0, Lcom/google/android/gms/games/ui/card/b;->j:I

    goto :goto_0

    .line 67
    :pswitch_3
    sget v0, Lcom/google/android/gms/games/ui/card/b;->k:I

    goto :goto_0

    .line 71
    :pswitch_4
    sget v0, Lcom/google/android/gms/games/ui/card/b;->l:I

    goto :goto_0

    .line 53
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected final h()I
    .locals 2

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/card/b;->w()I

    move-result v0

    const v1, 0xffff

    and-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/card/b;->x()I

    move-result v1

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    return v0
.end method

.method public final r()I
    .locals 4

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/card/b;->x()I

    move-result v0

    .line 86
    packed-switch v0, :pswitch_data_0

    .line 106
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown card type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 88
    :pswitch_0
    sget v0, Lcom/google/android/gms/k;->k:I

    .line 108
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/b;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0

    .line 92
    :pswitch_1
    sget v0, Lcom/google/android/gms/k;->l:I

    goto :goto_0

    .line 96
    :pswitch_2
    sget v0, Lcom/google/android/gms/k;->j:I

    goto :goto_0

    .line 102
    :pswitch_3
    sget v0, Lcom/google/android/gms/k;->m:I

    goto :goto_0

    .line 86
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final u()I
    .locals 1

    .prologue
    .line 127
    sget v0, Lcom/google/android/gms/g;->ak:I

    return v0
.end method

.method protected abstract w()I
.end method

.method protected abstract x()I
.end method
