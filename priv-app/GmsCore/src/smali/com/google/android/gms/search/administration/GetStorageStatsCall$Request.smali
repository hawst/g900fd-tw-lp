.class public Lcom/google/android/gms/search/administration/GetStorageStatsCall$Request;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/administration/b;


# instance fields
.field final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/google/android/gms/search/administration/b;

    invoke-direct {v0}, Lcom/google/android/gms/search/administration/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Request;->CREATOR:Lcom/google/android/gms/search/administration/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Request;->a:I

    .line 53
    return-void
.end method

.method constructor <init>(I)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput p1, p0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Request;->a:I

    .line 68
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Request;->CREATOR:Lcom/google/android/gms/search/administration/b;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Request;->CREATOR:Lcom/google/android/gms/search/administration/b;

    invoke-static {p0, p1}, Lcom/google/android/gms/search/administration/b;->a(Lcom/google/android/gms/search/administration/GetStorageStatsCall$Request;Landroid/os/Parcel;)V

    .line 80
    return-void
.end method
