.class final Lcom/google/android/gms/cast/c/at;
.super Lcom/google/android/gms/cast/c/ap;
.source "SourceFile"


# instance fields
.field a:I

.field d:[Ljava/lang/String;

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>([Ljava/lang/String;Lcom/google/android/gms/cast/c/am;)V
    .locals 1

    .prologue
    .line 27
    const/16 v0, 0x21

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/gms/cast/c/ap;-><init>([Ljava/lang/String;ILcom/google/android/gms/cast/c/am;)V

    .line 28
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/cast/c/am;)V
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p1}, Lcom/google/android/gms/cast/c/am;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/cast/c/at;->e:I

    .line 97
    invoke-virtual {p1}, Lcom/google/android/gms/cast/c/am;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/cast/c/at;->f:I

    .line 98
    invoke-virtual {p1}, Lcom/google/android/gms/cast/c/am;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/cast/c/at;->a:I

    .line 99
    invoke-virtual {p1}, Lcom/google/android/gms/cast/c/am;->e()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/c/at;->d:[Ljava/lang/String;

    .line 100
    return-void
.end method

.method protected final a(Lcom/google/android/gms/cast/c/an;)V
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/google/android/gms/cast/c/at;->e:I

    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/c/an;->b(I)V

    .line 105
    iget v0, p0, Lcom/google/android/gms/cast/c/at;->f:I

    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/c/an;->b(I)V

    .line 106
    iget v0, p0, Lcom/google/android/gms/cast/c/at;->a:I

    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/c/an;->b(I)V

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/cast/c/at;->d:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/c/an;->a([Ljava/lang/String;)V

    .line 108
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/cast/c/at;->b:[Ljava/lang/String;

    array-length v0, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    .line 65
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/at;->b:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/cast/c/at;->b:[Ljava/lang/String;

    array-length v0, v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    .line 73
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/at;->b:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 112
    const-string v0, "SRV: %s:%d (prio=%d, weight=%d)"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/cast/c/at;->d:[Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/cast/c/at;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/gms/cast/c/at;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/gms/cast/c/at;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/gms/cast/c/at;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
