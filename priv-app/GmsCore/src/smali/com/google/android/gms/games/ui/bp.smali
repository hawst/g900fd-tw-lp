.class final Lcom/google/android/gms/games/ui/bp;
.super Lcom/google/android/gms/games/ui/cx;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final k:Landroid/view/View;

.field private final n:Landroid/widget/TextView;

.field private final o:Landroid/widget/TextView;

.field private final p:Landroid/view/View;

.field private final q:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 194
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/cx;-><init>(Landroid/view/View;)V

    .line 196
    sget v0, Lcom/google/android/gms/j;->dB:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/bp;->k:Landroid/view/View;

    .line 197
    sget v0, Lcom/google/android/gms/j;->jH:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/bp;->n:Landroid/widget/TextView;

    .line 198
    sget v0, Lcom/google/android/gms/j;->jF:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/bp;->o:Landroid/widget/TextView;

    .line 199
    sget v0, Lcom/google/android/gms/j;->jy:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/bp;->p:Landroid/view/View;

    .line 200
    sget v0, Lcom/google/android/gms/j;->jz:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/bp;->q:Landroid/widget/TextView;

    .line 201
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/ac;I)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 205
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/cx;->a(Lcom/google/android/gms/games/ui/ac;I)V

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bp;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/bn;

    .line 209
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bp;->n:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/bn;->a(Lcom/google/android/gms/games/ui/bn;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    invoke-static {v0}, Lcom/google/android/gms/games/ui/bn;->b(Lcom/google/android/gms/games/ui/bn;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 212
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bp;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 213
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bp;->o:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/bn;->b(Lcom/google/android/gms/games/ui/bn;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bp;->k:Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 219
    invoke-static {v0}, Lcom/google/android/gms/games/ui/bn;->c(Lcom/google/android/gms/games/ui/bn;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 220
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bp;->q:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 221
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bp;->q:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/bn;->d(Lcom/google/android/gms/games/ui/bn;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 223
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bp;->p:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 224
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bp;->q:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/bn;->e(Lcom/google/android/gms/games/ui/bn;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bp;->q:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/bn;->f(Lcom/google/android/gms/games/ui/bn;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 231
    :goto_1
    return-void

    .line 215
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bp;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 227
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bp;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setClickable(Z)V

    .line 229
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bp;->p:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bp;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/bn;

    .line 240
    invoke-static {v0}, Lcom/google/android/gms/games/ui/bn;->g(Lcom/google/android/gms/games/ui/bn;)Lcom/google/android/gms/games/ui/bo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/games/ui/bn;->c(Lcom/google/android/gms/games/ui/bn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 241
    invoke-static {v0}, Lcom/google/android/gms/games/ui/bn;->g(Lcom/google/android/gms/games/ui/bn;)Lcom/google/android/gms/games/ui/bo;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/bn;->d(Lcom/google/android/gms/games/ui/bn;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/bo;->a(Ljava/lang/String;)V

    .line 243
    :cond_0
    return-void
.end method
