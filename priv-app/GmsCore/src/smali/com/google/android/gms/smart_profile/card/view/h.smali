.class public final Lcom/google/android/gms/smart_profile/card/view/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/smart_profile/card/view/g;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    new-instance v0, Lcom/google/android/gms/smart_profile/card/view/g;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/smart_profile/card/view/g;-><init>(B)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    .line 82
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/smart_profile/card/view/h;
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/smart_profile/card/view/g;->o:Z

    .line 156
    return-object p0
.end method

.method public final a(Landroid/content/Intent;)Lcom/google/android/gms/smart_profile/card/view/h;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    iput-object p1, v0, Lcom/google/android/gms/smart_profile/card/view/g;->h:Landroid/content/Intent;

    .line 121
    return-object p0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/smart_profile/card/view/h;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    iput-object p1, v0, Lcom/google/android/gms/smart_profile/card/view/g;->a:Landroid/graphics/drawable/Drawable;

    .line 86
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/smart_profile/card/view/h;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    iput-object p1, v0, Lcom/google/android/gms/smart_profile/card/view/g;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    .line 126
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    iput-object p1, v0, Lcom/google/android/gms/smart_profile/card/view/g;->c:Ljava/lang/String;

    .line 96
    return-object p0
.end method

.method public final b(Landroid/content/Intent;)Lcom/google/android/gms/smart_profile/card/view/h;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    iput-object p1, v0, Lcom/google/android/gms/smart_profile/card/view/g;->l:Landroid/content/Intent;

    .line 141
    return-object p0
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/smart_profile/card/view/h;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    iput-object p1, v0, Lcom/google/android/gms/smart_profile/card/view/g;->k:Landroid/graphics/drawable/Drawable;

    .line 136
    return-object p0
.end method

.method public final b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/smart_profile/card/view/h;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    iput-object p1, v0, Lcom/google/android/gms/smart_profile/card/view/g;->m:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    .line 146
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    iput-object p1, v0, Lcom/google/android/gms/smart_profile/card/view/g;->d:Ljava/lang/String;

    .line 101
    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    iput-object p1, v0, Lcom/google/android/gms/smart_profile/card/view/g;->f:Ljava/lang/String;

    .line 111
    return-object p0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    iput-object p1, v0, Lcom/google/android/gms/smart_profile/card/view/g;->p:Ljava/lang/String;

    .line 161
    return-object p0
.end method

.method public final e(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    iput-object p1, v0, Lcom/google/android/gms/smart_profile/card/view/g;->q:Ljava/lang/String;

    .line 167
    return-object p0
.end method
