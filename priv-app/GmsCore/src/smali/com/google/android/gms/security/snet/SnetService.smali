.class public Lcom/google/android/gms/security/snet/SnetService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Z

.field private c:Lcom/google/android/gms/security/snet/a;

.field private d:Lcom/google/android/gms/security/snet/g;

.field private e:I

.field private f:Lcom/google/android/gms/security/snet/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/gms/security/snet/SnetService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/security/snet/SnetService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/security/snet/SnetService;->b:Z

    .line 41
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/security/snet/SnetService;)Lcom/google/android/gms/security/snet/f;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/gms/security/snet/SnetService;->f:Lcom/google/android/gms/security/snet/f;

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/gms/security/snet/SnetService;->f:Lcom/google/android/gms/security/snet/f;

    invoke-virtual {v0}, Lcom/google/android/gms/security/snet/f;->c()V

    .line 151
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/security/snet/SnetService;->b:Z

    .line 152
    invoke-virtual {p0}, Lcom/google/android/gms/security/snet/SnetService;->stopSelf()V

    .line 153
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/security/snet/SnetService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/gms/security/snet/SnetService;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 133
    new-instance v0, Lcom/google/android/gms/security/snet/l;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/security/snet/l;-><init>(Lcom/google/android/gms/security/snet/SnetService;Ljava/lang/String;)V

    .line 134
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/security/snet/l;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 135
    return-void
.end method

.method static a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 161
    invoke-static {p0}, Lcom/google/android/gms/security/a/a;->a(Landroid/content/Context;)V

    .line 162
    sget-object v0, Lcom/google/android/gms/security/a/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private b()I
    .locals 4

    .prologue
    .line 170
    const/4 v0, -0x1

    .line 171
    invoke-virtual {p0}, Lcom/google/android/gms/security/snet/SnetService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 173
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/security/snet/SnetService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v0, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/security/snet/SnetService;)Lcom/google/android/gms/security/snet/a;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/gms/security/snet/SnetService;->c:Lcom/google/android/gms/security/snet/a;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/security/snet/SnetService;)I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/google/android/gms/security/snet/SnetService;->e:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/security/snet/SnetService;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/gms/security/snet/SnetService;->a()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 90
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 91
    invoke-static {p0}, Lcom/google/android/gms/security/a/a;->a(Landroid/content/Context;)V

    .line 92
    sget-object v0, Lcom/google/android/gms/security/a/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 93
    invoke-direct {p0}, Lcom/google/android/gms/security/snet/SnetService;->b()I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/security/snet/SnetService;->e:I

    .line 94
    new-instance v1, Lcom/google/android/gms/security/snet/k;

    iget v2, p0, Lcom/google/android/gms/security/snet/SnetService;->e:I

    invoke-direct {v1, p0, p0, v0, v2}, Lcom/google/android/gms/security/snet/k;-><init>(Lcom/google/android/gms/security/snet/SnetService;Landroid/content/Context;Ljava/lang/String;I)V

    iput-object v1, p0, Lcom/google/android/gms/security/snet/SnetService;->d:Lcom/google/android/gms/security/snet/g;

    .line 100
    new-instance v0, Lcom/google/android/gms/security/snet/a;

    iget v1, p0, Lcom/google/android/gms/security/snet/SnetService;->e:I

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/security/snet/a;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/gms/security/snet/SnetService;->c:Lcom/google/android/gms/security/snet/a;

    .line 101
    new-instance v0, Lcom/google/android/gms/security/snet/f;

    iget-object v1, p0, Lcom/google/android/gms/security/snet/SnetService;->c:Lcom/google/android/gms/security/snet/a;

    iget v2, p0, Lcom/google/android/gms/security/snet/SnetService;->e:I

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/security/snet/f;-><init>(Landroid/content/Context;Lcom/google/android/gms/security/snet/a;I)V

    iput-object v0, p0, Lcom/google/android/gms/security/snet/SnetService;->f:Lcom/google/android/gms/security/snet/f;

    .line 102
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/gms/security/snet/SnetService;->d:Lcom/google/android/gms/security/snet/g;

    iget-object v0, v0, Lcom/google/android/gms/security/snet/g;->a:Lcom/android/volley/s;

    invoke-virtual {v0}, Lcom/android/volley/s;->b()V

    .line 140
    sget-object v0, Lcom/google/android/gms/security/snet/SnetService;->a:Ljava/lang/String;

    const-string v1, "snet destroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 142
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/google/android/gms/security/snet/SnetService;->b:Z

    if-nez v0, :cond_3

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/security/snet/SnetService;->b:Z

    .line 108
    new-instance v0, Lcom/google/android/gms/security/snet/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/security/snet/e;-><init>(Landroid/content/Context;)V

    .line 109
    invoke-static {p0}, Lcom/google/android/gms/security/snet/SnetService;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/security/snet/e;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/google/android/gms/security/a/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/security/a/a;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 111
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/security/snet/SnetAlarmService;->a(Landroid/content/Context;J)V

    .line 112
    sget-object v0, Lcom/google/android/gms/security/a/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/security/snet/SnetService;->c:Lcom/google/android/gms/security/snet/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/security/snet/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/security/snet/SnetService;->c:Lcom/google/android/gms/security/snet/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/security/snet/a;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/security/snet/SnetService;->d:Lcom/google/android/gms/security/snet/g;

    invoke-virtual {v0}, Lcom/google/android/gms/security/snet/g;->a()V

    .line 119
    :goto_0
    const/4 v0, 0x2

    return v0

    .line 112
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/security/snet/SnetService;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 114
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/security/snet/SnetService;->a()V

    goto :goto_0

    .line 117
    :cond_3
    sget-object v0, Lcom/google/android/gms/security/snet/SnetService;->a:Ljava/lang/String;

    const-string v1, "snet re-entered."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
