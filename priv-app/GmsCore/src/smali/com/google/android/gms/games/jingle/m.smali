.class public final Lcom/google/android/gms/games/jingle/m;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 97
    packed-switch p0, :pswitch_data_0

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UNKNOWN-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 99
    :pswitch_0
    const-string v0, "STATE_INIT"

    goto :goto_0

    .line 101
    :pswitch_1
    const-string v0, "STATE_SENTINITIATE"

    goto :goto_0

    .line 103
    :pswitch_2
    const-string v0, "STATE_RECEIVEDINITIATE"

    goto :goto_0

    .line 105
    :pswitch_3
    const-string v0, "STATE_SENTPRACCEPT"

    goto :goto_0

    .line 107
    :pswitch_4
    const-string v0, "STATE_SENTACCEPT"

    goto :goto_0

    .line 109
    :pswitch_5
    const-string v0, "STATE_RECEIVEDPRACCEPT"

    goto :goto_0

    .line 111
    :pswitch_6
    const-string v0, "STATE_RECEIVEDACCEPT"

    goto :goto_0

    .line 113
    :pswitch_7
    const-string v0, "STATE_SENTMODIFY"

    goto :goto_0

    .line 115
    :pswitch_8
    const-string v0, "STATE_RECEIVEDMODIFY"

    goto :goto_0

    .line 117
    :pswitch_9
    const-string v0, "STATE_SENTREJECT"

    goto :goto_0

    .line 119
    :pswitch_a
    const-string v0, "STATE_RECEIVEDREJECT"

    goto :goto_0

    .line 121
    :pswitch_b
    const-string v0, "STATE_SENTREDIRECT"

    goto :goto_0

    .line 123
    :pswitch_c
    const-string v0, "STATE_SENTTERMINATE"

    goto :goto_0

    .line 125
    :pswitch_d
    const-string v0, "STATE_RECEIVEDTERMINATE"

    goto :goto_0

    .line 127
    :pswitch_e
    const-string v0, "STATE_INPROGRESS"

    goto :goto_0

    .line 129
    :pswitch_f
    const-string v0, "STATE_DEINIT"

    goto :goto_0

    .line 97
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method
