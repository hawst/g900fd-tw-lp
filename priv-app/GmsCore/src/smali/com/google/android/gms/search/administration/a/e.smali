.class public abstract Lcom/google/android/gms/search/administration/a/e;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/search/administration/a/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 18
    const-string v0, "com.google.android.gms.search.administration.internal.ISearchAdministrationService"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/search/administration/a/e;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 19
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 65
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 45
    :sswitch_0
    const-string v0, "com.google.android.gms.search.administration.internal.ISearchAdministrationService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v3

    .line 46
    goto :goto_0

    .line 50
    :sswitch_1
    const-string v0, "com.google.android.gms.search.administration.internal.ISearchAdministrationService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    sget-object v0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Request;->CREATOR:Lcom/google/android/gms/search/administration/b;

    invoke-static {p2}, Lcom/google/android/gms/search/administration/b;->a(Landroid/os/Parcel;)Lcom/google/android/gms/search/administration/GetStorageStatsCall$Request;

    move-result-object v0

    move-object v1, v0

    .line 59
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    if-nez v4, :cond_1

    .line 60
    :goto_2
    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/search/administration/a/e;->a(Lcom/google/android/gms/search/administration/GetStorageStatsCall$Request;Lcom/google/android/gms/search/administration/a/a;)V

    .line 61
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v3

    .line 62
    goto :goto_0

    :cond_0
    move-object v1, v2

    .line 56
    goto :goto_1

    .line 59
    :cond_1
    const-string v0, "com.google.android.gms.search.administration.internal.ISearchAdministrationCallbacks"

    invoke-interface {v4, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v2, v0, Lcom/google/android/gms/search/administration/a/a;

    if-eqz v2, :cond_2

    check-cast v0, Lcom/google/android/gms/search/administration/a/a;

    move-object v2, v0

    goto :goto_2

    :cond_2
    new-instance v2, Lcom/google/android/gms/search/administration/a/c;

    invoke-direct {v2, v4}, Lcom/google/android/gms/search/administration/a/c;-><init>(Landroid/os/IBinder;)V

    goto :goto_2

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
