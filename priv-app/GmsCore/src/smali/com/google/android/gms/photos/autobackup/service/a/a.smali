.class public Lcom/google/android/gms/photos/autobackup/service/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/photos/autobackup/a;

.field private final c:Lcom/google/android/libraries/social/account/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/service/a/a;->a:Landroid/content/Context;

    .line 35
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/a;->a:Landroid/content/Context;

    const-class v1, Lcom/google/android/gms/photos/autobackup/a;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/a;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/a;->b:Lcom/google/android/gms/photos/autobackup/a;

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/a;->a:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/account/b;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/a;->c:Lcom/google/android/libraries/social/account/b;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;
    .locals 11

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/service/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You cannot migrate settings after they have already been migrated!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_0
    if-nez p1, :cond_1

    .line 61
    const/4 v0, 0x0

    .line 106
    :goto_0
    return-object v0

    .line 64
    :cond_1
    const-string v0, "is_enabled"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 65
    const-string v0, "account_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 66
    const-string v0, "wifi_only"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 67
    const-string v0, "roaming_upload"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 68
    const-string v0, "charing_only"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 69
    const-string v0, "wifi_only_video"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    .line 70
    const-string v0, "upload_full_resolution"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    .line 73
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/a;->b:Lcom/google/android/gms/photos/autobackup/a;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/photos/autobackup/a;->a(Ljava/lang/String;)I

    move-result v0

    .line 75
    if-eqz v2, :cond_3

    .line 76
    const-string v1, "obfuscated_gaia_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 77
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 78
    iget-object v9, p0, Lcom/google/android/gms/photos/autobackup/service/a/a;->c:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v9, v0}, Lcom/google/android/libraries/social/account/b;->b(I)Lcom/google/android/libraries/social/account/d;

    move-result-object v9

    const-string v10, "gaia_id"

    invoke-interface {v9, v10, v1}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/libraries/social/account/d;->c()I

    .line 84
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/service/a/a;->b:Lcom/google/android/gms/photos/autobackup/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/photos/autobackup/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 87
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/service/a/a;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/libraries/social/autobackup/s;->a(Landroid/content/Context;I)V

    .line 90
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/service/a/a;->a:Landroid/content/Context;

    invoke-static {v1, v0, v8}, Lcom/google/android/libraries/social/autobackup/s;->a(Landroid/content/Context;IZ)V

    .line 93
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/a;->a:Landroid/content/Context;

    invoke-static {v0, v4}, Lcom/google/android/libraries/social/autobackup/s;->a(Landroid/content/Context;Z)V

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/a;->a:Landroid/content/Context;

    invoke-static {v0, v7}, Lcom/google/android/libraries/social/autobackup/s;->b(Landroid/content/Context;Z)V

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/a;->a:Landroid/content/Context;

    invoke-static {v0, v5}, Lcom/google/android/libraries/social/autobackup/s;->c(Landroid/content/Context;Z)V

    .line 96
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/service/a/a;->a:Landroid/content/Context;

    if-nez v6, :cond_5

    const/4 v0, 0x1

    :goto_1
    invoke-static {v1, v0}, Lcom/google/android/libraries/social/autobackup/s;->d(Landroid/content/Context;Z)V

    .line 98
    const-string v0, "folders_excluded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 99
    if-eqz v1, :cond_6

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/a;->a:Landroid/content/Context;

    const-class v9, Lcom/google/android/libraries/social/autobackup/ac;

    invoke-static {v0, v9}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/ac;

    .line 101
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 102
    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/autobackup/ac;->b(Ljava/lang/String;)V

    goto :goto_2

    .line 96
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 106
    :cond_6
    new-instance v0, Lcom/google/android/gms/photos/autobackup/model/a;

    invoke-direct {v0, v3}, Lcom/google/android/gms/photos/autobackup/model/a;-><init>(Ljava/lang/String;)V

    iput-boolean v2, v0, Lcom/google/android/gms/photos/autobackup/model/a;->a:Z

    iput-boolean v4, v0, Lcom/google/android/gms/photos/autobackup/model/a;->b:Z

    iput-boolean v8, v0, Lcom/google/android/gms/photos/autobackup/model/a;->f:Z

    iput-boolean v5, v0, Lcom/google/android/gms/photos/autobackup/model/a;->c:Z

    iput-boolean v6, v0, Lcom/google/android/gms/photos/autobackup/model/a;->d:Z

    iput-boolean v7, v0, Lcom/google/android/gms/photos/autobackup/model/a;->e:Z

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/model/a;->a()Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/a;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "com.google.android.gms.photos.autobackup.service.internal.AutoBackupSettingsMigrator.IS_MIGRATED_KEY"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 48
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/a;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "com.google.android.gms.photos.autobackup.service.internal.AutoBackupSettingsMigrator.IS_MIGRATED_KEY"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
