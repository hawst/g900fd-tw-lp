.class public final Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/drive/ui/picker/view/h;


# instance fields
.field private final b:Ljava/util/Map;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/drive/g/n;

.field private final e:Lcom/google/android/gms/drive/g/i;

.field private final f:Lcom/google/android/gms/drive/g/ae;

.field private final g:Ljava/util/concurrent/ExecutorService;

.field private final h:Ljava/util/concurrent/ExecutorService;

.field private final i:Lcom/google/android/gms/drive/e/b;

.field private volatile j:Lcom/google/android/gms/drive/metadata/sync/syncadapter/u;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 156
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/view/h;->c:Lcom/google/android/gms/drive/ui/picker/view/h;

    sput-object v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a:Lcom/google/android/gms/drive/ui/picker/view/h;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/g/n;Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/drive/g/ae;Lcom/google/android/gms/drive/e/b;)V
    .locals 1

    .prologue
    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->b:Ljava/util/Map;

    .line 191
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/drive/g/u;->a(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->h:Ljava/util/concurrent/ExecutorService;

    .line 200
    iput-object p1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->c:Landroid/content/Context;

    .line 201
    iput-object p2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->d:Lcom/google/android/gms/drive/g/n;

    .line 202
    iput-object p3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->e:Lcom/google/android/gms/drive/g/i;

    .line 203
    iput-object p4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->f:Lcom/google/android/gms/drive/g/ae;

    .line 204
    sget-object v0, Lcom/google/android/gms/drive/ai;->Y:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 205
    invoke-static {v0}, Lcom/google/android/gms/drive/g/u;->a(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->g:Ljava/util/concurrent/ExecutorService;

    .line 206
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/e/b;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->i:Lcom/google/android/gms/drive/e/b;

    .line 207
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->c:Landroid/content/Context;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/a/d;Lcom/google/android/gms/common/a/d;)Lcom/google/android/gms/drive/g/ad;
    .locals 6

    .prologue
    .line 213
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 215
    new-instance v0, Lcom/google/android/gms/drive/g/ad;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->e:Lcom/google/android/gms/drive/g/i;

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->f:Lcom/google/android/gms/drive/g/ae;

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/g/ad;-><init>(Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/drive/g/ae;Ljava/lang/String;Lcom/google/android/gms/common/a/d;Lcom/google/android/gms/common/a/d;)V

    .line 217
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->j:Lcom/google/android/gms/drive/metadata/sync/syncadapter/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->j:Lcom/google/android/gms/drive/metadata/sync/syncadapter/u;

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/u;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;Ljava/lang/String;Landroid/content/SyncResult;)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->j:Lcom/google/android/gms/drive/metadata/sync/syncadapter/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->j:Lcom/google/android/gms/drive/metadata/sync/syncadapter/u;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/u;->a(Ljava/lang/String;Landroid/content/SyncResult;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;Ljava/lang/String;Lcom/google/android/gms/drive/ui/picker/view/h;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Ljava/lang/String;Lcom/google/android/gms/drive/ui/picker/view/h;)V

    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;Lcom/google/android/gms/drive/ui/picker/view/h;)V
    .locals 1

    .prologue
    .line 238
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->d(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;

    move-result-object v0

    iput-object p2, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;->e:Lcom/google/android/gms/drive/ui/picker/view/h;

    .line 241
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242
    monitor-exit p0

    return-void

    .line 238
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;)Lcom/google/android/gms/drive/e/b;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->i:Lcom/google/android/gms/drive/e/b;

    return-object v0
.end method

.method static synthetic b()Lcom/google/android/gms/drive/ui/picker/view/h;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a:Lcom/google/android/gms/drive/ui/picker/view/h;

    return-object v0
.end method

.method private declared-synchronized c(Ljava/lang/String;)Lcom/google/android/gms/drive/ui/picker/view/h;
    .locals 1

    .prologue
    .line 233
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->d(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;->e:Lcom/google/android/gms/drive/ui/picker/view/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 233
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;)Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->g:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method private declared-synchronized d(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;
    .locals 5

    .prologue
    .line 270
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;

    .line 271
    if-nez v0, :cond_0

    .line 272
    const-string v0, "SyncScheduler.rateLimiter."

    sget-object v1, Lcom/google/android/gms/drive/ai;->Z:Lcom/google/android/gms/common/a/d;

    sget-object v2, Lcom/google/android/gms/drive/ai;->aa:Lcom/google/android/gms/common/a/d;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/a/d;Lcom/google/android/gms/common/a/d;)Lcom/google/android/gms/drive/g/ad;

    move-result-object v1

    .line 277
    const-string v0, "SyncScheduler.firstPartyRateLimiter."

    sget-object v2, Lcom/google/android/gms/drive/ai;->ab:Lcom/google/android/gms/common/a/d;

    sget-object v3, Lcom/google/android/gms/drive/ai;->ac:Lcom/google/android/gms/common/a/d;

    invoke-direct {p0, p1, v0, v2, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/a/d;Lcom/google/android/gms/common/a/d;)Lcom/google/android/gms/drive/g/ad;

    move-result-object v2

    .line 282
    const-string v0, "SyncScheduler.onConnectRateLimiter."

    sget-object v3, Lcom/google/android/gms/drive/ai;->ad:Lcom/google/android/gms/common/a/d;

    sget-object v4, Lcom/google/android/gms/drive/ai;->ae:Lcom/google/android/gms/common/a/d;

    invoke-direct {p0, p1, v0, v3, v4}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/a/d;Lcom/google/android/gms/common/a/d;)Lcom/google/android/gms/drive/g/ad;

    move-result-object v3

    .line 287
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;-><init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;Lcom/google/android/gms/drive/g/at;Lcom/google/android/gms/drive/g/at;Lcom/google/android/gms/drive/g/at;)V

    .line 288
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 290
    :cond_0
    monitor-exit p0

    return-object v0

    .line 270
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 487
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/ui/picker/view/h;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/view/h;->c:Lcom/google/android/gms/drive/ui/picker/view/h;

    if-ne v0, v1, :cond_1

    .line 488
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->d(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;

    move-result-object v1

    .line 489
    iget-object v0, v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;->f:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 490
    iget-object v0, v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 491
    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->h:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v3, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 487
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 493
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;->f:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 496
    :cond_1
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;II)I
    .locals 7

    .prologue
    const/4 v1, 0x2

    const/4 v6, 0x0

    const/4 v0, 0x1

    .line 304
    sget-object v2, Lcom/google/android/gms/drive/api/e;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_0

    .line 306
    const/4 v0, 0x5

    .line 418
    :goto_0
    return v0

    .line 308
    :cond_0
    monitor-enter p0

    .line 309
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->c:Landroid/content/Context;

    invoke-static {v2, p1}, Lcom/google/android/gms/drive/g/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v2

    .line 311
    if-nez v2, :cond_1

    .line 312
    const-string v0, "SyncScheduler"

    const-string v1, "%s account does not exist."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "Ignoring sync request: "

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 313
    const/4 v0, 0x4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 359
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 315
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->d:Lcom/google/android/gms/drive/g/n;

    invoke-interface {v2}, Lcom/google/android/gms/drive/g/n;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 316
    const-string v0, "SyncScheduler"

    const-string v1, "%s device offline."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "Ignoring sync request: "

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 317
    const/4 v0, 0x3

    monitor-exit p0

    goto :goto_0

    .line 320
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->d(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;

    move-result-object v2

    .line 321
    iget-object v3, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;->e:Lcom/google/android/gms/drive/ui/picker/view/h;

    sget-object v4, Lcom/google/android/gms/drive/ui/picker/view/h;->c:Lcom/google/android/gms/drive/ui/picker/view/h;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/ui/picker/view/h;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 322
    const-string v1, "SyncScheduler"

    const-string v2, "%s already syncing."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "Ignoring sync request: "

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 323
    monitor-exit p0

    goto :goto_0

    .line 336
    :cond_3
    const/16 v0, 0x67

    if-ne p2, v0, :cond_6

    iget-object v0, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;->b:Lcom/google/android/gms/drive/g/at;

    .line 338
    :goto_1
    invoke-interface {v0}, Lcom/google/android/gms/drive/g/at;->d()Z

    move-result v0

    .line 339
    iget-object v2, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;->a:Lcom/google/android/gms/drive/g/at;

    invoke-interface {v2}, Lcom/google/android/gms/drive/g/at;->d()Z

    move-result v2

    .line 341
    const/16 v3, 0x66

    if-ne p2, v3, :cond_7

    .line 342
    if-eqz v0, :cond_4

    if-nez v2, :cond_5

    .line 343
    :cond_4
    const-string v0, "SyncScheduler"

    const-string v1, "No token available but proceeding anyway with explicit sync"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    :cond_5
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/view/h;->a:Lcom/google/android/gms/drive/ui/picker/view/h;

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Ljava/lang/String;Lcom/google/android/gms/drive/ui/picker/view/h;)V

    .line 359
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 362
    :try_start_2
    new-instance v4, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;

    invoke-direct {v4, p0, p1, p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;-><init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;Ljava/lang/String;I)V

    .line 411
    new-instance v3, Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->c:Landroid/content/Context;

    const-class v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/MetadataSyncService;

    invoke-direct {v3, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 412
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->c:Landroid/content/Context;

    const-string v2, "SyncScheduler"

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    move v0, v6

    .line 418
    goto/16 :goto_0

    .line 336
    :cond_6
    :try_start_3
    iget-object v0, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;->c:Lcom/google/android/gms/drive/g/at;

    goto :goto_1

    .line 346
    :cond_7
    if-nez v0, :cond_8

    .line 347
    const-string v0, "SyncScheduler"

    const-string v2, "%s rate limited (normal limit)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "Ignoring sync request: "

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 348
    monitor-exit p0

    move v0, v1

    goto/16 :goto_0

    .line 349
    :cond_8
    const/16 v0, 0x65

    if-ne p2, v0, :cond_5

    if-nez v2, :cond_5

    .line 351
    const-string v0, "SyncScheduler"

    const-string v2, "%s rate limited (on connection limit)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "Ignoring sync request: "

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 352
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    goto/16 :goto_0

    .line 414
    :catch_0
    move-exception v0

    .line 415
    sget-object v1, Lcom/google/android/gms/drive/ui/picker/view/h;->c:Lcom/google/android/gms/drive/ui/picker/view/h;

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Ljava/lang/String;Lcom/google/android/gms/drive/ui/picker/view/h;)V

    .line 416
    throw v0
.end method

.method public final a()Ljava/util/List;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->g:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/u;)V
    .locals 0

    .prologue
    .line 425
    iput-object p1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->j:Lcom/google/android/gms/drive/metadata/sync/syncadapter/u;

    .line 426
    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 459
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->d(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;

    move-result-object v0

    .line 460
    iget v1, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 461
    monitor-exit p0

    return-void

    .line 459
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 478
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->d(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;

    move-result-object v0

    .line 479
    iget-object v1, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;->f:Ljava/util/List;

    if-nez v1, :cond_0

    .line 480
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;->f:Ljava/util/List;

    .line 482
    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;->f:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 483
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484
    monitor-exit p0

    return-void

    .line 478
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lcom/google/android/gms/drive/ui/picker/view/h;Lcom/google/android/gms/drive/ui/picker/view/h;)Z
    .locals 6

    .prologue
    .line 255
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/ui/picker/view/h;

    move-result-object v0

    .line 259
    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/ui/picker/view/h;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 260
    if-eqz v1, :cond_0

    .line 261
    invoke-direct {p0, p1, p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Ljava/lang/String;Lcom/google/android/gms/drive/ui/picker/view/h;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 266
    :goto_0
    monitor-exit p0

    return v1

    .line 263
    :cond_0
    :try_start_1
    const-string v2, "SyncScheduler"

    const-string v3, "Current sync status %s != %s, not setting to %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object p2, v4, v0

    const/4 v0, 0x2

    aput-object p3, v4, v0

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 255
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 467
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->d(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;

    move-result-object v1

    .line 468
    iget v0, v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;->d:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Sync not started?"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 469
    iget v0, v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/t;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 470
    monitor-exit p0

    return-void

    .line 468
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 467
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
