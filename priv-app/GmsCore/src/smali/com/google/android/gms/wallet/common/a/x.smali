.class final Lcom/google/android/gms/wallet/common/a/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v0, -0x1

    const/4 v1, 0x1

    .line 23
    check-cast p1, Lcom/google/android/gms/wallet/common/a/w;

    check-cast p2, Lcom/google/android/gms/wallet/common/a/w;

    invoke-virtual {p1, v4}, Lcom/google/android/gms/wallet/common/a/w;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p2, v4}, Lcom/google/android/gms/wallet/common/a/w;->a(I)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p2, v4}, Lcom/google/android/gms/wallet/common/a/w;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v1}, Lcom/google/android/gms/wallet/common/a/w;->a(I)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p2, v1}, Lcom/google/android/gms/wallet/common/a/w;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/common/a/w;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/common/a/w;->a()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0

    :cond_3
    invoke-virtual {p2, v1}, Lcom/google/android/gms/wallet/common/a/w;->a(I)Z

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {p1, v3}, Lcom/google/android/gms/wallet/common/a/w;->a(I)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p2, v3}, Lcom/google/android/gms/wallet/common/a/w;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/common/a/w;->b()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/common/a/w;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    :cond_6
    invoke-virtual {p2, v3}, Lcom/google/android/gms/wallet/common/a/w;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    goto :goto_0
.end method
