.class public final Lcom/google/android/gms/drive/database/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/a/r;

.field private static final b:Lcom/google/android/gms/common/a/m;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 14
    new-instance v0, Lcom/google/android/gms/common/a/m;

    const-string v1, "database"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/a/m;-><init>(Ljava/lang/String;)V

    .line 19
    sput-object v0, Lcom/google/android/gms/drive/database/f;->b:Lcom/google/android/gms/common/a/m;

    const-string v1, "baseDatabaseSize"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/a/m;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/r;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/database/f;->a:Lcom/google/android/gms/common/a/r;

    return-void
.end method
