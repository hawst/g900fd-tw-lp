.class final Lcom/google/android/gms/plus/sharebox/v;
.super Landroid/widget/Filter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/sharebox/u;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/sharebox/u;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/v;->a:Lcom/google/android/gms/plus/sharebox/u;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/sharebox/u;B)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/sharebox/v;-><init>(Lcom/google/android/gms/plus/sharebox/u;)V

    return-void
.end method


# virtual methods
.method protected final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 3

    .prologue
    .line 38
    const/4 v0, 0x0

    .line 39
    if-eqz p1, :cond_0

    .line 40
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 41
    if-lez v1, :cond_0

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/aa;->a(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 42
    const/4 v0, 0x1

    invoke-interface {p1, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 46
    :cond_0
    new-instance v1, Landroid/widget/Filter$FilterResults;

    invoke-direct {v1}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 47
    iput-object v0, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 48
    return-object v1
.end method

.method protected final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    .prologue
    .line 53
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/v;->a:Lcom/google/android/gms/plus/sharebox/u;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/sharebox/u;->b(Ljava/lang/String;)V

    .line 54
    return-void
.end method
