.class public abstract Lcom/google/android/gms/security/snet/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/android/volley/s;

.field protected final b:Ljava/lang/String;

.field private final c:Landroid/content/Context;

.field private final d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/gms/security/snet/g;->c:Landroid/content/Context;

    .line 30
    invoke-static {p1}, Lcom/android/volley/toolbox/ac;->a(Landroid/content/Context;)Lcom/android/volley/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/snet/g;->a:Lcom/android/volley/s;

    .line 31
    iput-object p2, p0, Lcom/google/android/gms/security/snet/g;->b:Ljava/lang/String;

    .line 32
    iput p3, p0, Lcom/google/android/gms/security/snet/g;->d:I

    .line 33
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 40
    new-instance v4, Lcom/google/android/gms/security/snet/h;

    invoke-direct {v4, p0}, Lcom/google/android/gms/security/snet/h;-><init>(Lcom/google/android/gms/security/snet/g;)V

    .line 47
    new-instance v3, Lcom/google/android/gms/security/snet/i;

    invoke-direct {v3, p0}, Lcom/google/android/gms/security/snet/i;-><init>(Lcom/google/android/gms/security/snet/g;)V

    .line 53
    new-instance v0, Lcom/google/android/gms/security/snet/j;

    iget-object v1, p0, Lcom/google/android/gms/security/snet/g;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/security/snet/g;->b:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/gms/security/snet/g;->d:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/security/snet/j;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;I)V

    .line 55
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/security/snet/j;->a(Z)Lcom/android/volley/p;

    .line 56
    iget-object v1, p0, Lcom/google/android/gms/security/snet/g;->a:Lcom/android/volley/s;

    invoke-virtual {v1, v0}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    .line 57
    return-void
.end method

.method public abstract b()V
.end method
