.class public final enum Lcom/google/android/gms/drive/database/x;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/x;

.field public static final enum b:Lcom/google/android/gms/drive/database/x;

.field private static final synthetic c:[Lcom/google/android/gms/drive/database/x;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 81
    new-instance v0, Lcom/google/android/gms/drive/database/x;

    const-string v1, "AND"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    .line 82
    new-instance v0, Lcom/google/android/gms/drive/database/x;

    const-string v1, "OR"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/drive/database/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/database/x;->b:Lcom/google/android/gms/drive/database/x;

    .line 80
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/drive/database/x;

    sget-object v1, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/drive/database/x;->b:Lcom/google/android/gms/drive/database/x;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/gms/drive/database/x;->c:[Lcom/google/android/gms/drive/database/x;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/x;
    .locals 1

    .prologue
    .line 80
    const-class v0, Lcom/google/android/gms/drive/database/x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/x;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/x;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/google/android/gms/drive/database/x;->c:[Lcom/google/android/gms/drive/database/x;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/x;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/x;

    return-object v0
.end method


# virtual methods
.method public final varargs a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 4

    .prologue
    .line 88
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->d()Lcom/google/android/gms/drive/database/w;

    move-result-object v1

    .line 91
    array-length v2, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p2, v0

    .line 92
    invoke-virtual {v1, p0, v3}, Lcom/google/android/gms/drive/database/w;->a(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/w;

    .line 91
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 95
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/w;->a()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 99
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 100
    sget-object v0, Lcom/google/android/gms/drive/database/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    .line 111
    :cond_0
    :goto_0
    return-object v0

    .line 102
    :cond_1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    .line 103
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-eq v2, v1, :cond_0

    .line 106
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->d()Lcom/google/android/gms/drive/database/w;

    move-result-object v2

    .line 108
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 109
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-virtual {v2, p0, v0}, Lcom/google/android/gms/drive/database/w;->a(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/w;

    .line 108
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 111
    :cond_2
    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/w;->a()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    goto :goto_0
.end method
