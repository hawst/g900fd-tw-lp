.class public Lcom/google/android/gms/people/debug/PeopleExportActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static a:Ljava/lang/String;


# instance fields
.field private b:Landroid/widget/Button;

.field private c:Landroid/widget/Button;

.field private d:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-string v0, "PeopleExportActivity"

    sput-object v0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 88
    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/people/debug/PeopleExportActivity;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 38
    sget-object v0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->a:Ljava/lang/String;

    const-string v1, "Drafting email"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.extra.SUBJECT"

    sget v2, Lcom/google/android/gms/p;->rN:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    sget v2, Lcom/google/android/gms/p;->rL:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "application/zip"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    sget v1, Lcom/google/android/gms/p;->rM:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->d:Landroid/widget/Button;

    invoke-static {p0}, Lcom/google/android/gms/people/debug/a;->b(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 67
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 128
    invoke-direct {p0}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->b()V

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->b:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->c:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 131
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 71
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 72
    sget v1, Lcom/google/android/gms/j;->dl:I

    if-ne v0, v1, :cond_1

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->b:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->c:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 75
    new-instance v0, Lcom/google/android/gms/people/debug/b;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/people/debug/b;-><init>(Lcom/google/android/gms/people/debug/PeopleExportActivity;B)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/debug/b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    sget v1, Lcom/google/android/gms/j;->ed:I

    if-ne v0, v1, :cond_2

    .line 77
    invoke-static {p0}, Lcom/google/android/gms/people/debug/a;->c(Landroid/content/Context;)V

    .line 78
    invoke-direct {p0}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->b()V

    goto :goto_0

    .line 79
    :cond_2
    sget v1, Lcom/google/android/gms/j;->cq:I

    if-ne v0, v1, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 47
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->c(Landroid/app/Activity;)Ljava/lang/String;

    .line 52
    invoke-virtual {p0, v2}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->requestWindowFeature(I)Z

    .line 54
    sget v0, Lcom/google/android/gms/l;->cN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->setContentView(I)V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x1080027

    invoke-virtual {v0, v2, v1}, Landroid/view/Window;->setFeatureDrawableResource(II)V

    .line 59
    sget v0, Lcom/google/android/gms/j;->dl:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->b:Landroid/widget/Button;

    .line 60
    sget v0, Lcom/google/android/gms/j;->cq:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->c:Landroid/widget/Button;

    .line 61
    sget v0, Lcom/google/android/gms/j;->ed:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/people/debug/PeopleExportActivity;->d:Landroid/widget/Button;

    .line 62
    invoke-direct {p0}, Lcom/google/android/gms/people/debug/PeopleExportActivity;->b()V

    .line 63
    return-void
.end method
