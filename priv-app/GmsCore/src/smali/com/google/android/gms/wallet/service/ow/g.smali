.class public final Lcom/google/android/gms/wallet/service/ow/g;
.super Lcom/google/android/gms/wallet/service/ow/w;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/wallet/service/ow/w;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ow/w;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/ow/w;-><init>()V

    .line 22
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/g;->b:Landroid/content/Context;

    .line 23
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/g;->a:Lcom/google/android/gms/wallet/service/ow/w;

    .line 24
    return-void
.end method

.method private a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/g;->b:Landroid/content/Context;

    invoke-static {v0, p2, p1}, Lcom/google/android/gms/wallet/shared/common/c/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/Throwable;)V

    .line 90
    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;
    .locals 2

    .prologue
    .line 51
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/g;->a:Lcom/google/android/gms/wallet/service/ow/w;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 53
    :goto_0
    return-object v0

    .line 52
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 53
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    invoke-direct {p0, v1, p1}, Lcom/google/android/gms/wallet/service/ow/g;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;-><init>(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 62
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/g;->a:Lcom/google/android/gms/wallet/service/ow/w;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 64
    :goto_0
    return-object v0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/service/ow/g;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 82
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/g;->a:Lcom/google/android/gms/wallet/service/ow/w;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 84
    :goto_0
    return-object v0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/service/ow/g;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 72
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/g;->a:Lcom/google/android/gms/wallet/service/ow/w;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 74
    :goto_0
    return-object v0

    .line 73
    :catch_0
    move-exception v0

    .line 74
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/service/ow/g;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 41
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/g;->a:Lcom/google/android/gms/wallet/service/ow/w;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 43
    :goto_0
    return-object v0

    .line 42
    :catch_0
    move-exception v0

    .line 43
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/service/ow/g;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 30
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/g;->a:Lcom/google/android/gms/wallet/service/ow/w;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 32
    :goto_0
    return-object v0

    .line 31
    :catch_0
    move-exception v0

    .line 32
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/service/ow/g;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method
