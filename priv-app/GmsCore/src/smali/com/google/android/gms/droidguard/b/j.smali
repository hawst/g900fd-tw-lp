.class public final Lcom/google/android/gms/droidguard/b/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:[B

.field static final b:[B

.field private static final d:[I

.field private static final e:[I

.field private static final f:[I

.field private static final g:[I

.field private static final h:[I

.field private static final i:[I


# instance fields
.field c:Lcom/google/android/gms/droidguard/b/m;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 26
    const/16 v0, 0x31

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/droidguard/b/j;->d:[I

    .line 31
    const/16 v0, 0x2b

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/gms/droidguard/b/j;->e:[I

    .line 35
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/gms/droidguard/b/j;->f:[I

    .line 37
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/gms/droidguard/b/j;->g:[I

    .line 39
    const/16 v0, 0x14

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/gms/droidguard/b/j;->h:[I

    .line 42
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/google/android/gms/droidguard/b/j;->i:[I

    .line 45
    new-array v0, v1, [B

    fill-array-data v0, :array_6

    sput-object v0, Lcom/google/android/gms/droidguard/b/j;->a:[B

    .line 49
    new-array v0, v1, [B

    fill-array-data v0, :array_7

    sput-object v0, Lcom/google/android/gms/droidguard/b/j;->b:[B

    return-void

    .line 26
    :array_0
    .array-data 4
        0x72
        0x50
        0xf
        0x60
        0x72
        0x14
        0xf
        0x67
        0x63
        0x64
        0x54
        0x24
        0x39
        0x65
        0x5b
        0x45
        0x6
        0x49
        0x75
        0x48
        0x2c
        0x67
        0x12
        0x54
        0x2
        0x5a
        0x39
        0x24
        0x74
        0x24
        0x6d
        0x64
        0x64
        0x5d
        0xf
        0x6b
        0x69
        0x35
        0x39
        0x5f
        0x38
        0x5f
        0x6f
        0x48
        0x35
        0x5c
        0x39
        0x24
        0x2c
    .end array-data

    .line 31
    :array_1
    .array-data 4
        0x5b
        0x5a
        0x50
        0xf
        0x14
        0xf
        0x54
        0x24
        0x71
        0x39
        0x45
        0x6d
        0x6e
        0x5e
        0x6
        0x49
        0x48
        0x2c
        0x6d
        0x12
        0x5d
        0x54
        0x32
        0x39
        0x45
        0x6
        0x5e
        0x72
        0x59
        0x49
        0x39
        0x9
        0x58
        0x5e
        0x62
        0x66
        0xf
        0x4c
        0x1e
        0x3e
        0x72
        0x62
        0x29
    .end array-data

    .line 35
    :array_2
    .array-data 4
        0x74
        0x32
        0x1b
        0x2d
        0x30
        0x56
    .end array-data

    .line 37
    :array_3
    .array-data 4
        0x1e
        0x48
        0x35
        0x39
        0x5e
        0x65
        0x63
        0x24
        0x61
        0x2c
    .end array-data

    .line 39
    :array_4
    .array-data 4
        0x35
        0x39
        0x65
        0x2c
        0x5b
        0x5d
        0x58
        0x58
        0x5d
        0x1c
        0x4c
        0x5e
        0x24
        0x2c
        0xf
        0x6b
        0x6f
        0x4c
        0x45
        0x39
    .end array-data

    .line 42
    :array_5
    .array-data 4
        0x4c
        0x5d
        0x39
        0x3a
        0x6e
        0x2c
        0x2b
        0x12
        0x2c
        0x39
        0x24
    .end array-data

    .line 45
    :array_6
    .array-data 1
        -0xet
        0x76t
        0x2t
        0x28t
        -0x7bt
        0x1t
        -0x2t
        -0x1dt
        0x41t
        -0x7et
        -0x4ft
        -0x5ft
        0x67t
        0xbt
        -0x10t
        0x4at
    .end array-data

    .line 49
    :array_7
    .array-data 1
        -0x6bt
        0x17t
        0x2at
        0x34t
        0x9t
        0x39t
        0x18t
        0x3ft
        -0x28t
        -0x3bt
        -0xdt
        -0x8t
        0x4ft
        0x4dt
        -0x52t
        0x16t
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/gms/droidguard/b/m;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/google/android/gms/droidguard/b/j;->c:Lcom/google/android/gms/droidguard/b/m;

    .line 57
    return-void
.end method

.method private a(I)[B
    .locals 6

    .prologue
    .line 122
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/droidguard/b/j;->c:Lcom/google/android/gms/droidguard/b/m;

    sget-object v1, Lcom/google/android/gms/droidguard/b/j;->e:[I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 123
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    .line 124
    iget-object v2, p0, Lcom/google/android/gms/droidguard/b/j;->c:Lcom/google/android/gms/droidguard/b/m;

    sget-object v3, Lcom/google/android/gms/droidguard/b/j;->i:[I

    invoke-virtual {v2, v3}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, [B

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 126
    new-array v2, p1, [B

    .line 127
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-virtual {v0, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    return-object v2

    .line 129
    :catch_0
    move-exception v0

    .line 130
    new-instance v1, Lcom/google/android/gms/droidguard/b/k;

    invoke-direct {v1, v0}, Lcom/google/android/gms/droidguard/b/k;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static varargs a([[B)[B
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 135
    .line 136
    array-length v3, p0

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, p0, v0

    .line 137
    array-length v4, v4

    add-int/2addr v2, v4

    .line 136
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 139
    :cond_0
    new-array v3, v2, [B

    .line 141
    array-length v4, p0

    move v0, v1

    move v2, v1

    :goto_1
    if-ge v0, v4, :cond_1

    aget-object v5, p0, v0

    .line 142
    array-length v6, v5

    invoke-static {v5, v1, v3, v2, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 143
    array-length v5, v5

    add-int/2addr v2, v5

    .line 141
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 145
    :cond_1
    return-object v3
.end method

.method private b([B)[B
    .locals 2

    .prologue
    .line 91
    const/4 v0, 0x1

    .line 92
    :goto_0
    if-ltz v0, :cond_1

    .line 94
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/droidguard/b/j;->c([B)[B
    :try_end_0
    .catch Lcom/google/android/gms/droidguard/b/k; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 103
    :goto_1
    return-object v0

    .line 95
    :catch_0
    move-exception v1

    .line 96
    if-nez v0, :cond_0

    .line 97
    throw v1

    .line 99
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 103
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private c([B)[B
    .locals 7

    .prologue
    .line 108
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/droidguard/b/j;->c:Lcom/google/android/gms/droidguard/b/m;

    sget-object v1, Lcom/google/android/gms/droidguard/b/j;->d:[I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 109
    iget-object v1, p0, Lcom/google/android/gms/droidguard/b/j;->c:Lcom/google/android/gms/droidguard/b/m;

    sget-object v2, Lcom/google/android/gms/droidguard/b/j;->h:[I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 111
    iget-object v2, p0, Lcom/google/android/gms/droidguard/b/j;->c:Lcom/google/android/gms/droidguard/b/m;

    sget-object v3, Lcom/google/android/gms/droidguard/b/j;->g:[I

    invoke-virtual {v2, v3}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, [B

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 113
    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/droidguard/b/j;->c:Lcom/google/android/gms/droidguard/b/m;

    sget-object v6, Lcom/google/android/gms/droidguard/b/j;->f:[I

    invoke-virtual {v5, v6}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 114
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 115
    :catch_0
    move-exception v0

    .line 116
    new-instance v1, Lcom/google/android/gms/droidguard/b/k;

    invoke-direct {v1, v0}, Lcom/google/android/gms/droidguard/b/k;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a([B)[B
    .locals 14

    .prologue
    const/16 v13, 0x8

    const/4 v12, 0x4

    const/4 v6, 0x1

    const/4 v11, 0x2

    const/4 v2, 0x0

    .line 60
    invoke-direct {p0, v13}, Lcom/google/android/gms/droidguard/b/j;->a(I)[B

    move-result-object v7

    array-length v3, v7

    move v0, v2

    move v1, v6

    :goto_0
    if-ge v0, v3, :cond_0

    aget-byte v4, v7, v0

    xor-int/2addr v1, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    aget-byte v0, v7, v11

    and-int/lit8 v0, v0, -0x4

    aget-byte v3, v7, v11

    xor-int/2addr v1, v3

    and-int/lit8 v1, v1, 0x3

    or-int/2addr v0, v1

    int-to-byte v0, v0

    aput-byte v0, v7, v11

    .line 61
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/google/android/gms/droidguard/b/j;->a(I)[B

    move-result-object v8

    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/gms/droidguard/b/j;->b([B)[B

    move-result-object v0

    .line 63
    new-array v1, v11, [[B

    aput-object p1, v1, v2

    aput-object v0, v1, v6

    invoke-static {v1}, Lcom/google/android/gms/droidguard/b/j;->a([[B)[B

    move-result-object v1

    .line 64
    new-instance v0, Lcom/google/android/gms/droidguard/b/l;

    sget-object v3, Lcom/google/android/gms/droidguard/b/j;->a:[B

    sget-object v4, Lcom/google/android/gms/droidguard/b/j;->b:[B

    invoke-direct {v0, v3, v4}, Lcom/google/android/gms/droidguard/b/l;-><init>([B[B)V

    .line 65
    array-length v3, v8

    mul-int/lit8 v3, v3, 0x8

    iget-object v4, v0, Lcom/google/android/gms/droidguard/b/l;->a:[I

    invoke-static {v8, v2}, Lcom/google/android/gms/droidguard/b/l;->a([BI)I

    move-result v5

    aput v5, v4, v6

    iget-object v4, v0, Lcom/google/android/gms/droidguard/b/l;->a:[I

    invoke-static {v8, v12}, Lcom/google/android/gms/droidguard/b/l;->a([BI)I

    move-result v5

    aput v5, v4, v11

    iget-object v4, v0, Lcom/google/android/gms/droidguard/b/l;->a:[I

    const/4 v5, 0x3

    invoke-static {v8, v13}, Lcom/google/android/gms/droidguard/b/l;->a([BI)I

    move-result v9

    aput v9, v4, v5

    iget-object v4, v0, Lcom/google/android/gms/droidguard/b/l;->a:[I

    const/16 v5, 0xc

    invoke-static {v8, v5}, Lcom/google/android/gms/droidguard/b/l;->a([BI)I

    move-result v5

    aput v5, v4, v12

    const/16 v4, 0x100

    if-ne v3, v4, :cond_1

    const/16 v3, 0x10

    iget-object v4, v0, Lcom/google/android/gms/droidguard/b/l;->b:[B

    :goto_1
    iget-object v5, v0, Lcom/google/android/gms/droidguard/b/l;->a:[I

    const/16 v9, 0xb

    add-int/lit8 v10, v3, 0x0

    invoke-static {v8, v10}, Lcom/google/android/gms/droidguard/b/l;->a([BI)I

    move-result v10

    aput v10, v5, v9

    iget-object v5, v0, Lcom/google/android/gms/droidguard/b/l;->a:[I

    const/16 v9, 0xc

    add-int/lit8 v10, v3, 0x4

    invoke-static {v8, v10}, Lcom/google/android/gms/droidguard/b/l;->a([BI)I

    move-result v10

    aput v10, v5, v9

    iget-object v5, v0, Lcom/google/android/gms/droidguard/b/l;->a:[I

    const/16 v9, 0xd

    add-int/lit8 v10, v3, 0x8

    invoke-static {v8, v10}, Lcom/google/android/gms/droidguard/b/l;->a([BI)I

    move-result v10

    aput v10, v5, v9

    iget-object v5, v0, Lcom/google/android/gms/droidguard/b/l;->a:[I

    const/16 v9, 0xe

    add-int/lit8 v3, v3, 0xc

    invoke-static {v8, v3}, Lcom/google/android/gms/droidguard/b/l;->a([BI)I

    move-result v3

    aput v3, v5, v9

    iget-object v3, v0, Lcom/google/android/gms/droidguard/b/l;->a:[I

    invoke-static {v4, v2}, Lcom/google/android/gms/droidguard/b/l;->a([BI)I

    move-result v5

    aput v5, v3, v2

    iget-object v3, v0, Lcom/google/android/gms/droidguard/b/l;->a:[I

    const/4 v5, 0x5

    invoke-static {v4, v12}, Lcom/google/android/gms/droidguard/b/l;->a([BI)I

    move-result v9

    aput v9, v3, v5

    iget-object v3, v0, Lcom/google/android/gms/droidguard/b/l;->a:[I

    const/16 v5, 0xa

    invoke-static {v4, v13}, Lcom/google/android/gms/droidguard/b/l;->a([BI)I

    move-result v9

    aput v9, v3, v5

    iget-object v3, v0, Lcom/google/android/gms/droidguard/b/l;->a:[I

    const/16 v5, 0xf

    const/16 v9, 0xc

    invoke-static {v4, v9}, Lcom/google/android/gms/droidguard/b/l;->a([BI)I

    move-result v4

    aput v4, v3, v5

    .line 66
    iget-object v3, v0, Lcom/google/android/gms/droidguard/b/l;->a:[I

    const/4 v4, 0x6

    invoke-static {v7, v2}, Lcom/google/android/gms/droidguard/b/l;->a([BI)I

    move-result v5

    aput v5, v3, v4

    iget-object v3, v0, Lcom/google/android/gms/droidguard/b/l;->a:[I

    const/4 v4, 0x7

    invoke-static {v7, v12}, Lcom/google/android/gms/droidguard/b/l;->a([BI)I

    move-result v5

    aput v5, v3, v4

    iget-object v3, v0, Lcom/google/android/gms/droidguard/b/l;->a:[I

    aput v2, v3, v13

    iget-object v3, v0, Lcom/google/android/gms/droidguard/b/l;->a:[I

    const/16 v4, 0x9

    aput v2, v3, v4

    .line 67
    array-length v3, v1

    new-array v3, v3, [B

    .line 68
    array-length v5, v1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/droidguard/b/l;->a([BI[BII)V

    .line 69
    const/4 v0, 0x3

    new-array v0, v0, [[B

    aput-object v7, v0, v2

    aput-object v8, v0, v6

    aput-object v3, v0, v11

    invoke-static {v0}, Lcom/google/android/gms/droidguard/b/j;->a([[B)[B

    move-result-object v0

    .line 70
    return-object v0

    .line 65
    :cond_1
    const/16 v4, 0x80

    if-ne v3, v4, :cond_2

    iget-object v3, v0, Lcom/google/android/gms/droidguard/b/l;->c:[B

    move-object v4, v3

    move v3, v2

    goto/16 :goto_1

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "kbits: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
