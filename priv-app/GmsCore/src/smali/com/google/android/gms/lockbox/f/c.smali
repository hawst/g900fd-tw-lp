.class public final Lcom/google/android/gms/lockbox/f/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/lockbox/m;


# instance fields
.field private final a:Landroid/os/PowerManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/google/android/gms/lockbox/f/c;->a:Landroid/os/PowerManager;

    .line 33
    return-void
.end method


# virtual methods
.method public final a(J)Ljava/util/List;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 37
    new-instance v2, Lcom/google/android/gms/lockbox/f/b;

    invoke-direct {v2}, Lcom/google/android/gms/lockbox/f/b;-><init>()V

    .line 38
    iput-wide p1, v2, Lcom/google/android/gms/lockbox/f/b;->a:J

    .line 39
    iput-boolean v1, v2, Lcom/google/android/gms/lockbox/f/b;->c:Z

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/lockbox/f/c;->a:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput v0, v2, Lcom/google/android/gms/lockbox/f/b;->d:I

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 44
    invoke-static {v2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    return-object v0

    .line 40
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method
