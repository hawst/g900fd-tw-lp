.class public final Lcom/google/android/gms/wallet/common/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/android/volley/s;


# direct methods
.method public constructor <init>(Lcom/android/volley/s;)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/b/a;->a:Lcom/android/volley/s;

    .line 83
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;[BII)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 10

    .prologue
    .line 182
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 183
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot make network request from UI thread!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_0
    invoke-static {}, Lcom/android/volley/toolbox/aa;->a()Lcom/android/volley/toolbox/aa;

    move-result-object v5

    .line 186
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v6

    .line 188
    :try_start_0
    iget-object v8, p0, Lcom/google/android/gms/wallet/common/b/a;->a:Lcom/android/volley/s;

    new-instance v0, Lcom/google/android/gms/wallet/common/b/b;

    move-object v1, p1

    move-object v2, p4

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/common/b/b;-><init>(Ljava/lang/String;[BLjava/util/ArrayList;Ljava/lang/String;Lcom/android/volley/toolbox/aa;)V

    invoke-virtual {v8, v0}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    move-result-object v0

    .line 190
    invoke-virtual {v5, v0}, Lcom/android/volley/toolbox/aa;->a(Lcom/android/volley/p;)V

    .line 192
    invoke-virtual {v5}, Lcom/android/volley/toolbox/aa;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/volley/m;

    .line 193
    if-gez p6, :cond_a

    const/4 v4, 0x4

    :goto_0
    invoke-static {p5}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "application/x-protobuf"

    move-object v5, v1

    :goto_1
    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_4

    iget v8, v0, Lcom/android/volley/m;->a:I

    const/16 v3, 0x191

    if-ne v8, v3, :cond_9

    const/4 v3, 0x5

    :goto_2
    iget-object v1, v0, Lcom/android/volley/m;->c:Ljava/util/Map;

    const-string v9, "Content-Type"

    invoke-interface {v1, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-nez v1, :cond_1

    const-string v1, ""

    :cond_1
    if-eqz v5, :cond_2

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_2
    iget-object v0, v0, Lcom/android/volley/m;->b:[B

    sparse-switch v8, :sswitch_data_0

    move v1, v4

    :goto_3
    move-object v2, v0

    move p5, v1

    :goto_4
    const/4 v0, 0x0

    packed-switch p5, :pswitch_data_0

    :pswitch_0
    const-string v1, "ServerConnection"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected serverResponseType="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v0

    :goto_5
    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    invoke-direct {v0, p5, v2, v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(I[BLjava/lang/Class;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    :goto_6
    return-object v0

    .line 193
    :cond_3
    const/4 v1, 0x0

    move-object v5, v1

    goto :goto_1

    :sswitch_0
    move-object v2, v0

    goto :goto_4

    :cond_4
    :try_start_1
    const-string v0, "ServerConnection"

    const-string v3, "Null HTTP response"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move p5, v1

    goto :goto_4

    :pswitch_1
    const-class v0, Lcom/google/checkout/inapp/proto/ae;

    move-object v1, v0

    goto :goto_5

    :pswitch_2
    const-class v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;

    move-object v1, v0

    goto :goto_5

    :pswitch_3
    const-class v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;

    move-object v1, v0

    goto :goto_5

    :pswitch_4
    const-class v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;

    move-object v1, v0

    goto :goto_5

    :pswitch_5
    const-class v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;

    move-object v1, v0

    goto :goto_5

    :pswitch_6
    const-class v0, Lcom/google/aa/b/a/a/a/a/n;

    move-object v1, v0

    goto :goto_5

    :pswitch_7
    const-class v0, Lcom/google/aa/b/a/a/a/a/l;

    move-object v1, v0

    goto :goto_5

    :pswitch_8
    const-class v0, Lcom/google/aa/b/a/a/a/a/p;

    move-object v1, v0

    goto :goto_5

    :pswitch_9
    const-class v0, Lcom/google/aa/b/a/a/a/a/j;

    move-object v1, v0

    goto :goto_5

    :pswitch_a
    const-class v0, Lcom/google/checkout/b/a/c;

    move-object v1, v0

    goto :goto_5

    :pswitch_b
    const-class v0, Lcom/google/aa/a/a/a/f;

    move-object v1, v0

    goto :goto_5

    :pswitch_c
    const-class v0, Lcom/google/aa/b/a/a/a/a/d;

    move-object v1, v0

    goto :goto_5

    :pswitch_d
    const-class v0, Lcom/google/aa/b/a/a/a/a/f;

    move-object v1, v0

    goto :goto_5

    :pswitch_e
    const-class v0, Lcom/google/checkout/inapp/proto/al;

    move-object v1, v0

    goto :goto_5

    :pswitch_f
    const-class v0, Lcom/google/checkout/inapp/proto/an;

    move-object v1, v0

    goto :goto_5

    :pswitch_10
    const-class v0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;

    move-object v1, v0

    goto :goto_5

    :pswitch_11
    const-class v0, Lcom/google/checkout/inapp/proto/ag;

    move-object v1, v0

    goto :goto_5

    :pswitch_12
    const-class v0, Lcom/google/checkout/inapp/proto/ad;

    move-object v1, v0

    goto :goto_5

    :pswitch_13
    const-class v0, Lcom/google/checkout/inapp/proto/t;

    move-object v1, v0

    goto :goto_5

    :pswitch_14
    const-class v0, Lcom/google/checkout/inapp/proto/x;

    move-object v1, v0

    goto :goto_5

    :pswitch_15
    const-class v0, Lcom/google/checkout/inapp/proto/v;

    move-object v1, v0

    goto :goto_5

    :pswitch_16
    const-class v0, Lcom/google/aa/b/a/a/a/a/h;

    move-object v1, v0

    goto :goto_5

    :pswitch_17
    const-class v0, Lcom/google/checkout/inapp/proto/ai;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, v0

    goto :goto_5

    :pswitch_18
    move-object v1, v0

    goto :goto_5

    .line 195
    :catch_0
    move-exception v0

    .line 196
    :try_start_2
    const-string v1, "ServerConnection"

    const-string v2, "Exception sending Volley request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 197
    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 209
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_6

    .line 198
    :catch_1
    move-exception v0

    .line 199
    :try_start_3
    const-string v1, "ServerConnection"

    const-string v2, "Exception sending Volley request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 200
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    .line 201
    instance-of v2, v1, Lcom/android/volley/l;

    if-nez v2, :cond_5

    instance-of v1, v1, Lcom/android/volley/ab;

    if-eqz v1, :cond_6

    .line 202
    :cond_5
    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 209
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_6

    .line 204
    :cond_6
    :try_start_4
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Lcom/android/volley/a;

    if-eqz v0, :cond_7

    .line 205
    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b:Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 209
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_6

    .line 207
    :cond_7
    :try_start_5
    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 209
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_6

    :catchall_0
    move-exception v0

    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0

    :cond_8
    move-object v0, v2

    move v1, v3

    goto/16 :goto_3

    :cond_9
    move v3, v1

    goto/16 :goto_2

    :cond_a
    move/from16 v4, p6

    goto/16 :goto_0

    .line 193
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x191 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_f
        :pswitch_e
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_18
        :pswitch_3
        :pswitch_10
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch
.end method

.method private static a(Lcom/google/android/gms/wallet/a/b;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 344
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 345
    if-eqz p1, :cond_0

    .line 346
    const-string v1, "Content-Type"

    invoke-static {v1, p1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 348
    :cond_0
    if-eqz p0, :cond_1

    .line 349
    const-string v1, "Authorization"

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/a/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 352
    :cond_1
    const-string v1, "X-Modality"

    const-string v2, "ANDROID_NATIVE"

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    const-string v1, "X-Version"

    const v2, 0x6768a8

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 355
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/gms/wallet/a/b;Ljava/util/List;Ljava/util/ArrayList;II)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 119
    :try_start_0
    const-string v0, "application/x-www-form-urlencoded"

    invoke-static {p2, v0}, Lcom/google/android/gms/wallet/common/b/a;->a(Lcom/google/android/gms/wallet/a/b;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 121
    if-eqz p4, :cond_0

    .line 122
    invoke-virtual {v2, p4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 124
    :cond_0
    new-instance v0, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    const-string v1, "utf-8"

    invoke-direct {v0, p3, v1}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 125
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 126
    invoke-virtual {v0, v1}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;->writeTo(Ljava/io/OutputStream;)V

    .line 127
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    .line 128
    const-string v3, "application/x-www-form-urlencoded"

    move-object v0, p0

    move-object v1, p1

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/wallet/common/b/a;->a(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;[BII)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 135
    :goto_0
    return-object v0

    .line 132
    :catch_0
    move-exception v0

    const-string v0, "ServerConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception sending HTTP request to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/wallet/a/b;[BLjava/util/ArrayList;II)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 170
    const-string v0, "application/x-protobuf"

    invoke-static {p2, v0}, Lcom/google/android/gms/wallet/common/b/a;->a(Lcom/google/android/gms/wallet/a/b;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 172
    if-eqz p4, :cond_0

    .line 173
    invoke-virtual {v2, p4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 175
    :cond_0
    const-string v3, "application/x-protobuf"

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/wallet/common/b/a;->a(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;[BII)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method
