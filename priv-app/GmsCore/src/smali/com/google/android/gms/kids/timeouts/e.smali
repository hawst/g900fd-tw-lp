.class final Lcom/google/android/gms/kids/timeouts/e;
.super Ljava/util/HashMap;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/kids/timeouts/d;


# direct methods
.method constructor <init>(Lcom/google/android/gms/kids/timeouts/d;)V
    .locals 2

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/android/gms/kids/timeouts/e;->a:Lcom/google/android/gms/kids/timeouts/d;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 58
    const-string v0, "key"

    const-string v1, "BLOB PRIMARY KEY"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/kids/timeouts/e;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    const-string v0, "value"

    const-string v1, "BLOB NOT NULL"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/kids/timeouts/e;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    const-string v0, "start_time_millis"

    const-string v1, "INTEGER"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/kids/timeouts/e;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    const-string v0, "end_time_millis"

    const-string v1, "INTEGER"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/kids/timeouts/e;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    const-string v0, "repeat_on_day_of_week"

    const-string v1, "TEXT"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/kids/timeouts/e;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    const-string v0, "update_time_stamp"

    const-string v1, "INTEGER"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/kids/timeouts/e;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    const-string v0, "is_deleted"

    const-string v1, "INTEGER"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/kids/timeouts/e;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    return-void
.end method
