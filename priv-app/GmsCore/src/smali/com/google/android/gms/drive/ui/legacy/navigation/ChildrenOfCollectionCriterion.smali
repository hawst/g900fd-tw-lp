.class public Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/ui/legacy/navigation/Criterion;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/android/gms/drive/DriveId;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    new-instance v0, Lcom/google/android/gms/drive/ui/legacy/navigation/a;

    invoke-direct {v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;->a:Ljava/lang/String;

    .line 73
    const-class v0, Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    .line 74
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/drive/DriveId;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;->a:Ljava/lang/String;

    .line 34
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    .line 35
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/query/Filter;
    .locals 2

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->w:Lcom/google/android/gms/drive/metadata/internal/a/f;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/query/c;->a(Lcom/google/android/gms/drive/metadata/h;Ljava/lang/Object;)Lcom/google/android/gms/drive/query/Filter;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 44
    if-ne p1, p0, :cond_1

    .line 50
    :cond_0
    :goto_0
    return v0

    .line 46
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;

    if-eqz v2, :cond_3

    .line 47
    check-cast p1, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;

    .line 48
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    iget-object v3, p1, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 50
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 56
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 104
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ChildrenOfCollectionCriterion {DriveId=%s}"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;->b:Lcom/google/android/gms/drive/DriveId;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 68
    return-void
.end method
