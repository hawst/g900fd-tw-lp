.class public final Lcom/google/android/gms/wallet/common/a/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/x;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/android/volley/s;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Lcom/android/volley/x;

.field private final g:Lcom/android/volley/w;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/volley/s;ILjava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/a/i;->a:Landroid/content/Context;

    .line 77
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/a/i;->b:Lcom/android/volley/s;

    .line 78
    iput p3, p0, Lcom/google/android/gms/wallet/common/a/i;->c:I

    .line 79
    iput-object p4, p0, Lcom/google/android/gms/wallet/common/a/i;->d:Ljava/lang/String;

    .line 80
    iput-object p5, p0, Lcom/google/android/gms/wallet/common/a/i;->e:Ljava/lang/String;

    .line 81
    iput-object p6, p0, Lcom/google/android/gms/wallet/common/a/i;->f:Lcom/android/volley/x;

    .line 82
    iput-object p7, p0, Lcom/google/android/gms/wallet/common/a/i;->g:Lcom/android/volley/w;

    .line 83
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 98
    new-instance v0, Lcom/google/android/gms/wallet/common/a/j;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/a/i;->a:Landroid/content/Context;

    iget v2, p0, Lcom/google/android/gms/wallet/common/a/i;->c:I

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/a/i;->g:Lcom/android/volley/w;

    move-object v3, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/common/a/j;-><init>(Landroid/content/Context;ILjava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 101
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/a/i;->b:Lcom/android/volley/s;

    invoke-virtual {v1, v0}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    .line 102
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/i;->e:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/a/i;->a(Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 46
    check-cast p1, Lorg/json/JSONObject;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/i;->d:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/gms/wallet/common/a/e;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/a/i;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/i;->f:Lcom/android/volley/x;

    invoke-interface {v0, p1}, Lcom/android/volley/x;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
