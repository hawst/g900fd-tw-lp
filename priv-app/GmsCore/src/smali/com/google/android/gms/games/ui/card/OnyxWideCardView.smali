.class public final Lcom/google/android/gms/games/ui/card/OnyxWideCardView;
.super Lcom/google/android/gms/games/ui/card/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/card/g;
.implements Lcom/google/android/gms/games/ui/card/i;
.implements Lcom/google/android/gms/games/ui/card/j;
.implements Lcom/google/android/gms/games/ui/card/k;
.implements Lcom/google/android/gms/games/ui/card/m;


# instance fields
.field private A:Landroid/graphics/drawable/Drawable;

.field private B:Lcom/google/android/gms/games/ui/card/r;

.field private h:Landroid/widget/ProgressBar;

.field private i:Landroid/view/View;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/database/CharArrayBuffer;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/database/CharArrayBuffer;

.field private n:Landroid/widget/ImageView;

.field private o:Lcom/google/android/gms/games/ui/card/q;

.field private p:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field private q:Landroid/widget/ImageView;

.field private r:Landroid/view/View;

.field private s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/view/View;

.field private w:I

.field private x:Landroid/support/v7/widget/bp;

.field private y:Landroid/view/View;

.field private z:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/a;-><init>(Landroid/content/Context;)V

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/ui/card/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/card/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 77
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/bp;)V
    .locals 0

    .prologue
    .line 306
    iput-object p1, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->x:Landroid/support/v7/widget/bp;

    .line 307
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/ui/card/q;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->o:Lcom/google/android/gms/games/ui/card/q;

    .line 199
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/ui/card/r;)V
    .locals 0

    .prologue
    .line 401
    iput-object p1, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->B:Lcom/google/android/gms/games/ui/card/r;

    .line 402
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->i:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 240
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->i:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 267
    return-void
.end method

.method public final e()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 116
    invoke-super {p0}, Lcom/google/android/gms/games/ui/card/a;->e()V

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->j:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->l:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->i:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->p:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->q:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->q:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->t:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->t:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/f;->r:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->a(Landroid/widget/TextView;I)V

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->u:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->u:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/f;->r:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->a(Landroid/widget/TextView;I)V

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->r:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 141
    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->g(I)V

    .line 143
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->y:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->z:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->A:Landroid/graphics/drawable/Drawable;

    const/16 v1, 0x10

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->z:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 145
    :goto_0
    return-void

    .line 144
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->z:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final g(I)V
    .locals 2

    .prologue
    .line 311
    iput p1, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->w:I

    .line 312
    if-lez p1, :cond_0

    .line 313
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->v:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 317
    :goto_0
    return-void

    .line 315
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->v:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final h(I)V
    .locals 2

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->y:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 389
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->z:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 390
    return-void
.end method

.method public final i(I)V
    .locals 2

    .prologue
    .line 418
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->z:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 419
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 429
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->b:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 430
    new-instance v0, Landroid/util/Pair;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const-string v2, "banner"

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 431
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->n:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 432
    new-instance v1, Landroid/util/Pair;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->n:Landroid/widget/ImageView;

    const-string v3, "imageOverlay"

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 434
    iget-object v2, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->c:Lcom/google/android/gms/games/ui/card/s;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/util/Pair;

    aput-object v0, v3, v4

    aput-object v1, v3, v5

    invoke-interface {v2}, Lcom/google/android/gms/games/ui/card/s;->u()V

    .line 450
    :goto_0
    return-void

    .line 436
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->c:Lcom/google/android/gms/games/ui/card/s;

    new-array v2, v5, [Landroid/util/Pair;

    aput-object v0, v2, v4

    invoke-interface {v1}, Lcom/google/android/gms/games/ui/card/s;->u()V

    goto :goto_0

    .line 438
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->n:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_2

    .line 439
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->o:Lcom/google/android/gms/games/ui/card/q;

    goto :goto_0

    .line 440
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->v:Landroid/view/View;

    if-ne p1, v0, :cond_3

    .line 441
    new-instance v0, Landroid/support/v7/widget/bn;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/v7/widget/bn;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 442
    iget v1, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->w:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bn;->a(I)V

    .line 443
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->x:Landroid/support/v7/widget/bp;

    iput-object v1, v0, Landroid/support/v7/widget/bn;->c:Landroid/support/v7/widget/bp;

    .line 444
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->v:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/view/View;Landroid/support/v7/widget/bn;)V

    goto :goto_0

    .line 445
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->z:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    .line 446
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->B:Lcom/google/android/gms/games/ui/card/r;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/card/r;->y()V

    goto :goto_0

    .line 448
    :cond_4
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/card/a;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected final onFinishInflate()V
    .locals 2

    .prologue
    const/16 v1, 0x40

    .line 81
    invoke-super {p0}, Lcom/google/android/gms/games/ui/card/a;->onFinishInflate()V

    .line 83
    sget v0, Lcom/google/android/gms/j;->kd:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->h:Landroid/widget/ProgressBar;

    .line 84
    sget v0, Lcom/google/android/gms/j;->ke:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->i:Landroid/view/View;

    .line 85
    sget v0, Lcom/google/android/gms/j;->la:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->j:Landroid/widget/TextView;

    .line 86
    new-instance v0, Landroid/database/CharArrayBuffer;

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->k:Landroid/database/CharArrayBuffer;

    .line 87
    sget v0, Lcom/google/android/gms/j;->qw:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->l:Landroid/widget/TextView;

    .line 88
    new-instance v0, Landroid/database/CharArrayBuffer;

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->m:Landroid/database/CharArrayBuffer;

    .line 89
    sget v0, Lcom/google/android/gms/j;->kc:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->n:Landroid/widget/ImageView;

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    sget v0, Lcom/google/android/gms/j;->jW:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->p:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    .line 94
    sget v0, Lcom/google/android/gms/j;->pH:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->q:Landroid/widget/ImageView;

    .line 96
    sget v0, Lcom/google/android/gms/j;->rQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->r:Landroid/view/View;

    .line 97
    sget v0, Lcom/google/android/gms/j;->rT:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->t:Landroid/widget/TextView;

    .line 98
    sget v0, Lcom/google/android/gms/j;->rS:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->u:Landroid/widget/TextView;

    .line 99
    sget v0, Lcom/google/android/gms/j;->rR:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    sget v1, Lcom/google/android/gms/g;->ar:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->c(I)V

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    sget v1, Lcom/google/android/gms/g;->as:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(I)V

    .line 105
    sget v0, Lcom/google/android/gms/j;->nd:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->v:Landroid/view/View;

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->v:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    sget v0, Lcom/google/android/gms/j;->E:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->y:Landroid/view/View;

    .line 109
    sget v0, Lcom/google/android/gms/j;->pD:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->z:Landroid/widget/Button;

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->z:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->z:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxWideCardView;->A:Landroid/graphics/drawable/Drawable;

    .line 112
    return-void
.end method
