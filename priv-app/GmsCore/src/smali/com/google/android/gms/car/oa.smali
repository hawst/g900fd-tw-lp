.class public final Lcom/google/android/gms/car/oa;
.super Landroid/app/Presentation;
.source "SourceFile"


# instance fields
.field private a:Z

.field private volatile b:Lcom/google/android/gms/car/ar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/Display;)V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/car/oa;-><init>(Landroid/content/Context;Landroid/view/Display;B)V

    .line 52
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/Display;B)V
    .locals 2

    .prologue
    .line 64
    invoke-static {p1}, Lcom/google/android/gms/car/oa;->a(Landroid/content/Context;)I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Landroid/app/Presentation;-><init>(Landroid/content/Context;Landroid/view/Display;I)V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/oa;->a:Z

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/oa;->b:Lcom/google/android/gms/car/ar;

    .line 65
    invoke-virtual {p0}, Lcom/google/android/gms/car/oa;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 66
    const/16 v1, 0x7ee

    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    .line 68
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 69
    const/high16 v1, 0x1000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 70
    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 71
    return-void
.end method

.method private static a(Landroid/content/Context;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 87
    instance-of v1, p0, Landroid/app/Service;

    if-nez v1, :cond_1

    .line 88
    const-string v1, "CAR.PROJECTION"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    const-string v1, "CAR.PROJECTION"

    const-string v2, "getTheme: context not an instance of service"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :cond_0
    :goto_0
    return v0

    .line 93
    :cond_1
    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v2, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 96
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getServiceInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ServiceInfo;

    move-result-object v1

    .line 98
    iget-object v3, v1, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    .line 99
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->theme:I

    .line 101
    if-eqz v3, :cond_2

    .line 102
    const-string v4, "android.app.theme"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 104
    goto :goto_0

    .line 107
    :catch_0
    move-exception v1

    const-string v1, "CAR.PROJECTION"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not get theme for component "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; use the default theme"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private a(II)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v2, 0x0

    .line 189
    move v1, v2

    :goto_0
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 190
    invoke-virtual {p0}, Lcom/google/android/gms/car/oa;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    const-string v3, "CAR.PROJECTION"

    invoke-static {v3, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "CAR.PROJECTION"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "movefocus current = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/oa;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/view/View;->focusSearch(I)Landroid/view/View;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3, v4, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v0, "CAR.PROJECTION"

    invoke-static {v0, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "CAR.PROJECTION"

    const-string v3, "Nothing to grant focus to."

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 190
    :cond_2
    if-eqz v0, :cond_5

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    move v3, v0

    :goto_2
    const/4 v0, 0x2

    if-ne p2, v0, :cond_3

    const/4 v0, 0x1

    :goto_3
    add-int/2addr v0, v3

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocusFromTouch()Z

    const-string v3, "CAR.PROJECTION"

    invoke-static {v3, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "CAR.PROJECTION"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "movefocus next = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "touchmode"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/view/View;->isInTouchMode()Z

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    const/4 v0, -0x1

    goto :goto_3

    .line 192
    :cond_4
    return-void

    :cond_5
    move v3, v2

    goto :goto_2
.end method

.method private static a(Landroid/view/Window;Z)V
    .locals 2

    .prologue
    .line 273
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0, p1}, Landroid/view/Window;->setLocalFocus(ZZ)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    :goto_0
    return-void

    .line 275
    :catch_0
    move-exception v0

    const-string v0, "CAR.PROJECTION"

    const-string v1, "Trying to set input focus on window that\'s been removed."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/oa;)Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/oa;->a:Z

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 241
    invoke-virtual {p0}, Lcom/google/android/gms/car/oa;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/car/oa;->a:Z

    if-eqz v1, :cond_0

    .line 242
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v1

    const/16 v2, 0x1002

    if-ne v1, v2, :cond_1

    .line 244
    invoke-virtual {p0}, Lcom/google/android/gms/car/oa;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/oa;->a(Landroid/view/Window;Z)V

    .line 245
    invoke-virtual {p0}, Lcom/google/android/gms/car/oa;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/Window;->injectInputEvent(Landroid/view/InputEvent;)V

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 249
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/car/oa;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/gms/car/oa;->a(Landroid/view/Window;Z)V

    .line 250
    invoke-virtual {p0, p1}, Lcom/google/android/gms/car/oa;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 251
    const-string v2, "CAR.PROJECTION"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 252
    const-string v2, "CAR.PROJECTION"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "injectTouchEvent handled="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " event = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    :cond_2
    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 255
    const/16 v1, 0x9

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v1

    float-to-int v1, v1

    .line 256
    if-lez v1, :cond_3

    const/4 v0, 0x2

    .line 257
    :cond_3
    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/car/oa;->a(II)V

    goto :goto_0
.end method

.method public final cancel()V
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/google/android/gms/car/oa;->a:Z

    if-nez v0, :cond_0

    .line 136
    invoke-super {p0}, Landroid/app/Presentation;->cancel()V

    .line 138
    :cond_0
    return-void
.end method

.method public final dismiss()V
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/google/android/gms/car/oa;->a:Z

    if-nez v0, :cond_0

    .line 129
    invoke-super {p0}, Landroid/app/Presentation;->dismiss()V

    .line 131
    :cond_0
    return-void
.end method

.method public final show()V
    .locals 2

    .prologue
    .line 116
    invoke-super {p0}, Landroid/app/Presentation;->show()V

    .line 117
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/gms/car/ob;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/ob;-><init>(Lcom/google/android/gms/car/oa;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 124
    return-void
.end method
