.class final Lcom/google/android/gms/auth/setup/d2d/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/setup/d2d/c;


# instance fields
.field final synthetic a:Landroid/app/Fragment;

.field final synthetic b:Lcom/google/android/gms/auth/setup/d2d/TargetActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;Landroid/app/Fragment;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/google/android/gms/auth/setup/d2d/x;->b:Lcom/google/android/gms/auth/setup/d2d/TargetActivity;

    iput-object p2, p0, Lcom/google/android/gms/auth/setup/d2d/x;->a:Landroid/app/Fragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 246
    check-cast p1, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;

    invoke-static {p1}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->b(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->b(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;)Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/x;->a:Landroid/app/Fragment;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->b(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->o()Lcom/google/android/gms/auth/d/a;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Removing "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->b(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;)Landroid/app/Fragment;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->b(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;)Landroid/app/Fragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/x;->a:Landroid/app/Fragment;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/x;->a:Landroid/app/Fragment;

    invoke-static {p1, v1}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->b(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;Landroid/app/Fragment;)Landroid/app/Fragment;

    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->o()Lcom/google/android/gms/auth/d/a;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Adding "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->b(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;)Landroid/app/Fragment;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->b(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;)Landroid/app/Fragment;

    move-result-object v1

    const-string v2, "main"

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    :cond_2
    invoke-static {p1}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->a(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-static {p1}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->a(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;)Landroid/app/Fragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->o()Lcom/google/android/gms/auth/d/a;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Removing "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->a(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;)Landroid/app/Fragment;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->a(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;Landroid/app/Fragment;)Landroid/app/Fragment;

    :cond_3
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    :cond_4
    return-void
.end method
