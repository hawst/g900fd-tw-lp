.class public final Lcom/google/android/gms/wallet/ia/q;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/wallet/common/ui/bf;
.implements Lcom/google/android/gms/wallet/common/ui/bp;


# instance fields
.field A:I

.field B:Lcom/google/android/gms/wallet/ia/f;

.field C:Lcom/google/checkout/inapp/proto/al;

.field D:Lcom/google/android/gms/wallet/common/ui/cd;

.field E:Lcom/google/android/gms/wallet/common/ui/w;

.field private F:Landroid/view/View;

.field private G:Landroid/view/View;

.field private H:Landroid/view/View;

.field private I:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private J:Landroid/accounts/Account;

.field private K:Ljava/lang/String;

.field private L:Lcom/google/checkout/inapp/proto/q;

.field private M:Ljava/lang/String;

.field private N:Ljava/util/ArrayList;

.field private O:Ljava/util/ArrayList;

.field private P:Ljava/lang/String;

.field private Q:Z

.field private R:Lcom/google/android/gms/wallet/ia/u;

.field private S:Ljava/lang/String;

.field private T:I

.field private U:Lcom/google/android/gms/wallet/ia/p;

.field private V:Z

.field private W:Z

.field private X:Z

.field private Y:Lcom/google/android/gms/wallet/common/DisplayHints;

.field private Z:Z

.field a:Lcom/google/android/gms/wallet/ia/CartDetailsView;

.field private aa:Lcom/google/android/apps/common/a/a/i;

.field private ab:Lcom/google/android/apps/common/a/a/h;

.field private ac:Lcom/google/android/apps/common/a/a/i;

.field private ad:Lcom/google/android/apps/common/a/a/h;

.field private final ae:Lcom/google/android/gms/wallet/service/l;

.field b:Landroid/widget/ProgressBar;

.field c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

.field d:Landroid/view/View;

.field e:Lcom/google/android/gms/wallet/common/ui/cc;

.field f:Landroid/view/View;

.field g:Landroid/view/View;

.field h:Lcom/google/android/gms/wallet/common/ui/v;

.field i:Landroid/view/View;

.field j:Landroid/widget/TextView;

.field k:Landroid/widget/TextView;

.field l:Landroid/widget/TextView;

.field m:Landroid/widget/TextView;

.field n:Landroid/widget/TextView;

.field o:Lcom/google/android/gms/wallet/common/ui/bb;

.field p:Lcom/google/android/gms/wallet/common/ui/bb;

.field q:Landroid/widget/TextView;

.field r:Lcom/google/checkout/inapp/proto/n;

.field s:Lcom/google/android/gms/wallet/service/k;

.field t:[Lcom/google/checkout/inapp/proto/h;

.field u:Lcom/google/android/gms/wallet/common/PaymentModel;

.field v:Z

.field w:Z

.field x:Ljava/lang/String;

.field y:Ljava/lang/String;

.field z:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 77
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 224
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->v:Z

    .line 226
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->w:Z

    .line 228
    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->x:Ljava/lang/String;

    .line 230
    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->y:Ljava/lang/String;

    .line 236
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->W:Z

    .line 237
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->X:Z

    .line 239
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->Z:Z

    .line 244
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ia/q;->A:I

    .line 1096
    new-instance v0, Lcom/google/android/gms/wallet/ia/r;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ia/r;-><init>(Lcom/google/android/gms/wallet/ia/q;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->D:Lcom/google/android/gms/wallet/common/ui/cd;

    .line 1150
    new-instance v0, Lcom/google/android/gms/wallet/ia/s;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ia/s;-><init>(Lcom/google/android/gms/wallet/ia/q;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->E:Lcom/google/android/gms/wallet/common/ui/w;

    .line 1220
    new-instance v0, Lcom/google/android/gms/wallet/ia/t;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ia/t;-><init>(Lcom/google/android/gms/wallet/ia/q;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->ae:Lcom/google/android/gms/wallet/service/l;

    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lcom/google/android/gms/wallet/ia/q;
    .locals 3

    .prologue
    .line 264
    const-string v0, "buyFlowConfig must not be null or empty"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    const-string v0, "account must not be null or empty"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    new-instance v0, Lcom/google/android/gms/wallet/ia/q;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/ia/q;-><init>()V

    .line 267
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 268
    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 269
    const-string v2, "account"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 270
    const-string v2, "extraJwt"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ia/q;->setArguments(Landroid/os/Bundle;)V

    .line 272
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lcom/google/android/gms/wallet/ia/q;
    .locals 3

    .prologue
    .line 286
    const-string v0, "buyFlowConfig must not be null or empty"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    const-string v0, "account must not be null or empty"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    new-instance v0, Lcom/google/android/gms/wallet/ia/q;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/ia/q;-><init>()V

    .line 290
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 291
    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 292
    const-string v2, "account"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 293
    const-string v2, "extraPcid"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    const-string v2, "extraInfoText"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ia/q;->setArguments(Landroid/os/Bundle;)V

    .line 296
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->I:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/q;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/q;->S:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/q;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/q;->O:Ljava/util/ArrayList;

    return-object p1
.end method

.method static a(Lcom/google/checkout/inapp/proto/al;)Ljava/util/ArrayList;
    .locals 6

    .prologue
    .line 1198
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1199
    if-eqz p0, :cond_2

    .line 1200
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/al;->b:[Lcom/google/checkout/inapp/proto/n;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 1201
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/al;->b:[Lcom/google/checkout/inapp/proto/n;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 1202
    iget-object v5, v4, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v5, :cond_0

    .line 1203
    iget-object v5, v4, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1205
    :cond_0
    iget-object v5, v4, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    if-eqz v5, :cond_1

    .line 1206
    iget-object v5, v4, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    iget-object v5, v5, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v5, :cond_1

    .line 1207
    iget-object v4, v4, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    iget-object v4, v4, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1201
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1213
    :cond_2
    return-object v1
.end method

.method private a(Landroid/text/Spanned;)V
    .locals 2

    .prologue
    .line 771
    if-eqz p1, :cond_0

    .line 772
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 773
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->k:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 774
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->k:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 779
    :goto_0
    return-void

    .line 776
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->k:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 777
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->k:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/q;Lcom/google/checkout/inapp/proto/d;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/checkout/inapp/proto/d;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/q;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/ia/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/q;[Lcom/google/checkout/inapp/proto/h;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/q;->t:[Lcom/google/checkout/inapp/proto/h;

    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/q;->a(Landroid/text/Spanned;)V

    :goto_0
    return-void

    :cond_1
    sget v0, Lcom/google/android/gms/p;->CI:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget v3, Lcom/google/android/gms/p;->CH:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/wallet/ia/q;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Lcom/google/android/gms/wallet/common/w;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/q;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/q;->a(Landroid/text/Spanned;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/q;[Lcom/google/checkout/inapp/proto/n;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->U:Lcom/google/android/gms/wallet/ia/p;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/q;->V:Z

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/wallet/ia/p;->a([Lcom/google/checkout/inapp/proto/n;Z)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->V:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->U:Lcom/google/android/gms/wallet/ia/p;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/ia/p;->a()[Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/q;->w:Z

    if-nez v1, :cond_0

    invoke-static {v4}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->U:Lcom/google/android/gms/wallet/ia/p;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/ia/p;->a(Lcom/google/checkout/inapp/proto/a/b;)[Lcom/google/checkout/inapp/proto/j;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/gms/wallet/ia/q;->h:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v5, v4}, Lcom/google/android/gms/wallet/common/ui/v;->a([Lcom/google/checkout/inapp/proto/a/b;)V

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/q;->h:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v4, v0}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/q;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/gms/e;->g:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/q;->f:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/q;->g:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/q;->h:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v4, v3}, Lcom/google/android/gms/wallet/common/ui/v;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v4, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    move-object v0, v1

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    if-eqz v1, :cond_2

    array-length v6, v0

    move v5, v3

    :goto_1
    if-ge v5, v6, :cond_9

    aget-object v4, v0, v5

    iget-object v7, v4, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    iget-object v8, v1, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v4, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    const/4 v1, 0x1

    move v9, v1

    move-object v1, v4

    move v4, v9

    :goto_2
    if-nez v4, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v2, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    move-object v1, v2

    :cond_2
    if-nez v1, :cond_3

    iget-boolean v4, p0, Lcom/google/android/gms/wallet/ia/q;->v:Z

    if-nez v4, :cond_3

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v1

    :cond_3
    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/q;->b(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v2, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    move-object v1, v2

    :cond_4
    array-length v4, v0

    move v2, v3

    :goto_3
    if-ge v2, v4, :cond_7

    aget-object v5, v0, v2

    invoke-static {v5}, Lcom/google/android/gms/wallet/ia/q;->b(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v5

    if-nez v5, :cond_7

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->U:Lcom/google/android/gms/wallet/ia/p;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ia/p;->b()[Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    goto :goto_0

    :cond_6
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/q;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v2, v0}, Lcom/google/android/gms/wallet/common/ui/cc;->a([Lcom/google/checkout/inapp/proto/j;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->d:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0, v3}, Lcom/google/android/gms/wallet/common/ui/cc;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->m:Landroid/widget/TextView;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->i()V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->G:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_9
    move v4, v3

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/q;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 77
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->i:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->j:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->j:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ClickSpan;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->i:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Lcom/google/checkout/inapp/proto/d;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 843
    if-eqz p1, :cond_0

    .line 844
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->B:Lcom/google/android/gms/wallet/ia/f;

    invoke-interface {v0, p1}, Lcom/google/android/gms/wallet/ia/f;->a(Lcom/google/checkout/inapp/proto/d;)V

    .line 845
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->a:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->setVisibility(I)V

    .line 846
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->H:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 848
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 1036
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->R:Lcom/google/android/gms/wallet/ia/u;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/wallet/ia/u;->a(Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 1037
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ia/q;->Z:Z

    .line 1041
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->aa:Lcom/google/android/apps/common/a/a/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->ab:Lcom/google/android/apps/common/a/a/h;

    if-eqz v0, :cond_0

    .line 1042
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->aa:Lcom/google/android/apps/common/a/a/i;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->ab:Lcom/google/android/apps/common/a/a/h;

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "click_to_activity_result"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/common/a/a/i;->a(Lcom/google/android/apps/common/a/a/h;[Ljava/lang/String;)Z

    .line 1044
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/q;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->aa:Lcom/google/android/apps/common/a/a/i;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/apps/common/a/a/i;)V

    .line 1045
    iput-object v5, p0, Lcom/google/android/gms/wallet/ia/q;->aa:Lcom/google/android/apps/common/a/a/i;

    .line 1046
    iput-object v5, p0, Lcom/google/android/gms/wallet/ia/q;->ab:Lcom/google/android/apps/common/a/a/h;

    .line 1048
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/q;Z)Z
    .locals 0

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/ia/q;->V:Z

    return p1
.end method

.method static synthetic a(Lcom/google/checkout/inapp/proto/j;)Z
    .locals 1

    .prologue
    .line 77
    invoke-static {p0}, Lcom/google/android/gms/wallet/ia/q;->b(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ia/q;)Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->J:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ia/q;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/q;->P:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ia/q;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/q;->N:Ljava/util/ArrayList;

    return-object p1
.end method

.method private b(Lcom/google/checkout/inapp/proto/d;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 854
    if-eqz p1, :cond_0

    .line 855
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->B:Lcom/google/android/gms/wallet/ia/f;

    invoke-interface {v0, p1}, Lcom/google/android/gms/wallet/ia/f;->b(Lcom/google/checkout/inapp/proto/d;)V

    .line 856
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->a:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->setVisibility(I)V

    .line 857
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->H:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 859
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 798
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 799
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->l:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 800
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->l:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 805
    :goto_0
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/q;->y:Ljava/lang/String;

    .line 806
    return-void

    .line 802
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 803
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->l:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 750
    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/wallet/ia/q;->T:I

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/q;->T:I

    .line 751
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->f()V

    .line 752
    return-void

    .line 750
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->K:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ia/q;Z)Z
    .locals 0

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/ia/q;->Q:Z

    return p1
.end method

.method private static b(Lcom/google/checkout/inapp/proto/j;)Z
    .locals 1

    .prologue
    .line 1147
    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/wallet/common/w;->d(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/ia/q;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/q;->d(Ljava/lang/String;)V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1031
    sget v0, Lcom/google/android/gms/p;->Cl:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/q;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/ia/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1033
    return-void
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->L:Lcom/google/checkout/inapp/proto/q;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/ia/q;)Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->Q:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/ia/q;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->S:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 406
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->z:Z

    .line 407
    iput v0, p0, Lcom/google/android/gms/wallet/ia/q;->T:I

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->f()V

    .line 408
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/q;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "inapp.ReviewPurchaseFragment.PurchaseOptionsNetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->o:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 411
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/q;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "inapp.ReviewPurchaseFragment.PurchaseRequestNetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->p:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 415
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->s:Lcom/google/android/gms/wallet/service/k;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->ae:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/android/gms/wallet/service/l;)V

    .line 416
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->o:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_1

    .line 417
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->o:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 422
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->p:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->p:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->s:Lcom/google/android/gms/wallet/service/k;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->ae:Lcom/google/android/gms/wallet/service/l;

    iget v2, p0, Lcom/google/android/gms/wallet/ia/q;->A:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/android/gms/wallet/service/l;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ia/q;->A:I

    .line 426
    return-void

    .line 419
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->j()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/ia/q;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/q;->c(Ljava/lang/String;)V

    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 1051
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->R:Lcom/google/android/gms/wallet/ia/u;

    invoke-interface {v0, p1}, Lcom/google/android/gms/wallet/ia/u;->a(Ljava/lang/String;)V

    .line 1052
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ia/q;->Z:Z

    .line 1054
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->aa:Lcom/google/android/apps/common/a/a/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->ab:Lcom/google/android/apps/common/a/a/h;

    if-eqz v0, :cond_0

    .line 1055
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->aa:Lcom/google/android/apps/common/a/a/i;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->ab:Lcom/google/android/apps/common/a/a/h;

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "click_to_activity_result"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/common/a/a/i;->a(Lcom/google/android/apps/common/a/a/h;[Ljava/lang/String;)Z

    .line 1057
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/q;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->aa:Lcom/google/android/apps/common/a/a/i;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/apps/common/a/a/i;)V

    .line 1058
    iput-object v5, p0, Lcom/google/android/gms/wallet/ia/q;->aa:Lcom/google/android/apps/common/a/a/i;

    .line 1059
    iput-object v5, p0, Lcom/google/android/gms/wallet/ia/q;->ab:Lcom/google/android/apps/common/a/a/h;

    .line 1061
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/checkout/inapp/proto/q;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->L:Lcom/google/checkout/inapp/proto/q;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 436
    iget v0, p0, Lcom/google/android/gms/wallet/ia/q;->A:I

    if-gez v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->s:Lcom/google/android/gms/wallet/service/k;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->ae:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->c(Lcom/google/android/gms/wallet/service/l;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/q;->A:I

    .line 441
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/ia/q;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/q;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/wallet/ia/q;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->O:Ljava/util/ArrayList;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 729
    iget v0, p0, Lcom/google/android/gms/wallet/ia/q;->T:I

    if-lez v0, :cond_2

    move v0, v1

    .line 731
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->g()Z

    move-result v3

    if-eq v0, v3, :cond_1

    .line 732
    if-eqz v0, :cond_3

    .line 733
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 734
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/cc;->setEnabled(Z)V

    .line 735
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->V:Z

    if-eqz v0, :cond_0

    .line 736
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->h:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/v;->setEnabled(Z)V

    .line 745
    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->h()V

    .line 747
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 729
    goto :goto_0

    .line 739
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->b:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 740
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->setEnabled(Z)V

    .line 741
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->V:Z

    if-eqz v0, :cond_0

    .line 742
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->h:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/v;->setEnabled(Z)V

    goto :goto_1
.end method

.method static synthetic g(Lcom/google/android/gms/wallet/ia/q;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->i()V

    return-void
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 761
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic h(Lcom/google/android/gms/wallet/ia/q;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->P:Ljava/lang/String;

    return-object v0
.end method

.method private h()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 765
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->V:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 767
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/q;->c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {v3, v2}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 768
    return-void

    :cond_1
    move v0, v1

    .line 765
    goto :goto_0

    :cond_2
    move v2, v1

    .line 767
    goto :goto_1
.end method

.method static synthetic i(Lcom/google/android/gms/wallet/ia/q;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->N:Ljava/util/ArrayList;

    return-object v0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 949
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    .line 951
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/q;->V:Z

    if-eqz v1, :cond_2

    .line 952
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->U:Lcom/google/android/gms/wallet/ia/p;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/wallet/ia/p;->a(Lcom/google/checkout/inapp/proto/a/b;Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/n;

    move-result-object v0

    .line 957
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/n;->c:Lcom/google/checkout/inapp/proto/d;

    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ia/q;->b(Lcom/google/checkout/inapp/proto/d;)V

    :cond_0
    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->r:Lcom/google/checkout/inapp/proto/n;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->f()V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->h()V

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/q;->a(Ljava/lang/String;)V

    .line 958
    :cond_1
    return-void

    .line 955
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->U:Lcom/google/android/gms/wallet/ia/p;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/ia/p;->a(Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/n;

    move-result-object v0

    goto :goto_0
.end method

.method private j()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1006
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->C:Lcom/google/checkout/inapp/proto/al;

    if-eqz v0, :cond_1

    .line 1007
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->ae:Lcom/google/android/gms/wallet/service/l;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->C:Lcom/google/checkout/inapp/proto/al;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/inapp/proto/al;)V

    .line 1024
    :cond_0
    :goto_0
    return-void

    .line 1011
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->W:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/q;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1012
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->X:Z

    .line 1013
    new-instance v0, Lcom/google/checkout/inapp/proto/ak;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/ak;-><init>()V

    .line 1014
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1015
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->K:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/ak;->b:Ljava/lang/String;

    .line 1017
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1018
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->L:Lcom/google/checkout/inapp/proto/q;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/ak;->a:Lcom/google/checkout/inapp/proto/q;

    .line 1020
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->s:Lcom/google/android/gms/wallet/service/k;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/checkout/inapp/proto/ak;)V

    .line 1021
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v2, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->a:Z

    .line 1022
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/ia/q;->b(Z)V

    goto :goto_0
.end method

.method static synthetic j(Lcom/google/android/gms/wallet/ia/q;)Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->X:Z

    return v0
.end method

.method static synthetic k(Lcom/google/android/gms/wallet/ia/q;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->M:Ljava/lang/String;

    return-object v0
.end method

.method private k()V
    .locals 1

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->R:Lcom/google/android/gms/wallet/ia/u;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/ia/u;->g()V

    .line 1065
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->Z:Z

    .line 1066
    return-void
.end method

.method static synthetic l(Lcom/google/android/gms/wallet/ia/q;)Z
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->b()Z

    move-result v0

    return v0
.end method

.method static synthetic m(Lcom/google/android/gms/wallet/ia/q;)V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/q;->b(Z)V

    return-void
.end method

.method static synthetic n(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/android/apps/common/a/a/i;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->ac:Lcom/google/android/apps/common/a/a/i;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/android/apps/common/a/a/h;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->ad:Lcom/google/android/apps/common/a/a/h;

    return-object v0
.end method

.method static synthetic p(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/android/apps/common/a/a/i;
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->ac:Lcom/google/android/apps/common/a/a/i;

    return-object v0
.end method

.method static synthetic q(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/android/apps/common/a/a/h;
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->ad:Lcom/google/android/apps/common/a/a/h;

    return-object v0
.end method

.method static synthetic r(Lcom/google/android/gms/wallet/ia/q;)V
    .locals 1

    .prologue
    .line 77
    const-string v0, "com.google.android.libraries.inapp.ERROR_PAYMENT_FAILED"

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/q;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic s(Lcom/google/android/gms/wallet/ia/q;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->j()V

    return-void
.end method

.method static synthetic t(Lcom/google/android/gms/wallet/ia/q;)Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->W:Z

    return v0
.end method

.method static synthetic u(Lcom/google/android/gms/wallet/ia/q;)Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->X:Z

    return v0
.end method

.method static synthetic v(Lcom/google/android/gms/wallet/ia/q;)V
    .locals 3

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->p:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/q;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->p:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->p:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->p:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->p:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/q;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "inapp.ReviewPurchaseFragment.PurchaseRequestNetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic w(Lcom/google/android/gms/wallet/ia/q;)V
    .locals 3

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->o:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/q;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->o:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->o:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->o:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->o:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/q;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "inapp.ReviewPurchaseFragment.PurchaseOptionsNetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method final a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 982
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->aa:Lcom/google/android/apps/common/a/a/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->ab:Lcom/google/android/apps/common/a/a/h;

    if-nez v0, :cond_1

    .line 983
    :cond_0
    new-instance v0, Lcom/google/android/apps/common/a/a/i;

    const-string v1, "purchase"

    invoke-direct {v0, v1}, Lcom/google/android/apps/common/a/a/i;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->aa:Lcom/google/android/apps/common/a/a/i;

    .line 984
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->aa:Lcom/google/android/apps/common/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/common/a/a/i;->a()Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->ab:Lcom/google/android/apps/common/a/a/h;

    .line 987
    :cond_1
    new-instance v1, Lcom/google/checkout/inapp/proto/am;

    invoke-direct {v1}, Lcom/google/checkout/inapp/proto/am;-><init>()V

    .line 989
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 990
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->r:Lcom/google/checkout/inapp/proto/n;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/n;->c:Lcom/google/checkout/inapp/proto/d;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/d;->a:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/checkout/inapp/proto/am;->b:Ljava/lang/String;

    .line 991
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->K:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/checkout/inapp/proto/am;->e:Ljava/lang/String;

    .line 998
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->t:[Lcom/google/checkout/inapp/proto/h;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/q;->t:[Lcom/google/checkout/inapp/proto/h;

    array-length v2, v2

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/checkout/inapp/proto/h;

    iput-object v0, v1, Lcom/google/checkout/inapp/proto/am;->c:[Lcom/google/checkout/inapp/proto/h;

    .line 1000
    invoke-direct {p0, v3}, Lcom/google/android/gms/wallet/ia/q;->b(Z)V

    .line 1001
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->s:Lcom/google/android/gms/wallet/service/k;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/checkout/inapp/proto/am;)V

    .line 1002
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v3, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    .line 1003
    return-void

    .line 992
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 993
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->L:Lcom/google/checkout/inapp/proto/q;

    iput-object v0, v1, Lcom/google/checkout/inapp/proto/am;->a:Lcom/google/checkout/inapp/proto/q;

    .line 994
    new-instance v0, Lcom/google/checkout/inapp/proto/r;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/r;-><init>()V

    iput-object v0, v1, Lcom/google/checkout/inapp/proto/am;->d:Lcom/google/checkout/inapp/proto/r;

    .line 995
    iget-object v0, v1, Lcom/google/checkout/inapp/proto/am;->d:Lcom/google/checkout/inapp/proto/r;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    iget-object v2, v2, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/checkout/inapp/proto/r;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 690
    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_0

    .line 691
    packed-switch p1, :pswitch_data_0

    .line 700
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/q;->b(Z)V

    .line 706
    :goto_0
    return-void

    .line 693
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->j()V

    goto :goto_0

    .line 696
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->k()V

    goto :goto_0

    .line 704
    :cond_0
    const-string v0, "com.google.android.libraries.inapp.ERROR_PAYMENT_FAILED"

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/q;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 691
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 783
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 784
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->n:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 785
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->n:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 790
    :goto_0
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/q;->x:Ljava/lang/String;

    .line 791
    return-void

    .line 787
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->n:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 788
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->n:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 720
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->cc:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 721
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v4, Lcom/google/android/gms/j;->te:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 722
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 723
    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 724
    return-void

    :cond_0
    move v0, v2

    .line 722
    goto :goto_0

    :cond_1
    move v1, v2

    .line 723
    goto :goto_1
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 377
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 378
    new-instance v0, Lcom/google/android/gms/wallet/ia/p;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/ia/p;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->U:Lcom/google/android/gms/wallet/ia/p;

    .line 379
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->y:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/q;->b(Ljava/lang/String;)V

    .line 380
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->x:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/q;->a(Ljava/lang/String;)V

    .line 382
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->K:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->I:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/l;->a(Ljava/lang/String;Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Lcom/google/android/gms/wallet/common/DisplayHints;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->Y:Lcom/google/android/gms/wallet/common/DisplayHints;

    .line 385
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->Y:Lcom/google/android/gms/wallet/common/DisplayHints;

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->Y:Lcom/google/android/gms/wallet/common/DisplayHints;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/DisplayHints;->c()Lcom/google/checkout/inapp/proto/d;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/checkout/inapp/proto/d;)V

    .line 387
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->Y:Lcom/google/android/gms/wallet/common/DisplayHints;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/DisplayHints;->c()Lcom/google/checkout/inapp/proto/d;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/q;->b(Lcom/google/checkout/inapp/proto/d;)V

    .line 388
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->a:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->Y:Lcom/google/android/gms/wallet/common/DisplayHints;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/DisplayHints;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(Ljava/lang/String;)V

    .line 389
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->Y:Lcom/google/android/gms/wallet/common/DisplayHints;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/DisplayHints;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->q:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->q:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 392
    :cond_0
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 546
    iput-boolean v3, p0, Lcom/google/android/gms/wallet/ia/q;->W:Z

    .line 548
    packed-switch p1, :pswitch_data_0

    .line 678
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->Z:Z

    if-nez v0, :cond_0

    .line 680
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->z:Z

    if-eqz v0, :cond_0

    .line 681
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->d()V

    .line 685
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 686
    :goto_1
    return-void

    .line 550
    :pswitch_0
    if-ne p2, v0, :cond_1

    .line 551
    const-string v0, "ReviewPurchaseFragment"

    const-string v2, "Successfully added an instrument"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v2, Lcom/google/checkout/inapp/proto/j;

    invoke-static {p3, v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 556
    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->C:Lcom/google/checkout/inapp/proto/al;

    .line 557
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    goto :goto_0

    .line 558
    :cond_1
    if-nez p2, :cond_2

    .line 559
    const-string v0, "ReviewPurchaseFragment"

    const-string v1, "User canceled adding an instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 562
    :cond_2
    const-string v0, "ReviewPurchaseFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed adding an instrument resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 567
    :pswitch_1
    packed-switch p2, :pswitch_data_1

    .line 582
    const-string v0, "ReviewPurchaseFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed updating an instrument resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 569
    :pswitch_2
    const-string v0, "ReviewPurchaseFragment"

    const-string v2, "Successfully updated an instrument"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    const-string v0, "com.google.android.gms.wallet.instrument"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    const-class v2, Lcom/google/checkout/inapp/proto/j;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 574
    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->C:Lcom/google/checkout/inapp/proto/al;

    .line 575
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    goto :goto_0

    .line 578
    :pswitch_3
    const-string v0, "ReviewPurchaseFragment"

    const-string v1, "User canceled updating an instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 588
    :pswitch_4
    if-ne p2, v0, :cond_4

    .line 589
    const-string v0, "ReviewPurchaseFragment"

    const-string v2, "Successfully upgraded instrument"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 590
    const-string v0, "com.google.android.gms.wallet.instrument"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    const-class v2, Lcom/google/checkout/inapp/proto/j;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 594
    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->C:Lcom/google/checkout/inapp/proto/al;

    .line 598
    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v2

    if-nez v2, :cond_3

    move-object v0, v1

    .line 604
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    goto/16 :goto_0

    .line 602
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    goto :goto_2

    .line 605
    :cond_4
    if-nez p2, :cond_5

    .line 607
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->k()V

    goto/16 :goto_0

    .line 610
    :cond_5
    const-string v0, "com.google.android.libraries.inapp.ERROR_PAYMENT_FAILED"

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/q;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 615
    :pswitch_5
    if-ne p2, v0, :cond_6

    .line 616
    const-string v0, "ReviewPurchaseFragment"

    const-string v2, "Successfully signed up for Wallet"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->C:Lcom/google/checkout/inapp/proto/al;

    .line 619
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 620
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ia/q;->d(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 623
    :cond_6
    if-nez p2, :cond_8

    .line 625
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->k()V

    .line 630
    :cond_7
    :goto_3
    invoke-direct {p0, v3}, Lcom/google/android/gms/wallet/ia/q;->b(Z)V

    goto/16 :goto_0

    .line 628
    :cond_8
    const-string v0, "com.google.android.libraries.inapp.ERROR_PAYMENT_FAILED"

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/q;->c(Ljava/lang/String;)V

    goto :goto_3

    .line 634
    :pswitch_6
    packed-switch p2, :pswitch_data_2

    .line 650
    const-string v0, "ReviewPurchaseFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed adding an address resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 636
    :pswitch_7
    const-string v0, "ReviewPurchaseFragment"

    const-string v2, "Successfully added an address"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    const-string v0, "com.google.android.gms.wallet.address"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    const-class v2, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 642
    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->C:Lcom/google/checkout/inapp/proto/al;

    .line 643
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    goto/16 :goto_0

    .line 646
    :pswitch_8
    const-string v0, "ReviewPurchaseFragment"

    const-string v1, "User canceled adding an address"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 656
    :pswitch_9
    packed-switch p2, :pswitch_data_3

    .line 672
    const-string v0, "ReviewPurchaseFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed updating an address resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 658
    :pswitch_a
    const-string v0, "ReviewPurchaseFragment"

    const-string v2, "Successfully updated an address"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    const-string v0, "com.google.android.gms.wallet.address"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    const-class v2, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 664
    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->C:Lcom/google/checkout/inapp/proto/al;

    .line 665
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    goto/16 :goto_0

    .line 668
    :pswitch_b
    const-string v0, "ReviewPurchaseFragment"

    const-string v1, "User canceled updating an address"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 548
    nop

    :pswitch_data_0
    .packed-switch 0x1f5
        :pswitch_0
        :pswitch_5
        :pswitch_1
        :pswitch_6
        :pswitch_9
        :pswitch_4
    .end packed-switch

    .line 567
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 634
    :pswitch_data_2
    .packed-switch -0x1
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 656
    :pswitch_data_3
    .packed-switch -0x1
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 6

    .prologue
    .line 311
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 314
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/wallet/ia/u;

    move-object v1, v0

    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->R:Lcom/google/android/gms/wallet/ia/u;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 320
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/q;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    .line 321
    const-string v1, "buyFlowConfig"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->I:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 322
    const-string v1, "account"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/accounts/Account;

    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->J:Landroid/accounts/Account;

    .line 323
    const-string v1, "extraJwt"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->K:Ljava/lang/String;

    .line 324
    const-string v1, "extraPcid"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 325
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 326
    new-instance v3, Lcom/google/checkout/inapp/proto/q;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/q;-><init>()V

    iput-object v3, p0, Lcom/google/android/gms/wallet/ia/q;->L:Lcom/google/checkout/inapp/proto/q;

    .line 327
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/q;->L:Lcom/google/checkout/inapp/proto/q;

    iput-object v1, v3, Lcom/google/checkout/inapp/proto/q;->a:Ljava/lang/String;

    .line 329
    :cond_0
    const-string v3, "extraInfoText"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/wallet/ia/q;->M:Ljava/lang/String;

    .line 331
    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/q;->K:Ljava/lang/String;

    if-eqz v2, :cond_3

    if-eqz v1, :cond_3

    .line 332
    const-string v1, "ReviewPurchaseFragment"

    const-string v2, "Cannot specify both PCID and JWT"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    const-string v1, "com.google.android.libraries.inapp.ERROR_CODE_INVALID_PARAMETER"

    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ia/q;->c(Ljava/lang/String;)V

    .line 339
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->s:Lcom/google/android/gms/wallet/service/k;

    if-nez v1, :cond_2

    .line 340
    new-instance v1, Lcom/google/android/gms/wallet/service/e;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/q;->I:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/q;->J:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/q;->getActivity()Landroid/support/v4/app/q;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/q;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/wallet/service/e;-><init>(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->s:Lcom/google/android/gms/wallet/service/k;

    .line 343
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->s:Lcom/google/android/gms/wallet/service/k;

    invoke-interface {v1}, Lcom/google/android/gms/wallet/service/k;->a()V

    .line 345
    :cond_2
    return-void

    .line 315
    :catch_0
    move-exception v1

    .line 316
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " must implement OnPurchaseEndedListener interface!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 334
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/q;->K:Ljava/lang/String;

    if-nez v2, :cond_1

    if-nez v1, :cond_1

    .line 335
    const-string v1, "ReviewPurchaseFragment"

    const-string v2, "Cannot leave out both PCID and JWT"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    const-string v1, "com.google.android.libraries.inapp.ERROR_CODE_INVALID_PARAMETER"

    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ia/q;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 711
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->pB:I

    if-ne v0, v1, :cond_0

    .line 712
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/q;->b(Ljava/lang/String;)V

    .line 713
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/q;->a()V

    .line 715
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 349
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 350
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/q;->setRetainInstance(Z)V

    .line 352
    if-eqz p1, :cond_2

    .line 353
    const-string v0, "model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    .line 354
    const-string v0, "waitingForActivityResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->W:Z

    .line 356
    const-string v0, "purchaseOptionsPostResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    const-string v0, "purchaseOptionsPostResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    const-class v1, Lcom/google/checkout/inapp/proto/al;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/al;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->C:Lcom/google/checkout/inapp/proto/al;

    .line 361
    :cond_0
    const-string v0, "serviceConnectionSavePoint"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 362
    const-string v0, "serviceConnectionSavePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/q;->A:I

    .line 373
    :cond_1
    :goto_0
    return-void

    .line 366
    :cond_2
    new-instance v0, Lcom/google/android/apps/common/a/a/i;

    const-string v1, "get_purchase_options"

    invoke-direct {v0, v1}, Lcom/google/android/apps/common/a/a/i;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->ac:Lcom/google/android/apps/common/a/a/i;

    .line 367
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->ac:Lcom/google/android/apps/common/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/common/a/a/i;->a()Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->ad:Lcom/google/android/apps/common/a/a/h;

    .line 369
    new-instance v0, Lcom/google/android/gms/wallet/common/PaymentModel;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/PaymentModel;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    .line 370
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->W:Z

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 467
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 468
    sget v0, Lcom/google/android/gms/l;->gI:I

    invoke-virtual {p1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    .line 473
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->ch:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->l:Landroid/widget/TextView;

    .line 474
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->pO:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->b:Landroid/widget/ProgressBar;

    .line 475
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->kA:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->d:Landroid/view/View;

    .line 477
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->kz:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cc;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    .line 479
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->D:Lcom/google/android/gms/wallet/common/ui/cd;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/android/gms/wallet/common/ui/cd;)V

    .line 480
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->au:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->f:Landroid/view/View;

    .line 481
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->aw:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->g:Landroid/view/View;

    .line 482
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->at:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/v;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->h:Lcom/google/android/gms/wallet/common/ui/v;

    .line 483
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->h:Lcom/google/android/gms/wallet/common/ui/v;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->E:Lcom/google/android/gms/wallet/common/ui/w;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/android/gms/wallet/common/ui/w;)V

    .line 484
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->cp:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->i:Landroid/view/View;

    .line 485
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->co:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->j:Landroid/widget/TextView;

    .line 486
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->fB:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->k:Landroid/widget/TextView;

    .line 487
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->cj:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    .line 488
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->cD:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ia/CartDetailsView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->a:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    .line 489
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->M:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 490
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->kq:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->m:Landroid/widget/TextView;

    .line 491
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->m:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 493
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->kw:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->n:Landroid/widget/TextView;

    .line 494
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->rW:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->G:Landroid/view/View;

    .line 495
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->ej:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->H:Landroid/view/View;

    .line 496
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->qt:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->q:Landroid/widget/TextView;

    .line 498
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 499
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->fQ:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    .line 501
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->a:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bj;)V

    .line 502
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/bj;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bj;)V

    .line 503
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->h:Lcom/google/android/gms/wallet/common/ui/v;

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/bj;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bj;)V

    .line 504
    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bp;)V

    .line 507
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/view/View$OnClickListener;)V

    .line 509
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 514
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->I:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->f()Landroid/os/Bundle;

    move-result-object v0

    .line 515
    if-eqz v0, :cond_6

    .line 516
    const-string v1, "com.google.android.libraries.inapp.EXTRA_UI_TEMPLATE"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 517
    const-string v1, "com.google.android.libraries.inapp.EXTRA_USAGE_UNIT"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 518
    const-string v1, "com.google.android.gms.wallet.freeUsageAmount"

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 519
    const-string v4, "com.google.android.gms.wallet.usageDiscountMessage"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 521
    :goto_1
    packed-switch v2, :pswitch_data_0

    .line 534
    new-instance v0, Lcom/google/android/gms/wallet/ia/l;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->a:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/ia/l;-><init>(Lcom/google/android/gms/wallet/ia/CartDetailsView;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->B:Lcom/google/android/gms/wallet/ia/f;

    .line 541
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    return-object v0

    .line 471
    :cond_3
    sget v0, Lcom/google/android/gms/l;->gH:I

    invoke-virtual {p1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->F:Landroid/view/View;

    goto/16 :goto_0

    .line 523
    :pswitch_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 524
    new-instance v1, Lcom/google/android/gms/wallet/ia/v;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/q;->a:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/gms/wallet/ia/v;-><init>(Lcom/google/android/gms/wallet/ia/CartDetailsView;ILjava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->B:Lcom/google/android/gms/wallet/ia/f;

    .line 530
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    sget v1, Lcom/google/android/gms/p;->zq:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/ia/q;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 527
    :cond_4
    new-instance v0, Lcom/google/android/gms/wallet/ia/v;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/q;->a:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/gms/wallet/ia/v;-><init>(Lcom/google/android/gms/wallet/ia/CartDetailsView;II)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->B:Lcom/google/android/gms/wallet/ia/f;

    goto :goto_3

    .line 537
    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 538
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    sget v1, Lcom/google/android/gms/p;->zq:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/ia/q;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_6
    move-object v0, v2

    move v1, v3

    move v2, v3

    goto :goto_1

    .line 521
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onPause()V
    .locals 0

    .prologue
    .line 445
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 446
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->e()V

    .line 447
    return-void
.end method

.method public final onResume()V
    .locals 1

    .prologue
    .line 396
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 398
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->W:Z

    if-eqz v0, :cond_0

    .line 399
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->z:Z

    .line 403
    :goto_0
    return-void

    .line 401
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->d()V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 451
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 453
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/q;->e()V

    .line 455
    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/ia/q;->A:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 456
    const-string v0, "model"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 457
    const-string v0, "waitingForActivityResult"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/q;->W:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 458
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/q;->C:Lcom/google/checkout/inapp/proto/al;

    if-eqz v0, :cond_0

    .line 459
    const-string v0, "purchaseOptionsPostResponse"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/q;->C:Lcom/google/checkout/inapp/proto/al;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 462
    :cond_0
    return-void
.end method

.method public final startActivityForResult(Landroid/content/Intent;I)V
    .locals 3

    .prologue
    .line 1190
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/q;->W:Z

    .line 1191
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1192
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/q;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/b;->E:I

    sget v2, Lcom/google/android/gms/b;->C:I

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/q;->overridePendingTransition(II)V

    .line 1193
    return-void
.end method
