.class public final Lcom/google/android/gms/drive/api/a/y;
.super Lcom/google/android/gms/drive/api/a/b;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/api/f;

.field private final f:Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;

.field private final g:I

.field private final h:Lcom/google/android/gms/drive/realtime/f;

.field private final i:Lcom/google/android/gms/drive/g/aw;

.field private final j:Lcom/google/android/gms/drive/realtime/b/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/api/f;Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;Lcom/google/android/gms/drive/internal/ca;I)V
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0, p1, p4}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V

    .line 64
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/y;->e:Lcom/google/android/gms/drive/api/f;

    .line 65
    iput-object p3, p0, Lcom/google/android/gms/drive/api/a/y;->f:Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;

    .line 66
    iput p5, p0, Lcom/google/android/gms/drive/api/a/y;->g:I

    .line 67
    invoke-interface {p1}, Lcom/google/android/gms/drive/api/d;->q()Lcom/google/android/gms/drive/g/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/a/y;->i:Lcom/google/android/gms/drive/g/aw;

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/y;->i:Lcom/google/android/gms/drive/g/aw;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->f()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/y;->i:Lcom/google/android/gms/drive/g/aw;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/realtime/b/a;->a(Landroid/content/Context;Lcom/google/android/gms/drive/g/aw;)Lcom/google/android/gms/drive/realtime/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/a/y;->j:Lcom/google/android/gms/drive/realtime/b/a;

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/y;->j:Lcom/google/android/gms/drive/realtime/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/realtime/b/a;->b()Lcom/google/android/gms/drive/realtime/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/a/y;->h:Lcom/google/android/gms/drive/realtime/f;

    .line 71
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/api/a/y;)Lcom/google/android/gms/drive/realtime/b/a;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/y;->j:Lcom/google/android/gms/drive/realtime/b/a;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/drive/DriveId;Lcom/google/c/a/a/a/c/u;Lcom/google/android/gms/drive/realtime/j;)V
    .locals 7

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/y;->j:Lcom/google/android/gms/drive/realtime/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/realtime/b/a;->d()Lcom/google/android/gms/drive/realtime/b;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/drive/realtime/e;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/y;->a:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v2}, Lcom/google/android/gms/drive/api/d;->a()Lcom/google/android/gms/drive/auth/g;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lcom/google/android/gms/drive/realtime/e;-><init>(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/auth/g;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/realtime/b;->c(Lcom/google/android/gms/drive/realtime/e;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0xa

    const-string v2, "This document is already opened. Documents may not be opened more than once."

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {p3, v0}, Lcom/google/android/gms/drive/realtime/j;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 201
    :goto_0
    return-void

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/y;->h:Lcom/google/android/gms/drive/realtime/f;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/y;->a:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v1}, Lcom/google/android/gms/drive/api/d;->a()Lcom/google/android/gms/drive/auth/g;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/y;->f:Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;->b()Z

    move-result v5

    iget v6, p0, Lcom/google/android/gms/drive/api/a/y;->g:I

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/drive/realtime/f;->a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/auth/g;Lcom/google/c/a/a/a/c/u;Lcom/google/android/gms/drive/realtime/j;ZI)V

    goto :goto_0
.end method

.method private a(Lcom/google/c/a/a/a/c/u;Lcom/google/android/gms/drive/realtime/j;)Z
    .locals 3

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/y;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/y;->f:Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/api/d;->b(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 177
    if-nez v0, :cond_0

    .line 178
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0xa

    const-string v2, "Drive resource not found."

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;)V

    throw v0

    .line 181
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    .line 182
    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 183
    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/drive/api/a/y;->a(Lcom/google/android/gms/drive/DriveId;Lcom/google/c/a/a/a/c/u;Lcom/google/android/gms/drive/realtime/j;)V

    .line 184
    const/4 v0, 0x1

    .line 186
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/drive/api/a/y;)Lcom/google/android/gms/drive/api/f;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/y;->e:Lcom/google/android/gms/drive/api/f;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/drive/api/a/y;)Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/y;->f:Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x7

    const/4 v7, 0x1

    .line 76
    new-instance v0, Lcom/google/android/gms/drive/realtime/a;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/drive/realtime/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V

    .line 79
    new-instance v1, Lcom/google/android/gms/drive/api/a/z;

    invoke-direct {v1, p0}, Lcom/google/android/gms/drive/api/a/z;-><init>(Lcom/google/android/gms/drive/api/a/y;)V

    .line 121
    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/y;->f:Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 122
    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/y;->f:Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/gms/drive/api/a/y;->a(Lcom/google/android/gms/drive/DriveId;Lcom/google/c/a/a/a/c/u;Lcom/google/android/gms/drive/realtime/j;)V

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/api/a/y;->a(Lcom/google/c/a/a/a/c/u;Lcom/google/android/gms/drive/realtime/j;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 133
    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/y;->i:Lcom/google/android/gms/drive/g/aw;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/g/aw;->e()Lcom/google/android/gms/drive/g/n;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/drive/g/n;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 134
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const-string v1, "getRealtimeDocument requires an active network connection."

    invoke-direct {v0, v8, v1, v9}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/api/a/y;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0

    .line 142
    :cond_2
    new-instance v2, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v2, v7}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 143
    new-instance v3, Lcom/google/android/gms/drive/api/a/aa;

    invoke-direct {v3, p0, v2}, Lcom/google/android/gms/drive/api/a/aa;-><init>(Lcom/google/android/gms/drive/api/a/y;Ljava/util/concurrent/CountDownLatch;)V

    .line 155
    iget-object v4, p0, Lcom/google/android/gms/drive/api/a/y;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v5, p0, Lcom/google/android/gms/drive/api/a/y;->f:Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v5

    invoke-interface {v4, v5, v7, v3}, Lcom/google/android/gms/drive/api/d;->a(Lcom/google/android/gms/drive/DriveId;ILcom/google/android/gms/drive/internal/cd;)V

    .line 158
    const-wide/16 v4, 0xa

    :try_start_0
    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v6}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/y;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v4, p0, Lcom/google/android/gms/drive/api/a/y;->f:Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v4

    invoke-interface {v2, v4, v7, v3}, Lcom/google/android/gms/drive/api/d;->b(Lcom/google/android/gms/drive/DriveId;ILcom/google/android/gms/drive/internal/cd;)V

    .line 164
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/api/a/y;->a(Lcom/google/c/a/a/a/c/u;Lcom/google/android/gms/drive/realtime/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const-string v1, "getRealtimeDocument requires an active network connection."

    invoke-direct {v0, v8, v1, v9}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/api/a/y;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_1
.end method
