.class public final Lcom/google/android/gms/drive/metadata/sync/syncadapter/MetadataSyncService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;

.field private b:Lcom/google/android/gms/drive/metadata/sync/syncadapter/i;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 30
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 78
    const-string v0, "MetadataSyncService"

    invoke-static {v0, p1, p0}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 79
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;ILandroid/content/SyncResult;)V
    .locals 18

    .prologue
    .line 55
    new-instance v11, Lcom/google/android/gms/drive/g/ac;

    const-string v2, "MetadataSyncService"

    move-object/from16 v0, p0

    invoke-direct {v11, v0, v2}, Lcom/google/android/gms/drive/g/ac;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 57
    :try_start_0
    sget-object v2, Lcom/google/android/gms/drive/ai;->af:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, v11, Lcom/google/android/gms/drive/g/ac;->a:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v6, 0x3e8

    mul-long/2addr v2, v6

    invoke-virtual {v4, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/drive/metadata/sync/syncadapter/o; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/af/c/a/c; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/google/af/c/a/a; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    iget-object v2, v11, Lcom/google/android/gms/drive/g/ac;->b:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v2, :cond_0

    iget-object v2, v11, Lcom/google/android/gms/drive/g/ac;->b:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/gms/drive/metadata/sync/syncadapter/o; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/google/af/c/a/c; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lcom/google/af/c/a/a; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 58
    :cond_0
    :try_start_2
    move-object/from16 v0, p3

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v4, 0x0

    iput-wide v4, v2, Landroid/content/SyncStats;->numEntries:J

    .line 59
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/MetadataSyncService;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;

    iget-object v3, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->d:Lcom/google/android/gms/drive/c/b;

    invoke-interface {v3}, Lcom/google/android/gms/drive/c/b;->c()Lcom/google/android/gms/drive/c/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/drive/c/a;->c()Lcom/google/android/gms/drive/c/a;

    move-result-object v3

    const/4 v4, 0x1

    const/16 v5, 0x17

    invoke-interface {v3, v4, v5}, Lcom/google/android/gms/drive/c/a;->a(II)Lcom/google/android/gms/drive/c/a;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v3, v0}, Lcom/google/android/gms/drive/c/a;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/c/a;

    move-result-object v10

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/gms/drive/metadata/sync/syncadapter/o; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/google/af/c/a/c; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Lcom/google/af/c/a/a; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-wide v12

    :try_start_3
    iget-object v3, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a:Lcom/google/android/gms/drive/database/r;

    move-object/from16 v0, p1

    invoke-interface {v3, v0}, Lcom/google/android/gms/drive/database/r;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/a;

    move-result-object v5

    iget-object v3, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a:Lcom/google/android/gms/drive/database/r;

    move-object/from16 v0, p1

    invoke-interface {v3, v0}, Lcom/google/android/gms/drive/database/r;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/c;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/drive/database/model/c;->f()Ljava/util/Date;

    move-result-object v4

    sget-object v3, Lcom/google/android/gms/drive/ai;->ag:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-nez v4, :cond_4

    const/4 v3, 0x1

    :goto_0
    const/4 v4, 0x0

    :goto_1
    move-object/from16 v6, p3

    move/from16 v9, p2

    invoke-virtual/range {v2 .. v10}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a(ZZLcom/google/android/gms/drive/database/model/a;Landroid/content/SyncResult;ILcom/google/android/gms/drive/database/model/c;ILcom/google/android/gms/drive/c/a;)Z

    iget-object v3, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->e:Ljava/util/Set;

    move-object/from16 v0, p1

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v5

    sget-object v3, Lcom/google/android/gms/drive/g/j;->a:Lcom/google/android/gms/drive/g/j;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/g/j;->a()J

    move-result-wide v6

    sget-object v3, Lcom/google/android/gms/drive/ai;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iget-object v3, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a:Lcom/google/android/gms/drive/database/r;

    move-object/from16 v0, p1

    invoke-interface {v3, v0}, Lcom/google/android/gms/drive/database/r;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/c;->c()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a:Lcom/google/android/gms/drive/database/r;

    move-object/from16 v0, p1

    invoke-interface {v3, v0}, Lcom/google/android/gms/drive/database/r;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/c;->d()J

    move-result-wide v14

    sub-long/2addr v6, v14

    cmp-long v3, v6, v8

    if-lez v3, :cond_b

    :cond_1
    const/4 v3, 0x1

    move v4, v3

    :goto_2
    iget-object v3, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->c:Lcom/google/android/gms/drive/g/ae;

    invoke-interface {v3}, Lcom/google/android/gms/drive/g/ae;->a()Z

    move-result v3

    if-eqz v3, :cond_c

    sget-object v3, Lcom/google/android/gms/drive/g/af;->b:Lcom/google/android/gms/drive/g/af;

    :goto_3
    iget-object v6, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->b:Lcom/google/android/gms/drive/g/n;

    invoke-interface {v6}, Lcom/google/android/gms/drive/g/n;->c()Lcom/google/android/gms/drive/g/o;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/google/android/gms/drive/g/af;->a(Lcom/google/android/gms/drive/g/o;)Z

    move-result v3

    if-nez v5, :cond_2

    if-eqz v4, :cond_3

    if-eqz v3, :cond_3

    :cond_2
    sget-object v3, Lcom/google/android/gms/drive/g/j;->a:Lcom/google/android/gms/drive/g/j;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/g/j;->a()J

    move-result-wide v4

    iget-object v3, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v3}, Lcom/google/android/gms/drive/database/r;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iget-object v3, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a:Lcom/google/android/gms/drive/database/r;

    move-object/from16 v0, p1

    invoke-interface {v3, v0}, Lcom/google/android/gms/drive/database/r;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/c;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/c;->a(J)V

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/c;->i()V

    iget-object v3, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v3}, Lcom/google/android/gms/drive/database/r;->e()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    iget-object v2, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v2}, Lcom/google/android/gms/drive/database/r;->c()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_3
    :try_start_6
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, v12

    const-wide/16 v4, 0x1f4

    add-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    const-string v4, "SyncManager"

    const-string v5, "GoKart performSync took %ds"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v6, v7

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-interface {v10}, Lcom/google/android/gms/drive/c/a;->d()Lcom/google/android/gms/drive/c/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/drive/c/a;->a()V
    :try_end_6
    .catch Landroid/accounts/AuthenticatorException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lcom/google/android/gms/drive/metadata/sync/syncadapter/o; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Lcom/google/af/c/a/c; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Lcom/google/af/c/a/a; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 73
    invoke-virtual {v11}, Lcom/google/android/gms/drive/g/ac;->a()V

    .line 74
    :goto_4
    return-void

    .line 57
    :catch_0
    move-exception v2

    :try_start_7
    iget-object v3, v11, Lcom/google/android/gms/drive/g/ac;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v2
    :try_end_7
    .catch Landroid/accounts/AuthenticatorException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Lcom/google/android/gms/drive/metadata/sync/syncadapter/o; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Lcom/google/af/c/a/c; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Lcom/google/af/c/a/a; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 60
    :catch_1
    move-exception v2

    .line 61
    :try_start_8
    const-string v3, "Cannot obtain required authentication."

    invoke-static {v3, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/MetadataSyncService;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 73
    invoke-virtual {v11}, Lcom/google/android/gms/drive/g/ac;->a()V

    goto :goto_4

    .line 59
    :cond_4
    :try_start_9
    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v14

    const-wide v16, 0x7fffffffffffffffL

    cmp-long v3, v14, v16

    if-nez v3, :cond_5

    const/4 v3, 0x0

    const/4 v4, 0x1

    goto/16 :goto_1

    :cond_5
    iget-object v3, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a:Lcom/google/android/gms/drive/database/r;

    invoke-static {v3, v5}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/metadata/sync/b/e;

    move-result-object v6

    iget-object v3, v6, Lcom/google/android/gms/drive/metadata/sync/b/e;->c:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/gms/drive/database/model/bb;

    iget-object v3, v3, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    iget-wide v14, v3, Lcom/google/android/gms/drive/metadata/sync/a/d;->b:J

    int-to-long v0, v7

    move-wide/from16 v16, v0

    cmp-long v3, v14, v16

    if-gez v3, :cond_6

    iget-object v3, v6, Lcom/google/android/gms/drive/metadata/sync/b/e;->c:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/gms/drive/database/model/bb;

    iget-object v3, v3, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/metadata/sync/a/d;->c()Z

    move-result v3

    if-eqz v3, :cond_8

    :cond_6
    const/4 v3, 0x1

    move v4, v3

    :goto_5
    iget-object v3, v6, Lcom/google/android/gms/drive/metadata/sync/b/e;->d:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/gms/drive/database/model/bb;

    iget-object v3, v3, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    iget-wide v14, v3, Lcom/google/android/gms/drive/metadata/sync/a/d;->b:J

    int-to-long v0, v7

    move-wide/from16 v16, v0

    cmp-long v3, v14, v16

    if-gez v3, :cond_7

    iget-object v3, v6, Lcom/google/android/gms/drive/metadata/sync/b/e;->d:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/gms/drive/database/model/bb;

    iget-object v3, v3, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/metadata/sync/a/d;->c()Z

    move-result v3

    if-eqz v3, :cond_9

    :cond_7
    const/4 v3, 0x1

    :goto_6
    if-eqz v4, :cond_a

    if-eqz v3, :cond_a

    const/4 v3, 0x1

    goto/16 :goto_0

    :cond_8
    const/4 v3, 0x0

    move v4, v3

    goto :goto_5

    :cond_9
    const/4 v3, 0x0

    goto :goto_6

    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_b
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_2

    :cond_c
    sget-object v3, Lcom/google/android/gms/drive/g/af;->c:Lcom/google/android/gms/drive/g/af;

    goto/16 :goto_3

    :catchall_0
    move-exception v3

    iget-object v2, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v2}, Lcom/google/android/gms/drive/database/r;->c()V

    throw v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :catchall_1
    move-exception v2

    :try_start_a
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v12

    const-wide/16 v6, 0x1f4

    add-long/2addr v4, v6

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    const-string v3, "SyncManager"

    const-string v6, "GoKart performSync took %ds"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v7, v8

    invoke-static {v3, v6, v7}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-interface {v10}, Lcom/google/android/gms/drive/c/a;->d()Lcom/google/android/gms/drive/c/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/drive/c/a;->a()V

    throw v2
    :try_end_a
    .catch Landroid/accounts/AuthenticatorException; {:try_start_a .. :try_end_a} :catch_1
    .catch Lcom/google/android/gms/drive/metadata/sync/syncadapter/o; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_3
    .catch Lcom/google/af/c/a/c; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5
    .catch Lcom/google/af/c/a/a; {:try_start_a .. :try_end_a} :catch_6
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 62
    :catch_2
    move-exception v2

    .line 63
    :try_start_b
    invoke-virtual {v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/o;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/MetadataSyncService;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 73
    invoke-virtual {v11}, Lcom/google/android/gms/drive/g/ac;->a()V

    goto/16 :goto_4

    .line 64
    :catch_3
    move-exception v2

    .line 65
    :try_start_c
    const-string v3, "The sync was interrupted."

    invoke-static {v3, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/MetadataSyncService;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 73
    invoke-virtual {v11}, Lcom/google/android/gms/drive/g/ac;->a()V

    goto/16 :goto_4

    .line 66
    :catch_4
    move-exception v2

    .line 67
    :try_start_d
    const-string v3, "There was a network error."

    invoke-static {v3, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/MetadataSyncService;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 73
    invoke-virtual {v11}, Lcom/google/android/gms/drive/g/ac;->a()V

    goto/16 :goto_4

    .line 68
    :catch_5
    move-exception v2

    .line 69
    :try_start_e
    const-string v3, "There was a network error."

    invoke-static {v3, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/MetadataSyncService;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 73
    invoke-virtual {v11}, Lcom/google/android/gms/drive/g/ac;->a()V

    goto/16 :goto_4

    .line 70
    :catch_6
    move-exception v2

    .line 71
    :try_start_f
    const-string v3, "There was an error authenticating."

    invoke-static {v3, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/MetadataSyncService;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    .line 73
    invoke-virtual {v11}, Lcom/google/android/gms/drive/g/ac;->a()V

    goto/16 :goto_4

    :catchall_2
    move-exception v2

    invoke-virtual {v11}, Lcom/google/android/gms/drive/g/ac;->a()V

    throw v2
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/MetadataSyncService;->b:Lcom/google/android/gms/drive/metadata/sync/syncadapter/i;

    return-object v0
.end method

.method public final onCreate()V
    .locals 2

    .prologue
    .line 38
    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->a()Lcom/google/android/gms/drive/g/aw;

    move-result-object v0

    .line 39
    new-instance v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;-><init>(Lcom/google/android/gms/drive/g/aw;)V

    iput-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/MetadataSyncService;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;

    .line 40
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/i;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/i;-><init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/MetadataSyncService;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/MetadataSyncService;->b:Lcom/google/android/gms/drive/metadata/sync/syncadapter/i;

    .line 41
    return-void
.end method

.method public final onUnbind(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/MetadataSyncService;->stopSelf()V

    .line 51
    const/4 v0, 0x0

    return v0
.end method
