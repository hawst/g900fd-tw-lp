.class final Lcom/google/android/gms/plus/audience/bq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/audience/bl;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/audience/bl;)V
    .locals 0

    .prologue
    .line 537
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/bq;->a:Lcom/google/android/gms/plus/audience/bl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/audience/bl;B)V
    .locals 0

    .prologue
    .line 537
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/audience/bq;-><init>(Lcom/google/android/gms/plus/audience/bl;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 537
    check-cast p1, Lcom/google/android/gms/people/k;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/people/k;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bq;->a:Lcom/google/android/gms/plus/audience/bl;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/bl;->i(Lcom/google/android/gms/plus/audience/bl;)V

    :goto_0
    return-void

    :cond_1
    invoke-interface {p1}, Lcom/google/android/gms/people/k;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/people/k;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bq;->a:Lcom/google/android/gms/plus/audience/bl;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/bl;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.common.audience.EXTRA_ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v1, "com.google.android.gms.common.audience.EXTRA_PAGE_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bq;->a:Lcom/google/android/gms/plus/audience/bl;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/bl;->j(Lcom/google/android/gms/plus/audience/bl;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bq;->a:Lcom/google/android/gms/plus/audience/bl;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/bl;->j(Lcom/google/android/gms/plus/audience/bl;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    sget-object v0, Lcom/google/android/gms/people/x;->f:Lcom/google/android/gms/people/j;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bq;->a:Lcom/google/android/gms/plus/audience/bl;

    invoke-static {v1}, Lcom/google/android/gms/plus/audience/bl;->k(Lcom/google/android/gms/plus/audience/bl;)Lcom/google/android/gms/common/api/v;

    move-result-object v1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-interface {p1}, Lcom/google/android/gms/people/k;->b()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/people/j;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/audience/bu;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/bq;->a:Lcom/google/android/gms/plus/audience/bl;

    invoke-direct {v1, v2, v7, v4}, Lcom/google/android/gms/plus/audience/bu;-><init>(Lcom/google/android/gms/plus/audience/bl;Lcom/google/android/gms/common/people/data/AudienceMember;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/gms/common/audience/a/h;

    const/4 v1, 0x3

    invoke-direct {v0, v5, v7, v1, v6}, Lcom/google/android/gms/common/audience/a/h;-><init>(ILcom/google/android/gms/common/people/data/AudienceMember;ILjava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/audience/a/h;->a()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bq;->a:Lcom/google/android/gms/plus/audience/bl;

    invoke-static {v1, v0, v6}, Lcom/google/android/gms/plus/audience/bl;->a(Lcom/google/android/gms/plus/audience/bl;Landroid/content/Intent;Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0
.end method
