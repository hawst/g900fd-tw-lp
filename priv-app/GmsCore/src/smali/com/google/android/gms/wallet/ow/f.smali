.class final Lcom/google/android/gms/wallet/ow/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/f;->a:Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/f;->a:Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/f;->a:Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1, p2}, Lcom/google/android/gms/wallet/shared/common/c/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/Throwable;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/f;->a:Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a(Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/f;->a:Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/f;->a:Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a(Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->finishActivity(I)V

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/f;->a:Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->b(Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;)Ljava/lang/Integer;

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/f;->a:Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/CrashActivity;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/content/Intent;

    move-result-object v0

    .line 113
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/f;->a:Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->startActivity(Landroid/content/Intent;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/f;->a:Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->finish()V

    .line 118
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 119
    return-void
.end method
