.class public final Lcom/google/android/gms/games/g/p;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Z

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 994
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 995
    iput v0, p0, Lcom/google/android/gms/games/g/p;->a:I

    iput-boolean v0, p0, Lcom/google/android/gms/games/g/p;->b:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/p;->c:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/p;->cachedSize:I

    .line 996
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1058
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1059
    iget v1, p0, Lcom/google/android/gms/games/g/p;->a:I

    if-eqz v1, :cond_0

    .line 1060
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/g/p;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1063
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/games/g/p;->b:Z

    if-eqz v1, :cond_1

    .line 1064
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/games/g/p;->b:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1067
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/g/p;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1068
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/games/g/p;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1071
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1008
    if-ne p1, p0, :cond_1

    .line 1028
    :cond_0
    :goto_0
    return v0

    .line 1011
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/p;

    if-nez v2, :cond_2

    move v0, v1

    .line 1012
    goto :goto_0

    .line 1014
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/p;

    .line 1015
    iget v2, p0, Lcom/google/android/gms/games/g/p;->a:I

    iget v3, p1, Lcom/google/android/gms/games/g/p;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1016
    goto :goto_0

    .line 1018
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/gms/games/g/p;->b:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/g/p;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1019
    goto :goto_0

    .line 1021
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/g/p;->c:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 1022
    iget-object v2, p1, Lcom/google/android/gms/games/g/p;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1023
    goto :goto_0

    .line 1025
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/g/p;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/p;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1026
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1033
    iget v0, p0, Lcom/google/android/gms/games/g/p;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 1035
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/g/p;->b:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    .line 1036
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/p;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    add-int/2addr v0, v1

    .line 1038
    return v0

    .line 1035
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0

    .line 1036
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/p;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 962
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/g/p;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/g/p;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/p;->c:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1044
    iget v0, p0, Lcom/google/android/gms/games/g/p;->a:I

    if-eqz v0, :cond_0

    .line 1045
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/g/p;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1047
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/games/g/p;->b:Z

    if-eqz v0, :cond_1

    .line 1048
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/gms/games/g/p;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1050
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/p;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1051
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/games/g/p;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1053
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1054
    return-void
.end method
