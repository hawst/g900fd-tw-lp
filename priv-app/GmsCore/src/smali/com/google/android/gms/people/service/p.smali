.class public final Lcom/google/android/gms/people/service/p;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 97
    return-void
.end method


# virtual methods
.method public final f(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x3

    const/4 v6, 0x0

    .line 105
    if-gtz p2, :cond_0

    .line 106
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "clientVersion too old"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid callingPackage"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/service/p;->b:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/people/service/p;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 122
    if-nez p4, :cond_3

    move-object v0, v1

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    move-object v4, v0

    .line 123
    :goto_1
    const-string v0, "real_client_package_name"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 124
    const-string v0, "support_new_image_callback"

    invoke-virtual {p4, v0, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    .line 127
    invoke-static {v8}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 128
    const-string v0, "PeopleService"

    const-string v1, "OrigAppId=%s  ResolvedAppID=%s  Caller=%s  RealCaller=%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const-string v5, "social_client_application_id"

    invoke-virtual {p4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v6

    const/4 v5, 0x1

    aput-object v4, v2, v5

    const/4 v5, 0x2

    aput-object p3, v2, v5

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    :cond_2
    new-instance v0, Lcom/google/android/gms/people/service/q;

    iget-object v1, p0, Lcom/google/android/gms/people/service/p;->b:Landroid/content/Context;

    sget-object v5, Lcom/google/android/gms/people/service/PeopleRequestProcessor;->b:Lcom/google/android/gms/people/service/c;

    sget-object v6, Lcom/google/android/gms/people/service/PeopleImageRequestProcessor;->a:Lcom/google/android/gms/people/service/c;

    move-object v2, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/people/service/q;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/service/c;Lcom/google/android/gms/people/service/c;Z)V

    invoke-virtual {v0}, Lcom/google/android/gms/people/service/q;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 140
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 141
    const-string v2, "post_init_configuration"

    iget-object v3, p0, Lcom/google/android/gms/people/service/p;->b:Landroid/content/Context;

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    sget-object v5, Lcom/google/android/gms/people/f/h;->a:[Ljava/lang/String;

    sget-object v6, Lcom/google/android/gms/people/f/h;->b:[Ljava/lang/String;

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/people/internal/o;->a(Landroid/os/Bundle;[Ljava/lang/String;[Ljava/lang/String;)V

    const-string v5, "use_contactables_api"

    sget-object v6, Lcom/google/android/gms/people/a/a;->j:Lcom/google/android/gms/common/a/d;

    invoke-static {v6}, Lcom/google/android/gms/common/c/a;->a(Lcom/google/android/gms/common/a/d;)Z

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v5, "config.email_type_map"

    invoke-static {v3}, Lcom/google/android/gms/people/service/f;->a(Landroid/content/Context;)V

    sget-object v6, Lcom/google/android/gms/people/service/f;->a:Landroid/os/Bundle;

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v5, "config.phone_type_map"

    invoke-static {v3}, Lcom/google/android/gms/people/service/f;->a(Landroid/content/Context;)V

    sget-object v3, Lcom/google/android/gms/people/service/f;->b:Landroid/os/Bundle;

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 143
    const-string v2, "post_init_resolution"

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 145
    const/4 v2, 0x0

    :try_start_0
    invoke-interface {p1, v2, v0, v1}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :goto_2
    return-void

    .line 122
    :cond_3
    const-string v0, "social_client_application_id"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    invoke-static {p3}, Lcom/google/android/gms/people/f/n;->a(Ljava/lang/String;)I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_5

    :goto_3
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    move-object v4, v1

    goto/16 :goto_1

    :cond_5
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_6
    sget-object v0, Lcom/google/android/gms/people/a/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "PeopleService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Please use new constructor and specify app ID.  Talk to the team: package="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/gms/common/analytics/a;->a:Ljava/lang/String;

    goto/16 :goto_1

    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Please use new constructor and specify app ID.  Talk to the team"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149
    :catch_0
    move-exception v0

    goto :goto_2
.end method
