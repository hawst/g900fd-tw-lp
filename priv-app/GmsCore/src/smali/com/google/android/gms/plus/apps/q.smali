.class final Lcom/google/android/gms/plus/apps/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/apps/k;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/apps/k;)V
    .locals 0

    .prologue
    .line 486
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/q;->a:Lcom/google/android/gms/plus/apps/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/apps/k;B)V
    .locals 0

    .prologue
    .line 486
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/apps/q;-><init>(Lcom/google/android/gms/plus/apps/k;)V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    .line 490
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/q;->a:Lcom/google/android/gms/plus/apps/k;

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/k;->b(Lcom/google/android/gms/plus/apps/k;)Lcom/google/android/gms/plus/apps/u;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/gms/plus/apps/u;->b(I)Lcom/google/android/gms/plus/apps/v;

    move-result-object v0

    .line 492
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/q;->a:Lcom/google/android/gms/plus/apps/k;

    iget-object v1, v1, Lcom/google/android/gms/plus/apps/k;->j:Lcom/google/android/gms/plus/apps/c;

    sget-object v2, Lcom/google/android/gms/common/analytics/c;->z:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/q;->a:Lcom/google/android/gms/plus/apps/k;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/apps/k;->d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/plus/apps/c;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 493
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/q;->a:Lcom/google/android/gms/plus/apps/k;

    iget v2, v0, Lcom/google/android/gms/plus/apps/v;->b:I

    invoke-static {v1, v2}, Lcom/google/android/gms/plus/apps/k;->b(Lcom/google/android/gms/plus/apps/k;I)V

    .line 494
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/q;->a:Lcom/google/android/gms/plus/apps/k;

    invoke-static {v1}, Lcom/google/android/gms/plus/apps/k;->b(Lcom/google/android/gms/plus/apps/k;)Lcom/google/android/gms/plus/apps/u;

    move-result-object v1

    iget v0, v0, Lcom/google/android/gms/plus/apps/v;->b:I

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/u;->a(I)V

    .line 496
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/q;->a:Lcom/google/android/gms/plus/apps/k;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/apps/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->gg:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 497
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/q;->a:Lcom/google/android/gms/plus/apps/k;

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/q;->a:Lcom/google/android/gms/plus/apps/k;

    invoke-static {v2}, Lcom/google/android/gms/plus/apps/k;->c(Lcom/google/android/gms/plus/apps/k;)[Lcom/google/android/gms/plus/apps/p;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/q;->a:Lcom/google/android/gms/plus/apps/k;

    invoke-static {v3}, Lcom/google/android/gms/plus/apps/k;->d(Lcom/google/android/gms/plus/apps/k;)I

    move-result v3

    aget-object v2, v2, v3

    iget v2, v2, Lcom/google/android/gms/plus/apps/p;->b:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/apps/k;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    .line 499
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/q;->a:Lcom/google/android/gms/plus/apps/k;

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/k;->e(Lcom/google/android/gms/plus/apps/k;)Landroid/support/v7/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/a;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 500
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/q;->a:Lcom/google/android/gms/plus/apps/k;

    invoke-static {v1}, Lcom/google/android/gms/plus/apps/k;->f(Lcom/google/android/gms/plus/apps/k;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 501
    return-void
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 505
    return-void
.end method
