.class final Lcom/google/android/gms/ads/internal/a/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/g/m;


# instance fields
.field final synthetic a:Lcom/google/android/gms/ads/internal/a/d;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/a/d;)V
    .locals 0

    .prologue
    .line 372
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/a/m;->a:Lcom/google/android/gms/ads/internal/a/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/ads/internal/p/a;Ljava/util/Map;)V
    .locals 2

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/m;->a:Lcom/google/android/gms/ads/internal/a/d;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/ads/internal/a/d;->a(Ljava/util/Map;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 384
    :cond_0
    :goto_0
    return-void

    .line 378
    :cond_1
    const-string v0, "isVisible"

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    const-string v0, "1"

    const-string v1, "isVisible"

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "true"

    const-string v1, "isVisible"

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 383
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/m;->a:Lcom/google/android/gms/ads/internal/a/d;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/ads/internal/a/d;->a(Z)V

    goto :goto_0

    .line 381
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
