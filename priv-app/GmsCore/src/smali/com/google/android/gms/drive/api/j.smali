.class final Lcom/google/android/gms/drive/api/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field final a:I

.field final b:Landroid/os/ParcelFileDescriptor;

.field final c:Ljava/lang/String;

.field final d:Ljava/lang/String;

.field final e:Lcom/google/android/gms/drive/b/l;

.field f:Lcom/google/android/gms/drive/database/model/EntrySpec;

.field final g:Lcom/google/android/gms/drive/auth/AppIdentity;

.field final h:Landroid/os/IBinder;

.field final i:J

.field final synthetic j:Lcom/google/android/gms/drive/api/i;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/api/i;Landroid/os/ParcelFileDescriptor;Lcom/google/android/gms/drive/b/l;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/gms/drive/api/j;->j:Lcom/google/android/gms/drive/api/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object p3, p0, Lcom/google/android/gms/drive/api/j;->e:Lcom/google/android/gms/drive/b/l;

    .line 99
    iput-object p4, p0, Lcom/google/android/gms/drive/api/j;->f:Lcom/google/android/gms/drive/database/model/EntrySpec;

    .line 100
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelFileDescriptor;

    iput-object v0, p0, Lcom/google/android/gms/drive/api/j;->b:Landroid/os/ParcelFileDescriptor;

    .line 101
    iget-object v0, p1, Lcom/google/android/gms/drive/api/i;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/api/j;->a:I

    .line 102
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/AppIdentity;

    iput-object v0, p0, Lcom/google/android/gms/drive/api/j;->g:Lcom/google/android/gms/drive/auth/AppIdentity;

    .line 103
    invoke-static {p6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;

    iput-object v0, p0, Lcom/google/android/gms/drive/api/j;->h:Landroid/os/IBinder;

    .line 104
    iget-object v0, p1, Lcom/google/android/gms/drive/api/i;->b:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/api/j;->i:J

    .line 105
    iput-object p7, p0, Lcom/google/android/gms/drive/api/j;->c:Ljava/lang/String;

    .line 106
    iput-object p8, p0, Lcom/google/android/gms/drive/api/j;->d:Ljava/lang/String;

    .line 107
    return-void
.end method


# virtual methods
.method final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 111
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/drive/api/j;->h:Landroid/os/IBinder;

    const/4 v2, 0x0

    invoke-interface {v1, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    const/4 v0, 0x1

    .line 114
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method final b()V
    .locals 3

    .prologue
    .line 120
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/j;->b:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/j;->e:Lcom/google/android/gms/drive/b/l;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/drive/api/j;->e:Lcom/google/android/gms/drive/b/l;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/b/l;->c()V

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/j;->h:Landroid/os/IBinder;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 128
    return-void

    .line 122
    :catch_0
    move-exception v0

    const-string v0, "HashBasedOpenContentsStore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to close file: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/drive/api/j;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final binderDied()V
    .locals 5

    .prologue
    .line 133
    const-string v0, "HashBasedOpenContentsStore"

    const-string v1, "Client died with open content: %d."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/gms/drive/api/j;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/drive/api/j;->j:Lcom/google/android/gms/drive/api/i;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/drive/api/i;->a(Lcom/google/android/gms/drive/api/j;)V

    .line 135
    invoke-virtual {p0}, Lcom/google/android/gms/drive/api/j;->b()V

    .line 136
    return-void
.end method
