.class final Lcom/google/android/gms/audiomodem/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/audiomodem/t;

.field private b:Landroid/media/AudioTrack;

.field private c:I

.field private d:[B

.field private e:[B

.field private f:Ljava/lang/Runnable;

.field private g:Landroid/os/Handler;

.field private h:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/audiomodem/t;Landroid/media/AudioTrack;I[B[BLjava/lang/Runnable;Landroid/os/Handler;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 390
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/w;->a:Lcom/google/android/gms/audiomodem/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391
    iput-object p2, p0, Lcom/google/android/gms/audiomodem/w;->b:Landroid/media/AudioTrack;

    .line 392
    iput p3, p0, Lcom/google/android/gms/audiomodem/w;->c:I

    .line 393
    iput-object p4, p0, Lcom/google/android/gms/audiomodem/w;->d:[B

    .line 394
    iput-object p5, p0, Lcom/google/android/gms/audiomodem/w;->e:[B

    .line 395
    iput-object p6, p0, Lcom/google/android/gms/audiomodem/w;->f:Ljava/lang/Runnable;

    .line 396
    iput-object p7, p0, Lcom/google/android/gms/audiomodem/w;->g:Landroid/os/Handler;

    .line 401
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/w;->b:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getSampleRate()I

    move-result v0

    const/4 v1, 0x4

    invoke-static {v0, v1, v2}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/gms/audiomodem/w;->h:I

    .line 402
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/w;->b:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getAudioFormat()I

    move-result v0

    if-ne v0, v2, :cond_0

    iget v0, p0, Lcom/google/android/gms/audiomodem/w;->h:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 405
    iget v0, p0, Lcom/google/android/gms/audiomodem/w;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/audiomodem/w;->h:I

    .line 407
    :cond_0
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const/4 v1, 0x3

    const/4 v2, 0x0

    .line 411
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/w;->b:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 412
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/w;->b:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->flush()V

    .line 413
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/w;->b:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    .line 419
    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/audiomodem/w;->d:[B

    .line 428
    iget v0, p0, Lcom/google/android/gms/audiomodem/w;->h:I

    move v5, v0

    move v1, v2

    move v0, v2

    .line 433
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/audiomodem/w;->a:Lcom/google/android/gms/audiomodem/t;

    iget-object v4, v4, Lcom/google/android/gms/audiomodem/t;->g:[Z

    iget v6, p0, Lcom/google/android/gms/audiomodem/w;->c:I

    aget-boolean v4, v4, v6

    if-nez v4, :cond_1

    .line 434
    iget-object v4, p0, Lcom/google/android/gms/audiomodem/w;->a:Lcom/google/android/gms/audiomodem/t;

    iget-wide v6, v4, Lcom/google/android/gms/audiomodem/t;->h:J

    const-wide/16 v8, -0x1

    cmp-long v4, v6, v8

    if-eqz v4, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v4, p0, Lcom/google/android/gms/audiomodem/w;->a:Lcom/google/android/gms/audiomodem/t;

    iget-wide v8, v4, Lcom/google/android/gms/audiomodem/t;->h:J

    cmp-long v4, v6, v8

    if-lez v4, :cond_2

    .line 436
    const-string v0, "audioModem"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 437
    const-string v0, "AudioPlayer: Stopping audio playback due to timeout"

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/w;->b:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->pause()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 468
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/w;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/audiomodem/w;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 469
    return-void

    .line 444
    :cond_2
    :try_start_1
    array-length v4, v3

    sub-int/2addr v4, v0

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 446
    iget-object v4, p0, Lcom/google/android/gms/audiomodem/w;->b:Landroid/media/AudioTrack;

    invoke-virtual {v4, v3, v0, v6}, Landroid/media/AudioTrack;->write([BII)I

    .line 447
    add-int/2addr v0, v6

    .line 448
    array-length v4, v3

    if-lt v0, v4, :cond_5

    .line 450
    if-nez v1, :cond_4

    .line 451
    const/4 v0, 0x1

    .line 452
    iget-object v1, p0, Lcom/google/android/gms/audiomodem/w;->e:[B

    move v3, v0

    move-object v4, v1

    move v1, v2

    .line 455
    :goto_1
    sub-int v0, v5, v6

    .line 459
    if-nez v0, :cond_3

    .line 460
    iget v0, p0, Lcom/google/android/gms/audiomodem/w;->h:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    move v5, v0

    move v0, v1

    move v1, v3

    move-object v3, v4

    .line 462
    goto :goto_0

    .line 468
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/audiomodem/w;->g:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/w;->f:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    throw v0

    :cond_4
    move-object v4, v3

    move v3, v1

    move v1, v2

    goto :goto_1

    :cond_5
    move-object v4, v3

    move v3, v1

    move v1, v0

    goto :goto_1
.end method
