.class public final Lcom/google/android/gms/auth/o;
.super Lcom/google/android/b/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/GetToken;

.field private volatile b:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/auth/GetToken;)V
    .locals 2

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/android/gms/auth/o;->a:Lcom/google/android/gms/auth/GetToken;

    invoke-direct {p0}, Lcom/google/android/b/b;-><init>()V

    .line 64
    const-string v0, "GLSActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/o;->b:Z

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 161
    new-instance v0, Lcom/google/android/gms/auth/a/c;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/a/c;-><init>(Landroid/content/Context;)V

    .line 163
    new-instance v1, Lcom/google/android/gms/auth/firstparty/dataservice/w;

    iget-object v0, v0, Lcom/google/android/gms/auth/a/c;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/w;-><init>(Landroid/content/Context;)V

    .line 165
    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenRequest;

    invoke-direct {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenRequest;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenRequest;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenRequest;

    move-result-object v0

    .line 167
    const-string v2, "ClearTokenRequest cannot be null!"

    invoke-static {v2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/google/android/gms/auth/firstparty/dataservice/y;

    invoke-direct {v2, v1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/y;-><init>(Lcom/google/android/gms/auth/firstparty/dataservice/w;Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenRequest;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a(Lcom/google/android/gms/auth/firstparty/dataservice/ap;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenResponse;

    .line 168
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 169
    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenResponse;->a()Lcom/google/android/gms/auth/firstparty/shared/k;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/auth/firstparty/shared/k;->a:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/firstparty/shared/k;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 170
    sget-object v2, Lcom/google/android/gms/auth/firstparty/shared/k;->T:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenResponse;->a()Lcom/google/android/gms/auth/firstparty/shared/k;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/auth/firstparty/shared/k;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    :cond_0
    const-string v2, "booleanResult"

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenResponse;->a()Lcom/google/android/gms/auth/firstparty/shared/k;

    move-result-object v0

    sget-object v3, Lcom/google/android/gms/auth/firstparty/shared/k;->a:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/firstparty/shared/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 175
    return-object v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 12

    .prologue
    .line 94
    :try_start_0
    new-instance v6, Lcom/google/android/gms/auth/e/d;

    iget-object v0, p0, Lcom/google/android/gms/auth/o;->a:Lcom/google/android/gms/auth/GetToken;

    invoke-direct {v6, v0}, Lcom/google/android/gms/auth/e/d;-><init>(Landroid/content/Context;)V

    .line 95
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 101
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    .line 102
    new-instance v7, Lcom/google/android/gms/auth/a/c;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-direct {v7, v0}, Lcom/google/android/gms/auth/a/c;-><init>(Landroid/content/Context;)V

    .line 103
    if-nez p3, :cond_6

    .line 104
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    :goto_0
    :try_start_1
    iget-object v0, v7, Lcom/google/android/gms/auth/a/c;->b:Landroid/content/pm/PackageManager;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;I)Z
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    .line 117
    :goto_1
    if-nez v0, :cond_1

    .line 118
    :try_start_2
    invoke-virtual {v5}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 119
    if-eqz v0, :cond_0

    const-string v2, "_opt_"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 120
    invoke-virtual {v5, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 153
    :catch_0
    move-exception v0

    .line 154
    const-string v1, "GLSActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/gms/auth/GetToken;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - getToken exception!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 155
    throw v0

    .line 115
    :catch_1
    move-exception v0

    const/4 v0, 0x0

    goto :goto_1

    .line 125
    :cond_1
    :try_start_3
    new-instance v0, Lcom/google/android/gms/auth/ad;

    invoke-direct {v0, v7}, Lcom/google/android/gms/auth/ad;-><init>(Lcom/google/android/gms/auth/a/c;)V

    .line 126
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    sget-object v1, Lcom/google/android/gms/auth/r;->a:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v5, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    sget-object v3, Lcom/google/android/gms/auth/r;->b:Ljava/lang/String;

    invoke-virtual {v5, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_2

    invoke-virtual {v7, v3}, Lcom/google/android/gms/auth/a/c;->a(Ljava/lang/String;)I

    move-result v1

    :cond_2
    invoke-static {v2, v3, v1, v7}, Lcom/google/android/gms/auth/a/a;->a(ILjava/lang/String;ILcom/google/android/gms/auth/a/c;)Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v1

    .line 127
    const-string v2, "accountManagerResponse"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/accounts/AccountAuthenticatorResponse;

    move-object v3, p1

    move-object v4, p2

    .line 130
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/auth/ad;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 136
    if-nez v1, :cond_3

    .line 137
    const-string v0, "token result is null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    iget-boolean v3, p0, Lcom/google/android/gms/auth/o;->b:Z

    if-eqz v3, :cond_3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "GLSActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/gms/auth/GetToken;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_3
    sget-object v0, Lcom/google/android/gms/auth/r;->b:Ljava/lang/String;

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 141
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 142
    sub-long v8, v10, v8

    iput-wide v8, v6, Lcom/google/android/gms/auth/e/d;->d:J

    .line 143
    const-string v0, "handle_notification"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v6, Lcom/google/android/gms/auth/e/d;->e:Z

    .line 145
    const-string v0, "callback_intent"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_3
    iput-boolean v0, v6, Lcom/google/android/gms/auth/e/d;->f:Z

    .line 146
    const-string v0, "sync_extras"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_4
    iput-boolean v0, v6, Lcom/google/android/gms/auth/e/d;->g:Z

    .line 147
    const-string v0, "TokenCache"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v6, Lcom/google/android/gms/auth/e/d;->h:Z

    .line 148
    invoke-virtual {v6, v2}, Lcom/google/android/gms/auth/e/d;->a(Ljava/lang/String;)V

    .line 149
    invoke-virtual {v6, p2}, Lcom/google/android/gms/auth/e/d;->b(Ljava/lang/String;)V

    .line 150
    invoke-virtual {v7, v2}, Lcom/google/android/gms/auth/a/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/android/gms/auth/e/d;->c(Ljava/lang/String;)V

    .line 151
    invoke-virtual {v6}, Lcom/google/android/gms/auth/e/d;->a()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0

    .line 152
    return-object v1

    .line 145
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 146
    :cond_5
    const/4 v0, 0x0

    goto :goto_4

    :cond_6
    move-object v5, p3

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/gms/auth/AccountChangeEventsRequest;)Lcom/google/android/gms/auth/AccountChangeEventsResponse;
    .locals 2

    .prologue
    .line 181
    new-instance v0, Lcom/google/android/gms/auth/a/c;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/a/c;-><init>(Landroid/content/Context;)V

    .line 183
    new-instance v1, Lcom/google/android/gms/auth/firstparty/dataservice/w;

    iget-object v0, v0, Lcom/google/android/gms/auth/a/c;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/w;-><init>(Landroid/content/Context;)V

    .line 185
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/ai;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/auth/firstparty/dataservice/ai;-><init>(Lcom/google/android/gms/auth/firstparty/dataservice/w;Lcom/google/android/gms/auth/AccountChangeEventsRequest;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a(Lcom/google/android/gms/auth/firstparty/dataservice/ap;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/AccountChangeEventsResponse;

    return-object v0
.end method
