.class public final Lcom/google/android/gms/wallet/cache/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string v0, "com.google.android.gms.wallet.cache.MerchantCache"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/cache/f;->a:Landroid/content/SharedPreferences;

    .line 30
    return-void
.end method

.method public static b(ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    invoke-static {}, Lcom/google/android/gms/wallet/common/af;->a()Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ah;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ah;->a(I)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(ILjava/lang/String;)Lcom/google/android/gms/wallet/cache/g;
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 33
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    new-instance v0, Lcom/google/android/gms/wallet/cache/g;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/cache/g;-><init>()V

    .line 55
    :goto_0
    return-object v0

    .line 36
    :cond_0
    invoke-static {p1, p2}, Lcom/google/android/gms/wallet/cache/f;->b(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 37
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/f;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 41
    if-nez v0, :cond_1

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/f;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0, p2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 43
    if-eqz v0, :cond_1

    .line 44
    iget-object v3, p0, Lcom/google/android/gms/wallet/cache/f;->a:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3, p2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 47
    :cond_1
    new-instance v3, Lcom/google/android/gms/wallet/cache/g;

    invoke-direct {v3}, Lcom/google/android/gms/wallet/cache/g;-><init>()V

    if-eqz v0, :cond_3

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/af;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ag;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ag;->a()I

    move-result v0

    iput v0, v3, Lcom/google/android/gms/wallet/cache/g;->a:I

    iget v0, v3, Lcom/google/android/gms/wallet/cache/g;->a:I

    if-le v0, v6, :cond_5

    invoke-virtual {v5, v2}, Lcom/google/android/gms/wallet/common/ag;->a(Z)Z

    move-result v0

    iput-boolean v0, v3, Lcom/google/android/gms/wallet/cache/g;->b:Z

    :goto_1
    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ag;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/wallet/cache/g;->c:Ljava/lang/String;

    iget v0, v3, Lcom/google/android/gms/wallet/cache/g;->a:I

    const/4 v2, 0x3

    if-le v0, v2, :cond_2

    invoke-virtual {v5, v1}, Lcom/google/android/gms/wallet/common/ag;->a(Z)Z

    move-result v0

    iput-boolean v0, v3, Lcom/google/android/gms/wallet/cache/g;->d:Z

    :cond_2
    iget v0, v3, Lcom/google/android/gms/wallet/cache/g;->a:I

    if-le v0, v7, :cond_3

    invoke-virtual {v5, v1}, Lcom/google/android/gms/wallet/common/ag;->a(Z)Z

    move-result v0

    iput-boolean v0, v3, Lcom/google/android/gms/wallet/cache/g;->e:Z

    .line 51
    :cond_3
    iget v0, v3, Lcom/google/android/gms/wallet/cache/g;->a:I

    if-ge v0, v7, :cond_4

    .line 52
    const/4 v0, 0x5

    iput v0, v3, Lcom/google/android/gms/wallet/cache/g;->a:I

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/f;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/cache/g;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_4
    move-object v0, v3

    .line 55
    goto :goto_0

    .line 47
    :cond_5
    iget v0, v3, Lcom/google/android/gms/wallet/cache/g;->a:I

    if-ne v0, v6, :cond_7

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ag;->a()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_2
    iput-boolean v0, v3, Lcom/google/android/gms/wallet/cache/g;->b:Z

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    iget v0, v3, Lcom/google/android/gms/wallet/cache/g;->a:I

    if-ne v0, v1, :cond_8

    move v2, v1

    :cond_8
    iput-boolean v2, v3, Lcom/google/android/gms/wallet/cache/g;->b:Z

    iput v1, v3, Lcom/google/android/gms/wallet/cache/g;->a:I

    goto :goto_1
.end method
