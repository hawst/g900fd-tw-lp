.class public final Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private final d:Ljava/util/Set;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;->d:Ljava/util/Set;

    .line 61
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;
    .locals 5

    .prologue
    .line 102
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAclentryResourceEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;->d:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAclentryResourceEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;
    .locals 2

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;->b:Ljava/lang/String;

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;->d:Ljava/util/Set;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 79
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;
    .locals 2

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;->c:Ljava/lang/String;

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;->d:Ljava/util/Set;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 95
    return-object p0
.end method
