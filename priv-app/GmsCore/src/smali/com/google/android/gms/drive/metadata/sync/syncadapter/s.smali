.class final Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/i;

.field final synthetic b:Landroid/content/ServiceConnection;

.field final synthetic c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;Lcom/google/android/gms/drive/metadata/sync/syncadapter/i;Landroid/content/ServiceConnection;)V
    .locals 0

    .prologue
    .line 373
    iput-object p1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;

    iput-object p2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/i;

    iput-object p3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->b:Landroid/content/ServiceConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 376
    new-instance v0, Landroid/content/SyncResult;

    invoke-direct {v0}, Landroid/content/SyncResult;-><init>()V

    .line 377
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;

    iget-object v1, v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;

    iget-object v2, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->a:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/drive/ui/picker/view/h;->a:Lcom/google/android/gms/drive/ui/picker/view/h;

    sget-object v4, Lcom/google/android/gms/drive/ui/picker/view/h;->b:Lcom/google/android/gms/drive/ui/picker/view/h;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Ljava/lang/String;Lcom/google/android/gms/drive/ui/picker/view/h;Lcom/google/android/gms/drive/ui/picker/view/h;)Z

    move-result v1

    .line 380
    if-nez v1, :cond_0

    .line 381
    const-string v0, "SyncScheduler"

    const-string v1, "%s sync not pending."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "Ignoring sync request: "

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 400
    :goto_0
    return-void

    .line 385
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;

    iget-object v1, v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;

    iget-object v2, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;Ljava/lang/String;)V

    .line 388
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/i;->a()Lcom/google/android/gms/drive/metadata/sync/syncadapter/MetadataSyncService;

    move-result-object v1

    .line 389
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;

    iget-object v2, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;

    iget v3, v3, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->b:I

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/MetadataSyncService;->a(Ljava/lang/String;ILandroid/content/SyncResult;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;

    iget-object v2, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    invoke-static {v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->b:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    .line 394
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;

    iget-object v1, v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;

    iget-object v2, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->a:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/drive/ui/picker/view/h;->b:Lcom/google/android/gms/drive/ui/picker/view/h;

    sget-object v4, Lcom/google/android/gms/drive/ui/picker/view/h;->c:Lcom/google/android/gms/drive/ui/picker/view/h;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Ljava/lang/String;Lcom/google/android/gms/drive/ui/picker/view/h;Lcom/google/android/gms/drive/ui/picker/view/h;)Z

    .line 396
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;

    iget-object v1, v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;

    iget-object v2, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;Ljava/lang/String;Landroid/content/SyncResult;)V

    .line 399
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->b(Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;)Lcom/google/android/gms/drive/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/e/b;->b()Ljava/util/concurrent/Future;

    goto :goto_0

    .line 393
    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;

    iget-object v2, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    invoke-static {v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->b:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    .line 394
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;

    iget-object v1, v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;

    iget-object v2, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->a:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/drive/ui/picker/view/h;->b:Lcom/google/android/gms/drive/ui/picker/view/h;

    sget-object v4, Lcom/google/android/gms/drive/ui/picker/view/h;->c:Lcom/google/android/gms/drive/ui/picker/view/h;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Ljava/lang/String;Lcom/google/android/gms/drive/ui/picker/view/h;Lcom/google/android/gms/drive/ui/picker/view/h;)Z

    .line 396
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;

    iget-object v1, v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;

    iget-object v2, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->a:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;Ljava/lang/String;Landroid/content/SyncResult;)V

    throw v0
.end method
