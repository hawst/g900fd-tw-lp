.class final Lcom/google/android/gms/drive/realtime/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/a/c/s;


# instance fields
.field private final a:Lcom/google/android/gms/drive/realtime/j;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/realtime/j;)V
    .locals 0

    .prologue
    .line 325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 326
    iput-object p1, p0, Lcom/google/android/gms/drive/realtime/k;->a:Lcom/google/android/gms/drive/realtime/j;

    .line 327
    return-void
.end method

.method private a(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    .prologue
    .line 330
    const-string v0, "RealtimeLoader"

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/k;->a:Lcom/google/android/gms/drive/realtime/j;

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/realtime/j;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 332
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 336
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x5e0

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/realtime/k;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 338
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 348
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x5de

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/realtime/k;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 350
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 354
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x5df

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/realtime/k;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 356
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 360
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/4 v1, 0x7

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/realtime/k;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 361
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 370
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0xbbc

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/realtime/k;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 371
    return-void
.end method
