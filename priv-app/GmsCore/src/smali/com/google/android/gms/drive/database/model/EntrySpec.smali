.class public final Lcom/google/android/gms/drive/database/model/EntrySpec;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/gms/drive/database/model/aq;

    invoke-direct {v0}, Lcom/google/android/gms/drive/database/model/aq;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/database/model/EntrySpec;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(J)V
    .locals 3

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-wide p1, p0, Lcom/google/android/gms/drive/database/model/EntrySpec;->a:J

    .line 41
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 42
    return-void

    .line 41
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(JB)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/database/model/EntrySpec;-><init>(J)V

    return-void
.end method

.method public static a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;
    .locals 2

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/drive/database/model/EntrySpec;-><init>(J)V

    return-object v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/google/android/gms/drive/database/model/EntrySpec;->a:J

    return-wide v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 62
    if-ne p0, p1, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v0

    .line 65
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 66
    goto :goto_0

    .line 68
    :cond_2
    instance-of v2, p1, Lcom/google/android/gms/drive/database/model/EntrySpec;

    if-nez v2, :cond_3

    move v0, v1

    .line 69
    goto :goto_0

    .line 71
    :cond_3
    check-cast p1, Lcom/google/android/gms/drive/database/model/EntrySpec;

    .line 72
    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/EntrySpec;->a:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/database/model/EntrySpec;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/google/android/gms/drive/database/model/EntrySpec;->a:J

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/EntrySpec;->a:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1f

    .line 57
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 77
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "EntrySpec[%s]"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/google/android/gms/drive/database/model/EntrySpec;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/google/android/gms/drive/database/model/EntrySpec;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 88
    return-void
.end method
