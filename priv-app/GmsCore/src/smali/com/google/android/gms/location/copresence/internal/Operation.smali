.class public Lcom/google/android/gms/location/copresence/internal/Operation;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public final b:Lcom/google/android/gms/location/copresence/internal/PublishOperation;

.field public final c:Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;

.field public final d:Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;

.field public final e:Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/t;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/internal/t;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/internal/Operation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IILcom/google/android/gms/location/copresence/internal/PublishOperation;Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput p1, p0, Lcom/google/android/gms/location/copresence/internal/Operation;->f:I

    .line 97
    iput p2, p0, Lcom/google/android/gms/location/copresence/internal/Operation;->a:I

    .line 98
    iput-object p3, p0, Lcom/google/android/gms/location/copresence/internal/Operation;->b:Lcom/google/android/gms/location/copresence/internal/PublishOperation;

    .line 99
    iput-object p4, p0, Lcom/google/android/gms/location/copresence/internal/Operation;->c:Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;

    .line 100
    iput-object p5, p0, Lcom/google/android/gms/location/copresence/internal/Operation;->d:Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;

    .line 101
    iput-object p6, p0, Lcom/google/android/gms/location/copresence/internal/Operation;->e:Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

    .line 102
    return-void
.end method

.method private constructor <init>(ILcom/google/android/gms/location/copresence/internal/PublishOperation;Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)V
    .locals 7

    .prologue
    .line 106
    const/4 v1, 0x1

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/location/copresence/internal/Operation;-><init>(IILcom/google/android/gms/location/copresence/internal/PublishOperation;Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)V

    .line 107
    return-void
.end method

.method public static a(Lcom/google/android/gms/location/copresence/internal/PublishOperation;)Lcom/google/android/gms/location/copresence/internal/Operation;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 60
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/Operation;

    const/4 v1, 0x1

    move-object v2, p0

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/copresence/internal/Operation;-><init>(ILcom/google/android/gms/location/copresence/internal/PublishOperation;Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;)Lcom/google/android/gms/location/copresence/internal/Operation;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 76
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/Operation;

    const/4 v1, 0x3

    move-object v3, v2

    move-object v4, p0

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/copresence/internal/Operation;-><init>(ILcom/google/android/gms/location/copresence/internal/PublishOperation;Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;)Lcom/google/android/gms/location/copresence/internal/Operation;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 68
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/Operation;

    const/4 v1, 0x2

    move-object v3, p0

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/copresence/internal/Operation;-><init>(ILcom/google/android/gms/location/copresence/internal/PublishOperation;Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)Lcom/google/android/gms/location/copresence/internal/Operation;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 84
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/Operation;

    const/4 v1, 0x4

    move-object v3, v2

    move-object v4, v2

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/copresence/internal/Operation;-><init>(ILcom/google/android/gms/location/copresence/internal/PublishOperation;Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)V

    return-object v0
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lcom/google/android/gms/location/copresence/internal/Operation;->f:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 120
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/location/copresence/internal/t;->a(Lcom/google/android/gms/location/copresence/internal/Operation;Landroid/os/Parcel;I)V

    .line 121
    return-void
.end method
