.class public final Lcom/google/android/gms/ads/internal/o/h;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;

.field public b:I

.field public c:I

.field public final d:Lcom/google/android/gms/ads/internal/o/e;

.field public final e:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/ads/internal/o/e;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/h;->a:Ljava/lang/Object;

    .line 40
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/o/h;->d:Lcom/google/android/gms/ads/internal/o/e;

    .line 41
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/o/h;->e:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/google/android/gms/ads/internal/o/e;->a()Lcom/google/android/gms/ads/internal/o/e;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/ads/internal/o/h;-><init>(Lcom/google/android/gms/ads/internal/o/e;Ljava/lang/String;)V

    .line 37
    return-void
.end method


# virtual methods
.method public final a()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 57
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/o/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 58
    :try_start_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 59
    const-string v2, "pmnli"

    iget v3, p0, Lcom/google/android/gms/ads/internal/o/h;->b:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 61
    const-string v2, "pmnll"

    iget v3, p0, Lcom/google/android/gms/ads/internal/o/h;->c:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 63
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
