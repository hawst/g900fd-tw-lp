.class public Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/aj;


# instance fields
.field final a:I

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Landroid/net/Uri;

.field public final e:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

.field public final f:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

.field public final g:Z

.field public final h:Landroid/accounts/Account;

.field public final i:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

.field public final j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/gms/appdatasearch/aj;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/aj;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/aj;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Landroid/net/Uri;[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;ZLandroid/accounts/Account;Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput p1, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->a:I

    .line 110
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->b:Ljava/lang/String;

    .line 111
    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->c:Ljava/lang/String;

    .line 112
    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->d:Landroid/net/Uri;

    .line 113
    iput-object p5, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->e:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    .line 114
    iput-object p6, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->f:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    .line 115
    iput-boolean p7, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->g:Z

    .line 116
    iput-object p8, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->h:Landroid/accounts/Account;

    .line 117
    iput-object p9, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->i:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    .line 118
    iput-object p10, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->j:Ljava/lang/String;

    .line 119
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;ZLandroid/accounts/Account;Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 130
    const/4 v1, 0x2

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/net/Uri;[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;ZLandroid/accounts/Account;Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;Ljava/lang/String;)V

    .line 132
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/ai;
    .locals 1

    .prologue
    .line 166
    new-instance v0, Lcom/google/android/gms/appdatasearch/ai;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/ai;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 137
    sget-object v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/aj;

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 148
    instance-of v1, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    if-eqz v1, :cond_0

    .line 149
    check-cast p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    .line 150
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->d:Landroid/net/Uri;

    iget-object v2, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->d:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->e:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    iget-object v2, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->e:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 154
    :cond_0
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/aj;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/aj;->a(Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;Landroid/os/Parcel;I)V

    .line 144
    return-void
.end method
