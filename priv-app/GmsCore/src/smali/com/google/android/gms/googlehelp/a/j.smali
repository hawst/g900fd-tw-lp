.class public abstract Lcom/google/android/gms/googlehelp/a/j;
.super Lcom/android/volley/p;
.source "SourceFile"


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field protected final g:Landroid/content/Context;

.field protected final h:Ljava/util/HashMap;

.field private final i:Landroid/accounts/Account;

.field private final j:Lcom/android/volley/x;

.field private k:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-string v0, "http.agent"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/a/j;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;ILjava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 63
    invoke-direct {p0, p3, p4, p6}, Lcom/android/volley/p;-><init>(ILjava/lang/String;Lcom/android/volley/w;)V

    .line 64
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/a/j;->g:Landroid/content/Context;

    .line 65
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/a/j;->i:Landroid/accounts/Account;

    .line 66
    iput-object p5, p0, Lcom/google/android/gms/googlehelp/a/j;->j:Lcom/android/volley/x;

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/a/j;->h:Ljava/util/HashMap;

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/j;->h:Ljava/util/HashMap;

    const-string v1, "User-Agent"

    sget-object v2, Lcom/google/android/gms/googlehelp/a/j;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    invoke-direct {p0, v3}, Lcom/google/android/gms/googlehelp/a/j;->b(Z)V

    .line 72
    iput-boolean v3, p0, Lcom/android/volley/p;->c:Z

    .line 73
    new-instance v0, Lcom/google/android/gms/googlehelp/a/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/a/k;-><init>(Lcom/google/android/gms/googlehelp/a/j;)V

    iput-object v0, p0, Lcom/android/volley/p;->d:Lcom/android/volley/z;

    .line 75
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/a/j;)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/a/j;->b(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/j;->i:Landroid/accounts/Account;

    if-nez v0, :cond_0

    .line 104
    :goto_0
    return-void

    .line 95
    :cond_0
    if-eqz p1, :cond_1

    .line 96
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/j;->g:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/a/j;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/auth/r;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/j;->g:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/a/j;->i:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v2, "oauth2:https://www.googleapis.com/auth/supportcontent"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/a/j;->k:Ljava/lang/String;

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/j;->h:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/a/j;->k:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/server/i;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 101
    :catch_0
    move-exception v0

    .line 102
    const-string v1, "GOOGLEHELP_GoogleHelpBaseRequest"

    const-string v2, "Updating auth token failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic c(Lcom/android/volley/ac;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 28
    iget-object v1, p0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v1, v1, Lcom/android/volley/m;->a:I

    const/16 v2, 0x190

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v1, v1, Lcom/android/volley/m;->a:I

    const/16 v2, 0x1f4

    if-ge v1, v2, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/common/server/b/c;->b(Lcom/android/volley/ac;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v0, "userRateLimitExceeded"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic d(Lcom/android/volley/ac;)Z
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v0, v0, Lcom/android/volley/m;->a:I

    const/16 v1, 0x191

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/j;->j:Lcom/android/volley/x;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/j;->j:Lcom/android/volley/x;

    invoke-interface {v0, p1}, Lcom/android/volley/x;->a(Ljava/lang/Object;)V

    .line 87
    :cond_0
    return-void
.end method

.method public i()Ljava/util/Map;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/j;->h:Ljava/util/HashMap;

    return-object v0
.end method
