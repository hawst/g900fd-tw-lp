.class public final Lcom/google/android/gms/drive/a/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/drive/database/r;

.field private final c:Lcom/google/android/gms/drive/database/i;

.field private final d:Lcom/google/android/gms/drive/g/n;

.field private final e:Lcom/google/android/gms/drive/a/a/j;

.field private final f:Lcom/google/android/gms/drive/a/a/h;

.field private final g:Lcom/google/android/gms/drive/g/aw;

.field private final h:Lcom/google/android/gms/drive/g/au;

.field private final i:Lcom/google/android/gms/drive/b/d;

.field private j:Lcom/google/android/gms/drive/a/a/m;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/g/n;Lcom/google/android/gms/drive/a/a/j;Lcom/google/android/gms/drive/a/a/h;Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/g/au;Lcom/google/android/gms/drive/b/d;)V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p1, p0, Lcom/google/android/gms/drive/a/a/a;->a:Landroid/content/Context;

    .line 88
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/r;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/a;->b:Lcom/google/android/gms/drive/database/r;

    .line 89
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/i;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/a;->c:Lcom/google/android/gms/drive/database/i;

    .line 90
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/n;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/a;->d:Lcom/google/android/gms/drive/g/n;

    .line 91
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/a/j;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/a;->e:Lcom/google/android/gms/drive/a/a/j;

    .line 92
    iput-object p6, p0, Lcom/google/android/gms/drive/a/a/a;->f:Lcom/google/android/gms/drive/a/a/h;

    .line 93
    iput-object p7, p0, Lcom/google/android/gms/drive/a/a/a;->g:Lcom/google/android/gms/drive/g/aw;

    .line 94
    iput-object p8, p0, Lcom/google/android/gms/drive/a/a/a;->h:Lcom/google/android/gms/drive/g/au;

    .line 95
    iput-object p9, p0, Lcom/google/android/gms/drive/a/a/a;->i:Lcom/google/android/gms/drive/b/d;

    .line 96
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/g/n;Lcom/google/android/gms/drive/events/k;Lcom/google/android/gms/drive/b/f;Lcom/google/android/gms/drive/b/d;)V
    .locals 12

    .prologue
    .line 70
    new-instance v7, Lcom/google/android/gms/drive/a/a/j;

    move-object/from16 v0, p6

    move-object/from16 v1, p7

    invoke-direct {v7, v0, p3, v1, p1}, Lcom/google/android/gms/drive/a/a/j;-><init>(Lcom/google/android/gms/drive/events/k;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/b/f;Landroid/content/Context;)V

    new-instance v8, Lcom/google/android/gms/drive/a/a/h;

    invoke-direct {v8}, Lcom/google/android/gms/drive/a/a/h;-><init>()V

    new-instance v10, Lcom/google/android/gms/drive/a/a/b;

    invoke-direct {v10}, Lcom/google/android/gms/drive/a/a/b;-><init>()V

    move-object v2, p0

    move-object v3, p1

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object v9, p2

    move-object/from16 v11, p8

    invoke-direct/range {v2 .. v11}, Lcom/google/android/gms/drive/a/a/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/g/n;Lcom/google/android/gms/drive/a/a/j;Lcom/google/android/gms/drive/a/a/h;Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/g/au;Lcom/google/android/gms/drive/b/d;)V

    .line 74
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/gms/drive/a/c;)I
    .locals 13

    .prologue
    const/4 v8, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 114
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/a;->c:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 115
    :try_start_1
    invoke-interface {p1}, Lcom/google/android/gms/drive/a/c;->a()Lcom/google/android/gms/drive/database/model/a;

    move-result-object v9

    .line 120
    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/a/a;->a()Lcom/google/android/gms/drive/a/a/m;

    move-result-object v10

    .line 121
    const-string v0, "ActionQueue"

    const-string v1, "Applying action locally: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/gms/drive/a/u; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/google/android/gms/drive/a/ab; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/google/android/gms/drive/a/h; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 124
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/a;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {p1, v0}, Lcom/google/android/gms/drive/a/c;->c(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/a/g;
    :try_end_2
    .catch Lcom/google/android/gms/drive/a/i; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/gms/drive/a/u; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/google/android/gms/drive/a/ab; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/google/android/gms/drive/a/h; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    .line 130
    :try_start_3
    new-instance v0, Lcom/google/android/gms/drive/database/model/be;

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/a;->c:Lcom/google/android/gms/drive/database/i;

    iget-wide v2, v9, Lcom/google/android/gms/drive/database/model/a;->b:J

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    const-string v11, "forward"

    iget-object v12, v4, Lcom/google/android/gms/drive/a/g;->a:Lcom/google/android/gms/drive/a/c;

    invoke-interface {v12}, Lcom/google/android/gms/drive/a/c;->f()Lorg/json/JSONObject;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v11, "reverse"

    iget-object v4, v4, Lcom/google/android/gms/drive/a/g;->b:Lcom/google/android/gms/drive/a/c;

    invoke-interface {v4}, Lcom/google/android/gms/drive/a/c;->f()Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v5, v11, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, Lcom/google/android/gms/drive/a/c;->j()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/database/model/be;-><init>(Lcom/google/android/gms/drive/database/i;JLjava/lang/String;Lcom/google/android/gms/drive/database/model/EntrySpec;)V

    .line 132
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/be;->i()V

    .line 133
    iget-wide v2, v0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-ltz v0, :cond_1

    move v0, v7

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    iget-object v0, v10, Lcom/google/android/gms/drive/a/a/m;->a:Lcom/google/android/gms/drive/a/a/q;

    new-instance v1, Lcom/google/android/gms/drive/a/a/n;

    invoke-direct {v1, v10, v2, v3}, Lcom/google/android/gms/drive/a/a/n;-><init>(Lcom/google/android/gms/drive/a/a/m;J)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/a/a/q;->execute(Ljava/lang/Runnable;)V

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/a;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0, v9}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/database/model/a;)V

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/a;->c:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->k()V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/android/gms/drive/a/u; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/google/android/gms/drive/a/ab; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/google/android/gms/drive/a/h; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 136
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/a;->c:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->l()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move v0, v6

    .line 166
    :goto_1
    monitor-exit p0

    return v0

    :catch_0
    move-exception v0

    .line 127
    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/a;->c:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->l()V

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/a;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {p1, v0}, Lcom/google/android/gms/drive/a/c;->a(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/a/a/i;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/a;->e:Lcom/google/android/gms/drive/a/a/j;

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/gms/drive/a/a/j;->a(ILcom/google/android/gms/drive/a/a/i;Z)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_0
    move v0, v6

    goto :goto_1

    :cond_1
    move v0, v6

    .line 133
    goto :goto_0

    .line 137
    :catch_1
    move-exception v0

    .line 138
    :try_start_6
    const-string v1, "ActionQueue"

    const-string v2, "Failed to commit action"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 141
    :try_start_7
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/a;->c:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->l()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move v0, v7

    .line 166
    goto :goto_1

    .line 142
    :catch_2
    move-exception v0

    .line 143
    :try_start_8
    const-string v1, "ActionQueue"

    const-string v2, "Failed to apply action"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 146
    :try_start_9
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/a;->c:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->l()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move v0, v8

    .line 166
    goto :goto_1

    .line 147
    :catch_3
    move-exception v0

    .line 148
    :try_start_a
    const-string v1, "ActionQueue"

    const-string v2, "Failed to apply action"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 151
    :try_start_b
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/a;->c:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->l()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 166
    const/4 v0, 0x3

    goto :goto_1

    .line 152
    :catch_4
    move-exception v0

    .line 153
    :try_start_c
    const-string v1, "ActionQueue"

    const-string v2, "Failed to apply action"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 156
    :try_start_d
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/a;->c:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->l()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    move v0, v7

    .line 166
    goto :goto_1

    .line 157
    :catch_5
    move-exception v0

    .line 158
    :try_start_e
    const-string v1, "ActionQueue"

    const-string v2, "Unexpected exception when applying action"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 161
    :try_start_f
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/a;->c:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->l()V

    move v0, v7

    .line 166
    goto :goto_1

    .line 163
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/a;->c:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->l()V

    .line 166
    throw v0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 114
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Lcom/google/android/gms/drive/a/a/m;
    .locals 2

    .prologue
    .line 100
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/a;->j:Lcom/google/android/gms/drive/a/a/m;

    const-string v1, "PendingActionManager is not initialized"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/a/m;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 188
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/a;->j:Lcom/google/android/gms/drive/a/a/m;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    monitor-exit p0

    return-void

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 11

    .prologue
    const/4 v0, 0x1

    const/4 v9, 0x0

    .line 222
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/a;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->d()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v0

    :goto_0
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 223
    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/a;->j:Lcom/google/android/gms/drive/a/a/m;

    if-nez v1, :cond_1

    move v1, v0

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 224
    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/a;->b:Lcom/google/android/gms/drive/database/r;

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a/a;->d:Lcom/google/android/gms/drive/g/n;

    iget-object v3, p0, Lcom/google/android/gms/drive/a/a/a;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/drive/a/a/a;->f:Lcom/google/android/gms/drive/a/a/h;

    iget-object v5, p0, Lcom/google/android/gms/drive/a/a/a;->g:Lcom/google/android/gms/drive/g/aw;

    iget-object v6, p0, Lcom/google/android/gms/drive/a/a/a;->e:Lcom/google/android/gms/drive/a/a/j;

    iget-object v7, p0, Lcom/google/android/gms/drive/a/a/a;->h:Lcom/google/android/gms/drive/g/au;

    iget-object v8, p0, Lcom/google/android/gms/drive/a/a/a;->i:Lcom/google/android/gms/drive/b/d;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->d()Z

    move-result v10

    if-nez v10, :cond_2

    :goto_2
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    new-instance v9, Lcom/google/android/gms/drive/a/a/o;

    invoke-direct {v9}, Lcom/google/android/gms/drive/a/a/o;-><init>()V

    new-instance v0, Lcom/google/android/gms/drive/a/a/m;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/drive/a/a/m;-><init>(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Landroid/content/Context;Lcom/google/android/gms/drive/a/a/h;Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/a/a/j;Lcom/google/android/gms/drive/g/au;Lcom/google/android/gms/drive/b/d;Lcom/google/android/gms/drive/a/a/q;)V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/a/a/m;->a()V

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/a;->j:Lcom/google/android/gms/drive/a/a/m;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    monitor-exit p0

    return-void

    :cond_0
    move v1, v9

    .line 222
    goto :goto_0

    :cond_1
    move v1, v9

    .line 223
    goto :goto_1

    :cond_2
    move v0, v9

    .line 224
    goto :goto_2

    .line 222
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
