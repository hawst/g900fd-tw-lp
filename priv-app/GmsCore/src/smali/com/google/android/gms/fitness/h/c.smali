.class public final Lcom/google/android/gms/fitness/h/c;
.super Lcom/google/android/gms/fitness/h/a;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Map;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/android/gms/fitness/l/z;

.field private e:Lcom/google/android/gms/fitness/service/av;

.field private f:Lcom/google/android/gms/fitness/sensors/a/a;

.field private g:Lcom/google/android/gms/fitness/sensors/a;

.field private h:Lcom/google/android/gms/fitness/k/a;

.field private i:Lcom/google/android/gms/fitness/a/o;

.field private j:Lcom/google/android/gms/fitness/sync/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    new-instance v0, Landroid/support/v4/g/a;

    invoke-direct {v0}, Landroid/support/v4/g/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/h/c;->a:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/google/android/gms/fitness/h/a;-><init>()V

    .line 104
    iput-object p1, p0, Lcom/google/android/gms/fitness/h/c;->b:Landroid/content/Context;

    .line 105
    iput-object p2, p0, Lcom/google/android/gms/fitness/h/c;->c:Ljava/lang/String;

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/c;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/fitness/h/c;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/l/y;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/fitness/l/y;

    move-result-object v0

    .line 108
    iget-object v1, p0, Lcom/google/android/gms/fitness/h/c;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/fitness/l/af;->a(Landroid/content/Context;Lcom/google/android/gms/fitness/l/y;)Lcom/google/android/gms/fitness/l/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/h/c;->d:Lcom/google/android/gms/fitness/l/z;

    .line 109
    return-void
.end method

.method public static declared-synchronized b(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/fitness/h/a;
    .locals 3

    .prologue
    .line 93
    const-class v1, Lcom/google/android/gms/fitness/h/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/fitness/h/c;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/h/a;

    .line 94
    if-nez v0, :cond_0

    .line 95
    new-instance v0, Lcom/google/android/gms/fitness/h/c;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, p1}, Lcom/google/android/gms/fitness/h/c;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 97
    sget-object v2, Lcom/google/android/gms/fitness/h/c;->a:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    :cond_0
    monitor-exit v1

    return-object v0

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/gms/fitness/k/a;)Lcom/google/android/gms/fitness/a/o;
    .locals 3

    .prologue
    .line 172
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/c;->i:Lcom/google/android/gms/fitness/a/o;

    if-nez v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/c;->d:Lcom/google/android/gms/fitness/l/z;

    .line 174
    new-instance v1, Lcom/google/android/gms/fitness/a/i;

    invoke-direct {v1, p1, v0}, Lcom/google/android/gms/fitness/a/i;-><init>(Lcom/google/android/gms/fitness/k/a;Lcom/google/android/gms/fitness/l/z;)V

    .line 176
    new-instance v2, Lcom/google/android/gms/fitness/a/o;

    invoke-direct {v2, p1, v0, v1}, Lcom/google/android/gms/fitness/a/o;-><init>(Lcom/google/android/gms/fitness/k/a;Lcom/google/android/gms/fitness/l/z;Lcom/google/android/gms/fitness/a/i;)V

    iput-object v2, p0, Lcom/google/android/gms/fitness/h/c;->i:Lcom/google/android/gms/fitness/a/o;

    .line 178
    iget-object v1, p0, Lcom/google/android/gms/fitness/h/c;->i:Lcom/google/android/gms/fitness/a/o;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/a/o;->a()Lcom/google/android/gms/fitness/l/ac;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/l/ac;)V

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/c;->i:Lcom/google/android/gms/fitness/a/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/fitness/sensors/a;)Lcom/google/android/gms/fitness/k/a;
    .locals 13

    .prologue
    .line 162
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/c;->h:Lcom/google/android/gms/fitness/k/a;

    if-nez v0, :cond_0

    .line 163
    iget-object v1, p0, Lcom/google/android/gms/fitness/h/c;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/fitness/h/c;->d:Lcom/google/android/gms/fitness/l/z;

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/h/c;->b()Lcom/google/android/gms/fitness/sync/d;

    move-result-object v4

    new-instance v10, Lcom/google/android/gms/fitness/b/a/a;

    sget-object v0, Lcom/google/android/gms/fitness/data/Application;->a:Lcom/google/android/gms/fitness/data/Application;

    invoke-direct {v10, v0}, Lcom/google/android/gms/fitness/b/a/a;-><init>(Lcom/google/android/gms/fitness/data/Application;)V

    new-instance v3, Lcom/google/android/gms/fitness/service/d;

    invoke-direct {v3, v2, v4}, Lcom/google/android/gms/fitness/service/d;-><init>(Lcom/google/android/gms/fitness/l/z;Lcom/google/android/gms/fitness/sync/d;)V

    new-instance v0, Lcom/google/android/gms/fitness/k/c;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/fitness/k/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/fitness/l/z;Lcom/google/android/gms/fitness/service/d;Lcom/google/android/gms/fitness/sync/d;Lcom/google/android/gms/fitness/sensors/a;)V

    new-instance v3, Lcom/google/android/gms/fitness/b/c/ao;

    invoke-direct {v3}, Lcom/google/android/gms/fitness/b/c/ao;-><init>()V

    sget-object v2, Lcom/google/android/gms/fitness/g/c;->aK:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iput-boolean v2, v3, Lcom/google/android/gms/fitness/b/c/ao;->a:Z

    invoke-virtual {v3}, Lcom/google/android/gms/fitness/b/c/ao;->a()Lcom/google/android/gms/fitness/b/c/an;

    move-result-object v3

    new-instance v2, Lcom/google/android/gms/fitness/b/c/ao;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/ao;-><init>()V

    const/4 v4, 0x1

    iput-boolean v4, v2, Lcom/google/android/gms/fitness/b/c/ao;->a:Z

    const/4 v4, 0x1

    iput-boolean v4, v2, Lcom/google/android/gms/fitness/b/c/ao;->b:Z

    const/4 v4, 0x1

    iput-boolean v4, v2, Lcom/google/android/gms/fitness/b/c/ao;->c:Z

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/ao;->a()Lcom/google/android/gms/fitness/b/c/an;

    move-result-object v4

    new-instance v2, Lcom/google/android/gms/fitness/b/c/p;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/p;-><init>()V

    iput-object v10, v2, Lcom/google/android/gms/fitness/b/c/p;->a:Lcom/google/android/gms/fitness/b/b;

    const-string v5, "merge_accelerometer"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->b:Ljava/lang/String;

    const-string v5, "com.google.accelerometer"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->c:Ljava/lang/String;

    const-string v5, "default_accelerometer"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->d:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/p;->a()Lcom/google/android/gms/fitness/b/c/o;

    move-result-object v2

    sget-object v5, Lcom/google/android/gms/fitness/c/e;->c:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v5}, Lcom/google/android/gms/fitness/k/c;->b(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/ah;

    invoke-direct {v2, v10, v4}, Lcom/google/android/gms/fitness/b/c/ah;-><init>(Lcom/google/android/gms/fitness/b/b;Lcom/google/android/gms/fitness/b/c/an;)V

    sget-object v5, Lcom/google/android/gms/fitness/c/e;->c:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v5}, Lcom/google/android/gms/fitness/k/c;->b(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/p;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/p;-><init>()V

    iput-object v10, v2, Lcom/google/android/gms/fitness/b/c/p;->a:Lcom/google/android/gms/fitness/b/b;

    const-string v5, "merge_calories_consumed"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->b:Ljava/lang/String;

    const-string v5, "com.google.calories.consumed"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->c:Ljava/lang/String;

    const-string v5, "default_calories_consumed"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->d:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/p;->a()Lcom/google/android/gms/fitness/b/c/o;

    move-result-object v2

    sget-object v5, Lcom/google/android/gms/fitness/c/e;->c:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v5}, Lcom/google/android/gms/fitness/k/c;->b(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/p;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/p;-><init>()V

    iput-object v10, v2, Lcom/google/android/gms/fitness/b/c/p;->a:Lcom/google/android/gms/fitness/b/b;

    const-string v5, "merge_calories_expended"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->b:Ljava/lang/String;

    const-string v5, "com.google.calories.expended"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->c:Ljava/lang/String;

    const-string v5, "default_calories_expended"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->d:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/p;->a()Lcom/google/android/gms/fitness/b/c/o;

    move-result-object v2

    sget-object v5, Lcom/google/android/gms/fitness/c/e;->c:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v5}, Lcom/google/android/gms/fitness/k/c;->b(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/p;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/p;-><init>()V

    iput-object v10, v2, Lcom/google/android/gms/fitness/b/c/p;->a:Lcom/google/android/gms/fitness/b/b;

    const-string v5, "merge_distance_delta"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->b:Ljava/lang/String;

    const-string v5, "com.google.distance.delta"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->c:Ljava/lang/String;

    const-string v5, "default_distance_delta"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->d:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/p;->a()Lcom/google/android/gms/fitness/b/c/o;

    move-result-object v2

    sget-object v5, Lcom/google/android/gms/fitness/c/e;->c:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v5}, Lcom/google/android/gms/fitness/k/c;->b(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/p;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/p;-><init>()V

    iput-object v10, v2, Lcom/google/android/gms/fitness/b/c/p;->a:Lcom/google/android/gms/fitness/b/b;

    const-string v5, "merge_heart_rate_bpm"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->b:Ljava/lang/String;

    const-string v5, "com.google.heart_rate.bpm"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->c:Ljava/lang/String;

    const-string v5, "default_heart_rate_bpm"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->d:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/p;->a()Lcom/google/android/gms/fitness/b/c/o;

    move-result-object v2

    sget-object v5, Lcom/google/android/gms/fitness/c/e;->c:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v5}, Lcom/google/android/gms/fitness/k/c;->b(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/p;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/p;-><init>()V

    iput-object v10, v2, Lcom/google/android/gms/fitness/b/c/p;->a:Lcom/google/android/gms/fitness/b/b;

    const-string v5, "merge_height"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->b:Ljava/lang/String;

    const-string v5, "com.google.height"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->c:Ljava/lang/String;

    const-string v5, "default_height"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->d:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/p;->a()Lcom/google/android/gms/fitness/b/c/o;

    move-result-object v2

    sget-object v5, Lcom/google/android/gms/fitness/c/e;->c:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v5}, Lcom/google/android/gms/fitness/k/c;->b(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/p;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/p;-><init>()V

    iput-object v10, v2, Lcom/google/android/gms/fitness/b/c/p;->a:Lcom/google/android/gms/fitness/b/b;

    const-string v5, "merge_location_samples"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->b:Ljava/lang/String;

    const-string v5, "com.google.location.sample"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->c:Ljava/lang/String;

    const-string v5, "default_location_samples"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->d:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/p;->a()Lcom/google/android/gms/fitness/b/c/o;

    move-result-object v2

    sget-object v5, Lcom/google/android/gms/fitness/c/e;->c:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v5}, Lcom/google/android/gms/fitness/k/c;->b(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/p;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/p;-><init>()V

    iput-object v10, v2, Lcom/google/android/gms/fitness/b/c/p;->a:Lcom/google/android/gms/fitness/b/b;

    const-string v5, "merge_power_sample"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->b:Ljava/lang/String;

    const-string v5, "com.google.power.sample"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->c:Ljava/lang/String;

    const-string v5, "default_power_sample"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->d:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/p;->a()Lcom/google/android/gms/fitness/b/c/o;

    move-result-object v2

    sget-object v5, Lcom/google/android/gms/fitness/c/e;->c:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v5}, Lcom/google/android/gms/fitness/k/c;->b(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/p;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/p;-><init>()V

    iput-object v10, v2, Lcom/google/android/gms/fitness/b/c/p;->a:Lcom/google/android/gms/fitness/b/b;

    const-string v5, "merge_speed"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->b:Ljava/lang/String;

    const-string v5, "com.google.speed"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->c:Ljava/lang/String;

    const-string v5, "default_speed"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->d:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/p;->a()Lcom/google/android/gms/fitness/b/c/o;

    move-result-object v2

    sget-object v5, Lcom/google/android/gms/fitness/c/e;->c:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v5}, Lcom/google/android/gms/fitness/k/c;->b(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    sget-object v2, Lcom/google/android/gms/fitness/g/c;->aK:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/google/android/gms/fitness/g/c;->aL:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    new-instance v5, Lcom/google/android/gms/fitness/b/c/m;

    invoke-direct {v5}, Lcom/google/android/gms/fitness/b/c/m;-><init>()V

    invoke-virtual {v5, v10}, Lcom/google/android/gms/fitness/b/c/m;->a(Lcom/google/android/gms/fitness/b/b;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v5

    const-string v6, "merge_step_deltas"

    invoke-virtual {v5, v6}, Lcom/google/android/gms/fitness/b/c/f;->d(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v5

    const-string v6, "com.google.step_count.delta"

    invoke-virtual {v5, v6}, Lcom/google/android/gms/fitness/b/c/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v5

    const-string v6, "default_step_deltas"

    invoke-virtual {v5, v6}, Lcom/google/android/gms/fitness/b/c/f;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/google/android/gms/fitness/b/c/f;->a(I)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->a(Lcom/google/android/gms/fitness/b/c/an;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/f;->b()Lcom/google/android/gms/fitness/b/ad;

    move-result-object v2

    sget-object v5, Lcom/google/android/gms/fitness/c/e;->c:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v5}, Lcom/google/android/gms/fitness/k/c;->b(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    :goto_0
    new-instance v2, Lcom/google/android/gms/fitness/b/c/p;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/p;-><init>()V

    iput-object v10, v2, Lcom/google/android/gms/fitness/b/c/p;->a:Lcom/google/android/gms/fitness/b/b;

    const-string v5, "merge_weight"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->b:Ljava/lang/String;

    const-string v5, "com.google.weight"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->c:Ljava/lang/String;

    const-string v5, "default_weight"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->d:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/p;->a()Lcom/google/android/gms/fitness/b/c/o;

    move-result-object v2

    sget-object v5, Lcom/google/android/gms/fitness/c/e;->c:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v5}, Lcom/google/android/gms/fitness/k/c;->b(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v5, Lcom/google/android/gms/fitness/b/c/ao;

    invoke-direct {v5}, Lcom/google/android/gms/fitness/b/c/ao;-><init>()V

    const/4 v2, 0x1

    iput-boolean v2, v5, Lcom/google/android/gms/fitness/b/c/ao;->a:Z

    sget-object v2, Lcom/google/android/gms/fitness/g/c;->aJ:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iput-boolean v2, v5, Lcom/google/android/gms/fitness/b/c/ao;->b:Z

    sget-object v2, Lcom/google/android/gms/fitness/g/c;->aJ:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iput-boolean v2, v5, Lcom/google/android/gms/fitness/b/c/ao;->c:Z

    const/4 v2, 0x3

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    iput-object v2, v5, Lcom/google/android/gms/fitness/b/c/ao;->d:[I

    invoke-virtual {v5}, Lcom/google/android/gms/fitness/b/c/ao;->a()Lcom/google/android/gms/fitness/b/c/an;

    move-result-object v2

    new-instance v5, Lcom/google/android/gms/fitness/b/c/ag;

    invoke-direct {v5, v4, v10}, Lcom/google/android/gms/fitness/b/c/ag;-><init>(Lcom/google/android/gms/fitness/b/c/an;Lcom/google/android/gms/fitness/b/b;)V

    sget-object v4, Lcom/google/android/gms/fitness/c/e;->a:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v5, v4}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v4, Lcom/google/android/gms/fitness/b/c/al;

    invoke-direct {v4}, Lcom/google/android/gms/fitness/b/c/al;-><init>()V

    sget-object v5, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x5

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v6, v7, v8}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/google/android/gms/fitness/b/c/al;->a(J)Lcom/google/android/gms/fitness/b/c/al;

    move-result-object v4

    invoke-virtual {v4, v10}, Lcom/google/android/gms/fitness/b/c/al;->a(Lcom/google/android/gms/fitness/b/b;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/android/gms/fitness/b/c/f;->a(Z)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v4

    const-string v5, "com.google.accelerometer"

    invoke-virtual {v4, v5}, Lcom/google/android/gms/fitness/b/c/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v4

    const-string v5, "merge_accelerometer"

    invoke-virtual {v4, v5}, Lcom/google/android/gms/fitness/b/c/f;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v4

    const-string v5, "default_accelerometer"

    invoke-virtual {v4, v5}, Lcom/google/android/gms/fitness/b/c/f;->c(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/b/c/f;->b()Lcom/google/android/gms/fitness/b/ad;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/fitness/c/e;->a:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v4, Lcom/google/android/gms/fitness/b/c/ab;

    invoke-direct {v4}, Lcom/google/android/gms/fitness/b/c/ab;-><init>()V

    invoke-virtual {v4, v10}, Lcom/google/android/gms/fitness/b/c/ab;->a(Lcom/google/android/gms/fitness/b/b;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/android/gms/fitness/b/c/f;->a(Z)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v4

    const-string v5, "com.google.distance.delta"

    invoke-virtual {v4, v5}, Lcom/google/android/gms/fitness/b/c/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v4

    const-string v5, "merge_distance_delta"

    invoke-virtual {v4, v5}, Lcom/google/android/gms/fitness/b/c/f;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v4

    const-string v5, "default_distance_delta"

    invoke-virtual {v4, v5}, Lcom/google/android/gms/fitness/b/c/f;->c(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v4

    const-string v5, "pruned_distance"

    invoke-virtual {v4, v5}, Lcom/google/android/gms/fitness/b/c/f;->c(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/gms/fitness/b/c/f;->a(Lcom/google/android/gms/fitness/b/c/an;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/f;->b()Lcom/google/android/gms/fitness/b/ad;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/fitness/c/e;->a:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v4}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/al;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/al;-><init>()V

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x1

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v6, v7, v5}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/fitness/b/c/al;->a(J)Lcom/google/android/gms/fitness/b/c/al;

    move-result-object v2

    new-instance v4, Lcom/google/android/gms/fitness/b/x;

    invoke-direct {v4}, Lcom/google/android/gms/fitness/b/x;-><init>()V

    const/high16 v5, 0x41a00000    # 20.0f

    iput v5, v4, Lcom/google/android/gms/fitness/b/x;->a:F

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/b/x;->a()Lcom/google/android/gms/fitness/b/w;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/b/c/al;->a(Lcom/google/android/gms/fitness/b/l;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/google/android/gms/fitness/b/c/f;->a(Lcom/google/android/gms/fitness/b/b;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/b/c/f;->a(Z)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v4, "com.google.heart_rate.bpm"

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/b/c/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v4, "merge_heart_rate_bpm"

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/b/c/f;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v4, "default_heart_rate_bpm"

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/b/c/f;->c(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/f;->b()Lcom/google/android/gms/fitness/b/ad;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/fitness/c/e;->a:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v4}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/al;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/al;-><init>()V

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x1

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v6, v7, v5}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/fitness/b/c/al;->a(J)Lcom/google/android/gms/fitness/b/c/al;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/google/android/gms/fitness/b/c/al;->a(Lcom/google/android/gms/fitness/b/b;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/b/c/f;->a(Z)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v4, "com.google.height"

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/b/c/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v4, "merge_height"

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/b/c/f;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v4, "default_height"

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/b/c/f;->c(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/f;->b()Lcom/google/android/gms/fitness/b/ad;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/fitness/c/e;->a:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v4}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/ad;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/ad;-><init>()V

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0xa

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v6, v7, v5}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/fitness/b/c/ad;->a(J)Lcom/google/android/gms/fitness/b/c/al;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/google/android/gms/fitness/b/c/al;->a(Lcom/google/android/gms/fitness/b/b;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/b/c/f;->a(Z)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v4, "com.google.location.sample"

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/b/c/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v4, "merge_location_samples"

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/b/c/f;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v4, "default_location_samples"

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/b/c/f;->c(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/f;->b()Lcom/google/android/gms/fitness/b/ad;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/fitness/c/e;->a:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v4}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/al;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/al;-><init>()V

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x1

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v6, v7, v5}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/fitness/b/c/al;->a(J)Lcom/google/android/gms/fitness/b/c/al;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/google/android/gms/fitness/b/c/al;->a(Lcom/google/android/gms/fitness/b/b;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/b/c/f;->a(Z)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v4, "com.google.speed"

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/b/c/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v4, "merge_speed"

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/b/c/f;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v4, "default_speed"

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/b/c/f;->c(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/f;->b()Lcom/google/android/gms/fitness/b/ad;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/fitness/c/e;->a:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v4}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/ab;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/ab;-><init>()V

    invoke-virtual {v2, v10}, Lcom/google/android/gms/fitness/b/c/ab;->a(Lcom/google/android/gms/fitness/b/b;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/b/c/f;->a(Z)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->a(Lcom/google/android/gms/fitness/b/c/an;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v3, "com.google.step_count.delta"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v3, "merge_step_deltas"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v3, "estimated_steps"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->c(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v3, "live_step_deltas"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->c(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v3, "default_step_deltas"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->c(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/f;->b()Lcom/google/android/gms/fitness/b/ad;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/fitness/c/e;->a:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/al;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/al;-><init>()V

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5, v6}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/fitness/b/c/al;->a(J)Lcom/google/android/gms/fitness/b/c/al;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/google/android/gms/fitness/b/c/al;->a(Lcom/google/android/gms/fitness/b/b;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->a(Z)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/fitness/b/x;

    invoke-direct {v3}, Lcom/google/android/gms/fitness/b/x;-><init>()V

    const v4, 0x3e8f5c29    # 0.28f

    iput v4, v3, Lcom/google/android/gms/fitness/b/x;->a:F

    invoke-virtual {v3}, Lcom/google/android/gms/fitness/b/x;->a()Lcom/google/android/gms/fitness/b/w;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->a(Lcom/google/android/gms/fitness/b/l;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v3, "com.google.weight"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v3, "merge_weight"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v3, "default_weight"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->c(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/f;->b()Lcom/google/android/gms/fitness/b/ad;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/fitness/c/e;->a:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/ab;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/ab;-><init>()V

    invoke-virtual {v2, v10}, Lcom/google/android/gms/fitness/b/c/ab;->a(Lcom/google/android/gms/fitness/b/b;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->a(Z)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v3, "com.google.calories.consumed"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v3, "merge_calories_consumed"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v3, "default_calories_consumed"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->c(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/f;->b()Lcom/google/android/gms/fitness/b/ad;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/fitness/c/e;->a:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/ab;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/ab;-><init>()V

    invoke-virtual {v2, v10}, Lcom/google/android/gms/fitness/b/c/ab;->a(Lcom/google/android/gms/fitness/b/b;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->a(Z)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v3, "com.google.calories.expended"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v3, "merge_calories_expended"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v3, "default_calories_expended"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->c(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/f;->b()Lcom/google/android/gms/fitness/b/ad;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/fitness/c/e;->a:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/al;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/al;-><init>()V

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5, v6}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/fitness/b/c/al;->a(J)Lcom/google/android/gms/fitness/b/c/al;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/google/android/gms/fitness/b/c/al;->a(Lcom/google/android/gms/fitness/b/b;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->a(Z)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v3, "com.google.power.sample"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v3, "merge_power_sample"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    const-string v3, "default_power_sample"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/c/f;->c(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/f;->b()Lcom/google/android/gms/fitness/b/ad;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/fitness/c/e;->a:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/p;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/p;-><init>()V

    iput-object v10, v2, Lcom/google/android/gms/fitness/b/c/p;->a:Lcom/google/android/gms/fitness/b/b;

    const-string v3, "merge_activity_segments"

    iput-object v3, v2, Lcom/google/android/gms/fitness/b/c/p;->b:Ljava/lang/String;

    const-string v3, "com.google.activity.segment"

    iput-object v3, v2, Lcom/google/android/gms/fitness/b/c/p;->c:Ljava/lang/String;

    const-string v3, "default_activity_segments"

    iput-object v3, v2, Lcom/google/android/gms/fitness/b/c/p;->d:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/p;->a()Lcom/google/android/gms/fitness/b/c/o;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/fitness/c/e;->c:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v9, Lcom/google/android/gms/fitness/b/c/ap;

    const-string v11, "default_step_deltas"

    const-string v2, "overlay_user_input"

    invoke-static {v2}, Lcom/google/android/gms/fitness/b/c/v;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    new-instance v2, Lcom/google/android/gms/fitness/b/c/s;

    sget-object v3, Lcom/google/android/gms/fitness/g/c;->as:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/fitness/g/a;->b(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/fitness/g/c;->at:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/fitness/g/a;->c(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/fitness/g/c;->au:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/gms/fitness/g/a;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/fitness/g/c;->av:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    sget-object v7, Lcom/google/android/gms/fitness/g/c;->aw:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v7}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    sget-object v8, Lcom/google/android/gms/fitness/b/c/u;->a:Lcom/google/android/gms/fitness/b/c/u;

    invoke-direct/range {v2 .. v8}, Lcom/google/android/gms/fitness/b/c/s;-><init>(Ljava/util/Map;Ljava/util/Set;Ljava/util/Map;IILcom/google/android/gms/fitness/b/c/u;)V

    invoke-direct {v9, v11, v12, v10, v2}, Lcom/google/android/gms/fitness/b/c/ap;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/fitness/b/b;Lcom/google/android/gms/fitness/b/c/s;)V

    sget-object v2, Lcom/google/android/gms/fitness/c/e;->a:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v9, v2}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v3, Lcom/google/android/gms/fitness/b/c/x;

    sget-object v2, Lcom/google/android/gms/fitness/g/c;->az:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    sget-object v2, Lcom/google/android/gms/fitness/g/c;->aw:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-float v6, v2

    sget-object v2, Lcom/google/android/gms/fitness/g/c;->av:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-float v7, v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v2, Lcom/google/android/gms/fitness/g/c;->aA:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v8, v2

    invoke-virtual {v4, v8, v9}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v8

    move-object v4, v10

    invoke-direct/range {v3 .. v9}, Lcom/google/android/gms/fitness/b/c/x;-><init>(Lcom/google/android/gms/fitness/b/b;IFFJ)V

    sget-object v2, Lcom/google/android/gms/fitness/c/e;->a:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    new-instance v2, Lcom/google/android/gms/fitness/b/c/z;

    const-string v3, "default_distance_delta"

    const-string v4, "overlay_user_input"

    invoke-static {v4}, Lcom/google/android/gms/fitness/b/c/v;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v10}, Lcom/google/android/gms/fitness/b/c/z;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/fitness/b/b;)V

    sget-object v3, Lcom/google/android/gms/fitness/c/e;->c:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    sget-object v2, Lcom/google/android/gms/fitness/g/c;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sget-object v2, Lcom/google/android/gms/fitness/g/c;->r:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v1}, Lcom/google/android/gms/fitness/data/Device;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/google/android/gms/fitness/g/c;->p:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    :goto_1
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v3, Lcom/google/android/gms/fitness/b/c/j;

    const/4 v4, 0x1

    move-object v5, v10

    invoke-direct/range {v3 .. v9}, Lcom/google/android/gms/fitness/b/c/j;-><init>(ZLcom/google/android/gms/fitness/b/b;JJ)V

    :goto_2
    sget-object v2, Lcom/google/android/gms/fitness/c/e;->a:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;

    sget-object v2, Lcom/google/android/gms/fitness/c/b;->b:Lcom/google/android/gms/fitness/c/b;

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/c/a;->a(Landroid/content/Context;Lcom/google/android/gms/fitness/c/b;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/k/c;->a(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/k/c;

    new-instance v9, Lcom/google/android/gms/fitness/k/a;

    iget-object v1, v0, Lcom/google/android/gms/fitness/k/c;->a:Lcom/google/android/gms/fitness/l/z;

    iget-object v2, v0, Lcom/google/android/gms/fitness/k/c;->b:Lcom/google/android/gms/fitness/service/d;

    iget-object v3, v0, Lcom/google/android/gms/fitness/k/c;->d:Lcom/google/android/gms/fitness/sensors/a;

    iget-object v4, v0, Lcom/google/android/gms/fitness/k/c;->e:Lcom/google/android/gms/fitness/k/d;

    iget-object v5, v0, Lcom/google/android/gms/fitness/k/c;->c:Lcom/google/android/gms/fitness/sync/d;

    iget-object v6, v0, Lcom/google/android/gms/fitness/k/c;->g:Ljava/util/Map;

    iget-object v7, v0, Lcom/google/android/gms/fitness/k/c;->f:Ljava/util/Map;

    new-instance v8, Lcom/google/android/gms/fitness/k/f;

    iget-object v10, v0, Lcom/google/android/gms/fitness/k/c;->b:Lcom/google/android/gms/fitness/service/d;

    iget-object v11, v0, Lcom/google/android/gms/fitness/k/c;->f:Ljava/util/Map;

    iget-object v0, v0, Lcom/google/android/gms/fitness/k/c;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Device;->a(Landroid/content/Context;)Lcom/google/android/gms/fitness/data/Device;

    move-result-object v0

    invoke-direct {v8, v10, v11, v0}, Lcom/google/android/gms/fitness/k/f;-><init>(Lcom/google/android/gms/fitness/service/d;Ljava/util/Map;Lcom/google/android/gms/fitness/data/Device;)V

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/fitness/k/a;-><init>(Lcom/google/android/gms/fitness/l/z;Lcom/google/android/gms/fitness/service/d;Lcom/google/android/gms/fitness/sensors/a;Lcom/google/android/gms/fitness/k/d;Lcom/google/android/gms/fitness/sync/d;Ljava/util/Map;Ljava/util/Map;Lcom/google/android/gms/fitness/k/f;)V

    iput-object v9, p0, Lcom/google/android/gms/fitness/h/c;->h:Lcom/google/android/gms/fitness/k/a;

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/c;->h:Lcom/google/android/gms/fitness/k/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 163
    :cond_1
    :try_start_1
    new-instance v2, Lcom/google/android/gms/fitness/b/c/p;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/b/c/p;-><init>()V

    iput-object v10, v2, Lcom/google/android/gms/fitness/b/c/p;->a:Lcom/google/android/gms/fitness/b/b;

    const-string v5, "merge_step_deltas"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->b:Ljava/lang/String;

    const-string v5, "com.google.step_count.delta"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->c:Ljava/lang/String;

    const-string v5, "default_step_deltas"

    iput-object v5, v2, Lcom/google/android/gms/fitness/b/c/p;->d:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/b/c/p;->a()Lcom/google/android/gms/fitness/b/c/o;

    move-result-object v2

    sget-object v5, Lcom/google/android/gms/fitness/c/e;->c:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v0, v2, v5}, Lcom/google/android/gms/fitness/k/c;->b(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/c/e;)Lcom/google/android/gms/fitness/k/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 163
    :cond_2
    :try_start_2
    sget-object v2, Lcom/google/android/gms/fitness/g/c;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/fitness/b/c/h;

    const/4 v2, 0x1

    invoke-direct {v3, v2, v10, v6, v7}, Lcom/google/android/gms/fitness/b/c/h;-><init>(ZLcom/google/android/gms/fitness/b/b;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :array_0
    .array-data 4
        0x1
        0x3
        0x2
    .end array-data
.end method

.method public final a()Lcom/google/android/gms/fitness/l/z;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/c;->d:Lcom/google/android/gms/fitness/l/z;

    return-object v0
.end method

.method public final declared-synchronized a(Landroid/os/Handler;)Lcom/google/android/gms/fitness/sensors/a/a;
    .locals 3

    .prologue
    .line 132
    monitor-enter p0

    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v1, 0x12

    if-ge v0, v1, :cond_0

    .line 133
    const/4 v0, 0x0

    .line 140
    :goto_0
    monitor-exit p0

    return-object v0

    .line 136
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/c;->f:Lcom/google/android/gms/fitness/sensors/a/a;

    if-nez v0, :cond_1

    .line 137
    new-instance v0, Lcom/google/android/gms/fitness/sensors/a/a;

    iget-object v1, p0, Lcom/google/android/gms/fitness/h/c;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/fitness/h/c;->d:Lcom/google/android/gms/fitness/l/z;

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/gms/fitness/sensors/a/a;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/gms/fitness/l/z;)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/h/c;->f:Lcom/google/android/gms/fitness/sensors/a/a;

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/c;->d:Lcom/google/android/gms/fitness/l/z;

    iget-object v1, p0, Lcom/google/android/gms/fitness/h/c;->f:Lcom/google/android/gms/fitness/sensors/a/a;

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/l/aa;)V

    .line 140
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/c;->f:Lcom/google/android/gms/fitness/sensors/a/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/List;Lcom/google/android/gms/fitness/sensors/a/a;)Lcom/google/android/gms/fitness/sensors/a;
    .locals 4

    .prologue
    .line 146
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/c;->g:Lcom/google/android/gms/fitness/sensors/a;

    if-nez v0, :cond_0

    .line 147
    if-nez p2, :cond_1

    .line 148
    new-instance v0, Lcom/google/android/gms/fitness/sensors/b/b;

    invoke-direct {v0, p1}, Lcom/google/android/gms/fitness/sensors/b/b;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/h/c;->g:Lcom/google/android/gms/fitness/sensors/a;

    .line 157
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/c;->g:Lcom/google/android/gms/fitness/sensors/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 150
    :cond_1
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 151
    new-instance v1, Lcom/google/android/gms/fitness/sensors/b/d;

    invoke-direct {v1, p2}, Lcom/google/android/gms/fitness/sensors/b/d;-><init>(Lcom/google/android/gms/fitness/sensors/a;)V

    .line 153
    new-instance v2, Lcom/google/android/gms/fitness/sensors/e/f;

    const-string v3, "BLE"

    invoke-direct {v2, v1, v3}, Lcom/google/android/gms/fitness/sensors/e/f;-><init>(Lcom/google/android/gms/fitness/sensors/a;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    new-instance v1, Lcom/google/android/gms/fitness/sensors/b/b;

    invoke-direct {v1, v0}, Lcom/google/android/gms/fitness/sensors/b/b;-><init>(Ljava/util/List;)V

    iput-object v1, p0, Lcom/google/android/gms/fitness/h/c;->g:Lcom/google/android/gms/fitness/sensors/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/fitness/sensors/a;Lcom/google/android/gms/fitness/l/a/a;)Lcom/google/android/gms/fitness/service/av;
    .locals 2

    .prologue
    .line 119
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/c;->e:Lcom/google/android/gms/fitness/service/av;

    if-nez v0, :cond_0

    .line 120
    new-instance v0, Lcom/google/android/gms/fitness/service/av;

    iget-object v1, p0, Lcom/google/android/gms/fitness/h/c;->b:Landroid/content/Context;

    invoke-direct {v0, p1, p2, v1}, Lcom/google/android/gms/fitness/service/av;-><init>(Lcom/google/android/gms/fitness/sensors/a;Lcom/google/android/gms/fitness/l/a/a;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/h/c;->e:Lcom/google/android/gms/fitness/service/av;

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/c;->e:Lcom/google/android/gms/fitness/service/av;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Lcom/google/android/gms/fitness/sync/d;
    .locals 4

    .prologue
    .line 185
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/c;->j:Lcom/google/android/gms/fitness/sync/d;

    if-nez v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/c;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/fitness/data/Device;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188
    new-instance v0, Lcom/google/android/gms/fitness/sync/e;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/sync/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/h/c;->j:Lcom/google/android/gms/fitness/sync/d;

    .line 193
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/c;->j:Lcom/google/android/gms/fitness/sync/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 190
    :cond_1
    :try_start_1
    new-instance v0, Lcom/google/android/gms/fitness/apiary/m;

    iget-object v1, p0, Lcom/google/android/gms/fitness/h/c;->d:Lcom/google/android/gms/fitness/l/z;

    iget-object v2, p0, Lcom/google/android/gms/fitness/h/c;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/fitness/h/c;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/fitness/apiary/m;-><init>(Lcom/google/android/gms/fitness/l/z;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/h/c;->j:Lcom/google/android/gms/fitness/sync/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 185
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
