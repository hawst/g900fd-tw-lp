.class public final Lcom/google/android/gms/smart_profile/b/i;
.super Landroid/support/v4/a/j;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/internal/av;


# instance fields
.field private a:Ljava/util/List;

.field private b:Lcom/google/android/gms/plus/internal/ab;

.field private c:Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/plus/internal/ab;Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/support/v4/a/j;-><init>(Landroid/content/Context;)V

    .line 38
    iput-object p2, p0, Lcom/google/android/gms/smart_profile/b/i;->b:Lcom/google/android/gms/plus/internal/ab;

    .line 39
    iput-object p3, p0, Lcom/google/android/gms/smart_profile/b/i;->c:Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;

    .line 40
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/b/i;->a:Ljava/util/List;

    .line 78
    iget-boolean v0, p0, Landroid/support/v4/a/j;->p:Z

    if-eqz v0, :cond_0

    .line 79
    invoke-super {p0, p1}, Landroid/support/v4/a/j;->b(Ljava/lang/Object;)V

    .line 81
    :cond_0
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0}, Landroid/support/v4/a/j;->a()V

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/i;->b:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/i;->b:Lcom/google/android/gms/plus/internal/ab;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/b/i;->c:Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;

    invoke-interface {v0, p0, v1}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/av;Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;)V

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/i;->b:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/i;->b:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;)V
    .locals 2

    .prologue
    .line 86
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;->b()[B

    move-result-object v0

    .line 88
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 89
    invoke-static {v0}, Lcom/google/android/gms/smart_profile/ar;->a([B)Lcom/google/ac/c/a/a/a/r;

    move-result-object v0

    .line 91
    if-eqz v0, :cond_0

    .line 92
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/smart_profile/b/i;->a:Ljava/util/List;

    .line 93
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/b/i;->a:Ljava/util/List;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/r;->d:[Lcom/google/ac/c/a/a/a/f;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/i;->a:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/b/i;->a(Ljava/util/List;)V

    .line 101
    :goto_0
    return-void

    .line 99
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/b/i;->a:Ljava/util/List;

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/i;->a:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/b/i;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method public final synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/b/i;->a(Ljava/util/List;)V

    return-void
.end method

.method protected final e()V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Landroid/support/v4/a/j;->e()V

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/i;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/i;->a:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/b/i;->a(Ljava/util/List;)V

    .line 50
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/b/i;->i()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/i;->a:Ljava/util/List;

    if-nez v0, :cond_2

    .line 51
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/a/j;->a()V

    .line 53
    :cond_2
    return-void
.end method

.method protected final g()V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Landroid/support/v4/a/j;->g()V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/b/i;->f()V

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/b/i;->a:Ljava/util/List;

    .line 72
    return-void
.end method
