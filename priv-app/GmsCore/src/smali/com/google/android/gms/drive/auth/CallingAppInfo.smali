.class public Lcom/google/android/gms/drive/auth/CallingAppInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:J

.field private final b:Ljava/lang/String;

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/google/android/gms/drive/auth/h;

    invoke-direct {v0}, Lcom/google/android/gms/drive/auth/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/auth/CallingAppInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;I)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-wide p1, p0, Lcom/google/android/gms/drive/auth/CallingAppInfo;->a:J

    .line 44
    iput-object p3, p0, Lcom/google/android/gms/drive/auth/CallingAppInfo;->b:Ljava/lang/String;

    .line 45
    iput p4, p0, Lcom/google/android/gms/drive/auth/CallingAppInfo;->c:I

    .line 46
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/auth/g;I)V
    .locals 3

    .prologue
    .line 49
    iget-wide v0, p1, Lcom/google/android/gms/drive/auth/g;->b:J

    iget-object v2, p1, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/auth/AppIdentity;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2, p2}, Lcom/google/android/gms/drive/auth/CallingAppInfo;-><init>(JLjava/lang/String;I)V

    .line 50
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/database/model/f;)V
    .locals 4

    .prologue
    .line 53
    iget-wide v0, p1, Lcom/google/android/gms/drive/database/model/f;->b:J

    iget-object v2, p1, Lcom/google/android/gms/drive/database/model/f;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/auth/AppIdentity;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/drive/auth/CallingAppInfo;-><init>(JLjava/lang/String;I)V

    .line 55
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/google/android/gms/drive/auth/CallingAppInfo;->a:J

    return-wide v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/drive/auth/CallingAppInfo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/google/android/gms/drive/auth/CallingAppInfo;->c:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 86
    iget-wide v0, p0, Lcom/google/android/gms/drive/auth/CallingAppInfo;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/drive/auth/CallingAppInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 88
    iget v0, p0, Lcom/google/android/gms/drive/auth/CallingAppInfo;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 89
    return-void
.end method
