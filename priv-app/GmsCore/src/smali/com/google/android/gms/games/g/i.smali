.class public final Lcom/google/android/gms/games/g/i;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Z

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2641
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2642
    iput v1, p0, Lcom/google/android/gms/games/g/i;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/i;->b:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/games/g/i;->c:I

    iput-boolean v1, p0, Lcom/google/android/gms/games/g/i;->d:Z

    iput-boolean v1, p0, Lcom/google/android/gms/games/g/i;->e:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/i;->cachedSize:I

    .line 2643
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2721
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2722
    iget v1, p0, Lcom/google/android/gms/games/g/i;->a:I

    if-eqz v1, :cond_0

    .line 2723
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/g/i;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2726
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/g/i;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2727
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/g/i;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2730
    :cond_1
    iget v1, p0, Lcom/google/android/gms/games/g/i;->c:I

    if-eqz v1, :cond_2

    .line 2731
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/games/g/i;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2734
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/games/g/i;->d:Z

    if-eqz v1, :cond_3

    .line 2735
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/gms/games/g/i;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2738
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/gms/games/g/i;->e:Z

    if-eqz v1, :cond_4

    .line 2739
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/gms/games/g/i;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2742
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2657
    if-ne p1, p0, :cond_1

    .line 2683
    :cond_0
    :goto_0
    return v0

    .line 2660
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/i;

    if-nez v2, :cond_2

    move v0, v1

    .line 2661
    goto :goto_0

    .line 2663
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/i;

    .line 2664
    iget v2, p0, Lcom/google/android/gms/games/g/i;->a:I

    iget v3, p1, Lcom/google/android/gms/games/g/i;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 2665
    goto :goto_0

    .line 2667
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/i;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2668
    iget-object v2, p1, Lcom/google/android/gms/games/g/i;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 2669
    goto :goto_0

    .line 2671
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/g/i;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/i;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 2672
    goto :goto_0

    .line 2674
    :cond_5
    iget v2, p0, Lcom/google/android/gms/games/g/i;->c:I

    iget v3, p1, Lcom/google/android/gms/games/g/i;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 2675
    goto :goto_0

    .line 2677
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/gms/games/g/i;->d:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/g/i;->d:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2678
    goto :goto_0

    .line 2680
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/gms/games/g/i;->e:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/g/i;->e:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2681
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 2688
    iget v0, p0, Lcom/google/android/gms/games/g/i;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 2690
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/i;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v3

    .line 2692
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/games/g/i;->c:I

    add-int/2addr v0, v3

    .line 2693
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/g/i;->d:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 2694
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/google/android/gms/games/g/i;->e:Z

    if-eqz v3, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 2695
    return v0

    .line 2690
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/i;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2693
    goto :goto_1

    :cond_2
    move v1, v2

    .line 2694
    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2596
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/g/i;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/i;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/i;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/g/i;->d:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/g/i;->e:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2701
    iget v0, p0, Lcom/google/android/gms/games/g/i;->a:I

    if-eqz v0, :cond_0

    .line 2702
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/g/i;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2704
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/i;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2705
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/g/i;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2707
    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/g/i;->c:I

    if-eqz v0, :cond_2

    .line 2708
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/games/g/i;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2710
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/games/g/i;->d:Z

    if-eqz v0, :cond_3

    .line 2711
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/gms/games/g/i;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2713
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/games/g/i;->e:Z

    if-eqz v0, :cond_4

    .line 2714
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/gms/games/g/i;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2716
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2717
    return-void
.end method
