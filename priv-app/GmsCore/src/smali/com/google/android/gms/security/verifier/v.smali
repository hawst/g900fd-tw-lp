.class public final Lcom/google/android/gms/security/verifier/v;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Lcom/google/android/gms/security/verifier/w;

.field public c:[B

.field public d:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2333
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2334
    iput-object v0, p0, Lcom/google/android/gms/security/verifier/v;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/v;->b:Lcom/google/android/gms/security/verifier/w;

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/v;->c:[B

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/v;->d:[B

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/security/verifier/v;->cachedSize:I

    .line 2335
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2408
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2409
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/v;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2411
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/v;->b:Lcom/google/android/gms/security/verifier/w;

    if-eqz v1, :cond_0

    .line 2412
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/v;->b:Lcom/google/android/gms/security/verifier/w;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2415
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/v;->c:[B

    if-eqz v1, :cond_1

    .line 2416
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/v;->c:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 2419
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/v;->d:[B

    if-eqz v1, :cond_2

    .line 2420
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/v;->d:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 2423
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2348
    if-ne p1, p0, :cond_1

    .line 2376
    :cond_0
    :goto_0
    return v0

    .line 2351
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/security/verifier/v;

    if-nez v2, :cond_2

    move v0, v1

    .line 2352
    goto :goto_0

    .line 2354
    :cond_2
    check-cast p1, Lcom/google/android/gms/security/verifier/v;

    .line 2355
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/v;->a:Ljava/lang/Integer;

    if-nez v2, :cond_3

    .line 2356
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/v;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    move v0, v1

    .line 2357
    goto :goto_0

    .line 2359
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/v;->a:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/v;->a:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2360
    goto :goto_0

    .line 2361
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/v;->b:Lcom/google/android/gms/security/verifier/w;

    if-nez v2, :cond_5

    .line 2362
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/v;->b:Lcom/google/android/gms/security/verifier/w;

    if-eqz v2, :cond_6

    move v0, v1

    .line 2363
    goto :goto_0

    .line 2366
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/v;->b:Lcom/google/android/gms/security/verifier/w;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/v;->b:Lcom/google/android/gms/security/verifier/w;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/security/verifier/w;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 2367
    goto :goto_0

    .line 2370
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/v;->c:[B

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/v;->c:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 2371
    goto :goto_0

    .line 2373
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/v;->d:[B

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/v;->d:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2374
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2381
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/v;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2383
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/v;->b:Lcom/google/android/gms/security/verifier/w;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 2385
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/v;->c:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int/2addr v0, v1

    .line 2386
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/v;->d:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int/2addr v0, v1

    .line 2387
    return v0

    .line 2381
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/v;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 2383
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/v;->b:Lcom/google/android/gms/security/verifier/w;

    invoke-virtual {v1}, Lcom/google/android/gms/security/verifier/w;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2164
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/v;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/v;->b:Lcom/google/android/gms/security/verifier/w;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/security/verifier/w;

    invoke-direct {v0}, Lcom/google/android/gms/security/verifier/w;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/v;->b:Lcom/google/android/gms/security/verifier/w;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/v;->b:Lcom/google/android/gms/security/verifier/w;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/v;->c:[B

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/v;->d:[B

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2393
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/v;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2394
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/v;->b:Lcom/google/android/gms/security/verifier/w;

    if-eqz v0, :cond_0

    .line 2395
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/v;->b:Lcom/google/android/gms/security/verifier/w;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2397
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/v;->c:[B

    if-eqz v0, :cond_1

    .line 2398
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/v;->c:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 2400
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/v;->d:[B

    if-eqz v0, :cond_2

    .line 2401
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/v;->d:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 2403
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2404
    return-void
.end method
