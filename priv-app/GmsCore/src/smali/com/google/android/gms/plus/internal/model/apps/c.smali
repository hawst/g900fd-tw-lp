.class public final Lcom/google/android/gms/plus/internal/model/apps/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 41
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "icon_url"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "application_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "application_info"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "is_aspen"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "scopes"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "revoke_handle"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "has_conn_read"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "is_fitness"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/plus/internal/model/apps/c;->a:[Ljava/lang/String;

    return-void
.end method

.method public static final a([B)Landroid/content/pm/ApplicationInfo;
    .locals 3

    .prologue
    .line 71
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 73
    const/4 v0, 0x0

    :try_start_0
    array-length v2, p0

    invoke-virtual {v1, p0, v0, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 74
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 75
    const-class v0, Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public static final a(Landroid/content/pm/ApplicationInfo;)[B
    .locals 2

    .prologue
    .line 58
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 60
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1, p0, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 61
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 63
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method
