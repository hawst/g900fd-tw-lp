.class public final Lcom/google/android/gms/ads/internal/overlay/m;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Landroid/widget/ImageButton;


# direct methods
.method public constructor <init>(Landroid/app/Activity;I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 30
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 31
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/overlay/m;->a:Landroid/app/Activity;

    .line 34
    invoke-virtual {p0, p0}, Lcom/google/android/gms/ads/internal/overlay/m;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    new-instance v0, Landroid/widget/ImageButton;

    invoke-direct {v0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/m;->b:Landroid/widget/ImageButton;

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/m;->b:Landroid/widget/ImageButton;

    const v1, 0x1080017

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/m;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/m;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/m;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/ImageButton;->setPadding(IIII)V

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/m;->b:Landroid/widget/ImageButton;

    const-string v1, "Interstitial close button"

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 44
    invoke-static {p1, p2}, Lcom/google/android/gms/ads/internal/util/client/a;->a(Landroid/content/Context;I)I

    move-result v0

    .line 45
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/overlay/m;->b:Landroid/widget/ImageButton;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v3, 0x11

    invoke-direct {v2, v0, v0, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/ads/internal/overlay/m;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 47
    return-void
.end method


# virtual methods
.method public final a(ZZ)V
    .locals 2

    .prologue
    .line 58
    if-eqz p2, :cond_1

    .line 59
    if-eqz p1, :cond_0

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/m;->b:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 73
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/m;->b:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/m;->b:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/m;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 52
    return-void
.end method
