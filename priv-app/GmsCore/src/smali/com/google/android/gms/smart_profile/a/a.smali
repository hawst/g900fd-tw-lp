.class public final Lcom/google/android/gms/smart_profile/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final A:Lcom/google/android/gms/common/a/d;

.field public static final B:Lcom/google/android/gms/common/a/d;

.field public static final C:Lcom/google/android/gms/common/a/d;

.field public static final D:Lcom/google/android/gms/common/a/d;

.field public static final E:Lcom/google/android/gms/common/a/d;

.field public static final F:Lcom/google/android/gms/common/a/d;

.field public static final a:Lcom/google/android/gms/common/a/d;

.field public static final b:Lcom/google/android/gms/common/a/d;

.field public static final c:Lcom/google/android/gms/common/a/d;

.field public static final d:Lcom/google/android/gms/common/a/d;

.field public static final e:Lcom/google/android/gms/common/a/d;

.field public static final f:Lcom/google/android/gms/common/a/d;

.field public static final g:Lcom/google/android/gms/common/a/d;

.field public static final h:Lcom/google/android/gms/common/a/d;

.field public static final i:Lcom/google/android/gms/common/a/d;

.field public static final j:Lcom/google/android/gms/common/a/d;

.field public static final k:Lcom/google/android/gms/common/a/d;

.field public static final l:Lcom/google/android/gms/common/a/d;

.field public static final m:Lcom/google/android/gms/common/a/d;

.field public static final n:Lcom/google/android/gms/common/a/d;

.field public static final o:Lcom/google/android/gms/common/a/d;

.field public static final p:Lcom/google/android/gms/common/a/d;

.field public static final q:Lcom/google/android/gms/common/a/d;

.field public static final r:Lcom/google/android/gms/common/a/d;

.field public static final s:Lcom/google/android/gms/common/a/d;

.field public static final t:Lcom/google/android/gms/common/a/d;

.field public static final u:Lcom/google/android/gms/common/a/d;

.field public static final v:Lcom/google/android/gms/common/a/d;

.field public static final w:Lcom/google/android/gms/common/a/d;

.field public static final x:Lcom/google/android/gms/common/a/d;

.field public static final y:Lcom/google/android/gms/common/a/d;

.field public static final z:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 17
    const-string v0, "smart_profile.number_past_milliseconds_to_search_local_calendar"

    const-wide v2, 0x39ef8b000L

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->a:Lcom/google/android/gms/common/a/d;

    .line 26
    const-string v0, "smart_profile.number_future_milliseconds_to_search_local_calendar"

    const-wide/32 v2, 0x2932e00

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->b:Lcom/google/android/gms/common/a/d;

    .line 34
    const-string v0, "smart_profile.number_communicate_bar_entries"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->c:Lcom/google/android/gms/common/a/d;

    .line 40
    const-string v0, "smart_profile.hangouts_communicate_card_entries_first"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->d:Lcom/google/android/gms/common/a/d;

    .line 46
    const-string v0, "smart_profile.hangouts_package_name"

    const-string v1, "com.google.android.talk"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->e:Lcom/google/android/gms/common/a/d;

    .line 53
    const-string v0, "smart_profile.show_quick_contacts_link"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->f:Lcom/google/android/gms/common/a/d;

    .line 59
    const-string v0, "smart_profile.show_star_unstar_icon"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->g:Lcom/google/android/gms/common/a/d;

    .line 65
    const-string v0, "smart_profile.number_generic_card_entries"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->h:Lcom/google/android/gms/common/a/d;

    .line 71
    const-string v0, "smart_profile.number_about_card_entries"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->i:Lcom/google/android/gms/common/a/d;

    .line 77
    const-string v0, "smart_profile.number_call_entries_in_recent_interactions"

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->j:Lcom/google/android/gms/common/a/d;

    .line 85
    const-string v0, "smart_profile.number_upcoming_calendar_entries_in_recent_interactions"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->k:Lcom/google/android/gms/common/a/d;

    .line 92
    const-string v0, "smart_profile.number_sms_threads_in_recent_interactions"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->l:Lcom/google/android/gms/common/a/d;

    .line 98
    const-string v0, "smart_profile.number_enties_in_recent_interactions"

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->m:Lcom/google/android/gms/common/a/d;

    .line 105
    const-string v0, "smart_profile.number_recent_interactions_initially_visible"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->n:Lcom/google/android/gms/common/a/d;

    .line 111
    const-string v0, "smart_profile.number_photos_min"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->o:Lcom/google/android/gms/common/a/d;

    .line 117
    const-string v0, "smart_profile.number_photos_max"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->p:Lcom/google/android/gms/common/a/d;

    .line 123
    const-string v0, "smart_profile.number_people_min"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->q:Lcom/google/android/gms/common/a/d;

    .line 129
    const-string v0, "smart_profile.number_people_max"

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->r:Lcom/google/android/gms/common/a/d;

    .line 135
    const-string v0, "smart_profile.use_built_in_see_all_people"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->s:Lcom/google/android/gms/common/a/d;

    .line 141
    const-string v0, "smart_profile.view_google_plus_profile_intent_format_url"

    const-string v1, "https://plus.google.com/%s"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->t:Lcom/google/android/gms/common/a/d;

    .line 148
    const-string v0, "smart_profile.view_google_plus_people_in_common_intent_format_url"

    const-string v1, "http://plus.google.com/people/%s/peopleInCommon/%s"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->u:Lcom/google/android/gms/common/a/d;

    .line 156
    const-string v0, "smart_profile.view_google_plus_people_in_owner_circles_format_url"

    const-string v1, "http://plus.google.com/people/%s/peopleInOwnerCircles/%s"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->v:Lcom/google/android/gms/common/a/d;

    .line 164
    const-string v0, "smart_profile.edit_google_plus_profile_intent_format_url"

    const-string v1, "https://plus.google.com/%s/about"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->w:Lcom/google/android/gms/common/a/d;

    .line 171
    const-string v0, "smart_profile.set_google_plus_cover_photo_intent_format_url"

    const-string v1, "https://plus.google.com/%s/about/op/coverphoto"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->x:Lcom/google/android/gms/common/a/d;

    .line 178
    const-string v0, "smart_profile.view_about_google_plus_profile_intent_format_url"

    const-string v1, "http://plus.google.com/%s/about"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->y:Lcom/google/android/gms/common/a/d;

    .line 185
    const-string v0, "smart_profile.view_all_google_plus_photos_intent_format_url"

    const-string v1, "https://plus.google.com/photos/%s/albums"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->z:Lcom/google/android/gms/common/a/d;

    .line 196
    const-string v0, "smart_profile.view_google_plus_photo_intent_format_url"

    const-string v1, "https://plus.google.com/photos/%s/photo/%s"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->A:Lcom/google/android/gms/common/a/d;

    .line 205
    const-string v0, "smart_profile.default_account_avatar_url"

    const-string v1, "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/dS9ff5TYSlA/c/photo.jpg"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->B:Lcom/google/android/gms/common/a/d;

    .line 216
    const-string v0, "smart_profile.view_google_plus_photo_in_album_intent_format_url"

    const-string v1, "https://plus.google.com/photos/%s/albums/%s/%s"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->C:Lcom/google/android/gms/common/a/d;

    .line 225
    const-string v0, "smart_profile.is_add_to_contacts_broken"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->D:Lcom/google/android/gms/common/a/d;

    .line 231
    const-string v0, "smart_profile.number_past_microseconds_to_get_email_interactions"

    const-wide v2, 0x8cd0e3a000L

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->E:Lcom/google/android/gms/common/a/d;

    .line 238
    const-string v0, "smart_profile.number_see_all_related_people_to_fetch"

    const v1, 0x7fffffff

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/a/a;->F:Lcom/google/android/gms/common/a/d;

    return-void
.end method
