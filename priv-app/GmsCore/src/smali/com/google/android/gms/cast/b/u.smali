.class public final Lcom/google/android/gms/cast/b/u;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/cast/b/u;

.field private static final p:J


# instance fields
.field private final b:Lcom/google/android/gms/cast/internal/k;

.field private final c:Ljava/util/LinkedList;

.field private final d:Ljava/util/LinkedList;

.field private e:Ljava/nio/channels/Selector;

.field private volatile f:Z

.field private volatile g:Z

.field private volatile h:Ljava/lang/Thread;

.field private final i:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private volatile j:Ljava/lang/Throwable;

.field private final k:Landroid/content/Context;

.field private final l:Landroid/content/Intent;

.field private m:Ljava/util/concurrent/CountDownLatch;

.field private final n:Landroid/os/PowerManager$WakeLock;

.field private o:Z

.field private q:J

.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 52
    sget-object v0, Lcom/google/android/gms/cast/a/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/cast/b/u;->p:J

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput v3, p0, Lcom/google/android/gms/cast/b/u;->r:I

    .line 69
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "CastSocketMultiplexer"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/b/u;->b:Lcom/google/android/gms/cast/internal/k;

    .line 70
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/b/u;->c:Ljava/util/LinkedList;

    .line 71
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/b/u;->d:Ljava/util/LinkedList;

    .line 72
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/cast/b/u;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 73
    iput-object p1, p0, Lcom/google/android/gms/cast/b/u;->k:Landroid/content/Context;

    .line 74
    const-string v0, "com.google.android.gms.cast.service.SOCKET_MUX"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/u;->l:Landroid/content/Intent;

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->k:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 77
    const/4 v1, 0x1

    const-string v2, "CastSocketMultiplexer"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/u;->n:Landroid/os/PowerManager$WakeLock;

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->n:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 79
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/cast/b/u;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/gms/cast/b/u;->a:Lcom/google/android/gms/cast/b/u;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Lcom/google/android/gms/cast/b/u;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/b/u;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/cast/b/u;->a:Lcom/google/android/gms/cast/b/u;

    .line 63
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    .line 65
    :cond_0
    sget-object v0, Lcom/google/android/gms/cast/b/u;->a:Lcom/google/android/gms/cast/b/u;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/cast/b/u;Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/android/gms/cast/b/u;->j:Ljava/lang/Throwable;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/cast/b/u;)Ljava/util/concurrent/CountDownLatch;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->m:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method private declared-synchronized a(J)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 388
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/gms/cast/b/u;->o:Z

    if-eqz v1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/gms/cast/b/u;->q:J

    sub-long/2addr v2, v4

    cmp-long v1, v2, p1

    if-ltz v1, :cond_0

    .line 390
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "releaseWakeLock"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 391
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->n:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 392
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/cast/b/u;->o:Z

    .line 393
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/cast/b/u;->q:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 394
    const/4 v0, 0x1

    .line 396
    :cond_0
    monitor-exit p0

    return v0

    .line 388
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lcom/google/android/gms/cast/b/u;)V
    .locals 11

    .prologue
    .line 33
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/u;->f:Z

    if-nez v0, :cond_1a

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v5, p0, Lcom/google/android/gms/cast/b/u;->d:Ljava/util/LinkedList;

    monitor-enter v5

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/b/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/o;->b()Ljava/nio/channels/SocketChannel;

    move-result-object v1

    iget-object v7, p0, Lcom/google/android/gms/cast/b/u;->e:Ljava/nio/channels/Selector;

    const/4 v8, 0x0

    invoke-virtual {v1, v7, v8}, Ljava/nio/channels/SocketChannel;->register(Ljava/nio/channels/Selector;I)Ljava/nio/channels/SelectionKey;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/nio/channels/SelectionKey;->attach(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/u;->k:Landroid/content/Context;

    iget-object v7, p0, Lcom/google/android/gms/cast/b/u;->l:Landroid/content/Intent;

    invoke-virtual {v1, v7}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/u;->c:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/o;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/gms/cast/b/u;->r:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/gms/cast/b/u;->r:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_2
    iget-object v7, p0, Lcom/google/android/gms/cast/b/u;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v8, "Error while connecting socket."

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v7, v1, v8, v9}, Lcom/google/android/gms/cast/internal/k;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/b/o;

    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Lcom/google/android/gms/cast/b/o;->a(I)V

    goto :goto_2

    :cond_3
    invoke-interface {v4}, Ljava/util/List;->clear()V

    :cond_4
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/cast/b/u;->c:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/b/o;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/o;->m()Ljava/nio/channels/SocketChannel;

    move-result-object v6

    if-eqz v6, :cond_5

    iget-object v7, p0, Lcom/google/android/gms/cast/b/u;->e:Ljava/nio/channels/Selector;

    invoke-virtual {v6, v7}, Ljava/nio/channels/SocketChannel;->keyFor(Ljava/nio/channels/Selector;)Ljava/nio/channels/SelectionKey;

    move-result-object v7

    if-nez v7, :cond_6

    :cond_5
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/o;->a()Z

    move-result v0

    if-eqz v0, :cond_1b

    iget v0, p0, Lcom/google/android/gms/cast/b/u;->r:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/cast/b/u;->r:I

    goto :goto_3

    :cond_6
    iget-object v7, p0, Lcom/google/android/gms/cast/b/u;->e:Ljava/nio/channels/Selector;

    invoke-virtual {v6, v7}, Ljava/nio/channels/SocketChannel;->keyFor(Ljava/nio/channels/Selector;)Ljava/nio/channels/SelectionKey;

    move-result-object v6

    invoke-virtual {v0, v6, v2, v3}, Lcom/google/android/gms/cast/b/o;->a(Ljava/nio/channels/SelectionKey;J)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_8

    iget-object v7, p0, Lcom/google/android/gms/cast/b/u;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v8, "*** removing socket %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    invoke-virtual {v7, v8, v9}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/o;->a()Z

    move-result v7

    if-eqz v7, :cond_7

    iget v7, p0, Lcom/google/android/gms/cast/b/u;->r:I

    add-int/lit8 v7, v7, -0x1

    iput v7, p0, Lcom/google/android/gms/cast/b/u;->r:I

    :cond_7
    invoke-virtual {v0, v6}, Lcom/google/android/gms/cast/b/o;->a(I)V

    goto :goto_3

    :cond_8
    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/o;->e()Z

    move-result v6

    if-nez v6, :cond_9

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/o;->f()Z

    move-result v0

    if-eqz v0, :cond_1b

    :cond_9
    const/4 v1, 0x1

    move v0, v1

    :goto_4
    move v1, v0

    goto :goto_3

    :cond_a
    iget v0, p0, Lcom/google/android/gms/cast/b/u;->r:I

    if-nez v0, :cond_b

    const-wide/16 v2, 0x0

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/cast/b/u;->a(J)Z

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_15

    const/4 v0, 0x1

    :goto_5
    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->k:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/cast/b/u;->l:Landroid/content/Intent;

    invoke-virtual {v0, v2}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    :cond_c
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->e:Ljava/nio/channels/Selector;

    if-eqz v1, :cond_16

    const-wide/16 v2, 0x3e8

    :goto_6
    invoke-virtual {v0, v2, v3}, Ljava/nio/channels/Selector;->select(J)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/gms/cast/b/u;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v3, "eventCount=%d, wakeLockHeld=%b, connectionActionsPending=%b, mSockets.size=%d, keys.size=%d"

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-boolean v7, p0, Lcom/google/android/gms/cast/b/u;->o:Z

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v1, 0x3

    iget-object v6, p0, Lcom/google/android/gms/cast/b/u;->c:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v1, 0x4

    iget-object v6, p0, Lcom/google/android/gms/cast/b/u;->e:Ljava/nio/channels/Selector;

    invoke-virtual {v6}, Ljava/nio/channels/Selector;->selectedKeys()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v2, v3, v5}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-nez v0, :cond_d

    sget-wide v0, Lcom/google/android/gms/cast/b/u;->p:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/cast/b/u;->a(J)Z
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    :cond_d
    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/u;->f:Z

    if-nez v0, :cond_1a

    iget v0, p0, Lcom/google/android/gms/cast/b/u;->r:I

    if-lez v0, :cond_e

    invoke-direct {p0}, Lcom/google/android/gms/cast/b/u;->d()V

    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->e:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->selectedKeys()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    :try_start_5
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/channels/SelectionKey;

    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->attachment()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/cast/b/o;

    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->isConnectable()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-virtual {v1}, Lcom/google/android/gms/cast/b/o;->j()I

    move-result v3

    const/4 v5, -0x1

    if-eq v3, v5, :cond_10

    iget-object v5, p0, Lcom/google/android/gms/cast/b/u;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v6, "*** removing socket %s (onConnectable)"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v5, p0, Lcom/google/android/gms/cast/b/u;->c:Ljava/util/LinkedList;

    invoke-virtual {v5, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lcom/google/android/gms/cast/b/o;->a()Z

    move-result v5

    if-eqz v5, :cond_f

    iget v5, p0, Lcom/google/android/gms/cast/b/u;->r:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/google/android/gms/cast/b/u;->r:I

    :cond_f
    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->cancel()V

    invoke-virtual {v1, v3}, Lcom/google/android/gms/cast/b/o;->a(I)V

    :cond_10
    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->isReadable()Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-virtual {v1}, Lcom/google/android/gms/cast/b/o;->k()I

    move-result v3

    const/4 v5, -0x1

    if-eq v3, v5, :cond_12

    iget-object v5, p0, Lcom/google/android/gms/cast/b/u;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v6, "*** removing socket %s (onReadable)"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v5, p0, Lcom/google/android/gms/cast/b/u;->c:Ljava/util/LinkedList;

    invoke-virtual {v5, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lcom/google/android/gms/cast/b/o;->a()Z

    move-result v5

    if-eqz v5, :cond_11

    iget v5, p0, Lcom/google/android/gms/cast/b/u;->r:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/google/android/gms/cast/b/u;->r:I

    :cond_11
    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->cancel()V

    invoke-virtual {v1, v3}, Lcom/google/android/gms/cast/b/o;->a(I)V

    :cond_12
    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->isWritable()Z

    move-result v3

    if-eqz v3, :cond_14

    invoke-virtual {v1}, Lcom/google/android/gms/cast/b/o;->l()I

    move-result v3

    const/4 v5, -0x1

    if-eq v3, v5, :cond_14

    iget-object v5, p0, Lcom/google/android/gms/cast/b/u;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v6, "*** removing socket %s (onWritable)"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v5, p0, Lcom/google/android/gms/cast/b/u;->c:Ljava/util/LinkedList;

    invoke-virtual {v5, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lcom/google/android/gms/cast/b/o;->a()Z

    move-result v5

    if-eqz v5, :cond_13

    iget v5, p0, Lcom/google/android/gms/cast/b/u;->r:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/google/android/gms/cast/b/u;->r:I

    :cond_13
    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->cancel()V

    invoke-virtual {v1, v3}, Lcom/google/android/gms/cast/b/o;->a(I)V
    :try_end_5
    .catch Ljava/nio/channels/CancelledKeyException; {:try_start_5 .. :try_end_5} :catch_2

    :cond_14
    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_7

    :cond_15
    const/4 v0, 0x0

    goto/16 :goto_5

    :cond_16
    :try_start_6
    iget-boolean v2, p0, Lcom/google/android/gms/cast/b/u;->o:Z

    if-eqz v2, :cond_17

    sget-wide v2, Lcom/google/android/gms/cast/b/u;->p:J
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_6

    :cond_17
    const-wide/16 v2, 0x0

    goto/16 :goto_6

    :cond_18
    iget v0, p0, Lcom/google/android/gms/cast/b/u;->r:I

    if-lez v0, :cond_19

    sget-wide v0, Lcom/google/android/gms/cast/b/u;->p:J

    :goto_9
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/cast/b/u;->a(J)Z

    goto/16 :goto_0

    :cond_19
    const-wide/16 v0, 0x0

    goto :goto_9

    :cond_1a
    return-void

    :catch_2
    move-exception v0

    goto :goto_8

    :cond_1b
    move v0, v1

    goto/16 :goto_4
.end method

.method static synthetic c(Lcom/google/android/gms/cast/b/u;)Lcom/google/android/gms/cast/internal/k;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->b:Lcom/google/android/gms/cast/internal/k;

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 355
    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/u;->g:Z

    if-eqz v0, :cond_1

    .line 356
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 357
    const-string v1, "selector thread aborted due to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 358
    iget-object v1, p0, Lcom/google/android/gms/cast/b/u;->j:Ljava/lang/Throwable;

    if-eqz v1, :cond_0

    .line 359
    iget-object v1, p0, Lcom/google/android/gms/cast/b/u;->j:Ljava/lang/Throwable;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 360
    iget-object v1, p0, Lcom/google/android/gms/cast/b/u;->j:Ljava/lang/Throwable;

    invoke-virtual {v1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    .line 361
    const-string v2, " at "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    aget-object v3, v1, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const/16 v3, 0x3a

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v2

    aget-object v1, v1, v4

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 366
    :goto_0
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 364
    :cond_0
    const-string v1, "unknown condition"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 369
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->h:Ljava/lang/Thread;

    if-nez v0, :cond_2

    .line 370
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "not started; call start()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 372
    :cond_2
    return-void
.end method

.method private declared-synchronized d()V
    .locals 3

    .prologue
    .line 375
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/u;->o:Z

    if-nez v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "acquireWakeLock"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 377
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->n:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 378
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/cast/b/u;->o:Z

    .line 380
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/cast/b/u;->q:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381
    monitor-exit p0

    return-void

    .line 375
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic d(Lcom/google/android/gms/cast/b/u;)Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/cast/b/u;->g:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/cast/b/u;)V
    .locals 3

    .prologue
    .line 33
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/cast/b/u;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "disposeWakeLock: Unbalanced call in releasing the wake lock."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/cast/b/u;)Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/u;->h:Ljava/lang/Thread;

    return-object v0
.end method


# virtual methods
.method final declared-synchronized a()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 86
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/cast/b/u;->h:Ljava/lang/Thread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 125
    :cond_0
    monitor-exit p0

    return-void

    .line 90
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/cast/b/u;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v2, "starting multiplexer"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/gms/cast/b/u;->m:Ljava/util/concurrent/CountDownLatch;

    .line 93
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/cast/b/u;->g:Z

    .line 94
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/cast/b/u;->f:Z

    .line 95
    invoke-static {}, Ljava/nio/channels/Selector;->open()Ljava/nio/channels/Selector;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/cast/b/u;->e:Ljava/nio/channels/Selector;

    .line 96
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/google/android/gms/cast/b/v;

    invoke-direct {v2, p0}, Lcom/google/android/gms/cast/b/v;-><init>(Lcom/google/android/gms/cast/b/u;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/google/android/gms/cast/b/u;->h:Ljava/lang/Thread;

    .line 113
    iget-object v1, p0, Lcom/google/android/gms/cast/b/u;->h:Ljava/lang/Thread;

    const-string v2, "CastSocketMultiplexer"

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 114
    iget-object v1, p0, Lcom/google/android/gms/cast/b/u;->h:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/cast/b/u;->m:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    .line 122
    :goto_0
    if-nez v0, :cond_0

    .line 123
    :try_start_3
    new-instance v0, Ljava/io/IOException;

    const-string v1, "timed out or interrupted waiting for muxer thread to start"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/cast/b/o;)V
    .locals 5

    .prologue
    .line 166
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/u;->c()V

    .line 168
    iget-object v1, p0, Lcom/google/android/gms/cast/b/u;->d:Ljava/util/LinkedList;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 169
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->k:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/cast/b/u;->l:Landroid/content/Intent;

    invoke-virtual {v0, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v2, "added socket %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 172
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 173
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->e:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 175
    monitor-exit p0

    return-void

    .line 172
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 166
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 3

    .prologue
    .line 181
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "wakeup"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 182
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/u;->c()V

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/cast/b/u;->e:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    monitor-exit p0

    return-void

    .line 181
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
