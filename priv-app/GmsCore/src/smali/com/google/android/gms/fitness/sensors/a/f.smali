.class final Lcom/google/android/gms/fitness/sensors/a/f;
.super Landroid/bluetooth/BluetoothGattCallback;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/fitness/sensors/a/e;


# direct methods
.method constructor <init>(Lcom/google/android/gms/fitness/sensors/a/e;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/a/f;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    invoke-direct {p0}, Landroid/bluetooth/BluetoothGattCallback;-><init>()V

    return-void
.end method

.method private static a(Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 147
    const-string v0, "Cannot find read future for characteristic %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 150
    return-void
.end method


# virtual methods
.method public final onCharacteristicChanged(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 4

    .prologue
    .line 166
    const-string v0, "received characteristic %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/f;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->d:Lcom/google/android/gms/fitness/sensors/a/m;

    invoke-interface {v0, p2}, Lcom/google/android/gms/fitness/sensors/a/m;->a(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    .line 168
    return-void
.end method

.method public final onCharacteristicRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/f;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->p:Lcom/google/android/gms/fitness/sensors/a/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/sensors/a/ae;->a()V

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/f;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/k/a/aq;

    .line 126
    if-nez p3, :cond_1

    .line 127
    const-string v1, "characteristic with uuid received: %s"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 128
    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {v0, p2}, Lcom/google/k/k/a/aq;->a(Ljava/lang/Object;)Z

    .line 133
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/f;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->d:Lcom/google/android/gms/fitness/sensors/a/m;

    invoke-interface {v0, p2}, Lcom/google/android/gms/fitness/sensors/a/m;->a(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    .line 144
    :goto_1
    return-void

    .line 131
    :cond_0
    invoke-static {p2}, Lcom/google/android/gms/fitness/sensors/a/f;->a(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    goto :goto_0

    .line 135
    :cond_1
    if-eqz v0, :cond_2

    .line 136
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed to read characteristic %s with status %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/k/k/a/aq;->a(Ljava/lang/Throwable;)Z

    .line 142
    :goto_2
    const-string v0, "onCharacteristicRead received: %s"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    .line 140
    :cond_2
    invoke-static {p2}, Lcom/google/android/gms/fitness/sensors/a/f;->a(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    goto :goto_2
.end method

.method public final onConnectionStateChange(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    const/4 v1, 0x0

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/f;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    if-eqz p2, :cond_0

    if-eq p3, v5, :cond_3

    :cond_0
    move v0, p3

    .line 67
    :goto_0
    if-eq p3, v0, :cond_4

    .line 68
    const-string v2, "onConnectionStateChange: status=0x%04X newState=%s correctedNewState=%s oldState=%s"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/android/gms/fitness/sensors/a/f;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget v5, v5, Lcom/google/android/gms/fitness/sensors/a/e;->o:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 75
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/f;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v2, v2, Lcom/google/android/gms/fitness/sensors/a/e;->n:Ljava/lang/Object;

    monitor-enter v2

    .line 76
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/a/f;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iput v0, v3, Lcom/google/android/gms/fitness/sensors/a/e;->o:I

    .line 77
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    packed-switch v0, :pswitch_data_0

    .line 107
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v0, v1

    .line 66
    goto :goto_0

    .line 71
    :cond_4
    if-eqz p2, :cond_1

    .line 72
    const-string v2, "onConnectionStateChange: status=0x%04X newState=%s"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 80
    :pswitch_0
    const-string v0, "Connecting to GATT server."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2

    .line 83
    :pswitch_1
    const-string v0, "Connected to GATT server."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/f;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v1, v0, Lcom/google/android/gms/fitness/sensors/a/e;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object v2, v0, Lcom/google/android/gms/fitness/sensors/a/e;->k:Lcom/google/k/k/a/aq;

    if-eqz v2, :cond_5

    iget-object v2, v0, Lcom/google/android/gms/fitness/sensors/a/e;->k:Lcom/google/k/k/a/aq;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/k/k/a/aq;->a(Ljava/lang/Object;)Z

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/gms/fitness/sensors/a/e;->k:Lcom/google/k/k/a/aq;

    const-string v0, "Connect future is resolved to success."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_3
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/f;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/sensors/a/e;->c()V

    goto :goto_2

    .line 84
    :cond_5
    :try_start_2
    const-string v0, "Connect future is not set."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_3

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 88
    :pswitch_2
    const-string v0, "Disconnecting from GATT server."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2

    .line 91
    :pswitch_3
    const-string v0, "Disconnected from GATT server."

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/f;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v2, v0, Lcom/google/android/gms/fitness/sensors/a/e;->h:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-object v3, v0, Lcom/google/android/gms/fitness/sensors/a/e;->i:Lcom/google/k/k/a/aq;

    if-eqz v3, :cond_7

    iget-object v3, v0, Lcom/google/android/gms/fitness/sensors/a/e;->i:Lcom/google/k/k/a/aq;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/k/k/a/aq;->a(Ljava/lang/Object;)Z

    const/4 v3, 0x0

    iput-object v3, v0, Lcom/google/android/gms/fitness/sensors/a/e;->i:Lcom/google/k/k/a/aq;

    const-string v0, "Disconnect future is resolved to success."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_4
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/f;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 96
    const-string v0, "Close was requested."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/f;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v1, v0, Lcom/google/android/gms/fitness/sensors/a/e;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_4
    iget-object v2, v0, Lcom/google/android/gms/fitness/sensors/a/e;->m:Landroid/bluetooth/BluetoothGatt;

    if-eqz v2, :cond_6

    iget-object v2, v0, Lcom/google/android/gms/fitness/sensors/a/e;->m:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothGatt;->close()V

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/gms/fitness/sensors/a/e;->m:Landroid/bluetooth/BluetoothGatt;

    :cond_6
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto/16 :goto_2

    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0

    .line 92
    :cond_7
    :try_start_5
    const-string v0, "Disconnect future is not set."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    goto :goto_4

    :catchall_3
    move-exception v0

    monitor-exit v2

    throw v0

    .line 102
    :cond_8
    const-string v0, "Trying to reconnect..."

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 103
    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/f;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v2, Lcom/google/android/gms/fitness/sensors/a/e;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/sensors/a/e;->b()Lcom/google/k/k/a/af;

    const-string v0, "Restoring characteristics.."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, v2, Lcom/google/android/gms/fitness/sensors/a/e;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/fitness/sensors/a/e;->a(Landroid/bluetooth/BluetoothGattCharacteristic;)Lcom/google/k/k/a/af;

    goto :goto_5

    .line 78
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final onDescriptorWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/f;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->p:Lcom/google/android/gms/fitness/sensors/a/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/sensors/a/ae;->a()V

    .line 156
    if-nez p3, :cond_0

    .line 157
    const-string v0, "descriptor written with uuid: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattDescriptor;->getUuid()Ljava/util/UUID;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 161
    :goto_0
    return-void

    .line 159
    :cond_0
    const-string v0, "descriptor received: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public final onServicesDiscovered(Landroid/bluetooth/BluetoothGatt;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/f;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->p:Lcom/google/android/gms/fitness/sensors/a/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/sensors/a/ae;->a()V

    .line 112
    if-nez p2, :cond_0

    .line 113
    const-string v0, "onServicesDiscovered received: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 114
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/f;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->e:Lcom/google/k/k/a/aq;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getServices()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/k/k/a/aq;->a(Ljava/lang/Object;)Z

    .line 118
    :goto_0
    return-void

    .line 116
    :cond_0
    const-string v0, "onServicesDiscovered received: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method
