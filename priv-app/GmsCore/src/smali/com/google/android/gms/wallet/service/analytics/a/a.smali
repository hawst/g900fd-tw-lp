.class public final Lcom/google/android/gms/wallet/service/analytics/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/SharedPreferences;

.field public final b:J

.field public final c:I

.field public final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/service/analytics/a/a;-><init>(Landroid/content/Context;B)V

    .line 41
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/analytics/a/a;->d:Landroid/content/Context;

    .line 46
    const-string v0, "com.google.android.gms.wallet.service.analytics.AnalyticsPersistentStore"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/a;->a:Landroid/content/SharedPreferences;

    .line 47
    const-wide/32 v0, 0x5265c00

    iput-wide v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/a;->b:J

    .line 48
    const/16 v0, 0x3c

    iput v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/a;->c:I

    .line 49
    return-void
.end method
