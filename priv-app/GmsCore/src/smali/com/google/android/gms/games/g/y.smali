.class public final Lcom/google/android/gms/games/g/y;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4518
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 4519
    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/gms/games/g/y;->a:[B

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/y;->cachedSize:I

    .line 4520
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 4561
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 4562
    iget-object v1, p0, Lcom/google/android/gms/games/g/y;->a:[B

    sget-object v2, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4563
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/g/y;->a:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 4566
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4530
    if-ne p1, p0, :cond_1

    .line 4540
    :cond_0
    :goto_0
    return v0

    .line 4533
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/y;

    if-nez v2, :cond_2

    move v0, v1

    .line 4534
    goto :goto_0

    .line 4536
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/y;

    .line 4537
    iget-object v2, p0, Lcom/google/android/gms/games/g/y;->a:[B

    iget-object v3, p1, Lcom/google/android/gms/games/g/y;->a:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 4538
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 4545
    iget-object v0, p0, Lcom/google/android/gms/games/g/y;->a:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 4547
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 4498
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/y;->a:[B

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 4553
    iget-object v0, p0, Lcom/google/android/gms/games/g/y;->a:[B

    sget-object v1, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4554
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/g/y;->a:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 4556
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4557
    return-void
.end method
