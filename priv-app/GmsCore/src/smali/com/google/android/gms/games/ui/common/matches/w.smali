.class public abstract Lcom/google/android/gms/games/ui/common/matches/w;
.super Lcom/google/android/gms/games/ui/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/common/matches/aa;
.implements Lcom/google/android/gms/games/ui/common/matches/y;


# static fields
.field private static final j:I


# instance fields
.field protected i:Ljava/lang/String;

.field private k:[Lcom/google/android/gms/games/multiplayer/Participant;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Landroid/net/Uri;

.field private o:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    sget v0, Lcom/google/android/gms/l;->bo:I

    sput v0, Lcom/google/android/gms/games/ui/common/matches/w;->j:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 46
    sget v0, Lcom/google/android/gms/games/ui/common/matches/w;->j:I

    invoke-direct {p0, p1, v1, v0, v1}, Lcom/google/android/gms/games/ui/q;-><init>(IIII)V

    .line 48
    return-void
.end method


# virtual methods
.method public final N()[Lcom/google/android/gms/games/multiplayer/Participant;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/w;->k:[Lcom/google/android/gms/games/multiplayer/Participant;

    return-object v0
.end method

.method public final O()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/w;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final P()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/w;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final Q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/w;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final R()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/w;->n:Landroid/net/Uri;

    return-object v0
.end method

.method public final S()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/w;->o:Landroid/net/Uri;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 51
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/w;->requestWindowFeature(I)Z

    .line 52
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/q;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    .line 55
    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {v0}, Landroid/support/v7/app/a;->e()V

    .line 59
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/w;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 60
    const-string v0, "com.google.android.gms.games.PARTICIPANTS"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    .line 61
    if-nez v2, :cond_2

    .line 62
    const-string v0, "ParticipListAct"

    const-string v1, "Required participants list is missing."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/w;->finish()V

    .line 92
    :cond_1
    return-void

    .line 67
    :cond_2
    const-string v0, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/w;->l:Ljava/lang/String;

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/w;->l:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 69
    const-string v0, "ParticipListAct"

    const-string v3, "Required current account name is missing."

    invoke-static {v0, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/w;->finish()V

    .line 73
    :cond_3
    const-string v0, "com.google.android.gms.games.GAME_ID"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/w;->m:Ljava/lang/String;

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/w;->m:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 75
    const-string v0, "ParticipListAct"

    const-string v3, "Required application id is missing."

    invoke-static {v0, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/w;->finish()V

    .line 79
    :cond_4
    const-string v0, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/w;->i:Ljava/lang/String;

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/w;->i:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 81
    const-string v0, "ParticipListAct"

    const-string v3, "Required current player id is missing."

    invoke-static {v0, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/w;->finish()V

    .line 85
    :cond_5
    const-string v0, "com.google.android.gms.games.FEATURED_URI"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/w;->n:Landroid/net/Uri;

    .line 86
    const-string v0, "com.google.android.gms.games.ICON_URI"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/w;->o:Landroid/net/Uri;

    .line 88
    array-length v0, v2

    new-array v0, v0, [Lcom/google/android/gms/games/multiplayer/Participant;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/w;->k:[Lcom/google/android/gms/games/multiplayer/Participant;

    .line 89
    const/4 v0, 0x0

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 90
    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/matches/w;->k:[Lcom/google/android/gms/games/multiplayer/Participant;

    aget-object v0, v2, v1

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    aput-object v0, v4, v1

    .line 89
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method protected final r()Lcom/google/android/gms/common/api/v;
    .locals 3

    .prologue
    .line 96
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0, p0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V

    .line 97
    new-instance v1, Lcom/google/android/gms/people/ad;

    invoke-direct {v1}, Lcom/google/android/gms/people/ad;-><init>()V

    .line 98
    const/16 v2, 0x76

    iput v2, v1, Lcom/google/android/gms/people/ad;->a:I

    .line 99
    sget-object v2, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v1}, Lcom/google/android/gms/people/ad;->a()Lcom/google/android/gms/people/ac;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    .line 100
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    return-object v0
.end method
