.class public final Lcom/google/android/gms/drive/g/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/n;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/gms/drive/g/p;->a:Landroid/content/Context;

    .line 22
    return-void
.end method

.method private static a(Landroid/net/NetworkInfo;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 73
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/net/NetworkInfo;)Z
    .locals 1

    .prologue
    .line 78
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Landroid/net/NetworkInfo;
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/drive/g/p;->a:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 69
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/p;->e()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/g/p;->b(Landroid/net/NetworkInfo;)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/p;->e()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/g/p;->a(Landroid/net/NetworkInfo;)Z

    move-result v0

    return v0
.end method

.method public final c()Lcom/google/android/gms/drive/g/o;
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/p;->e()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 55
    invoke-static {v0}, Lcom/google/android/gms/drive/g/p;->b(Landroid/net/NetworkInfo;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 56
    sget-object v0, Lcom/google/android/gms/drive/g/o;->a:Lcom/google/android/gms/drive/g/o;

    .line 58
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/drive/g/p;->a(Landroid/net/NetworkInfo;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/drive/g/o;->b:Lcom/google/android/gms/drive/g/o;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/gms/drive/g/o;->c:Lcom/google/android/gms/drive/g/o;

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/p;->e()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 63
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
