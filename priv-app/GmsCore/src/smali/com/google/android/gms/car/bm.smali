.class public final Lcom/google/android/gms/car/bm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field private final a:Lcom/google/android/gms/car/bc;

.field private final b:Lcom/google/android/gms/car/kp;

.field private final c:I

.field private d:I

.field private final e:Lcom/google/android/gms/car/bn;

.field private volatile f:Z

.field private final g:Landroid/os/Handler;


# direct methods
.method private declared-synchronized a(I)V
    .locals 1

    .prologue
    .line 369
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/android/gms/car/bm;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 370
    monitor-exit p0

    return-void

    .line 369
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/bm;)V
    .locals 3

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/car/bm;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/bm;->g:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/bm;I)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 28
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    invoke-direct {p0, v3}, Lcom/google/android/gms/car/bm;->a(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/car/bm;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/bm;->g:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v3, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/bm;->a(I)V

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 129
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/bm;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 140
    :goto_0
    monitor-exit p0

    return-void

    .line 132
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/bm;->a:Lcom/google/android/gms/car/bc;

    iget v1, p0, Lcom/google/android/gms/car/bm;->c:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/bc;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 134
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/car/bm;->b:Lcom/google/android/gms/car/kp;

    iget-object v1, p0, Lcom/google/android/gms/car/bm;->e:Lcom/google/android/gms/car/bn;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/kp;->a(Lcom/google/android/gms/car/ks;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 138
    :goto_1
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/car/bm;->b:Lcom/google/android/gms/car/kp;

    invoke-interface {v0}, Lcom/google/android/gms/car/kp;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 139
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/bm;->f:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final binderDied()V
    .locals 2

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/gms/car/bm;->b:Lcom/google/android/gms/car/kp;

    invoke-interface {v0}, Lcom/google/android/gms/car/kp;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 360
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/bm;->f:Z

    .line 361
    iget-object v0, p0, Lcom/google/android/gms/car/bm;->a:Lcom/google/android/gms/car/bc;

    iget v1, p0, Lcom/google/android/gms/car/bm;->c:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/bc;->a(I)V

    .line 362
    return-void
.end method
