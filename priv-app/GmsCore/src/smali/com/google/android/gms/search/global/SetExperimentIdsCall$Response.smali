.class public Lcom/google/android/gms/search/global/SetExperimentIdsCall$Response;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/ap;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/global/r;


# instance fields
.field public a:Lcom/google/android/gms/common/api/Status;

.field final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 114
    new-instance v0, Lcom/google/android/gms/search/global/r;

    invoke-direct {v0}, Lcom/google/android/gms/search/global/r;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Response;->CREATOR:Lcom/google/android/gms/search/global/r;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Response;->b:I

    .line 103
    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/common/api/Status;)V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    iput p1, p0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Response;->b:I

    .line 124
    iput-object p2, p0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    .line 125
    return-void
.end method


# virtual methods
.method public final C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Response;->CREATOR:Lcom/google/android/gms/search/global/r;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 137
    sget-object v0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Response;->CREATOR:Lcom/google/android/gms/search/global/r;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/search/global/r;->a(Lcom/google/android/gms/search/global/SetExperimentIdsCall$Response;Landroid/os/Parcel;I)V

    .line 138
    return-void
.end method
