.class public Lcom/google/android/gms/auth/setup/d2d/SourceActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;


# instance fields
.field private b:Lcom/google/android/gms/auth/authzen/keyservice/g;

.field private c:[Landroid/accounts/Account;

.field private d:Landroid/bluetooth/BluetoothAdapter;

.field private e:Z

.field private f:Z

.field private g:Z

.field private volatile h:Z

.field private final i:Ljava/lang/Object;

.field private j:Z

.field private k:Landroid/app/AlertDialog;

.field private l:Lcom/google/android/gms/auth/setup/d2d/a/f;

.field private m:Lcom/google/android/gms/auth/setup/d2d/a/b;

.field private n:Lcom/google/android/gms/auth/setup/d2d/k;

.field private o:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 112
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "D2D"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "SourceActivity"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 110
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 129
    iput-boolean v1, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->e:Z

    .line 130
    iput-boolean v1, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->f:Z

    .line 131
    iput-boolean v1, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->g:Z

    .line 132
    iput-boolean v1, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->h:Z

    .line 133
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->i:Ljava/lang/Object;

    .line 134
    iput-boolean v1, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->j:Z

    .line 880
    return-void
.end method

.method public static a(Landroid/content/Context;[B)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 169
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "payload"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a()Lcom/google/android/gms/auth/d/a;
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;Lcom/google/android/gms/auth/setup/d2d/a/b;)Lcom/google/android/gms/auth/setup/d2d/a/b;
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->m:Lcom/google/android/gms/auth/setup/d2d/a/b;

    return-object p1
.end method

.method private a(Lcom/google/android/gms/auth/setup/d2d/b/e;Lcom/google/android/gms/auth/be/b/e;Lcom/google/android/gms/auth/be/b/b;)Lcom/google/android/gms/auth/setup/d2d/b/a;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 655
    iget v0, p1, Lcom/google/android/gms/auth/setup/d2d/b/e;->a:I

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 699
    :goto_0
    return-object v0

    .line 659
    :cond_0
    iget-object v2, p1, Lcom/google/android/gms/auth/setup/d2d/b/e;->b:Ljava/lang/String;

    .line 663
    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/auth/setup/d2d/b/e;->d:[B

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a([B)[B

    move-result-object v3

    .line 664
    const-string v0, "SHA-256"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v4

    .line 666
    invoke-interface {p2, v2}, Lcom/google/android/gms/auth/be/b/e;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/be/b/d;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could not get secret for account: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    move-object v0, v1

    .line 667
    :cond_1
    :goto_1
    if-nez v0, :cond_3

    move-object v0, v1

    .line 668
    goto :goto_0

    .line 666
    :cond_2
    invoke-interface {p3, v0}, Lcom/google/android/gms/auth/be/b/b;->a(Lcom/google/android/gms/auth/be/b/d;)Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could not get counter for account: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    .line 671
    :cond_3
    new-instance v5, Lcom/google/ab/b/a/e/l;

    invoke-direct {v5}, Lcom/google/ab/b/a/e/l;-><init>()V

    invoke-static {v4}, Lcom/google/protobuf/a/a;->a([B)Lcom/google/protobuf/a/a;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/google/ab/b/a/e/l;->a(Lcom/google/protobuf/a/a;)Lcom/google/ab/b/a/e/l;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/google/ab/b/a/e/l;->a(J)Lcom/google/ab/b/a/e/l;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/google/ab/b/a/e/l;->a(I)Lcom/google/ab/b/a/e/l;

    move-result-object v0

    .line 677
    new-instance v4, Lcom/google/ab/b/a/e/t;

    sget-object v5, Lcom/google/ab/b/a/e/u;->j:Lcom/google/ab/b/a/e/u;

    invoke-virtual {v0}, Lcom/google/ab/b/a/e/l;->g()[B

    move-result-object v0

    invoke-direct {v4, v5, v0}, Lcom/google/ab/b/a/e/t;-><init>(Lcom/google/ab/b/a/e/u;[B)V

    .line 681
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->b:Lcom/google/android/gms/auth/authzen/keyservice/g;

    const-string v5, "device_key"

    invoke-virtual {v0, v5}, Lcom/google/android/gms/auth/authzen/keyservice/g;->a(Ljava/lang/String;)Ljava/security/KeyPair;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->b:Lcom/google/android/gms/auth/authzen/keyservice/g;

    invoke-virtual {v5, v2}, Lcom/google/android/gms/auth/authzen/keyservice/g;->c(Ljava/lang/String;)Lcom/google/android/gms/auth/authzen/keyservice/b;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/auth/authzen/keyservice/b;->a:Lcom/google/android/gms/auth/authzen/keyservice/c;

    iget-object v2, v2, Lcom/google/android/gms/auth/authzen/keyservice/c;->b:Ljavax/crypto/SecretKey;

    invoke-static {v4, v0, v2}, Lcom/google/ab/b/a/e/s;->a(Lcom/google/ab/b/a/e/t;Ljava/security/KeyPair;Ljavax/crypto/SecretKey;)[B
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/gms/auth/authzen/keyservice/h; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v1

    .line 693
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/b/a;

    invoke-direct {v0}, Lcom/google/android/gms/auth/setup/d2d/b/a;-><init>()V

    .line 694
    iget-object v2, p1, Lcom/google/android/gms/auth/setup/d2d/b/e;->b:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/gms/auth/setup/d2d/b/a;->a:Ljava/lang/String;

    .line 695
    iget-object v2, p1, Lcom/google/android/gms/auth/setup/d2d/b/e;->d:[B

    iput-object v2, v0, Lcom/google/android/gms/auth/setup/d2d/b/a;->e:[B

    .line 696
    iget-object v2, p1, Lcom/google/android/gms/auth/setup/d2d/b/e;->e:[B

    iput-object v2, v0, Lcom/google/android/gms/auth/setup/d2d/b/a;->d:[B

    .line 697
    iput-object v3, v0, Lcom/google/android/gms/auth/setup/d2d/b/a;->b:[B

    .line 698
    iput-object v1, v0, Lcom/google/android/gms/auth/setup/d2d/b/a;->c:[B

    goto/16 :goto_0

    .line 684
    :catch_0
    move-exception v0

    .line 689
    :goto_2
    sget-object v2, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    const-string v3, "Unexpected error."

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 690
    goto/16 :goto_0

    .line 684
    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_2

    :catch_4
    move-exception v0

    goto :goto_2
.end method

.method private static a(Landroid/content/Intent;)Lcom/google/android/gms/auth/setup/d2d/b/d;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 316
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/b/d;

    invoke-direct {v0}, Lcom/google/android/gms/auth/setup/d2d/b/d;-><init>()V

    .line 317
    const-string v2, "payload"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v2

    .line 318
    if-eqz v2, :cond_1

    .line 320
    :try_start_0
    invoke-static {v0, v2}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    .line 339
    :cond_0
    :goto_0
    return-object v0

    .line 322
    :catch_0
    move-exception v0

    .line 323
    sget-object v2, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    const-string v3, "Could not deserialize BootstrapInfo!"

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 324
    goto :goto_0

    .line 327
    :cond_1
    const-string v2, "btMacAddress"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/auth/setup/d2d/b/d;->b:Ljava/lang/String;

    .line 328
    const-string v2, "initiatorHello"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v2

    .line 329
    if-eqz v2, :cond_2

    .line 330
    iput-object v2, v0, Lcom/google/android/gms/auth/setup/d2d/b/d;->c:[B

    .line 334
    :cond_2
    iget-object v2, v0, Lcom/google/android/gms/auth/setup/d2d/b/d;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 335
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Bluetooth MAC address information missing."

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    move-object v0, v1

    .line 336
    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;Lcom/google/android/gms/auth/setup/d2d/k;)Lcom/google/android/gms/auth/setup/d2d/k;
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->n:Lcom/google/android/gms/auth/setup/d2d/k;

    return-object p1
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 819
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->c()V

    .line 820
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/t;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/auth/setup/d2d/t;-><init>(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 826
    return-void
.end method

.method private a(Landroid/app/AlertDialog;)V
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->k:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->k:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->k:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 286
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->k:Landroid/app/AlertDialog;

    .line 287
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->k:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 288
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->k:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 289
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->c()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;Lcom/google/android/gms/auth/setup/d2d/b/c;)V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 110
    new-instance v3, Lcom/google/android/gms/auth/be/b/f;

    invoke-direct {v3, p0}, Lcom/google/android/gms/auth/be/b/f;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/google/android/gms/auth/be/b/a;

    invoke-direct {v4, p0}, Lcom/google/android/gms/auth/be/b/a;-><init>(Landroid/content/Context;)V

    iget-object v0, p1, Lcom/google/android/gms/auth/setup/d2d/b/c;->a:Lcom/google/android/gms/auth/setup/d2d/b/n;

    iget-object v5, v0, Lcom/google/android/gms/auth/setup/d2d/b/n;->a:[Lcom/google/android/gms/auth/setup/d2d/b/e;

    new-instance v6, Ljava/util/ArrayList;

    array-length v0, v5

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    :goto_0
    array-length v7, v5

    if-ge v0, v7, :cond_1

    aget-object v7, v5, v0

    invoke-direct {p0, v7, v4, v3}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a(Lcom/google/android/gms/auth/setup/d2d/b/e;Lcom/google/android/gms/auth/be/b/e;Lcom/google/android/gms/auth/be/b/b;)Lcom/google/android/gms/auth/setup/d2d/b/a;

    move-result-object v7

    if-nez v7, :cond_0

    sget-object v7, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Could not create assertion for account: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v9, v5, v0

    iget-object v9, v9, Lcom/google/android/gms/auth/setup/d2d/b/e;->b:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "No assertion infos returned!"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    sget v0, Lcom/google/android/gms/p;->aW:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a(I)V

    :goto_2
    return-void

    :cond_2
    new-instance v3, Lcom/google/android/gms/auth/setup/d2d/b/i;

    invoke-direct {v3}, Lcom/google/android/gms/auth/setup/d2d/b/i;-><init>()V

    sget-object v0, Lcom/google/android/gms/auth/b/a;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/gms/auth/setup/d2d/b/i;->f:Ljava/lang/String;

    iput v10, v3, Lcom/google/android/gms/auth/setup/d2d/b/i;->e:I

    iput v2, v3, Lcom/google/android/gms/auth/setup/d2d/b/i;->b:I

    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/b/o;

    invoke-direct {v0}, Lcom/google/android/gms/auth/setup/d2d/b/o;-><init>()V

    iput-object v0, v3, Lcom/google/android/gms/auth/setup/d2d/b/i;->c:Lcom/google/android/gms/auth/setup/d2d/b/o;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/gms/auth/setup/d2d/b/a;

    invoke-interface {v6, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/auth/setup/d2d/b/a;

    iput-object v0, v3, Lcom/google/android/gms/auth/setup/d2d/b/i;->a:[Lcom/google/android/gms/auth/setup/d2d/b/a;

    new-instance v4, Lcom/google/android/gms/auth/setup/d2d/b/k;

    invoke-direct {v4}, Lcom/google/android/gms/auth/setup/d2d/b/k;-><init>()V

    iput-object v3, v4, Lcom/google/android/gms/auth/setup/d2d/b/k;->a:Lcom/google/android/gms/auth/setup/d2d/b/i;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "backup_enabled"

    invoke-static {v0, v3, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_3

    move v1, v2

    :cond_3
    if-eqz v1, :cond_5

    new-instance v0, Lcom/google/android/gms/backup/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/backup/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/backup/b;->a()Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    :goto_3
    if-eqz v0, :cond_6

    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Backup account found."

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v1, v10}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Backup enabled with account: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    :cond_4
    iput-object v0, v4, Lcom/google/android/gms/auth/setup/d2d/b/k;->b:Ljava/lang/String;

    :goto_4
    invoke-static {}, Lcom/google/android/gms/common/util/e;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/auth/setup/d2d/b/k;->c:Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/b/g;

    invoke-direct {v0}, Lcom/google/android/gms/auth/setup/d2d/b/g;-><init>()V

    iput-object v4, v0, Lcom/google/android/gms/auth/setup/d2d/b/g;->f:Lcom/google/android/gms/auth/setup/d2d/b/k;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->n:Lcom/google/android/gms/auth/setup/d2d/k;

    invoke-interface {v1, v0}, Lcom/google/android/gms/auth/setup/d2d/k;->a(Lcom/google/android/gms/auth/setup/d2d/b/g;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_2

    :cond_5
    const/4 v0, 0x0

    goto :goto_3

    :cond_6
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Backup account not found."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    goto :goto_4
.end method

.method static synthetic a(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;Lcom/google/android/gms/auth/setup/d2d/b/d;Ljava/util/concurrent/Future;)V
    .locals 3

    .prologue
    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->e:Z

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->d:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/p;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/auth/setup/d2d/p;-><init>(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;Lcom/google/android/gms/auth/setup/d2d/b/d;Ljava/util/concurrent/Future;)V

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Could not enable Bluetooth."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->finish()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;Ljava/lang/CharSequence;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 110
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    const/4 v1, -0x2

    const/high16 v2, 0x1040000

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/auth/setup/d2d/l;

    invoke-direct {v3, p0}, Lcom/google/android/gms/auth/setup/d2d/l;-><init>(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    new-instance v1, Lcom/google/android/gms/auth/setup/d2d/m;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/setup/d2d/m;-><init>(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a(Landroid/app/AlertDialog;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 110
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/s;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/auth/setup/d2d/s;-><init>(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;[Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->n:Lcom/google/android/gms/auth/setup/d2d/k;

    invoke-interface {v0}, Lcom/google/android/gms/auth/setup/d2d/k;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->finish()V

    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;[Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)V
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0xa

    invoke-static {v1}, Lcom/google/k/a/x;->a(C)Lcom/google/k/a/x;

    move-result-object v1

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/k/a/x;->a(Ljava/lang/StringBuilder;Ljava/lang/Iterable;)Ljava/lang/StringBuilder;

    invoke-static {v5, v4}, Ljava/text/DateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->aG:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->aF:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/CharSequence;

    aput-object p2, v4, v5

    aput-object v1, v4, v6

    const/4 v1, 0x2

    aput-object v0, v4, v1

    invoke-static {v3, v4}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/common/notification/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/notification/b;-><init>(Landroid/content/Context;)V

    const-wide/32 v4, 0x1b7740

    invoke-virtual {v1, v4, v5}, Lcom/google/android/gms/common/notification/b;->b(J)Lcom/google/android/gms/common/notification/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/notification/b;->a(Ljava/lang/CharSequence;)Lcom/google/android/gms/common/notification/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/notification/b;->b(Ljava/lang/CharSequence;)Lcom/google/android/gms/common/notification/b;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/notification/b;->c(Ljava/lang/CharSequence;)Lcom/google/android/gms/common/notification/b;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/auth/b/a;->R:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/notification/b;->a(Landroid/net/Uri;)Lcom/google/android/gms/common/notification/b;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/android/gms/common/notification/b;->a(Z)Lcom/google/android/gms/common/notification/b;

    move-result-object v0

    const v1, 0x108008a

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/notification/b;->a(I)Lcom/google/android/gms/common/notification/b;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/notification/b;->d(Ljava/lang/CharSequence;)Lcom/google/android/gms/common/notification/b;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/common/notification/a;

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    invoke-direct {v3, v0, v1}, Lcom/google/android/gms/common/notification/a;-><init>(Landroid/app/NotificationManager;Landroid/app/AlarmManager;)V

    const-string v0, "d2d"

    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    move-result v1

    invoke-virtual {v3, v0, v1, v2}, Lcom/google/android/gms/common/notification/a;->a(Ljava/lang/String;ILcom/google/android/gms/common/notification/b;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/auth/setup/d2d/b/d;Ljava/util/concurrent/Future;)V
    .locals 7

    .prologue
    .line 412
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/q;

    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->d:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p1, Lcom/google/android/gms/auth/setup/d2d/b/d;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/gms/auth/setup/d2d/b/d;->b:Ljava/lang/String;

    move-object v1, p0

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/auth/setup/d2d/q;-><init>(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;Landroid/bluetooth/BluetoothAdapter;Ljava/util/UUID;Ljava/lang/String;Lcom/google/android/gms/auth/setup/d2d/b/d;Ljava/util/concurrent/Future;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->l:Lcom/google/android/gms/auth/setup/d2d/a/f;

    .line 460
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->l:Lcom/google/android/gms/auth/setup/d2d/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/setup/d2d/a/f;->start()V

    .line 461
    return-void
.end method

.method private a([B)[B
    .locals 10

    .prologue
    const/4 v0, 0x1

    const-wide/16 v2, -0x1

    .line 723
    new-instance v4, Lcom/google/android/gms/auth/setup/d2d/b/s;

    invoke-direct {v4}, Lcom/google/android/gms/auth/setup/d2d/b/s;-><init>()V

    .line 724
    iget-boolean v1, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->f:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    :cond_0
    iput v0, v4, Lcom/google/android/gms/auth/setup/d2d/b/s;->c:I

    .line 726
    iget-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->f:Z

    iput-boolean v0, v4, Lcom/google/android/gms/auth/setup/d2d/b/s;->a:Z

    .line 727
    iget-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->g:Z

    iput-boolean v0, v4, Lcom/google/android/gms/auth/setup/d2d/b/s;->b:Z

    .line 728
    iget-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->f:Z

    if-nez v0, :cond_2

    .line 729
    iput-wide v2, v4, Lcom/google/android/gms/auth/setup/d2d/b/s;->d:J

    .line 730
    iput-wide v2, v4, Lcom/google/android/gms/auth/setup/d2d/b/s;->e:J

    .line 754
    :goto_0
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 755
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "lastUnlockDurationInS: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, v4, Lcom/google/android/gms/auth/setup/d2d/b/s;->d:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 757
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "lockScreenSetupDurationInS: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, v4, Lcom/google/android/gms/auth/setup/d2d/b/s;->e:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 761
    :cond_1
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/b/h;

    invoke-direct {v0}, Lcom/google/android/gms/auth/setup/d2d/b/h;-><init>()V

    .line 762
    invoke-static {}, Lcom/google/android/gms/common/util/e;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/auth/setup/d2d/b/h;->a:Ljava/lang/String;

    .line 763
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/auth/setup/d2d/b/h;->c:Ljava/lang/String;

    .line 764
    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/auth/setup/d2d/b/h;->b:Ljava/lang/String;

    .line 765
    const-string v1, "6777000"

    iput-object v1, v0, Lcom/google/android/gms/auth/setup/d2d/b/h;->e:Ljava/lang/String;

    .line 767
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/auth/setup/d2d/b/h;->d:Ljava/lang/String;

    .line 768
    iput-object v4, v0, Lcom/google/android/gms/auth/setup/d2d/b/h;->g:Lcom/google/android/gms/auth/setup/d2d/b/s;

    .line 770
    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    .line 772
    new-instance v1, Lorg/json/JSONStringer;

    invoke-direct {v1}, Lorg/json/JSONStringer;-><init>()V

    invoke-virtual {v1}, Lorg/json/JSONStringer;->object()Lorg/json/JSONStringer;

    move-result-object v1

    const-string v2, "typ"

    invoke-virtual {v1, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v1

    const-string v2, "navigator.id.getAssertion"

    invoke-virtual {v1, v2}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    move-result-object v1

    const-string v2, "challenge"

    invoke-virtual {v1, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/gms/common/util/m;->c([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    move-result-object v1

    const-string v2, "source_device_signals"

    invoke-virtual {v1, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/common/util/m;->c([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONStringer;->endObject()Lorg/json/JSONStringer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONStringer;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/k/a/t;->c:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    return-object v0

    .line 732
    :cond_2
    new-instance v5, Lcom/google/android/gms/common/b;

    invoke-direct {v5}, Lcom/google/android/gms/common/b;-><init>()V

    .line 734
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/devicesignals/DeviceSignalsService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v5, v1}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 738
    invoke-virtual {v5}, Lcom/google/android/gms/common/b;->a()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/firstparty/a/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/auth/firstparty/a/a;

    move-result-object v0

    .line 740
    invoke-interface {v0}, Lcom/google/android/gms/auth/firstparty/a/a;->a()J

    move-result-wide v6

    .line 741
    invoke-interface {v0}, Lcom/google/android/gms/auth/firstparty/a/a;->b()J

    move-result-wide v8

    .line 742
    cmp-long v0, v6, v2

    if-nez v0, :cond_3

    move-wide v0, v2

    :goto_1
    iput-wide v0, v4, Lcom/google/android/gms/auth/setup/d2d/b/s;->d:J

    .line 744
    cmp-long v0, v8, v2

    if-nez v0, :cond_4

    move-wide v0, v2

    :goto_2
    iput-wide v0, v4, Lcom/google/android/gms/auth/setup/d2d/b/s;->e:J
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 751
    invoke-virtual {p0, v5}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->unbindService(Landroid/content/ServiceConnection;)V

    goto/16 :goto_0

    .line 742
    :cond_3
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, v6

    const-wide/16 v6, 0x3e8

    div-long/2addr v0, v6

    goto :goto_1

    .line 744
    :cond_4
    const-wide/16 v0, 0x3e8

    div-long v0, v8, v0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 746
    :catch_0
    move-exception v0

    .line 747
    :goto_3
    :try_start_2
    const-string v1, "Could not get device signals. Setting to insecure."

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 748
    const-wide/16 v0, -0x1

    iput-wide v0, v4, Lcom/google/android/gms/auth/setup/d2d/b/s;->d:J

    .line 749
    const-wide/16 v0, -0x1

    iput-wide v0, v4, Lcom/google/android/gms/auth/setup/d2d/b/s;->e:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 751
    invoke-virtual {p0, v5}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->unbindService(Landroid/content/ServiceConnection;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0, v5}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->unbindService(Landroid/content/ServiceConnection;)V

    throw v0

    .line 746
    :catch_1
    move-exception v0

    goto :goto_3
.end method

.method static synthetic b(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->d:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    .line 531
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/google/android/gms/auth/setup/d2d/r;

    invoke-direct {v2, p0}, Lcom/google/android/gms/auth/setup/d2d/r;-><init>(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;)V

    sget-object v0, Lcom/google/android/gms/auth/b/a;->T:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 543
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;Lcom/google/android/gms/auth/setup/d2d/b/d;Ljava/util/concurrent/Future;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a(Lcom/google/android/gms/auth/setup/d2d/b/d;Ljava/util/concurrent/Future;)V

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 635
    iput-boolean v1, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->h:Z

    .line 636
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->n:Lcom/google/android/gms/auth/setup/d2d/k;

    if-eqz v0, :cond_0

    .line 637
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/b/g;

    invoke-direct {v0}, Lcom/google/android/gms/auth/setup/d2d/b/g;-><init>()V

    .line 638
    iput-boolean v1, v0, Lcom/google/android/gms/auth/setup/d2d/b/g;->c:Z

    .line 640
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->n:Lcom/google/android/gms/auth/setup/d2d/k;

    invoke-interface {v1, v0}, Lcom/google/android/gms/auth/setup/d2d/k;->a(Lcom/google/android/gms/auth/setup/d2d/b/g;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 645
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->finish()V

    .line 646
    return-void

    .line 641
    :catch_0
    move-exception v0

    .line 642
    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;)Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->e:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;)Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->g:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;)Lcom/google/android/gms/auth/setup/d2d/a/b;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->m:Lcom/google/android/gms/auth/setup/d2d/a/b;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 110
    iget-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->f:Z

    if-eqz v0, :cond_4

    iget-object v4, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->i:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/b/g;

    invoke-direct {v0}, Lcom/google/android/gms/auth/setup/d2d/b/g;-><init>()V

    sget v3, Lcom/google/android/gms/p;->ba:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/gms/auth/setup/d2d/b/g;->b:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v3, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->n:Lcom/google/android/gms/auth/setup/d2d/k;

    invoke-interface {v3, v0}, Lcom/google/android/gms/auth/setup/d2d/k;->a(Lcom/google/android/gms/auth/setup/d2d/b/g;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    const/16 v0, 0x15

    :try_start_2
    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "keyguard"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    if-eqz v0, :cond_3

    sget v3, Lcom/google/android/gms/p;->aK:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v5}, Landroid/app/KeyguardManager;->createConfirmDeviceCredentialIntent(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->j:Z

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->startActivityForResult(Landroid/content/Intent;I)V

    move v3, v2

    :goto_1
    if-nez v3, :cond_2

    const-string v0, "device_policy"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    if-eqz v0, :cond_2

    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    const v5, 0x3000001a

    const-string v6, "d2d"

    invoke-virtual {v1, v5, v6}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->o:Landroid/os/PowerManager$WakeLock;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v1, 0x1

    :try_start_3
    iput-boolean v1, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->j:Z

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->lockNow()V
    :try_end_3
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v2

    :goto_2
    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-nez v0, :cond_0

    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Unable to bring up the lockscreen"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    :cond_0
    :goto_3
    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->b()V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    :try_start_5
    sget-object v3, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :catch_1
    move-exception v0

    const/4 v0, 0x0

    :try_start_6
    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->j:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->o:Landroid/os/PowerManager$WakeLock;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_2
    move v0, v3

    goto :goto_2

    :cond_3
    move v3, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_3
.end method

.method static synthetic g(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;)Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->h:Z

    return v0
.end method

.method static synthetic h(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->k:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;)V
    .locals 6

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->c:[Landroid/accounts/Account;

    array-length v0, v0

    new-array v1, v0, [Lcom/google/android/gms/auth/setup/d2d/b/r;

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->c:[Landroid/accounts/Account;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->c:[Landroid/accounts/Account;

    aget-object v2, v2, v0

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    new-instance v3, Lcom/google/android/gms/auth/setup/d2d/b/r;

    invoke-direct {v3}, Lcom/google/android/gms/auth/setup/d2d/b/r;-><init>()V

    iput-object v2, v3, Lcom/google/android/gms/auth/setup/d2d/b/r;->a:Ljava/lang/String;

    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->b:Lcom/google/android/gms/auth/authzen/keyservice/g;

    const-string v5, "device_key"

    invoke-virtual {v4, v5}, Lcom/google/android/gms/auth/authzen/keyservice/g;->a(Ljava/lang/String;)Ljava/security/KeyPair;

    move-result-object v4

    invoke-virtual {v4}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v4

    invoke-static {v4}, Lcom/google/ab/b/a/e/j;->a(Ljava/security/PublicKey;)[B

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gms/auth/setup/d2d/b/r;->b:[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/authzen/keyservice/h; {:try_start_0 .. :try_end_0} :catch_2

    aput-object v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_1
    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not get public key of user: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    sget v0, Lcom/google/android/gms/p;->aW:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a(I)V

    :goto_2
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/b/m;

    invoke-direct {v0}, Lcom/google/android/gms/auth/setup/d2d/b/m;-><init>()V

    const/4 v2, 0x1

    iput v2, v0, Lcom/google/android/gms/auth/setup/d2d/b/m;->b:I

    iput-object v1, v0, Lcom/google/android/gms/auth/setup/d2d/b/m;->a:[Lcom/google/android/gms/auth/setup/d2d/b/r;

    new-instance v1, Lcom/google/android/gms/auth/setup/d2d/b/b;

    invoke-direct {v1}, Lcom/google/android/gms/auth/setup/d2d/b/b;-><init>()V

    iput-object v0, v1, Lcom/google/android/gms/auth/setup/d2d/b/b;->a:Lcom/google/android/gms/auth/setup/d2d/b/m;

    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/b/g;

    invoke-direct {v0}, Lcom/google/android/gms/auth/setup/d2d/b/g;-><init>()V

    iput-object v1, v0, Lcom/google/android/gms/auth/setup/d2d/b/g;->d:Lcom/google/android/gms/auth/setup/d2d/b/b;

    sget v1, Lcom/google/android/gms/p;->bb:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/auth/setup/d2d/b/g;->b:Ljava/lang/String;

    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->n:Lcom/google/android/gms/auth/setup/d2d/k;

    invoke-interface {v1, v0}, Lcom/google/android/gms/auth/setup/d2d/k;->a(Lcom/google/android/gms/auth/setup/d2d/b/g;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/Throwable;)V

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method static synthetic j(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;)Lcom/google/android/gms/auth/setup/d2d/a/f;
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->l:Lcom/google/android/gms/auth/setup/d2d/a/f;

    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 268
    if-ne p1, v1, :cond_0

    .line 269
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 270
    iput-boolean v1, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->g:Z

    .line 271
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->b()V

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 273
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->c()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 176
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 178
    sget-object v0, Lcom/google/android/gms/auth/b/a;->Q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 179
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->finish()V

    .line 228
    :goto_0
    return-void

    .line 184
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185
    sget v0, Lcom/google/android/gms/p;->aJ:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a(I)V

    .line 186
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Cannot clone restricted profile"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 190
    :cond_1
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->c:[Landroid/accounts/Account;

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->c:[Landroid/accounts/Account;

    array-length v0, v0

    if-gtz v0, :cond_2

    .line 193
    sget v0, Lcom/google/android/gms/p;->aD:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a(I)V

    .line 194
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "No accounts found!"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 198
    :cond_2
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->d:Landroid/bluetooth/BluetoothAdapter;

    .line 199
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->d:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_3

    .line 200
    sget v0, Lcom/google/android/gms/p;->aE:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a(I)V

    .line 201
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Bluetooth not available."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 206
    :cond_3
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->setContentView(Landroid/view/View;)V

    .line 208
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 210
    new-instance v0, Lcom/google/android/gms/auth/authzen/keyservice/g;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/authzen/keyservice/g;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->b:Lcom/google/android/gms/auth/authzen/keyservice/g;

    .line 213
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 214
    const-string v0, "keyguard"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 216
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 217
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->f:Z

    .line 221
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a(Landroid/content/Intent;)Lcom/google/android/gms/auth/setup/d2d/b/d;

    move-result-object v1

    .line 222
    if-eqz v1, :cond_7

    .line 224
    invoke-static {}, Lcom/google/k/k/a/aq;->b()Lcom/google/k/k/a/aq;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->d:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a(Lcom/google/android/gms/auth/setup/d2d/b/d;Ljava/util/concurrent/Future;)V

    :cond_5
    sget v0, Lcom/google/android/gms/p;->aB:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-boolean v3, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->f:Z

    if-eqz v3, :cond_6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v3, Lcom/google/android/gms/p;->aC:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_6
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/google/android/gms/p;->aI:I

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v3, 0x104000a

    new-instance v4, Lcom/google/android/gms/auth/setup/d2d/o;

    invoke-direct {v4, p0, v2, v1}, Lcom/google/android/gms/auth/setup/d2d/o;-><init>(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;Lcom/google/k/k/a/aq;Lcom/google/android/gms/auth/setup/d2d/b/d;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    new-instance v3, Lcom/google/android/gms/auth/setup/d2d/n;

    invoke-direct {v3, p0, v2}, Lcom/google/android/gms/auth/setup/d2d/n;-><init>(Lcom/google/android/gms/auth/setup/d2d/SourceActivity;Lcom/google/k/k/a/aq;)V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a(Landroid/app/AlertDialog;)V

    goto/16 :goto_0

    .line 226
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->finish()V

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 860
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->k:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->k:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 861
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->k:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 862
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->k:Landroid/app/AlertDialog;

    .line 864
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->n:Lcom/google/android/gms/auth/setup/d2d/k;

    if-eqz v0, :cond_1

    .line 866
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->n:Lcom/google/android/gms/auth/setup/d2d/k;

    invoke-interface {v0}, Lcom/google/android/gms/auth/setup/d2d/k;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 871
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->l:Lcom/google/android/gms/auth/setup/d2d/a/f;

    if-eqz v0, :cond_2

    .line 872
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->l:Lcom/google/android/gms/auth/setup/d2d/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/setup/d2d/a/f;->b()V

    .line 874
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->e:Z

    if-eqz v0, :cond_3

    .line 875
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->d:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    .line 877
    :cond_3
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 878
    return-void

    .line 867
    :catch_0
    move-exception v0

    .line 868
    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 251
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 254
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->o:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 255
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Acquiring WakeLock"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/String;)V

    .line 256
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->o:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 259
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 260
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->j:Z

    if-nez v0, :cond_1

    .line 261
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->c()V

    .line 263
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 232
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 234
    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 235
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->j:Z

    .line 236
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->o:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 241
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Releasing WakeLock"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/String;)V

    .line 242
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->g:Z

    .line 243
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->o:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 244
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->o:Landroid/os/PowerManager$WakeLock;

    .line 245
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->b()V

    .line 247
    :cond_0
    return-void

    .line 236
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
