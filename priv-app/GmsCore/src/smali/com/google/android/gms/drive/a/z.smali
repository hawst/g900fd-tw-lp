.class public final Lcom/google/android/gms/drive/a/z;
.super Lcom/google/android/gms/drive/a/a;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/a/ac;)V
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/gms/drive/a/e;->a:Lcom/google/android/gms/drive/a/e;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/gms/drive/a/a;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/a/ac;)V

    .line 29
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/gms/drive/a/e;->a:Lcom/google/android/gms/drive/a/e;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/drive/a/a;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    .line 33
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;B)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/a/z;-><init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/a/c;
    .locals 0

    .prologue
    .line 38
    return-object p0
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/g/aw;)V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/drive/a/c;)Z
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->a(Lcom/google/android/gms/drive/a/c;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/drive/a/a;->a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic b(Lcom/google/android/gms/drive/g/aw;)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->b(Lcom/google/android/gms/drive/g/aw;)V

    return-void
.end method

.method public final bridge synthetic d(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->d(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 54
    instance-of v0, p1, Lcom/google/android/gms/drive/a/z;

    if-nez v0, :cond_0

    .line 55
    const/4 v0, 0x0

    .line 58
    :goto_0
    return v0

    .line 57
    :cond_0
    check-cast p1, Lcom/google/android/gms/drive/a/z;

    .line 58
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/a/z;->a(Lcom/google/android/gms/drive/a/a;)Z

    move-result v0

    goto :goto_0
.end method

.method public final bridge synthetic f()Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/google/android/gms/drive/a/a;->f()Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic g()Lcom/google/android/gms/drive/auth/AppIdentity;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/google/android/gms/drive/a/a;->g()Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/z;->i()I

    move-result v0

    mul-int/lit8 v0, v0, 0x11

    return v0
.end method

.method public final j()Lcom/google/android/gms/drive/database/model/EntrySpec;
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 68
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "NullAction[%s]"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/z;->h()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
