.class public final Lcom/google/android/gms/udc/e/o;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/android/gms/udc/e/o;


# instance fields
.field public a:Lcom/google/android/gms/udc/e/p;

.field public b:Lcom/google/android/gms/udc/e/s;

.field public c:Lcom/google/android/gms/udc/e/c;

.field public d:Lcom/google/android/gms/udc/e/l;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 262
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 263
    iput-object v0, p0, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    iput-object v0, p0, Lcom/google/android/gms/udc/e/o;->b:Lcom/google/android/gms/udc/e/s;

    iput-object v0, p0, Lcom/google/android/gms/udc/e/o;->c:Lcom/google/android/gms/udc/e/c;

    iput-object v0, p0, Lcom/google/android/gms/udc/e/o;->d:Lcom/google/android/gms/udc/e/l;

    iput-object v0, p0, Lcom/google/android/gms/udc/e/o;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/udc/e/o;->cachedSize:I

    .line 264
    return-void
.end method

.method public static a()[Lcom/google/android/gms/udc/e/o;
    .locals 2

    .prologue
    .line 239
    sget-object v0, Lcom/google/android/gms/udc/e/o;->e:[Lcom/google/android/gms/udc/e/o;

    if-nez v0, :cond_1

    .line 240
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 242
    :try_start_0
    sget-object v0, Lcom/google/android/gms/udc/e/o;->e:[Lcom/google/android/gms/udc/e/o;

    if-nez v0, :cond_0

    .line 243
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/udc/e/o;

    sput-object v0, Lcom/google/android/gms/udc/e/o;->e:[Lcom/google/android/gms/udc/e/o;

    .line 245
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 247
    :cond_1
    sget-object v0, Lcom/google/android/gms/udc/e/o;->e:[Lcom/google/android/gms/udc/e/o;

    return-object v0

    .line 245
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 359
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 360
    iget-object v1, p0, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    if-eqz v1, :cond_0

    .line 361
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 364
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/udc/e/o;->b:Lcom/google/android/gms/udc/e/s;

    if-eqz v1, :cond_1

    .line 365
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/udc/e/o;->b:Lcom/google/android/gms/udc/e/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 368
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/udc/e/o;->c:Lcom/google/android/gms/udc/e/c;

    if-eqz v1, :cond_2

    .line 369
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/udc/e/o;->c:Lcom/google/android/gms/udc/e/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 372
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/udc/e/o;->d:Lcom/google/android/gms/udc/e/l;

    if-eqz v1, :cond_3

    .line 373
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/udc/e/o;->d:Lcom/google/android/gms/udc/e/l;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 376
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 278
    if-ne p1, p0, :cond_1

    .line 279
    const/4 v0, 0x1

    .line 321
    :cond_0
    :goto_0
    return v0

    .line 281
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/udc/e/o;

    if-eqz v1, :cond_0

    .line 284
    check-cast p1, Lcom/google/android/gms/udc/e/o;

    .line 285
    iget-object v1, p0, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    if-nez v1, :cond_6

    .line 286
    iget-object v1, p1, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    if-nez v1, :cond_0

    .line 294
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/udc/e/o;->b:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_7

    .line 295
    iget-object v1, p1, Lcom/google/android/gms/udc/e/o;->b:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_0

    .line 303
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/udc/e/o;->c:Lcom/google/android/gms/udc/e/c;

    if-nez v1, :cond_8

    .line 304
    iget-object v1, p1, Lcom/google/android/gms/udc/e/o;->c:Lcom/google/android/gms/udc/e/c;

    if-nez v1, :cond_0

    .line 312
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/udc/e/o;->d:Lcom/google/android/gms/udc/e/l;

    if-nez v1, :cond_9

    .line 313
    iget-object v1, p1, Lcom/google/android/gms/udc/e/o;->d:Lcom/google/android/gms/udc/e/l;

    if-nez v1, :cond_0

    .line 321
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/android/gms/udc/e/o;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 290
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/p;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 299
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/udc/e/o;->b:Lcom/google/android/gms/udc/e/s;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/o;->b:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 308
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/udc/e/o;->c:Lcom/google/android/gms/udc/e/c;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/o;->c:Lcom/google/android/gms/udc/e/c;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 317
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/udc/e/o;->d:Lcom/google/android/gms/udc/e/l;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/o;->d:Lcom/google/android/gms/udc/e/l;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/l;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 326
    iget-object v0, p0, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 329
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/o;->b:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 331
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/o;->c:Lcom/google/android/gms/udc/e/c;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 333
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/udc/e/o;->d:Lcom/google/android/gms/udc/e/l;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 335
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/udc/e/o;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 336
    return v0

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/p;->hashCode()I

    move-result v0

    goto :goto_0

    .line 329
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/o;->b:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/s;->hashCode()I

    move-result v0

    goto :goto_1

    .line 331
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/o;->c:Lcom/google/android/gms/udc/e/c;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/c;->hashCode()I

    move-result v0

    goto :goto_2

    .line 333
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/udc/e/o;->d:Lcom/google/android/gms/udc/e/l;

    invoke-virtual {v1}, Lcom/google/android/gms/udc/e/l;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 233
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/udc/e/o;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/udc/e/p;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/p;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/o;->b:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/udc/e/s;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/o;->b:Lcom/google/android/gms/udc/e/s;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/o;->b:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/udc/e/o;->c:Lcom/google/android/gms/udc/e/c;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/udc/e/c;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/o;->c:Lcom/google/android/gms/udc/e/c;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/udc/e/o;->c:Lcom/google/android/gms/udc/e/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/gms/udc/e/o;->d:Lcom/google/android/gms/udc/e/l;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/gms/udc/e/l;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/o;->d:Lcom/google/android/gms/udc/e/l;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/udc/e/o;->d:Lcom/google/android/gms/udc/e/l;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    if-eqz v0, :cond_0

    .line 343
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/o;->b:Lcom/google/android/gms/udc/e/s;

    if-eqz v0, :cond_1

    .line 346
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/udc/e/o;->b:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 348
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/o;->c:Lcom/google/android/gms/udc/e/c;

    if-eqz v0, :cond_2

    .line 349
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/udc/e/o;->c:Lcom/google/android/gms/udc/e/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 351
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/o;->d:Lcom/google/android/gms/udc/e/l;

    if-eqz v0, :cond_3

    .line 352
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/udc/e/o;->d:Lcom/google/android/gms/udc/e/l;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 354
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 355
    return-void
.end method
