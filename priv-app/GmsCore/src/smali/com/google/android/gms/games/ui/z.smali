.class public abstract Lcom/google/android/gms/games/ui/z;
.super Landroid/support/v4/app/ar;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;
.implements Lcom/google/android/gms/games/ui/d/u;


# instance fields
.field protected i:Lcom/google/android/gms/games/ui/q;

.field protected j:Landroid/view/LayoutInflater;

.field protected k:Lcom/google/android/gms/games/ui/d/t;

.field protected l:Z

.field protected m:Landroid/support/v4/widget/SwipeRefreshLayout;

.field protected n:Landroid/support/v4/widget/bp;

.field private o:Landroid/view/View;

.field private p:Z

.field private q:Landroid/widget/AbsListView$OnScrollListener;

.field private r:Landroid/database/ContentObserver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Landroid/support/v4/app/ar;-><init>()V

    .line 72
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/z;->p:Z

    .line 75
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/z;->l:Z

    return-void
.end method


# virtual methods
.method public H_()V
    .locals 0

    .prologue
    .line 330
    return-void
.end method

.method protected final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 123
    sget v0, Lcom/google/android/gms/l;->bb:I

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 124
    sget v0, Lcom/google/android/gms/j;->sm:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/z;->m:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->m:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/z;->l:Z

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->m:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v2, 0x4

    new-array v2, v2, [I

    sget v3, Lcom/google/android/gms/f;->y:I

    aput v3, v2, v4

    const/4 v3, 0x1

    sget v4, Lcom/google/android/gms/f;->A:I

    aput v4, v2, v3

    const/4 v3, 0x2

    sget v4, Lcom/google/android/gms/f;->B:I

    aput v4, v2, v3

    const/4 v3, 0x3

    sget v4, Lcom/google/android/gms/f;->z:I

    aput v4, v2, v3

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->a([I)V

    .line 130
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/z;->l:Z

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->n:Landroid/support/v4/widget/bp;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->m:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/z;->n:Landroid/support/v4/widget/bp;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Landroid/support/v4/widget/bp;)V

    .line 134
    :cond_0
    return-object v1
.end method

.method public final a(Landroid/support/v4/widget/bp;)V
    .locals 2

    .prologue
    .line 308
    iput-object p1, p0, Lcom/google/android/gms/games/ui/z;->n:Landroid/support/v4/widget/bp;

    .line 309
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->m:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->m:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/z;->n:Landroid/support/v4/widget/bp;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Landroid/support/v4/widget/bp;)V

    .line 312
    :cond_0
    return-void
.end method

.method public abstract a(Lcom/google/android/gms/common/api/v;)V
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 0

    .prologue
    .line 161
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/z;->c()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 148
    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 149
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/z;->a(Lcom/google/android/gms/common/api/v;)V

    .line 150
    return-void
.end method

.method protected final c()Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->i:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 179
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 180
    return-object v0
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 284
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/z;->l:Z

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->m:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->m:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 288
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->m:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->m:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Z)V

    .line 325
    :cond_0
    return-void
.end method

.method public final f_(I)V
    .locals 2

    .prologue
    .line 154
    const-string v0, "GamesListFragment"

    const-string v1, "Unexpected call to onConnectionSuspended - subclasses should unregister as a listener in onStop() and clear data in onDestroyView()"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    return-void
.end method

.method protected final g()V
    .locals 4

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->r:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 371
    :goto_0
    return-void

    .line 355
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/aa;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/aa;-><init>(Lcom/google/android/gms/games/ui/z;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/z;->r:Landroid/database/ContentObserver;

    .line 368
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->i:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 369
    sget-object v1, Lcom/google/android/gms/games/internal/c/a;->b:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/games/ui/z;->r:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method protected final h()V
    .locals 2

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->r:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->i:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 379
    iget-object v1, p0, Lcom/google/android/gms/games/ui/z;->r:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 380
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/z;->r:Landroid/database/ContentObserver;

    .line 382
    :cond_0
    return-void
.end method

.method protected i()V
    .locals 0

    .prologue
    .line 393
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 83
    invoke-super {p0, p1}, Landroid/support/v4/app/ar;->onActivityCreated(Landroid/os/Bundle;)V

    .line 84
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/z;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/q;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/z;->i:Lcom/google/android/gms/games/ui/q;

    .line 85
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/z;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 86
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/z;->a()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 87
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/gms/games/ui/z;->j:Landroid/view/LayoutInflater;

    .line 107
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/games/ui/z;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/z;->o:Landroid/view/View;

    .line 116
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->o:Landroid/view/View;

    new-instance v1, Lcom/google/android/gms/games/ui/d/t;

    invoke-direct {v1, v0, p0}, Lcom/google/android/gms/games/ui/d/t;-><init>(Landroid/view/View;Lcom/google/android/gms/games/ui/d/v;)V

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/d/t;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/z;->k:Lcom/google/android/gms/games/ui/d/t;

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->k:Lcom/google/android/gms/games/ui/d/t;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/d/t;->a(I)V

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->o:Landroid/view/View;

    return-object v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->q:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->q:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    .line 256
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 228
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->q:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->q:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 233
    :cond_0
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 234
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/z;->p:Z

    if-nez v1, :cond_2

    if-ne p2, v2, :cond_2

    .line 235
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/z;->p:Z

    .line 236
    instance-of v1, v0, Lcom/google/android/gms/games/ui/m;

    if-eqz v1, :cond_1

    .line 237
    check-cast v0, Lcom/google/android/gms/games/ui/m;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/m;->m()V

    .line 245
    :cond_1
    :goto_0
    return-void

    .line 239
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/z;->p:Z

    if-eqz v1, :cond_1

    if-eq p2, v2, :cond_1

    .line 240
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/z;->p:Z

    .line 241
    instance-of v1, v0, Lcom/google/android/gms/games/ui/m;

    if-eqz v1, :cond_1

    .line 242
    check-cast v0, Lcom/google/android/gms/games/ui/m;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/m;->n()V

    goto :goto_0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0}, Landroid/support/v4/app/ar;->onStart()V

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->i:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/q;->a(Lcom/google/android/gms/common/api/x;)V

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->i:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/q;->a(Lcom/google/android/gms/common/api/y;)V

    .line 94
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->i:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/q;->b(Lcom/google/android/gms/common/api/x;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/games/ui/z;->i:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/q;->b(Lcom/google/android/gms/common/api/y;)V

    .line 100
    invoke-super {p0}, Landroid/support/v4/app/ar;->onStop()V

    .line 101
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 2

    .prologue
    .line 213
    invoke-super {p0, p1}, Landroid/support/v4/app/ar;->setUserVisibleHint(Z)V

    .line 218
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/z;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 219
    instance-of v1, v0, Lcom/google/android/gms/games/ui/o;

    if-eqz v1, :cond_0

    .line 220
    check-cast v0, Lcom/google/android/gms/games/ui/o;

    .line 221
    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/games/ui/o;->a(ZZ)V

    .line 223
    :cond_0
    return-void
.end method
