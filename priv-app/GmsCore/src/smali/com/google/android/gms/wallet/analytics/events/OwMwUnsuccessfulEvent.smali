.class public Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;
.super Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final c:I

.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/p;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/analytics/events/p;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(IILjava/lang/String;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>()V

    .line 30
    iput-object p3, p0, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;->j:Ljava/lang/String;

    .line 31
    iput p1, p0, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;->c:I

    .line 32
    iput p2, p0, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;->d:I

    .line 33
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>(Landroid/os/Parcel;)V

    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;->c:I

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;->d:I

    .line 39
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static a(Landroid/content/Context;IILjava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;-><init>(IILjava/lang/String;)V

    .line 45
    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 46
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 57
    iget v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 58
    iget v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    return-void
.end method
