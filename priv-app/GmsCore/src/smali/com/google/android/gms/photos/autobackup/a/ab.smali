.class public final Lcom/google/android/gms/photos/autobackup/a/ab;
.super Lcom/google/android/gms/common/internal/aj;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V
    .locals 6

    .prologue
    .line 44
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;[Ljava/lang/String;)V

    .line 45
    iget-object v0, p3, Lcom/google/android/gms/common/internal/ClientSettings;->a:Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/a/ab;->a:Ljava/lang/String;

    .line 46
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    .prologue
    .line 25
    invoke-static {p1}, Lcom/google/android/gms/photos/autobackup/a/ag;->a(Landroid/os/IBinder;)Lcom/google/android/gms/photos/autobackup/a/af;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/internal/bj;Lcom/google/android/gms/common/internal/an;)V
    .locals 3

    .prologue
    .line 130
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 131
    const v1, 0x6768a8

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcom/google/android/gms/common/internal/bj;->p(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 134
    return-void
.end method

.method public final a(Lcom/google/android/gms/photos/autobackup/a/ac;)V
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/a/ab;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/a/af;

    invoke-interface {v0, p1}, Lcom/google/android/gms/photos/autobackup/a/af;->c(Lcom/google/android/gms/photos/autobackup/a/ac;)V

    .line 69
    return-void
.end method

.method public final a(Lcom/google/android/gms/photos/autobackup/a/ac;Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;)V
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/a/ab;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/a/af;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/photos/autobackup/a/af;->a(Lcom/google/android/gms/photos/autobackup/a/ac;Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;)V

    .line 74
    return-void
.end method

.method public final a(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/a/ab;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/a/af;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/photos/autobackup/a/af;->b(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method public final a(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;Lcom/google/android/gms/photos/autobackup/model/LocalFolder;)V
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/a/ab;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/a/af;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/photos/autobackup/a/af;->a(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;Lcom/google/android/gms/photos/autobackup/model/LocalFolder;)V

    .line 80
    return-void
.end method

.method protected final a_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    const-string v0, "com.google.android.gms.photos.autobackup.service.START"

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/photos/autobackup/a/ac;)V
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/a/ab;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/a/af;

    invoke-interface {v0, p1}, Lcom/google/android/gms/photos/autobackup/a/af;->e(Lcom/google/android/gms/photos/autobackup/a/ac;)V

    .line 110
    return-void
.end method

.method public final b(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/a/ab;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/a/af;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/photos/autobackup/a/af;->c(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V

    .line 85
    return-void
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    const-string v0, "com.google.android.gms.photos.autobackup.internal.IAutoBackupService"

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/a/ab;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/a/af;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/photos/autobackup/a/af;->d(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V

    .line 94
    return-void
.end method
