.class public final Lcom/google/android/gms/appstate/service/a/d;
.super Lcom/google/android/gms/appstate/service/a/a;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/appstate/c/h;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 25
    const-string v0, "ListStatesOp"

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/appstate/service/a/a;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/server/ClientContext;)V

    .line 26
    iput-object p2, p0, Lcom/google/android/gms/appstate/service/a/d;->b:Lcom/google/android/gms/appstate/c/h;

    .line 27
    iput-object p3, p0, Lcom/google/android/gms/appstate/service/a/d;->c:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a/d;->b:Lcom/google/android/gms/appstate/c/h;

    invoke-interface {v0, p1}, Lcom/google/android/gms/appstate/c/h;->a(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 38
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/appstate/a/e;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a/d;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/appstate/service/a/d;->c:Ljava/lang/String;

    invoke-virtual {p2, p1, v0, v1}, Lcom/google/android/gms/appstate/a/e;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
