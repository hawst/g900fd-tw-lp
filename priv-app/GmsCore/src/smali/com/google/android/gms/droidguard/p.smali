.class public final Lcom/google/android/gms/droidguard/p;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/16 v0, 0x3a98

    sput v0, Lcom/google/android/gms/droidguard/p;->a:I

    .line 20
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/http/GoogleHttpClient;
    .locals 4

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/http/GoogleHttpClient;

    const-string v1, "DroidGuard/6777000"

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 29
    invoke-virtual {v0}, Lcom/google/android/gms/http/GoogleHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v1

    const-string v2, "http.socket.timeout"

    sget v3, Lcom/google/android/gms/droidguard/p;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    move-result-object v1

    const-string v2, "http.connection.timeout"

    sget v3, Lcom/google/android/gms/droidguard/p;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 32
    return-object v0
.end method
