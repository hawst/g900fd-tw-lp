.class public abstract Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field protected final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 26
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 27
    if-eqz v0, :cond_0

    move-object p1, v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;->b:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;->a:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method protected varargs abstract a([Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method protected final varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 34
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;->a([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 37
    :goto_0
    return-object v0

    .line 35
    :catch_0
    move-exception v0

    .line 36
    iget-object v1, p0, Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;->a:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/wallet/dynamite/logging/b;->a(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v1, "WalletCrash"

    const-string v2, "Uncaught exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 37
    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 36
    :cond_1
    new-instance v3, Lcom/google/android/gms/wallet/dynamite/logging/a;

    invoke-direct {v3, v0, v2}, Lcom/google/android/gms/wallet/dynamite/logging/a;-><init>(Ljava/lang/Throwable;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/dynamite/logging/a;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.google.android.gms.wallet.REPORT_CRASH"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "com.google.android.gms"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "com.google.android.gms.wallet.CRASH_LOG"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_1
.end method
