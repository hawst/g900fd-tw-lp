.class final Lcom/google/android/gms/plus/apps/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/apps/k;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/apps/k;)V
    .locals 0

    .prologue
    .line 512
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/s;->a:Lcom/google/android/gms/plus/apps/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/apps/k;B)V
    .locals 0

    .prologue
    .line 512
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/apps/s;-><init>(Lcom/google/android/gms/plus/apps/k;)V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    .line 517
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/s;->a:Lcom/google/android/gms/plus/apps/k;

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/k;->a(Lcom/google/android/gms/plus/apps/k;)Lcom/google/android/gms/plus/apps/u;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/gms/plus/apps/u;->b(I)Lcom/google/android/gms/plus/apps/v;

    move-result-object v0

    .line 518
    iget v1, v0, Lcom/google/android/gms/plus/apps/v;->a:I

    packed-switch v1, :pswitch_data_0

    .line 528
    :goto_0
    return-void

    .line 520
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/s;->a:Lcom/google/android/gms/plus/apps/k;

    iget-object v1, v1, Lcom/google/android/gms/plus/apps/k;->j:Lcom/google/android/gms/plus/apps/c;

    sget-object v2, Lcom/google/android/gms/common/analytics/c;->A:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/s;->a:Lcom/google/android/gms/plus/apps/k;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/apps/k;->d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/plus/apps/c;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 521
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/s;->a:Lcom/google/android/gms/plus/apps/k;

    iget-object v1, v1, Lcom/google/android/gms/plus/apps/k;->j:Lcom/google/android/gms/plus/apps/c;

    iget-object v2, v0, Lcom/google/android/gms/plus/apps/v;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/apps/c;->b(Ljava/lang/String;)V

    .line 522
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/s;->a:Lcom/google/android/gms/plus/apps/k;

    invoke-static {v1}, Lcom/google/android/gms/plus/apps/k;->a(Lcom/google/android/gms/plus/apps/k;)Lcom/google/android/gms/plus/apps/u;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/plus/apps/v;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/u;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 525
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/s;->a:Lcom/google/android/gms/plus/apps/k;

    iget-object v1, v1, Lcom/google/android/gms/plus/apps/k;->j:Lcom/google/android/gms/plus/apps/c;

    sget-object v2, Lcom/google/android/gms/common/analytics/c;->z:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/s;->a:Lcom/google/android/gms/plus/apps/k;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/apps/k;->d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/plus/apps/c;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 526
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/s;->a:Lcom/google/android/gms/plus/apps/k;

    iget v2, v0, Lcom/google/android/gms/plus/apps/v;->b:I

    invoke-static {v1, v2}, Lcom/google/android/gms/plus/apps/k;->b(Lcom/google/android/gms/plus/apps/k;I)V

    .line 527
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/s;->a:Lcom/google/android/gms/plus/apps/k;

    invoke-static {v1}, Lcom/google/android/gms/plus/apps/k;->a(Lcom/google/android/gms/plus/apps/k;)Lcom/google/android/gms/plus/apps/u;

    move-result-object v1

    iget v0, v0, Lcom/google/android/gms/plus/apps/v;->b:I

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/u;->a(I)V

    goto :goto_0

    .line 518
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 537
    return-void
.end method
