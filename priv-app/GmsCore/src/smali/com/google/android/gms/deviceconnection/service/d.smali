.class final Lcom/google/android/gms/deviceconnection/service/d;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/deviceconnection/service/DeviceConnectionServiceBroker;


# direct methods
.method constructor <init>(Lcom/google/android/gms/deviceconnection/service/DeviceConnectionServiceBroker;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/gms/deviceconnection/service/d;->a:Lcom/google/android/gms/deviceconnection/service/DeviceConnectionServiceBroker;

    .line 73
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 74
    return-void
.end method


# virtual methods
.method public final l(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 79
    const-string v0, "DeviceConnectionService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "client connected with version: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid package name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/service/d;->b:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 89
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lcom/google/android/gms/deviceconnection/service/a;

    iget-object v2, p0, Lcom/google/android/gms/deviceconnection/service/d;->a:Lcom/google/android/gms/deviceconnection/service/DeviceConnectionServiceBroker;

    iget-object v3, p0, Lcom/google/android/gms/deviceconnection/service/d;->a:Lcom/google/android/gms/deviceconnection/service/DeviceConnectionServiceBroker;

    invoke-static {v3}, Lcom/google/android/gms/deviceconnection/service/DeviceConnectionServiceBroker;->a(Lcom/google/android/gms/deviceconnection/service/DeviceConnectionServiceBroker;)Lcom/google/android/gms/deviceconnection/service/c;

    move-result-object v3

    invoke-direct {v1, v2, p3, v3}, Lcom/google/android/gms/deviceconnection/service/a;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/deviceconnection/service/c;)V

    invoke-virtual {v1}, Lcom/google/android/gms/deviceconnection/service/a;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :goto_0
    return-void

    .line 93
    :catch_0
    move-exception v0

    const-string v0, "DeviceConnectionService"

    const-string v1, "Client died while brokering service."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
