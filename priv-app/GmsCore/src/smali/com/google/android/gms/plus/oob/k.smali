.class public final Lcom/google/android/gms/plus/oob/k;
.super Lcom/google/android/gms/plus/oob/d;
.source "SourceFile"


# instance fields
.field private d:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/oob/d;-><init>(Landroid/content/Context;Z)V

    .line 21
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 25
    sget v0, Lcom/google/android/gms/l;->ef:I

    return v0
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;Lcom/google/android/gms/plus/oob/e;)V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/oob/d;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;Lcom/google/android/gms/plus/oob/e;)V

    .line 42
    sget v0, Lcom/google/android/gms/p;->uQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/k;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/k;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/k;->d:Landroid/widget/TextView;

    .line 44
    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->m()Lcom/google/android/gms/plus/service/v1whitelisted/models/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/f;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/k;->d:Landroid/widget/TextView;

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->m()Lcom/google/android/gms/plus/service/v1whitelisted/models/f;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/f;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/android/gms/plus/service/v1whitelisted/models/b;
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 53
    const-string v0, "appealButton"

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/k;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 60
    const-string v0, "changeButton"

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/k;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/k;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/k;->d:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/k;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
