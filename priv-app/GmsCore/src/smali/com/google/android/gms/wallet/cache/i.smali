.class public final Lcom/google/android/gms/wallet/cache/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:[Lcom/google/aa/b/a/h;

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Ljava/lang/String;

.field public final g:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/cache/i;-><init>(B)V

    .line 144
    return-void
.end method

.method public constructor <init>(B)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 147
    invoke-static {}, Lcom/google/aa/b/a/h;->a()[Lcom/google/aa/b/a/h;

    move-result-object v4

    move-object v0, p0

    move-object v3, v2

    move-object v5, v2

    move v6, v1

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/wallet/cache/i;-><init>(ZLjava/lang/String;Ljava/lang/String;[Lcom/google/aa/b/a/h;Ljava/lang/String;ZLjava/lang/String;)V

    .line 149
    return-void
.end method

.method private constructor <init>(ZLjava/lang/String;Ljava/lang/String;[Lcom/google/aa/b/a/h;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/cache/i;->g:Z

    .line 156
    iput-object p2, p0, Lcom/google/android/gms/wallet/cache/i;->a:Ljava/lang/String;

    .line 157
    iput-object p3, p0, Lcom/google/android/gms/wallet/cache/i;->b:Ljava/lang/String;

    .line 158
    if-nez p4, :cond_0

    .line 159
    invoke-static {}, Lcom/google/aa/b/a/h;->a()[Lcom/google/aa/b/a/h;

    move-result-object p4

    .line 161
    :cond_0
    iput-object p4, p0, Lcom/google/android/gms/wallet/cache/i;->c:[Lcom/google/aa/b/a/h;

    .line 162
    iput-object p5, p0, Lcom/google/android/gms/wallet/cache/i;->d:Ljava/lang/String;

    .line 163
    iput-boolean p6, p0, Lcom/google/android/gms/wallet/cache/i;->e:Z

    .line 164
    iput-object p7, p0, Lcom/google/android/gms/wallet/cache/i;->f:Ljava/lang/String;

    .line 165
    return-void
.end method

.method static a(Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/i;
    .locals 8

    .prologue
    .line 182
    invoke-static {p0}, Lcom/google/android/gms/wallet/common/af;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ag;

    move-result-object v0

    .line 183
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ag;->b()Ljava/lang/String;

    move-result-object v2

    .line 184
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ag;->c()Ljava/lang/String;

    .line 185
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ag;->b()Ljava/lang/String;

    move-result-object v3

    .line 186
    const-class v1, Lcom/google/aa/b/a/h;

    invoke-static {}, Lcom/google/aa/b/a/h;->a()[Lcom/google/aa/b/a/h;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Lcom/google/android/gms/wallet/common/ag;->a(Ljava/lang/Class;[Lcom/google/protobuf/nano/j;)[Lcom/google/protobuf/nano/j;

    move-result-object v4

    check-cast v4, [Lcom/google/aa/b/a/h;

    .line 188
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ag;->b()Ljava/lang/String;

    move-result-object v5

    .line 189
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ag;->a(Z)Z

    move-result v6

    .line 190
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ag;->b()Ljava/lang/String;

    move-result-object v7

    .line 191
    new-instance v0, Lcom/google/android/gms/wallet/cache/i;

    const/4 v1, 0x1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/wallet/cache/i;-><init>(ZLjava/lang/String;Ljava/lang/String;[Lcom/google/aa/b/a/h;Ljava/lang/String;ZLjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 197
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/i;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/i;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    const-string v1, "SHA-256"

    invoke-static {p1, p2, v0, v1}, Lcom/google/android/gms/wallet/service/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)Z

    move-result v0

    .line 201
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 169
    invoke-static {}, Lcom/google/android/gms/wallet/common/af;->a()Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/cache/i;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ah;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ah;->a(I)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/cache/i;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ah;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/cache/i;->c:[Lcom/google/aa/b/a/h;

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ah;->a:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/cache/i;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ah;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/cache/i;->e:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ah;->a(Z)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/cache/i;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ah;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v2, v0, Lcom/google/android/gms/wallet/common/ah;->a:Ljava/util/ArrayList;

    const-string v3, "\u203d"

    const/4 v4, 0x1

    invoke-static {v3, v1, v4}, Lcom/google/android/gms/wallet/common/af;->a(Ljava/lang/String;[Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
