.class final Lcom/google/android/gms/drive/c/a/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/c/a;


# instance fields
.field final a:Lcom/google/android/libraries/rocket/impressions/g;

.field final b:Landroid/content/Context;

.field c:Ljava/lang/String;

.field private final d:Lcom/google/android/gms/drive/c/a/g;

.field private final e:Lcom/google/c/b/b/a/a/b/al;

.field private final f:Lcom/google/c/b/b/a/a/b/z;

.field private g:Z


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/c/a/g;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/c/a/d;->g:Z

    .line 95
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/c/a/g;

    iput-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->d:Lcom/google/android/gms/drive/c/a/g;

    .line 96
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->b:Landroid/content/Context;

    .line 98
    new-instance v0, Lcom/google/android/libraries/rocket/impressions/g;

    invoke-direct {v0}, Lcom/google/android/libraries/rocket/impressions/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->a:Lcom/google/android/libraries/rocket/impressions/g;

    .line 99
    new-instance v0, Lcom/google/c/b/b/a/a/b/al;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/al;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->e:Lcom/google/c/b/b/a/a/b/al;

    .line 100
    new-instance v0, Lcom/google/c/b/b/a/a/b/z;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/z;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->e:Lcom/google/c/b/b/a/a/b/al;

    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/al;->j:Lcom/google/c/b/b/a/a/b/z;

    .line 102
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/google/android/gms/drive/c/a/d;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Event already sent."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 106
    return-void

    .line 105
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(I)Lcom/google/android/gms/drive/c/a;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/b/z;->j:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Can\'t call setOpenMode() twice"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    sparse-switch p1, :sswitch_data_0

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->b:Landroid/content/Context;

    const-string v1, "ImpressionLogEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown open mode "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->j:Ljava/lang/Integer;

    :goto_1
    return-object p0

    :cond_0
    move v0, v2

    goto :goto_0

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->j:Ljava/lang/Integer;

    goto :goto_1

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->j:Ljava/lang/Integer;

    goto :goto_1

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->j:Ljava/lang/Integer;

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x10000000 -> :sswitch_0
        0x20000000 -> :sswitch_1
        0x30000000 -> :sswitch_2
    .end sparse-switch
.end method

.method public final synthetic a(II)Lcom/google/android/gms/drive/c/a;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/b/z;->g:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Can\'t call setType() twice"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->b:Landroid/content/Context;

    const-string v1, "ImpressionLogEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown category code "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->g:Ljava/lang/Integer;

    :goto_1
    packed-switch p2, :pswitch_data_1

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->b:Landroid/content/Context;

    const-string v1, "ImpressionLogEvent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown event code "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x3ec

    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/d;->a:Lcom/google/android/libraries/rocket/impressions/g;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/rocket/impressions/g;->a(I)Lcom/google/android/libraries/rocket/impressions/g;

    return-object p0

    :cond_0
    move v0, v2

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->g:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->g:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->g:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->g:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_4
    const/16 v0, 0x52d

    goto :goto_2

    :pswitch_5
    const/16 v0, 0x552

    goto :goto_2

    :pswitch_6
    const/16 v0, 0x52e

    goto :goto_2

    :pswitch_7
    const/16 v0, 0x52f

    goto :goto_2

    :pswitch_8
    const/16 v0, 0x530

    goto :goto_2

    :pswitch_9
    const/16 v0, 0x531

    goto :goto_2

    :pswitch_a
    const/16 v0, 0x532

    goto :goto_2

    :pswitch_b
    const/16 v0, 0x533

    goto :goto_2

    :pswitch_c
    const/16 v0, 0x534

    goto :goto_2

    :pswitch_d
    const/16 v0, 0x535

    goto :goto_2

    :pswitch_e
    const/16 v0, 0x536

    goto :goto_2

    :pswitch_f
    const/16 v0, 0x537

    goto :goto_2

    :pswitch_10
    const/16 v0, 0x538

    goto :goto_2

    :pswitch_11
    const/16 v0, 0x539

    goto :goto_2

    :pswitch_12
    const/16 v0, 0x53a

    goto :goto_2

    :pswitch_13
    const/16 v0, 0x53b

    goto :goto_2

    :pswitch_14
    const/16 v0, 0x53c

    goto :goto_2

    :pswitch_15
    const/16 v0, 0x53d

    goto :goto_2

    :pswitch_16
    const/16 v0, 0x53e

    goto :goto_2

    :pswitch_17
    const/16 v0, 0x53f

    goto :goto_2

    :pswitch_18
    const/16 v0, 0x540

    goto :goto_2

    :pswitch_19
    const/16 v0, 0x541

    goto :goto_2

    :pswitch_1a
    const/16 v0, 0x542

    goto :goto_2

    :pswitch_1b
    const/16 v0, 0x543

    goto :goto_2

    :pswitch_1c
    const/16 v0, 0x544

    goto :goto_2

    :pswitch_1d
    const/16 v0, 0x545

    goto :goto_2

    :pswitch_1e
    const/16 v0, 0x546

    goto :goto_2

    :pswitch_1f
    const/16 v0, 0x547

    goto/16 :goto_2

    :pswitch_20
    const/16 v0, 0x548

    goto/16 :goto_2

    :pswitch_21
    const/16 v0, 0x549

    goto/16 :goto_2

    :pswitch_22
    const/16 v0, 0x54a

    goto/16 :goto_2

    :pswitch_23
    const/16 v0, 0x54b

    goto/16 :goto_2

    :pswitch_24
    const/16 v0, 0x54c

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_5
    .end packed-switch
.end method

.method public final synthetic a(IJIII)Lcom/google/android/gms/drive/c/a;
    .locals 6

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/b/z;->m:Lcom/google/c/b/b/a/a/b/ai;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t call setSyncInfo() twice"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/c/b/b/a/a/b/ai;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/ai;-><init>()V

    packed-switch p1, :pswitch_data_0

    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/d;->b:Landroid/content/Context;

    const-string v2, "ImpressionLogEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown sync type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ai;->a:Ljava/lang/Integer;

    :goto_1
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ai;->b:Ljava/lang/Long;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ai;->c:Ljava/lang/Integer;

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ai;->d:Ljava/lang/Integer;

    packed-switch p4, :pswitch_data_1

    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/d;->b:Landroid/content/Context;

    const-string v2, "ImpressionLogEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown sync trigger "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ai;->e:Ljava/lang/Integer;

    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iput-object v0, v1, Lcom/google/c/b/b/a/a/b/z;->m:Lcom/google/c/b/b/a/a/b/ai;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ai;->a:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_1
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ai;->a:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_2
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ai;->a:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_3
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ai;->a:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_4
    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ai;->a:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_5
    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ai;->a:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_6
    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ai;->a:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_7
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ai;->e:Ljava/lang/Integer;

    goto :goto_2

    :pswitch_8
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ai;->e:Ljava/lang/Integer;

    goto :goto_2

    :pswitch_9
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ai;->e:Ljava/lang/Integer;

    goto :goto_2

    :pswitch_a
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ai;->e:Ljava/lang/Integer;

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_9
    .end packed-switch
.end method

.method public final synthetic a(JJ)Lcom/google/android/gms/drive/c/a;
    .locals 3

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/b/z;->c:Lcom/google/c/b/b/a/a/b/ac;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t call setDeviceStatus() twice"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/c/b/b/a/a/b/ac;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/ac;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ac;->a:Ljava/lang/Long;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ac;->b:Ljava/lang/Long;

    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iput-object v0, v1, Lcom/google/c/b/b/a/a/b/z;->c:Lcom/google/c/b/b/a/a/b/ac;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/ad;)Lcom/google/android/gms/drive/c/a;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/b/z;->e:Lcom/google/c/b/b/a/a/b/ad;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Can\'t call setExecutionOptions() twice"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    new-instance v3, Lcom/google/c/b/b/a/a/b/ad;

    invoke-direct {v3}, Lcom/google/c/b/b/a/a/b/ad;-><init>()V

    iget v0, p1, Lcom/google/android/gms/drive/ad;->c:I

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->b:Landroid/content/Context;

    const-string v4, "ImpressionLogEvent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unknown conflict strategy "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p1, Lcom/google/android/gms/drive/ad;->c:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v4, v5}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lcom/google/c/b/b/a/a/b/ad;->c:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/google/android/gms/drive/ad;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v3, Lcom/google/c/b/b/a/a/b/ad;->a:Ljava/lang/Boolean;

    iget-boolean v0, p1, Lcom/google/android/gms/drive/ad;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v3, Lcom/google/c/b/b/a/a/b/ad;->b:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iput-object v3, v0, Lcom/google/c/b/b/a/a/b/z;->e:Lcom/google/c/b/b/a/a/b/ad;

    return-object p0

    :cond_0
    move v0, v2

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x2

    goto :goto_1

    :pswitch_1
    move v0, v1

    goto :goto_1

    :cond_1
    move v1, v2

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/ah;)Lcom/google/android/gms/drive/c/a;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/b/z;->k:Lcom/google/c/b/b/a/a/b/af;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Can\'t call setPreferenceChange() twice"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/c/b/b/a/a/b/af;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/af;-><init>()V

    invoke-interface {p1}, Lcom/google/android/gms/drive/ah;->c()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    iget-object v3, p0, Lcom/google/android/gms/drive/c/a/d;->b:Landroid/content/Context;

    const-string v4, "ImpressionLogEvent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unknown battery usage preference "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gms/drive/ah;->c()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/c/b/b/a/a/b/af;->b:Ljava/lang/Integer;

    :goto_1
    invoke-interface {p1}, Lcom/google/android/gms/drive/ah;->a()I

    move-result v3

    packed-switch v3, :pswitch_data_1

    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/d;->b:Landroid/content/Context;

    const-string v3, "ImpressionLogEvent"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown data connection preference "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gms/drive/ah;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/af;->a:Ljava/lang/Integer;

    :goto_2
    invoke-interface {p1}, Lcom/google/android/gms/drive/ah;->b()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/af;->c:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iput-object v0, v1, Lcom/google/c/b/b/a/a/b/z;->k:Lcom/google/c/b/b/a/a/b/af;

    return-object p0

    :cond_0
    move v0, v2

    goto :goto_0

    :pswitch_0
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/c/b/b/a/a/b/af;->b:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/c/b/b/a/a/b/af;->b:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_2
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/af;->a:Ljava/lang/Integer;

    goto :goto_2

    :pswitch_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/af;->a:Ljava/lang/Integer;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x100
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/aj;)Lcom/google/android/gms/drive/c/a;
    .locals 2

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/b/z;->l:Lcom/google/c/b/b/a/a/b/ah;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t call setResourceInfo() twice"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/c/b/b/a/a/b/ah;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/ah;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/aj;->e()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ah;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/aj;->n()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ah;->c:Ljava/lang/Boolean;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/aj;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ah;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iput-object v0, v1, Lcom/google/c/b/b/a/a/b/z;->l:Lcom/google/c/b/b/a/a/b/ah;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/auth/CallingAppInfo;)Lcom/google/android/gms/drive/c/a;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/c/a/d;->b(Lcom/google/android/gms/drive/auth/CallingAppInfo;)Lcom/google/android/gms/drive/c/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/drive/c/a;
    .locals 2

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/b/z;->l:Lcom/google/c/b/b/a/a/b/ah;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t call setResourceInfo() twice"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/c/b/b/a/a/b/ah;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/ah;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ah;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->an()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ah;->c:Ljava/lang/Boolean;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->K()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/ah;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iput-object v0, v1, Lcom/google/c/b/b/a/a/b/z;->l:Lcom/google/c/b/b/a/a/b/ah;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/events/f;II)Lcom/google/android/gms/drive/c/a;
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/b/z;->b:Lcom/google/c/b/b/a/a/b/ab;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Can\'t call setCompletionEventDetails() twice"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    new-instance v4, Lcom/google/c/b/b/a/a/b/ab;

    invoke-direct {v4}, Lcom/google/c/b/b/a/a/b/ab;-><init>()V

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/drive/events/f;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    new-array v6, v5, [I

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_1

    sget-object v7, Lcom/google/android/gms/drive/c/a/e;->a:[I

    invoke-virtual {p1}, Lcom/google/android/gms/drive/events/f;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/a/e;->ordinal()I

    move-result v0

    aget v0, v7, v0

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->b:Landroid/content/Context;

    const-string v7, "ImpressionLogEvent"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Unknown action type "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/events/f;->h()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v7, v8}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    aput v2, v6, v3

    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_0

    :pswitch_0
    aput v1, v6, v3

    goto :goto_2

    :pswitch_1
    aput v10, v6, v3

    goto :goto_2

    :cond_1
    iput-object v6, v4, Lcom/google/c/b/b/a/a/b/ab;->c:[I

    invoke-virtual {p1}, Lcom/google/android/gms/drive/events/f;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lcom/google/c/b/b/a/a/b/ab;->b:Ljava/lang/Integer;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/events/f;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->b:Landroid/content/Context;

    const-string v3, "ImpressionLogEvent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unknown completion event status "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/events/f;->i()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v3, v5}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lcom/google/c/b/b/a/a/b/ab;->a:Ljava/lang/Integer;

    :cond_2
    :goto_3
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lcom/google/c/b/b/a/a/b/ab;->e:Ljava/lang/Integer;

    packed-switch p3, :pswitch_data_2

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->b:Landroid/content/Context;

    const-string v1, "ImpressionLogEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Unknown completion event resolution "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lcom/google/c/b/b/a/a/b/ab;->d:Ljava/lang/Integer;

    :goto_4
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iput-object v4, v0, Lcom/google/c/b/b/a/a/b/z;->b:Lcom/google/c/b/b/a/a/b/ab;

    return-object p0

    :pswitch_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lcom/google/c/b/b/a/a/b/ab;->a:Ljava/lang/Integer;

    goto :goto_3

    :pswitch_3
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lcom/google/c/b/b/a/a/b/ab;->a:Ljava/lang/Integer;

    goto :goto_3

    :pswitch_4
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lcom/google/c/b/b/a/a/b/ab;->a:Ljava/lang/Integer;

    goto :goto_3

    :pswitch_5
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lcom/google/c/b/b/a/a/b/ab;->a:Ljava/lang/Integer;

    goto :goto_3

    :pswitch_6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lcom/google/c/b/b/a/a/b/ab;->d:Ljava/lang/Integer;

    goto :goto_4

    :pswitch_7
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lcom/google/c/b/b/a/a/b/ab;->d:Ljava/lang/Integer;

    goto :goto_4

    :pswitch_8
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lcom/google/c/b/b/a/a/b/ab;->d:Ljava/lang/Integer;

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/c/a;
    .locals 8

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/b/z;->i:Lcom/google/c/b/b/a/a/b/ae;

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    const-string v6, "Can\'t call setMetadataUpdate() twice"

    invoke-static {v0, v6}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    new-instance v6, Lcom/google/c/b/b/a/a/b/ae;

    invoke-direct {v6}, Lcom/google/c/b/b/a/a/b/ae;-><init>()V

    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->m:Lcom/google/android/gms/drive/metadata/internal/a/d;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, v6, Lcom/google/c/b/b/a/a/b/ae;->a:Ljava/lang/Boolean;

    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v6, Lcom/google/c/b/b/a/a/b/ae;->b:Ljava/lang/Boolean;

    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->c:Lcom/google/android/gms/drive/metadata/internal/a/c;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;->a:Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v1

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->b()I

    move-result v0

    if-nez v0, :cond_6

    if-ne v2, v1, :cond_4

    move v2, v3

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    if-ne v2, v4, :cond_2

    move v2, v5

    :cond_5
    :goto_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Lcom/google/c/b/b/a/a/b/ae;->c:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iput-object v6, v0, Lcom/google/c/b/b/a/a/b/z;->i:Lcom/google/c/b/b/a/a/b/ae;

    return-object p0

    :cond_6
    if-ne v2, v1, :cond_7

    move v2, v4

    goto :goto_1

    :cond_7
    if-ne v2, v3, :cond_2

    move v2, v5

    goto :goto_2
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/query/Query;)Lcom/google/android/gms/drive/c/a;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/b/z;->o:Lcom/google/c/b/b/a/a/b/ag;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Can\'t call setQueryDetails() twice"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    new-instance v4, Lcom/google/c/b/b/a/a/b/ag;

    invoke-direct {v4}, Lcom/google/c/b/b/a/a/b/ag;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/query/Query;->a()Lcom/google/android/gms/drive/query/Filter;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/drive/c/a/f;

    invoke-direct {v2, p0, v1}, Lcom/google/android/gms/drive/c/a/f;-><init>(Lcom/google/android/gms/drive/c/a/d;B)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/drive/query/Filter;->a(Lcom/google/android/gms/drive/query/internal/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    new-array v5, v2, [I

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v1

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_1

    add-int/lit8 v0, v2, 0x1

    aput v1, v5, v2

    move v2, v0

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v5, v2

    move v2, v3

    goto :goto_1

    :cond_2
    iput-object v5, v4, Lcom/google/c/b/b/a/a/b/ag;->a:[I

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iput-object v4, v0, Lcom/google/c/b/b/a/a/b/z;->o:Lcom/google/c/b/b/a/a/b/ag;

    return-object p0
.end method

.method public final synthetic a(Ljava/lang/String;)Lcom/google/android/gms/drive/c/a;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/c/a/d;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/c/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Z)Lcom/google/android/gms/drive/c/a;
    .locals 2

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/b/z;->h:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t call setIsCached() twice"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->h:Ljava/lang/Boolean;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/c/a/d;->g:Z

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->a:Lcom/google/android/libraries/rocket/impressions/g;

    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/d;->e:Lcom/google/c/b/b/a/a/b/al;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/rocket/impressions/g;->a(Lcom/google/c/b/b/a/a/b/al;)Lcom/google/android/libraries/rocket/impressions/g;

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->d:Lcom/google/android/gms/drive/c/a/g;

    invoke-interface {v0, p0}, Lcom/google/android/gms/drive/c/a/g;->a(Lcom/google/android/gms/drive/c/a/d;)V

    .line 114
    return-void
.end method

.method public final b(Lcom/google/android/gms/drive/auth/CallingAppInfo;)Lcom/google/android/gms/drive/c/a/d;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 296
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    .line 297
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/b/z;->a:Lcom/google/c/b/b/a/a/b/aa;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Can\'t call setAppInfo() twice"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 299
    new-instance v0, Lcom/google/c/b/b/a/a/b/aa;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/b/aa;-><init>()V

    .line 300
    invoke-virtual {p1}, Lcom/google/android/gms/drive/auth/CallingAppInfo;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v0, Lcom/google/c/b/b/a/a/b/aa;->a:Ljava/lang/Long;

    .line 301
    invoke-virtual {p1}, Lcom/google/android/gms/drive/auth/CallingAppInfo;->b()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/c/b/b/a/a/b/aa;->c:Ljava/lang/String;

    .line 303
    invoke-virtual {p1}, Lcom/google/android/gms/drive/auth/CallingAppInfo;->c()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 314
    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/d;->b:Landroid/content/Context;

    const-string v3, "ImpressionLogEvent"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown proxy "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/auth/CallingAppInfo;->c()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/aa;->b:Ljava/lang/Integer;

    .line 318
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iput-object v0, v1, Lcom/google/c/b/b/a/a/b/z;->a:Lcom/google/c/b/b/a/a/b/aa;

    .line 319
    return-object p0

    :cond_0
    move v0, v2

    .line 297
    goto :goto_0

    .line 305
    :pswitch_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/aa;->b:Ljava/lang/Integer;

    goto :goto_1

    .line 308
    :pswitch_1
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/aa;->b:Ljava/lang/Integer;

    goto :goto_1

    .line 311
    :pswitch_2
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/aa;->b:Ljava/lang/Integer;

    goto :goto_1

    .line 303
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/drive/c/a/d;
    .locals 2

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t call setAccountName() twice"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 134
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->c:Ljava/lang/String;

    .line 135
    return-object p0

    .line 133
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic b()Lcom/google/android/gms/drive/c/a;
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->a:Lcom/google/android/libraries/rocket/impressions/g;

    invoke-virtual {v0}, Lcom/google/android/libraries/rocket/impressions/g;->a()Lcom/google/android/libraries/rocket/impressions/g;

    return-object p0
.end method

.method public final synthetic b(I)Lcom/google/android/gms/drive/c/a;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/b/z;->d:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Can\'t call setEventDeliveryMechanism() twice"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->b:Landroid/content/Context;

    const-string v1, "ImpressionLogEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown event delivery mechanism "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->d:Ljava/lang/Integer;

    :goto_1
    return-object p0

    :cond_0
    move v0, v2

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->d:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->d:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final synthetic c()Lcom/google/android/gms/drive/c/a;
    .locals 4

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->a:Lcom/google/android/libraries/rocket/impressions/g;

    iget-object v1, v0, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    new-instance v2, Lcom/google/c/b/b/a/a/a;

    invoke-direct {v2}, Lcom/google/c/b/b/a/a/a;-><init>()V

    iput-object v2, v1, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    iget-object v1, v0, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v1, v1, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    new-instance v2, Lcom/google/c/b/b/a/a/b;

    invoke-direct {v2}, Lcom/google/c/b/b/a/a/b;-><init>()V

    iput-object v2, v1, Lcom/google/c/b/b/a/a/a;->b:Lcom/google/c/b/b/a/a/b;

    iget-object v1, v0, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v1, v1, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    iget-object v1, v1, Lcom/google/c/b/b/a/a/a;->b:Lcom/google/c/b/b/a/a/b;

    iget-object v0, v0, Lcom/google/android/libraries/rocket/impressions/g;->a:Lcom/google/android/libraries/rocket/impressions/f;

    invoke-interface {v0}, Lcom/google/android/libraries/rocket/impressions/f;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Lcom/google/c/b/b/a/a/b;->a:Ljava/lang/Long;

    return-object p0
.end method

.method public final synthetic c(I)Lcom/google/android/gms/drive/c/a;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/b/z;->n:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Can\'t call setUiResult() twice"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->b:Landroid/content/Context;

    const-string v1, "ImpressionLogEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown UI result "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->n:Ljava/lang/Integer;

    :goto_1
    return-object p0

    :cond_0
    move v0, v2

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->n:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->n:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->n:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->n:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final synthetic d()Lcom/google/android/gms/drive/c/a;
    .locals 4

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->a:Lcom/google/android/libraries/rocket/impressions/g;

    iget-object v1, v0, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v1, v1, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v1, v1, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    iget-object v1, v1, Lcom/google/c/b/b/a/a/a;->b:Lcom/google/c/b/b/a/a/b;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v1, v1, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    iget-object v1, v1, Lcom/google/c/b/b/a/a/a;->b:Lcom/google/c/b/b/a/a/b;

    iget-object v1, v1, Lcom/google/c/b/b/a/a/b;->a:Ljava/lang/Long;

    if-nez v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "must call setElapsedStartTimeToSystemTime first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v1, v0, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v1, v1, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/c/b/b/a/a/a;->c:Ljava/lang/Integer;

    iget-object v1, v0, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v1, v1, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    iget-object v1, v1, Lcom/google/c/b/b/a/a/a;->b:Lcom/google/c/b/b/a/a/b;

    iget-object v0, v0, Lcom/google/android/libraries/rocket/impressions/g;->a:Lcom/google/android/libraries/rocket/impressions/f;

    invoke-interface {v0}, Lcom/google/android/libraries/rocket/impressions/f;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Lcom/google/c/b/b/a/a/b;->b:Ljava/lang/Long;

    return-object p0
.end method

.method public final synthetic d(I)Lcom/google/android/gms/drive/c/a;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/drive/c/a/d;->e()V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/b/z;->f:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Can\'t call setFileType() twice"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->b:Landroid/content/Context;

    const-string v1, "ImpressionLogEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown file type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->f:Ljava/lang/Integer;

    :goto_1
    return-object p0

    :cond_0
    move v0, v2

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->f:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/d;->f:Lcom/google/c/b/b/a/a/b/z;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/b/z;->f:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
