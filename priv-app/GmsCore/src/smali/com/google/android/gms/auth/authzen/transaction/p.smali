.class public final Lcom/google/android/gms/auth/authzen/transaction/p;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;
    .locals 2

    .prologue
    .line 180
    const/4 v0, 0x0

    const/high16 v1, 0x10000000

    invoke-static {p0, v0, p1, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;
    .locals 2

    .prologue
    .line 371
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 372
    const-string v1, "AUTHZEN_NOTIFICATION_DISMISSED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 373
    const-class v1, Lcom/google/android/gms/auth/authzen/GcmReceiverService$Receiver;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 374
    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/ab/b/a/a/h;)Landroid/net/Uri;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 247
    if-eqz p0, :cond_0

    iget-boolean v1, p0, Lcom/google/ab/b/a/a/h;->f:Z

    if-nez v1, :cond_1

    .line 264
    :cond_0
    :goto_0
    :pswitch_0
    return-object v0

    .line 251
    :cond_1
    iget v1, p0, Lcom/google/ab/b/a/a/h;->g:I

    packed-switch v1, :pswitch_data_0

    .line 263
    const-string v1, "AuthZen"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown sound type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/ab/b/a/a/h;->g:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 255
    :pswitch_1
    iget-object v0, p0, Lcom/google/ab/b/a/a/h;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 261
    :pswitch_2
    const/4 v0, 0x2

    invoke-static {v0}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 251
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/google/ab/b/a/a/r;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 83
    iget-object v1, p0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    .line 84
    const/4 v0, -0x1

    .line 85
    iget v2, v1, Lcom/google/ab/b/a/a/q;->b:I

    packed-switch v2, :pswitch_data_0

    .line 93
    const-string v2, "AuthZen"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Bad prompt type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v1, Lcom/google/ab/b/a/a/q;->b:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :goto_0
    iget-boolean v2, v1, Lcom/google/ab/b/a/a/q;->h:Z

    if-eqz v2, :cond_0

    .line 98
    iget-object v1, v1, Lcom/google/ab/b/a/a/q;->i:Lcom/google/ab/b/a/a/h;

    iget v1, v1, Lcom/google/ab/b/a/a/h;->h:I

    .line 103
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "authzen:notificationTag:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x3a

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 87
    :pswitch_0
    const/4 v0, 0x2

    .line 88
    goto :goto_0

    .line 90
    :pswitch_1
    iget-object v0, v1, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    iget v0, v0, Lcom/google/ab/b/a/a/e;->d:I

    goto :goto_0

    .line 101
    :cond_0
    new-instance v1, Lcom/google/ab/b/a/a/h;

    invoke-direct {v1}, Lcom/google/ab/b/a/a/h;-><init>()V

    iget v1, v1, Lcom/google/ab/b/a/a/h;->h:I

    goto :goto_1

    .line 85
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Lcom/google/ab/b/a/a/r;Landroid/content/Intent;)V
    .locals 9

    .prologue
    .line 49
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 50
    const-string v2, "creation_elapsed_time"

    invoke-virtual {p2, v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 52
    invoke-static {p1}, Lcom/google/android/gms/auth/authzen/transaction/p;->a(Lcom/google/ab/b/a/a/r;)Ljava/lang/String;

    move-result-object v4

    .line 53
    invoke-virtual {p2, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    new-instance v0, Landroid/content/Intent;

    const-string v1, "AUTHZEN_CLOSE_ACTIVITY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "notification_tag"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "transaction_id"

    iget-object v2, p1, Lcom/google/ab/b/a/a/r;->a:Lcom/google/protobuf/a/a;

    invoke-virtual {v2}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const/high16 v0, 0x20000000

    invoke-static {p0, p2, v0}, Lcom/google/android/gms/auth/authzen/transaction/p;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Landroid/app/PendingIntent;->send()V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-static {p0, v4}, Lcom/google/android/gms/auth/authzen/transaction/p;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 58
    iget-object v0, p1, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    iget v1, v0, Lcom/google/ab/b/a/a/q;->c:I

    if-eqz v1, :cond_1

    iget-boolean v0, p1, Lcom/google/ab/b/a/a/r;->g:Z

    if-eqz v0, :cond_8

    iget-object v0, p1, Lcom/google/ab/b/a/a/r;->h:Lcom/google/ab/b/a/a/i;

    iget-object v0, v0, Lcom/google/ab/b/a/a/i;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/util/au;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_9

    :cond_1
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_d

    .line 60
    invoke-static {p0, p2}, Lcom/google/android/gms/auth/authzen/transaction/p;->c(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v5

    const-string v0, "creation_elapsed_time"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {p2, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {p1}, Lcom/google/android/gms/auth/authzen/transaction/p;->b(Lcom/google/ab/b/a/a/r;)J

    move-result-wide v0

    const-string v2, "AuthZen"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v8, "Prompt lifetime in millis: "

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_10

    const-wide/16 v0, 0x0

    move-wide v2, v0

    :goto_3
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    const/4 v1, 0x2

    add-long/2addr v2, v6

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 62
    iget-object v0, p1, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    iget-object v0, v0, Lcom/google/ab/b/a/a/q;->i:Lcom/google/ab/b/a/a/h;

    iget-object v1, p1, Lcom/google/ab/b/a/a/r;->h:Lcom/google/ab/b/a/a/i;

    new-instance v2, Landroid/support/v4/app/bk;

    invoke-direct {v2, p0}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Landroid/support/v4/app/bk;->a()Landroid/support/v4/app/bk;

    move-result-object v2

    const/4 v3, 0x1

    iput v3, v2, Landroid/support/v4/app/bk;->z:I

    const/4 v3, 0x0

    const/high16 v5, 0x50000000

    invoke-static {p0, v3, p2, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    iput-object v3, v2, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    const/4 v3, 0x0

    invoke-static {p0, p2, v3}, Lcom/google/android/gms/auth/authzen/transaction/p;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bk;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/bk;

    move-result-object v2

    sget v3, Lcom/google/android/gms/h;->n:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v5, Lcom/google/android/gms/h;->o:I

    invoke-static {v3, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, v2, Landroid/support/v4/app/bk;->g:Landroid/graphics/Bitmap;

    iget-boolean v3, v0, Lcom/google/ab/b/a/a/h;->d:Z

    const/4 v5, 0x2

    invoke-virtual {v2, v5, v3}, Landroid/support/v4/app/bk;->a(IZ)V

    iget-boolean v3, v0, Lcom/google/ab/b/a/a/h;->c:Z

    iput-boolean v3, v2, Landroid/support/v4/app/bk;->l:Z

    iget v3, v0, Lcom/google/ab/b/a/a/h;->a:I

    iput v3, v2, Landroid/support/v4/app/bk;->j:I

    iget-object v3, v1, Lcom/google/ab/b/a/a/i;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v2

    iget-boolean v3, v1, Lcom/google/ab/b/a/a/i;->b:Z

    if-eqz v3, :cond_2

    iget-object v3, v1, Lcom/google/ab/b/a/a/i;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    :cond_2
    iget-boolean v3, v1, Lcom/google/ab/b/a/a/i;->f:Z

    if-eqz v3, :cond_3

    iget-object v3, v1, Lcom/google/ab/b/a/a/i;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bk;->d(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    :cond_3
    iget-boolean v3, v1, Lcom/google/ab/b/a/a/i;->d:Z

    if-eqz v3, :cond_4

    iget-object v3, v1, Lcom/google/ab/b/a/a/i;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bk;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    :cond_4
    iget-boolean v3, v1, Lcom/google/ab/b/a/a/i;->h:Z

    if-eqz v3, :cond_5

    iget-object v1, v1, Lcom/google/ab/b/a/a/i;->i:Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/support/v4/app/bk;->e(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    :cond_5
    invoke-static {v0}, Lcom/google/android/gms/auth/authzen/transaction/p;->a(Lcom/google/ab/b/a/a/h;)Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v2, v1}, Landroid/support/v4/app/bk;->a(Landroid/net/Uri;)Landroid/support/v4/app/bk;

    :cond_6
    invoke-static {v0}, Lcom/google/android/gms/auth/authzen/transaction/p;->b(Lcom/google/ab/b/a/a/h;)[J

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v1, v2, Landroid/support/v4/app/bk;->B:Landroid/app/Notification;

    iput-object v0, v1, Landroid/app/Notification;->vibrate:[J

    :cond_7
    invoke-virtual {v2}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v1

    .line 64
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 66
    const/4 v2, 0x1

    invoke-virtual {v0, v4, v2, v1}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 74
    :goto_4
    return-void

    .line 55
    :catch_0
    move-exception v0

    const-string v1, "AuthZen"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "AuthZen"

    const-string v2, "PendingIntent cancelled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 58
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_9
    const/4 v0, 0x1

    if-ne v1, v0, :cond_a

    const/4 v0, 0x1

    goto/16 :goto_2

    :cond_a
    const/4 v0, 0x2

    if-ne v1, v0, :cond_c

    invoke-static {p0}, Lcom/google/android/gms/auth/authzen/a/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_b

    const/4 v0, 0x1

    goto/16 :goto_2

    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_c
    const-string v0, "AuthZen"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown interaction type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto/16 :goto_2

    .line 70
    :cond_d
    iget-object v0, p1, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    iget-object v0, v0, Lcom/google/ab/b/a/a/q;->i:Lcom/google/ab/b/a/a/h;

    invoke-static {v0}, Lcom/google/android/gms/auth/authzen/transaction/p;->a(Lcom/google/ab/b/a/a/h;)Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_e

    invoke-static {p0, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/Ringtone;->play()V

    :cond_e
    invoke-static {v0}, Lcom/google/android/gms/auth/authzen/transaction/p;->b(Lcom/google/ab/b/a/a/h;)[J

    move-result-object v1

    if-eqz v1, :cond_f

    const-string v0, "vibrator"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 72
    :cond_f
    invoke-virtual {p0, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_4

    :cond_10
    move-wide v2, v0

    goto/16 :goto_3
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 223
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 225
    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 226
    return-void
.end method

.method static b(Lcom/google/ab/b/a/a/r;)J
    .locals 4

    .prologue
    .line 174
    iget-wide v0, p0, Lcom/google/ab/b/a/a/r;->b:J

    .line 175
    iget-object v2, p0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    iget-wide v2, v2, Lcom/google/ab/b/a/a/q;->a:J

    .line 176
    sub-long v0, v2, v0

    return-wide v0
.end method

.method static b(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 209
    invoke-static {p0, p1}, Lcom/google/android/gms/auth/authzen/transaction/p;->c(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 210
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 211
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 212
    return-void
.end method

.method private static b(Lcom/google/ab/b/a/a/h;)[J
    .locals 8

    .prologue
    .line 269
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/google/ab/b/a/a/h;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 272
    :cond_0
    const/4 v0, 0x0

    .line 274
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/ab/b/a/a/h;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [J

    const/4 v0, 0x0

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v3, v1

    move v1, v2

    goto :goto_1

    :cond_2
    move-object v0, v3

    goto :goto_0
.end method

.method private static c(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 216
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 217
    const-string v1, "AUTHZEN_NOTIFICATION_EXPIRED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 218
    const-class v1, Lcom/google/android/gms/auth/authzen/GcmReceiverService$Receiver;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 219
    const/4 v1, 0x0

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method
