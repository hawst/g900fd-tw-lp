.class public final Lcom/google/android/gms/audiomodem/bl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/audiomodem/p;

.field b:Lcom/google/android/gms/audiomodem/Encoding;

.field c:I

.field d:[B


# direct methods
.method public constructor <init>(Lcom/google/android/gms/audiomodem/p;Lcom/google/android/gms/audiomodem/Encoding;I[B)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/bl;->a:Lcom/google/android/gms/audiomodem/p;

    .line 237
    iput-object p2, p0, Lcom/google/android/gms/audiomodem/bl;->b:Lcom/google/android/gms/audiomodem/Encoding;

    .line 238
    iput p3, p0, Lcom/google/android/gms/audiomodem/bl;->c:I

    .line 239
    iput-object p4, p0, Lcom/google/android/gms/audiomodem/bl;->d:[B

    .line 240
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 259
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/google/android/gms/audiomodem/bl;

    if-nez v1, :cond_1

    .line 263
    :cond_0
    :goto_0
    return v0

    .line 262
    :cond_1
    check-cast p1, Lcom/google/android/gms/audiomodem/bl;

    .line 263
    iget-object v1, p0, Lcom/google/android/gms/audiomodem/bl;->a:Lcom/google/android/gms/audiomodem/p;

    iget-object v2, p1, Lcom/google/android/gms/audiomodem/bl;->a:Lcom/google/android/gms/audiomodem/p;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/audiomodem/bl;->b:Lcom/google/android/gms/audiomodem/Encoding;

    iget-object v2, p1, Lcom/google/android/gms/audiomodem/bl;->b:Lcom/google/android/gms/audiomodem/Encoding;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/gms/audiomodem/bl;->c:I

    iget v2, p1, Lcom/google/android/gms/audiomodem/bl;->c:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/audiomodem/bl;->d:[B

    iget-object v2, p1, Lcom/google/android/gms/audiomodem/bl;->d:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 270
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/bl;->a:Lcom/google/android/gms/audiomodem/p;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/bl;->b:Lcom/google/android/gms/audiomodem/Encoding;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/audiomodem/bl;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/bl;->d:[B

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
