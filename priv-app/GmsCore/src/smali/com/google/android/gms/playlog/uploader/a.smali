.class public final Lcom/google/android/gms/playlog/uploader/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/playlog/uploader/a;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/common/util/p;

.field private final d:J

.field private final e:J

.field private f:J

.field private g:J


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;JJ)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/playlog/uploader/a;->b:Landroid/content/Context;

    .line 50
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/util/p;

    iput-object v0, p0, Lcom/google/android/gms/playlog/uploader/a;->c:Lcom/google/android/gms/common/util/p;

    .line 51
    iput-wide p3, p0, Lcom/google/android/gms/playlog/uploader/a;->d:J

    .line 52
    iput-wide p5, p0, Lcom/google/android/gms/playlog/uploader/a;->e:J

    .line 53
    iput-wide v2, p0, Lcom/google/android/gms/playlog/uploader/a;->f:J

    .line 54
    iput-wide v2, p0, Lcom/google/android/gms/playlog/uploader/a;->g:J

    .line 55
    return-void
.end method

.method public static declared-synchronized a()Lcom/google/android/gms/playlog/uploader/a;
    .locals 9

    .prologue
    .line 27
    const-class v8, Lcom/google/android/gms/playlog/uploader/a;

    monitor-enter v8

    :try_start_0
    sget-object v0, Lcom/google/android/gms/playlog/uploader/a;->a:Lcom/google/android/gms/playlog/uploader/a;

    if-nez v0, :cond_0

    .line 28
    sget-object v0, Lcom/google/android/gms/playlog/a/e;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/playlog/uploader/a;->b(J)J

    move-result-wide v4

    .line 30
    sget-object v0, Lcom/google/android/gms/playlog/a/e;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/playlog/uploader/a;->b(J)J

    move-result-wide v6

    .line 31
    new-instance v1, Lcom/google/android/gms/playlog/uploader/a;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v3

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/playlog/uploader/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;JJ)V

    sput-object v1, Lcom/google/android/gms/playlog/uploader/a;->a:Lcom/google/android/gms/playlog/uploader/a;

    .line 34
    :cond_0
    sget-object v0, Lcom/google/android/gms/playlog/uploader/a;->a:Lcom/google/android/gms/playlog/uploader/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v8

    return-object v0

    .line 27
    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0
.end method

.method private a(JJLjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 116
    invoke-static {}, Lcom/google/android/gms/playlog/uploader/a;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    const-string v0, "Scheduler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "scheduleTask: windowStart="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " windowEnd="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " tag="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v0, v2, p1

    if-gtz v0, :cond_1

    cmp-long v0, p1, p3

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/playlog/uploader/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/gcm/y;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/y;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/gcm/an;

    invoke-direct {v2}, Lcom/google/android/gms/gcm/an;-><init>()V

    const-class v3, Lcom/google/android/gms/playlog/uploader/UploaderService;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/gcm/an;->a(Ljava/lang/Class;)Lcom/google/android/gms/gcm/an;

    move-result-object v2

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/google/android/gms/gcm/an;->a(JJ)Lcom/google/android/gms/gcm/an;

    move-result-object v2

    invoke-virtual {v2, p5}, Lcom/google/android/gms/gcm/an;->a(Ljava/lang/String;)Lcom/google/android/gms/gcm/an;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/gcm/an;->a(Z)Lcom/google/android/gms/gcm/an;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/gcm/an;->b()Lcom/google/android/gms/gcm/OneoffTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/y;->a(Lcom/google/android/gms/gcm/Task;)V

    .line 130
    return-void

    :cond_1
    move v0, v1

    .line 120
    goto :goto_0
.end method

.method private static b(J)J
    .locals 2

    .prologue
    .line 38
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p0, p1, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method private d()J
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/gms/playlog/uploader/a;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/playlog/uploader/a;->b(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private e()J
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/gms/playlog/uploader/a;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/playlog/uploader/a;->b(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private static f()Z
    .locals 1

    .prologue
    .line 133
    sget-object v0, Lcom/google/android/gms/playlog/a/e;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(J)V
    .locals 5

    .prologue
    .line 58
    invoke-static {p1, p2}, Lcom/google/android/gms/playlog/uploader/a;->b(J)J

    move-result-wide v2

    .line 59
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 60
    invoke-direct {p0}, Lcom/google/android/gms/playlog/uploader/a;->d()J

    move-result-wide v0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/playlog/uploader/a;->f:J

    .line 61
    return-void

    .line 59
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 10

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/google/android/gms/playlog/uploader/a;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/gms/playlog/uploader/a;->g:J

    iget-wide v2, p0, Lcom/google/android/gms/playlog/uploader/a;->d:J

    add-long/2addr v0, v2

    invoke-direct {p0}, Lcom/google/android/gms/playlog/uploader/a;->e()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 86
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/playlog/uploader/a;->e()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/playlog/uploader/a;->g:J

    .line 79
    invoke-static {}, Lcom/google/android/gms/playlog/uploader/a;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    const-string v0, "Scheduler"

    const-string v1, "scheduleUpload"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/playlog/uploader/a;->d()J

    move-result-wide v0

    .line 83
    iget-wide v2, p0, Lcom/google/android/gms/playlog/uploader/a;->f:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/gms/playlog/uploader/a;->e:J

    long-to-double v4, v4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    add-double/2addr v6, v8

    mul-double/2addr v4, v6

    double-to-long v4, v4

    add-long/2addr v4, v2

    .line 85
    sub-long v2, v4, v0

    iget-wide v6, p0, Lcom/google/android/gms/playlog/uploader/a;->e:J

    add-long/2addr v4, v6

    sub-long/2addr v4, v0

    const-string v6, "upload"

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/playlog/uploader/a;->a(JJLjava/lang/String;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 8

    .prologue
    .line 99
    invoke-static {}, Lcom/google/android/gms/playlog/uploader/a;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    const-string v0, "Scheduler"

    const-string v1, "scheduleUploadAsap"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/playlog/uploader/a;->d()J

    move-result-wide v0

    .line 103
    iget-wide v2, p0, Lcom/google/android/gms/playlog/uploader/a;->f:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 104
    sub-long v2, v4, v0

    iget-wide v6, p0, Lcom/google/android/gms/playlog/uploader/a;->e:J

    add-long/2addr v4, v6

    sub-long/2addr v4, v0

    const-string v6, "upload_asap"

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/playlog/uploader/a;->a(JJLjava/lang/String;)V

    .line 105
    return-void
.end method
