.class public final Lcom/google/android/gms/plus/audience/AudienceSearchActivity;
.super Lcom/google/android/gms/plus/audience/n;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/google/android/gms/plus/audience/bh;


# instance fields
.field private t:Landroid/widget/EditText;

.field private u:Lcom/google/android/gms/plus/audience/g;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/n;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/google/android/gms/p;->sX:I

    return v0
.end method

.method protected final synthetic a(Landroid/content/Intent;Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/plus/audience/al;
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 33
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/n;->b:Ljava/lang/String;

    const-string v2, "SHOULD_LOAD_GROUPS"

    invoke-virtual {p1, v2, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    const-string v3, "LOAD_CIRCLES"

    invoke-virtual {p1, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    const-string v4, "LOAD_PEOPLE"

    invoke-virtual {p1, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    const-string v5, "LOAD_PEOPLE"

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    const-string v6, "EXTRA_SEARCH_DEVICE"

    invoke-virtual {p1, v6, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    const-string v7, "EXTRA_SEARCH_EMAIL"

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    iget-object v8, p0, Lcom/google/android/gms/plus/audience/n;->d:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/gms/plus/audience/n;->c:Ljava/lang/String;

    invoke-static/range {v0 .. v9}, Lcom/google/android/gms/plus/audience/g;->a(Ljava/lang/String;Ljava/lang/String;ZZZZZZLjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/audience/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSearchActivity;->u:Lcom/google/android/gms/plus/audience/g;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSearchActivity;->u:Lcom/google/android/gms/plus/audience/g;

    return-object v0
.end method

.method protected final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/n;->a(Landroid/os/Bundle;)V

    .line 66
    sget v0, Lcom/google/android/gms/j;->B:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/n;->f:Lcom/google/android/gms/plus/audience/bg;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/bg;->a(Lcom/google/android/gms/plus/audience/bh;)V

    .line 70
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->dz:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSearchActivity;->t:Landroid/widget/EditText;

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSearchActivity;->t:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSearchActivity;->t:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 77
    sget v0, Lcom/google/android/gms/j;->qP:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/AudienceSearchActivity;->t:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 78
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/n;->f:Lcom/google/android/gms/plus/audience/bg;

    iget-object v0, v0, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 85
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSearchActivity;->b()V

    .line 87
    :cond_0
    return-void
.end method

.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 114
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSearchActivity;->u:Lcom/google/android/gms/plus/audience/g;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/g;->a(Ljava/lang/String;)V

    .line 117
    :cond_0
    return-void
.end method

.method protected final b()V
    .locals 2

    .prologue
    .line 91
    sget-object v0, Lcom/google/android/gms/common/analytics/u;->z:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSearchActivity;->h()Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSearchActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    .line 92
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/n;->b()V

    .line 93
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 122
    return-void
.end method

.method protected final c()V
    .locals 2

    .prologue
    .line 97
    sget-object v0, Lcom/google/android/gms/common/analytics/u;->A:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSearchActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    .line 98
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/n;->c()V

    .line 99
    return-void
.end method

.method protected final d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/google/android/gms/common/analytics/v;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 103
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 105
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSearchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 107
    const/4 v0, 0x1

    .line 109
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 127
    return-void
.end method
