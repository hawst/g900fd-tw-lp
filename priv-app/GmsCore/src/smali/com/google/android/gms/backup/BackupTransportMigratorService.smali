.class public Lcom/google/android/gms/backup/BackupTransportMigratorService;
.super Landroid/app/IntentService;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 33
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "Backup"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "BackupTransportMigratorService"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 113
    const-string v0, "BackupTransportMigratorService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 114
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/backup/BackupTransportMigratorService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method static synthetic a()Lcom/google/android/gms/auth/d/a;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    return-object v0
.end method

.method private static a(Landroid/content/Context;Landroid/content/ComponentName;Z)Z
    .locals 9

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 298
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 299
    invoke-virtual {v4, p1}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v3

    .line 301
    if-nez v3, :cond_3

    .line 302
    const/4 v3, 0x0

    .line 308
    :goto_0
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eq v3, p2, :cond_2

    .line 309
    :cond_0
    sget-object v3, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v5, "Switch enabled status of %s to %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 311
    if-eqz p2, :cond_1

    move v2, v0

    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v4, p1, v2, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 319
    :cond_2
    :goto_1
    return v0

    .line 304
    :cond_3
    if-ne v3, v0, :cond_4

    move v3, v0

    :goto_2
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    :cond_4
    move v3, v1

    goto :goto_2

    .line 318
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Component name not found : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    move v0, v1

    .line 319
    goto :goto_1
.end method

.method public static b(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 61
    invoke-static {p0}, Lcom/google/android/gms/backup/BackupTransportMigratorService;->e(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    :goto_0
    return-void

    .line 64
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 68
    :try_start_0
    const-string v0, "com.google.android.backuptransport/com.google.android.backup.BackupTransportService"

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 70
    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 86
    :goto_1
    if-eqz v0, :cond_1

    const/4 v3, 0x2

    if-eq v1, v3, :cond_1

    .line 88
    sget-object v1, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v3, "Legacy backup transport was not disabled correctly. Going to disable again."

    invoke-virtual {v1, v3}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 89
    invoke-static {p0, v0, v2}, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a(Landroid/content/Context;Landroid/content/ComponentName;Z)Z

    .line 93
    :cond_1
    new-instance v0, Lcom/google/android/gms/backup/y;

    invoke-direct {v0}, Lcom/google/android/gms/backup/y;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/content/Context;

    aput-object p0, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/backup/y;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :catch_0
    move-exception v0

    .line 74
    :try_start_1
    const-string v0, "com.google.android.backup/.BackupTransportService"

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 76
    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    goto :goto_1

    .line 80
    :catch_1
    move-exception v0

    sget-object v0, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Didn\'t find legacy backup transport component."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->d(Ljava/lang/String;)V

    .line 81
    const/4 v0, 0x0

    move v1, v2

    .line 82
    goto :goto_1
.end method

.method private b()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 169
    :try_start_0
    new-instance v1, Landroid/app/backup/BackupManager;

    invoke-direct {v1, p0}, Landroid/app/backup/BackupManager;-><init>(Landroid/content/Context;)V

    .line 174
    const-string v2, "com.google.android.backup/.BackupTransportService"

    invoke-virtual {v1}, Landroid/app/backup/BackupManager;->getCurrentTransport()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 175
    sget-object v2, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v3, "Selecting GMS BackupTransportService."

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 176
    :cond_0
    const-string v2, "com.google.android.gms/.backup.BackupTransportService"

    invoke-virtual {v1, v2}, Landroid/app/backup/BackupManager;->selectBackupTransport(Ljava/lang/String;)Ljava/lang/String;

    .line 180
    :cond_1
    const-string v2, "com.google.android.backup/.BackupTransportService"

    invoke-virtual {v1}, Landroid/app/backup/BackupManager;->getCurrentTransport()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 182
    const-wide/16 v2, 0x1388

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 183
    sget-object v2, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v3, "Selecting GMS BackupTransportService again."

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 184
    :cond_2
    const-string v2, "com.google.android.gms/.backup.BackupTransportService"

    invoke-virtual {v1, v2}, Landroid/app/backup/BackupManager;->selectBackupTransport(Ljava/lang/String;)Ljava/lang/String;

    .line 187
    const-string v2, "com.google.android.gms/.backup.BackupTransportService"

    invoke-virtual {v1}, Landroid/app/backup/BackupManager;->getCurrentTransport()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 189
    sget-object v1, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 190
    sget-object v1, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Could not select GMS BackupTransportService."

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 211
    :cond_3
    :goto_0
    return v0

    .line 196
    :cond_4
    invoke-static {p0}, Lcom/google/android/gms/backup/BackupTransportMigratorService;->f(Landroid/content/Context;)V

    .line 199
    const-string v1, "com.google.android.backuptransport/com.google.android.backup.BackupTransportService"

    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a(Landroid/content/Context;Landroid/content/ComponentName;Z)Z

    move-result v1

    if-nez v1, :cond_5

    .line 202
    const-string v1, "com.google.android.backup/.BackupTransportService"

    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a(Landroid/content/Context;Landroid/content/ComponentName;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 207
    :cond_5
    const/4 v0, 0x1

    goto :goto_0

    .line 208
    :catch_0
    move-exception v1

    .line 209
    sget-object v2, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v3, "Unexpected exception!"

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 210
    invoke-direct {p0}, Lcom/google/android/gms/backup/BackupTransportMigratorService;->c()V

    goto :goto_0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 216
    sget-object v0, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Rolling back migration."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 217
    new-instance v0, Landroid/app/backup/BackupManager;

    invoke-direct {v0, p0}, Landroid/app/backup/BackupManager;-><init>(Landroid/content/Context;)V

    .line 219
    sget-object v1, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Transports: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/app/backup/BackupManager;->listAllTransports()[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 223
    :try_start_0
    const-string v1, "com.google.android.backuptransport/com.google.android.backup.BackupTransportService"

    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a(Landroid/content/Context;Landroid/content/ComponentName;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 226
    const-string v1, "com.google.android.backup/.BackupTransportService"

    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a(Landroid/content/Context;Landroid/content/ComponentName;Z)Z

    .line 231
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/backup/BackupAccountNotifierService;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/backup/BackupTransportMigratorService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 233
    const-string v1, "com.google.android.backup/.BackupTransportService"

    invoke-virtual {v0}, Landroid/app/backup/BackupManager;->getCurrentTransport()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 236
    const-wide/16 v2, 0x1388

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 237
    sget-object v1, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Selecting legacy BackupTransportService."

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 238
    :cond_1
    const-string v1, "com.google.android.backup/.BackupTransportService"

    invoke-virtual {v0, v1}, Landroid/app/backup/BackupManager;->selectBackupTransport(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 243
    :cond_2
    :goto_0
    return-void

    .line 240
    :catch_0
    move-exception v0

    .line 241
    sget-object v1, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Unexpected exception!"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 107
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.gms/.backup.BackupTransportService"

    new-instance v1, Landroid/app/backup/BackupManager;

    invoke-direct {v1, p0}, Landroid/app/backup/BackupManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Landroid/app/backup/BackupManager;->getCurrentTransport()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 31
    invoke-static {p0}, Lcom/google/android/gms/backup/BackupTransportMigratorService;->f(Landroid/content/Context;)V

    return-void
.end method

.method private static e(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 140
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 163
    :goto_0
    return v0

    .line 144
    :cond_0
    sget-object v0, Lcom/google/android/gms/backup/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 145
    if-nez v0, :cond_1

    .line 146
    sget-object v0, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Gms backup is disabled by gservcie."

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    move v0, v1

    .line 147
    goto :goto_0

    .line 152
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "com.google.android.gms/.backup.BackupTransportService"

    invoke-static {v2}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getServiceInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ServiceInfo;

    move-result-object v0

    .line 155
    invoke-virtual {v0}, Landroid/content/pm/ServiceInfo;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 156
    const/4 v0, 0x1

    goto :goto_0

    .line 158
    :cond_2
    sget-object v0, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "GMS BackupTransportService not enabled!"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 159
    goto :goto_0

    .line 162
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "GMS BackupTransportService not found!"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    move v0, v1

    .line 163
    goto :goto_0
.end method

.method private static f(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 248
    new-instance v0, Lcom/google/android/gms/backup/an;

    invoke-direct {v0}, Lcom/google/android/gms/backup/an;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/backup/an;->a()Landroid/accounts/Account;

    move-result-object v0

    .line 250
    invoke-static {}, Lcom/google/android/gms/backup/BackupAccountManagerService;->a()Landroid/content/Intent;

    move-result-object v2

    .line 252
    if-nez v2, :cond_0

    .line 253
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GmsBackupAccountManagerService not found!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :cond_0
    new-instance v3, Lcom/google/android/gms/common/b;

    invoke-direct {v3}, Lcom/google/android/gms/common/b;-><init>()V

    .line 258
    const/4 v1, 0x0

    .line 260
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, p0, v2, v3, v5}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    .line 264
    if-nez v1, :cond_2

    .line 265
    sget-object v0, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Fail to bind service : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 289
    if-eqz v1, :cond_1

    .line 290
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    invoke-virtual {v0, p0, v3}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    .line 293
    :cond_1
    :goto_0
    return-void

    .line 269
    :cond_2
    :try_start_1
    invoke-virtual {v3}, Lcom/google/android/gms/common/b;->a()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/backup/al;->a(Landroid/os/IBinder;)Lcom/google/android/gms/backup/ak;

    move-result-object v2

    .line 273
    invoke-interface {v2}, Lcom/google/android/gms/backup/ak;->a()Landroid/accounts/Account;

    move-result-object v4

    .line 274
    if-eqz v4, :cond_3

    .line 275
    sget-object v0, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "GmsCore already has backup account : "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 289
    if-eqz v1, :cond_1

    .line 290
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    invoke-virtual {v0, p0, v3}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    goto :goto_0

    .line 279
    :cond_3
    if-eqz v0, :cond_4

    .line 281
    :try_start_2
    sget-object v4, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Found backup account:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 282
    invoke-interface {v2, v0}, Lcom/google/android/gms/backup/ak;->a(Landroid/accounts/Account;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 289
    :goto_1
    if-eqz v1, :cond_1

    .line 290
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    invoke-virtual {v0, p0, v3}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    goto :goto_0

    .line 285
    :cond_4
    :try_start_3
    sget-object v0, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Couldn\'t find backup account, notifying."

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 286
    invoke-static {p0}, Lcom/google/android/gms/backup/BackupAccountNotifierService;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 289
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_5

    .line 290
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    invoke-virtual {v1, p0, v3}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    :cond_5
    throw v0
.end method


# virtual methods
.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 119
    const-string v0, "rollback"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120
    invoke-direct {p0}, Lcom/google/android/gms/backup/BackupTransportMigratorService;->c()V

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/backup/BackupTransportMigratorService;->e(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 125
    sget-object v0, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Should not migrate."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 128
    :cond_2
    sget-object v0, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Migrating if not already migrated."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 129
    :cond_3
    invoke-static {p0}, Lcom/google/android/gms/backup/BackupTransportMigratorService;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    sget-object v0, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Starting migration..."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 131
    invoke-direct {p0}, Lcom/google/android/gms/backup/BackupTransportMigratorService;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 132
    sget-object v0, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Successfully migrated to use GMS BackupTransportService!"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 134
    :cond_4
    sget-object v0, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Could not migrate transport!"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
