.class public abstract Lcom/google/android/gms/auth/gsf/b;
.super Landroid/support/v4/app/q;
.source "SourceFile"


# static fields
.field static d:Z

.field protected static final e:Z

.field protected static p:Z

.field protected static q:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

.field public static r:Lcom/google/android/gms/auth/gsf/d;

.field public static s:Ljava/lang/Object;


# instance fields
.field private a:Z

.field protected f:Z

.field protected g:Z

.field protected h:Z

.field protected i:[B

.field protected j:Ljava/lang/String;

.field protected k:Ljava/lang/String;

.field protected l:Ljava/lang/String;

.field protected m:Lcom/google/android/gms/auth/gsf/h;

.field protected n:Z

.field protected o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/auth/gsf/b;->d:Z

    .line 47
    const-string v0, "GLSActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/auth/gsf/b;->e:Z

    .line 216
    new-instance v0, Lcom/google/android/gms/auth/gsf/d;

    invoke-direct {v0}, Lcom/google/android/gms/auth/gsf/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/gsf/b;->r:Lcom/google/android/gms/auth/gsf/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    .line 163
    iput-boolean v0, p0, Lcom/google/android/gms/auth/gsf/b;->f:Z

    .line 169
    iput-boolean v0, p0, Lcom/google/android/gms/auth/gsf/b;->g:Z

    .line 171
    iput-boolean v0, p0, Lcom/google/android/gms/auth/gsf/b;->h:Z

    .line 194
    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 556
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 557
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->l:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/auth/gsf/h;->b(Ljava/lang/String;)V

    .line 558
    iput-object v2, p0, Lcom/google/android/gms/auth/gsf/b;->l:Ljava/lang/String;

    .line 561
    :cond_0
    if-nez p1, :cond_1

    .line 562
    const/4 v0, 0x4

    const-string v1, "canceled"

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/gms/auth/gsf/b;->a(Landroid/os/Bundle;ILjava/lang/String;)V

    .line 567
    :goto_0
    return-void

    .line 565
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v2}, Lcom/google/android/gms/auth/gsf/b;->a(Landroid/os/Bundle;ILjava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;ILjava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x3

    .line 579
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/gsf/b;->f:Z

    .line 580
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/h;->h:Landroid/accounts/AccountAuthenticatorResponse;

    .line 581
    if-eqz v0, :cond_1

    .line 583
    if-eqz p1, :cond_3

    .line 584
    invoke-virtual {v0, p1}, Landroid/accounts/AccountAuthenticatorResponse;->onResult(Landroid/os/Bundle;)V

    .line 585
    const-string v0, "GLSActivity"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586
    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountAuthenticatorResult: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    :cond_0
    sput-object p1, Lcom/google/android/gms/auth/gsf/b;->s:Ljava/lang/Object;

    .line 596
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/auth/gsf/h;->h:Landroid/accounts/AccountAuthenticatorResponse;

    .line 598
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/h;->D:Landroid/app/PendingIntent;

    if-eqz v0, :cond_2

    .line 599
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 600
    if-eqz p1, :cond_2

    .line 601
    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 604
    :cond_2
    return-void

    .line 590
    :cond_3
    invoke-virtual {v0, p2, p3}, Landroid/accounts/AccountAuthenticatorResponse;->onError(ILjava/lang/String;)V

    .line 591
    const-string v0, "GLSActivity"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 592
    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountAuthenticatorResult: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    :cond_4
    sput-object p3, Lcom/google/android/gms/auth/gsf/b;->s:Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/a/h;
.end method

.method public final a(Landroid/content/Intent;I)V
    .locals 4

    .prologue
    .line 503
    if-nez p1, :cond_0

    .line 514
    :goto_0
    return-void

    .line 506
    :cond_0
    if-eqz p1, :cond_4

    const-string v0, "authAccount"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "authAccount"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v1, v1, Lcom/google/android/gms/auth/gsf/h;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->l:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, "GLSActivity"

    const-string v1, "Start intent without session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v0, "session"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/h;->c:Lcom/google/android/gms/auth/firstparty/shared/k;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/h;->c:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gsf/b;->a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/a/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/a/h;->b(Landroid/content/Intent;)V

    :cond_3
    const-string v0, "is_new_account"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v1, v1, Lcom/google/android/gms/auth/gsf/h;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "useImmersiveMode"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v1, v1, Lcom/google/android/gms/auth/gsf/h;->ai:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "firstRun"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v1, v1, Lcom/google/android/gms/auth/gsf/h;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "account_name"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v1, v1, Lcom/google/android/gms/auth/gsf/h;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "calling_app_description"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v1, v1, Lcom/google/android/gms/auth/gsf/h;->ac:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "service"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "confirmCredentials"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/gsf/b;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "addAccount"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/gsf/b;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "hasMultipleUsers"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/gsf/b;->n:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "theme"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 507
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    .line 508
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/b;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 510
    sget-object v0, Lcom/google/android/gms/auth/gsf/b;->q:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    iget-object v0, v0, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->d:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->b(Landroid/content/Intent;)V

    .line 512
    :cond_5
    sget-object v0, Lcom/google/android/gms/auth/gsf/b;->q:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    const-string v1, "GLSActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Starting for result: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/auth/gsf/b;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 620
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 621
    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    const-string v1, "accountType"

    invoke-static {p1}, Lcom/google/android/gms/common/util/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/gsf/b;->a(Landroid/os/Bundle;)V

    .line 624
    return-void
.end method

.method protected final c()V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 633
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 634
    const-string v1, "setupSkipped"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 635
    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/gsf/b;->a(Landroid/os/Bundle;)V

    .line 636
    return-void
.end method

.method public finish()V
    .locals 3

    .prologue
    .line 668
    invoke-super {p0}, Landroid/support/v4/app/q;->finish()V

    .line 670
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 671
    const/high16 v0, 0x10a0000

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/gsf/b;->overridePendingTransition(II)V

    .line 674
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    if-eqz v0, :cond_1

    .line 675
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/h;->F:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 679
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/auth/gsf/b;->f:Z

    if-eqz v0, :cond_3

    .line 682
    const-string v0, "GLSActivity"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 683
    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountAuthenticatorResult: finish on "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 685
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/gsf/b;->a(Landroid/os/Bundle;)V

    .line 687
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/h;->F:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 688
    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Remaining GLS activities after end: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v2, v2, Lcom/google/android/gms/auth/gsf/h;->F:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 691
    :cond_3
    sget-object v0, Lcom/google/android/gms/auth/gsf/b;->q:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    const-string v1, "GLSActivity"

    const-string v2, "finished!"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const v9, 0x106000d

    const/16 v8, 0x15

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 259
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/b;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 260
    if-nez p1, :cond_b

    .line 263
    const/4 v0, 0x0

    .line 264
    if-eqz v4, :cond_0

    .line 265
    invoke-static {v4}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->a(Landroid/content/Intent;)Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    move-result-object v0

    .line 267
    :cond_0
    if-nez v0, :cond_1

    .line 269
    const-string v0, "session"

    invoke-static {v0}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    move-result-object v0

    .line 272
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/gsf/b;->q:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    .line 277
    :goto_0
    if-nez p1, :cond_c

    const-string v0, "Intent"

    .line 278
    :goto_1
    sget-object v3, Lcom/google/android/gms/auth/gsf/b;->q:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    const-string v5, "GLSActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "onCreate() with "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 281
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    const/4 v3, 0x3

    if-lt v0, v3, :cond_d

    move v0, v1

    :goto_2
    sput-boolean v0, Lcom/google/android/gms/auth/gsf/b;->p:Z

    .line 283
    if-eqz p1, :cond_10

    .line 288
    const-string v0, "session"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 289
    if-eqz v0, :cond_e

    .line 297
    invoke-static {v0}, Lcom/google/android/gms/auth/gsf/h;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/gsf/h;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    .line 298
    iget-object v3, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    if-nez v3, :cond_f

    .line 299
    invoke-static {v0, p1}, Lcom/google/android/gms/auth/gsf/h;->a(Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/auth/gsf/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    move-object v3, p1

    .line 319
    :cond_2
    :goto_3
    const-string v0, "isTop"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/gsf/b;->f:Z

    .line 322
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/h;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->l:Ljava/lang/String;

    .line 325
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/h;->F:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 328
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/h;->a:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 330
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    const-string v4, "authAccount"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/google/android/gms/auth/gsf/h;->a:Ljava/lang/String;

    .line 334
    :cond_3
    sget-object v0, Lcom/google/android/gms/auth/firstparty/shared/k;->T:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 335
    iget-object v4, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v4, v4, Lcom/google/android/gms/auth/gsf/h;->c:Lcom/google/android/gms/auth/firstparty/shared/k;

    if-nez v4, :cond_4

    if-eqz v0, :cond_4

    .line 336
    iget-object v4, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    invoke-static {v0}, Lcom/google/android/gms/auth/a/h;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/k;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/auth/gsf/h;->c:Lcom/google/android/gms/auth/firstparty/shared/k;

    .line 339
    :cond_4
    const-string v0, "service"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->k:Ljava/lang/String;

    .line 341
    const-string v0, "confirmCredentials"

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/gsf/b;->g:Z

    .line 342
    const-string v0, "addAccount"

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/gsf/b;->h:Z

    .line 343
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/h;->h:Landroid/accounts/AccountAuthenticatorResponse;

    if-eqz v0, :cond_5

    .line 344
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/h;->h:Landroid/accounts/AccountAuthenticatorResponse;

    invoke-virtual {v0}, Landroid/accounts/AccountAuthenticatorResponse;->onRequestContinued()V

    .line 346
    :cond_5
    const-string v0, "CaptchaData"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->i:[B

    .line 347
    const-string v0, "CaptchaToken"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->j:Ljava/lang/String;

    .line 348
    const-string v0, "showingProgressDialog"

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/gsf/b;->a:Z

    .line 349
    const-string v0, "hasMultipleUsers"

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/gsf/b;->n:Z

    .line 350
    const-string v0, "theme"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->o:Ljava/lang/String;

    .line 351
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->o:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 356
    invoke-static {v8}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 357
    const-string v0, "material_light"

    iput-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->o:Ljava/lang/String;

    .line 364
    :cond_6
    :goto_4
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/h;->C:Landroid/os/Bundle;

    const-string v4, "allowSkip"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "allowSkip"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 367
    const-string v0, "GLSActivity"

    const-string v4, "Accepting legacy allowSkip from intent"

    invoke-static {v0, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/h;->C:Landroid/os/Bundle;

    const-string v4, "allowSkip"

    const-string v5, "allowSkip"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 372
    :cond_7
    const-string v0, "firstRun"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 373
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    const-string v4, "firstRun"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, v0, Lcom/google/android/gms/auth/gsf/h;->l:Z

    .line 374
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/gsf/h;->l:Z

    if-eqz v0, :cond_8

    .line 375
    iput-boolean v1, p0, Lcom/google/android/gms/auth/gsf/b;->h:Z

    .line 381
    :cond_8
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 382
    const-string v1, "callerExtras"

    iget-object v4, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v4, v4, Lcom/google/android/gms/auth/gsf/h;->C:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 383
    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/auth/gsf/b;->setResult(ILandroid/content/Intent;)V

    .line 386
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    const-string v1, "useImmersiveMode"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/gms/auth/gsf/h;->ai:Z

    .line 388
    invoke-static {v8}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/gsf/h;->l:Z

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/gsf/h;->ai:Z

    if-eqz v0, :cond_a

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/b;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/auth/gsf/c;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/auth/gsf/c;-><init>(Lcom/google/android/gms/auth/gsf/b;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v1

    or-int/lit16 v1, v1, 0x1602

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/b;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, -0x80000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/b;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/Window;->setStatusBarColor(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/b;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/Window;->setNavigationBarColor(I)V

    .line 389
    :cond_a
    return-void

    .line 275
    :cond_b
    invoke-static {p1}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/gsf/b;->q:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    goto/16 :goto_0

    .line 277
    :cond_c
    const-string v0, "Icicle"

    goto/16 :goto_1

    :cond_d
    move v0, v2

    .line 281
    goto/16 :goto_2

    .line 302
    :cond_e
    invoke-static {p1}, Lcom/google/android/gms/auth/gsf/h;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/gsf/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    :cond_f
    move-object v3, p1

    .line 304
    goto/16 :goto_3

    .line 306
    :cond_10
    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 307
    if-nez v0, :cond_17

    .line 308
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    move-object v3, v0

    .line 310
    :goto_5
    const-string v0, "notificationId"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_13

    move v0, v2

    :goto_6
    if-nez v0, :cond_a

    .line 314
    invoke-static {v3}, Lcom/google/android/gms/auth/gsf/h;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/gsf/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    .line 315
    const-string v0, "accountAuthenticatorResponse"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/h;->h:Landroid/accounts/AccountAuthenticatorResponse;

    if-nez v0, :cond_11

    iget-object v4, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    const-string v0, "accountAuthenticatorResponse"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountAuthenticatorResponse;

    iput-object v0, v4, Lcom/google/android/gms/auth/gsf/h;->h:Landroid/accounts/AccountAuthenticatorResponse;

    :cond_11
    const-string v0, "pendingIntent"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/h;->D:Landroid/app/PendingIntent;

    if-nez v0, :cond_12

    iget-object v4, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    const-string v0, "pendingIntent"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iput-object v0, v4, Lcom/google/android/gms/auth/gsf/h;->D:Landroid/app/PendingIntent;

    :cond_12
    const-string v0, "hasAccountManagerOptions"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/h;->C:Landroid/os/Bundle;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iput-object v3, v0, Lcom/google/android/gms/auth/gsf/h;->C:Landroid/os/Bundle;

    goto/16 :goto_3

    .line 310
    :cond_13
    const-string v0, "session"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_14

    invoke-static {v0}, Lcom/google/android/gms/auth/gsf/h;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/gsf/h;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v4, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    if-eqz v4, :cond_14

    move v0, v2

    goto :goto_6

    :cond_14
    const-string v4, "GLSActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Notification without session "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "-"

    invoke-virtual {v5, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_15

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gsf/b;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v5, v1}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    :goto_7
    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/gsf/b;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/b;->finish()V

    move v0, v1

    goto/16 :goto_6

    :cond_15
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gsf/b;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const/4 v4, 0x2

    invoke-virtual {v0, v5, v4}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    goto :goto_7

    .line 359
    :cond_16
    const-string v0, "holo"

    iput-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->o:Ljava/lang/String;

    goto/16 :goto_4

    :cond_17
    move-object v3, v0

    goto/16 :goto_5
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 393
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 394
    const-string v0, "CaptchaData"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->i:[B

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 395
    const-string v0, "CaptchaToken"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    const-string v0, "showingProgressDialog"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/gsf/b;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 397
    const-string v0, "isTop"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/gsf/b;->f:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 398
    const-string v0, "hasMultipleUsers"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/gsf/b;->n:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 399
    const-string v0, "theme"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    const-string v0, "authAccount"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v1, v1, Lcom/google/android/gms/auth/gsf/h;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->l:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "GLSActivity"

    const-string v1, "Start intent without session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "session"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "is_new_account"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v1, v1, Lcom/google/android/gms/auth/gsf/h;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "useImmersiveMode"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v1, v1, Lcom/google/android/gms/auth/gsf/h;->ai:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "firstRun"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v1, v1, Lcom/google/android/gms/auth/gsf/h;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "account_name"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v1, v1, Lcom/google/android/gms/auth/gsf/h;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "calling_app_description"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v1, v1, Lcom/google/android/gms/auth/gsf/h;->ac:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "service"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "confirmCredentials"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/gsf/b;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "addAccount"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/gsf/b;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "hasMultipleUsers"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/gsf/b;->n:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "theme"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/b;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/b;->m:Lcom/google/android/gms/auth/gsf/h;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/gsf/h;->b(Landroid/os/Bundle;)V

    .line 406
    sget-object v0, Lcom/google/android/gms/auth/gsf/b;->q:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->b(Landroid/os/Bundle;)V

    .line 407
    sget-object v0, Lcom/google/android/gms/auth/gsf/b;->q:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    const-string v1, "GLSActivity"

    const-string v2, "onSaveInstanceState()!"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 652
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->startActivity(Landroid/content/Intent;)V

    .line 653
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 654
    const/high16 v0, 0x10a0000

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/gsf/b;->overridePendingTransition(II)V

    .line 656
    :cond_0
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 660
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/q;->startActivityForResult(Landroid/content/Intent;I)V

    .line 661
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 662
    const/high16 v0, 0x10a0000

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/gsf/b;->overridePendingTransition(II)V

    .line 664
    :cond_0
    return-void
.end method
