.class public final Lcom/google/android/gms/app/receiver/GetRestrictionsReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 22
    new-instance v0, Landroid/content/RestrictionEntry;

    const-string v1, "restricted_profile"

    const-string v2, "true"

    invoke-direct {v0, v1, v2}, Landroid/content/RestrictionEntry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/RestrictionEntry;->setType(I)V

    .line 26
    const-string v1, "Restricted profile"

    invoke-virtual {v0, v1}, Landroid/content/RestrictionEntry;->setTitle(Ljava/lang/String;)V

    .line 28
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 29
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 31
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 32
    const-string v2, "android.intent.extra.restrictions_list"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 33
    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/gms/app/receiver/GetRestrictionsReceiver;->setResult(ILjava/lang/String;Landroid/os/Bundle;)V

    .line 34
    return-void
.end method
