.class public Lcom/google/android/gms/kids/restrictions/receiver/ManualSyncReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 25
    invoke-static {p1}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 42
    :cond_0
    :goto_0
    return-void

    .line 30
    :cond_1
    invoke-static {}, Lcom/google/android/gms/kids/common/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    const-string v0, "ManualSyncReceiver"

    const-string v1, "onReceive: action=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    invoke-static {p1}, Lcom/google/android/gms/kids/common/d;->a(Landroid/content/Context;)Lcom/google/android/gms/kids/account/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/kids/account/b;->a()Landroid/accounts/Account;

    move-result-object v0

    .line 37
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 38
    invoke-static {}, Lcom/google/android/gms/kids/common/d;->f()Ljava/util/List;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/sync/KidsDataSyncService;->a(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/util/List;)Z

    goto :goto_0
.end method
