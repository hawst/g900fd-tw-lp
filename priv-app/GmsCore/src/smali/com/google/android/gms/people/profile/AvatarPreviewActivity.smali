.class public Lcom/google/android/gms/people/profile/AvatarPreviewActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/net/Uri;

.field private b:Lcom/google/android/gms/people/profile/AvatarView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 93
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->setResult(I)V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->finish()V

    .line 95
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 63
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 64
    sget v1, Lcom/google/android/gms/j;->cs:I

    if-ne v0, v1, :cond_1

    .line 65
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->setResult(I)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->finish()V

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    sget v1, Lcom/google/android/gms/j;->d:I

    if-ne v0, v1, :cond_0

    .line 68
    const-string v0, "cropped-avatar.jpg"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/people/profile/f;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v0

    .line 70
    if-nez v0, :cond_2

    .line 71
    const-string v0, "People.Avatar"

    const-string v1, "Failed to get temp file to crop photo"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    sget v0, Lcom/google/android/gms/p;->rH:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->a(I)V

    goto :goto_0

    .line 77
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->b:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/profile/AvatarView;->a(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 85
    const-string v2, "com.google.android.gms.people.profile.EXTRA_URI"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 86
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->setResult(ILandroid/content/Intent;)V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->finish()V

    goto :goto_0

    .line 78
    :catch_0
    move-exception v0

    .line 79
    const-string v1, "People.Avatar"

    const-string v2, "Failed to write cropped photo"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 80
    sget v0, Lcom/google/android/gms/p;->rH:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->a(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 33
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 35
    if-nez p1, :cond_0

    .line 36
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.android.gms.people.profile.EXTRA_URI"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->a:Landroid/net/Uri;

    .line 41
    :goto_0
    sget v0, Lcom/google/android/gms/l;->I:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->setContentView(I)V

    .line 42
    sget v0, Lcom/google/android/gms/j;->bL:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/profile/AvatarView;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->b:Lcom/google/android/gms/people/profile/AvatarView;

    .line 44
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->b:Lcom/google/android/gms/people/profile/AvatarView;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/profile/AvatarView;->b(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->b:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/gms/people/profile/AvatarView;->a()V

    .line 51
    sget v0, Lcom/google/android/gms/j;->cs:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    sget v0, Lcom/google/android/gms/j;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    :goto_1
    return-void

    .line 38
    :cond_0
    const-string v0, "photo_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->a:Landroid/net/Uri;

    goto :goto_0

    .line 45
    :catch_0
    move-exception v0

    .line 46
    const-string v1, "People.Avatar"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to initialize AvatarView: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    sget v0, Lcom/google/android/gms/p;->rH:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->a(I)V

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 58
    const-string v0, "photo_uri"

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 59
    return-void
.end method
