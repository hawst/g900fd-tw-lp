.class public final Lcom/google/android/gms/drive/e/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/model/ah;

.field private final b:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/drive/e/a;->a:Lcom/google/android/gms/drive/database/model/ah;

    .line 28
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->m:Lcom/google/android/gms/drive/metadata/internal/a/d;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 29
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->m:Lcom/google/android/gms/drive/metadata/internal/a/d;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 31
    iget-object v1, p0, Lcom/google/android/gms/drive/e/a;->a:Lcom/google/android/gms/drive/database/model/ah;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ah;->aj()Z

    move-result v1

    if-eq v0, v1, :cond_2

    .line 32
    if-eqz v0, :cond_1

    .line 33
    iget-object v0, p0, Lcom/google/android/gms/drive/e/a;->a:Lcom/google/android/gms/drive/database/model/ah;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    iput v4, p0, Lcom/google/android/gms/drive/e/a;->b:I

    .line 50
    :goto_0
    return-void

    .line 36
    :cond_0
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0xa

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Pinning is not enabled for this document: %s"

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/gms/drive/e/a;->a:Lcom/google/android/gms/drive/database/model/ah;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v6}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 42
    :cond_1
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/drive/e/a;->b:I

    goto :goto_0

    .line 45
    :cond_2
    iput v6, p0, Lcom/google/android/gms/drive/e/a;->b:I

    goto :goto_0

    .line 48
    :cond_3
    iput v6, p0, Lcom/google/android/gms/drive/e/a;->b:I

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/e/b;Lcom/google/android/gms/drive/b/d;)V
    .locals 3

    .prologue
    .line 54
    iget v0, p0, Lcom/google/android/gms/drive/e/a;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 55
    invoke-virtual {p1}, Lcom/google/android/gms/drive/e/b;->b()Ljava/util/concurrent/Future;

    .line 60
    :cond_0
    :goto_0
    return-void

    .line 56
    :cond_1
    iget v0, p0, Lcom/google/android/gms/drive/e/a;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/drive/e/a;->a:Lcom/google/android/gms/drive/database/model/ah;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/drive/e/b;->e:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v2, p1, Lcom/google/android/gms/drive/e/b;->e:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/e/d;

    if-eqz v0, :cond_2

    invoke-static {v0}, Lcom/google/android/gms/drive/e/d;->a(Lcom/google/android/gms/drive/e/d;)Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {v0}, Lcom/google/android/gms/drive/e/d;->a(Lcom/google/android/gms/drive/e/d;)Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;->a()V

    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    invoke-virtual {p2}, Lcom/google/android/gms/drive/b/d;->b()V

    goto :goto_0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
