.class public Lcom/google/android/gms/playlog/service/MonitorService;
.super Landroid/app/IntentService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    const-string v0, "MonitorService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 30
    return-void
.end method

.method private a(JJLjava/lang/String;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 57
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-nez v0, :cond_0

    .line 64
    :goto_0
    return-void

    .line 60
    :cond_0
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/playlog/service/MonitorService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 61
    invoke-static {p0, p5}, Lcom/google/android/gms/playlog/service/MonitorAlarmReceiver;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 62
    const/4 v2, 0x1

    add-long v4, p1, p3

    invoke-static {p0, v3, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v2, v4, v5, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 10

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/gms/playlog/a/c;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 36
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    .line 37
    invoke-static {v8}, Lcom/google/android/gms/common/util/au;->d(Ljava/lang/String;)Z

    move-result v9

    .line 39
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 41
    if-nez v9, :cond_0

    const-string v0, "com.google.android.gms.playlog.service.UPLOAD_DEFAULT"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 42
    :cond_0
    if-eqz v7, :cond_1

    const-string v0, "MonitorService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "UPLOAD_DEFAULT action at "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    :cond_1
    invoke-static {}, Lcom/google/android/gms/common/analytics/f;->e()V

    .line 44
    sget-object v0, Lcom/google/android/gms/common/a/c;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-string v6, "com.google.android.gms.playlog.service.UPLOAD_DEFAULT"

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/playlog/service/MonitorService;->a(JJLjava/lang/String;)V

    .line 47
    :cond_2
    if-nez v9, :cond_3

    const-string v0, "com.google.android.gms.playlog.service.UPLOAD_INTERNAL_STATS"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 48
    :cond_3
    if-eqz v7, :cond_4

    const-string v0, "MonitorService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "UPLOAD_INTERNAL_STATS action at "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    :cond_4
    invoke-static {}, Lcom/google/android/gms/common/analytics/f;->c()V

    .line 50
    sget-object v0, Lcom/google/android/gms/common/a/c;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-string v6, "com.google.android.gms.playlog.service.UPLOAD_INTERNAL_STATS"

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/playlog/service/MonitorService;->a(JJLjava/lang/String;)V

    .line 53
    :cond_5
    return-void
.end method
