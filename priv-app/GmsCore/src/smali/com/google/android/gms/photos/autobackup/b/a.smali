.class public final Lcom/google/android/gms/photos/autobackup/b/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/service/b;


# instance fields
.field private final a:Lcom/google/android/gms/photos/autobackup/a/ac;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/b/a;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    .line 25
    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/b/a;->b:Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/b/a;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(ILcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;)V

    .line 42
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 13

    .prologue
    const/4 v7, -0x1

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 17
    check-cast p1, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/b/b;

    invoke-direct {v1, p1}, Lcom/google/android/gms/photos/autobackup/b/b;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/b/a;->b:Ljava/lang/String;

    iget-object v5, v1, Lcom/google/android/gms/photos/autobackup/b/b;->b:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v5, v0}, Lcom/google/android/libraries/social/account/b;->b(Ljava/lang/String;)I

    move-result v5

    if-eq v5, v7, :cond_0

    iget-object v6, v1, Lcom/google/android/gms/photos/autobackup/b/b;->d:Lcom/google/android/libraries/social/autobackup/o;

    invoke-virtual {v6}, Lcom/google/android/libraries/social/autobackup/o;->d()I

    move-result v6

    if-ne v6, v7, :cond_1

    :cond_0
    new-instance v2, Lcom/google/android/gms/photos/autobackup/model/c;

    invoke-direct {v2}, Lcom/google/android/gms/photos/autobackup/model/c;-><init>()V

    iput-object v0, v2, Lcom/google/android/gms/photos/autobackup/model/c;->b:Ljava/lang/String;

    iput v3, v2, Lcom/google/android/gms/photos/autobackup/model/c;->a:I

    iget-object v0, v1, Lcom/google/android/gms/photos/autobackup/b/b;->d:Lcom/google/android/libraries/social/autobackup/o;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/o;->d()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/photos/autobackup/b/b;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/photos/autobackup/model/c;->i:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/photos/autobackup/model/c;->a()Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_13

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/b/a;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    invoke-interface {v1, v3, v0}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(ILcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;)V

    :goto_1
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/libraries/social/autobackup/aq;

    iget-object v6, v1, Lcom/google/android/gms/photos/autobackup/b/b;->a:Landroid/content/Context;

    invoke-direct {v0, v6, v5}, Lcom/google/android/libraries/social/autobackup/aq;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/aq;->m()Lcom/google/android/libraries/social/autobackup/as;

    move-result-object v6

    if-nez v6, :cond_2

    move-object v0, v4

    goto :goto_0

    :cond_2
    iget-object v7, v6, Lcom/google/android/libraries/social/autobackup/as;->n:Ljava/lang/String;

    iget-object v0, v6, Lcom/google/android/libraries/social/autobackup/as;->b:Ljava/util/Map;

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    if-nez v0, :cond_7

    const/4 v0, 0x0

    :goto_2
    iget v5, v6, Lcom/google/android/libraries/social/autobackup/as;->d:I

    invoke-virtual {v1, v5}, Lcom/google/android/gms/photos/autobackup/b/b;->a(I)Ljava/lang/String;

    move-result-object v5

    iget-object v8, v1, Lcom/google/android/gms/photos/autobackup/b/b;->d:Lcom/google/android/libraries/social/autobackup/o;

    invoke-virtual {v8}, Lcom/google/android/libraries/social/autobackup/o;->d()I

    move-result v8

    invoke-virtual {v1, v8}, Lcom/google/android/gms/photos/autobackup/b/b;->a(I)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/google/android/gms/photos/autobackup/model/c;

    invoke-direct {v9}, Lcom/google/android/gms/photos/autobackup/model/c;-><init>()V

    iput-object v5, v9, Lcom/google/android/gms/photos/autobackup/model/c;->b:Ljava/lang/String;

    iget-object v5, v1, Lcom/google/android/gms/photos/autobackup/b/b;->c:Lcom/google/android/libraries/social/networkcapability/a;

    invoke-interface {v5}, Lcom/google/android/libraries/social/networkcapability/a;->a()Z

    move-result v5

    iget-object v10, v1, Lcom/google/android/gms/photos/autobackup/b/b;->c:Lcom/google/android/libraries/social/networkcapability/a;

    invoke-interface {v10}, Lcom/google/android/libraries/social/networkcapability/a;->b()Z

    move-result v10

    iget-object v1, v1, Lcom/google/android/gms/photos/autobackup/b/b;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/libraries/social/autobackup/ap;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/ap;

    move-result-object v11

    iget v1, v6, Lcom/google/android/libraries/social/autobackup/as;->h:I

    if-nez v1, :cond_8

    if-nez v5, :cond_8

    iget v1, v6, Lcom/google/android/libraries/social/autobackup/as;->k:I

    if-lez v1, :cond_8

    invoke-interface {v11}, Lcom/google/android/libraries/social/autobackup/ao;->c()Z

    move-result v1

    if-nez v1, :cond_8

    move v1, v2

    :goto_3
    iget v12, v6, Lcom/google/android/libraries/social/autobackup/as;->h:I

    if-nez v12, :cond_9

    if-nez v5, :cond_9

    iget v5, v6, Lcom/google/android/libraries/social/autobackup/as;->l:I

    if-lez v5, :cond_9

    invoke-interface {v11}, Lcom/google/android/libraries/social/autobackup/ao;->b()Z

    move-result v5

    if-nez v5, :cond_9

    move v5, v2

    :goto_4
    if-nez v1, :cond_3

    if-eqz v5, :cond_a

    :cond_3
    move v5, v2

    :goto_5
    if-nez v5, :cond_b

    if-nez v10, :cond_b

    iget v1, v6, Lcom/google/android/libraries/social/autobackup/as;->h:I

    if-nez v1, :cond_b

    iget v1, v6, Lcom/google/android/libraries/social/autobackup/as;->k:I

    if-lez v1, :cond_4

    invoke-interface {v11}, Lcom/google/android/libraries/social/autobackup/ao;->c()Z

    move-result v1

    if-nez v1, :cond_5

    :cond_4
    iget v1, v6, Lcom/google/android/libraries/social/autobackup/as;->l:I

    if-lez v1, :cond_b

    invoke-interface {v11}, Lcom/google/android/libraries/social/autobackup/ao;->b()Z

    move-result v1

    if-eqz v1, :cond_b

    :cond_5
    move v1, v2

    :goto_6
    iget-boolean v10, v6, Lcom/google/android/libraries/social/autobackup/as;->e:Z

    if-nez v10, :cond_c

    move v2, v3

    :cond_6
    :goto_7
    iput v2, v9, Lcom/google/android/gms/photos/autobackup/model/c;->a:I

    iput-object v7, v9, Lcom/google/android/gms/photos/autobackup/model/c;->c:Ljava/lang/String;

    iput v0, v9, Lcom/google/android/gms/photos/autobackup/model/c;->d:F

    iget v0, v6, Lcom/google/android/libraries/social/autobackup/as;->j:I

    iput v0, v9, Lcom/google/android/gms/photos/autobackup/model/c;->e:I

    iget v0, v6, Lcom/google/android/libraries/social/autobackup/as;->i:I

    iput v0, v9, Lcom/google/android/gms/photos/autobackup/model/c;->g:I

    iget v0, v6, Lcom/google/android/libraries/social/autobackup/as;->g:I

    iput v0, v9, Lcom/google/android/gms/photos/autobackup/model/c;->f:I

    iget v0, v6, Lcom/google/android/libraries/social/autobackup/as;->i:I

    if-nez v0, :cond_10

    move-object v0, v4

    :goto_8
    iput-object v0, v9, Lcom/google/android/gms/photos/autobackup/model/c;->h:[Ljava/lang/String;

    iput-object v8, v9, Lcom/google/android/gms/photos/autobackup/model/c;->i:Ljava/lang/String;

    invoke-virtual {v9}, Lcom/google/android/gms/photos/autobackup/model/c;->a()Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto/16 :goto_2

    :cond_8
    move v1, v3

    goto :goto_3

    :cond_9
    move v5, v3

    goto :goto_4

    :cond_a
    move v5, v3

    goto :goto_5

    :cond_b
    move v1, v3

    goto :goto_6

    :cond_c
    if-eqz v5, :cond_d

    const/4 v2, 0x3

    goto :goto_7

    :cond_d
    if-eqz v1, :cond_e

    const/4 v2, 0x4

    goto :goto_7

    :cond_e
    iget v1, v6, Lcom/google/android/libraries/social/autobackup/as;->h:I

    if-gtz v1, :cond_f

    iget v1, v6, Lcom/google/android/libraries/social/autobackup/as;->g:I

    if-lez v1, :cond_6

    :cond_f
    const/4 v2, 0x2

    goto :goto_7

    :cond_10
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v6, Lcom/google/android/libraries/social/autobackup/as;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/social/autobackup/at;->f:Lcom/google/android/libraries/social/autobackup/at;

    if-ne v5, v6, :cond_11

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/16 v5, 0xa

    if-lt v0, v5, :cond_11

    :cond_12
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_8

    :cond_13
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/b/a;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v4}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(ILcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;)V

    goto/16 :goto_1
.end method
