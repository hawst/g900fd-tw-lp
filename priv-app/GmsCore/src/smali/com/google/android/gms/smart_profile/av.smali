.class final Lcom/google/android/gms/smart_profile/av;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/google/android/gms/smart_profile/SmartProfileActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/smart_profile/SmartProfileActivity;Z)V
    .locals 0

    .prologue
    .line 801
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/av;->b:Lcom/google/android/gms/smart_profile/SmartProfileActivity;

    iput-boolean p2, p0, Lcom/google/android/gms/smart_profile/av;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/ap;)V
    .locals 4

    .prologue
    .line 804
    invoke-interface {p1}, Lcom/google/android/gms/common/api/ap;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 805
    const-string v0, "SmartProfile"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed star operation with status "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gms/common/api/ap;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/bc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    :goto_0
    return-void

    .line 809
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/av;->b:Lcom/google/android/gms/smart_profile/SmartProfileActivity;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->a(Lcom/google/android/gms/smart_profile/SmartProfileActivity;)Lcom/google/android/gms/smart_profile/bg;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/smart_profile/av;->a:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/bg;->b(Z)V

    .line 810
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/av;->b:Lcom/google/android/gms/smart_profile/SmartProfileActivity;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b(Lcom/google/android/gms/smart_profile/SmartProfileActivity;)V

    .line 813
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/av;->b:Lcom/google/android/gms/smart_profile/SmartProfileActivity;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->a(Lcom/google/android/gms/smart_profile/SmartProfileActivity;)Lcom/google/android/gms/smart_profile/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->c()Lcom/google/android/gms/smart_profile/ak;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/av;->b:Lcom/google/android/gms/smart_profile/SmartProfileActivity;

    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/av;->a:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/smart_profile/b;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_1
    sget-object v3, Lcom/google/android/gms/smart_profile/c;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/gms/smart_profile/b;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_1
.end method
