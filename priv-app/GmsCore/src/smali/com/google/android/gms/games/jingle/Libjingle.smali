.class public Lcom/google/android/gms/games/jingle/Libjingle;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final ANDROID_SETTINGS_KEYS_MAP:[[Ljava/lang/String;

.field public static FAILURE_OPS_ID:I = 0x0

.field private static final TAG:Ljava/lang/String; = "games_rtmp:Libjingle"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mInitialized:Z

.field private mNativeContext:J


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 47
    const-string v0, "gmscore"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 48
    const-string v0, "games_rtmp_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 49
    invoke-static {}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeInit()V

    .line 59
    invoke-static {v7}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeSetLoggingLevel(I)V

    .line 73
    const/16 v0, 0x9

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "gtalk_vc_xmpp_connect_ojid"

    aput-object v2, v1, v4

    const-string v2, "XMPP_CONNECT_OJID"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "gtalk_vc_xmpp_hostname"

    aput-object v2, v1, v4

    const-string v2, "XMPP_HOSTNAME"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "gtalk_vc_xmpp_port"

    aput-object v2, v1, v4

    const-string v2, "XMPP_PORT"

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    const/4 v1, 0x3

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "games.notification_channel"

    aput-object v3, v2, v4

    const-string v3, "GAMES_NOTIFICATION_CHANNEL"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "games.notification_jid"

    aput-object v3, v2, v4

    const-string v3, "GAMES_NOTIFICATION_JID"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "games.buzzbot_channel"

    aput-object v2, v1, v4

    const-string v2, "GAMES_BUZZBOT_CHANNEL"

    aput-object v2, v1, v5

    aput-object v1, v0, v7

    const/4 v1, 0x6

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "games.buzzbot_jid"

    aput-object v3, v2, v4

    const-string v3, "GAMES_BUZZBOT_JID"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "games.use_default_networks_only"

    aput-object v3, v2, v4

    const-string v3, "USE_DEFAULT_NETWORKS_ONLY"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "games.force_add_ipv4_default_address"

    aput-object v3, v2, v4

    const-string v3, "FORCE_ADD_IPV4_DEFAULT_ADDRESS"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/jingle/Libjingle;->ANDROID_SETTINGS_KEYS_MAP:[[Ljava/lang/String;

    .line 303
    const/4 v0, -0x1

    sput v0, Lcom/google/android/gms/games/jingle/Libjingle;->FAILURE_OPS_ID:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput-object p1, p0, Lcom/google/android/gms/games/jingle/Libjingle;->mContext:Landroid/content/Context;

    .line 95
    iput-object p2, p0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    .line 96
    return-void
.end method

.method private static dispatchNativeEvent(Ljava/lang/Object;IIILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 562
    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/gms/games/jingle/Libjingle;

    .line 563
    invoke-static {v6}, Lcom/google/android/gms/games/jingle/Libjingle;->instanceOk(Lcom/google/android/gms/games/jingle/Libjingle;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564
    packed-switch p1, :pswitch_data_0

    .line 584
    const-string v0, "games_rtmp:Libjingle"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown message: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    :cond_0
    :goto_0
    return-void

    :pswitch_0
    move-object v1, p4

    .line 566
    check-cast v1, Ljava/lang/String;

    move-object v2, p5

    .line 567
    check-cast v2, Ljava/lang/String;

    move-object v3, p6

    .line 568
    check-cast v3, Ljava/lang/String;

    .line 569
    new-instance v0, Lcom/google/android/gms/games/jingle/d;

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/jingle/d;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-static {v6, v0}, Lcom/google/android/gms/games/jingle/Libjingle;->sendMessage(Lcom/google/android/gms/games/jingle/Libjingle;Lcom/google/android/gms/games/jingle/h;)V

    goto :goto_0

    .line 576
    :pswitch_1
    if-ne p2, v1, :cond_1

    move v0, v1

    .line 577
    :goto_1
    check-cast p4, Ljava/lang/String;

    .line 578
    new-instance v1, Lcom/google/android/gms/games/jingle/k;

    invoke-direct {v1, v0, p4, p3}, Lcom/google/android/gms/games/jingle/k;-><init>(ZLjava/lang/String;I)V

    invoke-static {v6, v1}, Lcom/google/android/gms/games/jingle/Libjingle;->sendMessage(Lcom/google/android/gms/games/jingle/Libjingle;Lcom/google/android/gms/games/jingle/h;)V

    goto :goto_0

    .line 576
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 564
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static instanceOk(Lcom/google/android/gms/games/jingle/Libjingle;)Z
    .locals 4

    .prologue
    .line 672
    if-eqz p0, :cond_0

    iget-wide v0, p0, Lcom/google/android/gms/games/jingle/Libjingle;->mNativeContext:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 673
    const/4 v0, 0x1

    .line 676
    :goto_0
    return v0

    .line 675
    :cond_0
    const-string v0, "games_rtmp:Libjingle"

    const-string v1, "Received invalid Libjingle instance on callback."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 681
    const-string v0, "games_rtmp:Libjingle"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 682
    return-void
.end method

.method private static logTagToInt(Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x4

    const/4 v1, 0x3

    const/4 v0, 0x2

    .line 99
    invoke-static {p0, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 111
    :goto_0
    return v0

    .line 102
    :cond_0
    invoke-static {p0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 103
    goto :goto_0

    .line 105
    :cond_1
    invoke-static {p0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 106
    goto :goto_0

    .line 108
    :cond_2
    invoke-static {p0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v3

    .line 109
    goto :goto_0

    .line 111
    :cond_3
    const/4 v0, 0x6

    goto :goto_0
.end method

.method private final native nativeAcceptCall(Ljava/lang/String;)V
.end method

.method private final native nativeCall(Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method private final native nativeConnectAndSignin(Ljava/lang/String;Z)V
.end method

.method private final native nativeCreateSocketConnection(Ljava/lang/String;)I
.end method

.method private final native nativeDeclineCall(Ljava/lang/String;)V
.end method

.method private final native nativeDisconnectAndSignout(Ljava/lang/String;)V
.end method

.method private final native nativeEndCall(Ljava/lang/String;)V
.end method

.method private final native nativeFinalize()V
.end method

.method private final native nativeGetDebugString()Ljava/lang/String;
.end method

.method private final native nativeGetPeerDiagnosticMetrics(Ljava/lang/Object;Ljava/lang/String;)V
.end method

.method private static native nativeInit()V
.end method

.method private final native nativeListenForBuzzNotifications()V
.end method

.method private final native nativePrepareEngine(Ljava/lang/String;)V
.end method

.method private final native nativeProcessSessionStanza(Ljava/lang/String;)V
.end method

.method private final native nativeProcessSessionStanzaResponse(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private final native nativeRegisterWithBuzzbot(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private final native nativeRelease()V
.end method

.method private final native nativeReleaseEngine()V
.end method

.method private final native nativeSendData(Ljava/lang/String;[B)V
.end method

.method private final native nativeSendDirectedPresence(Ljava/lang/String;)I
.end method

.method private final native nativeSendIbbData(Ljava/lang/String;[B)I
.end method

.method private final native nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private final native nativeSetJingleInfoStanza(Ljava/lang/String;)V
.end method

.method private static final native nativeSetLoggingLevel(I)V
.end method

.method private final native nativeSetPeerCapabilities(Ljava/lang/String;I)V
.end method

.method private final native nativeSetup(Ljava/lang/Object;ZI)V
.end method

.method private final native nativeSubscribeToBuzzChannels(Z)V
.end method

.method private final native nativeTerminateAllCalls()V
.end method

.method private final native nativeUnregisterWithBuzzbot()V
.end method

.method private final native nativeUpdateRemoteJidForSession(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private static onDataChannelConnectionResult(Ljava/lang/Object;Ljava/lang/String;ZI)V
    .locals 2

    .prologue
    .line 642
    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/jingle/Libjingle;

    .line 644
    invoke-static {v0}, Lcom/google/android/gms/games/jingle/Libjingle;->instanceOk(Lcom/google/android/gms/games/jingle/Libjingle;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 645
    new-instance v1, Lcom/google/android/gms/games/jingle/i;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/gms/games/jingle/i;-><init>(Ljava/lang/String;ZI)V

    invoke-static {v0, v1}, Lcom/google/android/gms/games/jingle/Libjingle;->sendMessage(Lcom/google/android/gms/games/jingle/Libjingle;Lcom/google/android/gms/games/jingle/h;)V

    .line 648
    :cond_0
    return-void
.end method

.method private static onDirectedPresenceReceipt(Ljava/lang/Object;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 659
    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/jingle/Libjingle;

    .line 661
    invoke-static {v0}, Lcom/google/android/gms/games/jingle/Libjingle;->instanceOk(Lcom/google/android/gms/games/jingle/Libjingle;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 662
    new-instance v1, Lcom/google/android/gms/games/jingle/e;

    invoke-direct {v1, p2, p1}, Lcom/google/android/gms/games/jingle/e;-><init>(ZLjava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/games/jingle/Libjingle;->sendMessage(Lcom/google/android/gms/games/jingle/Libjingle;Lcom/google/android/gms/games/jingle/h;)V

    .line 664
    :cond_0
    return-void
.end method

.method private static onNotifSubscriptionResult(Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 624
    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/jingle/Libjingle;

    .line 626
    invoke-static {v0}, Lcom/google/android/gms/games/jingle/Libjingle;->instanceOk(Lcom/google/android/gms/games/jingle/Libjingle;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 627
    new-instance v1, Lcom/google/android/gms/games/jingle/c;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/jingle/c;-><init>(Z)V

    invoke-static {v0, v1}, Lcom/google/android/gms/games/jingle/Libjingle;->sendMessage(Lcom/google/android/gms/games/jingle/Libjingle;Lcom/google/android/gms/games/jingle/h;)V

    .line 629
    :cond_0
    return-void
.end method

.method private prepareEngine(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 203
    const-string v0, "prepare engine"

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/jingle/Libjingle;->log(Ljava/lang/String;)V

    .line 204
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->nativePrepareEngine(Ljava/lang/String;)V

    .line 205
    return-void
.end method

.method private static receiveBuzzNotif(Ljava/lang/Object;[B[B)V
    .locals 4

    .prologue
    .line 615
    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/jingle/Libjingle;

    .line 617
    invoke-static {v0}, Lcom/google/android/gms/games/jingle/Libjingle;->instanceOk(Lcom/google/android/gms/games/jingle/Libjingle;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 618
    new-instance v1, Lcom/google/android/gms/games/jingle/b;

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p1}, Ljava/lang/String;-><init>([B)V

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, p2}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/games/jingle/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/games/jingle/Libjingle;->sendMessage(Lcom/google/android/gms/games/jingle/Libjingle;Lcom/google/android/gms/games/jingle/h;)V

    .line 621
    :cond_0
    return-void
.end method

.method private static receiveDataChannelData(Ljava/lang/Object;Ljava/lang/String;[B)V
    .locals 2

    .prologue
    .line 633
    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/jingle/Libjingle;

    .line 635
    invoke-static {v0}, Lcom/google/android/gms/games/jingle/Libjingle;->instanceOk(Lcom/google/android/gms/games/jingle/Libjingle;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 636
    new-instance v1, Lcom/google/android/gms/games/jingle/j;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/jingle/j;-><init>(Ljava/lang/String;[B)V

    invoke-static {v0, v1}, Lcom/google/android/gms/games/jingle/Libjingle;->sendMessage(Lcom/google/android/gms/games/jingle/Libjingle;Lcom/google/android/gms/games/jingle/h;)V

    .line 638
    :cond_0
    return-void
.end method

.method private static receiveIbbData(Ljava/lang/Object;Ljava/lang/String;[B)V
    .locals 2

    .prologue
    .line 597
    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/jingle/Libjingle;

    .line 599
    invoke-static {v0}, Lcom/google/android/gms/games/jingle/Libjingle;->instanceOk(Lcom/google/android/gms/games/jingle/Libjingle;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 600
    new-instance v1, Lcom/google/android/gms/games/jingle/f;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/jingle/f;-><init>(Ljava/lang/String;[B)V

    invoke-static {v0, v1}, Lcom/google/android/gms/games/jingle/Libjingle;->sendMessage(Lcom/google/android/gms/games/jingle/Libjingle;Lcom/google/android/gms/games/jingle/h;)V

    .line 602
    :cond_0
    return-void
.end method

.method private static receiveIbbSendResult(Ljava/lang/Object;ILjava/lang/String;Z)V
    .locals 2

    .prologue
    .line 606
    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/jingle/Libjingle;

    .line 608
    invoke-static {v0}, Lcom/google/android/gms/games/jingle/Libjingle;->instanceOk(Lcom/google/android/gms/games/jingle/Libjingle;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 609
    new-instance v1, Lcom/google/android/gms/games/jingle/g;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/gms/games/jingle/g;-><init>(ILjava/lang/String;Z)V

    invoke-static {v0, v1}, Lcom/google/android/gms/games/jingle/Libjingle;->sendMessage(Lcom/google/android/gms/games/jingle/Libjingle;Lcom/google/android/gms/games/jingle/h;)V

    .line 611
    :cond_0
    return-void
.end method

.method private static sendMessage(Lcom/google/android/gms/games/jingle/Libjingle;Lcom/google/android/gms/games/jingle/h;)V
    .locals 2

    .prologue
    .line 667
    iget-object v0, p0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    iget v1, p1, Lcom/google/android/gms/games/jingle/h;->f:I

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 668
    iget-object v1, p0, Lcom/google/android/gms/games/jingle/Libjingle;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 669
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;[B)I
    .locals 1

    .prologue
    .line 314
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeSendIbbData(Ljava/lang/String;[B)I

    move-result v0

    return v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/google/android/gms/games/jingle/Libjingle;->mInitialized:Z

    if-nez v0, :cond_0

    .line 175
    const-string v0, "release: already released"

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/jingle/Libjingle;->log(Ljava/lang/String;)V

    .line 183
    :goto_0
    return-void

    .line 179
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/jingle/Libjingle;->mInitialized:Z

    .line 181
    const-string v0, "Release: call nativeRelease"

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/jingle/Libjingle;->log(Ljava/lang/String;)V

    .line 182
    invoke-direct {p0}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeRelease()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 254
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeAcceptCall(Ljava/lang/String;)V

    .line 255
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 128
    iget-boolean v0, p0, Lcom/google/android/gms/games/jingle/Libjingle;->mInitialized:Z

    if-eqz v0, :cond_0

    .line 129
    const-string v0, "init: already initialized"

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/jingle/Libjingle;->log(Ljava/lang/String;)V

    .line 168
    :goto_0
    return-void

    .line 133
    :cond_0
    iput-boolean v8, p0, Lcom/google/android/gms/games/jingle/Libjingle;->mInitialized:Z

    .line 137
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v0, v1, p2}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeSetup(Ljava/lang/Object;ZI)V

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/games/jingle/Libjingle;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 140
    sget-object v3, Lcom/google/android/gms/games/jingle/Libjingle;->ANDROID_SETTINGS_KEYS_MAP:[[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_5

    aget-object v5, v3, v0

    .line 142
    aget-object v6, v5, v1

    .line 143
    aget-object v5, v5, v8

    .line 145
    const-string v7, "games.use_default_networks_only"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 152
    invoke-static {v2, v6, v8}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    .line 153
    if-ne v5, v8, :cond_1

    invoke-static {}, Lcom/google/android/gms/games/jingle/n;->a()Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_1
    const/4 v6, 0x2

    if-ne v5, v6, :cond_3

    .line 155
    :cond_2
    const-string v5, "games.use_default_networks_only"

    const-string v6, "true"

    invoke-direct {p0, v5, v6}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const-string v5, "games_rtmp:Libjingle"

    const-string v6, "Only using default networks!"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    :cond_3
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 160
    :cond_4
    invoke-static {v2, v6}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 161
    if-eqz v6, :cond_3

    .line 162
    invoke-direct {p0, v5, v6}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 167
    :cond_5
    invoke-static {p1}, Lcom/google/android/gms/games/jingle/Libjingle;->logTagToInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeSetLoggingLevel(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 225
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeCall(Ljava/lang/String;Ljava/lang/String;I)V

    .line 226
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 326
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeSubscribeToBuzzChannels(Z)V

    .line 327
    return-void
.end method

.method public final b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 267
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeCreateSocketConnection(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 343
    invoke-direct {p0}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeUnregisterWithBuzzbot()V

    .line 344
    return-void
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 423
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeSetPeerCapabilities(Ljava/lang/String;I)V

    .line 424
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 335
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeRegisterWithBuzzbot(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    return-void
.end method

.method public final b(Ljava/lang/String;[B)V
    .locals 0

    .prologue
    .line 361
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeSendData(Ljava/lang/String;[B)V

    .line 362
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 351
    invoke-direct {p0}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeListenForBuzzNotifications()V

    .line 352
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeConnectAndSignin(Ljava/lang/String;Z)V

    .line 287
    return-void
.end method

.method public final d(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 296
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeSendDirectedPresence(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 370
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeDisconnectAndSignout(Ljava/lang/String;)V

    .line 371
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 379
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeEndCall(Ljava/lang/String;)V

    .line 380
    return-void
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 192
    invoke-direct {p0}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeFinalize()V

    .line 193
    return-void
.end method

.method public final g(Ljava/lang/String;)Lcom/google/android/gms/games/jingle/PeerDiagnostics;
    .locals 1

    .prologue
    .line 411
    new-instance v0, Lcom/google/android/gms/games/jingle/PeerDiagnostics;

    invoke-direct {v0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics;-><init>()V

    .line 412
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/games/jingle/Libjingle;->nativeGetPeerDiagnosticMetrics(Ljava/lang/Object;Ljava/lang/String;)V

    .line 413
    return-object v0
.end method
