.class public final Lcom/google/android/gms/wallet/cache/l;
.super Lcom/android/volley/p;
.source "SourceFile"


# static fields
.field static final f:[B


# instance fields
.field private final g:Landroid/content/Context;

.field private final h:Landroid/os/Handler;

.field private final i:Ljava/util/HashMap;

.field private final j:Lcom/google/android/apps/common/a/a/i;

.field private final k:Lcom/google/android/apps/common/a/a/h;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 44
    new-instance v0, Lcom/google/checkout/inapp/proto/ah;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/ah;-><init>()V

    .line 45
    const/4 v1, 0x1

    new-array v1, v1, [I

    aput v2, v1, v2

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/ah;->a:[I

    .line 46
    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/cache/l;->f:[B

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/wallet/a/b;Landroid/os/Handler;)V
    .locals 3

    .prologue
    .line 56
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, p2, v1}, Lcom/android/volley/p;-><init>(ILjava/lang/String;Lcom/android/volley/w;)V

    .line 57
    iput-object p1, p0, Lcom/google/android/gms/wallet/cache/l;->g:Landroid/content/Context;

    .line 58
    iput-object p4, p0, Lcom/google/android/gms/wallet/cache/l;->h:Landroid/os/Handler;

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/cache/l;->i:Ljava/util/HashMap;

    .line 60
    iget-object v0, p3, Lcom/google/android/gms/wallet/a/b;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/l;->i:Ljava/util/HashMap;

    const-string v1, "Authorization"

    invoke-virtual {p3}, Lcom/google/android/gms/wallet/a/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/l;->i:Ljava/util/HashMap;

    const-string v1, "X-Version"

    const v2, 0x6768a8

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/l;->i:Ljava/util/HashMap;

    const-string v1, "X-Modality"

    const-string v2, "ANDROID_NATIVE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    new-instance v0, Lcom/google/android/apps/common/a/a/i;

    const-string v1, "get_wallet_profile"

    invoke-direct {v0, v1}, Lcom/google/android/apps/common/a/a/i;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/cache/l;->j:Lcom/google/android/apps/common/a/a/i;

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/l;->j:Lcom/google/android/apps/common/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/common/a/a/i;->a()Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/cache/l;->k:Lcom/google/android/apps/common/a/a/h;

    .line 72
    return-void
.end method

.method private static b(Lcom/android/volley/m;)Z
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/android/volley/m;->c:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/android/volley/m;->c:Ljava/util/Map;

    const-string v1, "Content-Type"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 126
    if-eqz v0, :cond_0

    const-string v1, "application/x-protobuf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    const/4 v0, 0x1

    .line 130
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/android/volley/m;)Lcom/android/volley/v;
    .locals 5

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/l;->j:Lcom/google/android/apps/common/a/a/i;

    iget-object v1, p0, Lcom/google/android/gms/wallet/cache/l;->k:Lcom/google/android/apps/common/a/a/h;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "rpc"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/common/a/a/i;->a(Lcom/google/android/apps/common/a/a/h;[Ljava/lang/String;)Z

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/l;->g:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/wallet/cache/l;->j:Lcom/google/android/apps/common/a/a/i;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/apps/common/a/a/i;)V

    .line 115
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/android/volley/ac;)V
    .locals 3

    .prologue
    .line 101
    iget-object v0, p1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    invoke-static {v0}, Lcom/google/android/gms/wallet/cache/l;->b(Lcom/android/volley/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/l;->h:Landroid/os/Handler;

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget-object v2, v2, Lcom/android/volley/m;->b:[B

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 107
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/l;->h:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method protected final synthetic b(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 36
    check-cast p1, Lcom/android/volley/m;

    invoke-static {p1}, Lcom/google/android/gms/wallet/cache/l;->b(Lcom/android/volley/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/l;->h:Landroid/os/Handler;

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/android/volley/m;->b:[B

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/l;->h:Landroid/os/Handler;

    const/4 v1, 0x2

    new-instance v2, Lcom/android/volley/ac;

    invoke-direct {v2, p1}, Lcom/android/volley/ac;-><init>(Lcom/android/volley/m;)V

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public final i()Ljava/util/Map;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/l;->i:Ljava/util/HashMap;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    const-string v0, "application/x-protobuf"

    return-object v0
.end method

.method public final m()[B
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/google/android/gms/wallet/cache/l;->f:[B

    return-object v0
.end method

.method public final o()Lcom/android/volley/r;
    .locals 1

    .prologue
    .line 120
    sget-object v0, Lcom/android/volley/r;->a:Lcom/android/volley/r;

    return-object v0
.end method
