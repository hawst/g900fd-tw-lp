.class public final Lcom/google/android/gms/plus/service/a/ae;
.super Lcom/google/android/gms/plus/service/a/d;
.source "SourceFile"


# instance fields
.field final c:Ljava/lang/String;

.field final d:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/c;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p3}, Lcom/google/android/gms/plus/service/a/d;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/c;)V

    .line 35
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/ae;->c:Ljava/lang/String;

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/plus/service/a/ae;->d:I

    .line 37
    return-void
.end method


# virtual methods
.method public final b(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)Landroid/util/Pair;
    .locals 9

    .prologue
    .line 42
    iget-object v6, p0, Lcom/google/android/gms/plus/service/a/ae;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v7, p0, Lcom/google/android/gms/plus/service/a/ae;->c:Ljava/lang/String;

    iget v8, p0, Lcom/google/android/gms/plus/service/a/ae;->d:I

    invoke-virtual {v6}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {v6}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/common/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v6}, Lcom/google/android/gms/common/server/ClientContext;->a()I

    move-result v1

    invoke-virtual {v6}, Lcom/google/android/gms/common/server/ClientContext;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6}, Lcom/google/android/gms/common/server/ClientContext;->e()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Lcom/google/android/gms/common/server/ClientContext;->a(Lcom/google/android/gms/common/server/ClientContext;)V

    invoke-virtual {v0, v6}, Lcom/google/android/gms/common/server/ClientContext;->b(Lcom/google/android/gms/common/server/ClientContext;)V

    move-object v2, v0

    :goto_0
    iget-object v0, p2, Lcom/google/android/gms/plus/b/b;->b:Lcom/google/android/gms/plus/b/p;

    invoke-virtual {p2, p1, v2}, Lcom/google/android/gms/plus/b/b;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    move-object v3, v7

    move v5, v8

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/plus/b/p;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;I)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v2, v6

    goto :goto_0
.end method
