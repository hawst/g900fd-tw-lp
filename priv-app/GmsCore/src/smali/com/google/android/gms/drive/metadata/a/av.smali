.class public abstract Lcom/google/android/gms/drive/metadata/a/av;
.super Lcom/google/android/gms/drive/metadata/a/ay;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/metadata/f;ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 19
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/metadata/a/ay;-><init>(Lcom/google/android/gms/drive/metadata/f;ZLjava/lang/String;)V

    .line 20
    return-void
.end method


# virtual methods
.method protected final synthetic a(Lorg/json/JSONObject;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/a/ay;->c:Lcom/google/android/gms/drive/metadata/f;

    invoke-interface {v0}, Lcom/google/android/gms/drive/metadata/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/gms/drive/auth/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Lorg/json/JSONObject;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 16
    check-cast p2, Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/a/ay;->c:Lcom/google/android/gms/drive/metadata/f;

    invoke-interface {v0}, Lcom/google/android/gms/drive/metadata/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method
