.class public Lcom/google/android/gms/people/sync/PeopleSyncService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/Object;

.field private static b:Lcom/google/android/gms/people/sync/r;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/sync/PeopleSyncService;->a:Ljava/lang/Object;

    .line 20
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/people/sync/PeopleSyncService;->b:Lcom/google/android/gms/people/sync/r;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 38
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-static {v0, p2, p3}, Lcom/google/android/gms/people/debug/c;->b(Landroid/content/Context;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/google/android/gms/people/sync/PeopleSyncService;->b:Lcom/google/android/gms/people/sync/r;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/r;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 24
    sget-object v1, Lcom/google/android/gms/people/sync/PeopleSyncService;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 25
    :try_start_0
    sget-object v0, Lcom/google/android/gms/people/sync/PeopleSyncService;->b:Lcom/google/android/gms/people/sync/r;

    if-nez v0, :cond_0

    .line 26
    new-instance v0, Lcom/google/android/gms/people/sync/r;

    invoke-virtual {p0}, Lcom/google/android/gms/people/sync/PeopleSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/people/sync/r;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/people/sync/PeopleSyncService;->b:Lcom/google/android/gms/people/sync/r;

    .line 28
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
