.class public Lcom/google/android/gms/wallet/service/l;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/service/l;->a:I

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/service/l;->b:Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 375
    return-void
.end method

.method public a(Lcom/google/aa/a/a/a/f;)V
    .locals 0

    .prologue
    .line 275
    return-void
.end method

.method public a(Lcom/google/aa/b/a/a/a/a/d;)V
    .locals 0

    .prologue
    .line 282
    return-void
.end method

.method public a(Lcom/google/aa/b/a/a/a/a/f;)V
    .locals 0

    .prologue
    .line 289
    return-void
.end method

.method public a(Lcom/google/aa/b/a/a/a/a/j;J)V
    .locals 0

    .prologue
    .line 295
    return-void
.end method

.method public a(Lcom/google/aa/b/a/a/a/a/l;)V
    .locals 0

    .prologue
    .line 269
    return-void
.end method

.method public a(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 0

    .prologue
    .line 313
    return-void
.end method

.method public a(Lcom/google/checkout/b/a/c;)V
    .locals 0

    .prologue
    .line 369
    return-void
.end method

.method public a(Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;)V
    .locals 0

    .prologue
    .line 241
    return-void
.end method

.method public a(Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;)V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public a(Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;)V
    .locals 0

    .prologue
    .line 319
    return-void
.end method

.method public a(Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;)V
    .locals 0

    .prologue
    .line 254
    return-void
.end method

.method public a(Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;)V
    .locals 0

    .prologue
    .line 228
    return-void
.end method

.method public a(Lcom/google/checkout/inapp/proto/a/b;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 0

    .prologue
    .line 235
    return-void
.end method

.method public a(Lcom/google/checkout/inapp/proto/ag;)V
    .locals 0

    .prologue
    .line 325
    return-void
.end method

.method public a(Lcom/google/checkout/inapp/proto/ai;)V
    .locals 0

    .prologue
    .line 363
    return-void
.end method

.method public a(Lcom/google/checkout/inapp/proto/al;)V
    .locals 0

    .prologue
    .line 301
    return-void
.end method

.method public a(Lcom/google/checkout/inapp/proto/an;)V
    .locals 0

    .prologue
    .line 307
    return-void
.end method

.method public a(Lcom/google/checkout/inapp/proto/j;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 0

    .prologue
    .line 208
    return-void
.end method

.method public a(Lcom/google/checkout/inapp/proto/t;)V
    .locals 0

    .prologue
    .line 337
    return-void
.end method

.method public a(Lcom/google/checkout/inapp/proto/v;)V
    .locals 0

    .prologue
    .line 351
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/service/l;->b:Z

    .line 54
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 389
    return-void
.end method

.method public b(Lcom/google/checkout/inapp/proto/a/b;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 0

    .prologue
    .line 248
    return-void
.end method

.method public b(Lcom/google/checkout/inapp/proto/j;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 0

    .prologue
    .line 222
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 384
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 261
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 331
    return-void
.end method

.method public f()V
    .locals 0

    .prologue
    .line 345
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 357
    return-void
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/google/android/gms/wallet/service/l;->a:I

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/service/l;->b:Z

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, p0, Lcom/google/android/gms/wallet/service/l;->a:I

    if-gt v0, v1, :cond_1

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p0, Lcom/google/android/gms/wallet/service/l;->a:I

    .line 72
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    .line 73
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    .line 74
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 75
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 197
    :pswitch_0
    const-string v0, "PaymentServiceResponseH"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown ServerResponse type="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/service/l;->b()V

    goto :goto_0

    .line 77
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/service/l;->a()V

    goto :goto_0

    .line 80
    :pswitch_2
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/service/l;->d()V

    goto :goto_0

    .line 83
    :pswitch_3
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;

    .line 85
    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    array-length v2, v2

    if-gtz v2, :cond_2

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lcom/google/checkout/inapp/proto/j;

    if-nez v2, :cond_3

    .line 87
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;)V

    goto :goto_0

    .line 89
    :cond_3
    iget-object v0, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lcom/google/checkout/inapp/proto/j;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/inapp/proto/j;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    goto :goto_0

    .line 94
    :pswitch_4
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;

    .line 96
    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->c:[I

    array-length v2, v2

    if-gtz v2, :cond_4

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->a:Lcom/google/checkout/inapp/proto/j;

    if-nez v2, :cond_5

    .line 98
    :cond_4
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;)V

    goto :goto_0

    .line 100
    :cond_5
    iget-object v0, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->a:Lcom/google/checkout/inapp/proto/j;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/service/l;->b(Lcom/google/checkout/inapp/proto/j;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    goto :goto_0

    .line 105
    :pswitch_5
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;

    .line 106
    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->c:[I

    array-length v2, v2

    if-gtz v2, :cond_6

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v2, :cond_7

    .line 108
    :cond_6
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;)V

    goto/16 :goto_0

    .line 110
    :cond_7
    iget-object v0, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/inapp/proto/a/b;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    goto/16 :goto_0

    .line 114
    :pswitch_6
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;

    .line 115
    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    array-length v2, v2

    if-gtz v2, :cond_8

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v2, :cond_9

    .line 117
    :cond_8
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;)V

    goto/16 :goto_0

    .line 119
    :cond_9
    iget-object v0, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/service/l;->b(Lcom/google/checkout/inapp/proto/a/b;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    goto/16 :goto_0

    .line 123
    :pswitch_7
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/service/l;->c()V

    goto/16 :goto_0

    .line 126
    :pswitch_8
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/l;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/aa/b/a/a/a/a/l;)V

    goto/16 :goto_0

    .line 130
    :pswitch_9
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/a/a/a/f;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/aa/a/a/a/f;)V

    goto/16 :goto_0

    .line 133
    :pswitch_a
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/b/a/c;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/b/a/c;)V

    goto/16 :goto_0

    .line 136
    :pswitch_b
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/d;

    .line 138
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/aa/b/a/a/a/a/d;)V

    goto/16 :goto_0

    .line 141
    :pswitch_c
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/j;

    .line 143
    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/aa/b/a/a/a/a/j;J)V

    goto/16 :goto_0

    .line 146
    :pswitch_d
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/f;

    .line 148
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/aa/b/a/a/a/a/f;)V

    goto/16 :goto_0

    .line 151
    :pswitch_e
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/al;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/inapp/proto/al;)V

    goto/16 :goto_0

    .line 155
    :pswitch_f
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/an;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/inapp/proto/an;)V

    goto/16 :goto_0

    .line 158
    :pswitch_10
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;

    .line 161
    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    array-length v2, v2

    if-gtz v2, :cond_a

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    array-length v2, v2

    if-gtz v2, :cond_a

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    array-length v2, v2

    if-lez v2, :cond_b

    .line 164
    :cond_a
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;)V

    goto/16 :goto_0

    .line 166
    :cond_b
    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    goto/16 :goto_0

    .line 170
    :pswitch_11
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/ag;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/inapp/proto/ag;)V

    goto/16 :goto_0

    .line 174
    :pswitch_12
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/service/l;->e()V

    goto/16 :goto_0

    .line 178
    :pswitch_13
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/t;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/inapp/proto/t;)V

    goto/16 :goto_0

    .line 182
    :pswitch_14
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/service/l;->f()V

    goto/16 :goto_0

    .line 186
    :pswitch_15
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/v;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/inapp/proto/v;)V

    goto/16 :goto_0

    .line 190
    :pswitch_16
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/service/l;->g()V

    goto/16 :goto_0

    .line 194
    :pswitch_17
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/ai;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/inapp/proto/ai;)V

    goto/16 :goto_0

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_f
        :pswitch_e
        :pswitch_2
        :pswitch_1
        :pswitch_7
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_10
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_c
        :pswitch_a
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_d
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch
.end method
