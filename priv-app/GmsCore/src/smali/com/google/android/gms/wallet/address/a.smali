.class public final Lcom/google/android/gms/wallet/address/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/wallet/cache/j;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/cache/j;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/android/gms/wallet/address/a;->a:Landroid/content/Context;

    .line 43
    iput-object p2, p0, Lcom/google/android/gms/wallet/address/a;->b:Lcom/google/android/gms/wallet/cache/j;

    .line 44
    return-void
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;Lcom/google/android/gms/identity/intents/UserAddressRequest;Lcom/google/android/gms/wallet/address/c;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/a;->b:Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual {v0, p1, v4}, Lcom/google/android/gms/wallet/cache/j;->a(Landroid/accounts/Account;I)Lcom/google/aa/a/a/a/b;

    move-result-object v1

    .line 58
    const-string v2, "AddressRetreiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "Is cached profile null?"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v1, :cond_0

    move v0, v4

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    if-eqz v1, :cond_1

    .line 60
    invoke-static {v1, p2}, Lcom/google/android/gms/wallet/common/a/e;->a(Lcom/google/aa/a/a/a/b;Lcom/google/android/gms/identity/intents/UserAddressRequest;)[Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v0

    .line 62
    array-length v1, v0

    if-lez v1, :cond_1

    .line 63
    invoke-interface {p3, v0}, Lcom/google/android/gms/wallet/address/c;->a([Lcom/google/checkout/inapp/proto/a/b;)V

    .line 73
    :goto_1
    return-void

    .line 58
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 67
    :cond_1
    new-instance v0, Lcom/google/android/gms/wallet/cache/c;

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/a;->a:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v2

    new-instance v5, Lcom/google/android/gms/wallet/address/b;

    invoke-direct {v5, p2, p3}, Lcom/google/android/gms/wallet/address/b;-><init>(Lcom/google/android/gms/identity/intents/UserAddressRequest;Lcom/google/android/gms/wallet/address/c;)V

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/app/GmsApplication;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/wallet/cache/c;-><init>(Landroid/content/Context;Lcom/android/volley/s;Landroid/accounts/Account;ILcom/google/android/gms/wallet/cache/e;Landroid/os/Looper;)V

    .line 72
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/cache/c;->run()V

    goto :goto_1
.end method
