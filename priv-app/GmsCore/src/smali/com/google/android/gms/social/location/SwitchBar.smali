.class public Lcom/google/android/gms/social/location/SwitchBar;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/Checkable;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private a:Z

.field private b:Landroid/widget/TextView;

.field private c:Lcom/google/android/gms/social/location/ai;

.field private d:Landroid/widget/CompoundButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 43
    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/google/android/gms/social/location/SwitchBar;->a:Z

    .line 52
    iget-object v1, p0, Lcom/google/android/gms/social/location/SwitchBar;->b:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/google/android/gms/social/location/SwitchBar;->a:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/gms/p;->rx:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/social/location/SwitchBar;->c:Lcom/google/android/gms/social/location/ai;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/social/location/SwitchBar;->c:Lcom/google/android/gms/social/location/ai;

    iget-boolean v1, p0, Lcom/google/android/gms/social/location/SwitchBar;->a:Z

    invoke-interface {v0, v1}, Lcom/google/android/gms/social/location/ai;->a(Z)V

    .line 56
    :cond_0
    return-void

    .line 52
    :cond_1
    sget v0, Lcom/google/android/gms/p;->ru:I

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/social/location/ai;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/google/android/gms/social/location/SwitchBar;->c:Lcom/google/android/gms/social/location/ai;

    .line 91
    return-void
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/google/android/gms/social/location/SwitchBar;->a:Z

    return v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0, p2}, Lcom/google/android/gms/social/location/SwitchBar;->a(Z)V

    .line 96
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/SwitchBar;->toggle()V

    .line 87
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/social/location/SwitchBar;->b:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 72
    invoke-virtual {p0, p0}, Lcom/google/android/gms/social/location/SwitchBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    sget v0, Lcom/google/android/gms/j;->sq:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/SwitchBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/social/location/SwitchBar;->b:Landroid/widget/TextView;

    .line 74
    sget v0, Lcom/google/android/gms/j;->sr:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/SwitchBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/google/android/gms/social/location/SwitchBar;->d:Landroid/widget/CompoundButton;

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/social/location/SwitchBar;->d:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 78
    invoke-direct {p0, v1}, Lcom/google/android/gms/social/location/SwitchBar;->a(Z)V

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/social/location/SwitchBar;->d:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 82
    :cond_0
    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/social/location/SwitchBar;->d:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 48
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/google/android/gms/social/location/SwitchBar;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/SwitchBar;->setChecked(Z)V

    .line 66
    return-void

    .line 65
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
