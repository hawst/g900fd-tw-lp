.class final Lcom/google/android/gms/appstate/service/b;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/appstate/service/AppStateAndroidService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/appstate/service/AppStateAndroidService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/gms/appstate/service/b;->a:Lcom/google/android/gms/appstate/service/AppStateAndroidService;

    .line 56
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 57
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 62
    const-string v0, "AppStateService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "client connected with version: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid package name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/b;->a:Lcom/google/android/gms/appstate/service/AppStateAndroidService;

    invoke-static {v0, p3}, Lcom/google/android/gms/appstate/service/AppStateAndroidService;->a(Lcom/google/android/gms/appstate/service/AppStateAndroidService;Ljava/lang/String;)Z

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/b;->a:Lcom/google/android/gms/appstate/service/AppStateAndroidService;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 75
    return-void
.end method
