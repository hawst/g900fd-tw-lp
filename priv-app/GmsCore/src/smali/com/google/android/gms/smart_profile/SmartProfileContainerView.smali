.class public Lcom/google/android/gms/smart_profile/SmartProfileContainerView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field private A:I

.field private B:F

.field private C:F

.field private D:F

.field private E:I

.field private F:Landroid/graphics/Point;

.field private G:Z

.field private H:Z

.field private a:I

.field private b:I

.field private c:Landroid/widget/LinearLayout;

.field private d:Landroid/widget/LinearLayout;

.field private e:Landroid/widget/TextView;

.field private f:I

.field private g:Lcom/google/android/gms/smart_profile/SmartProfileActivity;

.field private h:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

.field private i:Landroid/widget/LinearLayout;

.field private j:Landroid/widget/LinearLayout;

.field private k:Landroid/widget/LinearLayout;

.field private l:Landroid/widget/FrameLayout;

.field private m:Landroid/widget/FrameLayout;

.field private n:Landroid/widget/LinearLayout;

.field private o:Landroid/view/View;

.field private p:Landroid/view/View;

.field private q:Landroid/view/View;

.field private r:Landroid/view/View;

.field private s:Landroid/view/View;

.field private t:Landroid/widget/ScrollView;

.field private u:Landroid/support/v4/view/q;

.field private v:Landroid/support/v4/widget/x;

.field private w:Landroid/widget/Scroller;

.field private x:I

.field private y:I

.field private z:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 98
    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a(Landroid/content/Context;)V

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 103
    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a(Landroid/content/Context;)V

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 108
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a(Landroid/content/Context;)V

    .line 110
    return-void
.end method

.method static synthetic a(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 51
    invoke-static {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method private a(I)Landroid/animation/ValueAnimator;
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .prologue
    .line 665
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->d:Landroid/widget/LinearLayout;

    const-string v1, "translationY"

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->j()F

    move-result v4

    aput v4, v2, v3

    const/4 v3, 0x1

    int-to-float v4, p1

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 667
    new-instance v1, Lcom/google/android/gms/smart_profile/ba;

    invoke-direct {v1, p0}, Lcom/google/android/gms/smart_profile/ba;-><init>(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 677
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 678
    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 679
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)Lcom/google/android/gms/smart_profile/SmartProfileActivity;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g:Lcom/google/android/gms/smart_profile/SmartProfileActivity;

    return-object v0
.end method

.method private a(FF)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 683
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 684
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    float-to-int v2, p1

    float-to-int v4, p2

    const/high16 v7, -0x80000000

    const v8, 0x7fffffff

    move v3, v1

    move v5, v1

    move v6, v1

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 687
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 756
    new-instance v0, Landroid/support/v4/widget/x;

    invoke-direct {v0, p1}, Landroid/support/v4/widget/x;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->v:Landroid/support/v4/widget/x;

    .line 757
    check-cast p1, Lcom/google/android/gms/smart_profile/SmartProfileActivity;

    iput-object p1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g:Lcom/google/android/gms/smart_profile/SmartProfileActivity;

    .line 758
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    .line 759
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->z:F

    .line 761
    const/high16 v0, 0x43fa0000    # 500.0f

    iput v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->D:F

    .line 762
    iput-boolean v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->G:Z

    .line 763
    iput-boolean v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->H:Z

    .line 764
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 765
    return-void
.end method

.method private static a(Landroid/view/View;F)V
    .locals 2

    .prologue
    .line 891
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 892
    if-eqz v0, :cond_0

    .line 893
    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 895
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;F)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c(F)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;FF)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a(FF)V

    return-void
.end method

.method private a(F)Z
    .locals 2

    .prologue
    .line 430
    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->G:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->B:F

    sub-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->E:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 808
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 809
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    return v0
.end method

.method private b(I)J
    .locals 3

    .prologue
    .line 696
    const/high16 v0, 0x43fa0000    # 500.0f

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->j()F

    move-result v1

    int-to-float v2, p1

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g()Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    float-to-long v0, v0

    return-wide v0
.end method

.method static synthetic b(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private b(F)V
    .locals 2

    .prologue
    .line 739
    iput p1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->B:F

    .line 740
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->j()F

    move-result v0

    iput v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->C:F

    .line 741
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->t:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->y:I

    .line 742
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->x:I

    .line 743
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->n:Landroid/widget/LinearLayout;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b(Landroid/view/View;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->A:I

    .line 744
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 745
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)F
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->d()F

    move-result v0

    return v0
.end method

.method private c(F)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 781
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->d:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->j()F

    move-result v0

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 782
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    .line 783
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->d:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->z:F

    sub-float v1, p1, v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->offsetTopAndBottom(I)V

    .line 784
    iput p1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->z:F

    .line 790
    :cond_0
    :goto_0
    return-void

    .line 786
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setTranslationY(F)V

    .line 787
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->d()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    goto :goto_0
.end method

.method private d()F
    .locals 3

    .prologue
    .line 691
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->j()F

    move-result v1

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g()Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    const v1, 0x3f59999a    # 0.85f

    mul-float/2addr v0, v1

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)Landroid/widget/Scroller;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    return-object v0
.end method

.method private d(F)V
    .locals 3

    .prologue
    .line 820
    const/4 v0, 0x0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 821
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 824
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->k:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    cmpl-float v0, v1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 827
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 828
    return-void

    .line 824
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private e()I
    .locals 1

    .prologue
    .line 702
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    div-int/lit8 v0, v0, 0x4

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    return v0
.end method

.method private e(F)V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 833
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 888
    :goto_0
    return-void

    .line 838
    :cond_0
    iget v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b:I

    int-to-float v0, v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 839
    iget v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a:I

    int-to-float v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 841
    iget v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a:I

    int-to-float v1, v1

    sub-float v1, v0, v1

    iget v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b:I

    iget v3, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 844
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 845
    float-to-int v3, v0

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 846
    iget-object v3, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 848
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_3

    .line 851
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sub-float v3, v4, v1

    const/high16 v4, 0x437f0000    # 255.0f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 856
    float-to-double v2, v0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->r:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-double v4, v0

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v4, v6

    cmpg-double v0, v2, v4

    if-gez v0, :cond_2

    .line 857
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 858
    if-eqz v0, :cond_1

    .line 859
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->s:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 886
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->o:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a(Landroid/view/View;F)V

    .line 887
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->p:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a(Landroid/view/View;F)V

    goto :goto_0

    .line 862
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->getWidth()I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_1

    .line 867
    :cond_3
    sub-float v0, v4, v1

    const/high16 v2, -0x40000000    # -2.0f

    div-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b:I

    iget v3, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 869
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->m:Landroid/widget/FrameLayout;

    int-to-float v0, v0

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    .line 872
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->j:Landroid/widget/LinearLayout;

    sub-float v2, v4, v1

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 877
    const/high16 v0, 0x3f200000    # 0.625f

    const/high16 v2, 0x3ec00000    # 0.375f

    mul-float/2addr v2, v1

    add-float/2addr v0, v2

    .line 878
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getWidth()I

    move-result v2

    if-lez v2, :cond_1

    .line 879
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setScaleX(F)V

    .line 880
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setScaleY(F)V

    goto :goto_1
.end method

.method private f()I
    .locals 2

    .prologue
    .line 706
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->t:Landroid/widget/ScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->t:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method static synthetic f(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->n:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private g()Landroid/graphics/Point;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xd
    .end annotation

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->F:Landroid/graphics/Point;

    if-eqz v0, :cond_0

    .line 712
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->F:Landroid/graphics/Point;

    .line 725
    :goto_0
    return-object v0

    .line 715
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 716
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 718
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xd

    if-ge v1, v2, :cond_1

    .line 719
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->F:Landroid/graphics/Point;

    .line 720
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->F:Landroid/graphics/Point;

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Point;->set(II)V

    .line 721
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->F:Landroid/graphics/Point;

    goto :goto_0

    .line 723
    :cond_1
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->F:Landroid/graphics/Point;

    .line 724
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->F:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 725
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->F:Landroid/graphics/Point;

    goto :goto_0
.end method

.method static synthetic g(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->t:Landroid/widget/ScrollView;

    return-object v0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 748
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->G:Z

    .line 749
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->j()F

    move-result v0

    iput v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->C:F

    .line 750
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 751
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c()V

    .line 753
    :cond_0
    return-void
.end method

.method static synthetic i(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)F
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->j()F

    move-result v0

    return v0
.end method

.method private i()Z
    .locals 2

    .prologue
    .line 776
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()F
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 794
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 795
    iget v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->z:F

    .line 797
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getTranslationY()F

    move-result v0

    goto :goto_0
.end method

.method private k()Z
    .locals 2

    .prologue
    .line 813
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    .line 814
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->d:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->j()F

    move-result v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    cmpl-float v0, v1, v0

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->G:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->q:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->q:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 419
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->t:Landroid/widget/ScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 420
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->t:Landroid/widget/ScrollView;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/bf;->a(Landroid/view/View;)V

    .line 422
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 425
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->H:Z

    .line 426
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e(F)V

    .line 427
    return-void
.end method

.method public final c()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 499
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xc

    if-ge v0, v1, :cond_0

    .line 500
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c(F)V

    .line 501
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g:Lcom/google/android/gms/smart_profile/SmartProfileActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->finish()V

    .line 526
    :goto_0
    return-void

    .line 503
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a(I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 504
    new-instance v1, Lcom/google/android/gms/smart_profile/az;

    invoke-direct {v1, p0}, Lcom/google/android/gms/smart_profile/az;-><init>(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 524
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 114
    invoke-super {p0}, Landroid/widget/LinearLayout;->computeScroll()V

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    int-to-float v3, v0

    .line 125
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-ge v0, v4, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getStartY()I

    move-result v0

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->getCurrY()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->timePassed()I

    move-result v5

    div-int/2addr v4, v5

    sub-int/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    .line 127
    :goto_0
    iget v4, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_7

    .line 128
    cmpg-float v4, v3, v6

    if-gtz v4, :cond_4

    .line 133
    invoke-direct {p0, v6}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c(F)V

    .line 134
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 135
    iput v7, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    .line 136
    invoke-direct {p0, v6, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a(FF)V

    move v0, v1

    .line 224
    :goto_1
    if-eqz v0, :cond_0

    .line 225
    invoke-static {p0}, Landroid/support/v4/view/ay;->d(Landroid/view/View;)V

    .line 227
    :cond_0
    return-void

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrVelocity()F

    move-result v0

    goto :goto_0

    .line 138
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->H:Z

    if-eqz v2, :cond_3

    .line 139
    iput v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    .line 140
    iget v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b:I

    int-to-float v2, v2

    neg-float v0, v0

    invoke-direct {p0, v2, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a(FF)V

    move v0, v1

    goto :goto_1

    .line 142
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    move v0, v1

    goto :goto_1

    .line 145
    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g()Landroid/graphics/Point;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_5

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 149
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c(F)V

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g:Lcom/google/android/gms/smart_profile/SmartProfileActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->finish()V

    move v0, v2

    goto :goto_1

    .line 152
    :cond_5
    iget v3, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->D:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_6

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c(F)V

    .line 160
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c()V

    move v0, v2

    .line 161
    goto :goto_1

    .line 165
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c(F)V

    move v0, v1

    goto :goto_1

    .line 167
    :cond_7
    iget v4, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    if-ne v4, v1, :cond_a

    .line 168
    iget v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a:I

    int-to-float v2, v2

    cmpg-float v2, v3, v2

    if-gez v2, :cond_8

    .line 169
    iget v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a:I

    int-to-float v2, v2

    invoke-direct {p0, v2}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e(F)V

    .line 170
    iput v8, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    .line 174
    neg-float v0, v0

    invoke-direct {p0, v6, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a(FF)V

    move v0, v1

    goto/16 :goto_1

    .line 175
    :cond_8
    iget v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b:I

    int-to-float v0, v0

    cmpl-float v0, v3, v0

    if-lez v0, :cond_9

    .line 176
    iget v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b:I

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e(F)V

    .line 177
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    move v0, v1

    goto/16 :goto_1

    .line 183
    :cond_9
    invoke-direct {p0, v3}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e(F)V

    move v0, v1

    goto/16 :goto_1

    .line 185
    :cond_a
    iget v4, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    if-ne v4, v8, :cond_d

    .line 186
    cmpl-float v2, v3, v6

    if-lez v2, :cond_b

    .line 187
    invoke-direct {p0, v6}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->d(F)V

    .line 188
    iput v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    .line 189
    iget v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a:I

    int-to-float v2, v2

    invoke-direct {p0, v2, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a(FF)V

    move v0, v1

    goto/16 :goto_1

    .line 190
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    cmpg-float v2, v3, v2

    if-gez v2, :cond_c

    .line 195
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-direct {p0, v2}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->d(F)V

    .line 196
    iput v7, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    .line 197
    invoke-direct {p0, v6, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a(FF)V

    move v0, v1

    goto/16 :goto_1

    .line 199
    :cond_c
    invoke-direct {p0, v3}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->d(F)V

    move v0, v1

    goto/16 :goto_1

    .line 201
    :cond_d
    iget v4, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    if-ne v4, v7, :cond_11

    .line 202
    cmpg-float v4, v3, v6

    if-gez v4, :cond_f

    .line 203
    iget-object v3, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->t:Landroid/widget/ScrollView;

    invoke-virtual {v3, v2, v2}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 205
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 206
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    .line 209
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    move v0, v1

    goto/16 :goto_1

    .line 211
    :cond_e
    iput v8, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    .line 212
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-direct {p0, v2, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a(FF)V

    move v0, v1

    goto/16 :goto_1

    .line 214
    :cond_f
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v4, v3, v4

    if-lez v4, :cond_10

    .line 215
    iget-object v3, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->t:Landroid/widget/ScrollView;

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f()I

    move-result v4

    invoke-virtual {v3, v2, v4}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 216
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->v:Landroid/support/v4/widget/x;

    float-to-int v0, v0

    invoke-virtual {v2, v0}, Landroid/support/v4/widget/x;->a(I)Z

    move-result v0

    .line 217
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    invoke-virtual {v2, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    goto/16 :goto_1

    .line 219
    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->t:Landroid/widget/ScrollView;

    float-to-int v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/ScrollView;->scrollTo(II)V

    :cond_11
    move v0, v1

    goto/16 :goto_1
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->u:Landroid/support/v4/view/q;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->u:Landroid/support/v4/view/q;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/q;->a(Landroid/view/MotionEvent;)Z

    .line 235
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 240
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 243
    const/4 v0, 0x0

    .line 244
    invoke-static {p0}, Landroid/support/v4/view/ay;->a(Landroid/view/View;)I

    move-result v1

    .line 245
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 247
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->v:Landroid/support/v4/widget/x;

    invoke-virtual {v1}, Landroid/support/v4/widget/x;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 248
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 249
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    .line 250
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->getPaddingBottom()I

    move-result v2

    sub-int v2, v0, v2

    .line 251
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->getWidth()I

    move-result v0

    .line 255
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 256
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->t:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getWidth()I

    move-result v0

    .line 257
    iget-object v3, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v3

    add-int/2addr v3, v0

    neg-int v3, v3

    int-to-float v3, v3

    neg-int v4, v2

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 261
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->v:Landroid/support/v4/widget/x;

    invoke-virtual {v3, v0, v2}, Landroid/support/v4/widget/x;->a(II)V

    .line 262
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->v:Landroid/support/v4/widget/x;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/x;->a(Landroid/graphics/Canvas;)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 263
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 269
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    .line 270
    invoke-static {p0}, Landroid/support/v4/view/ay;->d(Landroid/view/View;)V

    .line 272
    :cond_2
    return-void

    .line 259
    :cond_3
    neg-int v3, v0

    int-to-float v3, v3

    neg-int v4, v2

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0

    .line 266
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->v:Landroid/support/v4/widget/x;

    invoke-virtual {v1}, Landroid/support/v4/widget/x;->b()V

    goto :goto_1
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .prologue
    .line 435
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g:Lcom/google/android/gms/smart_profile/SmartProfileActivity;

    if-eqz v0, :cond_0

    .line 436
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g:Lcom/google/android/gms/smart_profile/SmartProfileActivity;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 437
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->E:I

    .line 439
    new-instance v0, Lcom/google/android/gms/smart_profile/ax;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g:Lcom/google/android/gms/smart_profile/SmartProfileActivity;

    new-instance v2, Lcom/google/android/gms/smart_profile/bb;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/smart_profile/bb;-><init>(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;B)V

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/smart_profile/ax;-><init>(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->u:Landroid/support/v4/view/q;

    .line 447
    new-instance v0, Landroid/widget/Scroller;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g:Lcom/google/android/gms/smart_profile/SmartProfileActivity;

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->w:Landroid/widget/Scroller;

    .line 449
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 450
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    iput v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a:I

    iput v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b:I

    .line 459
    :cond_0
    :goto_0
    return-void

    .line 454
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-double v0, v0

    const-wide/high16 v2, 0x3fe8000000000000L    # 0.75

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b:I

    .line 455
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/g;->bS:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a:I

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 464
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 466
    sget v0, Lcom/google/android/gms/j;->dw:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->d:Landroid/widget/LinearLayout;

    .line 467
    sget v0, Lcom/google/android/gms/j;->qN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->t:Landroid/widget/ScrollView;

    .line 468
    sget v0, Lcom/google/android/gms/j;->rU:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c:Landroid/widget/LinearLayout;

    .line 469
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/google/android/gms/smart_profile/ay;

    invoke-direct {v1, p0}, Lcom/google/android/gms/smart_profile/ay;-><init>(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 475
    sget v0, Lcom/google/android/gms/j;->dE:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->q:Landroid/view/View;

    .line 476
    sget v0, Lcom/google/android/gms/j;->ta:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->r:Landroid/view/View;

    .line 477
    sget v0, Lcom/google/android/gms/j;->tb:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->s:Landroid/view/View;

    .line 478
    sget v0, Lcom/google/android/gms/j;->jE:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->k:Landroid/widget/LinearLayout;

    .line 479
    sget v0, Lcom/google/android/gms/j;->jK:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->o:Landroid/view/View;

    .line 480
    sget v0, Lcom/google/android/gms/j;->jJ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->p:Landroid/view/View;

    .line 482
    sget v0, Lcom/google/android/gms/j;->jI:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->h:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    .line 484
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->h:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->j()Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e:Landroid/widget/TextView;

    .line 485
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->h:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->d()Landroid/widget/LinearLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i:Landroid/widget/LinearLayout;

    .line 486
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->h:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->e()Landroid/widget/LinearLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->j:Landroid/widget/LinearLayout;

    .line 487
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->h:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->f()Landroid/widget/FrameLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->l:Landroid/widget/FrameLayout;

    .line 488
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->h:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->i()Landroid/widget/FrameLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->m:Landroid/widget/FrameLayout;

    .line 489
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->h:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->h()Landroid/widget/LinearLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->n:Landroid/widget/LinearLayout;

    .line 491
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/f;->an:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 492
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 493
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 495
    :cond_0
    return-void
.end method

.method public onGlobalLayout()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/16 v2, 0xb

    const/4 v3, 0x0

    .line 277
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i()Z

    move-result v0

    if-nez v0, :cond_1

    .line 278
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_0

    .line 279
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPivotX(F)V

    .line 280
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPivotY(F)V

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->m:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 289
    :goto_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v2, :cond_2

    .line 290
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/g;->bT:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 293
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e(F)V

    .line 299
    :goto_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_5

    .line 300
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 304
    :goto_2
    return-void

    .line 285
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->h:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->g()Lcom/google/android/gms/smart_profile/header/view/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/header/view/a;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 286
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    .line 295
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g()Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c(F)V

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->t:Landroid/widget/ScrollView;

    invoke-virtual {v0, v3, v3}, Landroid/widget/ScrollView;->scrollTo(II)V

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i()Z

    move-result v0

    if-nez v0, :cond_3

    iget v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b:I

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e(F)V

    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xc

    if-ge v0, v1, :cond_4

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g()Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    new-instance v3, Lcom/google/android/gms/smart_profile/aw;

    invoke-direct {v3, p0, v0, v1, v2}, Lcom/google/android/gms/smart_profile/aw;-><init>(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;FFLandroid/view/animation/AccelerateDecelerateInterpolator;)V

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b(I)J

    move-result-wide v0

    invoke-virtual {v3, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a(I)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_1

    .line 302
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_2
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 312
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    .line 314
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 326
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 317
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->h()V

    goto :goto_0

    .line 320
    :pswitch_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b(F)V

    goto :goto_0

    .line 323
    :pswitch_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a(F)Z

    move-result v0

    goto :goto_1

    .line 314
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 331
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    .line 335
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    move v0, v1

    move v3, v1

    .line 373
    :goto_0
    if-eqz v3, :cond_1

    .line 374
    invoke-static {p0}, Landroid/support/v4/view/ay;->d(Landroid/view/View;)V

    .line 377
    :cond_1
    if-nez v0, :cond_2

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v1, v2

    :cond_3
    return v1

    .line 338
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->v:Landroid/support/v4/widget/x;

    invoke-virtual {v0}, Landroid/support/v4/widget/x;->c()Z

    move-result v0

    .line 339
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->h()V

    move v3, v0

    move v0, v1

    .line 340
    goto :goto_0

    .line 342
    :pswitch_1
    invoke-direct {p0, v3}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b(F)V

    move v0, v1

    move v3, v1

    .line 343
    goto :goto_0

    .line 345
    :pswitch_2
    invoke-direct {p0, v3}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346
    iput-boolean v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->G:Z

    .line 349
    iget v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->B:F

    sub-float v0, v3, v0

    .line 353
    iget v4, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->y:I

    int-to-float v4, v4

    sub-float/2addr v4, v0

    float-to-int v4, v4

    .line 354
    iget v5, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->x:I

    int-to-float v5, v5

    add-float/2addr v5, v0

    float-to-int v5, v5

    .line 355
    iget v6, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->C:F

    add-float/2addr v6, v0

    float-to-int v6, v6

    .line 356
    iget v7, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->A:I

    int-to-float v7, v7

    add-float/2addr v0, v7

    float-to-int v7, v0

    .line 358
    iget v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->B:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_4

    move v0, v2

    :goto_1
    if-nez v0, :cond_5

    move v0, v2

    :goto_2
    if-eqz v0, :cond_d

    .line 360
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->t:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    if-lez v0, :cond_8

    if-gtz v4, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->t:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1, v1}, Landroid/widget/ScrollView;->scrollTo(II)V

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i()Z

    move-result v0

    if-eqz v0, :cond_6

    iput v12, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    :goto_3
    invoke-direct {p0, v3}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b(F)V

    :goto_4
    move v0, v1

    :goto_5
    move v3, v0

    move v0, v2

    .line 368
    goto :goto_0

    :cond_4
    move v0, v1

    .line 358
    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_2

    .line 360
    :cond_6
    iput v11, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->t:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1, v4}, Landroid/widget/ScrollView;->scrollTo(II)V

    iput v10, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    goto :goto_4

    :cond_8
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->n:Landroid/widget/LinearLayout;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b(Landroid/view/View;)I

    move-result v0

    if-gez v0, :cond_a

    if-ltz v7, :cond_9

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->d(F)V

    iget v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a:I

    add-int/2addr v0, v7

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e(F)V

    iput v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    invoke-direct {p0, v3}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b(F)V

    goto :goto_4

    :cond_9
    int-to-float v0, v7

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->d(F)V

    iput v11, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    goto :goto_4

    :cond_a
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i()Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    iget v4, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b:I

    if-ge v0, v4, :cond_c

    iget v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b:I

    if-lt v5, v0, :cond_b

    iget v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b:I

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e(F)V

    iget v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b:I

    sub-int v0, v5, v0

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c(F)V

    iput v12, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    invoke-direct {p0, v3}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b(F)V

    goto :goto_4

    :cond_b
    int-to-float v0, v5

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e(F)V

    iput v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    goto :goto_4

    :cond_c
    int-to-float v0, v6

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c(F)V

    iput v12, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    goto :goto_4

    .line 364
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->j()F

    move-result v8

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-lez v8, :cond_10

    if-gtz v6, :cond_f

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c(F)V

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i()Z

    move-result v0

    if-eqz v0, :cond_e

    iput v10, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    :goto_6
    invoke-direct {p0, v3}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b(F)V

    move v0, v1

    goto/16 :goto_5

    :cond_e
    iput v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    goto :goto_6

    :cond_f
    int-to-float v0, v6

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c(F)V

    iput v12, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    move v0, v1

    goto/16 :goto_5

    :cond_10
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i()Z

    move-result v6

    if-nez v6, :cond_12

    iget-boolean v6, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->H:Z

    if-eqz v6, :cond_12

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v6

    iget v8, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a:I

    if-le v6, v8, :cond_12

    iget v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a:I

    if-gt v5, v0, :cond_11

    iget v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a:I

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e(F)V

    iget v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a:I

    sub-int/2addr v0, v5

    neg-int v0, v0

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->d(F)V

    iput v11, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    invoke-direct {p0, v3}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b(F)V

    move v0, v1

    goto/16 :goto_5

    :cond_11
    int-to-float v0, v5

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e(F)V

    iput v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    move v0, v1

    goto/16 :goto_5

    :cond_12
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i()Z

    move-result v5

    if-nez v5, :cond_14

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->n:Landroid/widget/LinearLayout;

    invoke-static {v5}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b(Landroid/view/View;)I

    move-result v5

    neg-int v6, v0

    if-le v5, v6, :cond_14

    neg-int v4, v0

    if-gt v7, v4, :cond_13

    neg-int v4, v0

    int-to-float v4, v4

    invoke-direct {p0, v4}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->d(F)V

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->t:Landroid/widget/ScrollView;

    add-int/2addr v0, v7

    neg-int v0, v0

    invoke-virtual {v4, v1, v0}, Landroid/widget/ScrollView;->scrollTo(II)V

    iput v10, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    invoke-direct {p0, v3}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b(F)V

    move v0, v1

    goto/16 :goto_5

    :cond_13
    int-to-float v0, v7

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->d(F)V

    iput v11, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    move v0, v1

    goto/16 :goto_5

    :cond_14
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->t:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1, v4}, Landroid/widget/ScrollView;->scrollTo(II)V

    iput v10, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f:I

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f()I

    move-result v0

    if-le v4, v0, :cond_15

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->v:Landroid/support/v4/widget/x;

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f()I

    move-result v3

    sub-int v3, v4, v3

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/x;->a(F)Z

    move-result v0

    goto/16 :goto_5

    :cond_15
    move v0, v1

    goto/16 :goto_5

    .line 335
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
