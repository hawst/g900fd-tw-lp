.class public final Lcom/google/android/gms/plus/service/a/av;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/plus/internal/c;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/c;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/av;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 29
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/av;->b:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/google/android/gms/plus/service/a/av;->c:Lcom/google/android/gms/plus/internal/c;

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 7

    .prologue
    const/16 v6, 0x1f4

    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 36
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/av;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/av;->b:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/plus/c/a;->ai:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    if-nez v0, :cond_3

    :try_start_1
    iget-object v0, p2, Lcom/google/android/gms/plus/b/b;->d:Lcom/google/android/gms/plus/b/c;

    invoke-static {}, Lcom/google/android/gms/plus/b/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3, v2}, Lcom/google/android/gms/plus/b/c;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_1

    .line 37
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/av;->c:Lcom/google/android/gms/plus/internal/c;

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/av;->c:Lcom/google/android/gms/plus/internal/c;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 56
    :cond_0
    :goto_1
    return-void

    .line 36
    :catch_0
    move-exception v0

    instance-of v3, v0, Lcom/android/volley/l;

    if-nez v3, :cond_1

    iget-object v3, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v3, :cond_2

    iget-object v3, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v3, v3, Lcom/android/volley/m;->a:I

    if-lt v3, v6, :cond_2

    :cond_1
    iget-object v3, p2, Lcom/google/android/gms/plus/b/b;->d:Lcom/google/android/gms/plus/b/c;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/plus/b/c;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    :cond_2
    throw v0
    :try_end_2
    .catch Lcom/google/android/gms/auth/q; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_2 .. :try_end_2} :catch_2

    .line 41
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/av;->c:Lcom/google/android/gms/plus/internal/c;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/av;->c:Lcom/google/android/gms/plus/internal/c;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v1, v5, v4, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_1

    .line 36
    :cond_3
    :try_start_3
    iget-object v0, p2, Lcom/google/android/gms/plus/b/b;->d:Lcom/google/android/gms/plus/b/c;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/plus/b/c;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/google/android/gms/auth/q; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 44
    :catch_2
    move-exception v0

    .line 45
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/av;->c:Lcom/google/android/gms/plus/internal/c;

    if-eqz v1, :cond_0

    .line 46
    instance-of v1, v0, Lcom/android/volley/a;

    if-eqz v1, :cond_4

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/av;->c:Lcom/google/android/gms/plus/internal/c;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v1, v5, v4, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_1

    .line 48
    :cond_4
    instance-of v1, v0, Lcom/android/volley/l;

    if-nez v1, :cond_5

    iget-object v1, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v1, :cond_6

    iget-object v0, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v0, v0, Lcom/android/volley/m;->a:I

    if-lt v0, v6, :cond_6

    .line 50
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/av;->c:Lcom/google/android/gms/plus/internal/c;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v4, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_1

    .line 52
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/av;->c:Lcom/google/android/gms/plus/internal/c;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x8

    invoke-direct {v1, v2, v4, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/av;->c:Lcom/google/android/gms/plus/internal/c;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/av;->c:Lcom/google/android/gms/plus/internal/c;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x8

    invoke-direct {v1, v2, v3, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 63
    :cond_0
    return-void
.end method
