.class public final Lcom/google/android/gms/drive/realtime/cache/a/g;
.super Lcom/google/android/gms/drive/database/b;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 71
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/h;->values()[Lcom/google/android/gms/drive/realtime/cache/a/h;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/drive/database/b;-><init>(Landroid/content/Context;Ljava/lang/String;[Lcom/google/android/gms/drive/g/ak;)V

    .line 73
    return-void
.end method


# virtual methods
.method public final onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/database/b;->onOpen(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 79
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->enableWriteAheadLogging()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    const-string v0, "PRAGMA synchronous = normal;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 92
    :cond_0
    return-void
.end method
