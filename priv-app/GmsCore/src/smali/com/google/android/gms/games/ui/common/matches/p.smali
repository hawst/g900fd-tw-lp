.class final Lcom/google/android/gms/games/ui/common/matches/p;
.super Lcom/google/android/gms/games/ui/card/c;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/c;-><init>(Landroid/view/View;)V

    .line 139
    return-void
.end method

.method private A()V
    .locals 4

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/n;

    .line 279
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->d(Lcom/google/android/gms/games/ui/common/matches/n;)Lcom/google/android/gms/games/ui/common/matches/o;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/p;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->c(Lcom/google/android/gms/games/ui/common/matches/n;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->a(Lcom/google/android/gms/games/ui/common/matches/n;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v3, v0}, Lcom/google/android/gms/games/ui/common/matches/o;->a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/games/ui/ac;ILjava/lang/Object;)V
    .locals 12

    .prologue
    .line 135
    check-cast p3, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/card/c;->a(Lcom/google/android/gms/games/ui/ac;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->l()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/games/ui/d/al;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->a(Lcom/google/android/gms/games/ui/common/matches/n;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p3, v1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->y()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->b(Lcom/google/android/gms/games/ui/common/matches/n;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/Participant;->j()Landroid/net/Uri;

    move-result-object v2

    sget v1, Lcom/google/android/gms/h;->ag:I

    :goto_0
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/google/android/gms/games/ui/common/matches/p;->d(Z)V

    invoke-virtual {p0, v2, v1}, Lcom/google/android/gms/games/ui/common/matches/p;->a(Landroid/net/Uri;I)V

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->b(Lcom/google/android/gms/games/ui/common/matches/n;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/p;->c(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->l:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->mJ:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/gms/games/ui/common/matches/p;->b(Ljava/lang/String;)V

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->x()Ljava/lang/String;

    move-result-object v7

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->v()I

    move-result v8

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/n;

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    if-eqz v7, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->b(Lcom/google/android/gms/games/ui/common/matches/n;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_2
    if-ge v1, v2, :cond_0

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->l()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->j()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v10

    move v2, v0

    :goto_3
    if-ge v2, v10, :cond_6

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v7, :cond_5

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_4
    if-nez v11, :cond_1

    if-nez v1, :cond_1

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->j()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_2
    invoke-interface {v3}, Lcom/google/android/gms/games/Game;->k()Landroid/net/Uri;

    move-result-object v2

    sget v1, Lcom/google/android/gms/h;->af:I

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    goto :goto_4

    :cond_6
    const/4 v0, 0x0

    :goto_5
    if-ge v0, v8, :cond_7

    const/4 v1, 0x0

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_7
    sget v0, Lcom/google/android/gms/h;->ag:I

    invoke-virtual {p0, v9, v0}, Lcom/google/android/gms/games/ui/common/matches/p;->a(Ljava/util/ArrayList;I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/p;->e(Z)V

    invoke-virtual {p0, v6}, Lcom/google/android/gms/games/ui/common/matches/p;->i(Ljava/lang/String;)V

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->w()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->l:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->mI:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_6
    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->h()I

    move-result v1

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->g()I

    move-result v2

    const/4 v4, 0x3

    if-ne v1, v4, :cond_8

    const/4 v4, 0x2

    if-ne v2, v4, :cond_8

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->q()Z

    move-result v2

    if-eqz v2, :cond_8

    sget v2, Lcom/google/android/gms/p;->lM:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/common/matches/p;->h(I)V

    sget v2, Lcom/google/android/gms/p;->lN:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/common/matches/p;->i(I)V

    :cond_8
    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Match is in an undefined state."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/p;->l:Landroid/content/Context;

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->m()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    :goto_7
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/matches/p;->g(Ljava/lang/String;)V

    sget v1, Lcom/google/android/gms/f;->T:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/matches/p;->e(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/p;->e(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/c;->k:Lcom/google/android/gms/games/ui/card/a;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/card/a;->g()Landroid/database/CharArrayBuffer;

    move-result-object v1

    invoke-interface {v3, v1}, Lcom/google/android/gms/games/Game;->a(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/matches/p;->b(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/p;->a(Ljava/lang/String;)V

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->w()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/c;->k:Lcom/google/android/gms/games/ui/card/a;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/card/a;->a(F)V

    :cond_a
    sget v0, Lcom/google/android/gms/m;->j:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/p;->g(I)V

    return-void

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/p;->l:Landroid/content/Context;

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->m()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    goto :goto_7

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/p;->l:Landroid/content/Context;

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->m()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Lcom/google/android/gms/games/ui/d/al;->b(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    goto :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 321
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/n;

    .line 322
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/p;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    .line 323
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 324
    sget v4, Lcom/google/android/gms/j;->lP:I

    if-ne v2, v4, :cond_0

    .line 325
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/p;->p()Lcom/google/android/gms/common/data/d;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/data/s;

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/common/data/s;->a(Ljava/lang/String;)V

    .line 327
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->d(Lcom/google/android/gms/games/ui/common/matches/n;)Lcom/google/android/gms/games/ui/common/matches/o;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/o;->b(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    move v0, v3

    .line 337
    :goto_0
    return v0

    .line 329
    :cond_0
    sget v4, Lcom/google/android/gms/j;->lT:I

    if-ne v2, v4, :cond_1

    .line 330
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->d(Lcom/google/android/gms/games/ui/common/matches/n;)Lcom/google/android/gms/games/ui/common/matches/o;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->c(Lcom/google/android/gms/games/ui/common/matches/n;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->a(Lcom/google/android/gms/games/ui/common/matches/n;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v4, v0}, Lcom/google/android/gms/games/ui/common/matches/o;->a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v3

    .line 332
    goto :goto_0

    .line 333
    :cond_1
    sget v4, Lcom/google/android/gms/j;->lR:I

    if-ne v2, v4, :cond_2

    .line 334
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->d(Lcom/google/android/gms/games/ui/common/matches/n;)Lcom/google/android/gms/games/ui/common/matches/o;

    move-result-object v0

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/o;->c(Lcom/google/android/gms/games/Game;)V

    move v0, v3

    .line 335
    goto :goto_0

    .line 337
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final varargs u()V
    .locals 3

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/n;

    .line 287
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/p;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    .line 288
    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->w()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 289
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->l:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->jQ:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 294
    :goto_0
    return-void

    .line 292
    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->d(Lcom/google/android/gms/games/ui/common/matches/n;)Lcom/google/android/gms/games/ui/common/matches/o;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/o;->a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0
.end method

.method public final v()V
    .locals 4

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/n;

    .line 299
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->d(Lcom/google/android/gms/games/ui/common/matches/n;)Lcom/google/android/gms/games/ui/common/matches/o;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/p;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->c(Lcom/google/android/gms/games/ui/common/matches/n;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->a(Lcom/google/android/gms/games/ui/common/matches/n;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v3, v0}, Lcom/google/android/gms/games/ui/common/matches/o;->a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    return-void
.end method

.method public final w()V
    .locals 0

    .prologue
    .line 305
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/p;->A()V

    .line 306
    return-void
.end method

.method public final x()V
    .locals 0

    .prologue
    .line 310
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/p;->A()V

    .line 311
    return-void
.end method

.method public final y()V
    .locals 2

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/p;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/n;

    .line 316
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->d(Lcom/google/android/gms/games/ui/common/matches/n;)Lcom/google/android/gms/games/ui/common/matches/o;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/p;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/common/matches/o;->c(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    .line 317
    return-void
.end method
