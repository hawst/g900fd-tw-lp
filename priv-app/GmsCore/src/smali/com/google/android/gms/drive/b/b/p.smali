.class public final Lcom/google/android/gms/drive/b/b/p;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/io/InputStream;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 3

    .prologue
    .line 75
    new-instance v0, Lcom/google/android/gms/drive/internal/model/File;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/model/File;-><init>()V

    .line 77
    :try_start_0
    new-instance v1, Lcom/google/android/gms/common/server/response/d;

    invoke-direct {v1}, Lcom/google/android/gms/common/server/response/d;-><init>()V

    invoke-virtual {v1, p0, v0}, Lcom/google/android/gms/common/server/response/d;->a(Ljava/io/InputStream;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/common/server/response/m; {:try_start_0 .. :try_end_0} :catch_1

    .line 83
    return-object v0

    .line 78
    :catch_0
    move-exception v0

    .line 79
    new-instance v1, Lcom/google/android/gms/drive/b/b/k;

    const-string v2, "Failed to process contents"

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/b/b/k;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 80
    :catch_1
    move-exception v0

    .line 81
    new-instance v1, Lcom/google/android/gms/drive/b/b/k;

    const-string v2, "Failed to process contents"

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/b/b/k;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Ljava/lang/String;)Ljava/net/URL;
    .locals 4

    .prologue
    .line 92
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/drive/d/a/e;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 95
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 96
    invoke-static {v0}, Lcom/google/android/gms/drive/d/a/e;->a(Landroid/net/Uri$Builder;)V

    .line 97
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    .line 100
    :cond_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 101
    :catch_0
    move-exception v0

    .line 102
    new-instance v1, Lcom/google/android/gms/drive/b/b/k;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid URI: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/b/b/k;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/b/b/n;Lcom/google/android/gms/drive/g/aw;Z)Lcom/google/android/gms/drive/b/b/o;
    .locals 9

    .prologue
    .line 46
    iget-wide v0, p1, Lcom/google/android/gms/drive/b/b/n;->e:J

    .line 47
    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->f()Landroid/content/Context;

    move-result-object v0

    .line 48
    new-instance v4, Lcom/google/android/gms/drive/b/b;

    invoke-direct {v4}, Lcom/google/android/gms/drive/b/b;-><init>()V

    .line 49
    const/4 v1, 0x0

    .line 50
    if-eqz p3, :cond_0

    .line 51
    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->q()Lcom/google/android/gms/drive/b/f;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->y()Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v5

    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->z()Lcom/google/android/gms/drive/b/a/e;

    move-result-object v6

    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->A()Lcom/google/android/gms/drive/b/b/d;

    move-result-object v7

    move-object v1, p1

    move-object v8, p0

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/drive/b/b/a;->a(Landroid/content/Context;Lcom/google/android/gms/drive/b/b/n;Lcom/google/android/gms/drive/b/f;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/b/b;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/google/android/gms/drive/b/a/e;Lcom/google/android/gms/drive/b/b/d;Lcom/google/android/gms/drive/b/b/p;)Lcom/google/android/gms/drive/b/b/a;

    move-result-object v1

    .line 56
    :cond_0
    if-nez v1, :cond_1

    .line 57
    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->A()Lcom/google/android/gms/drive/b/b/d;

    move-result-object v1

    invoke-static {v0, p1, v4, v1, p0}, Lcom/google/android/gms/drive/b/b/g;->a(Landroid/content/Context;Lcom/google/android/gms/drive/b/b/n;Lcom/google/android/gms/drive/b/b;Lcom/google/android/gms/drive/b/b/d;Lcom/google/android/gms/drive/b/b/p;)Lcom/google/android/gms/drive/b/b/g;

    move-result-object v1

    .line 60
    :cond_1
    if-nez v1, :cond_3

    .line 61
    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->w()Lcom/google/android/gms/drive/b/b/e;

    move-result-object v1

    invoke-static {v0, p1, v1, v4, p0}, Lcom/google/android/gms/drive/b/b/h;->a(Landroid/content/Context;Lcom/google/android/gms/drive/b/b/n;Lcom/google/android/gms/drive/b/b/e;Lcom/google/android/gms/drive/b/b;Lcom/google/android/gms/drive/b/b/p;)Lcom/google/android/gms/drive/b/b/h;

    move-result-object v0

    .line 64
    :goto_0
    if-nez v0, :cond_2

    .line 65
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No uploader found:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_2
    return-object v0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method
