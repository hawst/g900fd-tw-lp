.class public final Lcom/google/android/gms/googlehelp/common/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/SharedPreferences$Editor;

.field final synthetic b:Lcom/google/android/gms/googlehelp/common/g;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/googlehelp/common/g;Landroid/content/SharedPreferences$Editor;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/common/h;->b:Lcom/google/android/gms/googlehelp/common/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    .line 81
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/h;->b:Lcom/google/android/gms/googlehelp/common/g;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/googlehelp/common/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 114
    return-object p0
.end method

.method public final a(Ljava/lang/String;I)Lcom/google/android/gms/googlehelp/common/h;
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/h;->b:Lcom/google/android/gms/googlehelp/common/g;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/googlehelp/common/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 90
    return-object p0
.end method

.method public final a(Ljava/lang/String;J)Lcom/google/android/gms/googlehelp/common/h;
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/h;->b:Lcom/google/android/gms/googlehelp/common/g;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/googlehelp/common/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 100
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/h;->b:Lcom/google/android/gms/googlehelp/common/g;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/googlehelp/common/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 85
    return-object p0
.end method

.method public final a(Ljava/lang/String;Z)Lcom/google/android/gms/googlehelp/common/h;
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/h;->b:Lcom/google/android/gms/googlehelp/common/g;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/googlehelp/common/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 95
    return-object p0
.end method

.method public final a(Ljava/util/Map;)Lcom/google/android/gms/googlehelp/common/h;
    .locals 3

    .prologue
    .line 105
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 106
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/googlehelp/common/d;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/d;->a()Lcom/google/android/gms/googlehelp/e/c;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/googlehelp/e/c;->a:Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    goto :goto_0

    .line 109
    :cond_0
    return-object p0
.end method

.method public final b(Ljava/util/Map;)Lcom/google/android/gms/googlehelp/common/h;
    .locals 2

    .prologue
    .line 119
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 120
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/d;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/d;->a()Lcom/google/android/gms/googlehelp/e/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/e/c;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    goto :goto_0

    .line 122
    :cond_0
    return-object p0
.end method
