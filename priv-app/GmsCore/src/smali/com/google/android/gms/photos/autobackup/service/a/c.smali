.class final Lcom/google/android/gms/photos/autobackup/service/a/c;
.super Lcom/google/android/gms/photos/autobackup/service/a/h;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/photos/autobackup/service/a/b;


# direct methods
.method constructor <init>(Lcom/google/android/gms/photos/autobackup/service/a/b;Landroid/content/ComponentName;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/service/a/c;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    invoke-direct {p0, p2}, Lcom/google/android/gms/photos/autobackup/service/a/h;-><init>(Landroid/content/ComponentName;)V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/c;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/service/a/b;->a(Lcom/google/android/gms/photos/autobackup/service/a/b;)Lcom/google/android/gms/photos/autobackup/service/a/f;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/c;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/service/a/b;->a(Lcom/google/android/gms/photos/autobackup/service/a/b;)Lcom/google/android/gms/photos/autobackup/service/a/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/photos/autobackup/service/a/f;->b()V

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/c;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/service/a/b;->c(Lcom/google/android/gms/photos/autobackup/service/a/b;)V

    .line 102
    return-void
.end method

.method protected final a(Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/c;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    invoke-static {p1}, Lcom/google/android/apps/a/a/b;->a(Landroid/os/IBinder;)Lcom/google/android/apps/a/a/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/photos/autobackup/service/a/b;->a(Lcom/google/android/gms/photos/autobackup/service/a/b;Lcom/google/android/apps/a/a/a;)Lcom/google/android/apps/a/a/a;

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/c;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/service/a/b;->a(Lcom/google/android/gms/photos/autobackup/service/a/b;)Lcom/google/android/gms/photos/autobackup/service/a/f;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/c;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/service/a/b;->a(Lcom/google/android/gms/photos/autobackup/service/a/b;)Lcom/google/android/gms/photos/autobackup/service/a/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/photos/autobackup/service/a/f;->a()V

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/c;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/service/a/b;->b(Lcom/google/android/gms/photos/autobackup/service/a/b;)V

    .line 94
    return-void
.end method

.method protected final b()V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/c;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/service/a/b;->a(Lcom/google/android/gms/photos/autobackup/service/a/b;)Lcom/google/android/gms/photos/autobackup/service/a/f;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/c;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/service/a/b;->a(Lcom/google/android/gms/photos/autobackup/service/a/b;)Lcom/google/android/gms/photos/autobackup/service/a/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/photos/autobackup/service/a/f;->c()V

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/c;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/photos/autobackup/service/a/b;->a(Lcom/google/android/gms/photos/autobackup/service/a/b;Lcom/google/android/apps/a/a/a;)Lcom/google/android/apps/a/a/a;

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/c;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/service/a/b;->c(Lcom/google/android/gms/photos/autobackup/service/a/b;)V

    .line 111
    return-void
.end method

.method protected final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    const-string v0, "PhotosClient"

    return-object v0
.end method
