.class public final Lcom/google/android/gms/icing/c/a/i;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/icing/c/a/j;

.field public b:Lcom/google/android/gms/icing/c/a/k;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 32
    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/i;->a:Lcom/google/android/gms/icing/c/a/j;

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/i;->b:Lcom/google/android/gms/icing/c/a/k;

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/i;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/i;->cachedSize:I

    .line 33
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 98
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 99
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/i;->a:Lcom/google/android/gms/icing/c/a/j;

    if-eqz v1, :cond_0

    .line 100
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/i;->a:Lcom/google/android/gms/icing/c/a/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/i;->b:Lcom/google/android/gms/icing/c/a/k;

    if-eqz v1, :cond_1

    .line 104
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/i;->b:Lcom/google/android/gms/icing/c/a/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 45
    if-ne p1, p0, :cond_1

    .line 46
    const/4 v0, 0x1

    .line 70
    :cond_0
    :goto_0
    return v0

    .line 48
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/i;

    if-eqz v1, :cond_0

    .line 51
    check-cast p1, Lcom/google/android/gms/icing/c/a/i;

    .line 52
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/i;->a:Lcom/google/android/gms/icing/c/a/j;

    if-nez v1, :cond_4

    .line 53
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/i;->a:Lcom/google/android/gms/icing/c/a/j;

    if-nez v1, :cond_0

    .line 61
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/i;->b:Lcom/google/android/gms/icing/c/a/k;

    if-nez v1, :cond_5

    .line 62
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/i;->b:Lcom/google/android/gms/icing/c/a/k;

    if-nez v1, :cond_0

    .line 70
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/i;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 57
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/i;->a:Lcom/google/android/gms/icing/c/a/j;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/i;->a:Lcom/google/android/gms/icing/c/a/j;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/c/a/j;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 66
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/i;->b:Lcom/google/android/gms/icing/c/a/k;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/i;->b:Lcom/google/android/gms/icing/c/a/k;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/c/a/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/i;->a:Lcom/google/android/gms/icing/c/a/j;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 78
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/i;->b:Lcom/google/android/gms/icing/c/a/k;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 80
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/i;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    return v0

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/i;->a:Lcom/google/android/gms/icing/c/a/j;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/c/a/j;->hashCode()I

    move-result v0

    goto :goto_0

    .line 78
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/i;->b:Lcom/google/android/gms/icing/c/a/k;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/c/a/k;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/i;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/i;->a:Lcom/google/android/gms/icing/c/a/j;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/icing/c/a/j;

    invoke-direct {v0}, Lcom/google/android/gms/icing/c/a/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/i;->a:Lcom/google/android/gms/icing/c/a/j;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/i;->a:Lcom/google/android/gms/icing/c/a/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/i;->b:Lcom/google/android/gms/icing/c/a/k;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/icing/c/a/k;

    invoke-direct {v0}, Lcom/google/android/gms/icing/c/a/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/i;->b:Lcom/google/android/gms/icing/c/a/k;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/i;->b:Lcom/google/android/gms/icing/c/a/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/i;->a:Lcom/google/android/gms/icing/c/a/j;

    if-eqz v0, :cond_0

    .line 88
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/i;->a:Lcom/google/android/gms/icing/c/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/i;->b:Lcom/google/android/gms/icing/c/a/k;

    if-eqz v0, :cond_1

    .line 91
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/i;->b:Lcom/google/android/gms/icing/c/a/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 93
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 94
    return-void
.end method
