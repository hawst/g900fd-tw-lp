.class final Lcom/google/android/gms/games/ui/d/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/d/w;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/d/w;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lcom/google/android/gms/games/ui/d/y;->a:Lcom/google/android/gms/games/ui/d/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 5

    .prologue
    .line 240
    check-cast p1, Lcom/google/android/gms/people/i;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/y;->a:Lcom/google/android/gms/games/ui/d/w;

    invoke-interface {p1}, Lcom/google/android/gms/people/i;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/people/i;->c()Lcom/google/android/gms/people/model/k;

    move-result-object v2

    iget-boolean v3, v0, Lcom/google/android/gms/games/ui/d/w;->e:Z

    if-eqz v3, :cond_0

    const-string v0, "ManageCirclesHelper"

    const-string v1, "onPeopleLoaded: Canceled! Ignoring this callback..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v2, "ManageCirclesHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onPeopleLoaded: error status: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/d/w;->d()V

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/google/android/gms/games/ui/d/w;->f:Lcom/google/android/gms/games/ui/d/z;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/gms/games/ui/d/w;->f:Lcom/google/android/gms/games/ui/d/z;

    invoke-interface {v1}, Lcom/google/android/gms/games/ui/d/z;->T()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "ManageCirclesHelper"

    const-string v2, "onCirclesLoaded: processing halted at client\'s request"

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/d/w;->d()V

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/gms/people/model/k;->c()I

    move-result v1

    if-lez v1, :cond_3

    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Lcom/google/android/gms/people/model/k;->b(I)Lcom/google/android/gms/people/model/j;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/people/model/j;->d()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/ui/d/w;->c:[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    invoke-virtual {v2}, Lcom/google/android/gms/people/model/k;->w_()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/games/ui/d/w;->d:Z

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/d/w;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/people/model/k;->w_()V

    throw v0
.end method
