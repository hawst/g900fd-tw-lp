.class public final Lcom/google/android/gms/fitness/sensors/a/al;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/fitness/sensors/a/aa;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field private final a:Landroid/bluetooth/BluetoothAdapter;

.field private final b:Ljava/util/concurrent/ConcurrentMap;


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothAdapter;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/a/al;->a:Landroid/bluetooth/BluetoothAdapter;

    .line 27
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/al;->b:Ljava/util/concurrent/ConcurrentMap;

    .line 28
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/fitness/sensors/a/ab;)Z
    .locals 3

    .prologue
    .line 32
    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/al;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/gms/fitness/sensors/a/am;->a(Lcom/google/android/gms/fitness/sensors/a/ab;)Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/al;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    if-eqz v0, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/bluetooth/BluetoothAdapter;->startLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)Z

    move-result v0

    return v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/fitness/sensors/a/ab;)V
    .locals 2

    .prologue
    .line 47
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/al;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    .line 52
    if-eqz v0, :cond_0

    .line 53
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/al;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V

    .line 55
    :cond_0
    return-void
.end method
