.class public final Lcom/google/android/gms/drive/a/a/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/drive/a/a/l;

.field final b:Lcom/google/android/gms/drive/DriveId;

.field final c:Ljava/lang/String;

.field final d:Ljava/lang/String;

.field final e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

.field final f:Lcom/google/android/gms/drive/auth/g;

.field final g:Lcom/google/android/gms/drive/database/model/EntrySpec;

.field final h:Lcom/google/android/gms/drive/a/e;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/a/a/l;Lcom/google/android/gms/drive/DriveId;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/a/e;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/a/l;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/i;->a:Lcom/google/android/gms/drive/a/a/l;

    .line 46
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/i;->b:Lcom/google/android/gms/drive/DriveId;

    .line 47
    iput-object p3, p0, Lcom/google/android/gms/drive/a/a/i;->c:Ljava/lang/String;

    .line 48
    iput-object p4, p0, Lcom/google/android/gms/drive/a/a/i;->d:Ljava/lang/String;

    .line 49
    iput-object p5, p0, Lcom/google/android/gms/drive/a/a/i;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 50
    invoke-static {p6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/g;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/i;->f:Lcom/google/android/gms/drive/auth/g;

    .line 51
    invoke-static {p7}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/EntrySpec;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/i;->g:Lcom/google/android/gms/drive/database/model/EntrySpec;

    .line 52
    invoke-static {p8}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/e;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/i;->h:Lcom/google/android/gms/drive/a/e;

    .line 53
    return-void
.end method
