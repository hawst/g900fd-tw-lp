.class public final Lcom/google/android/gms/wallet/service/r;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/Class;

.field public static final b:Ljava/lang/Class;

.field public static final c:Ljava/lang/Class;

.field public static final d:Ljava/lang/Class;


# instance fields
.field private e:Ljava/util/LinkedHashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Lcom/google/aa/b/a/a/a/a/u;

    sput-object v0, Lcom/google/android/gms/wallet/service/r;->a:Ljava/lang/Class;

    .line 63
    const-class v0, Lcom/google/checkout/inapp/proto/ak;

    sput-object v0, Lcom/google/android/gms/wallet/service/r;->b:Ljava/lang/Class;

    .line 66
    const-class v0, Lcom/google/checkout/inapp/proto/s;

    sput-object v0, Lcom/google/android/gms/wallet/service/r;->c:Ljava/lang/Class;

    .line 69
    const-class v0, Lcom/google/checkout/inapp/proto/ah;

    sput-object v0, Lcom/google/android/gms/wallet/service/r;->d:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v1, Lcom/google/android/gms/wallet/service/s;

    sget-object v0, Lcom/google/android/gms/wallet/b/a;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/wallet/service/s;-><init>(Lcom/google/android/gms/wallet/service/r;I)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    .line 88
    return-void
.end method

.method public static a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 529
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 549
    new-instance v1, Ljava/util/ArrayList;

    const/4 v0, 0x2

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 550
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 551
    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ":"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v0, v4, v3, v6, v5}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 553
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 556
    :cond_1
    return-object v1
.end method

.method private declared-synchronized a(Ljava/lang/String;Ljava/lang/Class;)V
    .locals 5

    .prologue
    .line 575
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v1

    .line 577
    const/4 v0, 0x0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_0

    .line 578
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 577
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 580
    :cond_0
    monitor-exit p0

    return-void

    .line 575
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/String;Ljava/lang/Class;Lcom/google/protobuf/nano/j;)V
    .locals 5

    .prologue
    .line 584
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v1

    .line 586
    const/4 v0, 0x0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_0

    .line 587
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4, p3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 586
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 589
    :cond_0
    monitor-exit p0

    return-void

    .line 584
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static a([Lcom/google/checkout/inapp/proto/j;Ljava/lang/String;Lcom/google/checkout/inapp/proto/j;)V
    .locals 4

    .prologue
    .line 562
    const/4 v0, 0x0

    array-length v1, p0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 563
    aget-object v2, p0, v0

    .line 564
    iget-object v3, v2, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    .line 565
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 566
    iget-boolean v1, v2, Lcom/google/checkout/inapp/proto/j;->f:Z

    iput-boolean v1, p2, Lcom/google/checkout/inapp/proto/j;->f:Z

    .line 567
    aput-object p2, p0, v0

    .line 571
    :cond_0
    return-void

    .line 562
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 100
    monitor-enter p0

    :try_start_0
    invoke-static {p1, p2}, Lcom/google/android/gms/wallet/common/y;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v0

    .line 101
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/nano/j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    if-nez v0, :cond_0

    move-object v0, v1

    .line 116
    :goto_0
    monitor-exit p0

    return-object v0

    .line 104
    :cond_0
    :try_start_1
    instance-of v2, v0, Lcom/google/aa/a/a/a/f;

    if-eqz v2, :cond_1

    .line 105
    new-instance v1, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v2, 0x13

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V

    move-object v0, v1

    goto :goto_0

    .line 107
    :cond_1
    instance-of v2, v0, Lcom/google/checkout/inapp/proto/al;

    if-eqz v2, :cond_2

    .line 108
    new-instance v1, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/4 v2, 0x3

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V

    move-object v0, v1

    goto :goto_0

    .line 110
    :cond_2
    instance-of v2, v0, Lcom/google/checkout/inapp/proto/t;

    if-eqz v2, :cond_3

    .line 111
    new-instance v1, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v2, 0x1b

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V

    move-object v0, v1

    goto :goto_0

    .line 113
    :cond_3
    instance-of v2, v0, Lcom/google/checkout/inapp/proto/ai;

    if-eqz v2, :cond_4

    .line 114
    new-instance v1, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v2, 0x1f

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 116
    goto :goto_0

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V
    .locals 14

    .prologue
    .line 129
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p3

    instance-of v1, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;

    if-eqz v1, :cond_4

    .line 130
    check-cast p3, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;

    .line 133
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    if-eqz v1, :cond_1

    .line 134
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->b:Ljava/lang/Class;

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Lcom/google/protobuf/nano/j;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 503
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 136
    :cond_1
    :try_start_1
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lcom/google/checkout/inapp/proto/j;

    if-eqz v1, :cond_0

    .line 137
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->b:Ljava/lang/Class;

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 140
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->a:Ljava/lang/Class;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v5

    .line 142
    const/4 v1, 0x0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v1

    :goto_1
    if-ge v4, v6, :cond_2

    .line 143
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 144
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/aa/a/a/a/f;

    .line 146
    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v2

    check-cast v2, Lcom/google/aa/a/a/a/f;

    .line 148
    iget-object v3, v2, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v3, v7}, Lcom/google/android/gms/common/util/h;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/checkout/inapp/proto/j;

    iput-object v3, v2, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    .line 153
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 156
    :cond_2
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->d:Ljava/lang/Class;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v5

    .line 158
    const/4 v1, 0x0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v1

    :goto_2
    if-ge v4, v6, :cond_3

    .line 159
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 160
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/checkout/inapp/proto/ai;

    .line 162
    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v2

    check-cast v2, Lcom/google/checkout/inapp/proto/ai;

    .line 164
    iget-object v3, v2, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v3, v7}, Lcom/google/android/gms/common/util/h;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/checkout/inapp/proto/j;

    iput-object v3, v2, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    .line 167
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 171
    :cond_3
    move-object/from16 v0, p2

    instance-of v1, v0, Lcom/google/checkout/inapp/proto/aa;

    if-eqz v1, :cond_0

    .line 172
    check-cast p2, Lcom/google/checkout/inapp/proto/aa;

    .line 174
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/aa;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v1, :cond_0

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/aa;->a:Lcom/google/checkout/inapp/proto/q;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/q;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 177
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->c:Ljava/lang/Class;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v5

    .line 181
    const/4 v1, 0x0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v1

    :goto_3
    if-ge v4, v6, :cond_0

    .line 183
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 184
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/checkout/inapp/proto/t;

    .line 187
    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v2

    check-cast v2, Lcom/google/checkout/inapp/proto/t;

    .line 189
    iget-object v3, v2, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v3, v7}, Lcom/google/android/gms/common/util/h;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/checkout/inapp/proto/j;

    iput-object v3, v2, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    .line 194
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_3

    .line 199
    :cond_4
    move-object/from16 v0, p3

    instance-of v1, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;

    if-eqz v1, :cond_8

    .line 200
    check-cast p2, Lcom/google/checkout/inapp/proto/ap;

    .line 202
    check-cast p3, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;

    .line 205
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    if-eqz v1, :cond_5

    .line 206
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->b:Ljava/lang/Class;

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Lcom/google/protobuf/nano/j;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 129
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 208
    :cond_5
    :try_start_2
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->a:Lcom/google/checkout/inapp/proto/j;

    if-eqz v1, :cond_0

    .line 209
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->b:Ljava/lang/Class;

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 210
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/checkout/inapp/proto/ap;->b:Ljava/lang/String;

    .line 211
    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->a:Lcom/google/checkout/inapp/proto/j;

    .line 214
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->a:Ljava/lang/Class;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v6

    .line 216
    const/4 v1, 0x0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v3, v1

    :goto_4
    if-ge v3, v7, :cond_6

    .line 217
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 218
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/aa/a/a/a/f;

    .line 221
    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v2

    check-cast v2, Lcom/google/aa/a/a/a/f;

    .line 226
    iget-object v8, v2, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    invoke-static {v8, v4, v5}, Lcom/google/android/gms/wallet/service/r;->a([Lcom/google/checkout/inapp/proto/j;Ljava/lang/String;Lcom/google/checkout/inapp/proto/j;)V

    .line 230
    iget-object v8, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v8, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_4

    .line 233
    :cond_6
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->d:Ljava/lang/Class;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v6

    .line 235
    const/4 v1, 0x0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v3, v1

    :goto_5
    if-ge v3, v7, :cond_7

    .line 236
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 237
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/checkout/inapp/proto/ai;

    .line 239
    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v2

    check-cast v2, Lcom/google/checkout/inapp/proto/ai;

    .line 241
    iget-object v8, v2, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    invoke-static {v8, v4, v5}, Lcom/google/android/gms/wallet/service/r;->a([Lcom/google/checkout/inapp/proto/j;Ljava/lang/String;Lcom/google/checkout/inapp/proto/j;)V

    .line 243
    iget-object v8, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v8, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_5

    .line 248
    :cond_7
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/ap;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v1, :cond_0

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/ap;->a:Lcom/google/checkout/inapp/proto/q;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/q;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 250
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->c:Ljava/lang/Class;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v6

    .line 252
    const/4 v1, 0x0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v3, v1

    :goto_6
    if-ge v3, v7, :cond_0

    .line 253
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 254
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/checkout/inapp/proto/t;

    .line 257
    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v2

    check-cast v2, Lcom/google/checkout/inapp/proto/t;

    .line 263
    iget-object v8, v2, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    invoke-static {v8, v4, v5}, Lcom/google/android/gms/wallet/service/r;->a([Lcom/google/checkout/inapp/proto/j;Ljava/lang/String;Lcom/google/checkout/inapp/proto/j;)V

    .line 267
    iget-object v8, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v8, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_6

    .line 271
    :cond_8
    move-object/from16 v0, p3

    instance-of v1, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;

    if-eqz v1, :cond_b

    .line 272
    check-cast p3, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;

    .line 274
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    if-eqz v1, :cond_9

    .line 275
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->b:Ljava/lang/Class;

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    .line 277
    :cond_9
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_0

    .line 278
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->b:Ljava/lang/Class;

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 281
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->a:Ljava/lang/Class;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v5

    .line 283
    const/4 v1, 0x0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v1

    :goto_7
    if-ge v4, v6, :cond_a

    .line 284
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 285
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/aa/a/a/a/f;

    .line 287
    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v2

    check-cast v2, Lcom/google/aa/a/a/a/f;

    .line 289
    iget-object v3, v2, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v3, v7}, Lcom/google/android/gms/common/util/h;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/checkout/inapp/proto/a/b;

    iput-object v3, v2, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    .line 294
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_7

    .line 297
    :cond_a
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->d:Ljava/lang/Class;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v5

    .line 299
    const/4 v1, 0x0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v1

    :goto_8
    if-ge v4, v6, :cond_0

    .line 300
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 301
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/checkout/inapp/proto/ai;

    .line 303
    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v2

    check-cast v2, Lcom/google/checkout/inapp/proto/ai;

    .line 305
    iget-object v3, v2, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v3, v7}, Lcom/google/android/gms/common/util/h;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/checkout/inapp/proto/a/b;

    iput-object v3, v2, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    .line 308
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_8

    .line 311
    :cond_b
    move-object/from16 v0, p3

    instance-of v1, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;

    if-eqz v1, :cond_12

    .line 312
    check-cast p2, Lcom/google/checkout/inapp/proto/ao;

    .line 313
    check-cast p3, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;

    .line 315
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    if-eqz v1, :cond_c

    .line 316
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->b:Ljava/lang/Class;

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    .line 318
    :cond_c
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_0

    .line 319
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->b:Ljava/lang/Class;

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 320
    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/google/checkout/inapp/proto/ao;->b:Ljava/lang/String;

    .line 321
    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    .line 324
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->a:Ljava/lang/Class;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v5

    .line 326
    const/4 v1, 0x0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v4, v1

    :goto_9
    if-ge v4, v8, :cond_f

    .line 327
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 328
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/aa/a/a/a/f;

    .line 331
    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v2

    check-cast v2, Lcom/google/aa/a/a/a/f;

    .line 336
    iget-object v3, v2, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v9, v3

    .line 337
    const/4 v3, 0x0

    :goto_a
    if-ge v3, v9, :cond_d

    .line 338
    iget-object v10, v2, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    aget-object v10, v10, v3

    .line 341
    iget-object v11, v10, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_e

    .line 342
    iget-boolean v9, v10, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    iput-boolean v9, v7, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    .line 343
    iget-object v9, v2, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    aput-object v7, v9, v3

    .line 349
    :cond_d
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_9

    .line 337
    :cond_e
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    .line 352
    :cond_f
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->d:Ljava/lang/Class;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v8

    .line 354
    const/4 v1, 0x0

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v5, v1

    :goto_b
    if-ge v5, v9, :cond_0

    .line 355
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 356
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/checkout/inapp/proto/ai;

    .line 358
    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v3

    check-cast v3, Lcom/google/checkout/inapp/proto/ai;

    .line 361
    iget-object v4, v2, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v10, v4

    .line 362
    const/4 v4, 0x0

    :goto_c
    if-ge v4, v10, :cond_10

    .line 363
    iget-object v11, v2, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    aget-object v11, v11, v4

    .line 365
    iget-object v12, v11, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    invoke-virtual {v12, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_11

    .line 366
    iget-boolean v2, v11, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    iput-boolean v2, v7, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    .line 367
    iget-object v2, v3, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    aput-object v7, v2, v4

    .line 372
    :cond_10
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_b

    .line 362
    :cond_11
    add-int/lit8 v4, v4, 0x1

    goto :goto_c

    .line 375
    :cond_12
    move-object/from16 v0, p3

    instance-of v1, v0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;

    if-eqz v1, :cond_14

    .line 376
    check-cast p3, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;

    .line 377
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->a:Lcom/google/checkout/inapp/proto/al;

    if-eqz v1, :cond_13

    .line 378
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->b:Ljava/lang/Class;

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->a:Lcom/google/checkout/inapp/proto/al;

    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    .line 381
    :cond_13
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->b:Ljava/lang/Class;

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 383
    :cond_14
    move-object/from16 v0, p3

    instance-of v1, v0, Lcom/google/checkout/inapp/proto/an;

    if-eqz v1, :cond_18

    .line 384
    check-cast p3, Lcom/google/checkout/inapp/proto/an;

    .line 385
    move-object/from16 v0, p3

    iget v1, v0, Lcom/google/checkout/inapp/proto/an;->b:I

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    .line 388
    :pswitch_0
    check-cast p2, Lcom/google/checkout/inapp/proto/am;

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/google/checkout/inapp/proto/am;->b:Ljava/lang/String;

    .line 390
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->b:Ljava/lang/Class;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v7

    .line 392
    const/4 v1, 0x0

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v5, v1

    :goto_d
    if-ge v5, v8, :cond_0

    .line 393
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 394
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/checkout/inapp/proto/al;

    .line 396
    iget-object v9, v2, Lcom/google/checkout/inapp/proto/al;->b:[Lcom/google/checkout/inapp/proto/n;

    .line 397
    invoke-static {v9, v6}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/n;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/n;

    move-result-object v10

    .line 400
    if-eqz v10, :cond_17

    .line 401
    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v2

    check-cast v2, Lcom/google/checkout/inapp/proto/al;

    .line 403
    const/4 v3, 0x0

    array-length v11, v9

    move v4, v3

    :goto_e
    if-ge v4, v11, :cond_16

    .line 404
    aget-object v3, v9, v4

    .line 405
    iget-object v12, v3, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    iget-object v12, v12, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    iget-object v13, v10, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    iget-object v13, v13, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_15

    .line 408
    iget-object v12, v3, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v12}, Lcom/google/android/gms/wallet/common/w;->f(Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v12

    .line 412
    invoke-static {v3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v3

    check-cast v3, Lcom/google/checkout/inapp/proto/n;

    .line 414
    iput-object v12, v3, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    .line 415
    iget-object v12, v2, Lcom/google/checkout/inapp/proto/al;->b:[Lcom/google/checkout/inapp/proto/n;

    aput-object v3, v12, v4

    .line 403
    :cond_15
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_e

    .line 418
    :cond_16
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 392
    :cond_17
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_d

    .line 423
    :cond_18
    move-object/from16 v0, p3

    instance-of v1, v0, Lcom/google/aa/b/a/a/a/a/l;

    if-eqz v1, :cond_1b

    .line 424
    check-cast p3, Lcom/google/aa/b/a/a/a/a/l;

    .line 426
    check-cast p2, Lcom/google/aa/b/a/a/a/a/k;

    .line 429
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    array-length v1, v1

    if-nez v1, :cond_0

    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    if-eqz v1, :cond_0

    .line 431
    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    .line 434
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    if-eqz v1, :cond_0

    iget-object v1, v5, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    if-eqz v1, :cond_0

    .line 438
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->a:Ljava/lang/Class;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v6

    .line 440
    const/4 v1, 0x0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v4, v1

    :goto_f
    if-ge v4, v7, :cond_0

    .line 441
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 442
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/aa/a/a/a/f;

    .line 444
    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v2

    check-cast v2, Lcom/google/aa/a/a/a/f;

    .line 446
    iget-object v3, v2, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    iget-object v8, v5, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    invoke-static {v8}, Lcom/google/android/gms/wallet/common/z;->a(Lcom/google/aa/b/a/a/a/a/w;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/google/android/gms/common/util/h;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/checkout/inapp/proto/j;

    iput-object v3, v2, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    .line 452
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v3, :cond_19

    .line 453
    iget-object v3, v2, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    iget-object v8, v5, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v3, v8}, Lcom/google/android/gms/common/util/h;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/checkout/inapp/proto/a/b;

    iput-object v3, v2, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    .line 458
    :cond_19
    iget-object v3, v2, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    if-eqz v3, :cond_1a

    .line 459
    iget-object v3, v2, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    iget-object v8, v2, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    iget-object v8, v8, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    const/4 v9, 0x2

    new-array v9, v9, [I

    fill-array-data v9, :array_0

    invoke-static {v8, v9}, Lcom/google/android/gms/common/util/h;->a([I[I)[I

    move-result-object v8

    iput-object v8, v3, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    .line 464
    iget-object v3, v2, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    invoke-static {}, Lcom/google/aa/b/a/a/a/a/s;->a()[Lcom/google/aa/b/a/a/a/a/s;

    move-result-object v8

    iput-object v8, v3, Lcom/google/aa/b/a/a/a/a/p;->g:[Lcom/google/aa/b/a/a/a/a/s;

    .line 469
    :cond_1a
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_f

    .line 473
    :cond_1b
    move-object/from16 v0, p3

    instance-of v1, v0, Lcom/google/checkout/inapp/proto/v;

    if-eqz v1, :cond_0

    .line 474
    check-cast p3, Lcom/google/checkout/inapp/proto/v;

    .line 477
    move-object/from16 v0, p3

    iget v1, v0, Lcom/google/checkout/inapp/proto/v;->a:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 478
    check-cast p2, Lcom/google/checkout/inapp/proto/u;

    .line 480
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/checkout/inapp/proto/u;->b:Ljava/lang/String;

    .line 482
    sget-object v1, Lcom/google/android/gms/wallet/service/r;->c:Ljava/lang/Class;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v5

    .line 484
    const/4 v1, 0x0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v3, v1

    :goto_10
    if-ge v3, v6, :cond_0

    .line 485
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 486
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/checkout/inapp/proto/t;

    .line 488
    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v2

    check-cast v2, Lcom/google/checkout/inapp/proto/t;

    .line 492
    iget-object v7, v2, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    invoke-static {v7, v4}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/j;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v7

    .line 494
    if-eqz v7, :cond_1c

    .line 495
    const/4 v8, 0x2

    iput v8, v7, Lcom/google/checkout/inapp/proto/j;->h:I

    .line 499
    :cond_1c
    iget-object v7, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v7, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 484
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_10

    .line 385
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 459
    :array_0
    .array-data 4
        0x1
        0x2
    .end array-data
.end method

.method public final declared-synchronized b(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 516
    monitor-enter p0

    :try_start_0
    invoke-static {p1, p2}, Lcom/google/android/gms/wallet/common/y;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v1

    .line 517
    if-nez p3, :cond_1

    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 518
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1, p3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 522
    :goto_1
    monitor-exit p0

    return-void

    .line 517
    :cond_1
    :try_start_1
    instance-of v2, p3, Lcom/google/checkout/inapp/proto/ae;

    if-nez v2, :cond_0

    instance-of v2, p3, Lcom/google/checkout/b/a/c;

    if-nez v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 520
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/r;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 516
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
