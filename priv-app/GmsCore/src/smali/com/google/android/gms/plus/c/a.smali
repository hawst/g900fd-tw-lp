.class public final Lcom/google/android/gms/plus/c/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static A:Lcom/google/android/gms/common/a/d;

.field public static B:Lcom/google/android/gms/common/a/d;

.field public static C:Lcom/google/android/gms/common/a/d;

.field public static D:Lcom/google/android/gms/common/a/d;

.field public static E:Lcom/google/android/gms/common/a/d;

.field public static F:Lcom/google/android/gms/common/a/d;

.field public static G:Lcom/google/android/gms/common/a/d;

.field public static H:Lcom/google/android/gms/common/a/d;

.field public static I:Lcom/google/android/gms/common/a/d;

.field public static J:Lcom/google/android/gms/common/a/d;

.field public static K:Lcom/google/android/gms/common/a/d;

.field public static L:Lcom/google/android/gms/common/a/d;

.field public static M:Lcom/google/android/gms/common/a/d;

.field public static N:Lcom/google/android/gms/common/a/d;

.field public static O:Lcom/google/android/gms/common/a/d;

.field public static P:Lcom/google/android/gms/common/a/d;

.field public static Q:Lcom/google/android/gms/common/a/d;

.field public static R:Lcom/google/android/gms/common/a/d;

.field public static S:Lcom/google/android/gms/common/a/d;

.field public static T:Lcom/google/android/gms/common/a/d;

.field public static U:Lcom/google/android/gms/common/a/d;

.field public static V:Lcom/google/android/gms/common/a/d;

.field public static W:Lcom/google/android/gms/common/a/d;

.field public static X:Lcom/google/android/gms/common/a/d;

.field public static Y:Lcom/google/android/gms/common/a/d;

.field public static Z:Lcom/google/android/gms/common/a/d;

.field public static a:Lcom/google/android/gms/common/a/d;

.field public static aa:Lcom/google/android/gms/common/a/d;

.field public static ab:Lcom/google/android/gms/common/a/d;

.field public static ac:Lcom/google/android/gms/common/a/d;

.field public static ad:Lcom/google/android/gms/common/a/d;

.field public static ae:Lcom/google/android/gms/common/a/d;

.field public static af:Lcom/google/android/gms/common/a/d;

.field public static ag:Lcom/google/android/gms/common/a/d;

.field public static ah:Lcom/google/android/gms/common/a/d;

.field public static ai:Lcom/google/android/gms/common/a/d;

.field public static aj:Lcom/google/android/gms/common/a/d;

.field public static b:Lcom/google/android/gms/common/a/d;

.field public static c:Lcom/google/android/gms/common/a/d;

.field public static d:Lcom/google/android/gms/common/a/d;

.field public static e:Lcom/google/android/gms/common/a/d;

.field public static f:Lcom/google/android/gms/common/a/d;

.field public static g:Lcom/google/android/gms/common/a/d;

.field public static h:Lcom/google/android/gms/common/a/d;

.field public static i:Lcom/google/android/gms/common/a/d;

.field public static j:Lcom/google/android/gms/common/a/d;

.field public static k:Lcom/google/android/gms/common/a/d;

.field public static l:Lcom/google/android/gms/common/a/d;

.field public static m:Lcom/google/android/gms/common/a/d;

.field public static n:Lcom/google/android/gms/common/a/d;

.field public static o:Lcom/google/android/gms/common/a/d;

.field public static p:Lcom/google/android/gms/common/a/d;

.field public static q:Lcom/google/android/gms/common/a/d;

.field public static r:Lcom/google/android/gms/common/a/d;

.field public static s:Lcom/google/android/gms/common/a/d;

.field public static t:Lcom/google/android/gms/common/a/d;

.field public static u:Lcom/google/android/gms/common/a/d;

.field public static v:Lcom/google/android/gms/common/a/d;

.field public static w:Lcom/google/android/gms/common/a/d;

.field public static x:Lcom/google/android/gms/common/a/d;

.field public static y:Lcom/google/android/gms/common/a/d;

.field public static z:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v3, 0x64

    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 23
    const-string v0, "plus.facl.max_age"

    const v1, 0xa4cb800

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->a:Lcom/google/android/gms/common/a/d;

    .line 26
    const-string v0, "plus.cache_enabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->b:Lcom/google/android/gms/common/a/d;

    .line 29
    const-string v0, "plus.verbose_logging"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->c:Lcom/google/android/gms/common/a/d;

    .line 32
    const-string v0, "plus.apiary_trace"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->d:Lcom/google/android/gms/common/a/d;

    .line 39
    const-string v0, "plus.pos_server_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->e:Lcom/google/android/gms/common/a/d;

    .line 42
    const-string v0, "plus.pos_server_api_path"

    const-string v1, "/pos/v1/"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->f:Lcom/google/android/gms/common/a/d;

    .line 45
    const-string v0, "plus.pos_backend_override"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->g:Lcom/google/android/gms/common/a/d;

    .line 57
    const-string v0, "plus.pos_anonymous_api_key"

    const-string v1, "AIzaSyBa9bgzwtnGchlkux96-c5Q_fi19fE1pEA"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->h:Lcom/google/android/gms/common/a/d;

    .line 63
    const-string v0, "plus.whitelisted_server_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->i:Lcom/google/android/gms/common/a/d;

    .line 66
    const-string v0, "plus.whitelisted_server_api_path"

    const-string v1, "/plus/v1whitelisted/"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->j:Lcom/google/android/gms/common/a/d;

    .line 69
    const-string v0, "plus.whitelisted_backend_override"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->k:Lcom/google/android/gms/common/a/d;

    .line 75
    const-string v0, "plus.v1_server_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->l:Lcom/google/android/gms/common/a/d;

    .line 78
    const-string v0, "plus.v1_server_api_path"

    const-string v1, "/plus/v1/"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->m:Lcom/google/android/gms/common/a/d;

    .line 81
    const-string v0, "plus.v1_backend_override"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->n:Lcom/google/android/gms/common/a/d;

    .line 87
    const-string v0, "plus.oauth_server_url"

    const-string v1, "https://accounts.google.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->o:Lcom/google/android/gms/common/a/d;

    .line 90
    const-string v0, "plus.oauth_server_api_path"

    const-string v1, "/o/oauth2"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->p:Lcom/google/android/gms/common/a/d;

    .line 93
    const-string v0, "plus.oauth_backend_override"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->q:Lcom/google/android/gms/common/a/d;

    .line 99
    const-string v0, "plus.connect_backend_override"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->r:Lcom/google/android/gms/common/a/d;

    .line 102
    const-string v0, "plus.connect_server_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->s:Lcom/google/android/gms/common/a/d;

    .line 105
    const-string v0, "plus.connect_server_api_path"

    const-string v1, "/plus/v1whitelisted/"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->t:Lcom/google/android/gms/common/a/d;

    .line 108
    const-string v0, "plus.connect_backend_override"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->u:Lcom/google/android/gms/common/a/d;

    .line 114
    const-string v0, "plus.proto_server_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->v:Lcom/google/android/gms/common/a/d;

    .line 117
    const-string v0, "plus.proto_server_api_path"

    const-string v1, "/plusi/v3/ozInternal"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->w:Lcom/google/android/gms/common/a/d;

    .line 120
    const-string v0, "plus.proto_backend_override"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->x:Lcom/google/android/gms/common/a/d;

    .line 126
    const-string v0, "plus.gcm.sender_id"

    const-string v1, "745476177629"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->y:Lcom/google/android/gms/common/a/d;

    .line 129
    const-string v0, "plus.list_apps.learn_more_url"

    const-string v1, "https://support.google.com/plus/?p=plus_sign_in"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->z:Lcom/google/android/gms/common/a/d;

    .line 132
    const-string v0, "plus.list_apps.help_url"

    const-string v1, "https://support.google.com/plus/?p=plus_controls_mobile"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->A:Lcom/google/android/gms/common/a/d;

    .line 135
    const-string v0, "plus.list_fitness_apps.learn_more_url"

    const-string v1, "https://support.google.com/mobile/?p=google_settings_fitness_devices"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->B:Lcom/google/android/gms/common/a/d;

    .line 139
    const-string v0, "plus.list_apps_fitness._help_url"

    const-string v1, "https://support.google.com/mobile/?p=google_settings_fitness"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->C:Lcom/google/android/gms/common/a/d;

    .line 143
    const-string v0, "plus.list_apps_lso._help_url"

    const-string v1, "https://support.google.com/accounts/answer/3290768"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->D:Lcom/google/android/gms/common/a/d;

    .line 147
    const-string v0, "plus.list_moments.help_url"

    const-string v1, "https://support.google.com/plus/?p=plus_controls_mobile"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->E:Lcom/google/android/gms/common/a/d;

    .line 151
    const-string v0, "plus.moments.manage_web_url"

    const-string v1, "https://support.google.com/plus/?p=plus_controls_mobile"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->F:Lcom/google/android/gms/common/a/d;

    .line 158
    const-string v0, "plus.list_apps.max_items"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->G:Lcom/google/android/gms/common/a/d;

    .line 164
    const-string v0, "plus.list_moments.max_items"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->H:Lcom/google/android/gms/common/a/d;

    .line 174
    const-string v0, "plus.post_max_individual_acls"

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->I:Lcom/google/android/gms/common/a/d;

    .line 180
    const-string v0, "plus.lso_aspen_scope_ids"

    const-string v1, "9335"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->J:Lcom/google/android/gms/common/a/d;

    .line 186
    const-string v0, "plus.lso_conn_read_scope_ids"

    const v1, 0xfd26

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->K:Lcom/google/android/gms/common/a/d;

    .line 192
    const-string v0, "plus.list_apps.disabled_versions"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->L:Lcom/google/android/gms/common/a/d;

    .line 195
    const-string v0, "plus.list_apps.disabled_url"

    const-string v1, "https://support.google.com/plus/?p=plus_controls_mobile"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->M:Lcom/google/android/gms/common/a/d;

    .line 202
    const-string v0, "plus.list_moments.disabled_versions"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->N:Lcom/google/android/gms/common/a/d;

    .line 205
    const-string v0, "plus.list_moments.disabled_url"

    const-string v1, "https://support.google.com/plus/?p=plus_controls_mobile"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->O:Lcom/google/android/gms/common/a/d;

    .line 213
    const-string v0, "plus.manage_app.disabled_versions"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->P:Lcom/google/android/gms/common/a/d;

    .line 216
    const-string v0, "plus.manage_app.disabled_url"

    const-string v1, "https://support.google.com/plus/?p=plus_controls_mobile"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->Q:Lcom/google/android/gms/common/a/d;

    .line 223
    const-string v0, "plus.oob_debugging"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->R:Lcom/google/android/gms/common/a/d;

    .line 230
    const-string v0, "plus.oob_default_birthday"

    const-string v1, "1940-01-01"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->S:Lcom/google/android/gms/common/a/d;

    .line 239
    const-string v0, "plus.oob_last_name_first_countries"

    const-string v1, "*ja*ko*hu*zh*"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->T:Lcom/google/android/gms/common/a/d;

    .line 245
    const-string v0, "plus.share_box.max_people"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->U:Lcom/google/android/gms/common/a/d;

    .line 252
    const-string v0, "plus.share_box.add_to_circle_enabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->V:Lcom/google/android/gms/common/a/d;

    .line 260
    const-string v0, "plus.share_box.add_to_circle_default"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->W:Lcom/google/android/gms/common/a/d;

    .line 266
    const-string v0, "plus.share_box.show_acl_picker_first"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->X:Lcom/google/android/gms/common/a/d;

    .line 272
    const-string v0, "plus.share_box.showcased_suggestion_count"

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->Y:Lcom/google/android/gms/common/a/d;

    .line 278
    const-string v0, "plus.share_box.suggestion_count"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->Z:Lcom/google/android/gms/common/a/d;

    .line 284
    const-string v0, "plus.share_box.client_suggestion_count"

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->aa:Lcom/google/android/gms/common/a/d;

    .line 290
    const-string v0, "plus.share_box.show_suggested_default"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->ab:Lcom/google/android/gms/common/a/d;

    .line 296
    const-string v0, "plus.share_box.include_suggestions_default"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->ac:Lcom/google/android/gms/common/a/d;

    .line 302
    const-string v0, "plus.share_box.show_add_to_circle_default"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->ad:Lcom/google/android/gms/common/a/d;

    .line 308
    const-string v0, "plus.share_box.add_to_circle_max_name_length"

    const/16 v1, 0x32

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->ae:Lcom/google/android/gms/common/a/d;

    .line 314
    const-string v0, "plus.share_box.people_sync_allowance_seconds"

    const-wide/16 v2, 0x7080

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->af:Lcom/google/android/gms/common/a/d;

    .line 321
    const-string v0, "plus.share_box.people_sync_allowance_seconds_sign_in"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->ag:Lcom/google/android/gms/common/a/d;

    .line 327
    const-string v0, "plus.server.timeout_ms.default"

    const/16 v1, 0x9c4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->ah:Lcom/google/android/gms/common/a/d;

    .line 424
    const-string v0, "plus.write_moments.online_disabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->ai:Lcom/google/android/gms/common/a/d;

    .line 428
    const-string v0, "smart_profile.social_cards_count"

    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/c/a;->aj:Lcom/google/android/gms/common/a/d;

    return-void
.end method

.method public static a(ILjava/lang/String;)I
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 345
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 346
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 347
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v3

    .line 350
    array-length v0, v3

    invoke-static {v2, p0, v3, v0, v1}, Lcom/google/android/gms/plus/c/a;->a(Ljava/lang/StringBuilder;I[Ljava/lang/Object;II)V

    .line 351
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 352
    if-gtz v0, :cond_0

    .line 357
    array-length v0, v3

    invoke-static {v2, p0, v3, v0}, Lcom/google/android/gms/plus/c/a;->a(Ljava/lang/StringBuilder;I[Ljava/lang/Object;I)I

    move-result v0

    .line 358
    if-gtz v0, :cond_0

    .line 360
    array-length v1, v3

    :goto_0
    if-lez v1, :cond_0

    .line 361
    add-int/lit8 v0, v1, -0x1

    const-string v4, "*"

    aput-object v4, v3, v0

    .line 362
    invoke-static {v2, p0, v3, v1}, Lcom/google/android/gms/plus/c/a;->a(Ljava/lang/StringBuilder;I[Ljava/lang/Object;I)I

    move-result v0

    .line 363
    if-gtz v0, :cond_0

    .line 364
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 369
    :cond_0
    if-lez v0, :cond_1

    :goto_1
    return v0

    :cond_1
    sget-object v0, Lcom/google/android/gms/plus/c/a;->ah:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1
.end method

.method private static a(Ljava/lang/StringBuilder;I[Ljava/lang/Object;I)I
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 375
    .line 376
    add-int/lit8 v0, p3, -0x1

    move v1, v0

    move v0, v2

    :goto_0
    if-ltz v1, :cond_0

    .line 377
    invoke-static {p0, p1, p2, p3, v1}, Lcom/google/android/gms/plus/c/a;->a(Ljava/lang/StringBuilder;I[Ljava/lang/Object;II)V

    .line 378
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 379
    if-gtz v0, :cond_0

    .line 380
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 383
    :cond_0
    return v0
.end method

.method private static a(Ljava/lang/StringBuilder;I[Ljava/lang/Object;II)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 389
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 391
    const-string v1, "plus.server.timeout_ms."

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    packed-switch p1, :pswitch_data_0

    .line 411
    :goto_0
    if-ge v0, p3, :cond_1

    .line 412
    const/16 v1, 0x2f

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 413
    if-ne v0, p4, :cond_0

    .line 414
    const/16 v1, 0x2a

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 411
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 395
    :pswitch_0
    const-string v1, "DEPRECATED_GET_OR_POST."

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 398
    :pswitch_1
    const-string v1, "GET."

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 401
    :pswitch_2
    const-string v1, "POST."

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 404
    :pswitch_3
    const-string v1, "PUT."

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 407
    :pswitch_4
    const-string v1, "DELETE."

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 416
    :cond_0
    aget-object v1, p2, v0

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 419
    :cond_1
    return-void

    .line 393
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
