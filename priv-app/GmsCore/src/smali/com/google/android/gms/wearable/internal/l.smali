.class public final Lcom/google/android/gms/wearable/internal/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wearable/g;


# instance fields
.field private final a:Lcom/google/android/gms/common/api/Status;

.field private final b:Lcom/google/android/gms/wearable/internal/StorageInfoResponse;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/wearable/internal/StorageInfoResponse;)V
    .locals 0

    .prologue
    .line 321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 322
    iput-object p1, p0, Lcom/google/android/gms/wearable/internal/l;->a:Lcom/google/android/gms/common/api/Status;

    .line 323
    iput-object p2, p0, Lcom/google/android/gms/wearable/internal/l;->b:Lcom/google/android/gms/wearable/internal/StorageInfoResponse;

    .line 324
    return-void
.end method


# virtual methods
.method public final C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/gms/wearable/internal/l;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/wearable/internal/StorageInfoResponse;
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/gms/wearable/internal/l;->b:Lcom/google/android/gms/wearable/internal/StorageInfoResponse;

    return-object v0
.end method
