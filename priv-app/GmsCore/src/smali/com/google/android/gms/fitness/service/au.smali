.class final Lcom/google/android/gms/fitness/service/au;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/fitness/service/aq;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/fitness/service/aq;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/gms/fitness/service/au;->a:Lcom/google/android/gms/fitness/service/aq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/fitness/service/aq;B)V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/service/au;-><init>(Lcom/google/android/gms/fitness/service/aq;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 107
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/au;->a:Lcom/google/android/gms/fitness/service/aq;

    iget-object v0, v0, Lcom/google/android/gms/fitness/service/aq;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 110
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 111
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/l/f;

    .line 113
    iget-object v4, p0, Lcom/google/android/gms/fitness/service/au;->a:Lcom/google/android/gms/fitness/service/aq;

    iget-object v4, v4, Lcom/google/android/gms/fitness/service/aq;->a:Landroid/content/Context;

    invoke-static {v4, v1}, Lcom/google/android/gms/common/util/a;->h(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    .line 114
    if-eqz v4, :cond_0

    .line 115
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/l/f;->a()V

    goto :goto_0

    .line 117
    :cond_0
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 121
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 122
    iget-object v2, p0, Lcom/google/android/gms/fitness/service/au;->a:Lcom/google/android/gms/fitness/service/aq;

    iget-object v2, v2, Lcom/google/android/gms/fitness/service/aq;->a:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/google/android/gms/fitness/l/af;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 123
    iget-object v2, p0, Lcom/google/android/gms/fitness/service/au;->a:Lcom/google/android/gms/fitness/service/aq;

    iget-object v2, v2, Lcom/google/android/gms/fitness/service/aq;->c:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 126
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/au;->a:Lcom/google/android/gms/fitness/service/aq;

    iget-object v0, v0, Lcom/google/android/gms/fitness/service/aq;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/au;->a:Lcom/google/android/gms/fitness/service/aq;

    iget-object v1, v0, Lcom/google/android/gms/fitness/service/aq;->b:Landroid/os/Handler;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->v:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v1, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 131
    :cond_3
    return-void
.end method
