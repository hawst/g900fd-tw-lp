.class final Lcom/google/android/gms/photos/autobackup/r;
.super Lcom/google/android/gms/photos/autobackup/a/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/photos/autobackup/q;


# direct methods
.method constructor <init>(Lcom/google/android/gms/photos/autobackup/q;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/r;->a:Lcom/google/android/gms/photos/autobackup/q;

    invoke-direct {p0}, Lcom/google/android/gms/photos/autobackup/a/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILjava/util/List;)V
    .locals 2

    .prologue
    .line 75
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/r;->a:Lcom/google/android/gms/photos/autobackup/q;

    iget-object v0, v0, Lcom/google/android/gms/photos/autobackup/q;->c:Lcom/google/android/gms/photos/autobackup/p;

    iget-object v0, v0, Lcom/google/android/gms/photos/autobackup/p;->b:Lcom/google/android/gms/photos/autobackup/service/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/service/a/b;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 78
    :goto_0
    if-eqz v0, :cond_0

    .line 79
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/r;->a:Lcom/google/android/gms/photos/autobackup/q;

    iget-object v1, v1, Lcom/google/android/gms/photos/autobackup/q;->a:Lcom/google/android/gms/photos/autobackup/service/a/a;

    invoke-virtual {v1}, Lcom/google/android/gms/photos/autobackup/service/a/a;->a()V

    .line 83
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/r;->a:Lcom/google/android/gms/photos/autobackup/q;

    iget-object v1, v1, Lcom/google/android/gms/photos/autobackup/q;->c:Lcom/google/android/gms/photos/autobackup/p;

    iget-object v1, v1, Lcom/google/android/gms/photos/autobackup/p;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    if-eqz v0, :cond_2

    :goto_1
    invoke-interface {v1, p1, p2}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(ILjava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/r;->a:Lcom/google/android/gms/photos/autobackup/q;

    iget-object v0, v0, Lcom/google/android/gms/photos/autobackup/q;->c:Lcom/google/android/gms/photos/autobackup/p;

    iget-object v0, v0, Lcom/google/android/gms/photos/autobackup/p;->b:Lcom/google/android/gms/photos/autobackup/service/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/service/a/b;->e()V

    .line 91
    return-void

    .line 75
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 83
    :cond_2
    const/16 p1, 0x8

    goto :goto_1

    .line 87
    :catch_0
    move-exception v0

    const-string v0, "GetAutoBackupSettingsOp"

    const-string v1, "failed trying to deliver settings list"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
