.class public final Lcom/google/android/gms/googlehelp/c/a/a;
.super Lcom/google/android/gms/googlehelp/c/a/n;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/c/a/p;Lcom/google/android/gms/googlehelp/common/a;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p4, p3}, Lcom/google/android/gms/googlehelp/c/a/n;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;Lcom/google/android/gms/googlehelp/c/a/p;)V

    .line 23
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 36
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a/a;->b:Lcom/google/android/gms/googlehelp/common/a;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a/a;->b:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {p1}, Lcom/google/android/gms/googlehelp/common/u;->e(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method final a(Lcom/google/protobuf/nano/j;)I
    .locals 2

    .prologue
    .line 27
    instance-of v0, p1, Lcom/google/ad/a/a/b;

    if-eqz v0, :cond_0

    .line 28
    check-cast p1, Lcom/google/ad/a/a/b;

    iget v0, p1, Lcom/google/ad/a/a/b;->a:I

    return v0

    .line 30
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected a ChatRequestStatus object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method final a(Lcom/google/android/gms/googlehelp/common/HelpConfig;I)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/a;->b:Lcom/google/android/gms/googlehelp/common/a;

    if-nez v0, :cond_0

    .line 64
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/a;->b:Lcom/google/android/gms/googlehelp/common/a;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/a;->b()Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/googlehelp/common/u;->g(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;I)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method

.method final a(Lcom/google/android/gms/googlehelp/common/HelpConfig;J)V
    .locals 6

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/a;->a:Landroid/content/Context;

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p0

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/googlehelp/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;JLcom/android/volley/x;Lcom/android/volley/w;)V

    .line 88
    return-void
.end method

.method final a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/protobuf/nano/j;)V
    .locals 2

    .prologue
    .line 77
    instance-of v0, p2, Lcom/google/ad/a/a/b;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/a;->b:Lcom/google/android/gms/googlehelp/common/a;

    check-cast p2, Lcom/google/ad/a/a/b;

    invoke-static {p1, v0, p2}, Lcom/google/android/gms/googlehelp/common/e;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;Lcom/google/ad/a/a/b;)V

    return-void

    .line 81
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected a ChatRequestStatus object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method final b(Lcom/google/android/gms/googlehelp/common/HelpConfig;)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/a;->b:Lcom/google/android/gms/googlehelp/common/a;

    if-nez v0, :cond_0

    move v0, v1

    .line 50
    :goto_0
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/a;->b:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {p1}, Lcom/google/android/gms/googlehelp/common/u;->g(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method final c(Lcom/google/android/gms/googlehelp/common/HelpConfig;)J
    .locals 4

    .prologue
    const-wide/16 v0, -0x1

    .line 68
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/c/a/a;->b:Lcom/google/android/gms/googlehelp/common/a;

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/c/a/a;->b:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {p1}, Lcom/google/android/gms/googlehelp/common/u;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method
