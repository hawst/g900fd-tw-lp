.class public Lcom/google/android/gms/drive/ui/h;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;


# instance fields
.field protected a:Lcom/google/android/gms/common/api/v;

.field private volatile b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/h;->b:Z

    return-void
.end method

.method private e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/h;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 3

    .prologue
    .line 179
    const-string v0, "BaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Connection failed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/common/ew;->a(ILandroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 181
    return-void
.end method

.method public final b()Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/h;->a:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 168
    const-string v0, "BaseActivity"

    const-string v1, "GoogleApiClient connected"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/h;->d()V

    .line 170
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 156
    iget-boolean v0, p0, Lcom/google/android/gms/drive/ui/h;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d()V
    .locals 0

    .prologue
    .line 164
    return-void
.end method

.method public final f_(I)V
    .locals 2

    .prologue
    .line 174
    const-string v0, "BaseActivity"

    const-string v1, "GoogleApiClient connections suspended"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 31
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/h;->b:Z

    .line 34
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/h;->a:Lcom/google/android/gms/common/api/v;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/drive/b;->f:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/b;->d:Lcom/google/android/gms/common/api/Scope;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/b;->e:Lcom/google/android/gms/common/api/Scope;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/x;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/h;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/h;->a:Lcom/google/android/gms/common/api/v;

    .line 44
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 48
    invoke-super {p0}, Landroid/support/v4/app/q;->onDestroy()V

    .line 49
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/h;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 74
    invoke-super {p0}, Landroid/support/v4/app/q;->onPause()V

    .line 75
    return-void
.end method

.method protected onPostResume()V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Landroid/support/v4/app/q;->onPostResume()V

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/h;->b:Z

    .line 69
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 58
    invoke-super {p0}, Landroid/support/v4/app/q;->onResume()V

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/h;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/h;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 62
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/h;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/google/android/gms/drive/g/a;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v1

    const/4 v2, -0x1

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/g/a;->a([Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v0

    if-ne v2, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/h;->finish()V

    .line 63
    :cond_1
    return-void

    .line 62
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 101
    const-string v0, "BaseIsRestart"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 102
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 106
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    iput-boolean v1, p0, Lcom/google/android/gms/drive/ui/h;->b:Z

    .line 109
    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0}, Landroid/support/v4/app/q;->onStart()V

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/h;->b:Z

    .line 90
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/h;->b:Z

    .line 95
    invoke-super {p0}, Landroid/support/v4/app/q;->onStop()V

    .line 96
    return-void
.end method
