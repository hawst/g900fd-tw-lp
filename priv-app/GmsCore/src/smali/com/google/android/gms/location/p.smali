.class public final Lcom/google/android/gms/location/p;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/api/c;

.field public static b:Lcom/google/android/gms/location/d;

.field public static c:Lcom/google/android/gms/location/g;

.field private static final d:Lcom/google/android/gms/common/api/j;

.field private static final e:Lcom/google/android/gms/common/api/i;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/gms/common/api/j;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/p;->d:Lcom/google/android/gms/common/api/j;

    .line 32
    new-instance v0, Lcom/google/android/gms/location/q;

    invoke-direct {v0}, Lcom/google/android/gms/location/q;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/p;->e:Lcom/google/android/gms/common/api/i;

    .line 61
    new-instance v0, Lcom/google/android/gms/common/api/c;

    sget-object v1, Lcom/google/android/gms/location/p;->e:Lcom/google/android/gms/common/api/i;

    sget-object v2, Lcom/google/android/gms/location/p;->d:Lcom/google/android/gms/common/api/j;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/gms/common/api/Scope;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/c;-><init>(Lcom/google/android/gms/common/api/i;Lcom/google/android/gms/common/api/j;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/location/p;->a:Lcom/google/android/gms/common/api/c;

    .line 97
    new-instance v0, Lcom/google/android/gms/location/internal/d;

    invoke-direct {v0}, Lcom/google/android/gms/location/internal/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/p;->b:Lcom/google/android/gms/location/d;

    .line 102
    new-instance v0, Lcom/google/android/gms/location/internal/m;

    invoke-direct {v0}, Lcom/google/android/gms/location/internal/m;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/p;->c:Lcom/google/android/gms/location/g;

    return-void
.end method

.method static synthetic a()Lcom/google/android/gms/common/api/j;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/google/android/gms/location/p;->d:Lcom/google/android/gms/common/api/j;

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/location/internal/w;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 70
    if-eqz p0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "GoogleApiClient parameter is required."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 72
    sget-object v0, Lcom/google/android/gms/location/p;->d:Lcom/google/android/gms/common/api/j;

    invoke-interface {p0, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/j;)Lcom/google/android/gms/common/api/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/w;

    .line 75
    if-eqz v0, :cond_1

    :goto_1
    const-string v2, "GoogleApiClient is not configured to use the LocationServices.API Api. Pass thisinto GoogleApiClient.Builder#addApi() to use this feature."

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 79
    return-object v0

    :cond_0
    move v0, v2

    .line 70
    goto :goto_0

    :cond_1
    move v1, v2

    .line 75
    goto :goto_1
.end method
