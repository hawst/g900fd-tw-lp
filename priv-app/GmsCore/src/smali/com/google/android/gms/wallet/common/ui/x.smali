.class public final Lcom/google/android/gms/wallet/common/ui/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# static fields
.field private static final b:[C


# instance fields
.field final a:Lcom/google/android/gms/wallet/common/ui/y;

.field private final c:Lcom/google/android/gms/wallet/common/ui/y;

.field private d:Landroid/content/Context;

.field private e:Landroid/widget/AdapterView;

.field private f:Lcom/google/android/gms/wallet/common/ui/w;

.field private g:Lcom/google/checkout/inapp/proto/a/b;

.field private h:Z

.field private i:Ljava/util/ArrayList;

.field private j:Z

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 41
    const/4 v0, 0x1

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x4e

    aput-char v2, v0, v1

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/x;->b:[C

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/AdapterView;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/x;->h:Z

    .line 55
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/x;->j:Z

    .line 56
    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/x;->k:I

    .line 57
    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/x;->l:I

    .line 58
    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/x;->m:I

    .line 59
    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/x;->n:I

    .line 60
    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/x;->o:I

    .line 61
    iput-boolean v3, p0, Lcom/google/android/gms/wallet/common/ui/x;->p:Z

    .line 74
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/x;->d:Landroid/content/Context;

    .line 75
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    .line 77
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/y;

    sget v1, Lcom/google/android/gms/p;->zs:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/wallet/common/ui/y;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->c:Lcom/google/android/gms/wallet/common/ui/y;

    .line 80
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/y;

    sget v1, Lcom/google/android/gms/p;->Cy:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/y;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->a:Lcom/google/android/gms/wallet/common/ui/y;

    .line 83
    sget-object v0, Lcom/google/android/gms/r;->aN:[I

    invoke-virtual {p1, p3, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Lcom/google/android/gms/r;->aS:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/x;->k:I

    sget v1, Lcom/google/android/gms/r;->aT:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/x;->j:Z

    sget v1, Lcom/google/android/gms/r;->aR:I

    sget v2, Lcom/google/android/gms/l;->gR:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/x;->l:I

    sget v1, Lcom/google/android/gms/r;->aQ:I

    sget v2, Lcom/google/android/gms/l;->gS:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/x;->m:I

    sget v1, Lcom/google/android/gms/r;->aP:I

    sget v2, Lcom/google/android/gms/l;->gO:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/x;->n:I

    sget v1, Lcom/google/android/gms/r;->aO:I

    sget v2, Lcom/google/android/gms/l;->gP:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/x;->o:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 84
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/x;)Landroid/widget/AdapterView;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/aa;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/aa;->a:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(IZ)V
    .locals 1

    .prologue
    .line 276
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 277
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/x;->d()V

    .line 278
    const/4 p1, 0x0

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    instance-of v0, v0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;

    if-eqz v0, :cond_1

    .line 281
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorExpander;->a(IZ)V

    .line 285
    :goto_0
    return-void

    .line 283
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0, p1}, Landroid/widget/AdapterView;->setSelection(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/x;Lcom/google/checkout/inapp/proto/a/b;)Z
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/x;->b(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/common/ui/x;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->m:I

    return v0
.end method

.method private b(Lcom/google/checkout/inapp/proto/a/b;)Z
    .locals 1

    .prologue
    .line 383
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->j:Z

    if-nez v0, :cond_0

    .line 384
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->b(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    .line 387
    :goto_0
    return v0

    :cond_0
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    array-length v0, v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b()[C
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/x;->b:[C

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/common/ui/x;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->l:I

    return v0
.end method

.method private c()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 315
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 316
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/z;

    .line 317
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/z;->a(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/x;->a:Lcom/google/android/gms/wallet/common/ui/y;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    .line 319
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 317
    goto :goto_0

    :cond_1
    move v0, v1

    .line 319
    goto :goto_0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 324
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/x;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 331
    :cond_0
    :goto_0
    return-void

    .line 327
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/z;

    .line 329
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/aa;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/x;->a:Lcom/google/android/gms/wallet/common/ui/y;

    invoke-direct {v1, v2}, Lcom/google/android/gms/wallet/common/ui/aa;-><init>(Lcom/google/android/gms/wallet/common/ui/y;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/z;->insert(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/common/ui/x;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->h:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/common/ui/x;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->o:I

    return v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 334
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/x;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 341
    :goto_0
    return-void

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/z;

    .line 339
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/z;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/aa;

    .line 340
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/z;->remove(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/google/android/gms/wallet/common/ui/x;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->n:I

    return v0
.end method

.method static synthetic g(Lcom/google/android/gms/wallet/common/ui/x;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->k:I

    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/common/ui/w;)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/x;->f:Lcom/google/android/gms/wallet/common/ui/w;

    .line 233
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/a/b;)V
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/wallet/common/ui/x;->a(Lcom/google/checkout/inapp/proto/a/b;Z)V

    .line 143
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/a/b;Z)V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    const-string v1, "Set addresses before setting the selected address"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/x;->e()V

    .line 152
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/z;

    .line 153
    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/z;->a(Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v0

    .line 154
    if-ltz v0, :cond_0

    .line 155
    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/wallet/common/ui/x;->a(IZ)V

    .line 159
    :goto_0
    return-void

    .line 157
    :cond_0
    const/4 v0, -0x1

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/wallet/common/ui/x;->a(IZ)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 224
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/x;->h:Z

    .line 225
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/z;

    .line 226
    if-eqz v0, :cond_0

    .line 227
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/z;->notifyDataSetChanged()V

    .line 229
    :cond_0
    return-void
.end method

.method public final a([Lcom/google/checkout/inapp/proto/a/b;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 162
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 163
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/aa;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/x;->a:Lcom/google/android/gms/wallet/common/ui/y;

    invoke-direct {v0, v3}, Lcom/google/android/gms/wallet/common/ui/aa;-><init>(Lcom/google/android/gms/wallet/common/ui/y;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    if-eqz p1, :cond_0

    .line 165
    array-length v3, p1

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, p1, v0

    .line 166
    new-instance v5, Lcom/google/android/gms/wallet/common/ui/aa;

    invoke-direct {v5, v4}, Lcom/google/android/gms/wallet/common/ui/aa;-><init>(Lcom/google/checkout/inapp/proto/a/b;)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 169
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->p:Z

    if-eqz v0, :cond_1

    .line 170
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/aa;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/x;->c:Lcom/google/android/gms/wallet/common/ui/y;

    invoke-direct {v0, v3}, Lcom/google/android/gms/wallet/common/ui/aa;-><init>(Lcom/google/android/gms/wallet/common/ui/y;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/x;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 174
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/x;->d()V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setSelection(I)V

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/z;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/z;->notifyDataSetChanged()V

    .line 183
    :goto_1
    return-void

    .line 179
    :cond_2
    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/x;->i:Ljava/util/ArrayList;

    .line 180
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/z;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/x;->d:Landroid/content/Context;

    iget v3, p0, Lcom/google/android/gms/wallet/common/ui/x;->l:I

    invoke-direct {v0, p0, v1, v3, v2}, Lcom/google/android/gms/wallet/common/ui/z;-><init>(Lcom/google/android/gms/wallet/common/ui/x;Landroid/content/Context;ILjava/util/List;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    invoke-virtual {v1, v0}, Landroid/widget/AdapterView;->setAdapter(Landroid/widget/Adapter;)V

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0, p0}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 298
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->p:Z

    if-eqz v0, :cond_0

    .line 299
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->p:Z

    .line 300
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/z;

    .line 301
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/z;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 302
    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/z;->a(Lcom/google/android/gms/wallet/common/ui/z;)Ljava/util/ArrayList;

    move-result-object v0

    .line 303
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/checkout/inapp/proto/a/b;

    .line 304
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 305
    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/x;->a([Lcom/google/checkout/inapp/proto/a/b;)V

    .line 308
    :cond_0
    return-void
.end method

.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 188
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->f:Lcom/google/android/gms/wallet/common/ui/w;

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 193
    instance-of v0, v1, Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 196
    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/x;->b(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 197
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->g:Lcom/google/checkout/inapp/proto/a/b;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/x;->a(Lcom/google/checkout/inapp/proto/a/b;Z)V

    goto :goto_0

    .line 201
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-eq v1, v0, :cond_3

    .line 202
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/x;->f:Lcom/google/android/gms/wallet/common/ui/w;

    move-object v0, v1

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-interface {v2, v0}, Lcom/google/android/gms/wallet/common/ui/w;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    .line 203
    check-cast v1, Lcom/google/checkout/inapp/proto/a/b;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/x;->g:Lcom/google/checkout/inapp/proto/a/b;

    .line 205
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/x;->e()V

    goto :goto_0

    .line 207
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->c:Lcom/google/android/gms/wallet/common/ui/y;

    if-ne v1, v0, :cond_5

    .line 209
    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/x;->g:Lcom/google/checkout/inapp/proto/a/b;

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->f:Lcom/google/android/gms/wallet/common/ui/w;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/w;->a()V

    goto :goto_0

    .line 213
    :cond_5
    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/x;->g:Lcom/google/checkout/inapp/proto/a/b;

    .line 214
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->f:Lcom/google/android/gms/wallet/common/ui/w;

    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/w;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    goto :goto_0
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/x;->f:Lcom/google/android/gms/wallet/common/ui/w;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/w;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    .line 221
    return-void
.end method
