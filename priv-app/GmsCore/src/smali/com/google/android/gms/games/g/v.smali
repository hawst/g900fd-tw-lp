.class public final Lcom/google/android/gms/games/g/v;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Lcom/google/android/gms/games/g/aa;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5437
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 5438
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/g/v;->a:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/g/v;->b:Lcom/google/android/gms/games/g/aa;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/v;->cachedSize:I

    .line 5439
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 5495
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 5496
    iget v1, p0, Lcom/google/android/gms/games/g/v;->a:I

    if-eqz v1, :cond_0

    .line 5497
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/g/v;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5500
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/g/v;->b:Lcom/google/android/gms/games/g/aa;

    if-eqz v1, :cond_1

    .line 5501
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/g/v;->b:Lcom/google/android/gms/games/g/aa;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5504
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5450
    if-ne p1, p0, :cond_1

    .line 5469
    :cond_0
    :goto_0
    return v0

    .line 5453
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/v;

    if-nez v2, :cond_2

    move v0, v1

    .line 5454
    goto :goto_0

    .line 5456
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/v;

    .line 5457
    iget v2, p0, Lcom/google/android/gms/games/g/v;->a:I

    iget v3, p1, Lcom/google/android/gms/games/g/v;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 5458
    goto :goto_0

    .line 5460
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/v;->b:Lcom/google/android/gms/games/g/aa;

    if-nez v2, :cond_4

    .line 5461
    iget-object v2, p1, Lcom/google/android/gms/games/g/v;->b:Lcom/google/android/gms/games/g/aa;

    if-eqz v2, :cond_0

    move v0, v1

    .line 5462
    goto :goto_0

    .line 5465
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/g/v;->b:Lcom/google/android/gms/games/g/aa;

    iget-object v3, p1, Lcom/google/android/gms/games/g/v;->b:Lcom/google/android/gms/games/g/aa;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/aa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 5466
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 5474
    iget v0, p0, Lcom/google/android/gms/games/g/v;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 5476
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/v;->b:Lcom/google/android/gms/games/g/aa;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 5478
    return v0

    .line 5476
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/v;->b:Lcom/google/android/gms/games/g/aa;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/aa;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5380
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/g/v;->a:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/v;->b:Lcom/google/android/gms/games/g/aa;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/g/aa;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/aa;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/v;->b:Lcom/google/android/gms/games/g/aa;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/v;->b:Lcom/google/android/gms/games/g/aa;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 5484
    iget v0, p0, Lcom/google/android/gms/games/g/v;->a:I

    if-eqz v0, :cond_0

    .line 5485
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/g/v;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5487
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/v;->b:Lcom/google/android/gms/games/g/aa;

    if-eqz v0, :cond_1

    .line 5488
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/g/v;->b:Lcom/google/android/gms/games/g/aa;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5490
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5491
    return-void
.end method
