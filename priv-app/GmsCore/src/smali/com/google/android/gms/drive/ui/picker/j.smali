.class public final Lcom/google/android/gms/drive/ui/picker/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/Runnable;


# instance fields
.field private final b:Landroid/os/Handler;

.field private final c:Ljava/util/Map;

.field private volatile d:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 138
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/k;

    invoke-direct {v0}, Lcom/google/android/gms/drive/ui/picker/k;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/j;->a:Ljava/lang/Runnable;

    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/j;->c:Ljava/util/Map;

    .line 151
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/j;->b:Landroid/os/Handler;

    .line 152
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Handler;B)V
    .locals 0

    .prologue
    .line 135
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/ui/picker/j;-><init>(Landroid/os/Handler;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/ui/picker/j;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/j;->c:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/drive/ui/picker/j;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/j;->d:Ljava/lang/Runnable;

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/j;->d:Ljava/lang/Runnable;

    .line 225
    if-eqz v0, :cond_0

    .line 226
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/j;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 228
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/drive/ui/picker/j;)V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/j;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/am;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/picker/j;->b(Lcom/google/android/gms/common/api/am;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/am;)Lcom/google/android/gms/common/api/ap;
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/j;->c:Ljava/util/Map;

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/j;->a:Ljava/lang/Runnable;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/j;->b()V

    .line 194
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 196
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/j;->c:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/j;->c:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/am;Lcom/google/android/gms/common/api/aq;)V
    .locals 1

    .prologue
    .line 160
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/j;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/drive/ui/picker/j;->a(Lcom/google/android/gms/common/api/am;Lcom/google/android/gms/common/api/aq;Ljava/lang/Runnable;)V

    .line 161
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/am;Lcom/google/android/gms/common/api/aq;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/j;->c:Ljava/util/Map;

    invoke-interface {v0, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/j;->b()V

    .line 177
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/l;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/drive/ui/picker/l;-><init>(Lcom/google/android/gms/drive/ui/picker/j;Lcom/google/android/gms/common/api/am;Lcom/google/android/gms/common/api/aq;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 184
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/j;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/api/am;)V
    .locals 2

    .prologue
    .line 206
    invoke-interface {p1}, Lcom/google/android/gms/common/api/am;->b()V

    .line 207
    invoke-interface {p1}, Lcom/google/android/gms/common/api/am;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/j;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 209
    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/j;->a:Ljava/lang/Runnable;

    if-eq v0, v1, :cond_0

    .line 211
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/j;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 214
    :cond_0
    return-void
.end method
