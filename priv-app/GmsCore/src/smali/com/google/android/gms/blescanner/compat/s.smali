.class final Lcom/google/android/gms/blescanner/compat/s;
.super Landroid/bluetooth/le/ScanCallback;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/blescanner/compat/v;


# direct methods
.method constructor <init>(Lcom/google/android/gms/blescanner/compat/v;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/google/android/gms/blescanner/compat/s;->a:Lcom/google/android/gms/blescanner/compat/v;

    invoke-direct {p0}, Landroid/bluetooth/le/ScanCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public final onBatchScanResults(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 160
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/le/ScanResult;

    invoke-static {v0}, Lcom/google/android/gms/blescanner/compat/q;->a(Landroid/bluetooth/le/ScanResult;)Lcom/google/android/gms/blescanner/compat/ScanResult;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 161
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/s;->a:Lcom/google/android/gms/blescanner/compat/v;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/blescanner/compat/v;->a(Ljava/util/List;)V

    .line 164
    :cond_1
    return-void
.end method

.method public final onScanFailed(I)V
    .locals 2

    .prologue
    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "LBluetoothLeScannerCompat::onScanFailed("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/blescanner/b/e;->c(Ljava/lang/String;)V

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/s;->a:Lcom/google/android/gms/blescanner/compat/v;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/blescanner/compat/v;->a(I)V

    .line 170
    return-void
.end method

.method public final onScanResult(ILandroid/bluetooth/le/ScanResult;)V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/s;->a:Lcom/google/android/gms/blescanner/compat/v;

    invoke-static {p2}, Lcom/google/android/gms/blescanner/compat/q;->a(Landroid/bluetooth/le/ScanResult;)Lcom/google/android/gms/blescanner/compat/ScanResult;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/blescanner/compat/v;->a(ILcom/google/android/gms/blescanner/compat/ScanResult;)V

    .line 156
    return-void
.end method
