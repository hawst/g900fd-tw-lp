.class public abstract Lcom/google/android/gms/common/ui/h;
.super Landroid/support/v4/a/j;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/es;
.implements Lcom/google/android/gms/common/et;


# instance fields
.field public a:Lcom/google/android/gms/common/c;

.field private b:Lcom/google/android/gms/common/er;

.field private c:Z

.field private d:Lcom/google/android/gms/common/data/a;

.field private final e:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/support/v4/a/j;-><init>(Landroid/content/Context;)V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/ui/h;->e:Ljava/util/ArrayList;

    .line 36
    return-void
.end method

.method private a(Lcom/google/android/gms/common/data/a;)V
    .locals 2

    .prologue
    .line 95
    iget-boolean v0, p0, Landroid/support/v4/a/j;->r:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/a;->d()V

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/ui/h;->d:Lcom/google/android/gms/common/data/a;

    iput-object p1, p0, Lcom/google/android/gms/common/ui/h;->d:Lcom/google/android/gms/common/data/a;

    iget-boolean v1, p0, Landroid/support/v4/a/j;->p:Z

    if-eqz v1, :cond_2

    invoke-super {p0, p1}, Landroid/support/v4/a/j;->b(Ljava/lang/Object;)V

    :cond_2
    if-eqz v0, :cond_0

    if-eq v0, p1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/common/ui/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/gms/common/ui/h;->c()V

    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/gms/common/ui/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 125
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/common/ui/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/a;->d()V

    .line 125
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/ui/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 129
    return-void
.end method


# virtual methods
.method public final T_()V
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/ui/h;->c:Z

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/common/ui/h;->b:Lcom/google/android/gms/common/er;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/ui/h;->a(Lcom/google/android/gms/common/er;)V

    .line 163
    return-void
.end method

.method public final W_()V
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Landroid/support/v4/a/j;->p:Z

    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/h;->e()V

    .line 171
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/ui/h;->c:Z

    .line 172
    return-void
.end method

.method protected abstract a(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)Lcom/google/android/gms/common/er;
.end method

.method protected final a()V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Landroid/support/v4/a/j;->a()V

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/common/ui/h;->b:Lcom/google/android/gms/common/er;

    invoke-interface {v0}, Lcom/google/android/gms/common/er;->c_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/common/ui/h;->b:Lcom/google/android/gms/common/er;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/ui/h;->a(Lcom/google/android/gms/common/er;)V

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/common/ui/h;->c:Z

    if-nez v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/common/ui/h;->b:Lcom/google/android/gms/common/er;

    invoke-interface {v0}, Lcom/google/android/gms/common/er;->a()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/ui/h;->c:Z

    .line 156
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/common/ui/h;->a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/data/a;)V

    .line 157
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/data/a;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/gms/common/ui/h;->a:Lcom/google/android/gms/common/c;

    .line 139
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/ui/h;->a(Lcom/google/android/gms/common/data/a;)V

    .line 140
    return-void
.end method

.method protected abstract a(Lcom/google/android/gms/common/er;)V
.end method

.method public final synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lcom/google/android/gms/common/data/a;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/ui/h;->a(Lcom/google/android/gms/common/data/a;)V

    return-void
.end method

.method protected final e()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/common/ui/h;->b:Lcom/google/android/gms/common/er;

    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    invoke-virtual {p0, v0, p0, p0}, Lcom/google/android/gms/common/ui/h;->a(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)Lcom/google/android/gms/common/er;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/ui/h;->b:Lcom/google/android/gms/common/er;

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/ui/h;->d:Lcom/google/android/gms/common/data/a;

    if-eqz v0, :cond_1

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/common/ui/h;->d:Lcom/google/android/gms/common/data/a;

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/ui/h;->a(Lcom/google/android/gms/common/data/a;)V

    .line 55
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/h;->i()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/common/ui/h;->d:Lcom/google/android/gms/common/data/a;

    if-nez v0, :cond_3

    .line 56
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/a/j;->a()V

    .line 58
    :cond_3
    return-void
.end method

.method protected final f()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/common/ui/h;->b:Lcom/google/android/gms/common/er;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/ui/h;->b:Lcom/google/android/gms/common/er;

    invoke-interface {v0}, Lcom/google/android/gms/common/er;->c_()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/common/ui/h;->c:Z

    if-eqz v0, :cond_2

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/ui/h;->b:Lcom/google/android/gms/common/er;

    invoke-interface {v0}, Lcom/google/android/gms/common/er;->b()V

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/ui/h;->c:Z

    .line 77
    :cond_2
    return-void
.end method

.method protected final g()V
    .locals 1

    .prologue
    .line 81
    invoke-super {p0}, Landroid/support/v4/a/j;->g()V

    .line 83
    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/h;->f()V

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/common/ui/h;->d:Lcom/google/android/gms/common/data/a;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/common/ui/h;->d:Lcom/google/android/gms/common/data/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/a;->d()V

    .line 87
    invoke-direct {p0}, Lcom/google/android/gms/common/ui/h;->c()V

    .line 89
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/ui/h;->d:Lcom/google/android/gms/common/data/a;

    .line 90
    return-void
.end method
