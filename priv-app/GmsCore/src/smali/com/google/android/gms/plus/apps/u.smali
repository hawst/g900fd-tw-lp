.class public final Lcom/google/android/gms/plus/apps/u;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private b:Ljava/util/ArrayList;

.field private c:Ljava/util/ArrayList;

.field private d:Ljava/lang/String;

.field private e:Lcom/google/android/gms/plus/apps/w;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/u;->b:Ljava/util/ArrayList;

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/u;->c:Ljava/util/ArrayList;

    .line 92
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/u;->d:Ljava/lang/String;

    .line 97
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/u;->a:Landroid/view/LayoutInflater;

    .line 98
    return-void
.end method

.method private c(I)I
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    sub-int v0, p1, v0

    return v0
.end method

.method private d(I)I
    .locals 3

    .prologue
    .line 159
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 160
    const/4 v0, 0x0

    .line 166
    :goto_0
    return v0

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 163
    const/4 v0, 0x2

    goto :goto_0

    .line 165
    :cond_1
    if-ltz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    if-ge p1, v0, :cond_2

    .line 166
    const/4 v0, 0x1

    goto :goto_0

    .line 168
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " outside of expected range"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 126
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/w;

    iget v0, v0, Lcom/google/android/gms/plus/apps/w;->b:I

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/w;

    .line 127
    :goto_1
    if-eqz v0, :cond_2

    .line 128
    iput-object v0, p0, Lcom/google/android/gms/plus/apps/u;->e:Lcom/google/android/gms/plus/apps/w;

    return-void

    .line 126
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 130
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Filter Id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/u;->d:Ljava/lang/String;

    .line 110
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->b:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/gms/plus/apps/w;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/plus/apps/w;-><init>(Lcom/google/android/gms/plus/apps/u;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    .line 116
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 117
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/l;

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 119
    :cond_0
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 334
    const/4 v0, 0x0

    return v0
.end method

.method public final b(I)Lcom/google/android/gms/plus/apps/v;
    .locals 5

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->e:Lcom/google/android/gms/plus/apps/w;

    if-nez v0, :cond_1

    .line 185
    :cond_0
    const/4 v0, -0x1

    move v1, v0

    .line 189
    :goto_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/apps/u;->d(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 203
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Item position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not within range 0 to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/u;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->e:Lcom/google/android/gms/plus/apps/w;

    iget v0, v0, Lcom/google/android/gms/plus/apps/w;->b:I

    move v1, v0

    goto :goto_0

    .line 191
    :pswitch_0
    new-instance v1, Lcom/google/android/gms/plus/apps/v;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/w;

    iget v0, v0, Lcom/google/android/gms/plus/apps/w;->b:I

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/u;->f:Ljava/lang/String;

    invoke-direct {v1, p0, v2, v0, v3}, Lcom/google/android/gms/plus/apps/v;-><init>(Lcom/google/android/gms/plus/apps/u;IILjava/lang/String;)V

    move-object v0, v1

    .line 199
    :goto_1
    return-object v0

    .line 195
    :pswitch_1
    new-instance v0, Lcom/google/android/gms/plus/apps/v;

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/u;->f:Ljava/lang/String;

    invoke-direct {v0, p0, v2, v1, v3}, Lcom/google/android/gms/plus/apps/v;-><init>(Lcom/google/android/gms/plus/apps/u;IILjava/lang/String;)V

    goto :goto_1

    .line 199
    :pswitch_2
    new-instance v2, Lcom/google/android/gms/plus/apps/v;

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->c:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/apps/u;->c(I)I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/l;

    iget-object v0, v0, Lcom/google/android/gms/plus/apps/l;->a:Ljava/lang/String;

    invoke-direct {v2, p0, v3, v1, v0}, Lcom/google/android/gms/plus/apps/v;-><init>(Lcom/google/android/gms/plus/apps/u;IILjava/lang/String;)V

    move-object v0, v2

    goto :goto_1

    .line 189
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/u;->f:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v4, 0x8

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 272
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->a:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->eK:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 278
    sget v0, Lcom/google/android/gms/j;->fd:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 279
    sget v1, Lcom/google/android/gms/j;->eZ:I

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 280
    iget-object v2, p0, Lcom/google/android/gms/plus/apps/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne p1, v2, :cond_5

    move v5, v6

    .line 281
    :goto_0
    if-eqz v1, :cond_0

    .line 282
    if-eqz v5, :cond_6

    move v2, v3

    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 284
    :cond_0
    if-eqz v0, :cond_1

    .line 285
    if-eqz v5, :cond_7

    :goto_2
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 289
    :cond_1
    if-nez v5, :cond_4

    .line 290
    sget v0, Lcom/google/android/gms/j;->fc:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 291
    if-eqz v0, :cond_4

    .line 292
    const-string v1, ""

    .line 295
    iget-object v2, p0, Lcom/google/android/gms/plus/apps/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, p1, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 296
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/apps/w;

    iget-object v1, v1, Lcom/google/android/gms/plus/apps/w;->a:Ljava/lang/String;

    .line 298
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/apps/u;->d(I)I

    move-result v2

    if-ne v2, v6, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/u;->c:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/apps/u;->c(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/apps/l;

    iget-object v1, v1, Lcom/google/android/gms/plus/apps/l;->a:Ljava/lang/String;

    .line 301
    :cond_3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/apps/u;->d(I)I

    move-result v0

    if-ne v0, v6, :cond_4

    .line 305
    sget v0, Lcom/google/android/gms/j;->fb:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 307
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/u;->c:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/apps/u;->c(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/apps/l;

    iget-object v1, v1, Lcom/google/android/gms/plus/apps/l;->b:Landroid/graphics/Bitmap;

    .line 309
    if-nez v1, :cond_8

    .line 310
    sget v1, Lcom/google/android/gms/plus/apps/k;->i:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 315
    :goto_3
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 319
    :cond_4
    return-object v7

    :cond_5
    move v5, v3

    .line 280
    goto :goto_0

    :cond_6
    move v2, v4

    .line 282
    goto :goto_1

    :cond_7
    move v4, v3

    .line 285
    goto :goto_2

    .line 313
    :cond_8
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_3
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/apps/u;->b(I)Lcom/google/android/gms/plus/apps/v;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 210
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 219
    const/4 v0, -0x1

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 224
    .line 225
    if-nez p2, :cond_0

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 228
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->a:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->eJ:I

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 238
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->e:Lcom/google/android/gms/plus/apps/w;

    if-eqz v0, :cond_5

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->e:Lcom/google/android/gms/plus/apps/w;

    iget-object v0, v0, Lcom/google/android/gms/plus/apps/w;->a:Ljava/lang/String;

    move-object v1, v0

    .line 246
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->f:Ljava/lang/String;

    .line 249
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/apps/u;->d(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_6

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->c:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/apps/u;->c(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/l;

    iget-object v0, v0, Lcom/google/android/gms/plus/apps/l;->a:Ljava/lang/String;

    move-object v2, v0

    .line 252
    :goto_2
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/apps/u;->d(I)I

    move-result v0

    if-nez v0, :cond_1

    .line 253
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/w;

    iget-object v1, v0, Lcom/google/android/gms/plus/apps/w;->a:Ljava/lang/String;

    .line 256
    :cond_1
    sget v0, Lcom/google/android/gms/j;->rY:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 257
    if-eqz v0, :cond_2

    .line 258
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 260
    :cond_2
    sget v0, Lcom/google/android/gms/j;->f:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 261
    if-eqz v0, :cond_3

    .line 262
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/u;->notifyDataSetChanged()V

    .line 267
    return-object p2

    .line 231
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->a:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->eG:I

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 241
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->a:Landroid/view/LayoutInflater;

    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    sget v1, Lcom/google/android/gms/j;->gg:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 243
    invoke-virtual {v0}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_6
    move-object v2, v0

    goto :goto_2
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 324
    const/4 v0, 0x1

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 329
    const/4 v0, 0x0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
