.class public Lcom/google/android/gms/people/service/PeopleRequestProcessor;
.super Lcom/google/android/gms/common/app/a;
.source "SourceFile"


# static fields
.field public static final b:Lcom/google/android/gms/people/service/c;

.field private static final c:Ljava/util/concurrent/ConcurrentLinkedQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/service/PeopleRequestProcessor;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 26
    new-instance v0, Lcom/google/android/gms/people/service/n;

    invoke-direct {v0}, Lcom/google/android/gms/people/service/n;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/service/PeopleRequestProcessor;->b:Lcom/google/android/gms/people/service/c;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/gms/people/a/a;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/app/a;-><init>(I)V

    .line 37
    return-void
.end method

.method static synthetic a()Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/android/gms/people/service/PeopleRequestProcessor;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 41
    sget-object v0, Lcom/google/android/gms/people/service/PeopleRequestProcessor;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/service/a;

    .line 42
    if-nez v0, :cond_0

    .line 43
    const-string v0, "PeopleRP"

    const-string v1, "No operation found when processing!"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    :goto_0
    return-void

    .line 47
    :cond_0
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/people/service/a;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/service/a;->a(Z)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/service/a;->a(Z)V

    throw v1
.end method
