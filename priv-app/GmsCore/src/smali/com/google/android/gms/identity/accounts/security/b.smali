.class final Lcom/google/android/gms/identity/accounts/security/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljavax/crypto/SecretKey;

.field b:Ljavax/crypto/SecretKey;

.field private final c:Landroid/content/SharedPreferences;

.field private final d:Lcom/google/android/gms/identity/accounts/security/i;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Lcom/google/android/gms/identity/accounts/security/i;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-string v0, "Preferences must not be null."

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/gms/identity/accounts/security/b;->c:Landroid/content/SharedPreferences;

    .line 60
    const-string v0, "Secret key wrapper must not be null."

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/identity/accounts/security/i;

    iput-object v0, p0, Lcom/google/android/gms/identity/accounts/security/b;->d:Lcom/google/android/gms/identity/accounts/security/i;

    .line 61
    const-string v0, "Secret key wrapper must not be null."

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/identity/accounts/security/b;->e:Ljava/lang/String;

    .line 62
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;)Ljavax/crypto/SecretKey;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 110
    iget-object v1, p0, Lcom/google/android/gms/identity/accounts/security/b;->c:Landroid/content/SharedPreferences;

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 111
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 114
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/security/b;->d:Lcom/google/android/gms/identity/accounts/security/i;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/identity/accounts/security/b;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/identity/accounts/security/i;->a([BLjava/lang/String;)Ljavax/crypto/SecretKey;

    move-result-object v0

    goto :goto_0
.end method

.method final a(Ljava/lang/String;Ljavax/crypto/SecretKey;)V
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/security/b;->d:Lcom/google/android/gms/identity/accounts/security/i;

    invoke-interface {v0, p2}, Lcom/google/android/gms/identity/accounts/security/i;->a(Ljavax/crypto/SecretKey;)[B

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/google/android/gms/identity/accounts/security/b;->c:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {v0, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 88
    return-void
.end method
