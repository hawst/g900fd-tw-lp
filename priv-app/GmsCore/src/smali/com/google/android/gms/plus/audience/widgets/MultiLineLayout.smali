.class public Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    iput v0, p0, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->a:I

    .line 17
    iput v0, p0, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->b:I

    .line 21
    return-void
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 137
    invoke-static {}, Lcom/google/android/gms/common/util/ag;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getPaddingStart()I

    move-result v0

    sub-int v0, p1, v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getPaddingStart()I

    move-result v0

    goto :goto_0
.end method

.method private static a(IIII)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 151
    invoke-static {}, Lcom/google/android/gms/common/util/ag;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    sub-int v2, p0, p1

    if-ge v2, p2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    add-int v2, p0, p1

    if-gt v2, p3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private b(I)I
    .locals 2

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getPaddingStart()I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getPaddingEnd()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public getPaddingEnd()I
    .locals 1

    .prologue
    .line 133
    invoke-static {}, Lcom/google/android/gms/common/util/ag;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getPaddingLeft()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getPaddingRight()I

    move-result v0

    goto :goto_0
.end method

.method public getPaddingStart()I
    .locals 1

    .prologue
    .line 128
    invoke-static {}, Lcom/google/android/gms/common/util/ag;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getPaddingRight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getPaddingLeft()I

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 15

    .prologue
    .line 25
    sub-int v4, p4, p2

    .line 26
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getPaddingStart()I

    move-result v5

    .line 27
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getPaddingTop()I

    move-result v6

    .line 29
    const/4 v3, 0x0

    .line 30
    invoke-direct {p0, v4}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->a(I)I

    move-result v2

    .line 31
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getPaddingTop()I

    move-result v1

    .line 32
    invoke-direct {p0, v4}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->b(I)I

    move-result v7

    .line 33
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getChildCount()I

    move-result v8

    .line 34
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->a:I

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->b:I

    .line 36
    const/4 v0, 0x0

    move v14, v0

    move v0, v1

    move v1, v2

    move v2, v3

    move v3, v14

    :goto_0
    if-ge v3, v8, :cond_4

    .line 37
    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 38
    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    move-result v10

    const/16 v11, 0x8

    if-eq v10, v11, :cond_2

    .line 39
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    .line 43
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    .line 44
    iget v12, p0, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->b:I

    if-ge v12, v11, :cond_0

    .line 45
    iput v11, p0, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->b:I

    .line 48
    :cond_0
    invoke-static {v1, v10, v5, v7}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->a(IIII)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 49
    iget v1, p0, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->a:I

    .line 50
    invoke-direct {p0, v4}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->a(I)I

    move-result v1

    .line 51
    add-int/2addr v2, v6

    add-int/2addr v0, v2

    .line 52
    const/4 v2, 0x0

    .line 55
    :cond_1
    invoke-static {}, Lcom/google/android/gms/common/util/ag;->a()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 56
    sub-int v12, v1, v10

    add-int v13, v0, v11

    invoke-virtual {v9, v12, v0, v1, v13}, Landroid/view/View;->layout(IIII)V

    .line 57
    add-int v9, v10, v5

    sub-int/2addr v1, v9

    .line 62
    :goto_1
    invoke-static {v2, v11}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 36
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 59
    :cond_3
    add-int v12, v1, v10

    add-int v13, v0, v11

    invoke-virtual {v9, v1, v0, v12, v13}, Landroid/view/View;->layout(IIII)V

    .line 60
    add-int v9, v10, v5

    add-int/2addr v1, v9

    goto :goto_1

    .line 65
    :cond_4
    return-void
.end method

.method protected onMeasure(II)V
    .locals 17

    .prologue
    .line 69
    const/4 v8, 0x0

    .line 70
    const/4 v7, 0x0

    .line 72
    const v3, 0x7fffffff

    move/from16 v0, p1

    invoke-static {v3, v0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->resolveSize(II)I

    move-result v10

    .line 73
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getPaddingStart()I

    move-result v11

    .line 74
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getPaddingTop()I

    move-result v12

    .line 76
    const/4 v6, 0x0

    .line 77
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->a(I)I

    move-result v5

    .line 78
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getPaddingTop()I

    move-result v4

    .line 79
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->b(I)I

    move-result v13

    .line 80
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getChildCount()I

    move-result v14

    .line 81
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->a:I

    .line 82
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->b:I

    .line 83
    const/4 v3, 0x0

    move v9, v3

    move v3, v4

    move v4, v5

    move v5, v6

    :goto_0
    if-ge v9, v14, :cond_4

    .line 84
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 85
    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v15

    const/16 v16, 0x8

    move/from16 v0, v16

    if-eq v15, v0, :cond_6

    .line 86
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v6, v1, v2}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->measureChild(Landroid/view/View;II)V

    .line 91
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    .line 92
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v16

    .line 93
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->b:I

    move/from16 v0, v16

    if-ge v6, v0, :cond_0

    .line 94
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->b:I

    .line 97
    :cond_0
    invoke-static {v4, v15, v11, v13}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->a(IIII)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 98
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->a:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->a:I

    .line 99
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->a(I)I

    move-result v4

    .line 100
    add-int/2addr v5, v12

    add-int/2addr v3, v5

    .line 101
    const/4 v5, 0x0

    .line 104
    :cond_1
    invoke-static {}, Lcom/google/android/gms/common/util/ag;->a()Z

    move-result v6

    if-eqz v6, :cond_2

    sub-int v6, v4, v15

    :goto_1
    invoke-static {v8, v6}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 106
    add-int v6, v3, v16

    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 108
    invoke-static {}, Lcom/google/android/gms/common/util/ag;->a()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 109
    add-int v7, v15, v11

    sub-int/2addr v4, v7

    .line 113
    :goto_2
    move/from16 v0, v16

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v5

    move v7, v8

    .line 83
    :goto_3
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    move v8, v7

    move v7, v6

    goto :goto_0

    .line 104
    :cond_2
    add-int v6, v4, v15

    goto :goto_1

    .line 111
    :cond_3
    add-int v7, v15, v11

    add-int/2addr v4, v7

    goto :goto_2

    .line 116
    :cond_4
    invoke-static {}, Lcom/google/android/gms/common/util/ag;->a()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 117
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getPaddingEnd()I

    move-result v3

    sub-int v3, v8, v3

    .line 121
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getPaddingBottom()I

    move-result v4

    add-int/2addr v4, v7

    .line 122
    move/from16 v0, p1

    invoke-static {v3, v0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->resolveSize(II)I

    move-result v3

    move/from16 v0, p2

    invoke-static {v4, v0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->resolveSize(II)I

    move-result v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->setMeasuredDimension(II)V

    .line 124
    return-void

    .line 119
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->getPaddingEnd()I

    move-result v3

    add-int/2addr v3, v8

    goto :goto_4

    :cond_6
    move v6, v7

    move v7, v8

    goto :goto_3
.end method
