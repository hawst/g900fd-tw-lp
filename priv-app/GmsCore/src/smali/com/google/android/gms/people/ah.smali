.class public Lcom/google/android/gms/people/ah;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;

.field private static b:Lcom/google/android/gms/people/ah;


# instance fields
.field private final c:Landroid/content/Context;

.field private d:Lcom/google/android/gms/people/a/c;

.field private e:Lcom/google/android/gms/people/debug/d;

.field private f:Lcom/google/android/gms/people/c/a/b;

.field private g:Lcom/google/android/gms/people/c/f;

.field private h:Lcom/google/android/gms/people/service/e;

.field private i:Lcom/google/android/gms/people/service/h;

.field private j:Lcom/google/android/gms/people/service/m;

.field private k:Lcom/google/android/gms/people/sync/u;

.field private l:Lcom/google/android/gms/people/sync/a;

.field private m:Lcom/google/android/gms/people/f/d;

.field private n:Lcom/google/android/gms/people/f/f;

.field private o:Lcom/google/android/gms/people/sync/c;

.field private p:Lcom/google/android/gms/people/sync/d;

.field private q:Lcom/google/android/gms/people/f/i;

.field private r:Lcom/google/android/gms/people/f/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/android/gms/people/ah;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/ah;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/ah;->c:Landroid/content/Context;

    .line 57
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;
    .locals 3

    .prologue
    .line 60
    const-class v1, Lcom/google/android/gms/people/ah;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/people/ah;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 61
    instance-of v2, v0, Lcom/google/android/gms/people/ah;

    if-eqz v2, :cond_0

    .line 62
    check-cast v0, Lcom/google/android/gms/people/ah;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    :goto_0
    monitor-exit v1

    return-object v0

    .line 64
    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/android/gms/people/ah;->b:Lcom/google/android/gms/people/ah;

    if-nez v0, :cond_1

    .line 65
    new-instance v0, Lcom/google/android/gms/people/ah;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/ah;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/people/ah;->b:Lcom/google/android/gms/people/ah;

    .line 67
    :cond_1
    sget-object v0, Lcom/google/android/gms/people/ah;->b:Lcom/google/android/gms/people/ah;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/google/android/gms/people/a/c;
    .locals 4

    .prologue
    .line 76
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->d:Lcom/google/android/gms/people/a/c;

    if-nez v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->c:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/a/c;

    const-string v2, "gms.people"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/people/a/c;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    iput-object v1, p0, Lcom/google/android/gms/people/ah;->d:Lcom/google/android/gms/people/a/c;

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->d:Lcom/google/android/gms/people/a/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Lcom/google/android/gms/people/debug/d;
    .locals 2

    .prologue
    .line 83
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->e:Lcom/google/android/gms/people/debug/d;

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->c:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/debug/d;

    invoke-direct {v1, v0}, Lcom/google/android/gms/people/debug/d;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/people/ah;->e:Lcom/google/android/gms/people/debug/d;

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->e:Lcom/google/android/gms/people/debug/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Lcom/google/android/gms/people/c/a/b;
    .locals 2

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->f:Lcom/google/android/gms/people/c/a/b;

    if-nez v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->c:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/c/a/b;

    invoke-direct {v1, v0}, Lcom/google/android/gms/people/c/a/b;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/people/ah;->f:Lcom/google/android/gms/people/c/a/b;

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->f:Lcom/google/android/gms/people/c/a/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Lcom/google/android/gms/people/c/f;
    .locals 1

    .prologue
    .line 98
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->g:Lcom/google/android/gms/people/c/f;

    if-nez v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/c/f;->b(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/ah;->g:Lcom/google/android/gms/people/c/f;

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->g:Lcom/google/android/gms/people/c/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Lcom/google/android/gms/people/service/e;
    .locals 1

    .prologue
    .line 105
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->h:Lcom/google/android/gms/people/service/e;

    if-nez v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/service/e;->b(Landroid/content/Context;)Lcom/google/android/gms/people/service/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/ah;->h:Lcom/google/android/gms/people/service/e;

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->h:Lcom/google/android/gms/people/service/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Lcom/google/android/gms/people/service/h;
    .locals 1

    .prologue
    .line 112
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->i:Lcom/google/android/gms/people/service/h;

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/service/h;->b(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/ah;->i:Lcom/google/android/gms/people/service/h;

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->i:Lcom/google/android/gms/people/service/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 112
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()Lcom/google/android/gms/people/service/m;
    .locals 1

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->j:Lcom/google/android/gms/people/service/m;

    if-nez v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/service/m;->b(Landroid/content/Context;)Lcom/google/android/gms/people/service/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/ah;->j:Lcom/google/android/gms/people/service/m;

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->j:Lcom/google/android/gms/people/service/m;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()Lcom/google/android/gms/people/sync/u;
    .locals 2

    .prologue
    .line 127
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->k:Lcom/google/android/gms/people/sync/u;

    if-nez v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->c:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/sync/u;

    invoke-direct {v1, v0}, Lcom/google/android/gms/people/sync/u;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/people/ah;->k:Lcom/google/android/gms/people/sync/u;

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->k:Lcom/google/android/gms/people/sync/u;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized i()Lcom/google/android/gms/people/f/d;
    .locals 1

    .prologue
    .line 134
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->m:Lcom/google/android/gms/people/f/d;

    if-nez v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/f/d;->b(Landroid/content/Context;)Lcom/google/android/gms/people/f/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/ah;->m:Lcom/google/android/gms/people/f/d;

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->m:Lcom/google/android/gms/people/f/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final j()Lcom/google/android/gms/people/f/f;
    .locals 3

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->n:Lcom/google/android/gms/people/f/f;

    if-nez v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->c:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/f/f;

    const-string v2, "images/people-cover-photos"

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/people/f/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/gms/people/ah;->n:Lcom/google/android/gms/people/f/f;

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->n:Lcom/google/android/gms/people/f/f;

    return-object v0
.end method

.method public final declared-synchronized k()Lcom/google/android/gms/people/f/b;
    .locals 2

    .prologue
    .line 150
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->r:Lcom/google/android/gms/people/f/b;

    if-nez v0, :cond_0

    .line 151
    new-instance v0, Lcom/google/android/gms/people/f/b;

    new-instance v1, Lcom/google/android/gms/people/f/c;

    invoke-direct {v1}, Lcom/google/android/gms/people/f/c;-><init>()V

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/f/b;-><init>(Lcom/google/android/gms/people/f/c;)V

    iput-object v0, p0, Lcom/google/android/gms/people/ah;->r:Lcom/google/android/gms/people/f/b;

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->r:Lcom/google/android/gms/people/f/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 150
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized l()Lcom/google/android/gms/people/sync/a;
    .locals 1

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->l:Lcom/google/android/gms/people/sync/a;

    if-nez v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->c:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/people/sync/a;->a()Lcom/google/android/gms/people/sync/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/ah;->l:Lcom/google/android/gms/people/sync/a;

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->l:Lcom/google/android/gms/people/sync/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized m()Lcom/google/android/gms/people/sync/c;
    .locals 2

    .prologue
    .line 166
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->o:Lcom/google/android/gms/people/sync/c;

    if-nez v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->c:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/sync/c;

    invoke-direct {v1, v0}, Lcom/google/android/gms/people/sync/c;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/people/ah;->o:Lcom/google/android/gms/people/sync/c;

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->o:Lcom/google/android/gms/people/sync/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final n()Lcom/google/android/gms/people/sync/d;
    .locals 3

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->p:Lcom/google/android/gms/people/sync/d;

    if-nez v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->c:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/sync/d;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    float-to-int v2, v2

    int-to-float v2, v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/people/sync/d;-><init>(Landroid/content/Context;F)V

    iput-object v1, p0, Lcom/google/android/gms/people/ah;->p:Lcom/google/android/gms/people/sync/d;

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->p:Lcom/google/android/gms/people/sync/d;

    return-object v0
.end method

.method public final declared-synchronized o()Lcom/google/android/gms/people/f/i;
    .locals 2

    .prologue
    .line 182
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->q:Lcom/google/android/gms/people/f/i;

    if-nez v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->c:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/f/i;

    invoke-direct {v1, v0}, Lcom/google/android/gms/people/f/i;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/people/ah;->q:Lcom/google/android/gms/people/f/i;

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/ah;->q:Lcom/google/android/gms/people/f/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 182
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
