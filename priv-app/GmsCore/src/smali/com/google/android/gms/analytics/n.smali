.class public final Lcom/google/android/gms/analytics/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/analytics/u;


# instance fields
.field private final a:Ljava/util/Set;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/analytics/n;->a:Ljava/util/Set;

    .line 19
    iget-object v0, p0, Lcom/google/android/gms/analytics/n;->a:Ljava/util/Set;

    const/16 v1, 0x12e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 20
    iget-object v0, p0, Lcom/google/android/gms/analytics/n;->a:Ljava/util/Set;

    const/16 v1, 0x194

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 21
    iget-object v0, p0, Lcom/google/android/gms/analytics/n;->a:Ljava/util/Set;

    const/16 v1, 0x1f6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 22
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 26
    const/16 v0, 0x7f4

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 31
    const/16 v0, 0x2000

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 36
    const/16 v0, 0x2000

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 41
    const/16 v0, 0x14

    return v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 46
    const-wide/16 v0, 0xe10

    return-wide v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    const-string v0, "/collect"

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    const-string v0, "/batch"

    return-object v0
.end method

.method public final h()Lcom/google/android/gms/analytics/m;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/google/android/gms/analytics/m;->b:Lcom/google/android/gms/analytics/m;

    return-object v0
.end method

.method public final i()Lcom/google/android/gms/analytics/q;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/google/android/gms/analytics/q;->b:Lcom/google/android/gms/analytics/q;

    return-object v0
.end method

.method public final j()Ljava/util/Set;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/analytics/n;->a:Ljava/util/Set;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    const-string v0, ""

    return-object v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method
