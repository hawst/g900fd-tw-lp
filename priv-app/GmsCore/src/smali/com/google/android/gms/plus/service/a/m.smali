.class public final Lcom/google/android/gms/plus/service/a/m;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/plus/internal/c;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/c;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/m;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 33
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/m;->c:Ljava/lang/String;

    .line 34
    iput-object p3, p0, Lcom/google/android/gms/plus/service/a/m;->b:Lcom/google/android/gms/plus/internal/c;

    .line 35
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 40
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/m;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/m;->c:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/plus/b/b;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/server/response/SafeParcelResponse;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/m;->b:Lcom/google/android/gms/plus/internal/c;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    .line 53
    :goto_0
    return-void

    .line 42
    :catch_0
    move-exception v0

    .line 43
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 44
    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v5, v0, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 45
    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/m;->b:Lcom/google/android/gms/plus/internal/c;

    invoke-interface {v0, v6, v1, v4}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V

    goto :goto_0

    .line 48
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/m;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 49
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/m;->b:Lcom/google/android/gms/plus/internal/c;

    invoke-interface {v1, v6, v0, v4}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V

    goto :goto_0

    .line 51
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/m;->b:Lcom/google/android/gms/plus/internal/c;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v4, v4}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/m;->b:Lcom/google/android/gms/plus/internal/c;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/m;->b:Lcom/google/android/gms/plus/internal/c;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V

    .line 61
    :cond_0
    return-void
.end method
