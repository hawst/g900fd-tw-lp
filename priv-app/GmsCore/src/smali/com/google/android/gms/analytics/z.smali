.class public final Lcom/google/android/gms/analytics/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# instance fields
.field private final a:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private final b:Lcom/google/android/gms/analytics/bv;

.field private final c:Landroid/content/Context;

.field private d:Lcom/google/android/gms/analytics/y;

.field private e:Lcom/google/android/gms/analytics/ax;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/analytics/bv;Ljava/lang/Thread$UncaughtExceptionHandler;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    if-nez p1, :cond_0

    .line 63
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "tracker cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_0
    if-nez p3, :cond_1

    .line 67
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_1
    iput-object p2, p0, Lcom/google/android/gms/analytics/z;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 71
    iput-object p1, p0, Lcom/google/android/gms/analytics/z;->b:Lcom/google/android/gms/analytics/bv;

    .line 72
    new-instance v0, Lcom/google/android/gms/analytics/bu;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, p3, v1}, Lcom/google/android/gms/analytics/bu;-><init>(Landroid/content/Context;Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/gms/analytics/z;->d:Lcom/google/android/gms/analytics/y;

    .line 73
    invoke-virtual {p3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/analytics/z;->c:Landroid/content/Context;

    .line 74
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "ExceptionReporter created, original handler is "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p2, :cond_2

    const-string v0, "null"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/analytics/bl;->c(Ljava/lang/String;)V

    .line 76
    return-void

    .line 74
    :cond_2
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 102
    const-string v0, "UncaughtException"

    .line 103
    iget-object v1, p0, Lcom/google/android/gms/analytics/z;->d:Lcom/google/android/gms/analytics/y;

    if-eqz v1, :cond_0

    .line 104
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v0

    .line 105
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/analytics/z;->d:Lcom/google/android/gms/analytics/y;

    invoke-interface {v1, v0, p2}, Lcom/google/android/gms/analytics/y;->a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    .line 107
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Tracking Exception: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/analytics/bl;->c(Ljava/lang/String;)V

    .line 108
    iget-object v1, p0, Lcom/google/android/gms/analytics/z;->b:Lcom/google/android/gms/analytics/bv;

    new-instance v2, Lcom/google/android/gms/analytics/bg;

    invoke-direct {v2}, Lcom/google/android/gms/analytics/bg;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/gms/analytics/bg;->a(Ljava/lang/String;)Lcom/google/android/gms/analytics/bg;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/analytics/bg;->a(Z)Lcom/google/android/gms/analytics/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/bg;->a()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/analytics/bv;->a(Ljava/util/Map;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/analytics/z;->e:Lcom/google/android/gms/analytics/ax;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/analytics/z;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/analytics/ax;->a(Landroid/content/Context;)Lcom/google/android/gms/analytics/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/analytics/z;->e:Lcom/google/android/gms/analytics/ax;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/analytics/z;->e:Lcom/google/android/gms/analytics/ax;

    .line 112
    invoke-virtual {v0}, Lcom/google/android/gms/analytics/ax;->e()V

    .line 113
    invoke-virtual {v0}, Lcom/google/android/gms/analytics/ax;->f()V

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/analytics/z;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v0, :cond_2

    .line 116
    const-string v0, "Passing exception to original handler."

    invoke-static {v0}, Lcom/google/android/gms/analytics/bl;->c(Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/analytics/z;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 119
    :cond_2
    return-void

    .line 104
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
