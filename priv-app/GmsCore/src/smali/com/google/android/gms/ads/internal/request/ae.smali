.class public final Lcom/google/android/gms/ads/internal/request/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/ads/internal/util/p;

.field private final c:Lcom/google/android/gms/ads/internal/b;

.field private final d:Lcom/google/android/gms/ads/internal/a/r;

.field private final e:Ljava/lang/Object;

.field private final f:Lcom/google/android/gms/ads/internal/o/b;

.field private g:Z

.field private h:I

.field private i:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/b;Lcom/google/android/gms/ads/internal/a/r;Lcom/google/android/gms/ads/internal/util/p;Lcom/google/android/gms/ads/internal/o/b;)V
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/request/ae;->e:Ljava/lang/Object;

    .line 104
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/ae;->a:Landroid/content/Context;

    .line 105
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/request/ae;->c:Lcom/google/android/gms/ads/internal/b;

    .line 106
    iput-object p4, p0, Lcom/google/android/gms/ads/internal/request/ae;->b:Lcom/google/android/gms/ads/internal/util/p;

    .line 107
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/request/ae;->d:Lcom/google/android/gms/ads/internal/a/r;

    .line 108
    iput-object p5, p0, Lcom/google/android/gms/ads/internal/request/ae;->f:Lcom/google/android/gms/ads/internal/o/b;

    .line 110
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/request/ae;->g:Z

    .line 111
    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/gms/ads/internal/request/ae;->h:I

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/request/ae;->i:Ljava/util/List;

    .line 113
    return-void
.end method

.method private a(Lcom/google/android/gms/ads/internal/f/d;)Lcom/google/android/gms/ads/internal/o/a;
    .locals 31

    .prologue
    .line 267
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/ads/internal/request/ae;->e:Ljava/lang/Object;

    monitor-enter v3

    .line 268
    :try_start_0
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/gms/ads/internal/request/ae;->h:I

    .line 269
    if-nez p1, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gms/ads/internal/request/ae;->h:I

    const/4 v4, -0x2

    if-ne v2, v4, :cond_0

    .line 270
    const/4 v6, 0x0

    .line 272
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275
    const/4 v2, -0x2

    if-eq v6, v2, :cond_1

    .line 276
    const/16 v30, 0x0

    .line 279
    :goto_0
    new-instance v2, Lcom/google/android/gms/ads/internal/o/a;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/ads/internal/request/ae;->f:Lcom/google/android/gms/ads/internal/o/b;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/o/b;->a:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->c:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/ads/internal/request/ae;->f:Lcom/google/android/gms/ads/internal/o/b;

    iget-object v5, v5, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v5, v5, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->d:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/ads/internal/request/ae;->f:Lcom/google/android/gms/ads/internal/o/b;

    iget-object v7, v7, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v7, v7, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->f:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/ads/internal/request/ae;->i:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/ads/internal/request/ae;->f:Lcom/google/android/gms/ads/internal/o/b;

    iget-object v9, v9, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget v9, v9, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->l:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/ads/internal/request/ae;->f:Lcom/google/android/gms/ads/internal/o/b;

    iget-object v10, v10, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-wide v10, v10, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->k:J

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gms/ads/internal/request/ae;->f:Lcom/google/android/gms/ads/internal/o/b;

    iget-object v12, v12, Lcom/google/android/gms/ads/internal/o/b;->a:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v12, v12, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->i:Ljava/lang/String;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const-wide/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/ae;->f:Lcom/google/android/gms/ads/internal/o/b;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/b;->d:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/ae;->f:Lcom/google/android/gms/ads/internal/o/b;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->g:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/ae;->f:Lcom/google/android/gms/ads/internal/o/b;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-wide v0, v0, Lcom/google/android/gms/ads/internal/o/b;->f:J

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/ae;->f:Lcom/google/android/gms/ads/internal/o/b;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-wide v0, v0, Lcom/google/android/gms/ads/internal/o/b;->g:J

    move-wide/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/ae;->f:Lcom/google/android/gms/ads/internal/o/b;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->o:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/ae;->f:Lcom/google/android/gms/ads/internal/o/b;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/b;->h:Lorg/json/JSONObject;

    move-object/from16 v29, v0

    invoke-direct/range {v2 .. v30}, Lcom/google/android/gms/ads/internal/o/a;-><init>(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Lcom/google/android/gms/ads/internal/p/a;Ljava/util/List;ILjava/util/List;Ljava/util/List;IJLjava/lang/String;ZLcom/google/android/gms/ads/internal/k/c;Lcom/google/android/gms/ads/internal/k/a/d;Ljava/lang/String;Lcom/google/android/gms/ads/internal/k/d;Lcom/google/android/gms/ads/internal/k/f;JLcom/google/android/gms/ads/internal/client/AdSizeParcel;JJJLjava/lang/String;Lorg/json/JSONObject;Lcom/google/android/gms/ads/internal/f/d;)V

    return-object v2

    .line 272
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_1
    move-object/from16 v30, p1

    goto/16 :goto_0
.end method

.method private static a(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 391
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 392
    if-nez v2, :cond_0

    .line 393
    const/4 v0, 0x0

    .line 400
    :goto_0
    return-object v0

    .line 396
    :cond_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v1, v0, [Ljava/lang/String;

    .line 397
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 398
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    .line 397
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 400
    goto :goto_0
.end method

.method private b()Lcom/google/android/gms/ads/internal/o/a;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x0

    .line 118
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/request/ae;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v3, v6

    .line 119
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/request/ae;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v2, v6

    .line 120
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/request/ae;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v1, v6

    .line 121
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/request/ae;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    move-object v0, v6

    .line 122
    :goto_3
    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/internal/request/ae;->a(Lcom/google/android/gms/ads/internal/f/d;)Lcom/google/android/gms/ads/internal/o/a;

    move-result-object v0

    .line 136
    :goto_4
    return-object v0

    .line 118
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/ae;->d:Lcom/google/android/gms/ads/internal/a/r;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/request/ae;->a:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/ae;->f:Lcom/google/android/gms/ads/internal/o/b;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/b;->a:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v3, v0, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->k:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    const-string v5, "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/native_ads.html"

    new-instance v4, Lcom/google/android/gms/ads/internal/util/j;

    invoke-direct {v4}, Lcom/google/android/gms/ads/internal/util/j;-><init>()V

    sget-object v7, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/gms/ads/internal/a/s;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/a/s;-><init>(Lcom/google/android/gms/ads/internal/a/r;Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Lcom/google/android/gms/ads/internal/util/j;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-interface {v4}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/internal/a/p;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/ae;->c:Lcom/google/android/gms/ads/internal/b;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/request/ae;->c:Lcom/google/android/gms/ads/internal/b;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/request/ae;->c:Lcom/google/android/gms/ads/internal/b;

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/request/ae;->c:Lcom/google/android/gms/ads/internal/b;

    iget-object v5, p0, Lcom/google/android/gms/ads/internal/request/ae;->c:Lcom/google/android/gms/ads/internal/b;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/a/p;->a(Lcom/google/android/gms/ads/internal/a;Lcom/google/android/gms/ads/internal/overlay/i;Lcom/google/android/gms/ads/internal/g/b;Lcom/google/android/gms/ads/internal/overlay/n;Lcom/google/android/gms/ads/internal/g/n;)V

    move-object v3, v0

    goto :goto_0

    .line 119
    :cond_1
    new-instance v0, Lcom/google/android/gms/ads/internal/util/j;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/util/j;-><init>()V

    new-instance v1, Lcom/google/android/gms/ads/internal/util/u;

    invoke-direct {v1}, Lcom/google/android/gms/ads/internal/util/u;-><init>()V

    new-instance v2, Lcom/google/android/gms/ads/internal/request/af;

    invoke-direct {v2, p0, v3, v1, v0}, Lcom/google/android/gms/ads/internal/request/af;-><init>(Lcom/google/android/gms/ads/internal/request/ae;Lcom/google/android/gms/ads/internal/a/p;Lcom/google/android/gms/ads/internal/util/u;Lcom/google/android/gms/ads/internal/util/j;)V

    iput-object v2, v1, Lcom/google/android/gms/ads/internal/util/u;->a:Ljava/lang/Object;

    const-string v1, "/nativeAdPreProcess"

    invoke-interface {v3, v1, v2}, Lcom/google/android/gms/ads/internal/a/p;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    new-instance v1, Lorg/json/JSONObject;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/request/ae;->f:Lcom/google/android/gms/ads/internal/o/b;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->c:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "google.afma.nativeAds.preProcessJsonGmsg"

    invoke-interface {v3, v2, v1}, Lcom/google/android/gms/ads/internal/a/p;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/util/j;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    move-object v2, v0

    goto :goto_1

    .line 120
    :cond_2
    const-string v0, "template_id"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "2"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v0, Lcom/google/android/gms/ads/internal/request/ai;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/request/ai;-><init>()V

    move-object v1, v0

    goto :goto_2

    :cond_3
    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/android/gms/ads/internal/request/aj;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/request/aj;-><init>()V

    move-object v1, v0

    goto/16 :goto_2

    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/request/ae;->a(I)V

    move-object v1, v6

    goto/16 :goto_2

    .line 121
    :cond_5
    const-string v0, "tracking_urls_and_actions"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v4, "impression_tracking_urls"

    invoke-static {v0, v4}, Lcom/google/android/gms/ads/internal/request/ae;->a(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    move-object v0, v6

    :goto_5
    iput-object v0, p0, Lcom/google/android/gms/ads/internal/request/ae;->i:Ljava/util/List;

    invoke-interface {v1, p0, v2}, Lcom/google/android/gms/ads/internal/request/ah;->a(Lcom/google/android/gms/ads/internal/request/ae;Lorg/json/JSONObject;)Lcom/google/android/gms/ads/internal/f/d;

    move-result-object v0

    if-nez v0, :cond_7

    const-string v0, "Failed to retrieve ad assets."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;)V

    move-object v0, v6

    goto/16 :goto_3

    :cond_6
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_5

    :cond_7
    new-instance v1, Lcom/google/android/gms/ads/internal/f/c;

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/request/ae;->c:Lcom/google/android/gms/ads/internal/b;

    invoke-direct {v1, v4, v3, v2}, Lcom/google/android/gms/ads/internal/f/c;-><init>(Lcom/google/android/gms/ads/internal/b;Lcom/google/android/gms/ads/internal/a/p;Lorg/json/JSONObject;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/ads/internal/f/d;->a(Lcom/google/android/gms/ads/internal/f/c;)V
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    goto/16 :goto_3

    .line 131
    :catch_0
    move-exception v0

    .line 133
    :goto_6
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/request/ae;->g:Z

    if-nez v0, :cond_8

    .line 134
    invoke-virtual {p0, v8}, Lcom/google/android/gms/ads/internal/request/ae;->a(I)V

    .line 136
    :cond_8
    invoke-direct {p0, v6}, Lcom/google/android/gms/ads/internal/request/ae;->a(Lcom/google/android/gms/ads/internal/f/d;)Lcom/google/android/gms/ads/internal/o/a;

    move-result-object v0

    goto/16 :goto_4

    .line 127
    :catch_1
    move-exception v0

    .line 128
    const-string v1, "Malformed native JSON response."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 129
    :catch_2
    move-exception v0

    .line 130
    const-string v1, "Timeout when loading native ad."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 131
    :catch_3
    move-exception v0

    goto :goto_6

    :catch_4
    move-exception v0

    goto :goto_6
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;Ljava/lang/String;Z)Ljava/util/concurrent/Future;
    .locals 3

    .prologue
    .line 316
    if-eqz p3, :cond_1

    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 319
    :goto_0
    if-nez v0, :cond_0

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 321
    :cond_0
    if-eqz p3, :cond_2

    const-string v1, "url"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 325
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 326
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/ads/internal/request/ae;->a(IZ)V

    .line 327
    new-instance v0, Lcom/google/android/gms/ads/internal/util/k;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/util/k;-><init>()V

    .line 330
    :goto_2
    return-object v0

    .line 316
    :cond_1
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    goto :goto_0

    .line 321
    :cond_2
    const-string v1, "url"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 330
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/ae;->b:Lcom/google/android/gms/ads/internal/util/p;

    new-instance v2, Lcom/google/android/gms/ads/internal/request/ag;

    invoke-direct {v2, p0, p3}, Lcom/google/android/gms/ads/internal/request/ag;-><init>(Lcom/google/android/gms/ads/internal/request/ae;Z)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/ads/internal/util/p;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/util/s;)Ljava/util/concurrent/Future;

    move-result-object v0

    goto :goto_2
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 373
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/ae;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 374
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/request/ae;->g:Z

    .line 375
    iput p1, p0, Lcom/google/android/gms/ads/internal/request/ae;->h:I

    .line 376
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(IZ)V
    .locals 0

    .prologue
    .line 385
    if-eqz p2, :cond_0

    .line 386
    invoke-virtual {p0, p1}, Lcom/google/android/gms/ads/internal/request/ae;->a(I)V

    .line 388
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 363
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/ae;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 364
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/request/ae;->g:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 365
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/request/ae;->b()Lcom/google/android/gms/ads/internal/o/a;

    move-result-object v0

    return-object v0
.end method
