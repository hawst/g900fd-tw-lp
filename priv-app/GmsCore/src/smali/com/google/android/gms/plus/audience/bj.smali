.class public final Lcom/google/android/gms/plus/audience/bj;
.super Lcom/google/android/gms/plus/f/d;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 349
    invoke-direct {p0}, Lcom/google/android/gms/plus/f/d;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/gms/plus/audience/bj;
    .locals 3

    .prologue
    .line 355
    invoke-static {p0}, Lcom/google/android/gms/plus/f/d;->c(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 356
    const-string v1, "resultCode"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 357
    const-string v1, "dataIntent"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 358
    new-instance v1, Lcom/google/android/gms/plus/audience/bj;

    invoke-direct {v1}, Lcom/google/android/gms/plus/audience/bj;-><init>()V

    .line 359
    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/audience/bj;->setArguments(Landroid/os/Bundle;)V

    .line 360
    return-object v1
.end method


# virtual methods
.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 365
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/f/d;->onDismiss(Landroid/content/DialogInterface;)V

    .line 366
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bj;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;

    .line 367
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bj;->getArguments()Landroid/os/Bundle;

    .line 368
    if-eqz v0, :cond_0

    .line 369
    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->finish()V

    .line 371
    :cond_0
    return-void
.end method
