.class public final Lcom/google/android/gms/wallet/service/ow/ar;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/SharedPreferences;

.field private final b:J

.field private final c:I

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/service/ow/ar;-><init>(Landroid/content/Context;Z)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/wallet/service/ow/ar;-><init>(Landroid/content/Context;ZB)V

    .line 61
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ZB)V
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/ar;->d:Landroid/content/Context;

    .line 66
    const/4 v0, 0x0

    .line 67
    if-eqz p2, :cond_0

    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    const/4 v0, 0x4

    .line 70
    :cond_0
    const-string v1, "com.google.android.gms.wallet.service.ow.TransactionContextStorage"

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/ar;->a:Landroid/content/SharedPreferences;

    .line 71
    const-wide/32 v0, 0x5265c00

    iput-wide v0, p0, Lcom/google/android/gms/wallet/service/ow/ar;->b:J

    .line 72
    const/16 v0, 0x10

    iput v0, p0, Lcom/google/android/gms/wallet/service/ow/ar;->c:I

    .line 73
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/aa/a/a/a/d;
    .locals 2

    .prologue
    .line 116
    new-instance v0, Lcom/google/aa/a/a/a/d;

    invoke-direct {v0}, Lcom/google/aa/a/a/a/d;-><init>()V

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object p0, v0, Lcom/google/aa/a/a/a/d;->a:Ljava/lang/String;

    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iput-object p1, v0, Lcom/google/aa/a/a/a/d;->b:Ljava/lang/String;

    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iput-object p2, v0, Lcom/google/aa/a/a/a/d;->c:Ljava/lang/String;

    :cond_2
    iput-boolean p3, v0, Lcom/google/aa/a/a/a/d;->d:Z

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/aa/a/a/a/d;->e:Z

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/gms/wallet/service/ow/as;
    .locals 14

    .prologue
    const/4 v11, 0x0

    const/4 v0, 0x0

    .line 101
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/ar;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 102
    if-nez v1, :cond_0

    .line 110
    :goto_0
    return-object v0

    .line 105
    :cond_0
    invoke-static {v1}, Lcom/google/android/gms/wallet/common/af;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ag;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/wallet/common/ag;->a(J)J

    move-result-wide v12

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ag;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ag;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ag;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ag;->b()Ljava/lang/String;

    move-result-object v6

    if-eqz v5, :cond_3

    if-eqz v6, :cond_3

    new-instance v4, Landroid/accounts/Account;

    invoke-direct {v4, v5, v6}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const-class v5, Lcom/google/aa/b/a/a/a/a/u;

    invoke-virtual {v1, v5, v0}, Lcom/google/android/gms/wallet/common/ag;->a(Ljava/lang/Class;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v5

    check-cast v5, Lcom/google/aa/b/a/a/a/a/u;

    invoke-virtual {v1, v11}, Lcom/google/android/gms/wallet/common/ag;->a(Z)Z

    move-result v6

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ag;->c()Ljava/lang/String;

    invoke-virtual {v1, v11}, Lcom/google/android/gms/wallet/common/ag;->a(Z)Z

    move-result v7

    sget-object v8, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ag;->c()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_1

    invoke-static {}, Lcom/google/android/gms/wallet/common/af;->c()Ljava/util/regex/Pattern;

    move-result-object v8

    const/4 v10, -0x1

    invoke-virtual {v8, v9, v10}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;I)[Ljava/lang/String;

    move-result-object v8

    :cond_1
    invoke-virtual {v1, v11}, Lcom/google/android/gms/wallet/common/ag;->a(Z)Z

    move-result v9

    invoke-virtual {v1, v11}, Lcom/google/android/gms/wallet/common/ag;->a(Z)Z

    move-result v10

    const-class v11, Lcom/google/aa/a/a/a/d;

    invoke-virtual {v1, v11, v0}, Lcom/google/android/gms/wallet/common/ag;->a(Ljava/lang/Class;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v11

    check-cast v11, Lcom/google/aa/a/a/a/d;

    new-instance v1, Lcom/google/android/gms/wallet/service/ow/as;

    invoke-direct/range {v1 .. v13}, Lcom/google/android/gms/wallet/service/ow/as;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/u;ZZ[Ljava/lang/String;ZZLcom/google/aa/a/a/a/d;J)V

    .line 106
    iget-wide v2, v1, Lcom/google/android/gms/wallet/service/ow/as;->a:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    .line 107
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/ar;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v1}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 110
    goto :goto_0

    :cond_3
    move-object v4, v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/wallet/service/ow/as;)V
    .locals 2

    .prologue
    .line 77
    iget-wide v0, p0, Lcom/google/android/gms/wallet/service/ow/ar;->b:J

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/gms/wallet/service/ow/ar;->a(Ljava/lang/String;Lcom/google/android/gms/wallet/service/ow/as;J)V

    .line 78
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/wallet/service/ow/as;J)V
    .locals 5

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/ar;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 83
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v2, p3

    iput-wide v2, p2, Lcom/google/android/gms/wallet/service/ow/as;->a:J

    .line 84
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/as;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 85
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 86
    new-instance v0, Lcom/google/android/gms/wallet/service/m;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/ar;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/ar;->a:Landroid/content/SharedPreferences;

    iget v3, p0, Lcom/google/android/gms/wallet/service/ow/ar;->c:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/service/m;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/service/m;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 88
    return-void
.end method
