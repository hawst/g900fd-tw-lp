.class final Lcom/google/android/gms/games/ui/d/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/d/aq;

.field final synthetic b:Lcom/google/android/gms/games/multiplayer/Invitation;

.field final synthetic c:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/d/aq;Lcom/google/android/gms/games/multiplayer/Invitation;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 547
    iput-object p1, p0, Lcom/google/android/gms/games/ui/d/am;->a:Lcom/google/android/gms/games/ui/d/aq;

    iput-object p2, p0, Lcom/google/android/gms/games/ui/d/am;->b:Lcom/google/android/gms/games/multiplayer/Invitation;

    iput-object p3, p0, Lcom/google/android/gms/games/ui/d/am;->c:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 551
    packed-switch p2, :pswitch_data_0

    .line 563
    :pswitch_0
    const-string v0, "UiUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled dialog action "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    :goto_0
    return-void

    .line 553
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/am;->a:Lcom/google/android/gms/games/ui/d/aq;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/am;->b:Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/d/aq;->b(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    goto :goto_0

    .line 557
    :pswitch_2
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    sget-object v0, Lcom/google/android/gms/games/ui/n;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 559
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/am;->c:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 551
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
