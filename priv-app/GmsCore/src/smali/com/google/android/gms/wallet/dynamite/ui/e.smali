.class public final Lcom/google/android/gms/wallet/dynamite/ui/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/dynamite/ui/a;


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private e:Lcom/google/android/gms/wallet/dynamite/ui/f;

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Ljava/lang/String;II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, -0x1

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->f:I

    .line 32
    iput v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->g:I

    .line 44
    const-string v0, "GOOGLE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 45
    if-gez v0, :cond_0

    .line 46
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find \"GOOGLE\" in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_0
    add-int/lit8 v1, v0, 0x6

    .line 51
    if-lez v0, :cond_1

    .line 52
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->b:Ljava/lang/String;

    .line 56
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 57
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->c:Ljava/lang/String;

    .line 62
    :goto_1
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->a:Landroid/graphics/Paint;

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->a:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 68
    iput p3, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->d:I

    .line 69
    return-void

    .line 54
    :cond_1
    iput-object v3, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->b:Ljava/lang/String;

    goto :goto_0

    .line 59
    :cond_2
    iput-object v3, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->c:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public final a(I)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 73
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->f:I

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->e:Lcom/google/android/gms/wallet/dynamite/ui/f;

    if-eqz v0, :cond_0

    .line 74
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->g:I

    .line 105
    :goto_0
    return v0

    .line 76
    :cond_0
    iput p1, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->f:I

    .line 78
    int-to-float v0, p1

    const v2, 0x3f1eb852    # 0.62f

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 79
    int-to-float v0, v2

    const v3, 0x40155555

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 81
    int-to-float v0, p1

    const v4, 0x3ecccccd    # 0.4f

    mul-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 82
    iget-object v4, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->a:Landroid/graphics/Paint;

    int-to-float v0, v0

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 83
    iput v3, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->g:I

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->a:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->b:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 86
    iget v4, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->g:I

    add-int/2addr v4, v0

    iput v4, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->g:I

    .line 88
    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->c:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 89
    iget v4, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->g:I

    iget-object v5, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->a:Landroid/graphics/Paint;

    iget-object v6, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    add-int/2addr v4, v5

    iput v4, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->g:I

    .line 92
    :cond_1
    iget-object v4, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->e:Lcom/google/android/gms/wallet/dynamite/ui/f;

    if-nez v4, :cond_2

    .line 93
    new-instance v4, Lcom/google/android/gms/wallet/dynamite/ui/f;

    invoke-direct {v4, v1}, Lcom/google/android/gms/wallet/dynamite/ui/f;-><init>(B)V

    iput-object v4, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->e:Lcom/google/android/gms/wallet/dynamite/ui/f;

    .line 95
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->e:Lcom/google/android/gms/wallet/dynamite/ui/f;

    iget-object v4, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->b:Ljava/lang/String;

    iput-object v4, v1, Lcom/google/android/gms/wallet/dynamite/ui/f;->f:Ljava/lang/String;

    .line 96
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->e:Lcom/google/android/gms/wallet/dynamite/ui/f;

    iget-object v4, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->c:Ljava/lang/String;

    iput-object v4, v1, Lcom/google/android/gms/wallet/dynamite/ui/f;->g:Ljava/lang/String;

    .line 97
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->e:Lcom/google/android/gms/wallet/dynamite/ui/f;

    new-instance v4, Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->a:Landroid/graphics/Paint;

    invoke-direct {v4, v5}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v4, v1, Lcom/google/android/gms/wallet/dynamite/ui/f;->h:Landroid/graphics/Paint;

    .line 98
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->e:Lcom/google/android/gms/wallet/dynamite/ui/f;

    sub-int v4, p1, v2

    div-int/lit8 v4, v4, 0x2

    iput v4, v1, Lcom/google/android/gms/wallet/dynamite/ui/f;->b:I

    .line 99
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->e:Lcom/google/android/gms/wallet/dynamite/ui/f;

    iget-object v4, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->e:Lcom/google/android/gms/wallet/dynamite/ui/f;

    iget v4, v4, Lcom/google/android/gms/wallet/dynamite/ui/f;->b:I

    add-int/2addr v4, v2

    iput v4, v1, Lcom/google/android/gms/wallet/dynamite/ui/f;->d:I

    .line 100
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->e:Lcom/google/android/gms/wallet/dynamite/ui/f;

    iput v0, v1, Lcom/google/android/gms/wallet/dynamite/ui/f;->a:I

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->e:Lcom/google/android/gms/wallet/dynamite/ui/f;

    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->e:Lcom/google/android/gms/wallet/dynamite/ui/f;

    iget v1, v1, Lcom/google/android/gms/wallet/dynamite/ui/f;->a:I

    add-int/2addr v1, v3

    iput v1, v0, Lcom/google/android/gms/wallet/dynamite/ui/f;->c:I

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->e:Lcom/google/android/gms/wallet/dynamite/ui/f;

    int-to-float v1, v2

    const v2, 0x3f2dd3c1

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->e:Lcom/google/android/gms/wallet/dynamite/ui/f;

    iget v2, v2, Lcom/google/android/gms/wallet/dynamite/ui/f;->b:I

    add-int/2addr v1, v2

    iput v1, v0, Lcom/google/android/gms/wallet/dynamite/ui/f;->e:I

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->e:Lcom/google/android/gms/wallet/dynamite/ui/f;

    iget v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->d:I

    iput v1, v0, Lcom/google/android/gms/wallet/dynamite/ui/f;->i:I

    .line 105
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->g:I

    goto/16 :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final a()Lcom/google/android/gms/wallet/dynamite/ui/b;
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->e:Lcom/google/android/gms/wallet/dynamite/ui/f;

    .line 115
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/e;->e:Lcom/google/android/gms/wallet/dynamite/ui/f;

    .line 116
    return-object v0
.end method
