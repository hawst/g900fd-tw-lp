.class public final Lcom/google/android/gms/games/ui/widget/a;
.super Landroid/graphics/drawable/shapes/RectShape;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    return-void
.end method

.method private a()Landroid/graphics/Path;
    .locals 8

    .prologue
    const/high16 v7, 0x40800000    # 4.0f

    const/high16 v6, 0x40000000    # 2.0f

    .line 32
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/a;->rect()Landroid/graphics/RectF;

    move-result-object v0

    .line 34
    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    iget v2, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v2

    .line 35
    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    float-to-double v4, v1

    mul-double/2addr v2, v4

    double-to-float v2, v2

    .line 37
    iget v3, v0, Landroid/graphics/RectF;->left:F

    iget v4, v0, Landroid/graphics/RectF;->right:F

    add-float/2addr v3, v4

    div-float/2addr v3, v6

    .line 38
    div-float v4, v1, v7

    .line 39
    const/high16 v5, 0x40400000    # 3.0f

    mul-float/2addr v1, v5

    div-float/2addr v1, v7

    .line 40
    div-float v5, v2, v6

    sub-float v5, v3, v5

    .line 41
    div-float/2addr v2, v6

    add-float/2addr v2, v3

    .line 43
    new-instance v6, Landroid/graphics/Path;

    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    .line 44
    iget v7, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v6, v3, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 45
    invoke-virtual {v6, v5, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 46
    invoke-virtual {v6, v5, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 47
    iget v5, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v6, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 48
    invoke-virtual {v6, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 49
    invoke-virtual {v6, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 50
    iget v0, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v6, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 51
    return-object v6
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/widget/a;->a()Landroid/graphics/Path;

    move-result-object v0

    .line 23
    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 24
    return-void
.end method

.method public final getOutline(Landroid/graphics/Outline;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/widget/a;->a()Landroid/graphics/Path;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Outline;->setConvexPath(Landroid/graphics/Path;)V

    .line 29
    return-void
.end method
