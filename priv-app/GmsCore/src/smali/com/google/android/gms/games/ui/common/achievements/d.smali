.class public abstract Lcom/google/android/gms/games/ui/common/achievements/d;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    sget v0, Lcom/google/android/gms/l;->aA:I

    sput v0, Lcom/google/android/gms/games/ui/common/achievements/d;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 278
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 279
    sget v1, Lcom/google/android/gms/j;->mb:I

    if-ne v0, v1, :cond_0

    .line 281
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/d;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 282
    const-string v1, "com.google.android.gms.games.VIEW_ACHIEVEMENTS"

    invoke-static {p0, v1, v0}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 284
    const/16 v1, 0x4d2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/common/achievements/d;->startActivityForResult(Landroid/content/Intent;I)V

    .line 285
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/d;->finish()V

    .line 287
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 20

    .prologue
    .line 45
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/common/achievements/d;->requestWindowFeature(I)Z

    .line 46
    invoke-super/range {p0 .. p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 48
    sget v2, Lcom/google/android/gms/games/ui/common/achievements/d;->a:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/common/achievements/d;->setContentView(I)V

    .line 50
    invoke-super/range {p0 .. p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v2

    .line 51
    if-eqz v2, :cond_0

    .line 52
    invoke-virtual {v2}, Landroid/support/v7/app/a;->e()V

    .line 55
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/games/ui/common/achievements/d;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 56
    const-string v3, "com.google.android.gms.games.ACHIEVEMENT"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/achievement/Achievement;

    .line 57
    if-nez v2, :cond_1

    .line 58
    const-string v2, "AchieveDescrActivity"

    const-string v3, "Required achievement is missing."

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/games/ui/common/achievements/d;->finish()V

    .line 274
    :goto_0
    return-void

    .line 63
    :cond_1
    sget v3, Lcom/google/android/gms/j;->qy:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/common/achievements/d;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .line 64
    sget v3, Lcom/google/android/gms/j;->kb:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/common/achievements/d;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 65
    sget v3, Lcom/google/android/gms/j;->sQ:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/common/achievements/d;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 66
    sget v3, Lcom/google/android/gms/j;->si:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/common/achievements/d;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 67
    sget v3, Lcom/google/android/gms/j;->kZ:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/common/achievements/d;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 68
    sget v3, Lcom/google/android/gms/j;->qv:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/common/achievements/d;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 69
    sget v3, Lcom/google/android/gms/j;->j:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/common/achievements/d;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    .line 70
    sget v9, Lcom/google/android/gms/j;->k:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/google/android/gms/games/ui/common/achievements/d;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;

    .line 72
    sget v10, Lcom/google/android/gms/j;->mb:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/gms/games/ui/common/achievements/d;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 74
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->n()I

    move-result v16

    .line 75
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->d()I

    move-result v10

    .line 76
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/games/ui/common/achievements/d;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    .line 78
    const/4 v12, 0x1

    if-ne v10, v12, :cond_2

    const/4 v10, 0x1

    move/from16 v0, v16

    if-ne v0, v10, :cond_2

    const/4 v10, 0x1

    .line 82
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/games/ui/common/achievements/d;->getIntent()Landroid/content/Intent;

    move-result-object v12

    const-string v13, "com.google.android.gms.games.SHOW_SEE_MORE"

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v12

    .line 83
    if-eqz v12, :cond_3

    .line 84
    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    .line 85
    move-object/from16 v0, p0

    invoke-virtual {v11, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    :goto_2
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->f()Ljava/lang/String;

    move-result-object v14

    .line 95
    const-string v13, ""

    .line 96
    const-string v12, ""

    .line 98
    if-nez v10, :cond_7

    .line 101
    packed-switch v16, :pswitch_data_0

    .line 122
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown achievement state "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 78
    :cond_2
    const/4 v10, 0x0

    goto :goto_1

    .line 87
    :cond_3
    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 103
    :pswitch_0
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->g()Landroid/net/Uri;

    move-result-object v10

    sget v11, Lcom/google/android/gms/h;->aj:I

    invoke-virtual {v3, v10, v11}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    .line 105
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 106
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->q()J

    move-result-wide v10

    const/high16 v12, 0x80000

    move-object/from16 v0, p0

    invoke-static {v0, v10, v11, v12}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v12

    .line 108
    invoke-virtual {v8, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    sget v10, Lcom/google/android/gms/f;->r:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setTextColor(I)V

    move-object v11, v12

    .line 131
    :goto_3
    if-nez v16, :cond_4

    .line 132
    const/16 v10, 0xff

    .line 138
    :goto_4
    const/16 v12, 0x10

    invoke-static {v12}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 139
    invoke-virtual {v3, v10}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setImageAlpha(I)V

    .line 144
    :goto_5
    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->setVisibility(I)V

    .line 145
    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    .line 147
    if-nez v16, :cond_6

    .line 148
    sget v3, Lcom/google/android/gms/p;->lK:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v9, v3

    move-object v3, v11

    .line 188
    :goto_6
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->n()I

    move-result v10

    const/4 v11, 0x2

    if-eq v10, v11, :cond_b

    .line 189
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->e()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    invoke-virtual {v4, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->r()J

    move-result-wide v10

    .line 193
    const-wide/16 v18, 0x0

    cmp-long v12, v10, v18

    if-lez v12, :cond_a

    .line 194
    sget v12, Lcom/google/android/gms/p;->hF:I

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v13, v16

    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 196
    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    const/4 v11, 0x0

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 202
    :goto_7
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->e()Ljava/lang/String;

    move-result-object v2

    move-object v11, v2

    move-object v2, v14

    .line 215
    :goto_8
    sget v12, Lcom/google/android/gms/f;->n:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    .line 216
    sget v13, Lcom/google/android/gms/f;->n:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    .line 217
    sget v14, Lcom/google/android/gms/f;->W:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    .line 218
    invoke-virtual {v6, v12}, Landroid/widget/TextView;->setTextColor(I)V

    .line 219
    invoke-virtual {v7, v14}, Landroid/widget/TextView;->setTextColor(I)V

    .line 220
    invoke-virtual {v4, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 222
    sget v12, Lcom/google/android/gms/p;->lI:I

    const/4 v13, 0x5

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v11, v13, v14

    const/4 v11, 0x1

    aput-object v2, v13, v11

    const/4 v2, 0x2

    aput-object v9, v13, v2

    const/4 v2, 0x3

    aput-object v10, v13, v2

    const/4 v2, 0x4

    aput-object v3, v13, v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 236
    invoke-virtual {v4}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v9

    .line 237
    new-instance v2, Lcom/google/android/gms/games/ui/common/achievements/e;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/gms/games/ui/common/achievements/e;-><init>(Lcom/google/android/gms/games/ui/common/achievements/d;Landroid/widget/TextView;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V

    invoke-virtual {v9, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto/16 :goto_0

    .line 113
    :pswitch_1
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->i()Landroid/net/Uri;

    move-result-object v10

    sget v11, Lcom/google/android/gms/h;->ai:I

    invoke-virtual {v3, v10, v11}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    .line 115
    const/4 v10, 0x4

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setVisibility(I)V

    move-object v11, v12

    .line 116
    goto/16 :goto_3

    .line 118
    :pswitch_2
    const/4 v10, 0x0

    sget v11, Lcom/google/android/gms/h;->ah:I

    invoke-virtual {v3, v10, v11}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    .line 119
    const/4 v10, 0x4

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setVisibility(I)V

    move-object v11, v12

    .line 120
    goto/16 :goto_3

    .line 134
    :cond_4
    sget v10, Lcom/google/android/gms/f;->P:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    .line 135
    invoke-static {v10}, Landroid/graphics/Color;->alpha(I)I

    move-result v10

    goto/16 :goto_4

    .line 141
    :cond_5
    invoke-virtual {v3, v10}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setAlpha(I)V

    goto/16 :goto_5

    .line 151
    :cond_6
    sget v3, Lcom/google/android/gms/p;->lJ:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v9, v3

    move-object v3, v11

    .line 154
    goto/16 :goto_6

    .line 157
    :cond_7
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->o()I

    move-result v10

    .line 158
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->k()I

    move-result v11

    .line 161
    if-gtz v11, :cond_8

    .line 164
    const-string v16, "AchieveDescrActivity"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Inconsistent achievement "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ": TYPE_INCREMENTAL, but totalSteps = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v16

    invoke-static {v0, v11}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const/4 v11, 0x1

    .line 170
    :cond_8
    if-lt v10, v11, :cond_9

    .line 174
    const-string v16, "AchieveDescrActivity"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Inconsistent achievement "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ": STATE_REVEALED, but steps = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v18, " / "

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v16

    invoke-static {v0, v10}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v10, v11

    .line 180
    :cond_9
    const/16 v16, 0x4

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 181
    invoke-virtual {v9, v10, v11}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->b(II)V

    .line 182
    const/16 v16, 0x8

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    .line 183
    const/4 v3, 0x0

    invoke-virtual {v9, v3}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->setVisibility(I)V

    .line 185
    mul-int/lit8 v3, v10, 0x64

    div-int/2addr v3, v11

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/google/android/gms/p;->ke:I

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v11, v16

    invoke-virtual {v9, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v9, v3

    move-object v3, v12

    goto/16 :goto_6

    .line 199
    :cond_a
    const/16 v10, 0x8

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setVisibility(I)V

    move-object v10, v13

    goto/16 :goto_7

    .line 205
    :cond_b
    sget v2, Lcom/google/android/gms/p;->hy:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 206
    sget v2, Lcom/google/android/gms/p;->hx:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    const/16 v2, 0x8

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 209
    sget v2, Lcom/google/android/gms/p;->hy:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 210
    sget v2, Lcom/google/android/gms/p;->hx:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v11, v10

    move-object v10, v13

    goto/16 :goto_8

    .line 101
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
