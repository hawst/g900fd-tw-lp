.class final Lcom/google/android/gms/cast/b/g;
.super Lcom/google/android/gms/cast/b/af;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/b/e;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/b/e;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 238
    iput-object p1, p0, Lcom/google/android/gms/cast/b/g;->a:Lcom/google/android/gms/cast/b/e;

    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/gms/cast/b/af;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/gms/cast/b/g;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/e;->f(Lcom/google/android/gms/cast/b/e;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 259
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/cast/b/g;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v1}, Lcom/google/android/gms/cast/b/e;->g(Lcom/google/android/gms/cast/b/e;)Z

    .line 260
    iget-object v1, p0, Lcom/google/android/gms/cast/b/g;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v1}, Lcom/google/android/gms/cast/b/e;->h(Lcom/google/android/gms/cast/b/e;)Ljava/lang/String;

    .line 261
    iget-object v1, p0, Lcom/google/android/gms/cast/b/g;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v1}, Lcom/google/android/gms/cast/b/e;->i(Lcom/google/android/gms/cast/b/e;)Ljava/lang/String;

    .line 262
    iget-object v1, p0, Lcom/google/android/gms/cast/b/g;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v1, v0}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/b/e;I)V

    .line 263
    return-void

    .line 257
    :cond_0
    const/16 v0, 0x7d5

    goto :goto_0
.end method

.method protected final a(I)V
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/gms/cast/b/g;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0, p1}, Lcom/google/android/gms/cast/b/e;->b(Lcom/google/android/gms/cast/b/e;I)V

    .line 253
    return-void
.end method

.method protected final a(Lcom/google/android/gms/cast/b/b;)V
    .locals 3

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/gms/cast/b/g;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/e;->d(Lcom/google/android/gms/cast/b/e;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/b/g;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/e;->e(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/ApplicationMetadata;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/google/android/gms/cast/b/g;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "an app was running before; notifying that it\'s gone now"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/cast/b/g;->a:Lcom/google/android/gms/cast/b/e;

    const/16 v1, 0x7d5

    invoke-static {v0, v1}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/b/e;I)V

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/g;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0, p1}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/b/e;Lcom/google/android/gms/cast/b/b;)V

    .line 248
    return-void
.end method

.method protected final a(Lcom/google/android/gms/cast/b/b;ZLcom/google/android/gms/cast/internal/DeviceStatus;)V
    .locals 3

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/gms/cast/b/g;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "onStatusReceived"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 274
    iget-object v0, p0, Lcom/google/android/gms/cast/b/g;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/b/e;Lcom/google/android/gms/cast/b/b;ZLcom/google/android/gms/cast/internal/DeviceStatus;)V

    .line 275
    return-void
.end method

.method protected final b(I)V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/gms/cast/b/g;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/e;->j(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/b/m;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/gms/cast/b/m;->d(I)V

    .line 268
    return-void
.end method

.method protected final c(I)V
    .locals 5

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/gms/cast/b/g;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "onStatusRequestFailed: statusCode=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 283
    iget-object v0, p0, Lcom/google/android/gms/cast/b/g;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/e;->k(Lcom/google/android/gms/cast/b/e;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/google/android/gms/cast/b/g;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/e;->l(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/b/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/aj;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/cast/b/g;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/e;->m(Lcom/google/android/gms/cast/b/e;)V

    .line 289
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/g;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/e;->n(Lcom/google/android/gms/cast/b/e;)Ljava/lang/String;

    .line 290
    iget-object v0, p0, Lcom/google/android/gms/cast/b/g;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/e;->o(Lcom/google/android/gms/cast/b/e;)Ljava/lang/String;

    .line 292
    :cond_0
    return-void

    .line 287
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/g;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0, p1}, Lcom/google/android/gms/cast/b/e;->b(Lcom/google/android/gms/cast/b/e;I)V

    goto :goto_0
.end method
