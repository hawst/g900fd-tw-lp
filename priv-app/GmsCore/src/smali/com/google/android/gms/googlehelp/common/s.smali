.class public final Lcom/google/android/gms/googlehelp/common/s;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/lang/String;I)Lcom/google/android/gms/googlehelp/e/c;
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/google/android/gms/googlehelp/e/c;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/e/c;-><init>()V

    .line 64
    iput-object p0, v0, Lcom/google/android/gms/googlehelp/e/c;->a:Ljava/lang/String;

    .line 65
    iput p1, v0, Lcom/google/android/gms/googlehelp/e/c;->b:I

    .line 66
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/e/n;
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/gms/googlehelp/e/n;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/e/n;-><init>()V

    .line 51
    iput-object p0, v0, Lcom/google/android/gms/googlehelp/e/n;->a:Ljava/lang/String;

    .line 52
    iput-object p1, v0, Lcom/google/android/gms/googlehelp/e/n;->b:Ljava/lang/String;

    .line 53
    return-object v0
.end method

.method public static a([BLcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 165
    invoke-static {p0}, Lcom/google/android/gms/common/util/ab;->a([B)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    invoke-static {p0}, Lcom/google/android/gms/googlehelp/common/s;->b([B)[B

    move-result-object p0

    .line 168
    :cond_0
    invoke-static {p1, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;)[B
    .locals 4

    .prologue
    .line 149
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 150
    const/16 v0, 0x2800

    new-array v0, v0, [B

    .line 153
    :goto_0
    const/4 v2, 0x0

    :try_start_0
    array-length v3, v0

    invoke-virtual {p0, v0, v2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 154
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 159
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    throw v0

    .line 156
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 157
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 159
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    return-object v0
.end method

.method public static a([B)[B
    .locals 2

    .prologue
    .line 124
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 126
    :try_start_0
    new-instance v0, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v0, v1}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 127
    invoke-virtual {v0, p0}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    .line 128
    invoke-virtual {v0}, Ljava/util/zip/GZIPOutputStream;->close()V

    .line 129
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 131
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    throw v0
.end method

.method private static b([B)[B
    .locals 3

    .prologue
    .line 137
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 138
    new-instance v1, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v1, v0}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    .line 140
    :try_start_0
    invoke-static {v1}, Lcom/google/android/gms/googlehelp/common/s;->a(Ljava/io/InputStream;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 142
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 143
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-object v2

    .line 142
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 143
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw v2
.end method
