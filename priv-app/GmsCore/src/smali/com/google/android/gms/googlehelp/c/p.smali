.class public final Lcom/google/android/gms/googlehelp/c/p;
.super Landroid/widget/RadioButton;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# instance fields
.field private final a:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/google/ad/a/a/k;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/widget/RadioButton;-><init>(Landroid/content/Context;)V

    .line 42
    iget-object v0, p3, Lcom/google/ad/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/p;->setTag(Ljava/lang/Object;)V

    .line 43
    iget-object v0, p3, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    iget-object v0, p3, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/p;->setText(Ljava/lang/CharSequence;)V

    .line 46
    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/android/gms/googlehelp/c/p;->setId(I)V

    .line 48
    iget-boolean v0, p3, Lcom/google/ad/a/a/k;->d:Z

    if-eqz v0, :cond_1

    .line 49
    invoke-static {p1, p0}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/content/Context;Landroid/widget/CompoundButton;)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/c/p;->a:Landroid/widget/EditText;

    .line 53
    :goto_0
    return-void

    .line 51
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/c/p;->a:Landroid/widget/EditText;

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/p;->a:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/p;->a:Landroid/widget/EditText;

    return-object v0
.end method
