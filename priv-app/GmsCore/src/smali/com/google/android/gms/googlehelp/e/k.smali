.class public final Lcom/google/android/gms/googlehelp/e/k;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/googlehelp/e/l;

.field public b:Lcom/google/android/gms/googlehelp/e/m;

.field public c:Lcom/google/android/gms/googlehelp/e/o;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/k;->a:Lcom/google/android/gms/googlehelp/e/l;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/k;->b:Lcom/google/android/gms/googlehelp/e/m;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/k;->c:Lcom/google/android/gms/googlehelp/e/o;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/googlehelp/e/k;->cachedSize:I

    .line 36
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 114
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 115
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/k;->a:Lcom/google/android/gms/googlehelp/e/l;

    if-eqz v1, :cond_0

    .line 116
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/k;->a:Lcom/google/android/gms/googlehelp/e/l;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/k;->b:Lcom/google/android/gms/googlehelp/e/m;

    if-eqz v1, :cond_1

    .line 120
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/k;->b:Lcom/google/android/gms/googlehelp/e/m;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/k;->c:Lcom/google/android/gms/googlehelp/e/o;

    if-eqz v1, :cond_2

    .line 124
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/k;->c:Lcom/google/android/gms/googlehelp/e/o;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 48
    if-ne p1, p0, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v0

    .line 51
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/googlehelp/e/k;

    if-nez v2, :cond_2

    move v0, v1

    .line 52
    goto :goto_0

    .line 54
    :cond_2
    check-cast p1, Lcom/google/android/gms/googlehelp/e/k;

    .line 55
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/k;->a:Lcom/google/android/gms/googlehelp/e/l;

    if-nez v2, :cond_3

    .line 56
    iget-object v2, p1, Lcom/google/android/gms/googlehelp/e/k;->a:Lcom/google/android/gms/googlehelp/e/l;

    if-eqz v2, :cond_4

    move v0, v1

    .line 57
    goto :goto_0

    .line 60
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/k;->a:Lcom/google/android/gms/googlehelp/e/l;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/k;->a:Lcom/google/android/gms/googlehelp/e/l;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/googlehelp/e/l;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 61
    goto :goto_0

    .line 64
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/k;->b:Lcom/google/android/gms/googlehelp/e/m;

    if-nez v2, :cond_5

    .line 65
    iget-object v2, p1, Lcom/google/android/gms/googlehelp/e/k;->b:Lcom/google/android/gms/googlehelp/e/m;

    if-eqz v2, :cond_6

    move v0, v1

    .line 66
    goto :goto_0

    .line 69
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/k;->b:Lcom/google/android/gms/googlehelp/e/m;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/k;->b:Lcom/google/android/gms/googlehelp/e/m;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/googlehelp/e/m;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 70
    goto :goto_0

    .line 73
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/k;->c:Lcom/google/android/gms/googlehelp/e/o;

    if-nez v2, :cond_7

    .line 74
    iget-object v2, p1, Lcom/google/android/gms/googlehelp/e/k;->c:Lcom/google/android/gms/googlehelp/e/o;

    if-eqz v2, :cond_0

    move v0, v1

    .line 75
    goto :goto_0

    .line 78
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/k;->c:Lcom/google/android/gms/googlehelp/e/o;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/k;->c:Lcom/google/android/gms/googlehelp/e/o;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/googlehelp/e/o;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 79
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/k;->a:Lcom/google/android/gms/googlehelp/e/l;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 90
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/k;->b:Lcom/google/android/gms/googlehelp/e/m;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 92
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/k;->c:Lcom/google/android/gms/googlehelp/e/o;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 94
    return v0

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/k;->a:Lcom/google/android/gms/googlehelp/e/l;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/e/l;->hashCode()I

    move-result v0

    goto :goto_0

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/k;->b:Lcom/google/android/gms/googlehelp/e/m;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/e/m;->hashCode()I

    move-result v0

    goto :goto_1

    .line 92
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/k;->c:Lcom/google/android/gms/googlehelp/e/o;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/e/o;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/k;->a:Lcom/google/android/gms/googlehelp/e/l;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/googlehelp/e/l;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/e/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/k;->a:Lcom/google/android/gms/googlehelp/e/l;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/k;->a:Lcom/google/android/gms/googlehelp/e/l;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/k;->b:Lcom/google/android/gms/googlehelp/e/m;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/googlehelp/e/m;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/e/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/k;->b:Lcom/google/android/gms/googlehelp/e/m;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/k;->b:Lcom/google/android/gms/googlehelp/e/m;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/k;->c:Lcom/google/android/gms/googlehelp/e/o;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/googlehelp/e/o;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/e/o;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/k;->c:Lcom/google/android/gms/googlehelp/e/o;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/k;->c:Lcom/google/android/gms/googlehelp/e/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/k;->a:Lcom/google/android/gms/googlehelp/e/l;

    if-eqz v0, :cond_0

    .line 101
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/k;->a:Lcom/google/android/gms/googlehelp/e/l;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/k;->b:Lcom/google/android/gms/googlehelp/e/m;

    if-eqz v0, :cond_1

    .line 104
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/k;->b:Lcom/google/android/gms/googlehelp/e/m;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 106
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/k;->c:Lcom/google/android/gms/googlehelp/e/o;

    if-eqz v0, :cond_2

    .line 107
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/k;->c:Lcom/google/android/gms/googlehelp/e/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 109
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 110
    return-void
.end method
