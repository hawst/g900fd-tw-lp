.class final Lcom/google/android/gms/wallet/ia/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/cd;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ia/q;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/ia/q;)V
    .locals 0

    .prologue
    .line 1098
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    const/4 v6, 0x0

    .line 1123
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/q;->b(Lcom/google/android/gms/wallet/ia/q;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/q;->h(Lcom/google/android/gms/wallet/ia/q;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v3}, Lcom/google/android/gms/wallet/ia/q;->i(Lcom/google/android/gms/wallet/ia/q;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v4}, Lcom/google/android/gms/wallet/ia/q;->c(Lcom/google/android/gms/wallet/ia/q;)Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v5}, Lcom/google/android/gms/wallet/ia/q;->c(Lcom/google/android/gms/wallet/ia/q;)Z

    move-result v5

    const/4 v8, 0x0

    iget-object v7, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v7}, Lcom/google/android/gms/wallet/ia/q;->d(Lcom/google/android/gms/wallet/ia/q;)Ljava/lang/String;

    move-result-object v9

    iget-object v7, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v7}, Lcom/google/android/gms/wallet/ia/q;->e(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/checkout/inapp/proto/q;

    move-result-object v10

    iget-object v7, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v7}, Lcom/google/android/gms/wallet/ia/q;->f(Lcom/google/android/gms/wallet/ia/q;)Ljava/util/ArrayList;

    move-result-object v11

    move-object v7, v6

    invoke-static/range {v0 .. v11}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;ZZ[I[IILjava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    .line 1137
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    const/16 v2, 0x1f5

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/wallet/ia/q;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1138
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/j;)V
    .locals 9

    .prologue
    .line 1101
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {p1}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1102
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/q;->b(Lcom/google/android/gms/wallet/ia/q;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/q;->c(Lcom/google/android/gms/wallet/ia/q;)Z

    move-result v3

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/q;->c(Lcom/google/android/gms/wallet/ia/q;)Z

    move-result v4

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/q;->d(Lcom/google/android/gms/wallet/ia/q;)Ljava/lang/String;

    move-result-object v5

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/q;->e(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/checkout/inapp/proto/q;

    move-result-object v6

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/q;->f(Lcom/google/android/gms/wallet/ia/q;)Ljava/util/ArrayList;

    move-result-object v7

    const/4 v8, 0x0

    move-object v2, p1

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/j;ZZLjava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;Lcom/google/checkout/inapp/proto/a/b;)Landroid/content/Intent;

    move-result-object v0

    .line 1113
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    const/16 v2, 0x1f7

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/wallet/ia/q;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1119
    :goto_0
    return-void

    .line 1115
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object p1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    .line 1116
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, v1, Lcom/google/android/gms/wallet/ia/q;->v:Z

    .line 1117
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/r;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->g(Lcom/google/android/gms/wallet/ia/q;)V

    goto :goto_0

    .line 1116
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(Lcom/google/checkout/inapp/proto/j;)V
    .locals 0

    .prologue
    .line 1143
    return-void
.end method
