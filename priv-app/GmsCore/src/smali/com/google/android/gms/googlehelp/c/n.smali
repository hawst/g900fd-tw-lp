.class public final Lcom/google/android/gms/googlehelp/c/n;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/googlehelp/c/e;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/googlehelp/c/m;

.field private final b:Landroid/widget/TextView;

.field private c:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/c/d;Lcom/google/ad/a/a/h;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 58
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 60
    iget-object v0, p3, Lcom/google/ad/a/a/h;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/n;->setTag(Ljava/lang/Object;)V

    .line 61
    invoke-virtual {p0, v5}, Lcom/google/android/gms/googlehelp/c/n;->setOrientation(I)V

    .line 63
    new-instance v0, Lcom/google/android/gms/googlehelp/c/m;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/googlehelp/c/m;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/c/d;Lcom/google/ad/a/a/h;Lcom/google/android/gms/googlehelp/c/n;Z)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->a:Lcom/google/android/gms/googlehelp/c/m;

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->a:Lcom/google/android/gms/googlehelp/c/m;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/n;->addView(Landroid/view/View;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->a:Lcom/google/android/gms/googlehelp/c/m;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/m;->c()Landroid/widget/TextView;

    move-result-object v0

    .line 67
    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/n;->addView(Landroid/view/View;)V

    .line 71
    :cond_0
    iget-boolean v0, p3, Lcom/google/ad/a/a/h;->q:Z

    if-eqz v0, :cond_1

    .line 72
    new-instance v0, Lcom/google/android/gms/googlehelp/c/a;

    invoke-direct {v0, p1, p3, p0}, Lcom/google/android/gms/googlehelp/c/a;-><init>(Landroid/content/Context;Lcom/google/ad/a/a/h;Lcom/google/android/gms/googlehelp/c/n;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->b:Landroid/widget/TextView;

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->b:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/n;->addView(Landroid/view/View;)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/n;->a()V

    .line 78
    :goto_0
    return-void

    .line 76
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->b:Landroid/widget/TextView;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->b:Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->a:Lcom/google/android/gms/googlehelp/c/m;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/m;->d()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->a:Lcom/google/android/gms/googlehelp/c/m;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/m;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v1

    .line 162
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/n;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_6

    if-eqz v0, :cond_6

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 161
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->c:Ljava/util/List;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_1

    .line 164
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/n;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-ne v1, v4, :cond_0

    if-nez v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Landroid/widget/EditText;)V
    .locals 4

    .prologue
    const/4 v2, -0x2

    .line 109
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/n;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 110
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 112
    const/16 v2, 0x10

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 113
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 114
    sget v1, Lcom/google/android/gms/h;->aO:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 116
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/n;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 117
    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 118
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 119
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/n;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/googlehelp/c/n;->addView(Landroid/view/View;I)V

    .line 124
    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->oH:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 126
    new-instance v2, Lcom/google/android/gms/googlehelp/c/o;

    invoke-direct {v2, p0, p1, v1}, Lcom/google/android/gms/googlehelp/c/o;-><init>(Lcom/google/android/gms/googlehelp/c/n;Landroid/widget/EditText;Landroid/widget/LinearLayout;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->c:Ljava/util/List;

    if-nez v0, :cond_0

    .line 136
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->c:Ljava/util/List;

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    invoke-virtual {p1}, Landroid/widget/EditText;->requestFocus()Z

    .line 140
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/n;->a()V

    .line 141
    return-void
.end method

.method public final b(Landroid/widget/EditText;)V
    .locals 2

    .prologue
    .line 144
    invoke-virtual {p1}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 146
    if-ltz v0, :cond_1

    .line 147
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/n;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 152
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 153
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/n;->a()V

    .line 154
    return-void

    .line 149
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->a:Lcom/google/android/gms/googlehelp/c/m;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/m;->requestFocus()Z

    goto :goto_0
.end method

.method public final c()Ljava/util/List;
    .locals 7

    .prologue
    .line 82
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 83
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/n;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 84
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/n;->c:Ljava/util/List;

    if-nez v1, :cond_1

    .line 85
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/n;->a:Lcom/google/android/gms/googlehelp/c/m;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/c/m;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/googlehelp/common/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/e/n;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    :cond_0
    return-object v4

    .line 89
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v1, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/c/n;->a:Lcom/google/android/gms/googlehelp/c/m;

    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/c/m;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/googlehelp/common/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/e/n;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/c/n;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v1

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 93
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "_"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v3, v2, 0x1

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/gms/googlehelp/common/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/e/n;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v2, v3

    .line 95
    goto :goto_0
.end method

.method public final d()Ljava/util/List;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/n;->a:Lcom/google/android/gms/googlehelp/c/m;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/m;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/gms/googlehelp/c/c;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/n;->a:Lcom/google/android/gms/googlehelp/c/m;

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-array v0, v2, [Lcom/google/android/gms/googlehelp/c/c;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
