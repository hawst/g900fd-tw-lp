.class final Lcom/google/android/gms/googlehelp/search/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/googlehelp/search/SearchView;


# direct methods
.method constructor <init>(Lcom/google/android/gms/googlehelp/search/SearchView;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/search/d;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/d;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 107
    if-eqz v0, :cond_0

    .line 108
    invoke-static {}, Lcom/google/android/gms/googlehelp/search/SearchView;->d()Lcom/google/android/gms/googlehelp/search/o;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/search/d;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    iget-object v3, v1, Lcom/google/android/gms/googlehelp/search/o;->c:Ljava/lang/reflect/Method;

    if-eqz v3, :cond_1

    :try_start_0
    iget-object v1, v1, Lcom/google/android/gms/googlehelp/search/o;->c:Ljava/lang/reflect/Method;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    .line 108
    :cond_1
    invoke-virtual {v0, v2, v6}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0
.end method
