.class public final Lcom/google/android/gms/drive/api/a/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/service/b;


# instance fields
.field private final a:Lcom/google/android/gms/drive/api/c;

.field private final b:Lcom/google/android/gms/drive/api/h;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:[Ljava/lang/String;

.field private final h:Lcom/google/android/gms/common/internal/bg;

.field private final i:I

.field private final j:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/c;Lcom/google/android/gms/drive/api/h;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/gms/common/internal/bg;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/google/android/gms/drive/api/a/j;->a:Lcom/google/android/gms/drive/api/c;

    .line 68
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/j;->b:Lcom/google/android/gms/drive/api/h;

    .line 69
    iput p3, p0, Lcom/google/android/gms/drive/api/a/j;->c:I

    .line 70
    iput-object p4, p0, Lcom/google/android/gms/drive/api/a/j;->d:Ljava/lang/String;

    .line 71
    iput-object p5, p0, Lcom/google/android/gms/drive/api/a/j;->e:Ljava/lang/String;

    .line 72
    iput-object p6, p0, Lcom/google/android/gms/drive/api/a/j;->f:Ljava/lang/String;

    .line 73
    iput-object p7, p0, Lcom/google/android/gms/drive/api/a/j;->g:[Ljava/lang/String;

    .line 74
    iput-object p8, p0, Lcom/google/android/gms/drive/api/a/j;->h:Lcom/google/android/gms/common/internal/bg;

    .line 75
    iput p9, p0, Lcom/google/android/gms/drive/api/a/j;->i:I

    .line 76
    iput-object p10, p0, Lcom/google/android/gms/drive/api/a/j;->j:Landroid/os/Bundle;

    .line 77
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 176
    const-string v0, "ClientConnectionOperation"

    const-string v1, "Handling failure"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/j;->h:Lcom/google/android/gms/common/internal/bg;

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v1

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    .line 178
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 12

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x4

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 36
    check-cast p1, Lcom/google/android/gms/drive/api/DriveAsyncService;

    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/drive/u;->b(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/drive/auth/c; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/common/service/h; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    invoke-virtual {p1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a()Lcom/google/android/gms/common/service/e;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/j;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/j;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/common/service/e;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "<<default account>>"

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/j;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/j;->h:Lcom/google/android/gms/common/internal/bg;

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "ClientConnectionOperation"

    const-string v2, "Interrupted while awaiting initialization"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/j;->h:Lcom/google/android/gms/common/internal/bg;

    const/16 v1, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_1
    .catch Lcom/google/android/gms/drive/auth/c; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/gms/common/service/h; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "ClientConnectionOperation"

    const-string v2, "Handling authorization failure"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/j;->h:Lcom/google/android/gms/common/internal/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/auth/c;->a()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v2}, Landroid/content/Intent;->getFlags()I

    move-result v3

    invoke-static {p1, v8, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    const-string v3, "pendingIntent"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-interface {v1, v10, v9, v0}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    :try_start_2
    invoke-static {}, Lcom/google/android/gms/common/internal/ay;->b()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/j;->h:Lcom/google/android/gms/common/internal/bg;

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_2
    .catch Lcom/google/android/gms/drive/auth/c; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/gms/common/service/h; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v1, "ClientConnectionOperation"

    const-string v2, "Handling OperationException"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/j;->h:Lcom/google/android/gms/common/internal/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/common/service/h;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    invoke-interface {v1, v0, v9, v9}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_1
    :try_start_3
    new-instance v1, Lcom/google/android/gms/common/server/ClientContext;

    iget v2, p0, Lcom/google/android/gms/drive/api/a/j;->c:I

    iget-object v3, p0, Lcom/google/android/gms/drive/api/a/j;->d:Ljava/lang/String;

    invoke-direct {v1, v2, v0, v0, v3}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/j;->g:[Ljava/lang/String;

    array-length v3, v2

    move v0, v8

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    invoke-virtual {v1, v4}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/j;->a:Lcom/google/android/gms/drive/api/c;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/j;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/drive/api/a/j;->f:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/drive/api/a/j;->i:I

    iget-object v7, p0, Lcom/google/android/gms/drive/api/a/j;->j:Landroid/os/Bundle;

    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->a()Lcom/google/android/gms/drive/g/aw;

    move-result-object v6

    const-string v0, "proxy_type"

    const/4 v5, 0x0

    invoke-virtual {v7, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    new-instance v0, Lcom/google/android/gms/drive/api/e;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/api/e;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IILcom/google/android/gms/drive/g/aw;)V

    const-string v1, "bypass_initial_sync"

    const/4 v2, 0x0

    invoke-virtual {v7, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/drive/api/e;->s()V

    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/j;->b:Lcom/google/android/gms/drive/api/h;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/j;->h:Lcom/google/android/gms/common/internal/bg;

    invoke-interface {v1}, Lcom/google/android/gms/common/internal/bg;->asBinder()Landroid/os/IBinder;

    move-result-object v6

    new-instance v1, Lcom/google/android/gms/drive/api/f;

    iget-object v4, v2, Lcom/google/android/gms/drive/api/h;->a:Lcom/google/android/gms/drive/api/b;

    iget-object v5, v2, Lcom/google/android/gms/drive/api/h;->b:Lcom/google/android/gms/drive/api/k;

    const/4 v7, 0x0

    move-object v2, p1

    move-object v3, v0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/drive/api/f;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/api/b;Lcom/google/android/gms/drive/api/k;Landroid/os/IBinder;B)V

    const/4 v2, 0x0

    invoke-interface {v6, v1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "com.google.android.gms.drive.root_id"

    invoke-interface {v0}, Lcom/google/android/gms/drive/api/d;->c()Lcom/google/android/gms/drive/DriveId;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v3, "com.google.android.gms.drive.appdata_id"

    invoke-interface {v0}, Lcom/google/android/gms/drive/api/d;->d()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/j;->h:Lcom/google/android/gms/common/internal/bg;

    const/4 v3, 0x0

    invoke-interface {v1}, Lcom/google/android/gms/drive/internal/bx;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-interface {v0, v3, v1, v2}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_3
    .catch Lcom/google/android/gms/drive/auth/c; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/android/gms/common/service/h; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/gms/drive/auth/c;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Ljava/io/IOException;

    if-eqz v0, :cond_5

    const/4 v0, 0x7

    invoke-interface {v1, v0, v9, v9}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :cond_5
    invoke-interface {v1, v11, v9, v9}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    goto/16 :goto_0
.end method
