.class public final Lcom/google/android/gms/fitness/a/o;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Set;

.field private static final b:Lcom/google/android/gms/fitness/l/ad;

.field private static final c:Ljava/util/Set;


# instance fields
.field private final d:Lcom/google/android/gms/fitness/k/a;

.field private final e:Lcom/google/android/gms/fitness/l/z;

.field private final f:Lcom/google/android/gms/fitness/a/i;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/gms/fitness/a/p;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/a/p;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/a/o;->a:Ljava/util/Set;

    .line 48
    new-instance v0, Lcom/google/android/gms/fitness/l/ad;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/fitness/l/ad;-><init>(Ljava/util/Set;J)V

    sput-object v0, Lcom/google/android/gms/fitness/a/o;->b:Lcom/google/android/gms/fitness/l/ad;

    .line 51
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/gms/fitness/data/DataType;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/gms/fitness/data/DataType;->b:Lcom/google/android/gms/fitness/data/DataType;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/google/android/gms/fitness/data/DataType;->o:Lcom/google/android/gms/fitness/data/DataType;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/google/android/gms/fitness/a/o;->c:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/fitness/k/a;Lcom/google/android/gms/fitness/l/z;Lcom/google/android/gms/fitness/a/i;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/google/android/gms/fitness/a/o;->d:Lcom/google/android/gms/fitness/k/a;

    .line 63
    iput-object p2, p0, Lcom/google/android/gms/fitness/a/o;->e:Lcom/google/android/gms/fitness/l/z;

    .line 64
    iput-object p3, p0, Lcom/google/android/gms/fitness/a/o;->f:Lcom/google/android/gms/fitness/a/i;

    .line 65
    return-void
.end method

.method private a(Ljava/util/List;JILjava/util/concurrent/TimeUnit;ILcom/google/android/gms/fitness/data/DataSource;)I
    .locals 26

    .prologue
    .line 192
    invoke-static/range {p4 .. p6}, Lcom/google/android/gms/fitness/a/o;->b(ILjava/util/concurrent/TimeUnit;I)Ljava/util/Map;

    move-result-object v19

    .line 194
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/fitness/a/o;->e:Lcom/google/android/gms/fitness/l/z;

    invoke-interface/range {v19 .. v19}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/gms/fitness/l/z;->b(Ljava/util/Collection;)J

    move-result-wide v4

    .line 195
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_0

    .line 196
    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v4, Lcom/google/android/gms/fitness/g/c;->B:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v6, v4

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v4

    .line 197
    sget-object v6, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x1

    invoke-virtual {v6, v8, v9}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v6

    .line 199
    sub-long v4, p2, v4

    const-wide/16 v8, 0x0

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 200
    rem-long v6, v4, v6

    sub-long/2addr v4, v6

    .line 202
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/fitness/a/o;->e:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v6, v4, v5}, Lcom/google/android/gms/fitness/l/z;->c(J)V

    :cond_0
    move-wide v6, v4

    .line 207
    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v4, Lcom/google/android/gms/fitness/g/c;->C:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v8, v4

    invoke-virtual {v5, v8, v9}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v20

    .line 208
    add-long v4, v6, v20

    move-wide/from16 v0, p2

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v12

    .line 220
    if-nez p4, :cond_1

    const-wide/16 v10, 0x0

    .line 221
    :goto_0
    const/4 v4, 0x1

    move/from16 v0, p6

    if-eq v0, v4, :cond_2

    .line 222
    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v4, Lcom/google/android/gms/fitness/g/c;->A:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v8, v4

    invoke-virtual {v5, v8, v9}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v4

    .line 223
    add-long v8, v12, v4

    move-wide/from16 v16, v12

    .line 241
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/fitness/a/o;->f:Lcom/google/android/gms/fitness/a/i;

    const/4 v14, 0x1

    move-object/from16 v5, p1

    move/from16 v12, p6

    move-object/from16 v13, p7

    invoke-virtual/range {v4 .. v14}, Lcom/google/android/gms/fitness/a/i;->a(Ljava/util/List;JJJILcom/google/android/gms/fitness/data/DataSource;Z)Ljava/util/List;

    move-result-object v18

    .line 250
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    move-object/from16 v11, v18

    .line 253
    :goto_2
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 254
    invoke-interface/range {v19 .. v19}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/fitness/data/DataSource;

    .line 255
    invoke-static {v4}, Lcom/google/android/gms/fitness/data/DataSet;->a(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataSet;

    move-result-object v8

    invoke-interface {v10, v4, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 220
    :cond_1
    move/from16 v0, p4

    int-to-long v4, v0

    move-object/from16 v0, p5

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v10

    goto :goto_0

    .line 229
    :cond_2
    if-lez p4, :cond_15

    .line 230
    rem-long v4, v6, v10

    const-wide/16 v8, 0x0

    cmp-long v4, v4, v8

    if-lez v4, :cond_3

    .line 231
    rem-long v4, v6, v10

    sub-long v4, v10, v4

    add-long/2addr v6, v4

    .line 233
    :cond_3
    rem-long v4, v12, v10

    sub-long v8, v12, v4

    move-wide/from16 v16, v8

    .line 234
    goto :goto_1

    .line 250
    :cond_4
    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v12

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v22

    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/fitness/data/Bucket;

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/data/Bucket;->d()I

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_7

    const-wide v8, 0x7fffffffffffffffL

    :cond_5
    invoke-static {v12, v13, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_6
    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/fitness/data/Bucket;

    sget-object v11, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v11}, Lcom/google/android/gms/fitness/data/Bucket;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v12

    cmp-long v11, v12, v22

    if-gtz v11, :cond_6

    sget-object v11, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v11}, Lcom/google/android/gms/fitness/data/Bucket;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v12

    cmp-long v11, v12, v8

    if-ltz v11, :cond_6

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_7
    const-wide v4, 0x7fffffffffffffffL

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/fitness/a/o;->e:Lcom/google/android/gms/fitness/l/z;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-wide/16 v14, -0x1

    invoke-interface/range {v8 .. v15}, Lcom/google/android/gms/fitness/l/z;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-wide v8, v4

    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/fitness/data/Session;

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/fitness/data/Session;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v14

    const-wide/16 v24, 0x0

    cmp-long v5, v14, v24

    if-eqz v5, :cond_8

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/fitness/data/Session;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v14

    cmp-long v5, v14, v22

    if-lez v5, :cond_14

    :cond_8
    const-string v5, "Found an ongoing session: %s while aggregating session buckets in [%d, %d]"

    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v4, v11, v14

    const/4 v14, 0x1

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v11, v14

    const/4 v14, 0x2

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v11, v14

    invoke-static {v5, v11}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/fitness/data/Session;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    invoke-static {v8, v9, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    :goto_6
    move-wide v8, v4

    goto :goto_5

    :cond_9
    move-object v11, v5

    goto/16 :goto_2

    .line 259
    :cond_a
    const/4 v4, 0x0

    .line 260
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    move v12, v4

    :goto_7
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/fitness/data/Bucket;

    .line 262
    invoke-interface/range {v19 .. v19}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v14

    move v9, v12

    :cond_b
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_c

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/fitness/data/DataSource;

    .line 263
    invoke-interface {v10, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/gms/fitness/data/DataSet;

    .line 264
    invoke-virtual {v5}, Lcom/google/android/gms/fitness/data/DataSource;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/fitness/data/Bucket;->a(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/data/DataSet;

    move-result-object v5

    .line 265
    if-eqz v5, :cond_b

    .line 267
    invoke-virtual {v5}, Lcom/google/android/gms/fitness/data/DataSet;->d()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_8
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/fitness/data/DataPoint;

    .line 268
    invoke-static {v5, v8}, Lcom/google/android/gms/fitness/a/o;->a(Lcom/google/android/gms/fitness/data/DataPoint;Lcom/google/android/gms/fitness/data/DataSet;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v5

    invoke-virtual {v8, v5}, Lcom/google/android/gms/fitness/data/DataSet;->a(Lcom/google/android/gms/fitness/data/DataPoint;)V

    .line 269
    add-int/lit8 v9, v9, 0x1

    .line 270
    goto :goto_8

    :cond_c
    move v12, v9

    .line 273
    goto :goto_7

    .line 276
    :cond_d
    invoke-interface {v10}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_9
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/fitness/data/DataSet;

    .line 277
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/fitness/a/o;->e:Lcom/google/android/gms/fitness/l/z;

    sget-object v9, Lcom/google/android/gms/fitness/data/Application;->a:Lcom/google/android/gms/fitness/data/Application;

    const/4 v10, 0x0

    invoke-interface {v8, v4, v9, v10}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/data/DataSet;Lcom/google/android/gms/fitness/data/Application;Z)V

    goto :goto_9

    .line 283
    :cond_e
    if-nez v12, :cond_13

    sub-long v4, p2, v20

    cmp-long v4, v6, v4

    if-gez v4, :cond_13

    .line 284
    const-string v4, "No un-aggregated data found, inserting zero step delta"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 285
    sget-object v4, Lcom/google/android/gms/fitness/data/DataType;->a:Lcom/google/android/gms/fitness/data/DataType;

    .line 286
    move/from16 v0, p6

    move/from16 v1, p4

    move-object/from16 v2, p5

    invoke-static {v4, v4, v0, v1, v2}, Lcom/google/android/gms/fitness/a/e;->a(Lcom/google/android/gms/fitness/data/DataType;Lcom/google/android/gms/fitness/data/DataType;IILjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v5

    .line 292
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-wide/from16 v8, v16

    :cond_f
    :goto_a
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/fitness/data/Bucket;

    .line 293
    sget-object v13, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide/from16 v0, v16

    invoke-virtual {v13, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v14

    .line 294
    sget-object v13, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v13}, Lcom/google/android/gms/fitness/data/Bucket;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v18

    cmp-long v13, v18, v14

    if-lez v13, :cond_f

    sget-object v13, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v13}, Lcom/google/android/gms/fitness/data/Bucket;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v14

    cmp-long v13, v14, v8

    if-gez v13, :cond_f

    .line 296
    sget-object v8, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v8}, Lcom/google/android/gms/fitness/data/Bucket;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    goto :goto_a

    .line 300
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/fitness/a/o;->e:Lcom/google/android/gms/fitness/l/z;

    invoke-static {v5}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v5

    sget-object v10, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v5 .. v10}, Lcom/google/android/gms/fitness/data/DataPoint;->a(JJLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v5

    const/4 v8, 0x1

    new-array v8, v8, [I

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v10, v8, v9

    invoke-virtual {v5, v8}, Lcom/google/android/gms/fitness/data/DataPoint;->a([I)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v5

    sget-object v8, Lcom/google/android/gms/fitness/data/Application;->a:Lcom/google/android/gms/fitness/data/Application;

    invoke-interface {v4, v5, v8}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/data/DataPoint;Lcom/google/android/gms/fitness/data/Application;)Ljava/lang/Long;

    .line 306
    add-int/lit8 v12, v12, 0x1

    move v4, v12

    .line 310
    :goto_b
    const/4 v5, 0x4

    move/from16 v0, p6

    if-ne v0, v5, :cond_12

    if-eqz p7, :cond_12

    .line 311
    invoke-static {}, Lcom/google/android/gms/fitness/a/e;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/fitness/data/DataSet;->a(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataSet;

    move-result-object v8

    .line 312
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    move v5, v4

    :goto_c
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_11

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/fitness/data/Bucket;

    .line 313
    invoke-virtual {v8}, Lcom/google/android/gms/fitness/data/DataSet;->a()Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v9

    sget-object v10, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v10}, Lcom/google/android/gms/fitness/data/Bucket;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v10

    sget-object v12, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v12}, Lcom/google/android/gms/fitness/data/Bucket;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v12

    sget-object v14, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v9 .. v14}, Lcom/google/android/gms/fitness/data/DataPoint;->a(JJLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [I

    const/4 v11, 0x0

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/data/Bucket;->b()I

    move-result v4

    aput v4, v10, v11

    invoke-virtual {v9, v10}, Lcom/google/android/gms/fitness/data/DataPoint;->a([I)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v4

    invoke-virtual {v8, v4}, Lcom/google/android/gms/fitness/data/DataSet;->a(Lcom/google/android/gms/fitness/data/DataPoint;)V

    .line 319
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    .line 320
    goto :goto_c

    .line 321
    :cond_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/fitness/a/o;->e:Lcom/google/android/gms/fitness/l/z;

    sget-object v9, Lcom/google/android/gms/fitness/data/Application;->a:Lcom/google/android/gms/fitness/data/Application;

    const/4 v10, 0x0

    invoke-interface {v4, v8, v9, v10}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/data/DataSet;Lcom/google/android/gms/fitness/data/Application;Z)V

    move v4, v5

    .line 326
    :cond_12
    const-string v5, "Inserted %d aggregated data points for %s, %3$tF %3$tT - %4$tF %4$tT"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    move/from16 v0, p6

    move/from16 v1, p4

    move-object/from16 v2, p5

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/fitness/a/e;->a(IILjava/util/concurrent/TimeUnit;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    sget-object v10, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v10, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v8, v9

    const/4 v6, 0x3

    sget-object v7, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide/from16 v0, v16

    invoke-virtual {v7, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v8, v6

    invoke-static {v5, v8}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 331
    return v4

    :cond_13
    move v4, v12

    goto/16 :goto_b

    :cond_14
    move-wide v4, v8

    goto/16 :goto_6

    :cond_15
    move-wide v8, v12

    move-wide/from16 v16, v12

    goto/16 :goto_1
.end method

.method private static a(Lcom/google/android/gms/fitness/data/DataPoint;Lcom/google/android/gms/fitness/data/DataSet;)Lcom/google/android/gms/fitness/data/DataPoint;
    .locals 7

    .prologue
    .line 390
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSet;->a()Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v1

    .line 391
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->c(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/fitness/data/DataPoint;->a(JJLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/DataPoint;

    .line 395
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/DataPoint;->a()[Lcom/google/android/gms/fitness/data/Value;

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 396
    invoke-virtual {v1, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->a(I)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v2

    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->a(I)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/data/Value;->a(Lcom/google/android/gms/fitness/data/Value;)V

    .line 395
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 398
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/DataPoint;->d()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->b(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataPoint;

    .line 399
    return-object v1
.end method

.method public static a(ILjava/util/concurrent/TimeUnit;ILcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/data/DataSource;
    .locals 1

    .prologue
    .line 418
    sget-object v0, Lcom/google/android/gms/fitness/a/j;->a:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/a/g;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/a/g;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    .line 419
    invoke-static {p3, v0, p2, p0, p1}, Lcom/google/android/gms/fitness/a/e;->a(Lcom/google/android/gms/fitness/data/DataType;Lcom/google/android/gms/fitness/data/DataType;IILjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(ILjava/util/concurrent/TimeUnit;I)Ljava/util/Map;
    .locals 1

    .prologue
    .line 41
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/fitness/a/o;->b(ILjava/util/concurrent/TimeUnit;I)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private static b(ILjava/util/concurrent/TimeUnit;I)Ljava/util/Map;
    .locals 4

    .prologue
    .line 407
    new-instance v1, Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/fitness/data/DataType;->w:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 408
    sget-object v0, Lcom/google/android/gms/fitness/data/DataType;->w:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataType;

    .line 409
    invoke-static {p0, p1, p2, v0}, Lcom/google/android/gms/fitness/a/o;->a(ILjava/util/concurrent/TimeUnit;ILcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v3

    .line 411
    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 413
    :cond_0
    return-object v1
.end method

.method static synthetic b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/gms/fitness/a/o;->a:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic c()Lcom/google/android/gms/fitness/l/ad;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/gms/fitness/a/o;->b:Lcom/google/android/gms/fitness/l/ad;

    return-object v0
.end method

.method static synthetic d()Ljava/util/Set;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/gms/fitness/a/o;->c:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/fitness/l/ac;
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/google/android/gms/fitness/a/q;

    invoke-direct {v0, p0}, Lcom/google/android/gms/fitness/a/q;-><init>(Lcom/google/android/gms/fitness/a/o;)V

    return-object v0
.end method

.method public final a(J)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x1

    .line 153
    const-string v0, "Pre-aggregating Fitness data up to %1$tT %1$tF"

    new-array v1, v5, [Ljava/lang/Object;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 155
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v0, Lcom/google/android/gms/fitness/data/DataType;->w:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataType;

    iget-object v3, p0, Lcom/google/android/gms/fitness/a/o;->d:Lcom/google/android/gms/fitness/k/a;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/fitness/k/a;->b(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string v3, "No default data source found for %s"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataType;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    invoke-static {v3, v4}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/a/o;->d:Lcom/google/android/gms/fitness/k/a;

    sget-object v2, Lcom/google/android/gms/fitness/data/DataType;->d:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/k/a;->b(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v7

    .line 161
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x4

    move-object v0, p0

    move-wide v2, p1

    :try_start_0
    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/fitness/a/o;->a(Ljava/util/List;JILjava/util/concurrent/TimeUnit;ILcom/google/android/gms/fitness/data/DataSource;)I

    .line 162
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v0, p0

    move-wide v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/fitness/a/o;->a(Ljava/util/List;JILjava/util/concurrent/TimeUnit;ILcom/google/android/gms/fitness/data/DataSource;)I

    .line 169
    const/4 v4, 0x1

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v0, p0

    move-wide v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/fitness/a/o;->a(Ljava/util/List;JILjava/util/concurrent/TimeUnit;ILcom/google/android/gms/fitness/data/DataSource;)I

    .line 170
    const/4 v4, 0x1

    sget-object v5, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v0, p0

    move-wide v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/fitness/a/o;->a(Ljava/util/List;JILjava/util/concurrent/TimeUnit;ILcom/google/android/gms/fitness/data/DataSource;)I
    :try_end_0
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    :goto_1
    return-void

    .line 175
    :catch_0
    move-exception v0

    const-string v1, "Database issue pre-aggregating.  Will try again later."

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1
.end method
