.class final Lcom/google/android/gms/droidguard/l;
.super Lcom/google/android/gms/droidguard/c/t;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/droidguard/DroidGuardService;

.field private final b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/gms/droidguard/DroidGuardService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/google/android/gms/droidguard/l;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-direct {p0}, Lcom/google/android/gms/droidguard/c/t;-><init>()V

    .line 214
    iput-object p2, p0, Lcom/google/android/gms/droidguard/l;->b:Ljava/lang/String;

    .line 215
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/droidguard/c/p;
    .locals 3

    .prologue
    .line 258
    new-instance v0, Lcom/google/android/gms/droidguard/j;

    iget-object v1, p0, Lcom/google/android/gms/droidguard/l;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    iget-object v2, p0, Lcom/google/android/gms/droidguard/l;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/droidguard/j;-><init>(Lcom/google/android/gms/droidguard/DroidGuardService;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/droidguard/c/m;Ljava/lang/String;Ljava/util/Map;)V
    .locals 5

    .prologue
    .line 221
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/droidguard/l;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/DroidGuardService;->b(Lcom/google/android/gms/droidguard/DroidGuardService;)Lcom/google/android/gms/droidguard/d/e;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/droidguard/l;->b:Ljava/lang/String;

    new-instance v2, Lcom/google/android/gms/droidguard/n;

    iget-object v3, p0, Lcom/google/android/gms/droidguard/l;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    iget-object v4, p0, Lcom/google/android/gms/droidguard/l;->b:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/droidguard/n;-><init>(Lcom/google/android/gms/droidguard/DroidGuardService;Ljava/lang/String;)V

    invoke-virtual {v0, p2, v1, v2}, Lcom/google/android/gms/droidguard/d/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/droidguard/d/k;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/droidguard/d/f;

    invoke-direct {v2, v0, p3}, Lcom/google/android/gms/droidguard/d/f;-><init>(Lcom/google/android/gms/droidguard/d/e;Ljava/util/Map;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/droidguard/d/k;->a(Lcom/google/android/gms/droidguard/d/n;)Lcom/google/android/gms/droidguard/d/k;

    move-result-object v0

    .line 223
    new-instance v1, Lcom/google/android/gms/droidguard/m;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/gms/droidguard/m;-><init>(Lcom/google/android/gms/droidguard/l;Lcom/google/android/gms/droidguard/c/m;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/droidguard/d/k;->a(Lcom/google/android/gms/droidguard/d/m;)Lcom/google/android/gms/droidguard/d/k;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 254
    :goto_0
    return-void

    .line 244
    :catch_0
    move-exception v0

    .line 248
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ERROR : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 249
    invoke-interface {p1, v0}, Lcom/google/android/gms/droidguard/c/m;->a([B)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 250
    :catch_1
    move-exception v0

    .line 251
    const-string v1, "DroidGuardService"

    const-string v2, "Remote callback failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
