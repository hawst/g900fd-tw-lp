.class public final Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/content/Context;

.field private final d:Landroid/content/pm/PackageManager;

.field private final e:Landroid/content/res/Resources;

.field private final f:Ljava/lang/String;

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:Landroid/graphics/drawable/Drawable;

.field private l:Landroid/graphics/drawable/Drawable;

.field private m:Landroid/graphics/drawable/Drawable;

.field private n:Landroid/graphics/drawable/Drawable;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private final q:Ljava/util/Set;

.field private final r:Ljava/util/Set;

.field private final s:Ljava/util/Set;

.field private final t:Ljava/util/Set;

.field private final u:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-string v0, ";"

    sput-object v0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->b:Landroid/content/Context;

    .line 93
    iput-object p2, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->c:Landroid/content/Context;

    .line 94
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->d:Landroid/content/pm/PackageManager;

    .line 95
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    .line 96
    iput-object p3, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->f:Ljava/lang/String;

    .line 98
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->q:Ljava/util/Set;

    .line 99
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->r:Ljava/util/Set;

    .line 100
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->s:Ljava/util/Set;

    .line 101
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->t:Ljava/util/Set;

    .line 102
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->u:Ljava/util/Set;

    .line 103
    return-void
.end method

.method public static a(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Landroid/content/ContentResolver;)Landroid/net/Uri;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 446
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->g()Ljava/util/List;

    move-result-object v0

    .line 447
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 448
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/f;

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/f;->d()Ljava/lang/String;

    move-result-object v0

    .line 449
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 451
    invoke-static {p1, v0}, Landroid/provider/ContactsContract$Data;->getContactLookupUri(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 460
    :goto_0
    return-object v0

    .line 453
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->A()Ljava/util/List;

    move-result-object v0

    .line 454
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 455
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/s;

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/s;->f()Ljava/lang/String;

    move-result-object v0

    .line 456
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 458
    invoke-static {p1, v0}, Landroid/provider/ContactsContract$Data;->getContactLookupUri(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 460
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/g;
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 275
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->r:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 276
    invoke-static {v0, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_8

    if-eqz v0, :cond_1

    if-nez p3, :cond_2

    :cond_1
    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    move-object v0, v2

    .line 315
    :goto_1
    return-object v0

    .line 276
    :cond_2
    sget-object v6, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->a:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    sget-object v0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    array-length v0, v6

    array-length v8, v7

    if-eq v0, v8, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    :goto_2
    array-length v8, v6

    if-ge v0, v8, :cond_8

    aget-object v8, v6, v0

    invoke-static {v8}, Landroid/telephony/PhoneNumberUtils;->convertKeypadLettersToDigits(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aget-object v9, v7, v0

    const-string v10, "[\\\\+]?[0-9.*()/\\[\\]~x -]+"

    invoke-virtual {v8, v10}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    const-string v10, "[\\\\+]?[0-9.*()/\\[\\]~x -]+"

    invoke-virtual {v9, v10}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_5

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    invoke-static {v8}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v9}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v11

    sub-int/2addr v10, v11

    if-ne v10, v4, :cond_6

    invoke-virtual {v8, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const-string v11, "+1"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-virtual {v8, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_7

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_8
    move v0, v3

    goto :goto_0

    .line 281
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->h:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_a

    .line 282
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/gms/h;->cJ:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->h:Landroid/graphics/drawable/Drawable;

    .line 284
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->i:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_b

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/gms/h;->cK:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->i:Landroid/graphics/drawable/Drawable;

    .line 287
    :cond_b
    new-instance v5, Landroid/content/Intent;

    const-string v0, "android.intent.action.DIAL"

    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "tel:"

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 288
    invoke-static {p3}, Lcom/google/android/gms/smart_profile/aj;->b(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 290
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->d:Landroid/content/pm/PackageManager;

    invoke-virtual {v5, v0}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 291
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->r:Ljava/util/Set;

    invoke-static {p3}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 292
    new-instance v0, Lcom/google/android/gms/smart_profile/card/view/h;

    invoke-direct {v0}, Lcom/google/android/gms/smart_profile/card/view/h;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v2

    iget-object v7, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_c
    :goto_3
    packed-switch v0, :pswitch_data_0

    :goto_4
    invoke-virtual {v2, p2}, Lcom/google/android/gms/smart_profile/card/view/h;->c(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Landroid/content/Intent;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/smart_profile/b;->m:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/card/view/h;->a()Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/gms/p;->wt:I

    new-array v5, v3, [Ljava/lang/Object;

    aput-object p3, v5, v1

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/smart_profile/card/view/h;->d(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    .line 304
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->d:Landroid/content/pm/PackageManager;

    invoke-virtual {v6, v2}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_d

    .line 305
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->s:Ljava/util/Set;

    invoke-static {p3}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 306
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/smart_profile/card/view/h;->b(Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/google/android/gms/smart_profile/card/view/h;->b(Landroid/content/Intent;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/smart_profile/b;->n:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/smart_profile/card/view/h;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/gms/p;->wz:I

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p3, v3, v1

    invoke-virtual {v4, v5, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/gms/smart_profile/card/view/h;->e(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    .line 313
    :cond_d
    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    goto/16 :goto_1

    .line 292
    :sswitch_0
    const-string v4, "home"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    move v0, v1

    goto :goto_3

    :sswitch_1
    const-string v4, "work"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    move v0, v3

    goto :goto_3

    :sswitch_2
    const-string v8, "mobile"

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    move v0, v4

    goto :goto_3

    :sswitch_3
    const-string v4, "homeFax"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    const/4 v0, 0x3

    goto :goto_3

    :sswitch_4
    const-string v4, "workFax"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    const/4 v0, 0x4

    goto/16 :goto_3

    :sswitch_5
    const-string v4, "otherFax"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    const/4 v0, 0x5

    goto/16 :goto_3

    :sswitch_6
    const-string v4, "pager"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    const/4 v0, 0x6

    goto/16 :goto_3

    :sswitch_7
    const-string v4, "workMobile"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    const/4 v0, 0x7

    goto/16 :goto_3

    :sswitch_8
    const-string v4, "workPager"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    const/16 v0, 0x8

    goto/16 :goto_3

    :sswitch_9
    const-string v4, "main"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    const/16 v0, 0x9

    goto/16 :goto_3

    :sswitch_a
    const-string v4, "googleVoice"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    const/16 v0, 0xa

    goto/16 :goto_3

    :sswitch_b
    const-string v4, "other"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    const/16 v0, 0xb

    goto/16 :goto_3

    :pswitch_0
    sget v0, Lcom/google/android/gms/p;->wD:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_4

    :pswitch_1
    sget v0, Lcom/google/android/gms/p;->wK:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_4

    :pswitch_2
    sget v0, Lcom/google/android/gms/p;->wG:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_4

    :pswitch_3
    sget v0, Lcom/google/android/gms/p;->wE:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_4

    :pswitch_4
    sget v0, Lcom/google/android/gms/p;->wL:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_4

    :pswitch_5
    sget v0, Lcom/google/android/gms/p;->wI:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_4

    :pswitch_6
    sget v0, Lcom/google/android/gms/p;->wJ:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_4

    :pswitch_7
    sget v0, Lcom/google/android/gms/p;->wM:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_4

    :pswitch_8
    sget v0, Lcom/google/android/gms/p;->wN:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_4

    :pswitch_9
    sget v0, Lcom/google/android/gms/p;->wF:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_4

    :pswitch_a
    sget v0, Lcom/google/android/gms/p;->wC:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_4

    :pswitch_b
    sget v0, Lcom/google/android/gms/p;->wH:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_4

    :cond_e
    move-object v0, v2

    .line 315
    goto/16 :goto_1

    .line 292
    :sswitch_data_0
    .sparse-switch
        -0x45ce8613 -> :sswitch_5
        -0x40000ced -> :sswitch_7
        -0x3fb56f5e -> :sswitch_2
        -0x14747187 -> :sswitch_a
        0x30f4df -> :sswitch_0
        0x3305b9 -> :sswitch_9
        0x37c711 -> :sswitch_1
        0x6527f10 -> :sswitch_b
        0x657efc3 -> :sswitch_6
        0x4023fb32 -> :sswitch_8
        0x4120fdbe -> :sswitch_3
        0x5ae7a64c -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 467
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 468
    const-string v0, ""

    .line 484
    :goto_0
    return-object v0

    .line 470
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 472
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 473
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 474
    const/16 v3, 0xa

    invoke-static {v2, v3}, Ljava/lang/Character;->digit(CI)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    const/16 v3, 0x2b

    if-eq v2, v3, :cond_2

    const/16 v3, 0x2c

    if-eq v2, v3, :cond_2

    const/16 v3, 0x3b

    if-eq v2, v3, :cond_2

    const/16 v3, 0x61

    if-gt v3, v2, :cond_1

    const/16 v3, 0x7a

    if-le v2, v3, :cond_2

    :cond_1
    const/16 v3, 0x41

    if-gt v3, v2, :cond_3

    const/16 v3, 0x5a

    if-gt v2, v3, :cond_3

    .line 480
    :cond_2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 472
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 484
    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;Ljava/util/List;)V
    .locals 6

    .prologue
    .line 141
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->B()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 142
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->A()Ljava/util/List;

    move-result-object v2

    .line 144
    new-instance v0, Lcom/google/android/gms/smart_profile/header/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/smart_profile/header/a;-><init>(Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;)V

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 150
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 151
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 152
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/s;

    .line 153
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/s;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/s;->b()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/s;->f()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v4, v5, v0}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/g;

    move-result-object v0

    .line 156
    if-eqz v0, :cond_0

    .line 157
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    const-string v0, "2"

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 161
    :cond_1
    invoke-static {p2, v3}, Lcom/google/android/gms/smart_profile/card/view/g;->a(Ljava/util/List;Ljava/util/List;)V

    .line 163
    :cond_2
    return-void
.end method

.method private a(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 167
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->l()Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->t()Z

    move-result v0

    if-nez v0, :cond_6

    :cond_0
    move v0, v2

    :goto_0
    if-nez v0, :cond_5

    .line 168
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->k()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->t:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->k:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/gms/h;->cT:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->k:Landroid/graphics/drawable/Drawable;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->l:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/gms/h;->cS:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->l:Landroid/graphics/drawable/Drawable;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/l;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->c:Landroid/content/Context;

    invoke-static {v0, p4, v3, v2}, Lcom/google/android/gms/common/util/l;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/common/util/l;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->c:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-static {v2, p4, v3, v4}, Lcom/google/android/gms/common/util/l;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    :goto_2
    if-eqz v0, :cond_4

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->d:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v4}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v4

    if-eqz v4, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->t:Ljava/util/Set;

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/gms/smart_profile/card/view/h;

    invoke-direct {v1}, Lcom/google/android/gms/smart_profile/card/view/h;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/gms/p;->wx:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Landroid/content/Intent;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/smart_profile/b;->o:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/card/view/h;->a()Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    if-eqz v2, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->d:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/card/view/h;->b(Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/smart_profile/card/view/h;->b(Landroid/content/Intent;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/smart_profile/b;->p:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/smart_profile/card/view/h;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/gms/p;->wA:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/smart_profile/card/view/h;->e(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    :cond_3
    iget-object v1, v0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    .line 169
    :cond_4
    if-eqz v1, :cond_5

    .line 170
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    const-string v0, "4"

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    :cond_5
    return-void

    .line 167
    :cond_6
    const-string v0, "page"

    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->s()Lcom/google/android/gms/people/identity/models/r;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/people/identity/models/r;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_0

    :cond_7
    move-object v0, v1

    .line 168
    goto/16 :goto_1

    :cond_8
    move-object v2, v1

    goto :goto_2
.end method

.method private a()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 131
    :try_start_0
    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 136
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 131
    goto :goto_0

    .line 136
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;Ljava/util/List;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 177
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->B()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 178
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->A()Ljava/util/List;

    move-result-object v3

    .line 179
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 180
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 181
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/s;

    .line 182
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/s;->f()Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->s:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->j:Landroid/graphics/drawable/Drawable;

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    sget v6, Lcom/google/android/gms/h;->cK:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->j:Landroid/graphics/drawable/Drawable;

    :cond_0
    iget-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->p:Ljava/lang/String;

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    sget v6, Lcom/google/android/gms/p;->wy:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->p:Ljava/lang/String;

    :cond_1
    invoke-static {v0}, Lcom/google/android/gms/smart_profile/aj;->b(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->d:Landroid/content/pm/PackageManager;

    invoke-virtual {v5, v6}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->s:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v6, Lcom/google/android/gms/smart_profile/card/view/h;

    invoke-direct {v6}, Lcom/google/android/gms/smart_profile/card/view/h;-><init>()V

    iget-object v7, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, v7}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->p:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/gms/smart_profile/card/view/h;->c(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Landroid/content/Intent;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/smart_profile/b;->n:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v5, v6}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/smart_profile/card/view/h;->a()Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    sget v7, Lcom/google/android/gms/p;->wz:I

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v0, v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/android/gms/smart_profile/card/view/h;->d(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    .line 183
    :goto_1
    if-eqz v0, :cond_2

    .line 184
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    const-string v0, "3"

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 182
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 188
    :cond_4
    invoke-static {p2, v4}, Lcom/google/android/gms/smart_profile/card/view/g;->a(Ljava/util/List;Ljava/util/List;)V

    .line 190
    :cond_5
    return-void
.end method

.method private c(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;Ljava/util/List;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 193
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->h()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 194
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->g()Ljava/util/List;

    move-result-object v3

    .line 195
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 196
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 197
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/f;

    .line 198
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/f;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->q:Ljava/util/Set;

    invoke-interface {v5, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->g:Landroid/graphics/drawable/Drawable;

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    sget v6, Lcom/google/android/gms/h;->cO:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->g:Landroid/graphics/drawable/Drawable;

    :cond_0
    iget-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->o:Ljava/lang/String;

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    sget v6, Lcom/google/android/gms/p;->wu:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->o:Ljava/lang/String;

    :cond_1
    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.SENDTO"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "mailto:"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v6, "android.intent.extra.EMAIL"

    new-array v7, v9, [Ljava/lang/String;

    aput-object v0, v7, v2

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->d:Landroid/content/pm/PackageManager;

    invoke-virtual {v5, v6}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->q:Ljava/util/Set;

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v6, Lcom/google/android/gms/smart_profile/card/view/h;

    invoke-direct {v6}, Lcom/google/android/gms/smart_profile/card/view/h;-><init>()V

    iget-object v7, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, v7}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->o:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/gms/smart_profile/card/view/h;->c(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Landroid/content/Intent;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/smart_profile/b;->l:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v5, v6}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/smart_profile/card/view/h;->a()Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    sget v7, Lcom/google/android/gms/p;->wv:I

    new-array v8, v9, [Ljava/lang/Object;

    aput-object v0, v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/android/gms/smart_profile/card/view/h;->d(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    .line 199
    :goto_1
    if-eqz v0, :cond_2

    .line 200
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    const-string v0, "1"

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 198
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 204
    :cond_4
    invoke-static {p2, v4}, Lcom/google/android/gms/smart_profile/card/view/g;->a(Ljava/util/List;Ljava/util/List;)V

    .line 206
    :cond_5
    return-void
.end method

.method private d(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;Ljava/util/List;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 209
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->d()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 210
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->c()Ljava/util/List;

    move-result-object v3

    .line 211
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 212
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 213
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/b;

    .line 214
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/b;->d()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 215
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/b;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->u:Ljava/util/Set;

    invoke-interface {v5, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->m:Landroid/graphics/drawable/Drawable;

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    sget v6, Lcom/google/android/gms/h;->cU:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->m:Landroid/graphics/drawable/Drawable;

    :cond_0
    iget-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->n:Landroid/graphics/drawable/Drawable;

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    sget v6, Lcom/google/android/gms/h;->cM:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->n:Landroid/graphics/drawable/Drawable;

    :cond_1
    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "geo:0,0?q="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v7, "https://maps.google.com/maps?daddr=%s"

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v7, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->d:Landroid/content/pm/PackageManager;

    invoke-virtual {v5, v7}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v7

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->u:Ljava/util/Set;

    invoke-interface {v7, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v7, Lcom/google/android/gms/smart_profile/card/view/h;

    invoke-direct {v7}, Lcom/google/android/gms/smart_profile/card/view/h;-><init>()V

    iget-object v8, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7, v8}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Landroid/content/Intent;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v5

    sget-object v7, Lcom/google/android/gms/smart_profile/b;->q:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v5, v7}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/smart_profile/card/view/h;->a()Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v5

    iget-object v7, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    sget v8, Lcom/google/android/gms/p;->wr:I

    new-array v9, v10, [Ljava/lang/Object;

    aput-object v0, v9, v2

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/android/gms/smart_profile/card/view/h;->d(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v5

    iget-object v7, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->d:Landroid/content/pm/PackageManager;

    invoke-virtual {v6, v7}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, v7}, Lcom/google/android/gms/smart_profile/card/view/h;->b(Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/google/android/gms/smart_profile/card/view/h;->b(Landroid/content/Intent;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v6

    sget-object v7, Lcom/google/android/gms/smart_profile/b;->r:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v6, v7}, Lcom/google/android/gms/smart_profile/card/view/h;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e:Landroid/content/res/Resources;

    sget v8, Lcom/google/android/gms/p;->ws:I

    new-array v9, v10, [Ljava/lang/Object;

    aput-object v0, v9, v2

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/android/gms/smart_profile/card/view/h;->e(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    :cond_2
    iget-object v0, v5, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    .line 216
    :goto_1
    if-eqz v0, :cond_3

    .line 217
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    const-string v0, "6"

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 215
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 222
    :cond_5
    invoke-static {p2, v4}, Lcom/google/android/gms/smart_profile/card/view/g;->a(Ljava/util/List;Ljava/util/List;)V

    .line 224
    :cond_6
    return-void
.end method

.method private e(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;Ljava/util/List;)V
    .locals 4

    .prologue
    .line 227
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 228
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->L()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/bd;

    .line 231
    const-string v2, "com.google"

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bd;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 232
    new-instance v2, Lcom/google/android/gms/smart_profile/card/view/h;

    invoke-direct {v2}, Lcom/google/android/gms/smart_profile/card/view/h;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->c:Landroid/content/Context;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/smart_profile/bd;->a(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bd;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bd;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/smart_profile/card/view/h;->c(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bd;->i()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Landroid/content/Intent;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/smart_profile/b;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    .line 235
    if-eqz v0, :cond_0

    .line 236
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    const-string v0, "0"

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 241
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/lang/String;)Landroid/util/Pair;
    .locals 3

    .prologue
    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 108
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 114
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 115
    invoke-direct {p0, p1, v0, v1, p2}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->a(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    .line 116
    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->a(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;Ljava/util/List;)V

    .line 121
    :goto_0
    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->b(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;Ljava/util/List;)V

    .line 122
    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->c(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;Ljava/util/List;)V

    .line 123
    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->e(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;Ljava/util/List;)V

    .line 124
    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->d(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;Ljava/util/List;)V

    .line 126
    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2

    .line 118
    :cond_0
    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->a(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;Ljava/util/List;)V

    .line 119
    invoke-direct {p0, p1, v0, v1, p2}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->a(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0
.end method
