.class final Lcom/google/android/gms/wallet/service/ia/ah;
.super Lcom/google/android/gms/wallet/service/o;
.source "SourceFile"


# instance fields
.field final synthetic c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field final synthetic d:Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;

.field final synthetic e:Lcom/google/android/gms/wallet/service/ia/ad;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/service/ia/ad;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ia/ah;->e:Lcom/google/android/gms/wallet/service/ia/ad;

    iput-object p4, p0, Lcom/google/android/gms/wallet/service/ia/ah;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object p5, p0, Lcom/google/android/gms/wallet/service/ia/ah;->d:Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;

    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/wallet/service/o;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/a/b;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 9

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ah;->e:Lcom/google/android/gms/wallet/service/ia/ad;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ia/ad;->a(Lcom/google/android/gms/wallet/service/ia/ad;)Lcom/google/android/gms/wallet/service/ia/o;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ah;->e:Lcom/google/android/gms/wallet/service/ia/ad;

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ah;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ia/ad;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ah;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ah;->d:Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;->b()Lcom/google/checkout/inapp/proto/u;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ah;->a:Landroid/accounts/Account;

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ah;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v1, Lcom/google/android/gms/wallet/service/ia/o;->a:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/u;

    move-object v4, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/wallet/service/ia/u;-><init>(Lcom/google/android/gms/wallet/service/ia/o;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/a/b;Lcom/google/checkout/inapp/proto/u;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "billing_make_payment"

    invoke-static {v8, v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/gms/common/util/w;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    return-object v0
.end method
