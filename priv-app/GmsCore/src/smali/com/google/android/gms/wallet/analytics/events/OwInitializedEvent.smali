.class public Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;
.super Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field public final d:I

.field public final e:I

.field public final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/k;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/analytics/events/k;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(IIILcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>()V

    .line 31
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->j:Ljava/lang/String;

    .line 32
    iput-object p5, p0, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->a:Ljava/lang/String;

    .line 33
    iput p1, p0, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->d:I

    .line 34
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->e:I

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->f:I

    .line 36
    iput-object p4, p0, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 37
    invoke-virtual {p0, p4}, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>(Landroid/os/Parcel;)V

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->d:I

    .line 43
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->e:I

    .line 44
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->f:I

    .line 45
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 46
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;

    const/4 v2, 0x1

    const/4 v3, 0x0

    move v1, p1

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;-><init>(IIILcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;)V

    .line 52
    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 53
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 64
    iget v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 65
    iget v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 66
    iget v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 68
    return-void
.end method
