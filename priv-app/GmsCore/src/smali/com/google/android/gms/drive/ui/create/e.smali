.class final Lcom/google/android/gms/drive/ui/create/e;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/ui/create/a;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/drive/ui/create/a;)V
    .locals 0

    .prologue
    .line 386
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/create/e;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/ui/create/a;B)V
    .locals 0

    .prologue
    .line 386
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/ui/create/e;-><init>(Lcom/google/android/gms/drive/ui/create/a;)V

    return-void
.end method

.method private varargs a()Lcom/google/android/gms/drive/DriveId;
    .locals 7

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/e;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/create/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/h;

    .line 391
    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/h;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 392
    const-string v2, "callerPackageName"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 393
    const-string v3, "accountName"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 394
    new-instance v4, Lcom/google/android/gms/common/api/w;

    invoke-direct {v4, v0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v0, Lcom/google/android/gms/drive/b;->g:Lcom/google/android/gms/common/api/c;

    new-instance v5, Lcom/google/android/gms/drive/g;

    invoke-direct {v5}, Lcom/google/android/gms/drive/g;-><init>()V

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Lcom/google/android/gms/drive/g;->a(I)Lcom/google/android/gms/drive/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/g;->a()Lcom/google/android/gms/drive/f;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    iput-object v2, v0, Lcom/google/android/gms/common/api/w;->b:Ljava/lang/String;

    const-string v2, "clientScopes"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a([Ljava/lang/String;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    iput-object v3, v0, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 404
    const-wide/16 v2, 0x5

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/common/api/v;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/c;

    move-result-object v0

    .line 405
    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 407
    :try_start_0
    sget-object v0, Lcom/google/android/gms/drive/b;->j:Lcom/google/android/gms/drive/ab;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/create/e;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-static {v2}, Lcom/google/android/gms/drive/ui/create/a;->c(Lcom/google/android/gms/drive/ui/create/a;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/drive/am;

    iget-object v4, p0, Lcom/google/android/gms/drive/ui/create/e;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-static {v4}, Lcom/google/android/gms/drive/ui/create/a;->g(Lcom/google/android/gms/drive/ui/create/a;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/gms/drive/am;-><init>(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    iget-object v4, p0, Lcom/google/android/gms/drive/ui/create/e;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-static {v4}, Lcom/google/android/gms/drive/ui/create/a;->h(Lcom/google/android/gms/drive/ui/create/a;)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/gms/drive/ui/create/e;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-static {v5}, Lcom/google/android/gms/drive/ui/create/a;->i(Lcom/google/android/gms/drive/ui/create/a;)I

    move-result v5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/drive/ab;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/am;II)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/j;

    .line 410
    invoke-interface {v0}, Lcom/google/android/gms/drive/j;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    .line 411
    const-string v3, "CreateFileDialogFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "createFileFromUi completed with status "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 413
    invoke-interface {v0}, Lcom/google/android/gms/drive/j;->b()Lcom/google/android/gms/drive/DriveId;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 416
    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->d()V

    .line 419
    :goto_0
    return-object v0

    .line 416
    :cond_0
    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->d()V

    .line 419
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 416
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->d()V

    throw v0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 386
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/create/e;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 386
    check-cast p1, Lcom/google/android/gms/drive/DriveId;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/e;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-static {v0, p1}, Lcom/google/android/gms/drive/ui/create/a;->a(Lcom/google/android/gms/drive/ui/create/a;Lcom/google/android/gms/drive/DriveId;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/e;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/ui/create/a;->j(Lcom/google/android/gms/drive/ui/create/a;)V

    goto :goto_0
.end method
