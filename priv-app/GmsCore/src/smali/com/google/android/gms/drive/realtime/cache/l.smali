.class public final Lcom/google/android/gms/drive/realtime/cache/l;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;Lcom/google/android/gms/drive/database/r;)V
    .locals 7

    .prologue
    .line 32
    const-string v0, "RealtimeCacheCleanup"

    const-string v1, "Beginning Realtime garbage collection."

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    invoke-interface {p1}, Lcom/google/android/gms/drive/database/r;->w()Ljava/util/Set;

    move-result-object v0

    .line 34
    new-instance v1, Lcom/google/android/gms/drive/realtime/cache/m;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/realtime/cache/m;-><init>(Ljava/util/Set;)V

    .line 48
    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->a()Lcom/google/android/gms/drive/g/aw;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/drive/realtime/b/a;->a(Landroid/content/Context;Lcom/google/android/gms/drive/g/aw;)Lcom/google/android/gms/drive/realtime/b/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/realtime/b/a;->e()Lcom/google/android/gms/drive/realtime/cache/c;

    .line 49
    invoke-static {p0}, Lcom/google/android/gms/drive/realtime/cache/c;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 50
    invoke-virtual {v0, v1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    .line 51
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 52
    const-string v4, "RealtimeCacheCleanup"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Deleting Realtime cache database: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 55
    :cond_0
    const-string v0, "RealtimeCacheCleanup"

    const-string v1, "Finished Realtime garbage collection."

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    return-void
.end method
