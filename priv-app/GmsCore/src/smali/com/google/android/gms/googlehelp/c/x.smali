.class public final Lcom/google/android/gms/googlehelp/c/x;
.super Landroid/widget/Spinner;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/googlehelp/c/af;
.implements Lcom/google/android/gms/googlehelp/c/c;
.implements Lcom/google/android/gms/googlehelp/c/e;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# instance fields
.field private final a:Z

.field private b:Lcom/google/android/gms/googlehelp/c/d;

.field private c:Lcom/google/android/gms/googlehelp/c/ac;

.field private d:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/c/d;Lcom/google/ad/a/a/h;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 59
    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    .line 61
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/c/x;->b:Lcom/google/android/gms/googlehelp/c/d;

    .line 62
    iput-object v1, p0, Lcom/google/android/gms/googlehelp/c/x;->c:Lcom/google/android/gms/googlehelp/c/ac;

    .line 63
    iput-object v1, p0, Lcom/google/android/gms/googlehelp/c/x;->d:Ljava/util/List;

    .line 64
    iget-boolean v1, p3, Lcom/google/ad/a/a/h;->c:Z

    iput-boolean v1, p0, Lcom/google/android/gms/googlehelp/c/x;->a:Z

    .line 65
    iget-boolean v1, p0, Lcom/google/android/gms/googlehelp/c/x;->a:Z

    if-eqz v1, :cond_0

    .line 66
    new-instance v1, Lcom/google/android/gms/googlehelp/c/y;

    invoke-direct {v1, p0}, Lcom/google/android/gms/googlehelp/c/y;-><init>(Lcom/google/android/gms/googlehelp/c/x;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/googlehelp/c/x;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 80
    :cond_0
    iget-object v1, p3, Lcom/google/ad/a/a/h;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/googlehelp/c/x;->setTag(Ljava/lang/Object;)V

    .line 82
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 83
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 84
    const/4 v1, -0x1

    .line 86
    iget-object v6, p3, Lcom/google/ad/a/a/h;->e:[Lcom/google/ad/a/a/k;

    array-length v7, v6

    move v2, v0

    move v9, v0

    move v0, v1

    move v1, v9

    :goto_0
    if-ge v2, v7, :cond_2

    aget-object v3, v6, v2

    .line 87
    iget-object v8, v3, Lcom/google/ad/a/a/k;->a:Ljava/lang/String;

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    iget-object v8, v3, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/gms/common/util/au;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    iget-boolean v3, v3, Lcom/google/ad/a/a/k;->c:Z

    if-eqz v3, :cond_1

    move v0, v1

    .line 93
    :cond_1
    add-int/lit8 v3, v1, 0x1

    .line 86
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_0

    .line 96
    :cond_2
    new-instance v1, Lcom/google/android/gms/googlehelp/c/ae;

    invoke-direct {v1, p1, v5, v4}, Lcom/google/android/gms/googlehelp/c/ae;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V

    .line 98
    invoke-virtual {p0, v1}, Lcom/google/android/gms/googlehelp/c/x;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 99
    if-ltz v0, :cond_3

    .line 100
    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/x;->setSelection(I)V

    .line 102
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/x;->b:Lcom/google/android/gms/googlehelp/c/d;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/googlehelp/c/d;->a(Lcom/google/android/gms/googlehelp/c/af;)V

    .line 103
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/c/x;)Lcom/google/android/gms/googlehelp/c/ac;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/x;->c:Lcom/google/android/gms/googlehelp/c/ac;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/googlehelp/c/ac;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/c/x;->c:Lcom/google/android/gms/googlehelp/c/ac;

    .line 153
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 107
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/c/x;->d:Ljava/util/List;

    .line 108
    new-instance v0, Lcom/google/android/gms/googlehelp/c/z;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/c/z;-><init>(Lcom/google/android/gms/googlehelp/c/x;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/x;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 119
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/x;->getSelectedView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/x;->b:Lcom/google/android/gms/googlehelp/c/d;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/d;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/util/List;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 157
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/x;->e()Ljava/lang/String;

    move-result-object v1

    .line 158
    if-nez v1, :cond_0

    new-array v0, v3, [Lcom/google/android/gms/googlehelp/e/n;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    new-array v2, v0, [Lcom/google/android/gms/googlehelp/e/n;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/x;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/googlehelp/common/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/e/n;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Ljava/util/List;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 165
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/x;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/gms/googlehelp/c/c;

    aput-object p0, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-array v0, v1, [Lcom/google/android/gms/googlehelp/c/c;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/x;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 124
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/x;->d:Ljava/util/List;

    if-nez v0, :cond_1

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/x;->d:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/c/ad;->a(Ljava/util/List;)V

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/x;->c:Lcom/google/android/gms/googlehelp/c/ac;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/x;->c:Lcom/google/android/gms/googlehelp/c/ac;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/ac;->a()V

    goto :goto_0
.end method
