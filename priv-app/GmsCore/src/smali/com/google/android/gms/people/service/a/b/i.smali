.class public final Lcom/google/android/gms/people/service/a/b/i;
.super Lcom/google/android/gms/people/service/a/b/c;
.source "SourceFile"


# instance fields
.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/Boolean;

.field private final l:Ljava/lang/String;

.field private final m:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 42
    move-object v1, p0

    move-object v2, p1

    move-object v3, p6

    move-object v4, p2

    move v5, p4

    move-object v6, p5

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/people/service/a/b/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/b/i;->i:Ljava/lang/String;

    .line 45
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/b/i;->j:Ljava/lang/String;

    .line 46
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/b/i;->k:Ljava/lang/Boolean;

    .line 47
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/b/i;->l:Ljava/lang/String;

    .line 48
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    iput-object p2, p0, Lcom/google/android/gms/people/service/a/b/i;->m:Ljava/lang/String;

    .line 49
    return-void

    :cond_0
    move-object p2, p3

    .line 48
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/people/service/e;Lcom/google/android/gms/people/e/a;)Landroid/util/Pair;
    .locals 10

    .prologue
    const/4 v2, 0x3

    const/4 v9, 0x1

    .line 55
    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    const-string v0, "PeopleService"

    const-string v1, "updateCircle: name=%s efs=%s desc=%s"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/b/i;->j:Ljava/lang/String;

    aput-object v4, v2, v3

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/i;->k:Ljava/lang/Boolean;

    aput-object v3, v2, v9

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/b/i;->l:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/u;->b()V

    .line 64
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/i;->i:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/i;->j:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/b/i;->k:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/google/android/gms/people/service/a/b/i;->l:Ljava/lang/String;

    move-object v0, p2

    move-object v1, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/people/service/e;->a(Lcom/google/android/gms/people/e/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v2whitelisted/models/a;

    .line 67
    invoke-static {p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/u;->b()V

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/i;->m:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/b/i;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/i;->i:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/i;->j:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/b/i;->k:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/b/i;->l:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/people/service/a/b/i;->h:Ljava/lang/String;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v9}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "p"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v3, :cond_1

    new-instance v2, Lcom/google/android/gms/common/server/y;

    invoke-direct {v2, p1}, Lcom/google/android/gms/common/server/y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/server/y;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/plus/a/m;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/common/server/y;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/plus/a/b;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/google/android/gms/common/server/y;->c(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v2

    invoke-static {v6}, Lcom/google/android/gms/common/analytics/e;->b(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/server/y;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/y;)V

    :cond_1
    if-eqz v4, :cond_2

    new-instance v2, Lcom/google/android/gms/common/server/y;

    invoke-direct {v2, p1}, Lcom/google/android/gms/common/server/y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/server/y;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/plus/a/m;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/server/y;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/plus/a/b;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/google/android/gms/common/server/y;->c(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    invoke-static {v6}, Lcom/google/android/gms/common/analytics/e;->b(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/server/y;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/y;)V

    .line 74
    :cond_2
    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/b/i;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/i;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/i;->i:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/b/i;->j:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/people/service/a/b/i;->k:Ljava/lang/Boolean;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)I

    move-result v0

    .line 77
    if-eq v0, v9, :cond_4

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/i;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/b/i;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/i;->h:Ljava/lang/String;

    const-string v3, "UpdateCircle"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 88
    :cond_3
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.people.BROADCAST_CIRCLES_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.permission.INTERNAL_BROADCAST"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 90
    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lcom/google/android/gms/people/service/b;->c:Lcom/google/android/gms/people/service/b;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    .line 81
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/i;->h:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/i;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/i;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/b/i;->g:Ljava/lang/String;

    const-string v2, "UpdateCircle"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/sync/u;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
