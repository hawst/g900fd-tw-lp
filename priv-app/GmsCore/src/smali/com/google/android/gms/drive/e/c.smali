.class final Lcom/google/android/gms/drive/e/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/e/b;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/drive/e/b;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/google/android/gms/drive/e/c;->a:Lcom/google/android/gms/drive/e/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/e/b;B)V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/e/c;-><init>(Lcom/google/android/gms/drive/e/b;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 191
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/drive/e/c;->a:Lcom/google/android/gms/drive/e/b;

    const-string v0, "PinnedContentDownloader"

    const-string v1, "Started downloading pinned content."

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v2, Lcom/google/android/gms/drive/e/b;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :goto_0
    iget-object v3, v2, Lcom/google/android/gms/drive/e/b;->e:Ljava/util/Map;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v0, v2, Lcom/google/android/gms/drive/e/b;->a:Lcom/google/android/gms/drive/g/n;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/n;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "PinnedContentDownloader"

    const-string v1, "Stopped downloading pinned content because the device is offline."

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v3

    .line 197
    :goto_1
    return-void

    .line 191
    :cond_0
    iget-object v0, v2, Lcom/google/android/gms/drive/e/b;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->v()Lcom/google/android/gms/drive/database/b/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    :try_start_2
    invoke-interface {v4}, Lcom/google/android/gms/drive/database/b/d;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ah;

    iget-object v1, v2, Lcom/google/android/gms/drive/e/b;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v6

    sget-object v1, Lcom/google/android/gms/drive/ai;->H:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge v6, v1, :cond_2

    iget-object v1, v2, Lcom/google/android/gms/drive/e/b;->e:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v2, v0}, Lcom/google/android/gms/drive/e/b;->a(Lcom/google/android/gms/drive/database/model/ah;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "PinnedContentDownloader"

    const-string v6, "Queueing download of file (%d of %d): %s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, v2, Lcom/google/android/gms/drive/e/b;->e:Ljava/util/Map;

    invoke-interface {v9}, Ljava/util/Map;->size()I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    sget-object v9, Lcom/google/android/gms/drive/ai;->H:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v9}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v1, v6, v7}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    new-instance v1, Lcom/google/android/gms/drive/e/d;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/e/d;-><init>(Lcom/google/android/gms/drive/e/b;Lcom/google/android/gms/drive/database/model/ah;)V

    iget-object v6, v2, Lcom/google/android/gms/drive/e/b;->e:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    invoke-interface {v6, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v2, Lcom/google/android/gms/drive/e/b;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-interface {v4}, Lcom/google/android/gms/drive/database/b/d;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v3

    throw v0
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 196
    :catch_0
    move-exception v0

    goto/16 :goto_1

    .line 191
    :cond_2
    :try_start_5
    invoke-interface {v4}, Lcom/google/android/gms/drive/database/b/d;->close()V

    iget-object v0, v2, Lcom/google/android/gms/drive/e/b;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "PinnedContentDownloader"

    const-string v1, "Finished downloading pinned content."

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v3

    goto/16 :goto_1

    :cond_3
    iget-object v0, v2, Lcom/google/android/gms/drive/e/b;->e:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_0

    .line 194
    :catch_1
    move-exception v0

    .line 195
    const-string v1, "PinnedContentDownloader"

    const-string v2, "Pinned content download task failed."

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
