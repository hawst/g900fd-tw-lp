.class public abstract Lcom/google/android/gms/drive/api/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/service/b;


# instance fields
.field protected final a:Lcom/google/android/gms/drive/api/d;

.field protected final b:Lcom/google/android/gms/drive/internal/ca;

.field protected final c:Lcom/google/android/gms/drive/c/a;

.field protected final d:Z


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 2

    .prologue
    .line 51
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;Lcom/google/android/gms/drive/c/a;Z)V

    .line 52
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;I)V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;IZ)V

    .line 47
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;IZ)V
    .locals 2

    .prologue
    .line 38
    invoke-interface {p1}, Lcom/google/android/gms/drive/api/d;->b()Lcom/google/android/gms/drive/c/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/b;->c()Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/a;->c()Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1, p3}, Lcom/google/android/gms/drive/c/a;->a(II)Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, p4}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;Lcom/google/android/gms/drive/c/a;Z)V

    .line 42
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;Lcom/google/android/gms/drive/c/a;Z)V
    .locals 2

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/api/d;

    iput-object v0, p0, Lcom/google/android/gms/drive/api/a/b;->a:Lcom/google/android/gms/drive/api/d;

    .line 57
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/ca;

    iput-object v0, p0, Lcom/google/android/gms/drive/api/a/b;->b:Lcom/google/android/gms/drive/internal/ca;

    .line 58
    iput-object p3, p0, Lcom/google/android/gms/drive/api/a/b;->c:Lcom/google/android/gms/drive/c/a;

    .line 59
    iput-boolean p4, p0, Lcom/google/android/gms/drive/api/a/b;->d:Z

    .line 60
    iget-boolean v0, p0, Lcom/google/android/gms/drive/api/a/b;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/b;->c:Lcom/google/android/gms/drive/c/a;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Send what after doExecute??"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 62
    return-void

    .line 60
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
.end method

.method public a(Lcom/google/android/gms/common/api/Status;)V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/b;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 114
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 4

    .prologue
    .line 27
    check-cast p1, Lcom/google/android/gms/drive/api/DriveAsyncService;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/b;->a:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/api/d;->f()Lcom/google/android/gms/drive/auth/g;
    :try_end_0
    .catch Lcom/google/android/gms/drive/auth/c; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/drive/ai;->y:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/drive/metadata/sync/c/g;->a()Lcom/google/android/gms/drive/metadata/sync/c/g;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, v2, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, p0}, Lcom/google/android/gms/drive/metadata/sync/c/g;->a(Ljava/lang/String;Lcom/google/android/gms/common/service/b;)V

    :cond_0
    invoke-virtual {p0, p1, v1}, Lcom/google/android/gms/drive/api/a/b;->a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V

    iget-boolean v0, p0, Lcom/google/android/gms/drive/api/a/b;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/b;->c:Lcom/google/android/gms/drive/c/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/b;->c:Lcom/google/android/gms/drive/c/a;

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/a;->d()Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/a;->a()V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0x8

    const-string v2, "Authorization has been revoked by the user. Reconnect the Drive API client to reauthorize."

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0
.end method
