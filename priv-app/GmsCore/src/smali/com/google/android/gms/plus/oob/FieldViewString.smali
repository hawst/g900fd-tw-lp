.class public final Lcom/google/android/gms/plus/oob/FieldViewString;
.super Lcom/google/android/gms/plus/oob/d;
.source "SourceFile"


# instance fields
.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/oob/d;-><init>(Landroid/content/Context;Z)V

    .line 79
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->a:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/l;->en:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/google/android/gms/l;->em:I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;Lcom/google/android/gms/plus/oob/e;)V
    .locals 3

    .prologue
    .line 123
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/oob/d;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;Lcom/google/android/gms/plus/oob/e;)V

    .line 124
    sget v0, Lcom/google/android/gms/p;->uT:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewString;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewString;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->d:Landroid/widget/TextView;

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewString;->j()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    sget v0, Lcom/google/android/gms/p;->uS:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewString;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewString;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->e:Landroid/widget/EditText;

    .line 129
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->e:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->t()Lcom/google/android/gms/plus/service/v1whitelisted/models/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/i;->n()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->e:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewString;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 131
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewString;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->e:Landroid/widget/EditText;

    new-instance v1, Lcom/google/android/gms/plus/oob/r;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/plus/oob/r;-><init>(Lcom/google/android/gms/plus/oob/FieldViewString;B)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 134
    :cond_1
    return-void

    .line 129
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->t()Lcom/google/android/gms/plus/service/v1whitelisted/models/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/i;->m()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewString;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/gms/plus/service/v1whitelisted/models/b;
    .locals 3

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewString;->i()Lcom/google/android/gms/plus/service/v1whitelisted/models/c;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->e:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->b(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/i;)Lcom/google/android/gms/plus/service/v1whitelisted/models/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    move-result-object v0

    return-object v0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 91
    instance-of v0, p1, Lcom/google/android/gms/plus/oob/FieldViewString$SavedState;

    if-nez v0, :cond_0

    .line 92
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/oob/d;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 98
    :goto_0
    return-void

    .line 95
    :cond_0
    check-cast p1, Lcom/google/android/gms/plus/oob/FieldViewString$SavedState;

    .line 96
    invoke-virtual {p1}, Lcom/google/android/gms/plus/oob/FieldViewString$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/google/android/gms/plus/oob/d;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->e:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/google/android/gms/plus/oob/FieldViewString$SavedState;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 83
    invoke-super {p0}, Lcom/google/android/gms/plus/oob/d;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 84
    new-instance v1, Lcom/google/android/gms/plus/oob/FieldViewString$SavedState;

    invoke-direct {v1, v0}, Lcom/google/android/gms/plus/oob/FieldViewString$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/plus/oob/FieldViewString$SavedState;->a:Ljava/lang/String;

    .line 86
    return-object v1
.end method
