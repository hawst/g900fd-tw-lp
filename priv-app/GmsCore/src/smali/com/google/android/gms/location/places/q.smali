.class public final Lcom/google/android/gms/location/places/q;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lcom/google/android/gms/common/api/j;

.field public static final b:Lcom/google/android/gms/common/api/c;

.field public static final c:Lcom/google/android/gms/location/places/u;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 60
    new-instance v0, Lcom/google/android/gms/common/api/j;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/places/q;->a:Lcom/google/android/gms/common/api/j;

    .line 117
    new-instance v0, Lcom/google/android/gms/common/api/c;

    new-instance v1, Lcom/google/android/gms/location/places/r;

    invoke-direct {v1}, Lcom/google/android/gms/location/places/r;-><init>()V

    sget-object v2, Lcom/google/android/gms/location/places/q;->a:Lcom/google/android/gms/common/api/j;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/gms/common/api/Scope;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/c;-><init>(Lcom/google/android/gms/common/api/i;Lcom/google/android/gms/common/api/j;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/location/places/q;->b:Lcom/google/android/gms/common/api/c;

    .line 134
    new-instance v0, Lcom/google/android/gms/location/places/x;

    invoke-direct {v0}, Lcom/google/android/gms/location/places/x;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/places/q;->c:Lcom/google/android/gms/location/places/u;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/common/api/c;
    .locals 4

    .prologue
    .line 130
    new-instance v0, Lcom/google/android/gms/common/api/c;

    new-instance v1, Lcom/google/android/gms/location/places/r;

    invoke-direct {v1, p0}, Lcom/google/android/gms/location/places/r;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/gms/location/places/q;->a:Lcom/google/android/gms/common/api/j;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/gms/common/api/Scope;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/c;-><init>(Lcom/google/android/gms/common/api/i;Lcom/google/android/gms/common/api/j;[Lcom/google/android/gms/common/api/Scope;)V

    return-object v0
.end method
