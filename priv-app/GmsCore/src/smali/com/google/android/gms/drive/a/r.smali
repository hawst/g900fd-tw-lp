.class public final Lcom/google/android/gms/drive/a/r;
.super Lcom/google/android/gms/drive/a/b;
.source "SourceFile"


# instance fields
.field private final d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;)V
    .locals 6

    .prologue
    .line 62
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/drive/a/ac;->a:Lcom/google/android/gms/drive/a/ac;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/a/r;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/lang/Boolean;Lcom/google/android/gms/drive/a/ac;)V

    .line 63
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/lang/Boolean;Lcom/google/android/gms/drive/a/ac;)V
    .locals 6

    .prologue
    .line 53
    sget-object v1, Lcom/google/android/gms/drive/a/e;->m:Lcom/google/android/gms/drive/a/e;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/a/b;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/a/ac;)V

    .line 54
    iput-object p4, p0, Lcom/google/android/gms/drive/a/r;->d:Ljava/lang/Boolean;

    .line 55
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/gms/drive/a/e;->m:Lcom/google/android/gms/drive/a/e;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/drive/a/b;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    .line 42
    const-string v0, "deleted"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/r;->d:Ljava/lang/Boolean;

    .line 43
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;B)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/a/r;-><init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/a/c;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 67
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->D()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/drive/a/r;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v0, v1, :cond_0

    .line 68
    new-instance v0, Lcom/google/android/gms/drive/a/z;

    iget-object v1, p3, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, p3, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    sget-object v3, Lcom/google/android/gms/drive/a/ac;->b:Lcom/google/android/gms/drive/a/ac;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/a/z;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/a/ac;)V

    .line 93
    :goto_0
    return-object v0

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/r;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->L()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 76
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->L()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/a/r;->d(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    iget-wide v4, v0, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->an()Z

    move-result v6

    move-object v1, p1

    move-object v2, p3

    invoke-interface/range {v1 .. v6}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;JZ)Ljava/util/List;

    move-result-object v0

    .line 79
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ah;

    .line 80
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->i()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->i()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    .line 83
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->h()V

    .line 84
    new-instance v0, Lcom/google/android/gms/drive/a/z;

    iget-object v1, p3, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, p3, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    sget-object v3, Lcom/google/android/gms/drive/a/ac;->b:Lcom/google/android/gms/drive/a/ac;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/a/z;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/a/ac;)V

    goto :goto_0

    .line 90
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/a/r;->d:Ljava/lang/Boolean;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/drive/database/model/ah;->a(Ljava/lang/Boolean;)V

    .line 91
    invoke-virtual {p2, v7}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    .line 93
    new-instance v0, Lcom/google/android/gms/drive/a/r;

    iget-object v1, p3, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, p3, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/drive/a/r;->d:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_3

    move v4, v7

    :goto_1
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/drive/a/ac;->b:Lcom/google/android/gms/drive/a/ac;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/a/r;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/lang/Boolean;Lcom/google/android/gms/drive/a/ac;)V

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/drive/g/aw;)V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/drive/a/r;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const-string v1, "Cannot undelete an entry on the server."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 102
    invoke-virtual {p3}, Lcom/google/android/gms/drive/g/aw;->o()Lcom/google/android/gms/drive/d/f;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/drive/d/f;->c(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    .line 104
    invoke-virtual {p3}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    .line 105
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/a/r;->e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->h()V

    .line 107
    invoke-virtual {p3}, Lcom/google/android/gms/drive/g/aw;->v()Lcom/google/android/gms/drive/b/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/b/d;->b()V

    .line 108
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 127
    if-ne p0, p1, :cond_1

    .line 132
    :cond_0
    :goto_0
    return v0

    .line 128
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 130
    :cond_3
    check-cast p1, Lcom/google/android/gms/drive/a/r;

    .line 132
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/a/r;->a(Lcom/google/android/gms/drive/a/a;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/drive/a/r;->d:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/r;->d:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final f()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 112
    invoke-super {p0}, Lcom/google/android/gms/drive/a/b;->f()Lorg/json/JSONObject;

    move-result-object v0

    .line 113
    const-string v1, "deleted"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/r;->d:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 114
    return-object v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/gms/drive/a/b;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/EntrySpec;->hashCode()I

    move-result v0

    .line 121
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/drive/a/r;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 122
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 137
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "DeleteFileAction [%s, mDeleted=%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/r;->h()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/drive/a/r;->d:Ljava/lang/Boolean;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
