.class public Lcom/google/android/gms/plus/audience/g;
.super Lcom/google/android/gms/plus/audience/al;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/audience/f;


# static fields
.field private static final o:Ljava/lang/String;


# instance fields
.field private A:Lcom/google/android/gms/plus/audience/i;

.field protected i:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Ljava/lang/String;

.field private v:Lcom/google/android/gms/plus/audience/a/h;

.field private w:Lcom/google/android/gms/plus/audience/a/a;

.field private x:Lcom/google/android/gms/plus/audience/a/c;

.field private y:Lcom/google/android/gms/plus/audience/a/i;

.field private z:Lcom/google/android/gms/plus/audience/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "account_name = ? AND (data1 LIKE ? OR "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/plus/audience/v;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LIKE ? OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/plus/audience/v;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LIKE ?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/audience/g;->o:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/al;-><init>()V

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/g;->u:Ljava/lang/String;

    .line 335
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;ZZZZZZLjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/audience/g;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Lcom/google/android/gms/plus/audience/g;

    invoke-direct {v0}, Lcom/google/android/gms/plus/audience/g;-><init>()V

    .line 88
    invoke-static/range {p0 .. p9}, Lcom/google/android/gms/plus/audience/g;->b(Ljava/lang/String;Ljava/lang/String;ZZZZZZLjava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/g;->setArguments(Landroid/os/Bundle;)V

    .line 91
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/g;->u:Ljava/lang/String;

    return-object v0
.end method

.method protected static b(Ljava/lang/String;Ljava/lang/String;ZZZZZZLjava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 98
    invoke-static {p0, p1, p8, p9}, Lcom/google/android/gms/plus/audience/al;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 101
    const-string v1, "searchGroups"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 102
    const-string v1, "searchCircles"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 103
    const-string v1, "searchPeople"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 104
    const-string v1, "searchWeb"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 105
    const-string v1, "searchDevice"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 106
    const-string v1, "searchEmail"

    invoke-virtual {v0, v1, p7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 108
    return-object v0
.end method

.method static synthetic g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/gms/plus/audience/g;->o:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final X_()V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/g;->y:Lcom/google/android/gms/plus/audience/a/i;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/a/i;->d()V

    .line 191
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 165
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/g;->u:Ljava/lang/String;

    .line 166
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/g;->o()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/e;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/e;->a(Z)V

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/g;->x:Lcom/google/android/gms/plus/audience/a/c;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/g;->x:Lcom/google/android/gms/plus/audience/a/c;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/g;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/a/c;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/audience/a/e;

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/g;->w:Lcom/google/android/gms/plus/audience/a/a;

    if-eqz v0, :cond_1

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/g;->w:Lcom/google/android/gms/plus/audience/a/a;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/g;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/a/a;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/audience/a/e;

    .line 174
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/g;->v:Lcom/google/android/gms/plus/audience/a/h;

    if-eqz v0, :cond_2

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/g;->v:Lcom/google/android/gms/plus/audience/a/h;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/g;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/a/h;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/audience/a/e;

    .line 177
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/g;->y:Lcom/google/android/gms/plus/audience/a/i;

    if-eqz v0, :cond_3

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/g;->y:Lcom/google/android/gms/plus/audience/a/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/g;->u:Ljava/lang/String;

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/audience/a/i;->a(Ljava/lang/String;I)Lcom/google/android/gms/plus/audience/a/i;

    .line 180
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/g;->A:Lcom/google/android/gms/plus/audience/i;

    if-eqz v0, :cond_4

    .line 181
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/g;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/g;->A:Lcom/google/android/gms/plus/audience/i;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/au;->b(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 183
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/g;->z:Lcom/google/android/gms/plus/audience/a/b;

    if-eqz v0, :cond_5

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/g;->z:Lcom/google/android/gms/plus/audience/a/b;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/g;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/a/b;->a(Ljava/lang/String;)V

    .line 186
    :cond_5
    return-void

    .line 166
    :cond_6
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected final c()Landroid/view/View;
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    return-object v0
.end method

.method protected d()Lcom/google/android/gms/plus/audience/e;
    .locals 6

    .prologue
    .line 113
    new-instance v0, Lcom/google/android/gms/plus/audience/e;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/audience/ae;

    invoke-interface {v2}, Lcom/google/android/gms/plus/audience/ae;->g()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/al;->l:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/al;->m:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/gms/plus/audience/g;->i:Z

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/audience/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/plus/audience/bg;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/e;->a(Lcom/google/android/gms/plus/audience/f;)Lcom/google/android/gms/plus/audience/e;

    move-result-object v0

    return-object v0
.end method

.method protected final e()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 196
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/g;->p:Z

    if-eqz v0, :cond_0

    .line 197
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/g;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/audience/k;

    invoke-direct {v1, p0, v3}, Lcom/google/android/gms/plus/audience/k;-><init>(Lcom/google/android/gms/plus/audience/g;B)V

    invoke-virtual {v0, v3, v4, v1}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/a/c;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/g;->x:Lcom/google/android/gms/plus/audience/a/c;

    .line 201
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/g;->q:Z

    if-eqz v0, :cond_1

    .line 202
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/g;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/gms/plus/audience/h;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/audience/h;-><init>(Lcom/google/android/gms/plus/audience/g;B)V

    invoke-virtual {v0, v1, v4, v2}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/a/a;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/g;->w:Lcom/google/android/gms/plus/audience/a/a;

    .line 206
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/g;->r:Z

    if-eqz v0, :cond_2

    .line 207
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/g;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/gms/plus/audience/l;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/audience/l;-><init>(Lcom/google/android/gms/plus/audience/g;B)V

    invoke-virtual {v0, v1, v4, v2}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/a/h;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/g;->v:Lcom/google/android/gms/plus/audience/a/h;

    .line 211
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/g;->s:Z

    if-eqz v0, :cond_3

    .line 212
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/g;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v1, 0x3

    new-instance v2, Lcom/google/android/gms/plus/audience/m;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/audience/m;-><init>(Lcom/google/android/gms/plus/audience/g;B)V

    invoke-virtual {v0, v1, v4, v2}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/a/i;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/g;->y:Lcom/google/android/gms/plus/audience/a/i;

    .line 217
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/g;->i:Z

    if-eqz v0, :cond_4

    .line 218
    new-instance v0, Lcom/google/android/gms/plus/audience/i;

    invoke-direct {v0, p0, v3}, Lcom/google/android/gms/plus/audience/i;-><init>(Lcom/google/android/gms/plus/audience/g;B)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/g;->A:Lcom/google/android/gms/plus/audience/i;

    .line 219
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/g;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/g;->A:Lcom/google/android/gms/plus/audience/i;

    invoke-virtual {v0, v1, v4, v2}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 222
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/g;->t:Z

    if-eqz v0, :cond_5

    .line 223
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/g;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v1, 0x4

    new-instance v2, Lcom/google/android/gms/plus/audience/j;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/audience/j;-><init>(Lcom/google/android/gms/plus/audience/g;B)V

    invoke-virtual {v0, v1, v4, v2}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/a/b;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/g;->z:Lcom/google/android/gms/plus/audience/a/b;

    .line 226
    :cond_5
    return-void
.end method

.method protected synthetic f()Landroid/widget/BaseAdapter;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/g;->d()Lcom/google/android/gms/plus/audience/e;

    move-result-object v0

    return-object v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 132
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/al;->onAttach(Landroid/app/Activity;)V

    .line 133
    instance-of v0, p1, Lcom/google/android/gms/plus/audience/ae;

    if-nez v0, :cond_0

    .line 134
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity must implement AudienceSelectionFragmentHost"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 141
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/al;->onCreate(Landroid/os/Bundle;)V

    .line 142
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/g;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 143
    const-string v1, "searchGroups"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/plus/audience/g;->p:Z

    .line 144
    const-string v1, "searchCircles"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/plus/audience/g;->q:Z

    .line 145
    const-string v1, "searchPeople"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/plus/audience/g;->r:Z

    .line 146
    const-string v1, "searchWeb"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/plus/audience/g;->s:Z

    .line 147
    const-string v1, "searchDevice"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/plus/audience/g;->i:Z

    .line 148
    const-string v1, "searchEmail"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/g;->t:Z

    .line 149
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 120
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/al;->onStart()V

    .line 121
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/g;->o()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/e;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/e;->j()V

    .line 122
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/g;->o()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/e;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/e;->k()V

    .line 127
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/al;->onStop()V

    .line 128
    return-void
.end method
