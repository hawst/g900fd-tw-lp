.class public final Lcom/google/android/gms/people/service/a/b/j;
.super Lcom/google/android/gms/people/service/a/b/c;
.source "SourceFile"


# instance fields
.field private final i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:Ljava/util/List;

.field private final m:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 9

    .prologue
    .line 68
    move-object v1, p0

    move-object v2, p1

    move-object v3, p6

    move-object v4, p2

    move v5, p4

    move-object v6, p5

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/people/service/a/b/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/b/j;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    .line 71
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/b/j;->j:Ljava/lang/String;

    .line 72
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    :goto_0
    iput-object p2, p0, Lcom/google/android/gms/people/service/a/b/j;->k:Ljava/lang/String;

    .line 73
    if-nez p10, :cond_0

    new-instance p10, Ljava/util/ArrayList;

    invoke-direct/range {p10 .. p10}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/b/j;->l:Ljava/util/List;

    .line 74
    if-nez p11, :cond_1

    new-instance p11, Ljava/util/ArrayList;

    invoke-direct/range {p11 .. p11}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/b/j;->m:Ljava/util/List;

    .line 75
    return-void

    :cond_2
    move-object p2, p3

    .line 72
    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/people/service/e;Landroid/content/Context;Lcom/google/android/gms/people/e/a;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Landroid/util/Pair;
    .locals 17

    .prologue
    .line 388
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    const-string v2, "qualifiedId"

    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/at;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    if-nez p4, :cond_0

    .line 393
    new-instance p4, Ljava/util/ArrayList;

    invoke-direct/range {p4 .. p4}, Ljava/util/ArrayList;-><init>()V

    .line 395
    :cond_0
    if-nez p5, :cond_1

    .line 396
    new-instance p5, Ljava/util/ArrayList;

    invoke-direct/range {p5 .. p5}, Ljava/util/ArrayList;-><init>()V

    .line 399
    :cond_1
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v6

    .line 400
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v7

    .line 402
    new-instance v8, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v8, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 403
    new-instance v9, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v9, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 404
    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/people/internal/at;->i(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 405
    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/people/internal/at;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 411
    :goto_0
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 414
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 416
    const/4 v4, 0x0

    .line 417
    const/4 v3, 0x0

    .line 419
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/u;->b()V

    .line 421
    const/4 v2, 0x0

    move v5, v2

    :goto_1
    if-ge v5, v6, :cond_4

    .line 423
    :try_start_0
    move-object/from16 v0, p4

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v2, v8, v9}, Lcom/google/android/gms/people/service/e;->a(Lcom/google/android/gms/people/e/a;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;

    move-result-object v2

    .line 428
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/gms/people/sync/u;->b()V

    .line 430
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;->e()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 431
    const/4 v12, 0x3

    invoke-static {v12}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 432
    const-string v12, "PeopleService"

    const-string v13, "%s added to %s"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object p3, v14, v15

    const/4 v15, 0x1

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;->e()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v12, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1

    .line 421
    :cond_2
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_1

    .line 407
    :cond_3
    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/people/internal/at;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 435
    :catch_0
    move-exception v2

    move-object v4, v2

    .line 445
    :cond_4
    :goto_2
    if-nez v4, :cond_a

    if-nez v3, :cond_a

    .line 446
    const/4 v2, 0x0

    move v5, v2

    :goto_3
    if-ge v5, v7, :cond_a

    .line 448
    :try_start_1
    move-object/from16 v0, p5

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 449
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v2, v8, v9}, Lcom/google/android/gms/people/service/e;->b(Lcom/google/android/gms/people/e/a;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    .line 454
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/people/sync/u;->b()V

    .line 456
    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 457
    const/4 v6, 0x3

    invoke-static {v6}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 458
    const-string v6, "PeopleService"

    const-string v12, "%s removed from %s"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object p3, v13, v14

    const/4 v14, 0x1

    aput-object v2, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v6, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_3

    .line 446
    :cond_5
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_3

    .line 438
    :catch_1
    move-exception v2

    move-object v3, v2

    .line 440
    goto :goto_2

    .line 461
    :catch_2
    move-exception v2

    move-object/from16 v16, v3

    move-object v3, v2

    move-object/from16 v2, v16

    .line 472
    :goto_4
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/2addr v4, v5

    if-nez v4, :cond_9

    .line 473
    if-eqz v3, :cond_7

    .line 474
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 475
    const-string v2, "PeopleService"

    const-string v4, "Error during a server call."

    invoke-static {v2, v4, v3}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 477
    :cond_6
    throw v3

    .line 464
    :catch_3
    move-exception v2

    move-object v3, v4

    .line 466
    goto :goto_4

    .line 479
    :cond_7
    if-eqz v2, :cond_9

    .line 480
    const/4 v3, 0x3

    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 481
    const-string v3, "PeopleService"

    const-string v4, "Error during a server call."

    invoke-static {v3, v4, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 483
    :cond_8
    throw v2

    .line 487
    :cond_9
    invoke-static {v10, v11}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    return-object v2

    :cond_a
    move-object v2, v3

    move-object v3, v4

    goto :goto_4
.end method

.method private static a(Ljava/util/ArrayList;Ljava/util/ArrayList;)Landroid/util/Pair;
    .locals 3

    .prologue
    .line 186
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 187
    const-string v1, "added_circles"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 188
    const-string v1, "removed_circles"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 189
    new-instance v1, Landroid/util/Pair;

    sget-object v2, Lcom/google/android/gms/people/service/b;->c:Lcom/google/android/gms/people/service/b;

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 342
    if-nez p5, :cond_0

    .line 343
    sget-object p5, Lcom/google/android/gms/plus/a/b;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    .line 346
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 347
    new-instance v0, Lcom/google/android/gms/common/server/y;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/server/y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/server/y;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/plus/a/m;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/y;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-virtual {v0, p5}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-virtual {v0, p6}, Lcom/google/android/gms/common/server/y;->c(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-static {p3}, Lcom/google/android/gms/common/analytics/e;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/server/y;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/y;)V

    .line 358
    :cond_1
    if-eqz p4, :cond_2

    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 359
    new-instance v0, Lcom/google/android/gms/common/server/y;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/server/y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/server/y;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/plus/a/m;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/y;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-virtual {v0, p5}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-virtual {v0, p6}, Lcom/google/android/gms/common/server/y;->c(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-static {p4}, Lcom/google/android/gms/common/analytics/e;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/server/y;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/y;)V

    .line 369
    :cond_2
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/people/e/a;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z
    .locals 3

    .prologue
    .line 534
    new-instance v0, Lcom/google/android/gms/people/sync/ap;

    iget-object v1, p1, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/gms/people/e/a;->b:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/people/sync/ap;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    :try_start_0
    invoke-virtual {v0, p2}, Lcom/google/android/gms/people/sync/ap;->c(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    .line 551
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 544
    :catch_0
    move-exception v0

    .line 547
    const-string v1, "PeopleService"

    const-string v2, "FK error trying to create a person."

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 548
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/content/Context;Lcom/google/android/gms/people/service/e;Lcom/google/android/gms/people/e/a;)Landroid/util/Pair;
    .locals 15

    .prologue
    .line 93
    :try_start_0
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/google/android/gms/plus/service/v2whitelisted/models/s;

    invoke-direct {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/s;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/j;->j:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/people/internal/at;->i(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/j;->j:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/people/internal/at;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/plus/service/v2whitelisted/models/s;->a:Ljava/lang/String;

    iget-object v4, v2, Lcom/google/android/gms/plus/service/v2whitelisted/models/s;->d:Ljava/util/Set;

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/b/j;->l:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/people/service/a/b/j;->g:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/people/service/a/b/j;->h:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/people/service/a/b/j;->j:Ljava/lang/String;

    invoke-virtual {v4, v5, v6, v7, v3}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/plus/service/v2whitelisted/models/s;->b:Ljava/lang/String;

    iget-object v3, v2, Lcom/google/android/gms/plus/service/v2whitelisted/models/s;->d:Ljava/util/Set;

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    new-instance v3, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity$CircleMemberIdEntity;

    iget-object v4, v2, Lcom/google/android/gms/plus/service/v2whitelisted/models/s;->d:Ljava/util/Set;

    iget-object v5, v2, Lcom/google/android/gms/plus/service/v2whitelisted/models/s;->a:Ljava/lang/String;

    iget-object v6, v2, Lcom/google/android/gms/plus/service/v2whitelisted/models/s;->b:Ljava/lang/String;

    iget-object v2, v2, Lcom/google/android/gms/plus/service/v2whitelisted/models/s;->c:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v6, v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity$CircleMemberIdEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/j;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    new-instance v6, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;

    invoke-direct {v6}, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;-><init>()V

    invoke-virtual {v6, v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v2whitelisted/models/q;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/r;)Lcom/google/android/gms/plus/service/v2whitelisted/models/q;

    move-result-object v2

    const-string v6, "add"

    invoke-virtual {v2, v6}, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;->b(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v2whitelisted/models/q;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;->a()Lcom/google/android/gms/plus/service/v2whitelisted/models/p;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 94
    :catch_0
    move-exception v2

    .line 96
    iget-object v3, v2, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v3, :cond_6

    iget-object v3, v2, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v3, v3, Lcom/android/volley/m;->a:I

    const/16 v4, 0x193

    if-ne v3, v4, :cond_6

    .line 97
    new-instance v2, Landroid/util/Pair;

    sget-object v3, Lcom/google/android/gms/people/service/b;->j:Lcom/google/android/gms/people/service/b;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 181
    :goto_2
    return-object v2

    .line 93
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/j;->j:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/people/internal/at;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/plus/service/v2whitelisted/models/s;->c:Ljava/lang/String;

    iget-object v3, v2, Lcom/google/android/gms/plus/service/v2whitelisted/models/s;->d:Ljava/util/Set;

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/j;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    new-instance v6, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;

    invoke-direct {v6}, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;-><init>()V

    invoke-virtual {v6, v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v2whitelisted/models/q;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/r;)Lcom/google/android/gms/plus/service/v2whitelisted/models/q;

    move-result-object v2

    const-string v6, "remove"

    invoke-virtual {v2, v6}, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;->b(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v2whitelisted/models/q;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;->a()Lcom/google/android/gms/plus/service/v2whitelisted/models/p;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_3
    new-instance v3, Lcom/google/android/gms/plus/service/v2whitelisted/models/o;

    invoke-direct {v3}, Lcom/google/android/gms/plus/service/v2whitelisted/models/o;-><init>()V

    iput-object v4, v3, Lcom/google/android/gms/plus/service/v2whitelisted/models/o;->a:Ljava/util/List;

    iget-object v2, v3, Lcom/google/android/gms/plus/service/v2whitelisted/models/o;->b:Ljava/util/Set;

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity;

    iget-object v4, v3, Lcom/google/android/gms/plus/service/v2whitelisted/models/o;->b:Ljava/util/Set;

    iget-object v3, v3, Lcom/google/android/gms/plus/service/v2whitelisted/models/o;->a:Ljava/util/List;

    invoke-direct {v2, v4, v3}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity;-><init>(Ljava/util/Set;Ljava/util/List;)V

    check-cast v2, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity;

    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/people/sync/u;->b()V

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/service/e;->a(Lcom/google/android/gms/people/e/a;Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity;)Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse;

    move-result-object v2

    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/people/sync/u;->b()V

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse;->c()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->c()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse$Items;->b()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    move-object v13, v2

    .line 103
    :goto_4
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 104
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 105
    if-nez v13, :cond_7

    .line 107
    invoke-static {v5, v6}, Lcom/google/android/gms/people/service/a/b/j;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)Landroid/util/Pair;

    move-result-object v2

    goto/16 :goto_2

    .line 93
    :cond_5
    const/4 v2, 0x0

    move-object v13, v2

    goto :goto_4

    .line 99
    :cond_6
    throw v2

    .line 109
    :cond_7
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 110
    invoke-virtual {v13}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->o()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v13}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->e()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 111
    invoke-virtual {v13}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->d()Ljava/util/List;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 113
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/j;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_9
    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 114
    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 115
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 118
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/j;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_b
    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 119
    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_b

    .line 120
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 125
    :cond_c
    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/j;->k:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/b/j;->g:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/people/service/a/b/j;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v8, p0, Lcom/google/android/gms/people/service/a/b/j;->h:Ljava/lang/String;

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v8}, Lcom/google/android/gms/people/service/a/b/j;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 135
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 136
    invoke-virtual {v13}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->o()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-virtual {v13}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->e()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 137
    invoke-virtual {v13}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->d()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 139
    :cond_d
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/b/j;->g:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/people/service/a/b/j;->h:Ljava/lang/String;

    sget-object v8, Lcom/google/android/gms/people/internal/at;->d:[Ljava/lang/String;

    invoke-interface {v2, v8}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    invoke-virtual {v3, v4, v7, v2}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 141
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/j;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/j;->h:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/people/sync/z;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :cond_e
    const/4 v3, 0x0

    .line 146
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/gms/people/service/a/b/j;->g:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/gms/people/service/a/b/j;->h:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/gms/people/service/a/b/j;->j:Ljava/lang/String;

    move-object v11, v5

    move-object v12, v6

    invoke-virtual/range {v7 .. v12}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)I

    move-result v2

    .line 148
    const/4 v4, 0x2

    if-ne v2, v4, :cond_15

    .line 149
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1, v13}, Lcom/google/android/gms/people/service/a/b/j;->a(Landroid/content/Context;Lcom/google/android/gms/people/e/a;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 150
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/gms/people/service/a/b/j;->g:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/gms/people/service/a/b/j;->h:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/gms/people/service/a/b/j;->j:Ljava/lang/String;

    move-object v11, v5

    move-object v12, v6

    invoke-virtual/range {v7 .. v12}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)I

    move-result v3

    .line 152
    const/4 v2, 0x1

    if-eq v3, v2, :cond_13

    const/4 v2, 0x1

    :goto_7
    move v14, v3

    move v3, v2

    move v2, v14

    .line 164
    :cond_f
    :goto_8
    iget-object v4, p0, Lcom/google/android/gms/people/service/a/b/j;->h:Ljava/lang/String;

    if-nez v4, :cond_10

    .line 165
    iget-object v4, p0, Lcom/google/android/gms/people/service/a/b/j;->a:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v4

    iget-object v7, p0, Lcom/google/android/gms/people/service/a/b/j;->g:Ljava/lang/String;

    const-string v8, "UpdatePersonCirclesV2"

    invoke-virtual {v4, v7, v8}, Lcom/google/android/gms/people/sync/u;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_10
    const/4 v4, 0x1

    if-ne v2, v4, :cond_11

    .line 171
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.google.android.gms.people.BROADCAST_CIRCLES_CHANGED"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "com.google.android.gms.permission.INTERNAL_BROADCAST"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 175
    :cond_11
    if-eqz v3, :cond_12

    .line 177
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/j;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/j;->g:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/b/j;->h:Ljava/lang/String;

    const-string v7, "UpdatePersonCirclesV2"

    invoke-virtual {v2, v3, v4, v7}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 181
    :cond_12
    invoke-static {v5, v6}, Lcom/google/android/gms/people/service/a/b/j;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)Landroid/util/Pair;

    move-result-object v2

    goto/16 :goto_2

    .line 152
    :cond_13
    const/4 v2, 0x0

    goto :goto_7

    .line 155
    :cond_14
    const/4 v3, 0x1

    goto :goto_8

    .line 157
    :cond_15
    const/4 v4, 0x3

    if-ne v2, v4, :cond_f

    .line 160
    const/4 v3, 0x1

    goto :goto_8
.end method

.method private c(Landroid/content/Context;Lcom/google/android/gms/people/service/e;Lcom/google/android/gms/people/e/a;)Landroid/util/Pair;
    .locals 11

    .prologue
    .line 259
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 262
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/j;->j:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/b/j;->l:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/gms/people/service/a/b/j;->m:Ljava/util/List;

    move-object v0, p2

    move-object v1, p1

    move-object v2, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/people/service/a/b/j;->a(Lcom/google/android/gms/people/service/e;Landroid/content/Context;Lcom/google/android/gms/people/e/a;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Landroid/util/Pair;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 273
    iget-object v1, p0, Lcom/google/android/gms/people/service/a/b/j;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/j;->g:Ljava/lang/String;

    iget-object v3, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/util/ArrayList;

    iget-object v4, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/gms/people/service/a/b/j;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v6, p0, Lcom/google/android/gms/people/service/a/b/j;->h:Ljava/lang/String;

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/people/service/a/b/j;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 282
    const/4 v6, 0x0

    .line 283
    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/b/j;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/j;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/j;->j:Ljava/lang/String;

    iget-object v4, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/util/List;

    iget-object v5, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/util/List;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)I

    move-result v7

    .line 289
    packed-switch v7, :pswitch_data_0

    move v0, v7

    move v1, v6

    .line 316
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/j;->h:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 318
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/j;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/j;->g:Ljava/lang/String;

    const-string v4, "UpdatePersonCircles"

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/people/sync/u;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    :cond_0
    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 323
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.people.BROADCAST_CIRCLES_CHANGED"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.google.android.gms.permission.INTERNAL_BROADCAST"

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 326
    :cond_1
    if-eqz v1, :cond_2

    .line 328
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/j;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/b/j;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/j;->h:Ljava/lang/String;

    const-string v3, "UpdatePersonCircles"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 332
    :cond_2
    const-string v1, "added_circles"

    iget-object v0, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v8, v1, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 333
    const-string v1, "removed_circles"

    iget-object v0, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v8, v1, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 335
    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lcom/google/android/gms/people/service/b;->c:Lcom/google/android/gms/people/service/b;

    invoke-direct {v0, v1, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_1
    return-object v0

    .line 264
    :catch_0
    move-exception v0

    .line 266
    iget-object v1, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v1, v1, Lcom/android/volley/m;->a:I

    const/16 v2, 0x193

    if-ne v1, v2, :cond_3

    .line 268
    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lcom/google/android/gms/people/service/b;->j:Lcom/google/android/gms/people/service/b;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 270
    :cond_3
    throw v0

    .line 291
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/j;->j:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "PeopleService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trying to download a person for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->k(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->j(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "PeopleService"

    const-string v2, "Type of qualified ID not supported."

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    .line 294
    :goto_2
    if-eqz v0, :cond_7

    .line 297
    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/b/j;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/j;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/j;->j:Ljava/lang/String;

    iget-object v4, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/util/List;

    iget-object v5, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/util/List;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)I

    move-result v1

    .line 301
    const/4 v0, 0x1

    if-eq v1, v0, :cond_6

    const/4 v0, 0x1

    :goto_3
    move v10, v1

    move v1, v0

    move v0, v10

    goto/16 :goto_0

    .line 291
    :cond_5
    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/gms/people/service/e;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/service/e;->b()Lcom/google/android/gms/people/service/g;

    move-result-object v1

    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/b;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/service/v2whitelisted/b;-><init>(Lcom/google/android/gms/common/server/n;)V

    iget-object v1, p3, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p3, Lcom/google/android/gms/people/e/a;->b:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/c;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    move-result-object v0

    invoke-static {p1, p3, v0}, Lcom/google/android/gms/people/service/a/b/j;->a(Landroid/content/Context;Lcom/google/android/gms/people/e/a;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v0

    goto :goto_2

    .line 301
    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    .line 304
    :cond_7
    const/4 v0, 0x1

    move v1, v0

    move v0, v7

    .line 306
    goto/16 :goto_0

    .line 311
    :pswitch_1
    const/4 v0, 0x1

    move v1, v0

    move v0, v7

    goto/16 :goto_0

    .line 289
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/people/service/e;Lcom/google/android/gms/people/e/a;)Landroid/util/Pair;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/google/android/gms/people/a/a;->aJ:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/people/service/a/b/j;->b(Landroid/content/Context;Lcom/google/android/gms/people/service/e;Lcom/google/android/gms/people/e/a;)Landroid/util/Pair;

    move-result-object v0

    .line 84
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/people/service/a/b/j;->c(Landroid/content/Context;Lcom/google/android/gms/people/service/e;Lcom/google/android/gms/people/e/a;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0
.end method
