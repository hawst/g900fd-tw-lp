.class public final Lcom/google/android/gms/ads/internal/n;
.super Lcom/google/android/gms/ads/internal/client/j;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/ads/internal/client/f;

.field private final c:Lcom/google/android/gms/ads/internal/k/a/a;

.field private final d:Lcom/google/android/gms/ads/internal/f/a/g;

.field private final e:Lcom/google/android/gms/ads/internal/f/a/j;

.field private final f:Ljava/util/List;

.field private final g:Ljava/lang/String;

.field private final h:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/ads/internal/k/a/a;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Lcom/google/android/gms/ads/internal/client/f;Lcom/google/android/gms/ads/internal/f/a/g;Lcom/google/android/gms/ads/internal/f/a/j;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/client/j;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/n;->a:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/n;->g:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/n;->c:Lcom/google/android/gms/ads/internal/k/a/a;

    .line 51
    iput-object p4, p0, Lcom/google/android/gms/ads/internal/n;->h:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    .line 52
    iput-object p5, p0, Lcom/google/android/gms/ads/internal/n;->b:Lcom/google/android/gms/ads/internal/client/f;

    .line 53
    iput-object p6, p0, Lcom/google/android/gms/ads/internal/n;->d:Lcom/google/android/gms/ads/internal/f/a/g;

    .line 54
    iput-object p7, p0, Lcom/google/android/gms/ads/internal/n;->e:Lcom/google/android/gms/ads/internal/f/a/j;

    .line 56
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/n;->b()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/n;->f:Ljava/util/List;

    .line 57
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/ads/internal/n;)Lcom/google/android/gms/ads/internal/f/a/g;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/n;->d:Lcom/google/android/gms/ads/internal/f/a/g;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/ads/internal/n;)Lcom/google/android/gms/ads/internal/f/a/j;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/n;->e:Lcom/google/android/gms/ads/internal/f/a/j;

    return-object v0
.end method

.method private b()Ljava/util/List;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 78
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/n;->e:Lcom/google/android/gms/ads/internal/f/a/j;

    if-eqz v1, :cond_0

    .line 79
    const-string v1, "1"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/n;->d:Lcom/google/android/gms/ads/internal/f/a/g;

    if-eqz v1, :cond_1

    .line 82
    const-string v1, "2"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    :cond_1
    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/ads/internal/n;)Lcom/google/android/gms/ads/internal/client/f;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/n;->b:Lcom/google/android/gms/ads/internal/client/f;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/ads/internal/n;)Ljava/util/List;
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/n;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a()Lcom/google/android/gms/ads/internal/b;
    .locals 6

    .prologue
    .line 88
    new-instance v0, Lcom/google/android/gms/ads/internal/b;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/n;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/n;->a:Landroid/content/Context;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/gms/ads/b;

    invoke-static {v2, v3}, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->a(Landroid/content/Context;[Lcom/google/android/gms/ads/b;)Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/n;->g:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/n;->c:Lcom/google/android/gms/ads/internal/k/a/a;

    iget-object v5, p0, Lcom/google/android/gms/ads/internal/n;->h:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/b;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Ljava/lang/String;Lcom/google/android/gms/ads/internal/k/a/a;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)V
    .locals 2

    .prologue
    .line 61
    new-instance v0, Lcom/google/android/gms/ads/internal/o;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/ads/internal/o;-><init>(Lcom/google/android/gms/ads/internal/n;Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)V

    sget-object v1, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 74
    return-void
.end method
