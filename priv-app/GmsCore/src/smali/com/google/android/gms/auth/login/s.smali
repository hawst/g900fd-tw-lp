.class final Lcom/google/android/gms/auth/login/s;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/login/BrowserActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/login/BrowserActivity;)V
    .locals 0

    .prologue
    .line 321
    iput-object p1, p0, Lcom/google/android/gms/auth/login/s;->a:Lcom/google/android/gms/auth/login/BrowserActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/gms/auth/login/s;->a:Lcom/google/android/gms/auth/login/BrowserActivity;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/s;->a:Lcom/google/android/gms/auth/login/BrowserActivity;

    invoke-static {v1}, Lcom/google/android/gms/auth/login/BrowserActivity;->a(Lcom/google/android/gms/auth/login/BrowserActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/login/s;->a:Lcom/google/android/gms/auth/login/BrowserActivity;

    invoke-static {v2}, Lcom/google/android/gms/auth/login/BrowserActivity;->b(Lcom/google/android/gms/auth/login/BrowserActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/auth/login/s;->a:Lcom/google/android/gms/auth/login/BrowserActivity;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/auth/login/BrowserActivity;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 321
    check-cast p1, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/s;->a:Lcom/google/android/gms/auth/login/BrowserActivity;

    invoke-static {v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->c(Lcom/google/android/gms/auth/login/BrowserActivity;)Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/s;->a:Lcom/google/android/gms/auth/login/BrowserActivity;

    invoke-static {v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->d(Lcom/google/android/gms/auth/login/BrowserActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/s;->a:Lcom/google/android/gms/auth/login/BrowserActivity;

    invoke-static {v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->e(Lcom/google/android/gms/auth/login/BrowserActivity;)Lcom/google/android/gms/auth/login/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/login/f;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/s;->a:Lcom/google/android/gms/auth/login/BrowserActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->i()V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/s;->a:Lcom/google/android/gms/auth/login/BrowserActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/s;->a:Lcom/google/android/gms/auth/login/BrowserActivity;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/BrowserActivity;->m:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    const-string v1, "GLSActivity"

    const-string v2, "loadUrl from onCreate()."

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/s;->a:Lcom/google/android/gms/auth/login/BrowserActivity;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/s;->a:Lcom/google/android/gms/auth/login/BrowserActivity;

    invoke-static {v1}, Lcom/google/android/gms/auth/login/BrowserActivity;->f(Lcom/google/android/gms/auth/login/BrowserActivity;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/auth/login/CustomWebView;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/s;->a:Lcom/google/android/gms/auth/login/BrowserActivity;

    invoke-static {v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->d(Lcom/google/android/gms/auth/login/BrowserActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/s;->a:Lcom/google/android/gms/auth/login/BrowserActivity;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/s;->a:Lcom/google/android/gms/auth/login/BrowserActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/BrowserActivity;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/login/BrowserActivity;->setTitle(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/s;->a:Lcom/google/android/gms/auth/login/BrowserActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/auth/login/BrowserActivity;->a(Lcom/google/android/gms/auth/login/BrowserActivity;Z)V

    goto :goto_0
.end method
