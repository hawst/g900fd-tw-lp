.class public final Lcom/google/android/gms/plus/service/v1whitelisted/models/w;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:F

.field public c:I

.field public d:F

.field public e:I

.field public f:F

.field public g:I

.field public h:I

.field public final i:Ljava/util/Set;

.field private j:Ljava/util/List;

.field private k:Ljava/util/List;

.field private l:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->i:Ljava/util/Set;

    .line 163
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/plus/service/v1whitelisted/models/v;
    .locals 13

    .prologue
    .line 293
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclDetailsEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->i:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->j:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->k:Ljava/util/List;

    iget v4, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->l:I

    iget v5, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->a:I

    iget v6, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->b:F

    iget v7, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->c:I

    iget v8, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->d:F

    iget v9, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->e:I

    iget v10, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->f:F

    iget v11, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->g:I

    iget v12, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->h:I

    invoke-direct/range {v0 .. v12}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclDetailsEntity;-><init>(Ljava/util/Set;Ljava/util/List;Ljava/util/List;IIFIFIFII)V

    return-object v0
.end method

.method public final a(I)Lcom/google/android/gms/plus/service/v1whitelisted/models/w;
    .locals 2

    .prologue
    .line 193
    iput p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->l:I

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->i:Ljava/util/Set;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 195
    return-object p0
.end method
