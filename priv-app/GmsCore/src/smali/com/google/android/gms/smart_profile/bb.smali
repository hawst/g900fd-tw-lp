.class final Lcom/google/android/gms/smart_profile/bb;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)V
    .locals 0

    .prologue
    .line 899
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/bb;->a:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;B)V
    .locals 0

    .prologue
    .line 899
    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/bb;-><init>(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)V

    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 902
    const/4 v0, 0x1

    return v0
.end method

.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 907
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bb;->a:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->d(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)Landroid/widget/Scroller;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 910
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bb;->a:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->e(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 923
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bb;->a:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->i(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)F

    move-result v0

    float-to-int v0, v0

    .line 927
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/bb;->a:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    int-to-float v0, v0

    invoke-static {v1, v0, p4}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;FF)V

    .line 928
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bb;->a:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    invoke-static {v0}, Landroid/support/v4/view/ay;->d(Landroid/view/View;)V

    .line 929
    return v2

    .line 912
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bb;->a:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bb;->a:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->f(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a(Landroid/view/View;)I

    move-result v0

    goto :goto_0

    .line 915
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bb;->a:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->g(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    .line 918
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bb;->a:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->h(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    .line 919
    neg-float p4, p4

    .line 920
    goto :goto_0

    .line 910
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
