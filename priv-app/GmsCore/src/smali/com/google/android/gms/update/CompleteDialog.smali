.class public Lcom/google/android/gms/update/CompleteDialog;
.super Landroid/app/Activity;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 49
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/gms/update/CompleteDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 51
    if-nez v1, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/google/android/gms/update/CompleteDialog;->finish()V

    .line 75
    :goto_0
    return-void

    .line 56
    :cond_0
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    new-instance v0, Landroid/app/AlertDialog$Builder;

    const/4 v2, 0x5

    invoke-direct {v0, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    sget v2, Lcom/google/android/gms/p;->xS:I

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->xT:I

    new-instance v2, Lcom/google/android/gms/update/b;

    invoke-direct {v2, p0}, Lcom/google/android/gms/update/b;-><init>(Lcom/google/android/gms/update/CompleteDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 59
    :cond_1
    invoke-virtual {p0, v3}, Lcom/google/android/gms/update/CompleteDialog;->requestWindowFeature(I)Z

    .line 60
    sget v0, Lcom/google/android/gms/l;->fu:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/CompleteDialog;->setContentView(I)V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/gms/update/CompleteDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    sget v2, Lcom/google/android/gms/h;->bi:I

    invoke-virtual {v0, v3, v2}, Landroid/view/Window;->setFeatureDrawableResource(II)V

    .line 64
    sget v0, Lcom/google/android/gms/j;->mb:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/CompleteDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 65
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 66
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    sget v0, Lcom/google/android/gms/j;->mN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/CompleteDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/update/a;

    invoke-direct {v1, p0}, Lcom/google/android/gms/update/a;-><init>(Lcom/google/android/gms/update/CompleteDialog;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
