.class public final Lcom/google/android/gms/drive/b/b/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/b/b/o;


# static fields
.field static final a:Ljava/lang/String;

.field static final b:Ljava/lang/String;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/drive/b/b/n;

.field private final e:Lcom/google/android/gms/drive/g/au;

.field private final f:Lcom/google/android/gms/drive/b/b/e;

.field private final g:Lcom/google/android/gms/drive/b/b;

.field private final h:Lcom/google/android/gms/drive/b/b/p;

.field private i:J

.field private j:Ljava/io/BufferedInputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 54
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lcom/google/android/gms/drive/ai;->T:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/upload/drive/v2beta/files?uploadType=resumable&setModifiedDate=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/b/b/h;->a:Ljava/lang/String;

    .line 59
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lcom/google/android/gms/drive/ai;->T:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/upload/drive/v2beta/files/%s?uploadType=resumable&setModifiedDate=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/b/b/h;->b:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/b/b/n;Lcom/google/android/gms/drive/b/b/e;Lcom/google/android/gms/drive/b/b;Lcom/google/android/gms/drive/b/b/p;)V
    .locals 2

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/drive/b/b/h;->i:J

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/h;->j:Ljava/io/BufferedInputStream;

    .line 100
    iput-object p1, p0, Lcom/google/android/gms/drive/b/b/h;->c:Landroid/content/Context;

    .line 101
    iput-object p2, p0, Lcom/google/android/gms/drive/b/b/h;->d:Lcom/google/android/gms/drive/b/b/n;

    .line 102
    iput-object p3, p0, Lcom/google/android/gms/drive/b/b/h;->f:Lcom/google/android/gms/drive/b/b/e;

    .line 103
    iput-object p4, p0, Lcom/google/android/gms/drive/b/b/h;->g:Lcom/google/android/gms/drive/b/b;

    .line 104
    new-instance v0, Lcom/google/android/gms/drive/b/b/m;

    invoke-direct {v0}, Lcom/google/android/gms/drive/b/b/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/h;->e:Lcom/google/android/gms/drive/g/au;

    .line 105
    iput-object p5, p0, Lcom/google/android/gms/drive/b/b/h;->h:Lcom/google/android/gms/drive/b/b/p;

    .line 106
    return-void
.end method

.method private static a(Ljava/lang/String;)Lcom/google/android/gms/drive/b/b/b;
    .locals 3

    .prologue
    .line 476
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/drive/b/b/b;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/b/b/b;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 478
    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/gms/drive/b/b/k;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to upload file: invalid byte range returned by server. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/b/b/k;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/drive/b/b/n;Lcom/google/android/gms/drive/b/b/e;Lcom/google/android/gms/drive/b/b;Lcom/google/android/gms/drive/b/b/p;)Lcom/google/android/gms/drive/b/b/h;
    .locals 6

    .prologue
    .line 93
    new-instance v0, Lcom/google/android/gms/drive/b/b/h;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/b/b/h;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/b/b/n;Lcom/google/android/gms/drive/b/b/e;Lcom/google/android/gms/drive/b/b;Lcom/google/android/gms/drive/b/b/p;)V

    return-object v0
.end method

.method private a(Ljava/net/HttpURLConnection;)Lcom/google/android/gms/drive/d/e;
    .locals 3

    .prologue
    .line 407
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    .line 408
    const/16 v1, 0xc9

    if-eq v0, v1, :cond_0

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 409
    const/4 v0, 0x0

    .line 414
    :goto_0
    return-object v0

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/h;->h:Lcom/google/android/gms/drive/b/b/p;

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/b/b/p;->a(Ljava/io/InputStream;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v1

    .line 412
    const-string v0, "X-Server-Object-Version"

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 414
    new-instance v0, Lcom/google/android/gms/drive/d/e;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/d/e;-><init>(Lcom/google/android/gms/drive/internal/model/File;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/drive/b/b/n;)Ljava/net/HttpURLConnection;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 197
    :try_start_0
    iget-boolean v0, p1, Lcom/google/android/gms/drive/b/b/n;->c:Z

    if-eqz v0, :cond_1

    .line 198
    sget-object v0, Lcom/google/android/gms/drive/b/b/h;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/drive/b/b/p;->a(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v0

    .line 199
    iget-object v1, p0, Lcom/google/android/gms/drive/b/b/h;->f:Lcom/google/android/gms/drive/b/b/e;

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 200
    const-string v1, "POST"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    move-object v3, v0

    .line 208
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/h;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v0, v0, Lcom/google/android/gms/drive/b/b/n;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v1, "If-Match"

    invoke-virtual {v3, v1, v0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :cond_0
    const-string v0, "Content-Type"

    const-string v1, "application/json; charset=UTF-8"

    invoke-virtual {v3, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    sget-object v0, Lcom/google/android/gms/drive/ai;->T:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "https://"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "http://"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 212
    const-string v1, "Host"

    invoke-virtual {v3, v1, v0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const-string v0, "X-Upload-Content-Type"

    iget-object v1, p1, Lcom/google/android/gms/drive/b/b/n;->f:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    const-string v0, "X-Upload-Content-Length"

    iget-wide v4, p1, Lcom/google/android/gms/drive/b/b/n;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/h;->g:Lcom/google/android/gms/drive/b/b;

    iget-object v0, p1, Lcom/google/android/gms/drive/b/b/n;->a:Lcom/google/android/gms/drive/auth/g;

    iget-object v1, p0, Lcom/google/android/gms/drive/b/b/h;->c:Landroid/content/Context;

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/drive/b/b;->a(Ljava/net/HttpURLConnection;Lcom/google/android/gms/drive/auth/g;Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    invoke-virtual {v3, v7}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 225
    invoke-virtual {v3, v6}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    .line 226
    const/4 v2, 0x0

    .line 228
    :try_start_1
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v0, Ljava/io/OutputStreamWriter;

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    const-string v5, "UTF-8"

    invoke-direct {v0, v4, v5}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v1, v0}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 230
    :try_start_2
    iget-object v0, p1, Lcom/google/android/gms/drive/b/b/n;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 234
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 237
    return-object v3

    .line 202
    :cond_1
    :try_start_3
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v1, Lcom/google/android/gms/drive/b/b/h;->b:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/gms/drive/b/b/n;->d:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/b/b/p;->a(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v0

    .line 205
    iget-object v1, p0, Lcom/google/android/gms/drive/b/b/h;->f:Lcom/google/android/gms/drive/b/b/e;

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 206
    const-string v1, "PUT"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    move-object v3, v0

    goto/16 :goto_0

    .line 220
    :catch_0
    move-exception v0

    .line 221
    new-instance v1, Lcom/google/android/gms/drive/b/b/k;

    const-string v2, "Failed to init session"

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/b/b/k;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 231
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 232
    :goto_1
    :try_start_4
    new-instance v2, Lcom/google/android/gms/drive/b/b/k;

    const-string v3, "Failed to upload metadata"

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/drive/b/b/k;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 234
    :catchall_0
    move-exception v0

    :goto_2
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 231
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method private static a(Ljava/io/InputStream;Ljava/io/OutputStream;J)V
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 350
    const-wide/16 v0, 0x0

    .line 351
    const/16 v2, 0x4000

    new-array v2, v2, [B

    .line 352
    :goto_0
    cmp-long v3, v0, p2

    if-gez v3, :cond_0

    .line 353
    const-wide/16 v4, 0x4000

    sub-long v6, p2, v0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v3, v4

    .line 354
    invoke-virtual {p0, v2, v8, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    .line 355
    invoke-virtual {p1, v2, v8, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 356
    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 357
    goto :goto_0

    .line 358
    :cond_0
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    .line 359
    return-void
.end method

.method private static a(Ljava/net/HttpURLConnection;Lcom/google/android/gms/drive/b/b/n;)V
    .locals 2

    .prologue
    .line 453
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    .line 455
    const/16 v1, 0x190

    if-eq v0, v1, :cond_0

    const/16 v1, 0x194

    if-eq v0, v1, :cond_0

    const/16 v1, 0x191

    if-eq v0, v1, :cond_0

    const/16 v1, 0x1f4

    if-ne v0, v1, :cond_1

    .line 457
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/b/b/n;->a(Ljava/lang/String;)V

    .line 458
    new-instance v0, Lcom/google/android/gms/drive/b/b/k;

    const-string v1, "Url expired."

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/b/b/k;-><init>(Ljava/lang/String;)V

    throw v0

    .line 460
    :cond_1
    return-void
.end method

.method private b(Ljava/net/HttpURLConnection;)J
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 427
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    .line 428
    const/16 v3, 0x134

    if-eq v2, v3, :cond_1

    .line 429
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/h;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v0, v0, Lcom/google/android/gms/drive/b/b/n;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/h;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v0, v0, Lcom/google/android/gms/drive/b/b/n;->k:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v2, v0, :cond_0

    .line 431
    new-instance v0, Lcom/google/android/gms/drive/b/b/j;

    const-string v1, "Conflict uploading"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/b/b/j;-><init>(Ljava/lang/String;)V

    throw v0

    .line 433
    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/b/b/k;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response code "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/b/b/k;-><init>(Ljava/lang/String;)V

    throw v0

    .line 435
    :cond_1
    const-string v2, "Range"

    invoke-virtual {p1, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 436
    if-nez v2, :cond_2

    .line 443
    :goto_0
    return-wide v0

    .line 439
    :cond_2
    invoke-static {v2}, Lcom/google/android/gms/drive/b/b/h;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/b/b/b;

    move-result-object v2

    .line 440
    iget-wide v4, v2, Lcom/google/android/gms/drive/b/b/b;->c:J

    cmp-long v0, v4, v0

    if-eqz v0, :cond_3

    .line 441
    new-instance v0, Lcom/google/android/gms/drive/b/b/k;

    const-string v1, "Unable to upload item: Bytes lost in transmission."

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/b/b/k;-><init>(Ljava/lang/String;)V

    throw v0

    .line 443
    :cond_3
    iget-wide v0, v2, Lcom/google/android/gms/drive/b/b/b;->d:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/drive/b/b/n;)Lcom/google/android/gms/drive/d/e;
    .locals 17

    .prologue
    .line 259
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/google/android/gms/drive/b/b/n;->e:J

    .line 260
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/gms/drive/b/b/h;->i:J

    sub-long/2addr v2, v4

    .line 261
    const-wide/32 v4, 0x40000

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v5, v2

    .line 262
    const/4 v3, 0x0

    .line 264
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/b/b/h;->e:Lcom/google/android/gms/drive/g/au;

    invoke-interface {v2}, Lcom/google/android/gms/drive/g/au;->a()Lcom/google/android/gms/drive/g/at;

    move-result-object v6

    .line 266
    const/4 v2, 0x0

    move v4, v2

    :goto_0
    :try_start_0
    sget-object v2, Lcom/google/android/gms/drive/ai;->al:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v4, v2, :cond_0

    .line 267
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/b/b/h;->j:Ljava/io/BufferedInputStream;

    const/high16 v7, 0x40000

    invoke-virtual {v2, v7}, Ljava/io/BufferedInputStream;->mark(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 269
    :try_start_1
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/drive/b/b/n;->j:Ljava/lang/String;

    const-string v7, "ResumableUploader"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Uploading to url "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/b/b/h;->f:Lcom/google/android/gms/drive/b/b/e;

    new-instance v2, Ljava/net/URL;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/gms/drive/b/b/n;->j:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;

    const-string v7, "Content-Type"

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/gms/drive/b/b/n;->f:Ljava/lang/String;

    invoke-virtual {v2, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/drive/b/b/h;->g:Lcom/google/android/gms/drive/b/b;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/gms/drive/b/b/n;->a:Lcom/google/android/gms/drive/auth/g;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/drive/b/b/h;->c:Landroid/content/Context;

    invoke-static {v2, v7, v8}, Lcom/google/android/gms/drive/b/b;->a(Ljava/net/HttpURLConnection;Lcom/google/android/gms/drive/auth/g;Landroid/content/Context;)V

    const-string v7, "PUT"

    invoke-virtual {v2, v7}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    if-lez v5, :cond_3

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "bytes %d-%d/%d"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/gms/drive/b/b/h;->i:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/gms/drive/b/b/h;->i:J

    int-to-long v14, v5

    add-long/2addr v12, v14

    const-wide/16 v14, 0x1

    sub-long/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/google/android/gms/drive/b/b/n;->e:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "Content-Range"

    invoke-virtual {v2, v8, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v7, 0x1

    invoke-virtual {v2, v7}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    invoke-virtual {v2, v5}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/drive/b/b/h;->j:Ljava/io/BufferedInputStream;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v8

    int-to-long v10, v5

    invoke-static {v7, v8, v10, v11}, Lcom/google/android/gms/drive/b/b/h;->a(Ljava/io/InputStream;Ljava/io/OutputStream;J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270
    :goto_1
    :try_start_2
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    .line 271
    const-string v7, "ResumableUploader"

    const-string v8, "HTTP upload status %d"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v9, v10

    invoke-static {v7, v8, v9}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 272
    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/google/android/gms/drive/b/b/h;->a(Ljava/net/HttpURLConnection;Lcom/google/android/gms/drive/b/b/n;)V

    .line 274
    invoke-static {v2}, Lcom/google/android/gms/drive/b/b/h;->c(Ljava/net/HttpURLConnection;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v3

    if-nez v3, :cond_5

    move-object v3, v2

    .line 291
    :cond_0
    if-eqz v3, :cond_1

    :try_start_3
    invoke-static {v3}, Lcom/google/android/gms/drive/b/b/h;->c(Ljava/net/HttpURLConnection;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 292
    :cond_1
    new-instance v2, Lcom/google/android/gms/drive/b/b/k;

    const-string v4, "Failed to upload. Ran out of tries"

    invoke-direct {v2, v4}, Lcom/google/android/gms/drive/b/b/k;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 309
    :catchall_0
    move-exception v2

    :goto_2
    if-eqz v3, :cond_2

    .line 310
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_2
    throw v2

    .line 269
    :cond_3
    const/4 v7, 0x1

    :try_start_4
    invoke-virtual {v2, v7}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 279
    :catch_0
    move-exception v2

    .line 280
    :goto_3
    :try_start_5
    const-string v7, "ResumableUploader"

    const-string v8, "Upload attempt failed, retrying."

    invoke-static {v7, v2, v8}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 282
    :goto_4
    if-eqz v3, :cond_4

    .line 283
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 285
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/b/b/h;->j:Ljava/io/BufferedInputStream;

    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->reset()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 287
    :try_start_6
    invoke-interface {v6}, Lcom/google/android/gms/drive/g/at;->e()V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 266
    :goto_5
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_0

    .line 277
    :cond_5
    :try_start_7
    const-string v3, "ResumableUploader"

    const-string v7, "Upload attempt failed, retrying."

    invoke-static {v3, v7}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-object v3, v2

    .line 281
    goto :goto_4

    .line 295
    :cond_6
    :try_start_8
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/gms/drive/b/b/h;->a(Ljava/net/HttpURLConnection;)Lcom/google/android/gms/drive/d/e;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v2

    .line 296
    if-eqz v2, :cond_8

    .line 309
    if-eqz v3, :cond_7

    .line 310
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_7
    :goto_6
    return-object v2

    .line 300
    :cond_8
    :try_start_9
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/gms/drive/b/b/h;->b(Ljava/net/HttpURLConnection;)J

    move-result-wide v6

    .line 301
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/gms/drive/b/b/h;->i:J

    int-to-long v10, v5

    add-long/2addr v8, v10

    cmp-long v2, v8, v6

    if-eqz v2, :cond_9

    .line 302
    new-instance v2, Lcom/google/android/gms/drive/b/b/k;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "Server did not receive the correct number of bytes. "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/gms/drive/b/b/h;->i:J

    int-to-long v10, v5

    add-long/2addr v8, v10

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/google/android/gms/drive/b/b/k;-><init>(Ljava/lang/String;)V

    throw v2

    .line 306
    :cond_9
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/google/android/gms/drive/b/b/h;->i:J
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 307
    if-eqz v3, :cond_a

    .line 310
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_a
    const/4 v2, 0x0

    goto :goto_6

    :catch_1
    move-exception v2

    goto :goto_5

    .line 309
    :catchall_1
    move-exception v3

    move-object/from16 v16, v3

    move-object v3, v2

    move-object/from16 v2, v16

    goto/16 :goto_2

    .line 279
    :catch_2
    move-exception v3

    move-object/from16 v16, v3

    move-object v3, v2

    move-object/from16 v2, v16

    goto/16 :goto_3
.end method

.method private c(Lcom/google/android/gms/drive/b/b/n;)Lcom/google/android/gms/drive/d/e;
    .locals 6

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/h;->f:Lcom/google/android/gms/drive/b/b/e;

    new-instance v0, Ljava/net/URL;

    iget-object v1, p1, Lcom/google/android/gms/drive/b/b/n;->j:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 376
    :try_start_0
    const-string v1, "PUT"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 377
    const-string v1, "Content-Range"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bytes */"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p1, Lcom/google/android/gms/drive/b/b/n;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 381
    iget-object v1, p0, Lcom/google/android/gms/drive/b/b/h;->g:Lcom/google/android/gms/drive/b/b;

    iget-object v1, p1, Lcom/google/android/gms/drive/b/b/n;->a:Lcom/google/android/gms/drive/auth/g;

    iget-object v2, p0, Lcom/google/android/gms/drive/b/b/h;->c:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/b/b;->a(Ljava/net/HttpURLConnection;Lcom/google/android/gms/drive/auth/g;Landroid/content/Context;)V

    .line 383
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 385
    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/b/b/h;->a(Ljava/net/HttpURLConnection;)Lcom/google/android/gms/drive/d/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 386
    if-eqz v1, :cond_0

    .line 394
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    move-object v0, v1

    :goto_0
    return-object v0

    .line 389
    :cond_0
    :try_start_1
    invoke-static {v0, p1}, Lcom/google/android/gms/drive/b/b/h;->a(Ljava/net/HttpURLConnection;Lcom/google/android/gms/drive/b/b/n;)V

    .line 390
    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/b/b/h;->b(Ljava/net/HttpURLConnection;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/drive/b/b/h;->i:J

    .line 391
    iget-object v1, p0, Lcom/google/android/gms/drive/b/b/h;->j:Ljava/io/BufferedInputStream;

    iget-wide v2, p0, Lcom/google/android/gms/drive/b/b/h;->i:J

    invoke-virtual {v1, v2, v3}, Ljava/io/BufferedInputStream;->skip(J)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 392
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v1
.end method

.method private static c(Ljava/net/HttpURLConnection;)Z
    .locals 2

    .prologue
    .line 466
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    .line 467
    const/16 v1, 0x1f4

    if-lt v0, v1, :cond_0

    const/16 v1, 0x257

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/b/b/l;)Lcom/google/android/gms/drive/d/e;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 111
    .line 113
    :try_start_0
    new-instance v0, Ljava/io/BufferedInputStream;

    iget-object v1, p0, Lcom/google/android/gms/drive/b/b/h;->d:Lcom/google/android/gms/drive/b/b/n;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/b/b/n;->a()Ljava/io/InputStream;

    move-result-object v1

    const/high16 v3, 0x40000

    invoke-direct {v0, v1, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/h;->j:Ljava/io/BufferedInputStream;

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/h;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v0, v0, Lcom/google/android/gms/drive/b/b/n;->j:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 117
    const-string v0, "ResumableUploader"

    const-string v1, "Starting from the beginning"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/h;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v0, v0, Lcom/google/android/gms/drive/b/b/n;->d:Lcom/google/android/gms/drive/DriveId;

    invoke-interface {p1, v0}, Lcom/google/android/gms/drive/b/b/l;->a(Lcom/google/android/gms/drive/DriveId;)V

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/h;->d:Lcom/google/android/gms/drive/b/b/n;
    :try_end_0
    .catch Lcom/google/android/gms/drive/b/b/i; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/b/b/h;->a(Lcom/google/android/gms/drive/b/b/n;)Ljava/net/HttpURLConnection;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v1

    :try_start_2
    invoke-static {v1, v0}, Lcom/google/android/gms/drive/b/b/h;->a(Ljava/net/HttpURLConnection;Lcom/google/android/gms/drive/b/b/n;)V

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v4

    const/16 v5, 0xc8

    if-eq v4, v5, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/drive/b/b/h;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v2, v2, Lcom/google/android/gms/drive/b/b/n;->k:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/drive/b/b/h;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v2, v2, Lcom/google/android/gms/drive/b/b/n;->k:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v4, v2, :cond_1

    new-instance v0, Lcom/google/android/gms/drive/b/b/j;

    const-string v2, "Conflict uploading"

    invoke-direct {v0, v2}, Lcom/google/android/gms/drive/b/b/j;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    move-exception v0

    :goto_0
    :try_start_3
    new-instance v2, Lcom/google/android/gms/drive/b/b/k;

    const-string v3, "Failed to start request"

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/drive/b/b/k;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    :try_start_4
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_0
    throw v0
    :try_end_4
    .catch Lcom/google/android/gms/drive/b/b/i; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 139
    :catch_1
    move-exception v0

    :try_start_5
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 143
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/b/b/h;->j:Ljava/io/BufferedInputStream;

    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    throw v0

    .line 119
    :cond_1
    :try_start_6
    new-instance v2, Lcom/google/android/gms/drive/b/b/k;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unable to upload item: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to upload "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, v0, Lcom/google/android/gms/drive/b/b/n;->d:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/gms/drive/b/b/k;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    const-string v3, "Location"

    invoke-virtual {v1, v3}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    new-instance v0, Lcom/google/android/gms/drive/b/b/k;

    const-string v2, "Unable to upload item: Server upload URI invalid."

    invoke-direct {v0, v2}, Lcom/google/android/gms/drive/b/b/k;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/b/b/n;->a(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v1, :cond_5

    :try_start_7
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    move-object v0, v2

    .line 129
    :cond_4
    :goto_2
    if-nez v0, :cond_8

    .line 130
    invoke-static {}, Lcom/google/android/gms/drive/g/f;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Lcom/google/android/gms/drive/b/b/i;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " interrupted"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/b/b/i;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_7
    .catch Lcom/google/android/gms/drive/b/b/i; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 140
    :catch_2
    move-exception v0

    .line 141
    :try_start_8
    new-instance v1, Lcom/google/android/gms/drive/b/b/k;

    const-string v2, "Upload exception"

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/b/b/k;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :cond_5
    move-object v0, v2

    .line 119
    goto :goto_2

    .line 121
    :cond_6
    :try_start_9
    const-string v0, "ResumableUploader"

    const-string v1, "Starting from status request"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/h;->d:Lcom/google/android/gms/drive/b/b/n;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/b/b/h;->c(Lcom/google/android/gms/drive/b/b/n;)Lcom/google/android/gms/drive/d/e;
    :try_end_9
    .catch Lcom/google/android/gms/drive/b/b/i; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result-object v0

    .line 124
    if-eqz v0, :cond_4

    .line 143
    iget-object v1, p0, Lcom/google/android/gms/drive/b/b/h;->j:Ljava/io/BufferedInputStream;

    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 145
    :goto_3
    return-object v0

    .line 131
    :cond_7
    :try_start_a
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/h;->d:Lcom/google/android/gms/drive/b/b/n;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/b/b/h;->b(Lcom/google/android/gms/drive/b/b/n;)Lcom/google/android/gms/drive/d/e;

    move-result-object v6

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/h;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v1, v0, Lcom/google/android/gms/drive/b/b/n;->d:Lcom/google/android/gms/drive/DriveId;

    iget-wide v2, p0, Lcom/google/android/gms/drive/b/b/h;->i:J

    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/h;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-wide v4, v0, Lcom/google/android/gms/drive/b/b/n;->e:J

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/drive/b/b/l;->a(Lcom/google/android/gms/drive/DriveId;JJ)V

    move-object v0, v6

    goto :goto_2

    .line 137
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/drive/b/b/h;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v1, v1, Lcom/google/android/gms/drive/b/b/n;->d:Lcom/google/android/gms/drive/DriveId;

    invoke-interface {p1, v1}, Lcom/google/android/gms/drive/b/b/l;->b(Lcom/google/android/gms/drive/DriveId;)V
    :try_end_a
    .catch Lcom/google/android/gms/drive/b/b/i; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 143
    iget-object v1, p0, Lcom/google/android/gms/drive/b/b/h;->j:Ljava/io/BufferedInputStream;

    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    goto :goto_3

    .line 119
    :catchall_2
    move-exception v0

    move-object v1, v2

    goto/16 :goto_1

    :catch_3
    move-exception v0

    move-object v1, v2

    goto/16 :goto_0
.end method
