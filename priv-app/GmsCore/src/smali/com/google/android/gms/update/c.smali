.class public final Lcom/google/android/gms/update/c;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field a:Landroid/content/Context;

.field b:Landroid/content/SharedPreferences;

.field c:Z

.field d:Ljava/lang/String;

.field e:Ljava/net/URL;

.field f:Ljava/lang/String;

.field g:Ljava/io/File;

.field h:J

.field i:J

.field j:J

.field k:J

.field l:J

.field volatile m:I

.field volatile n:Z

.field o:Ljava/util/regex/Pattern;

.field p:I

.field q:J

.field private r:Lcom/google/android/gms/http/GoogleHttpClient;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;ZJJLcom/google/android/gms/http/GoogleHttpClient;)V
    .locals 8

    .prologue
    .line 125
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 61
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/gms/update/c;->l:J

    .line 63
    const/4 v2, 0x1

    iput v2, p0, Lcom/google/android/gms/update/c;->m:I

    .line 64
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/gms/update/c;->n:Z

    .line 238
    const-string v2, ".*filename=\"([a-zA-Z0-9_.-]+)\""

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/update/c;->o:Ljava/util/regex/Pattern;

    .line 512
    const/4 v2, -0x1

    iput v2, p0, Lcom/google/android/gms/update/c;->p:I

    .line 513
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/gms/update/c;->q:J

    .line 126
    iput-object p1, p0, Lcom/google/android/gms/update/c;->a:Landroid/content/Context;

    .line 127
    iput-object p2, p0, Lcom/google/android/gms/update/c;->b:Landroid/content/SharedPreferences;

    .line 128
    iput-boolean p5, p0, Lcom/google/android/gms/update/c;->c:Z

    .line 129
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/gms/update/c;->r:Lcom/google/android/gms/http/GoogleHttpClient;

    .line 130
    iput-wide p6, p0, Lcom/google/android/gms/update/c;->j:J

    .line 131
    move-wide/from16 v0, p8

    iput-wide v0, p0, Lcom/google/android/gms/update/c;->k:J

    .line 133
    invoke-interface {p2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 135
    const-string v3, "dl.url"

    const/4 v4, 0x0

    invoke-interface {p2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/update/c;->d:Ljava/lang/String;

    .line 136
    iget-object v3, p0, Lcom/google/android/gms/update/c;->d:Ljava/lang/String;

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 137
    const-string v3, "DownloadAttempt"

    const-string v4, "URL changed from last attempt; resetting"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    invoke-static {p1, p2}, Lcom/google/android/gms/update/c;->a(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 140
    const-string v3, "dl.url"

    invoke-interface {v2, v3, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 141
    if-eqz p4, :cond_1

    .line 142
    const-string v3, "dl.authtoken"

    invoke-interface {v2, v3, p4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 147
    :goto_0
    iput-object p3, p0, Lcom/google/android/gms/update/c;->d:Ljava/lang/String;

    .line 148
    iput-object p4, p0, Lcom/google/android/gms/update/c;->f:Ljava/lang/String;

    .line 149
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    .line 150
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/google/android/gms/update/c;->h:J

    .line 151
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/google/android/gms/update/c;->i:J

    .line 159
    :goto_1
    :try_start_0
    new-instance v3, Ljava/net/URL;

    iget-object v4, p0, Lcom/google/android/gms/update/c;->d:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/gms/update/c;->e:Ljava/net/URL;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    const-string v3, "dl.filename"

    const/4 v4, 0x0

    invoke-interface {p2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 167
    if-eqz v3, :cond_3

    .line 168
    const-string v4, "dl.dirname"

    const/4 v5, 0x0

    invoke-interface {p2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 169
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    .line 170
    const-string v3, "DownloadAttempt"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "current file is "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :goto_2
    const-string v3, "DownloadAttempt"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "mSize "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, p0, Lcom/google/android/gms/update/c;->h:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mDownloaded "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lcom/google/android/gms/update/c;->i:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v3, p0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    if-eqz v3, :cond_0

    iget-wide v4, p0, Lcom/google/android/gms/update/c;->h:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    iget-wide v4, p0, Lcom/google/android/gms/update/c;->i:J

    iget-wide v6, p0, Lcom/google/android/gms/update/c;->h:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 178
    const/4 v3, 0x0

    iput v3, p0, Lcom/google/android/gms/update/c;->m:I

    .line 181
    :cond_0
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 182
    :goto_3
    return-void

    .line 144
    :cond_1
    const-string v3, "dl.authtoken"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_0

    .line 153
    :cond_2
    const-string v3, "dl.size"

    const-wide/16 v4, -0x1

    invoke-interface {p2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/gms/update/c;->h:J

    .line 154
    const-string v3, "dl.downloaded"

    const-wide/16 v4, -0x1

    invoke-interface {p2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/gms/update/c;->i:J

    .line 155
    iput-object p4, p0, Lcom/google/android/gms/update/c;->f:Ljava/lang/String;

    goto/16 :goto_1

    .line 161
    :catch_0
    move-exception v2

    invoke-static {p1, p2}, Lcom/google/android/gms/update/c;->a(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 162
    const/4 v2, 0x5

    iput v2, p0, Lcom/google/android/gms/update/c;->m:I

    goto :goto_3

    .line 172
    :cond_3
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    .line 173
    const-string v3, "DownloadAttempt"

    const-string v4, "current file is null"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    packed-switch p0, :pswitch_data_0

    .line 118
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 111
    :pswitch_0
    const-string v0, "complete"

    goto :goto_0

    .line 112
    :pswitch_1
    const-string v0, "ready"

    goto :goto_0

    .line 113
    :pswitch_2
    const-string v0, "in_progress"

    goto :goto_0

    .line 114
    :pswitch_3
    const-string v0, "insufficient_space"

    goto :goto_0

    .line 115
    :pswitch_4
    const-string v0, "failed_temp"

    goto :goto_0

    .line 116
    :pswitch_5
    const-string v0, "failed"

    goto :goto_0

    .line 117
    :pswitch_6
    const-string v0, "cancelled"

    goto :goto_0

    .line 110
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 186
    const-string v1, "dl.dirname"

    invoke-interface {p1, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 187
    const-string v2, "dl.filename"

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 188
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 189
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v1, "DownloadAttempt"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, " deleting "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 194
    :cond_0
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "dl.url"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "dl.authtoken"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "dl.filename"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "dl.dirname"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "dl.size"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "dl.downloaded"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 206
    const-string v1, "download"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 207
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 208
    const-string v4, "DownloadAttempt"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " deleting "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 207
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 211
    :cond_1
    return-void
.end method

.method private static a(Ljava/net/HttpURLConnection;)V
    .locals 5

    .prologue
    .line 245
    const/4 v0, 0x0

    .line 246
    :goto_0
    invoke-virtual {p0, v0}, Ljava/net/HttpURLConnection;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v1

    .line 247
    if-eqz v1, :cond_0

    .line 248
    const-string v2, "DownloadAttempt"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "  header "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v0}, Ljava/net/HttpURLConnection;->getHeaderField(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 250
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 539
    iget-wide v4, p0, Lcom/google/android/gms/update/c;->h:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move v1, v2

    .line 608
    :goto_0
    return v1

    :cond_1
    move v3, v2

    .line 541
    :goto_1
    const/4 v0, 0x2

    if-ge v3, v0, :cond_9

    .line 542
    iget-boolean v0, p0, Lcom/google/android/gms/update/c;->c:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    rem-int/lit8 v0, v0, 0x2

    packed-switch v0, :pswitch_data_0

    .line 541
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 542
    goto :goto_2

    .line 544
    :pswitch_0
    new-instance v0, Ljava/io/File;

    const-string v4, "/cache"

    invoke-direct {v0, v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    .line 552
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/a/a;->m()J

    move-result-wide v4

    .line 553
    const-string v0, "DownloadAttempt"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "free space on /cache: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " package "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v8, p0, Lcom/google/android/gms/update/c;->h:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    iget-wide v6, p0, Lcom/google/android/gms/update/c;->h:J

    sub-long/2addr v4, v6

    iget-object v0, p0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    add-long/2addr v4, v6

    iget-wide v6, p0, Lcom/google/android/gms/update/c;->j:J

    cmp-long v0, v4, v6

    if-gez v0, :cond_3

    .line 557
    const-string v0, "DownloadAttempt"

    const-string v4, "saving on /cache wouldn\'t leave enough space"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 563
    :cond_3
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/a/a;->n()J

    move-result-wide v4

    .line 564
    const-string v0, "DownloadAttempt"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "free space on /data: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mMinDataFreeSize "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v8, p0, Lcom/google/android/gms/update/c;->k:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    iget-wide v6, p0, Lcom/google/android/gms/update/c;->k:J

    cmp-long v0, v4, v6

    if-gez v0, :cond_4

    .line 568
    const-string v0, "DownloadAttempt"

    const-string v4, "/cache download, but not enough space on /data"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 572
    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/update/c;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 573
    iget-object v0, p0, Lcom/google/android/gms/update/c;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "dl.dirname"

    const-string v3, "/cache"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "dl.filename"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    .line 577
    :cond_5
    const-string v0, "DownloadAttempt"

    const-string v4, "failed to save file on /cache"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 581
    :pswitch_1
    new-instance v0, Ljava/io/File;

    const-string v4, "/system/bin/uncrypt"

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 582
    const-string v0, "DownloadAttempt"

    const-string v4, "uncrypt exists, use of /data ok"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    iget-object v0, p0, Lcom/google/android/gms/update/c;->a:Landroid/content/Context;

    const-string v4, "download"

    invoke-virtual {v0, v4, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 584
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    .line 586
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/a/a;->n()J

    move-result-wide v4

    .line 587
    const-string v6, "DownloadAttempt"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "free space on /data: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " package "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/google/android/gms/update/c;->h:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    iget-wide v6, p0, Lcom/google/android/gms/update/c;->h:J

    sub-long/2addr v4, v6

    iget-object v6, p0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v6

    add-long/2addr v4, v6

    iget-wide v6, p0, Lcom/google/android/gms/update/c;->k:J

    cmp-long v4, v4, v6

    if-gez v4, :cond_6

    .line 589
    const-string v0, "DownloadAttempt"

    const-string v4, "saving on /data wouldn\'t leave enough space"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 593
    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/update/c;->c()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 594
    iget-object v2, p0, Lcom/google/android/gms/update/c;->b:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "dl.dirname"

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "dl.filename"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    .line 599
    :cond_7
    const-string v0, "DownloadAttempt"

    const-string v4, "failed to save file on /data"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 601
    :cond_8
    const-string v0, "DownloadAttempt"

    const-string v4, "not trying /data; no uncrypt"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 607
    :cond_9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    move v1, v2

    .line 608
    goto/16 :goto_0

    .line 542
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private varargs b()Ljava/lang/Integer;
    .locals 16

    .prologue
    const/4 v3, 0x0

    const-wide/16 v14, 0x0

    const/4 v6, 0x5

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 284
    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/google/android/gms/update/c;->n:Z

    .line 285
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gms/update/c;->m:I

    if-eq v2, v7, :cond_1

    .line 286
    const-string v2, "DownloadAttempt"

    const-string v3, "attempting to start download from non-ready state"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 501
    :cond_0
    :goto_0
    return-object v2

    .line 289
    :cond_1
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/gms/update/c;->m:I

    .line 291
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/a/a;->l()V

    .line 293
    const-string v2, "DownloadAttempt"

    const-string v4, "querying downloadmanager"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/c;->a:Landroid/content/Context;

    const-string v4, "download"

    invoke-virtual {v2, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/DownloadManager;

    new-instance v4, Landroid/app/DownloadManager$Query;

    invoke-direct {v4}, Landroid/app/DownloadManager$Query;-><init>()V

    invoke-virtual {v2, v4}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_1
    invoke-interface {v4}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v8

    if-nez v8, :cond_3

    const-string v8, "local_filename"

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_2

    const-string v9, "/cache/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    const-string v9, "_id"

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    new-array v9, v7, [J

    aput-wide v10, v9, v5

    invoke-virtual {v2, v9}, Landroid/app/DownloadManager;->remove([J)I

    const-string v9, "DownloadAttempt"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "remove old download id "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " local name "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    :cond_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 295
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/c;->a:Landroid/content/Context;

    const-string v4, "wifi"

    invoke-virtual {v2, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 297
    if-eqz v2, :cond_26

    .line 298
    const-string v4, "system update download"

    invoke-virtual {v2, v7, v4}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v2

    move-object v4, v2

    .line 304
    :goto_2
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/update/c;->c()Z

    move-result v2

    if-nez v2, :cond_5

    .line 305
    const-string v2, "DownloadAttempt"

    const-string v7, "Failed to extend the file to OTA size, returning insufficient space."

    invoke-static {v2, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/gms/update/c;->m:I

    .line 307
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gms/update/c;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 498
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto/16 :goto_0

    .line 310
    :cond_5
    if-eqz v4, :cond_6

    :try_start_1
    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 312
    :cond_6
    const-string v2, "DownloadAttempt"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "mUrl is "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/update/c;->e:Ljava/net/URL;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/c;->e:Ljava/net/URL;

    if-nez v2, :cond_7

    .line 314
    const-string v2, "DownloadAttempt"

    const-string v7, "no url to download"

    invoke-static {v2, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/gms/update/c;->m:I

    .line 316
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gms/update/c;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 498
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto/16 :goto_0

    .line 319
    :cond_7
    :try_start_2
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/gms/update/c;->h:J

    cmp-long v2, v8, v14

    if-lez v2, :cond_8

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/gms/update/c;->h:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/update/c;->i:J

    cmp-long v2, v8, v10

    if-nez v2, :cond_8

    .line 320
    const-string v2, "DownloadAttempt"

    const-string v7, "already completed"

    invoke-static {v2, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/gms/update/c;->m:I

    .line 322
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gms/update/c;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 498
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto/16 :goto_0

    .line 325
    :cond_8
    const/16 v8, 0xc8

    .line 326
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/c;->r:Lcom/google/android/gms/http/GoogleHttpClient;

    invoke-virtual {v2}, Lcom/google/android/gms/http/GoogleHttpClient;->getConnectionFactory()Lcom/google/android/gms/http/e;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/update/c;->e:Ljava/net/URL;

    invoke-virtual {v2, v9}, Lcom/google/android/gms/http/e;->a(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v3

    .line 327
    const-string v2, "Accept-Encoding"

    const-string v9, "identity"

    invoke-virtual {v3, v2, v9}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    const v2, 0x1d4c0

    invoke-virtual {v3, v2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 329
    const v2, 0x1d4c0

    invoke-virtual {v3, v2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 330
    instance-of v2, v3, Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v2, :cond_9

    .line 331
    move-object v0, v3

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    move-object v2, v0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/update/c;->r:Lcom/google/android/gms/http/GoogleHttpClient;

    invoke-virtual {v9}, Lcom/google/android/gms/http/GoogleHttpClient;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 333
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/c;->f:Ljava/lang/String;

    if-eqz v2, :cond_f

    .line 334
    const-string v2, "Authorization"

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/update/c;->f:Ljava/lang/String;

    invoke-virtual {v3, v2, v9}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    const-string v2, "DownloadAttempt"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, " including auth header "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/update/c;->f:Ljava/lang/String;

    const/4 v11, 0x0

    const/16 v12, 0x10

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "..."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    :goto_3
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/update/c;->i:J

    cmp-long v2, v10, v14

    if-lez v2, :cond_a

    .line 340
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v8, "bytes="

    invoke-direct {v2, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/gms/update/c;->i:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, "-"

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 341
    const-string v8, "Range"

    invoke-virtual {v3, v8, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    const-string v8, "DownloadAttempt"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "sending range request: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 343
    const/16 v2, 0xce

    move v8, v2

    .line 347
    :cond_a
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/c;->a:Landroid/content/Context;

    const-string v9, "Calling this from your main thread can lead to deadlock."

    invoke-static {v9}, Lcom/google/android/gms/common/internal/bx;->c(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/google/android/gms/checkin/f;->a(Landroid/content/Context;)Lcom/google/android/gms/common/b;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/google/android/gms/checkin/f;->a(Landroid/content/Context;Lcom/google/android/gms/common/b;)Ljava/lang/String;

    move-result-object v2

    .line 348
    if-eqz v2, :cond_b

    .line 349
    const-string v9, "ddvi"

    invoke-virtual {v3, v9, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    const-string v9, "DownloadAttempt"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Set ddvi for downloadAttempt as a request property: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v9, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 357
    :cond_b
    :goto_4
    const/4 v2, 0x1

    :try_start_5
    invoke-virtual {v3, v2}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 359
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    .line 360
    const-string v9, "DownloadAttempt"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "response is "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    if-eq v2, v8, :cond_16

    .line 363
    const/16 v7, 0x1f7

    if-ne v2, v7, :cond_14

    .line 365
    const-string v2, "Retry-After"

    invoke-virtual {v3, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 366
    const-string v7, "DownloadAttempt"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "retry after: ["

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 367
    const-wide/16 v8, 0xe10

    .line 368
    if-eqz v2, :cond_c

    .line 370
    :try_start_6
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_6
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-wide v8

    .line 378
    :cond_c
    :goto_5
    :try_start_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    mul-long/2addr v8, v12

    add-long/2addr v8, v10

    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/google/android/gms/update/c;->l:J

    .line 383
    :goto_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gms/update/c;->m:I

    const/16 v7, 0x1f4

    if-ge v2, v7, :cond_d

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gms/update/c;->m:I

    const/16 v7, 0x191

    if-ne v2, v7, :cond_15

    .line 387
    :cond_d
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/gms/update/c;->m:I

    .line 394
    :goto_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gms/update/c;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v2

    .line 498
    if-eqz v3, :cond_e

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 499
    :cond_e
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto/16 :goto_0

    .line 337
    :cond_f
    :try_start_8
    const-string v2, "DownloadAttempt"

    const-string v9, " including no auth header"

    invoke-static {v2, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_3

    .line 494
    :catch_0
    move-exception v2

    .line 495
    :try_start_9
    const-string v7, "DownloadAttempt"

    const-string v8, "caught ioexception"

    invoke-static {v7, v8, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 496
    if-eqz v5, :cond_25

    const/4 v2, 0x4

    :goto_8
    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/gms/update/c;->m:I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 498
    if-eqz v3, :cond_10

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 499
    :cond_10
    if-eqz v4, :cond_11

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 501
    :cond_11
    :goto_9
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gms/update/c;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 352
    :catch_1
    move-exception v2

    .line 354
    :try_start_a
    const-string v9, "DownloadAttempt"

    const-string v10, "Failed to set ddvi as a download request property."

    invoke-static {v9, v10, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_4

    .line 498
    :catchall_0
    move-exception v2

    if-eqz v3, :cond_12

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 499
    :cond_12
    if-eqz v4, :cond_13

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v3

    if-eqz v3, :cond_13

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    :cond_13
    throw v2

    .line 380
    :cond_14
    :try_start_b
    invoke-static {v3}, Lcom/google/android/gms/update/c;->a(Ljava/net/HttpURLConnection;)V

    goto :goto_6

    .line 391
    :cond_15
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/gms/update/c;->m:I

    goto :goto_7

    .line 397
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/c;->b:Landroid/content/SharedPreferences;

    const-string v8, "dl.filename"

    const/4 v9, 0x0

    invoke-interface {v2, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 399
    if-nez v2, :cond_17

    .line 400
    const-string v2, "update.zip"

    .line 401
    const-string v8, "Content-Disposition"

    invoke-virtual {v3, v8}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 402
    const-string v9, "DownloadAttempt"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "disposition header: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    if-eqz v8, :cond_17

    .line 404
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/update/c;->o:Ljava/util/regex/Pattern;

    invoke-virtual {v9, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    .line 405
    invoke-virtual {v8}, Ljava/util/regex/Matcher;->matches()Z

    move-result v9

    if-eqz v9, :cond_17

    .line 406
    const/4 v2, 0x1

    invoke-virtual {v8, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    .line 412
    :cond_17
    const-string v8, "ETag"

    invoke-virtual {v3, v8}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 413
    const-string v9, "DownloadAttempt"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "etag is "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v8

    int-to-long v8, v8

    .line 416
    const-string v10, "DownloadAttempt"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "contentLength is "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    const-wide/32 v10, 0xc800

    cmp-long v10, v8, v10

    if-gez v10, :cond_18

    .line 424
    invoke-static {v3}, Lcom/google/android/gms/update/c;->a(Ljava/net/HttpURLConnection;)V

    .line 427
    :cond_18
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/update/c;->h:J

    const-wide/16 v12, -0x1

    cmp-long v10, v10, v12

    if-nez v10, :cond_19

    .line 428
    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/google/android/gms/update/c;->h:J

    .line 429
    const-wide/16 v8, 0x0

    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/google/android/gms/update/c;->i:J

    .line 431
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/update/c;->b:Landroid/content/SharedPreferences;

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string v9, "dl.size"

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/update/c;->h:J

    invoke-interface {v8, v9, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string v9, "dl.downloaded"

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/update/c;->i:J

    invoke-interface {v8, v9, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 437
    :cond_19
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/gms/update/c;->h:J

    cmp-long v8, v8, v14

    if-gtz v8, :cond_1b

    .line 438
    const-string v2, "DownloadAttempt"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "bad download size = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/gms/update/c;->h:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/c;->b:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v7, "dl.size"

    invoke-interface {v2, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v7, "dl.downloaded"

    invoke-interface {v2, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 443
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/gms/update/c;->m:I

    .line 444
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gms/update/c;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move-result-object v2

    .line 498
    if-eqz v3, :cond_1a

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 499
    :cond_1a
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto/16 :goto_0

    .line 447
    :cond_1b
    :try_start_c
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    if-nez v8, :cond_1e

    if-eqz v2, :cond_1e

    .line 450
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gms/update/c;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1d

    .line 451
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 452
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/gms/update/c;->m:I

    .line 453
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gms/update/c;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-result-object v2

    .line 498
    if-eqz v3, :cond_1c

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 499
    :cond_1c
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto/16 :goto_0

    .line 455
    :cond_1d
    :try_start_d
    const-string v2, "DownloadAttempt"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "downloading to "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    :cond_1e
    const-string v2, "DownloadAttempt"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "downloaded "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/update/c;->i:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " / "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/update/c;->h:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " bytes"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/gms/update/c;->i:J

    const-wide/16 v10, 0x64

    mul-long/2addr v8, v10

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/update/c;->h:J

    div-long/2addr v8, v10

    long-to-int v2, v8

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gms/update/c;->b(I)V

    .line 461
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/gms/update/c;->i:J

    const-wide/32 v10, 0x20000

    add-long/2addr v8, v10

    .line 463
    new-instance v12, Ljava/io/RandomAccessFile;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    const-string v10, "rws"

    invoke-direct {v12, v2, v10}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 465
    :try_start_e
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 466
    const/16 v10, 0x1000

    new-array v13, v10, [B

    .line 467
    :cond_1f
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/update/c;->i:J

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/gms/update/c;->h:J

    cmp-long v10, v10, v14

    if-gez v10, :cond_23

    .line 468
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/update/c;->h:J

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/gms/update/c;->i:J

    sub-long/2addr v10, v14

    .line 469
    array-length v14, v13

    int-to-long v14, v14

    cmp-long v14, v10, v14

    if-lez v14, :cond_20

    array-length v10, v13

    int-to-long v10, v10

    .line 470
    :cond_20
    const/4 v14, 0x0

    long-to-int v10, v10

    invoke-virtual {v2, v13, v14, v10}, Ljava/io/InputStream;->read([BII)I

    move-result v10

    .line 471
    if-ltz v10, :cond_23

    .line 472
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/gms/update/c;->i:J

    invoke-virtual {v12, v14, v15}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 473
    const/4 v11, 0x0

    invoke-virtual {v12, v13, v11, v10}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 475
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/gms/update/c;->i:J

    int-to-long v10, v10

    add-long/2addr v10, v14

    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/google/android/gms/update/c;->i:J

    .line 476
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/update/c;->i:J

    cmp-long v10, v10, v8

    if-ltz v10, :cond_21

    .line 477
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/update/c;->b:Landroid/content/SharedPreferences;

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string v9, "dl.downloaded"

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/update/c;->i:J

    invoke-interface {v8, v9, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 478
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/gms/update/c;->i:J

    const-wide/32 v10, 0x20000

    add-long/2addr v8, v10

    move v5, v7

    .line 482
    :cond_21
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/update/c;->i:J

    const-wide/16 v14, 0x64

    mul-long/2addr v10, v14

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/gms/update/c;->h:J

    div-long/2addr v10, v14

    long-to-int v10, v10

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/gms/update/c;->b(I)V

    .line 484
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/update/c;->isCancelled()Z

    move-result v10

    if-eqz v10, :cond_1f

    .line 485
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/gms/update/c;->m:I

    .line 486
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gms/update/c;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    move-result-object v2

    .line 490
    :try_start_f
    invoke-virtual {v12}, Ljava/io/RandomAccessFile;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_0
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 498
    if-eqz v3, :cond_22

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 499
    :cond_22
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto/16 :goto_0

    .line 490
    :cond_23
    :try_start_10
    invoke-virtual {v12}, Ljava/io/RandomAccessFile;->close()V

    .line 492
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/c;->b:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v7, "dl.downloaded"

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/gms/update/c;->i:J

    invoke-interface {v2, v7, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 493
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/gms/update/c;->m:I
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_0
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 498
    if-eqz v3, :cond_24

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 499
    :cond_24
    if-eqz v4, :cond_11

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto/16 :goto_9

    .line 490
    :catchall_1
    move-exception v2

    :try_start_11
    invoke-virtual {v12}, Ljava/io/RandomAccessFile;->close()V

    throw v2
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_0
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    :cond_25
    move v2, v6

    .line 496
    goto/16 :goto_8

    :catch_2
    move-exception v2

    goto/16 :goto_5

    :cond_26
    move-object v4, v3

    goto/16 :goto_2
.end method

.method private b(I)V
    .locals 6

    .prologue
    .line 519
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 520
    iget v2, p0, Lcom/google/android/gms/update/c;->p:I

    if-le p1, v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/gms/update/c;->q:J

    const-wide/16 v4, 0xc8

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 522
    iput p1, p0, Lcom/google/android/gms/update/c;->p:I

    .line 523
    iput-wide v0, p0, Lcom/google/android/gms/update/c;->q:J

    .line 524
    iget-object v0, p0, Lcom/google/android/gms/update/c;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "download_progress"

    iget v2, p0, Lcom/google/android/gms/update/c;->p:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 526
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/update/c;->p:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/c;->publishProgress([Ljava/lang/Object;)V

    .line 527
    const-string v0, "DownloadAttempt"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "progress now "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/update/c;->p:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    :cond_0
    return-void
.end method

.method private c()Z
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    .line 612
    const/4 v2, 0x0

    .line 614
    :try_start_0
    iget-wide v6, p0, Lcom/google/android/gms/update/c;->h:J

    cmp-long v1, v6, v4

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/google/android/gms/update/c;->h:J

    cmp-long v1, v6, v8

    if-gez v1, :cond_2

    .line 616
    :cond_0
    const-string v1, "DownloadAttempt"

    const-string v3, "writing zero file"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    new-instance v3, Ljava/io/RandomAccessFile;

    iget-object v1, p0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    const-string v6, "rws"

    invoke-direct {v3, v1, v6}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618
    const/16 v1, 0x1000

    :try_start_1
    new-array v1, v1, [B

    .line 619
    :goto_0
    iget-wide v6, p0, Lcom/google/android/gms/update/c;->h:J

    const-wide/16 v8, 0x1000

    div-long/2addr v6, v8

    cmp-long v2, v4, v6

    if-gez v2, :cond_1

    .line 620
    invoke-virtual {v3, v1}, Ljava/io/RandomAccessFile;->write([B)V

    .line 619
    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    goto :goto_0

    .line 622
    :cond_1
    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/google/android/gms/update/c;->h:J

    const-wide/16 v6, 0x1000

    rem-long/2addr v4, v6

    long-to-int v4, v4

    invoke-virtual {v3, v1, v2, v4}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 623
    const-string v1, "DownloadAttempt"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "file extended to "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " bytes"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v2, v3

    .line 625
    :cond_2
    if-eqz v2, :cond_3

    :try_start_2
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 636
    :cond_3
    :goto_1
    const/4 v0, 0x1

    :cond_4
    :goto_2
    return v0

    .line 626
    :catch_0
    move-exception v1

    .line 627
    :goto_3
    :try_start_3
    const-string v3, "DownloadAttempt"

    const-string v4, "failed to extend file"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 628
    iget-object v1, p0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    if-eqz v1, :cond_5

    .line 629
    iget-object v1, p0, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 631
    :cond_5
    if-eqz v2, :cond_4

    :try_start_4
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_2

    .line 633
    :catchall_0
    move-exception v0

    .line 634
    :goto_4
    if-eqz v2, :cond_6

    :try_start_5
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 636
    :cond_6
    :goto_5
    throw v0

    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v1

    goto :goto_5

    .line 633
    :catchall_1
    move-exception v0

    move-object v2, v3

    goto :goto_4

    .line 626
    :catch_4
    move-exception v1

    move-object v2, v3

    goto :goto_3
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 222
    iget-boolean v0, p0, Lcom/google/android/gms/update/c;->n:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/update/c;->m:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 223
    const/4 v0, 0x2

    .line 225
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/gms/update/c;->m:I

    goto :goto_0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/gms/update/c;->b()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onCancelled(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/update/c;->a:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/update/c;->a:Landroid/content/Context;

    const-class v3, Lcom/google/android/gms/update/SystemUpdateService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/update/c;->a:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/update/c;->a:Landroid/content/Context;

    const-class v3, Lcom/google/android/gms/update/SystemUpdateService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method
