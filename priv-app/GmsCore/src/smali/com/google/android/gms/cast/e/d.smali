.class public abstract Lcom/google/android/gms/cast/e/d;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final d:I

.field private static final e:J

.field private static final f:J


# instance fields
.field protected final b:Ljava/lang/String;

.field protected final c:Lcom/google/android/gms/cast/e/h;

.field private g:Lcom/google/android/gms/http/GoogleHttpClient;

.field private final h:Ljava/lang/String;

.field private final i:Landroid/content/Context;

.field private final j:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 37
    const-string v0, "Android,%d,%s,%s,%s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const v3, 0x6768a8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/e/d;->a:Ljava/lang/String;

    .line 44
    sget-object v0, Lcom/google/android/gms/cast/a/b;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/gms/cast/e/d;->d:I

    .line 46
    sget-object v0, Lcom/google/android/gms/cast/a/b;->l:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/cast/e/d;->e:J

    .line 48
    sget-object v0, Lcom/google/android/gms/cast/a/b;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/cast/e/d;->f:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/android/gms/cast/e/d;->i:Landroid/content/Context;

    .line 61
    iput-object p3, p0, Lcom/google/android/gms/cast/e/d;->h:Ljava/lang/String;

    .line 62
    iput-object p4, p0, Lcom/google/android/gms/cast/e/d;->j:Landroid/os/Handler;

    .line 63
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "CastNearbyRequest"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/e/d;->c:Lcom/google/android/gms/cast/e/h;

    .line 64
    iput-object p2, p0, Lcom/google/android/gms/cast/e/d;->b:Ljava/lang/String;

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/cast/e/d;->c:Lcom/google/android/gms/cast/e/h;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;)V

    .line 66
    return-void
.end method

.method private static a(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 206
    new-instance v1, Ljava/util/Scanner;

    invoke-direct {v1, p0}, Ljava/util/Scanner;-><init>(Ljava/io/InputStream;)V

    .line 207
    const-string v0, "\\A"

    invoke-virtual {v1, v0}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    .line 208
    invoke-virtual {v1}, Ljava/util/Scanner;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v0

    .line 209
    :goto_0
    invoke-virtual {v1}, Ljava/util/Scanner;->close()V

    .line 210
    return-object v0

    .line 208
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private a(Lorg/apache/http/HttpResponse;)V
    .locals 8

    .prologue
    const/16 v4, 0x6000

    const/4 v6, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 124
    const-string v0, "Content-Type"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    .line 125
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 126
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 128
    sparse-switch v0, :sswitch_data_0

    .line 196
    iget-object v3, p0, Lcom/google/android/gms/cast/e/d;->c:Lcom/google/android/gms/cast/e/h;

    const-string v4, "Non 200 response code = %d"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v2

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 197
    if-eqz v1, :cond_0

    .line 198
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 199
    iget-object v1, p0, Lcom/google/android/gms/cast/e/d;->c:Lcom/google/android/gms/cast/e/h;

    invoke-static {v0}, Lcom/google/android/gms/cast/e/d;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 201
    :cond_0
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/e/d;->b(I)V

    .line 203
    :goto_0
    return-void

    .line 130
    :sswitch_0
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 131
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v0

    long-to-int v3, v0

    .line 132
    if-le v3, v4, :cond_1

    .line 133
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Http Response content longer than expected 24KB limit"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 136
    :cond_1
    if-lez v3, :cond_4

    .line 141
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v4

    .line 142
    new-array v0, v3, [B

    move v1, v2

    .line 143
    :goto_1
    if-ge v1, v3, :cond_2

    .line 144
    sub-int v5, v3, v1

    invoke-virtual {v4, v0, v1, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v5

    .line 147
    if-lez v5, :cond_2

    .line 148
    add-int/2addr v1, v5

    goto :goto_1

    .line 153
    :cond_2
    invoke-virtual {v4}, Ljava/io/InputStream;->available()I

    move-result v5

    if-eqz v5, :cond_3

    .line 154
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v5, "Stream still has %d bytes left after reading Http content length of %d bytes"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/io/InputStream;->available()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v7

    invoke-static {v1, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 158
    :cond_3
    if-eq v1, v3, :cond_6

    .line 159
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v5, "Http Content Length of %d doesn\'t match input stream length of %d"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_4
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 168
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 170
    new-array v3, v4, [B

    .line 172
    :goto_2
    array-length v4, v3

    invoke-virtual {v0, v3, v2, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_5

    .line 173
    invoke-virtual {v1, v3, v2, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_2

    .line 176
    :cond_5
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 177
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 180
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/cast/e/d;->j:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/cast/e/f;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/cast/e/f;-><init>(Lcom/google/android/gms/cast/e/d;[B)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 183
    :cond_7
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/e/d;->b(I)V

    goto/16 :goto_0

    .line 188
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/cast/e/d;->c:Lcom/google/android/gms/cast/e/h;

    const-string v3, "No matching device"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 189
    if-eqz v1, :cond_8

    .line 190
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 191
    iget-object v1, p0, Lcom/google/android/gms/cast/e/d;->c:Lcom/google/android/gms/cast/e/h;

    invoke-static {v0}, Lcom/google/android/gms/cast/e/d;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    :cond_8
    const/16 v0, 0x7d3

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/e/d;->b(I)V

    goto/16 :goto_0

    .line 128
    nop

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x193 -> :sswitch_1
        0x194 -> :sswitch_1
    .end sparse-switch
.end method

.method private varargs b()Ljava/lang/Void;
    .locals 11

    .prologue
    const/16 v3, 0xd

    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 74
    invoke-virtual {p0}, Lcom/google/android/gms/cast/e/d;->a()Lcom/google/protobuf/a/f;

    move-result-object v0

    .line 75
    if-nez v0, :cond_1

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/cast/e/d;->c:Lcom/google/android/gms/cast/e/h;

    const-string v1, "No message to send"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    invoke-direct {p0, v3}, Lcom/google/android/gms/cast/e/d;->b(I)V

    .line 120
    :cond_0
    :goto_0
    return-object v10

    .line 80
    :cond_1
    new-instance v1, Lcom/google/android/gms/http/GoogleHttpClient;

    iget-object v2, p0, Lcom/google/android/gms/cast/e/d;->i:Landroid/content/Context;

    sget-object v3, Lcom/google/android/gms/cast/e/i;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    iput-object v1, p0, Lcom/google/android/gms/cast/e/d;->g:Lcom/google/android/gms/http/GoogleHttpClient;

    .line 82
    new-instance v4, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual {v0}, Lcom/google/protobuf/a/f;->g()[B

    move-result-object v0

    invoke-direct {v4, v0}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 87
    sget v2, Lcom/google/android/gms/cast/e/d;->d:I

    .line 88
    sget-wide v0, Lcom/google/android/gms/cast/e/d;->e:J

    .line 90
    :goto_1
    add-int/lit8 v3, v2, -0x1

    if-lez v2, :cond_2

    .line 91
    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    iget-object v5, p0, Lcom/google/android/gms/cast/e/d;->h:Ljava/lang/String;

    invoke-direct {v2, v5}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 92
    const-string v5, "Content-Type"

    const-string v6, "application/x-protobuf"

    invoke-virtual {v2, v5, v6}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string v5, "x-cast-agent"

    sget-object v6, Lcom/google/android/gms/cast/e/d;->a:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    invoke-virtual {v2, v4}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 96
    :try_start_0
    iget-object v5, p0, Lcom/google/android/gms/cast/e/d;->c:Lcom/google/android/gms/cast/e/h;

    const-string v6, "Http request sent to url: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/gms/cast/e/d;->h:Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 97
    iget-object v5, p0, Lcom/google/android/gms/cast/e/d;->g:Lcom/google/android/gms/http/GoogleHttpClient;

    invoke-virtual {v5, v2}, Lcom/google/android/gms/http/GoogleHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    .line 98
    invoke-direct {p0, v2}, Lcom/google/android/gms/cast/e/d;->a(Lorg/apache/http/HttpResponse;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/cast/e/d;->g:Lcom/google/android/gms/http/GoogleHttpClient;

    invoke-virtual {v0}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    goto :goto_0

    .line 100
    :catch_0
    move-exception v2

    .line 101
    :try_start_1
    iget-object v5, p0, Lcom/google/android/gms/cast/e/d;->c:Lcom/google/android/gms/cast/e/h;

    const-string v6, "Exception sending HTTP request to %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/gms/cast/e/d;->h:Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-virtual {v5, v2, v6, v7}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104
    :try_start_2
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 105
    sget-wide v6, Lcom/google/android/gms/cast/e/d;->f:J
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    add-long/2addr v0, v6

    .line 114
    :goto_2
    iget-object v2, p0, Lcom/google/android/gms/cast/e/d;->g:Lcom/google/android/gms/http/GoogleHttpClient;

    invoke-virtual {v2}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    move v2, v3

    .line 115
    goto :goto_1

    .line 109
    :catch_1
    move-exception v0

    .line 110
    :try_start_3
    iget-object v1, p0, Lcom/google/android/gms/cast/e/d;->c:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Exception sending HTTP request to %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/gms/cast/e/d;->h:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v1, v0, v2, v4}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/e/d;->b(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 114
    iget-object v0, p0, Lcom/google/android/gms/cast/e/d;->g:Lcom/google/android/gms/http/GoogleHttpClient;

    invoke-virtual {v0}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    .line 116
    :cond_2
    if-nez v3, :cond_0

    .line 118
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/e/d;->b(I)V

    goto/16 :goto_0

    .line 114
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/cast/e/d;->g:Lcom/google/android/gms/http/GoogleHttpClient;

    invoke-virtual {v1}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    throw v0

    :catch_2
    move-exception v2

    goto :goto_2
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/gms/cast/e/d;->j:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/cast/e/e;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/cast/e/e;-><init>(Lcom/google/android/gms/cast/e/d;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 221
    return-void
.end method


# virtual methods
.method protected abstract a()Lcom/google/protobuf/a/f;
.end method

.method protected abstract a(I)V
.end method

.method protected abstract a([B)V
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/gms/cast/e/d;->b()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
