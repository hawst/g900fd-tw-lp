.class public Lcom/google/android/gms/games/jingle/PeerDiagnostics;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final mReliableChannelMetrics:Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;

.field private final mUnreliableChannelMetrics:Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;

    invoke-direct {v0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics;->mUnreliableChannelMetrics:Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;

    .line 12
    new-instance v0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;

    invoke-direct {v0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics;->mReliableChannelMetrics:Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;

    .line 14
    return-void
.end method


# virtual methods
.method public getReliableChannelMetrics()Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics;->mReliableChannelMetrics:Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;

    return-object v0
.end method

.method public getUnreliableChannelMetrics()Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics;->mUnreliableChannelMetrics:Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;

    return-object v0
.end method
