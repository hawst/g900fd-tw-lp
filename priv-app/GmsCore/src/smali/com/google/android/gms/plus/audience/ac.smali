.class public Lcom/google/android/gms/plus/audience/ac;
.super Lcom/google/android/gms/plus/audience/al;
.source "SourceFile"


# instance fields
.field i:Ljava/lang/String;

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:I

.field private w:I

.field private x:I

.field private y:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/al;-><init>()V

    .line 165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/ac;->u:Z

    .line 509
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/ac;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/gms/plus/audience/ac;->x:I

    return v0
.end method

.method protected static a(Ljava/lang/String;Ljava/lang/String;ZZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIIILjava/util/List;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 217
    invoke-static {p0, p1, p8, p9}, Lcom/google/android/gms/plus/audience/al;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 220
    const-string v2, "loadSuggested"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 221
    const-string v2, "loadGroups"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 222
    const-string v2, "loadCircles"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 223
    const-string v2, "loadPeople"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 224
    const-string v2, "requestCircleVisibility"

    invoke-virtual {v1, v2, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 225
    const-string v2, "description"

    invoke-virtual {v1, v2, p7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const-string v2, "headerVisible"

    invoke-virtual {v1, v2, p10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 227
    const-string v2, "includeSuggestions"

    invoke-virtual {v1, v2, p11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 228
    const-string v2, "maxSuggestedImages"

    move/from16 v0, p12

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 229
    const-string v2, "maxSuggestedListItems"

    move/from16 v0, p13

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 230
    const-string v2, "maxSuggestedDevice"

    move/from16 v0, p14

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 231
    if-eqz p15, :cond_0

    .line 232
    const-string v2, "excludedSuggestions"

    new-instance v3, Ljava/util/ArrayList;

    move-object/from16 v0, p15

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 236
    :cond_0
    return-object v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/audience/ac;
    .locals 18

    .prologue
    .line 202
    new-instance v17, Lcom/google/android/gms/plus/audience/ac;

    invoke-direct/range {v17 .. v17}, Lcom/google/android/gms/plus/audience/ac;-><init>()V

    .line 203
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    invoke-static/range {v1 .. v16}, Lcom/google/android/gms/plus/audience/ac;->a(Ljava/lang/String;Ljava/lang/String;ZZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIIILjava/util/List;)Landroid/os/Bundle;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/ac;->setArguments(Landroid/os/Bundle;)V

    .line 208
    return-object v17
.end method

.method static synthetic b(Lcom/google/android/gms/plus/audience/ac;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/gms/plus/audience/ac;->v:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/plus/audience/ac;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/gms/plus/audience/ac;->w:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/plus/audience/ac;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/ac;->s:Z

    return v0
.end method


# virtual methods
.method protected final e()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 356
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/ac;->o:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/gms/plus/audience/ac;->v:I

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/plus/audience/ac;->w:I

    if-lez v0, :cond_2

    .line 357
    :cond_0
    iget v0, p0, Lcom/google/android/gms/plus/audience/ac;->x:I

    if-lez v0, :cond_1

    .line 358
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v1, 0x4

    new-instance v2, Lcom/google/android/gms/plus/audience/ag;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/audience/ag;-><init>(Lcom/google/android/gms/plus/audience/ac;B)V

    invoke-virtual {v0, v1, v4, v2}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 360
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v1, 0x5

    new-instance v2, Lcom/google/android/gms/plus/audience/ak;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/audience/ak;-><init>(Lcom/google/android/gms/plus/audience/ac;B)V

    invoke-virtual {v0, v1, v4, v2}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 363
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/ac;->p:Z

    if-eqz v0, :cond_3

    .line 364
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/audience/ah;

    invoke-direct {v1, p0, v3}, Lcom/google/android/gms/plus/audience/ah;-><init>(Lcom/google/android/gms/plus/audience/ac;B)V

    invoke-virtual {v0, v3, v4, v1}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 367
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/ac;->q:Z

    if-eqz v0, :cond_4

    .line 368
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/gms/plus/audience/af;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/audience/af;-><init>(Lcom/google/android/gms/plus/audience/ac;B)V

    invoke-virtual {v0, v1, v4, v2}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 371
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/ac;->r:Z

    if-eqz v0, :cond_5

    .line 372
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/ac;->u:Z

    if-eqz v0, :cond_6

    .line 375
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v1, 0x3

    new-instance v2, Lcom/google/android/gms/plus/audience/aj;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/audience/aj;-><init>(Lcom/google/android/gms/plus/audience/ac;B)V

    invoke-virtual {v0, v1, v4, v2}, Landroid/support/v4/app/au;->b(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 381
    :cond_5
    :goto_0
    return-void

    .line 378
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/gms/plus/audience/ai;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/audience/ai;-><init>(Lcom/google/android/gms/plus/audience/ac;B)V

    invoke-virtual {v0, v1, v4, v2}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    goto :goto_0
.end method

.method protected synthetic f()Landroid/widget/BaseAdapter;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->i()Lcom/google/android/gms/plus/audience/o;

    move-result-object v0

    return-object v0
.end method

.method protected g()Lcom/google/android/gms/plus/audience/ae;
    .locals 1

    .prologue
    .line 314
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/ae;

    return-object v0
.end method

.method protected final h()Lcom/google/android/gms/plus/audience/ad;
    .locals 1

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/ad;

    return-object v0
.end method

.method protected i()Lcom/google/android/gms/plus/audience/o;
    .locals 10

    .prologue
    .line 323
    new-instance v0, Lcom/google/android/gms/plus/audience/o;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->j()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/al;->l:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/al;->m:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/gms/plus/audience/ac;->t:Z

    iget v6, p0, Lcom/google/android/gms/plus/audience/ac;->v:I

    iget v7, p0, Lcom/google/android/gms/plus/audience/ac;->w:I

    iget-object v8, p0, Lcom/google/android/gms/plus/audience/ac;->y:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->g()Lcom/google/android/gms/plus/audience/ae;

    move-result-object v9

    invoke-interface {v9}, Lcom/google/android/gms/plus/audience/ae;->i()Z

    move-result v9

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/plus/audience/o;-><init>(Landroid/content/Context;Lcom/google/android/gms/plus/audience/bg;Ljava/lang/String;Ljava/lang/String;ZIILjava/util/List;Z)V

    .line 328
    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/al;->n:Z

    if-eqz v1, :cond_0

    .line 329
    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/o;->j()V

    .line 331
    :cond_0
    return-object v0
.end method

.method protected final j()Lcom/google/android/gms/plus/audience/bg;
    .locals 1

    .prologue
    .line 335
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->g()Lcom/google/android/gms/plus/audience/ae;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/ae;->g()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 297
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/al;->onActivityCreated(Landroid/os/Bundle;)V

    .line 298
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->a()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 299
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->g()Lcom/google/android/gms/plus/audience/ae;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/ae;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/ad;

    check-cast v0, Lcom/google/android/gms/plus/audience/aw;

    .line 302
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->a()Landroid/widget/ListView;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/audience/AudienceSelectionListView;

    .line 303
    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 304
    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListView;->a(Lcom/google/android/gms/plus/audience/at;)V

    .line 305
    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListView;->a(Lcom/google/android/gms/plus/audience/as;)V

    .line 306
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->o()Landroid/widget/BaseAdapter;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/audience/o;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/plus/audience/o;->a(Landroid/view/View$OnClickListener;)V

    .line 307
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v0, v2, :cond_0

    .line 308
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListView;->setCacheColorHint(I)V

    .line 311
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 262
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/al;->onAttach(Landroid/app/Activity;)V

    .line 263
    instance-of v0, p1, Lcom/google/android/gms/plus/audience/ae;

    if-nez v0, :cond_0

    .line 264
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity must implement AudienceSelectionFragmentHost"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 267
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->g()Lcom/google/android/gms/plus/audience/ae;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/ae;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 268
    instance-of v0, p1, Lcom/google/android/gms/plus/audience/aw;

    if-nez v0, :cond_1

    .line 269
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity must extend AudienceSelectionScrollViewActivity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 273
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 277
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/al;->onCreate(Landroid/os/Bundle;)V

    .line 278
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 279
    const-string v1, "loadSuggested"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/plus/audience/ac;->o:Z

    .line 280
    const-string v1, "loadGroups"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/plus/audience/ac;->p:Z

    .line 281
    const-string v1, "loadCircles"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/plus/audience/ac;->q:Z

    .line 282
    const-string v1, "loadPeople"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/plus/audience/ac;->r:Z

    .line 283
    const-string v1, "requestCircleVisibility"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/plus/audience/ac;->s:Z

    .line 284
    const-string v1, "description"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/ac;->i:Ljava/lang/String;

    .line 285
    const-string v1, "headerVisible"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/plus/audience/ac;->t:Z

    .line 286
    const-string v1, "includeSuggestions"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/plus/audience/ac;->u:Z

    .line 287
    const-string v1, "maxSuggestedImages"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/plus/audience/ac;->v:I

    .line 288
    const-string v1, "maxSuggestedListItems"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/plus/audience/ac;->w:I

    .line 289
    const-string v1, "maxSuggestedDevice"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/plus/audience/ac;->x:I

    .line 290
    const-string v1, "excludedSuggestions"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 291
    const-string v1, "excludedSuggestions"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/ac;->y:Ljava/util/List;

    .line 293
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 250
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/al;->onStart()V

    .line 251
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->o()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/o;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/o;->j()V

    .line 252
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ac;->o()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/o;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/o;->k()V

    .line 257
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/al;->onStop()V

    .line 258
    return-void
.end method
