.class public final Lcom/google/android/gms/auth/be/proximity/registration/v3/a;
.super Lcom/google/android/gms/auth/be/proximity/registration/v3/h;
.source "SourceFile"


# static fields
.field private static final a:Landroid/util/SparseArray;

.field private static final b:Landroid/util/SparseArray;


# instance fields
.field private final c:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 39
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->a:Landroid/util/SparseArray;

    .line 40
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->b:Landroid/util/SparseArray;

    .line 42
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->a:Landroid/util/SparseArray;

    const/4 v1, 0x0

    const-string v2, "enabled"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 43
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->a:Landroid/util/SparseArray;

    const/4 v1, 0x1

    const-string v2, "disabled"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 44
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->a:Landroid/util/SparseArray;

    const/4 v1, 0x2

    const-string v2, "unknown"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 46
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->b:Landroid/util/SparseArray;

    const/16 v1, 0xa

    const-string v2, "enabled"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 47
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->b:Landroid/util/SparseArray;

    const/16 v1, 0xb

    const-string v2, "disabled"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 48
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->b:Landroid/util/SparseArray;

    const/16 v1, 0xc

    const-string v2, "unsupported"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 49
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;III)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/google/android/gms/auth/be/proximity/registration/v3/h;-><init>()V

    .line 77
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->c:Ljava/lang/String;

    .line 78
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->e:Ljava/lang/String;

    .line 79
    iput p3, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->f:I

    .line 80
    const v0, 0x6768a8

    iput v0, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->g:I

    .line 81
    iput p4, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->h:I

    .line 82
    iput p5, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->i:I

    .line 83
    return-void
.end method

.method public static a(II)Lcom/google/android/gms/auth/be/proximity/registration/v3/a;
    .locals 6

    .prologue
    .line 61
    new-instance v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v3}, Lcom/google/android/gms/common/util/au;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    move v4, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;-><init>(Ljava/lang/String;Ljava/lang/String;III)V

    return-object v0

    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2}, Lcom/google/android/gms/common/util/au;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public final a()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 111
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 112
    const-string v1, "type"

    const-string v2, "device_info"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 113
    const-string v1, "device_model"

    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 114
    const-string v1, "device_name"

    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 115
    const-string v1, "system_api_level"

    iget v2, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->f:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 116
    const-string v1, "gmscore_version"

    iget v2, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->g:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 117
    const-string v1, "screen_lock_status"

    sget-object v2, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->a:Landroid/util/SparseArray;

    iget v3, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->h:I

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 118
    const-string v1, "trust_agent_status"

    sget-object v2, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->b:Landroid/util/SparseArray;

    iget v3, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->i:I

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 119
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 124
    instance-of v1, p1, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;

    if-eqz v1, :cond_0

    .line 125
    check-cast p1, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;

    .line 126
    iget-object v1, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->f:I

    iget v2, p1, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->f:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->g:I

    iget v2, p1, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->g:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->h:I

    iget v2, p1, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->h:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->i:I

    iget v2, p1, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->i:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 133
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 138
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->i:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
