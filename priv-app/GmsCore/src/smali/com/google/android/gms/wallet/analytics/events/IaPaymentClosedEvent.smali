.class public Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;
.super Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/analytics/events/v;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final c:I

.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/e;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/analytics/events/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(IILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>()V

    .line 26
    iput-object p4, p0, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;->j:Ljava/lang/String;

    .line 27
    iput p1, p0, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;->c:I

    .line 28
    iput p2, p0, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;->d:I

    .line 29
    iput-object p3, p0, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;->a:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>(Landroid/os/Parcel;)V

    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;->c:I

    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;->d:I

    .line 36
    return-void
.end method

.method public static a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    .line 42
    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 43
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 54
    iget v0, p0, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 55
    iget v0, p0, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 56
    return-void
.end method
