.class public final Lcom/google/android/gms/drive/a/a/l;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/drive/a/a/l;


# instance fields
.field private final b:I

.field private final c:Z

.field private final d:Z

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 29
    new-instance v0, Lcom/google/android/gms/drive/a/a/l;

    const/4 v3, 0x1

    move v2, v1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/a/a/l;-><init>(IZZLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/a/a/l;->a:Lcom/google/android/gms/drive/a/a/l;

    return-void
.end method

.method private constructor <init>(IZZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput p1, p0, Lcom/google/android/gms/drive/a/a/l;->b:I

    .line 86
    iput-boolean p2, p0, Lcom/google/android/gms/drive/a/a/l;->c:Z

    .line 87
    iput-boolean p3, p0, Lcom/google/android/gms/drive/a/a/l;->d:Z

    .line 88
    iput-object p4, p0, Lcom/google/android/gms/drive/a/a/l;->e:Ljava/lang/String;

    .line 89
    iput-object p5, p0, Lcom/google/android/gms/drive/a/a/l;->f:Ljava/lang/String;

    .line 90
    return-void
.end method

.method public static a(IZZLjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/a/a/l;
    .locals 6

    .prologue
    .line 72
    invoke-static {p0}, Lcom/google/android/gms/drive/ad;->b(I)Z

    move-result v0

    const-string v1, "Invalid conflict strategy"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(ZLjava/lang/String;)V

    .line 75
    new-instance v0, Lcom/google/android/gms/drive/a/a/l;

    move v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/a/a/l;-><init>(IZZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/a/a/l;
    .locals 6

    .prologue
    .line 101
    const-string v0, "conflictStrategy"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "conflictStrategy"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 104
    :goto_0
    const-string v0, "notifyOnCompletion"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "notifyOnCompletion"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 108
    :goto_1
    const-string v0, "usesDefaultAccount"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 109
    const-string v0, "usesDefaultAccount"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 120
    :goto_2
    const-string v0, "operationTag"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "operationTag"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 122
    :goto_3
    const-string v0, "binderPackageName"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "binderPackageName"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 125
    :goto_4
    new-instance v0, Lcom/google/android/gms/drive/a/a/l;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/a/a/l;-><init>(IZZLjava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 101
    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/a/a/l;->a:Lcom/google/android/gms/drive/a/a/l;

    iget v1, v0, Lcom/google/android/gms/drive/a/a/l;->b:I

    goto :goto_0

    .line 104
    :cond_1
    sget-object v0, Lcom/google/android/gms/drive/a/a/l;->a:Lcom/google/android/gms/drive/a/a/l;

    iget-boolean v2, v0, Lcom/google/android/gms/drive/a/a/l;->c:Z

    goto :goto_1

    .line 112
    :cond_2
    const-string v0, "unresolvedAccountName"

    invoke-static {p0, v0}, Lcom/google/android/gms/drive/g/aa;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 114
    if-eqz v0, :cond_3

    .line 115
    const/4 v3, 0x0

    goto :goto_2

    .line 117
    :cond_3
    sget-object v0, Lcom/google/android/gms/drive/a/a/l;->a:Lcom/google/android/gms/drive/a/a/l;

    iget-boolean v3, v0, Lcom/google/android/gms/drive/a/a/l;->d:Z

    goto :goto_2

    .line 120
    :cond_4
    sget-object v0, Lcom/google/android/gms/drive/a/a/l;->a:Lcom/google/android/gms/drive/a/a/l;

    iget-object v4, v0, Lcom/google/android/gms/drive/a/a/l;->e:Ljava/lang/String;

    goto :goto_3

    .line 122
    :cond_5
    sget-object v0, Lcom/google/android/gms/drive/a/a/l;->a:Lcom/google/android/gms/drive/a/a/l;

    iget-object v5, v0, Lcom/google/android/gms/drive/a/a/l;->f:Ljava/lang/String;

    goto :goto_4
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/google/android/gms/drive/a/a/l;->c:Z

    return v0
.end method

.method final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/l;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Lorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 137
    const-string v0, "conflictStrategy"

    iget v1, p0, Lcom/google/android/gms/drive/a/a/l;->b:I

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 138
    const-string v0, "notifyOnCompletion"

    iget-boolean v1, p0, Lcom/google/android/gms/drive/a/a/l;->c:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 139
    const-string v0, "usesDefaultAccount"

    iget-boolean v1, p0, Lcom/google/android/gms/drive/a/a/l;->d:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 140
    const-string v0, "operationTag"

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/l;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 141
    const-string v0, "binderPackageName"

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/l;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 142
    return-void
.end method

.method final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/l;->f:Ljava/lang/String;

    return-object v0
.end method

.method final d()Z
    .locals 1

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/google/android/gms/drive/a/a/l;->d:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/google/android/gms/drive/a/a/l;->b:I

    invoke-static {v0}, Lcom/google/android/gms/drive/ad;->a(I)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 201
    if-ne p0, p1, :cond_1

    .line 209
    :cond_0
    :goto_0
    return v0

    .line 204
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 205
    goto :goto_0

    .line 208
    :cond_3
    check-cast p1, Lcom/google/android/gms/drive/a/a/l;

    .line 209
    iget v2, p0, Lcom/google/android/gms/drive/a/a/l;->b:I

    iget v3, p1, Lcom/google/android/gms/drive/a/a/l;->b:I

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/google/android/gms/drive/a/a/l;->c:Z

    iget-boolean v3, p1, Lcom/google/android/gms/drive/a/a/l;->c:Z

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/google/android/gms/drive/a/a/l;->d:Z

    iget-boolean v3, p1, Lcom/google/android/gms/drive/a/a/l;->d:Z

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a/l;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/a/l;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a/l;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/a/l;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 190
    iget v0, p0, Lcom/google/android/gms/drive/a/a/l;->b:I

    .line 191
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/drive/a/a/l;->c:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 192
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/google/android/gms/drive/a/a/l;->d:Z

    if-eqz v3, :cond_0

    move v2, v1

    :cond_0
    add-int/2addr v0, v2

    .line 193
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/l;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/l;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 194
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a/l;->f:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/l;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    .line 196
    return v0

    :cond_2
    move v0, v2

    .line 191
    goto :goto_0

    :cond_3
    move v0, v1

    .line 193
    goto :goto_1
.end method
