.class final Lcom/google/android/gms/drive/metadata/sync/c/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:[J

.field private b:I


# direct methods
.method public constructor <init>(I)V
    .locals 4

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-array v0, p1, [J

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/k;->a:[J

    .line 25
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/k;->a:[J

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/k;->b:I

    .line 27
    return-void
.end method


# virtual methods
.method public final a(I)J
    .locals 2

    .prologue
    .line 58
    if-ltz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/k;->a:[J

    array-length v0, v0

    if-ge p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Offset is out ot bounds"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 60
    iget v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/k;->b:I

    sub-int/2addr v0, p1

    .line 61
    if-gez v0, :cond_0

    .line 62
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/c/k;->a:[J

    array-length v1, v1

    add-int/2addr v0, v1

    .line 64
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/c/k;->a:[J

    aget-wide v0, v1, v0

    return-wide v0

    .line 58
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(J)J
    .locals 5

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/k;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/k;->b:I

    .line 43
    iget v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/k;->b:I

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/c/k;->a:[J

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/k;->b:I

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/k;->a:[J

    iget v1, p0, Lcom/google/android/gms/drive/metadata/sync/c/k;->b:I

    aget-wide v0, v0, v1

    .line 47
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/c/k;->a:[J

    iget v3, p0, Lcom/google/android/gms/drive/metadata/sync/c/k;->b:I

    aput-wide p1, v2, v3

    .line 48
    return-wide v0
.end method
