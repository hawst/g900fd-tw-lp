.class public final Lcom/google/android/gms/people/service/a/a/o;
.super Lcom/google/android/gms/people/service/a/a/g;
.source "SourceFile"


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/people/service/a/a/g;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;)V

    .line 24
    iput-object p5, p0, Lcom/google/android/gms/people/service/a/a/o;->g:Ljava/lang/String;

    .line 25
    iput-object p6, p0, Lcom/google/android/gms/people/service/a/a/o;->h:Ljava/lang/String;

    .line 29
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    const-string v0, "PeopleService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "loadOwnerCoverPhoto: account="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pageId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " minimumWidth="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    :cond_0
    return-void
.end method


# virtual methods
.method protected final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[cover: account="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/a/o;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/a/o;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final g()Lcom/google/android/gms/people/f/a;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 37
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/a/o;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->n()Lcom/google/android/gms/people/sync/d;

    move-result-object v7

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/a/o;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/a/o;->h:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/google/android/gms/people/sync/d;->a()V

    iget-object v0, v7, Lcom/google/android/gms/people/sync/d;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/a/a;->a(Landroid/content/Context;)I

    move-result v5

    new-instance v0, Lcom/google/android/gms/people/sync/ap;

    iget-object v4, v7, Lcom/google/android/gms/people/sync/d;->a:Landroid/content/Context;

    invoke-direct {v0, v4, v1, v3}, Lcom/google/android/gms/people/sync/ap;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/f/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    :goto_0
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v3

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/sync/ap;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/f/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    goto :goto_0

    :cond_1
    iget-object v0, v7, Lcom/google/android/gms/people/sync/d;->b:Lcom/google/android/gms/people/f/f;

    invoke-static {v6}, Lcom/google/android/gms/people/f/h;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/google/android/gms/people/f/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v6, v5}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, v7, Lcom/google/android/gms/people/sync/d;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/people/service/m;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/m;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v0, v4}, Lcom/google/android/gms/people/service/m;->a(Ljava/lang/String;Z)[B

    move-result-object v4

    if-eqz v4, :cond_2

    sget-object v0, Lcom/google/android/gms/people/service/a/a/e;->i:[B

    if-eq v4, v0, :cond_2

    sget-object v0, Lcom/google/android/gms/people/service/a/a/e;->j:[B

    if-eq v4, v0, :cond_2

    iget-object v0, v7, Lcom/google/android/gms/people/sync/d;->b:Lcom/google/android/gms/people/f/f;

    invoke-static {v6}, Lcom/google/android/gms/people/f/h;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/people/f/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BI)Z

    :cond_2
    iget-object v0, v7, Lcom/google/android/gms/people/sync/d;->b:Lcom/google/android/gms/people/f/f;

    invoke-static {v6}, Lcom/google/android/gms/people/f/h;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/f/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/people/f/a;

    move-result-object v0

    goto :goto_1
.end method
