.class final Lcom/google/android/gms/drive/ui/picker/t;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/ui/picker/m;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/ui/picker/m;Landroid/content/Context;II[Lcom/google/android/gms/drive/ui/picker/ab;)V
    .locals 0

    .prologue
    .line 316
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/t;->a:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 319
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 320
    invoke-static {}, Lcom/google/android/gms/drive/ui/picker/ab;->c()[Lcom/google/android/gms/drive/ui/picker/ab;

    move-result-object v0

    aget-object v2, v0, p1

    .line 321
    sget v0, Lcom/google/android/gms/j;->mp:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 322
    iget v3, v2, Lcom/google/android/gms/drive/ui/picker/ab;->g:I

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 324
    sget v0, Lcom/google/android/gms/j;->mq:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 325
    iget v3, v2, Lcom/google/android/gms/drive/ui/picker/ab;->h:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 326
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/t;->a:Lcom/google/android/gms/drive/ui/picker/m;

    iget v2, v2, Lcom/google/android/gms/drive/ui/picker/ab;->h:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/ui/picker/m;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 327
    return-object v1
.end method
