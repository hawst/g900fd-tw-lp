.class final Lcom/google/android/gms/ads/internal/request/a/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/ads/internal/request/a/k;

.field final synthetic b:Lcom/google/android/gms/ads/internal/d/a;

.field final synthetic c:Lcom/google/android/gms/ads/internal/i/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/request/a/k;Lcom/google/android/gms/ads/internal/d/a;Lcom/google/android/gms/ads/internal/i/a;)V
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/a/f;->a:Lcom/google/android/gms/ads/internal/request/a/k;

    iput-object p2, p0, Lcom/google/android/gms/ads/internal/request/a/f;->b:Lcom/google/android/gms/ads/internal/d/a;

    iput-object p3, p0, Lcom/google/android/gms/ads/internal/request/a/f;->c:Lcom/google/android/gms/ads/internal/i/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/a/f;->a:Lcom/google/android/gms/ads/internal/request/a/k;

    const-string v1, "destroyAdWebView must be called on the main thread."

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/request/a/k;->e:Lcom/google/android/gms/ads/internal/p/a;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/request/a/k;->e:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/p/a;->destroy()V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/ads/internal/request/a/k;->e:Lcom/google/android/gms/ads/internal/p/a;

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/a/f;->b:Lcom/google/android/gms/ads/internal/d/a;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/d/a;->f:Z

    if-eqz v0, :cond_1

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/a/f;->a:Lcom/google/android/gms/ads/internal/request/a/k;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/request/a/k;->a()Lcom/google/android/gms/ads/internal/util/a/a;

    move-result-object v0

    .line 250
    new-instance v1, Lcom/google/android/gms/ads/internal/request/a/g;

    invoke-direct {v1, p0}, Lcom/google/android/gms/ads/internal/request/a/g;-><init>(Lcom/google/android/gms/ads/internal/request/a/f;)V

    new-instance v2, Lcom/google/android/gms/ads/internal/util/a/c;

    invoke-direct {v2}, Lcom/google/android/gms/ads/internal/util/a/c;-><init>()V

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/ads/internal/util/a/a;->a(Lcom/google/android/gms/ads/internal/util/a/e;Lcom/google/android/gms/ads/internal/util/a/b;)V

    .line 261
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/a/f;->c:Lcom/google/android/gms/ads/internal/i/a;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/a/f;->a:Lcom/google/android/gms/ads/internal/request/a/k;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/request/a/k;->a()Lcom/google/android/gms/ads/internal/util/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/i/a;->a(Lcom/google/android/gms/ads/internal/util/a/a;)V

    .line 263
    :cond_1
    return-void
.end method
