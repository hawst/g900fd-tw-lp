.class public final Lcom/google/android/gms/games/g/ap;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7980
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 7981
    iput v1, p0, Lcom/google/android/gms/games/g/ap;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/ap;->b:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/games/g/ap;->c:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/ap;->d:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/games/g/ap;->e:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/ap;->cachedSize:I

    .line 7982
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 8065
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 8066
    iget v1, p0, Lcom/google/android/gms/games/g/ap;->a:I

    if-eqz v1, :cond_0

    .line 8067
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/g/ap;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8070
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/g/ap;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 8071
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/g/ap;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8074
    :cond_1
    iget v1, p0, Lcom/google/android/gms/games/g/ap;->c:I

    if-eqz v1, :cond_2

    .line 8075
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/games/g/ap;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8078
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/g/ap;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 8079
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/games/g/ap;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8082
    :cond_3
    iget v1, p0, Lcom/google/android/gms/games/g/ap;->e:I

    if-eqz v1, :cond_4

    .line 8083
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/games/g/ap;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8086
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 7996
    if-ne p1, p0, :cond_1

    .line 8026
    :cond_0
    :goto_0
    return v0

    .line 7999
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/ap;

    if-nez v2, :cond_2

    move v0, v1

    .line 8000
    goto :goto_0

    .line 8002
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/ap;

    .line 8003
    iget v2, p0, Lcom/google/android/gms/games/g/ap;->a:I

    iget v3, p1, Lcom/google/android/gms/games/g/ap;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 8004
    goto :goto_0

    .line 8006
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/ap;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 8007
    iget-object v2, p1, Lcom/google/android/gms/games/g/ap;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 8008
    goto :goto_0

    .line 8010
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/g/ap;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/ap;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 8011
    goto :goto_0

    .line 8013
    :cond_5
    iget v2, p0, Lcom/google/android/gms/games/g/ap;->c:I

    iget v3, p1, Lcom/google/android/gms/games/g/ap;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 8014
    goto :goto_0

    .line 8016
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/g/ap;->d:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 8017
    iget-object v2, p1, Lcom/google/android/gms/games/g/ap;->d:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 8018
    goto :goto_0

    .line 8020
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/games/g/ap;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/ap;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 8021
    goto :goto_0

    .line 8023
    :cond_8
    iget v2, p0, Lcom/google/android/gms/games/g/ap;->e:I

    iget v3, p1, Lcom/google/android/gms/games/g/ap;->e:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 8024
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 8031
    iget v0, p0, Lcom/google/android/gms/games/g/ap;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 8033
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/ap;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 8035
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/games/g/ap;->c:I

    add-int/2addr v0, v2

    .line 8036
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/g/ap;->d:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 8038
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/g/ap;->e:I

    add-int/2addr v0, v1

    .line 8039
    return v0

    .line 8033
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/ap;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 8036
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/g/ap;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 7928
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/g/ap;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/ap;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/android/gms/games/g/ap;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/ap;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    iput v0, p0, Lcom/google/android/gms/games/g/ap;->e:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 8045
    iget v0, p0, Lcom/google/android/gms/games/g/ap;->a:I

    if-eqz v0, :cond_0

    .line 8046
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/g/ap;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8048
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/ap;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 8049
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/g/ap;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 8051
    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/g/ap;->c:I

    if-eqz v0, :cond_2

    .line 8052
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/games/g/ap;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8054
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/ap;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 8055
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/games/g/ap;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 8057
    :cond_3
    iget v0, p0, Lcom/google/android/gms/games/g/ap;->e:I

    if-eqz v0, :cond_4

    .line 8058
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/gms/games/g/ap;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8060
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 8061
    return-void
.end method
