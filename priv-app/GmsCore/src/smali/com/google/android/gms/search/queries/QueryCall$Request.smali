.class public Lcom/google/android/gms/search/queries/QueryCall$Request;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/queries/k;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:[Ljava/lang/String;

.field public d:I

.field public e:I

.field public f:Lcom/google/android/gms/appdatasearch/QuerySpecification;

.field final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    new-instance v0, Lcom/google/android/gms/search/queries/k;

    invoke-direct {v0}, Lcom/google/android/gms/search/queries/k;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/queries/QueryCall$Request;->CREATOR:Lcom/google/android/gms/search/queries/k;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/queries/QueryCall$Request;->g:I

    .line 75
    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput p1, p0, Lcom/google/android/gms/search/queries/QueryCall$Request;->g:I

    .line 96
    iput-object p2, p0, Lcom/google/android/gms/search/queries/QueryCall$Request;->a:Ljava/lang/String;

    .line 97
    iput-object p3, p0, Lcom/google/android/gms/search/queries/QueryCall$Request;->b:Ljava/lang/String;

    .line 98
    iput-object p4, p0, Lcom/google/android/gms/search/queries/QueryCall$Request;->c:[Ljava/lang/String;

    .line 99
    iput p5, p0, Lcom/google/android/gms/search/queries/QueryCall$Request;->d:I

    .line 100
    iput p6, p0, Lcom/google/android/gms/search/queries/QueryCall$Request;->e:I

    .line 101
    iput-object p7, p0, Lcom/google/android/gms/search/queries/QueryCall$Request;->f:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    .line 102
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lcom/google/android/gms/search/queries/QueryCall$Request;->CREATOR:Lcom/google/android/gms/search/queries/k;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/google/android/gms/search/queries/QueryCall$Request;->CREATOR:Lcom/google/android/gms/search/queries/k;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/search/queries/k;->a(Lcom/google/android/gms/search/queries/QueryCall$Request;Landroid/os/Parcel;I)V

    .line 114
    return-void
.end method
