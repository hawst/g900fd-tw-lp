.class final Lcom/google/android/gms/backup/ar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Landroid/accounts/Account;

.field final synthetic b:Lcom/google/android/gms/backup/SetBackupAccountActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/backup/SetBackupAccountActivity;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/google/android/gms/backup/ar;->b:Lcom/google/android/gms/backup/SetBackupAccountActivity;

    iput-object p2, p0, Lcom/google/android/gms/backup/ar;->a:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 162
    invoke-static {p2}, Lcom/google/android/gms/backup/al;->a(Landroid/os/IBinder;)Lcom/google/android/gms/backup/ak;

    move-result-object v0

    .line 165
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/backup/ar;->a:Landroid/accounts/Account;

    invoke-interface {v0, v1}, Lcom/google/android/gms/backup/ak;->a(Landroid/accounts/Account;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/backup/ar;->b:Lcom/google/android/gms/backup/SetBackupAccountActivity;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 170
    return-void

    .line 166
    :catch_0
    move-exception v0

    .line 167
    invoke-static {}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->a()Lcom/google/android/gms/auth/d/a;

    move-result-object v1

    const-string v2, "Failed to set backup account!"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0

    .prologue
    .line 173
    return-void
.end method
