.class public Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:J

.field private final b:Lcom/google/android/gms/wallet/shared/service/ServerResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/q;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/service/ow/q;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-wide p1, p0, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->a:J

    .line 25
    iput-object p3, p0, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->b:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->a:J

    .line 20
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->b:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    .line 21
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->a:J

    return-wide v0
.end method

.method public final b()Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->b:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->b:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->writeToParcel(Landroid/os/Parcel;I)V

    .line 60
    return-void
.end method
