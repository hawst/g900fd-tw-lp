.class public Lcom/google/android/gms/car/CarFacet;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field public b:I

.field public c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/google/android/gms/car/ei;

    invoke-direct {v0}, Lcom/google/android/gms/car/ei;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarFacet;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IILjava/lang/String;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput p1, p0, Lcom/google/android/gms/car/CarFacet;->a:I

    .line 42
    iput p2, p0, Lcom/google/android/gms/car/CarFacet;->b:I

    .line 43
    iput-object p3, p0, Lcom/google/android/gms/car/CarFacet;->c:Ljava/lang/String;

    .line 44
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/google/android/gms/car/CarFacet;->a:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 53
    invoke-static {p0, p1}, Lcom/google/android/gms/car/ei;->a(Lcom/google/android/gms/car/CarFacet;Landroid/os/Parcel;)V

    .line 54
    return-void
.end method
