.class final Lcom/google/android/gms/drive/b/b/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/b/b/o;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/drive/b/b/n;

.field private final e:Lcom/google/android/gms/drive/b/b;

.field private final f:Lcom/google/android/gms/drive/b/b/d;

.field private final g:Lcom/google/android/gms/drive/b/b/p;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lcom/google/android/gms/drive/ai;->T:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/upload/drive/v2beta/files?uploadType=multipart&setModifiedDate=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/b/b/g;->a:Ljava/lang/String;

    .line 44
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lcom/google/android/gms/drive/ai;->T:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/upload/drive/v2beta/files/%s?uploadType=multipart&setModifiedDate=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/b/b/g;->b:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/b/b/n;Lcom/google/android/gms/drive/b/b;Lcom/google/android/gms/drive/b/b/d;Lcom/google/android/gms/drive/b/b/p;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/google/android/gms/drive/b/b/g;->c:Landroid/content/Context;

    .line 75
    iput-object p2, p0, Lcom/google/android/gms/drive/b/b/g;->d:Lcom/google/android/gms/drive/b/b/n;

    .line 76
    iput-object p3, p0, Lcom/google/android/gms/drive/b/b/g;->e:Lcom/google/android/gms/drive/b/b;

    .line 77
    iput-object p4, p0, Lcom/google/android/gms/drive/b/b/g;->f:Lcom/google/android/gms/drive/b/b/d;

    .line 78
    iput-object p5, p0, Lcom/google/android/gms/drive/b/b/g;->g:Lcom/google/android/gms/drive/b/b/p;

    .line 79
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/drive/b/b/n;Lcom/google/android/gms/drive/b/b;Lcom/google/android/gms/drive/b/b/d;Lcom/google/android/gms/drive/b/b/p;)Lcom/google/android/gms/drive/b/b/g;
    .locals 6

    .prologue
    .line 63
    iget-wide v0, p1, Lcom/google/android/gms/drive/b/b/n;->e:J

    const-wide/32 v2, 0x400000

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 64
    const/4 v0, 0x0

    .line 66
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/b/b/g;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/b/b/g;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/b/b/n;Lcom/google/android/gms/drive/b/b;Lcom/google/android/gms/drive/b/b/d;Lcom/google/android/gms/drive/b/b/p;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/b/b/l;)Lcom/google/android/gms/drive/d/e;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 87
    iget-object v1, p0, Lcom/google/android/gms/drive/b/b/g;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v3, v1, Lcom/google/android/gms/drive/b/b/n;->d:Lcom/google/android/gms/drive/DriveId;

    .line 88
    invoke-interface {p1, v3}, Lcom/google/android/gms/drive/b/b/l;->a(Lcom/google/android/gms/drive/DriveId;)V

    .line 93
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/drive/b/b/g;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-boolean v1, v1, Lcom/google/android/gms/drive/b/b/n;->c:Z

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/gms/drive/b/b/g;->a:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/drive/b/b/p;->a(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v1

    move-object v2, v1

    .line 94
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/drive/b/b/g;->f:Lcom/google/android/gms/drive/b/b/d;

    iget-object v4, p0, Lcom/google/android/gms/drive/b/b/g;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/drive/b/b/g;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-boolean v1, v1, Lcom/google/android/gms/drive/b/b/n;->c:Z

    if-eqz v1, :cond_2

    const-string v1, "POST"

    :goto_1
    iget-object v5, p0, Lcom/google/android/gms/drive/b/b/g;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v5, v5, Lcom/google/android/gms/drive/b/b/n;->a:Lcom/google/android/gms/drive/auth/g;

    iget-object v6, p0, Lcom/google/android/gms/drive/b/b/g;->e:Lcom/google/android/gms/drive/b/b;

    invoke-static {v4, v2, v1, v5, v6}, Lcom/google/android/gms/drive/b/b/d;->a(Landroid/content/Context;Ljava/net/URL;Ljava/lang/String;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/b/b;)Lcom/google/android/gms/drive/b/b/c;
    :try_end_0
    .catch Lcom/google/android/gms/drive/b/b/i; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v1

    .line 100
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/drive/b/b/g;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v2, v2, Lcom/google/android/gms/drive/b/b/n;->i:Ljava/lang/String;

    .line 101
    if-eqz v2, :cond_0

    .line 102
    const-string v4, "If-Match"

    invoke-virtual {v1, v4, v2}, Lcom/google/android/gms/drive/b/b/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/drive/b/b/g;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v2, v2, Lcom/google/android/gms/drive/b/b/n;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/b/b/c;->a(Ljava/lang/String;)J

    .line 106
    iget-object v2, p0, Lcom/google/android/gms/drive/b/b/g;->d:Lcom/google/android/gms/drive/b/b/n;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/b/b/n;->a()Ljava/io/InputStream;
    :try_end_1
    .catch Lcom/google/android/gms/drive/b/b/i; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v0

    .line 107
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/drive/b/b/g;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v2, v2, Lcom/google/android/gms/drive/b/b/n;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/drive/b/b/c;->a(Ljava/lang/String;Ljava/io/InputStream;)J

    .line 109
    invoke-virtual {v1}, Lcom/google/android/gms/drive/b/b/c;->a()I

    move-result v2

    .line 111
    const-string v4, "MultipartUploader"

    const-string v5, "HTTP upload status %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 112
    const/16 v4, 0xc9

    if-eq v2, v4, :cond_4

    const/16 v4, 0xc8

    if-eq v2, v4, :cond_4

    .line 113
    iget-object v3, p0, Lcom/google/android/gms/drive/b/b/g;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v3, v3, Lcom/google/android/gms/drive/b/b/n;->k:Ljava/lang/Integer;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/gms/drive/b/b/g;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v3, v3, Lcom/google/android/gms/drive/b/b/n;->k:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v2, v3, :cond_3

    .line 115
    new-instance v2, Lcom/google/android/gms/drive/b/b/j;

    const-string v3, "Conflict uploading"

    invoke-direct {v2, v3}, Lcom/google/android/gms/drive/b/b/j;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catch Lcom/google/android/gms/drive/b/b/i; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 127
    :catch_0
    move-exception v2

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    :goto_2
    :try_start_3
    new-instance v2, Ljava/lang/InterruptedException;

    invoke-direct {v2}, Ljava/lang/InterruptedException;-><init>()V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 131
    :catchall_0
    move-exception v2

    move-object v9, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v9

    :goto_3
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 132
    invoke-static {v2}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    throw v0

    .line 93
    :cond_1
    :try_start_4
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v2, Lcom/google/android/gms/drive/b/b/g;->b:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v3}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v1, v2, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/b/b/p;->a(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v1

    move-object v2, v1

    goto/16 :goto_0

    .line 94
    :cond_2
    const-string v1, "PUT"
    :try_end_4
    .catch Lcom/google/android/gms/drive/b/b/i; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto/16 :goto_1

    .line 117
    :cond_3
    :try_start_5
    new-instance v3, Lcom/google/android/gms/drive/b/b/k;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Upload failed HTTP status "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/google/android/gms/drive/b/b/k;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_5
    .catch Lcom/google/android/gms/drive/b/b/i; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 128
    :catch_1
    move-exception v2

    move-object v9, v2

    move-object v2, v0

    move-object v0, v9

    .line 129
    :goto_4
    :try_start_6
    new-instance v3, Lcom/google/android/gms/drive/b/b/k;

    const-string v4, "Upload exception"

    invoke-direct {v3, v4, v0}, Lcom/google/android/gms/drive/b/b/k;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 131
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 119
    :cond_4
    :try_start_7
    invoke-interface {p1, v3}, Lcom/google/android/gms/drive/b/b/l;->b(Lcom/google/android/gms/drive/DriveId;)V

    .line 120
    iget-object v2, p0, Lcom/google/android/gms/drive/b/b/g;->g:Lcom/google/android/gms/drive/b/b/p;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/b/b/c;->b()Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/drive/b/b/p;->a(Ljava/io/InputStream;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v2

    .line 122
    const-string v3, "X-Server-Object-Version"

    invoke-virtual {v1, v3}, Lcom/google/android/gms/drive/b/b/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 125
    new-instance v4, Lcom/google/android/gms/drive/d/e;

    invoke-direct {v4, v2, v3}, Lcom/google/android/gms/drive/d/e;-><init>(Lcom/google/android/gms/drive/internal/model/File;Ljava/lang/String;)V
    :try_end_7
    .catch Lcom/google/android/gms/drive/b/b/i; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 131
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 132
    invoke-static {v0}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    return-object v4

    .line 131
    :catchall_2
    move-exception v1

    move-object v2, v0

    move-object v9, v0

    move-object v0, v1

    move-object v1, v9

    goto :goto_3

    :catchall_3
    move-exception v2

    move-object v9, v2

    move-object v2, v0

    move-object v0, v9

    goto :goto_3

    :catchall_4
    move-exception v2

    move-object v9, v2

    move-object v2, v0

    move-object v0, v9

    goto :goto_3

    .line 128
    :catch_2
    move-exception v1

    move-object v2, v0

    move-object v9, v0

    move-object v0, v1

    move-object v1, v9

    goto :goto_4

    :catch_3
    move-exception v2

    move-object v9, v2

    move-object v2, v0

    move-object v0, v9

    goto :goto_4

    .line 127
    :catch_4
    move-exception v1

    move-object v1, v0

    goto/16 :goto_2

    :catch_5
    move-exception v2

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    goto/16 :goto_2
.end method
