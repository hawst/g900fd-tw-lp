.class public Lcom/google/android/gms/smart_profile/PeopleListActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 26
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 28
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/PeopleListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    .line 29
    const-string v0, "relationship"

    invoke-virtual {v7, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 30
    const-string v1, "target_person_id"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 31
    const-string v2, "com.google.android.gms.people.smart_profile.VIEWER_ACCOUNT_NAME"

    invoke-virtual {v7, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 32
    const-string v3, "com.google.android.gms.people.smart_profile.VIEWER_PAGE_ID"

    invoke-virtual {v7, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 33
    const-string v4, "com.google.android.gms.people.smart_profile.QUALIFIED_ID"

    invoke-virtual {v7, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 34
    const-string v5, "com.google.android.gms.people.smart_profile.APPLICATION_ID"

    invoke-virtual {v7, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 37
    packed-switch v0, :pswitch_data_0

    .line 48
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown people relationship: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 40
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/PeopleListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v8, Lcom/google/android/gms/p;->xh:I

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 51
    :goto_0
    sget v8, Lcom/google/android/gms/l;->cO:I

    invoke-virtual {p0, v8}, Lcom/google/android/gms/smart_profile/PeopleListActivity;->setContentView(I)V

    .line 53
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v8

    .line 54
    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/support/v7/app/a;->b(Z)V

    .line 55
    invoke-virtual {v8, v6}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    .line 56
    invoke-virtual {v8}, Landroid/support/v7/app/a;->f()V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/PeopleListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v9, Lcom/google/android/gms/h;->cW:I

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v8, v6}, Landroid/support/v7/app/a;->a(Landroid/graphics/drawable/Drawable;)V

    .line 58
    invoke-virtual {v7}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/smart_profile/bf;->a(Landroid/os/Bundle;)Ljava/lang/Integer;

    move-result-object v6

    .line 59
    if-nez v6, :cond_1

    .line 60
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/PeopleListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget-object v7, Landroid/graphics/drawable/GradientDrawable$Orientation;->BOTTOM_TOP:Landroid/graphics/drawable/GradientDrawable$Orientation;

    invoke-static {v7, v6}, Lcom/google/android/gms/smart_profile/bf;->a(Landroid/graphics/drawable/GradientDrawable$Orientation;Landroid/content/res/Resources;)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v6

    invoke-virtual {v8, v6}, Landroid/support/v7/app/a;->b(Landroid/graphics/drawable/Drawable;)V

    .line 66
    :goto_1
    if-nez p1, :cond_0

    .line 67
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/smart_profile/al;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/smart_profile/al;

    move-result-object v0

    .line 69
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/PeopleListActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    sget v2, Lcom/google/android/gms/j;->gq:I

    const-string v3, "peopleListFragment"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 73
    :cond_0
    return-void

    .line 44
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/PeopleListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v8, Lcom/google/android/gms/p;->xg:I

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 63
    :cond_1
    new-instance v7, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct {v7, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v8, v7}, Landroid/support/v7/app/a;->b(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
