.class public final Lcom/google/android/gms/appstate/service/AppStateAndroidService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/HashMap;

.field private static final b:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/appstate/service/AppStateAndroidService;->a:Ljava/util/HashMap;

    .line 42
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/appstate/service/AppStateAndroidService;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 151
    return-void
.end method

.method public static a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/appstate/service/a;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 103
    sget-object v3, Lcom/google/android/gms/appstate/service/AppStateAndroidService;->b:Ljava/lang/Object;

    monitor-enter v3

    .line 104
    :try_start_0
    sget-object v0, Lcom/google/android/gms/appstate/service/AppStateAndroidService;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 105
    if-nez v0, :cond_0

    .line 106
    monitor-exit v3

    move-object v0, v2

    .line 121
    :goto_0
    return-object v0

    .line 113
    :cond_0
    sget-object v1, Lcom/google/android/gms/appstate/service/AppStateAndroidService;->a:Ljava/util/HashMap;

    invoke-static {v1, p0}, Lcom/google/android/gms/common/util/ah;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/server/ClientContext;

    .line 116
    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/server/ClientContext;->c(Lcom/google/android/gms/common/server/ClientContext;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 118
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appstate/service/a;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 121
    :cond_1
    monitor-exit v3

    move-object v0, v2

    goto :goto_0
.end method

.method static a()V
    .locals 2

    .prologue
    .line 142
    sget-object v1, Lcom/google/android/gms/appstate/service/AppStateAndroidService;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 143
    :try_start_0
    sget-object v0, Lcom/google/android/gms/appstate/service/AppStateAndroidService;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 144
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/service/a;)V
    .locals 3

    .prologue
    .line 133
    sget-object v1, Lcom/google/android/gms/appstate/service/AppStateAndroidService;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 134
    :try_start_0
    sget-object v0, Lcom/google/android/gms/appstate/service/AppStateAndroidService;->a:Ljava/util/HashMap;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, p0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/appstate/service/AppStateAndroidService;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/gms/appstate/service/AppStateAndroidService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 46
    const-string v0, "com.google.android.gms.appstate.service.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    new-instance v0, Lcom/google/android/gms/appstate/service/b;

    invoke-direct {v0, p0, p0}, Lcom/google/android/gms/appstate/service/b;-><init>(Lcom/google/android/gms/appstate/service/AppStateAndroidService;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/appstate/service/b;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 49
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
