.class public Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;
.super Lcom/google/android/gms/googlehelp/helpactivities/r;
.source "SourceFile"


# instance fields
.field private b:I

.field private c:Landroid/widget/Spinner;

.field private d:Lcom/google/android/gms/googlehelp/c/a/b;

.field private e:Landroid/widget/EditText;

.field private f:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/helpactivities/r;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 30
    iget v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->b:I

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_0
    iget v3, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->b:I

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->w()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->d:Lcom/google/android/gms/googlehelp/c/a/b;

    iget-object v4, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->c:Landroid/widget/Spinner;

    invoke-virtual {v4}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/googlehelp/c/a/b;->a(I)Lcom/google/ad/a/a/m;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v2, v4, Lcom/google/ad/a/a/m;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v0, :cond_5

    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->q()Ljava/lang/String;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v2, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, ""

    :goto_2
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v4, Lcom/google/ad/a/a/m;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c(Ljava/lang/String;)V

    :cond_0
    :goto_3
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->e(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->finish()V

    if-eqz v0, :cond_7

    invoke-static {p0, v3}, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    invoke-static {p0}, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->a(Landroid/content/Context;)V

    :cond_1
    :goto_4
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1

    :cond_4
    const-string v2, ":"

    goto :goto_2

    :cond_5
    if-eqz v1, :cond_0

    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->s()Ljava/lang/String;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v2, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, ""

    :goto_5
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v4, Lcom/google/ad/a/a/m;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->d(Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    const-string v2, ":"

    goto :goto_5

    :cond_7
    if-eqz v1, :cond_1

    invoke-static {p0, v3}, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    invoke-static {p0}, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->a(Landroid/content/Context;)V

    goto :goto_4
.end method

.method static synthetic b(Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;)Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->c:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->e:Landroid/widget/EditText;

    return-object v0
.end method

.method private e()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 138
    new-instance v0, Lcom/google/android/gms/googlehelp/helpactivities/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/helpactivities/o;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;)V

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v4, 0x0

    const/4 v2, -0x1

    const/4 v3, 0x1

    .line 49
    invoke-super {p0, p1}, Lcom/google/android/gms/googlehelp/helpactivities/r;->onCreate(Landroid/os/Bundle;)V

    .line 50
    if-eqz p1, :cond_3

    const-string v0, "EXTRA_ESCALATION_TYPE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->b:I

    .line 52
    :cond_0
    sget v0, Lcom/google/android/gms/p;->ph:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 56
    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 57
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v1

    .line 58
    invoke-virtual {v1, v0}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    .line 59
    const-string v0, ""

    invoke-virtual {v1, v0}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    .line 61
    sget v0, Lcom/google/android/gms/l;->ck:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->setContentView(I)V

    .line 63
    sget v0, Lcom/google/android/gms/j;->iP:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->c:Landroid/widget/Spinner;

    .line 64
    sget v0, Lcom/google/android/gms/j;->iN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->e:Landroid/widget/EditText;

    .line 66
    sget v0, Lcom/google/android/gms/j;->iQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->f:Landroid/widget/TextView;

    .line 67
    sget v0, Lcom/google/android/gms/j;->iM:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 68
    sget v1, Lcom/google/android/gms/j;->iO:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 70
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 71
    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->g()Landroid/accounts/Account;

    move-result-object v5

    if-nez v5, :cond_1

    .line 72
    const-string v5, "GOOGLEHELP_RealtimeSupportClassifierActivity"

    const-string v6, "Account is required for realtime support."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->finish()V

    .line 75
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->g()Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->x()Lcom/google/ad/a/a/n;

    move-result-object v5

    .line 79
    if-eqz v5, :cond_4

    move v2, v3

    .line 81
    :goto_0
    if-eqz v2, :cond_5

    .line 82
    iget-boolean v0, v5, Lcom/google/ad/a/a/n;->c:Z

    if-eqz v0, :cond_2

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->e:Landroid/widget/EditText;

    sget v6, Lcom/google/android/gms/p;->pi:I

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setHint(I)V

    .line 86
    :cond_2
    new-instance v0, Lcom/google/android/gms/googlehelp/c/a/b;

    iget-object v6, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->c:Landroid/widget/Spinner;

    invoke-direct {v0, p0, v6, v5}, Lcom/google/android/gms/googlehelp/c/a/b;-><init>(Landroid/content/Context;Landroid/widget/Spinner;Lcom/google/ad/a/a/n;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->d:Lcom/google/android/gms/googlehelp/c/a/b;

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->d:Lcom/google/android/gms/googlehelp/c/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/a/b;->a()V

    .line 93
    :goto_1
    iget v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->b:I

    if-ne v0, v3, :cond_6

    sget v0, Lcom/google/android/gms/p;->pj:I

    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 98
    iget-object v6, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->f:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->e()Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    if-eqz v2, :cond_7

    iget-boolean v0, v5, Lcom/google/ad/a/a/n;->c:Z

    if-eqz v0, :cond_7

    .line 102
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->c:Landroid/widget/Spinner;

    new-instance v1, Lcom/google/android/gms/googlehelp/helpactivities/q;

    invoke-direct {v1, p0}, Lcom/google/android/gms/googlehelp/helpactivities/q;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->e:Landroid/widget/EditText;

    new-instance v1, Lcom/google/android/gms/googlehelp/helpactivities/p;

    invoke-direct {v1, p0}, Lcom/google/android/gms/googlehelp/helpactivities/p;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 114
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 115
    return-void

    .line 50
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_ESCALATION_TYPE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->b:I

    iget v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->b:I

    if-ne v0, v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No escalation type provided, but is required."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v2, v4

    .line 79
    goto/16 :goto_0

    .line 90
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->c:Landroid/widget/Spinner;

    invoke-virtual {v0, v7}, Landroid/widget/Spinner;->setVisibility(I)V

    goto :goto_1

    .line 93
    :cond_6
    sget v0, Lcom/google/android/gms/p;->pk:I

    goto :goto_2

    .line 110
    :cond_7
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->e()Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/widget/TextView;Landroid/view/View$OnClickListener;)V

    goto :goto_3
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 148
    const-string v0, "EXTRA_HELP_CONFIG"

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 149
    const-string v0, "EXTRA_ESCALATION_TYPE"

    iget v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 150
    invoke-super {p0, p1}, Lcom/google/android/gms/googlehelp/helpactivities/r;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 151
    return-void
.end method
