.class public final Lcom/google/android/gms/wallet/service/ia/d;
.super Lcom/google/android/gms/wallet/service/ia/m;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/wallet/service/ia/m;

.field private final b:Lcom/google/android/gms/wallet/service/r;

.field private final c:Lcom/google/android/gms/wallet/cache/j;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/service/ia/m;Lcom/google/android/gms/wallet/service/r;Lcom/google/android/gms/wallet/cache/j;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/ia/m;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ia/d;->a:Lcom/google/android/gms/wallet/service/ia/m;

    .line 40
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ia/d;->b:Lcom/google/android/gms/wallet/service/r;

    .line 41
    iput-object p3, p0, Lcom/google/android/gms/wallet/service/ia/d;->c:Lcom/google/android/gms/wallet/cache/j;

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 5

    .prologue
    .line 185
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;->a()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/service/r;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 187
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;->b()Lcom/google/checkout/inapp/proto/s;

    move-result-object v2

    .line 188
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/d;->b:Lcom/google/android/gms/wallet/service/r;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 190
    if-eqz v0, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-object v0

    .line 193
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/d;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 194
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v3

    const/16 v4, 0x1b

    if-ne v3, v4, :cond_0

    .line 196
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/ia/d;->b:Lcom/google/android/gms/wallet/service/r;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v4

    invoke-virtual {v3, v1, v2, v4}, Lcom/google/android/gms/wallet/service/r;->b(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 5

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/d;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 211
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v1

    const/16 v2, 0x1d

    if-ne v1, v2, :cond_0

    .line 212
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;->a()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 214
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ia/d;->b:Lcom/google/android/gms/wallet/service/r;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;->b()Lcom/google/checkout/inapp/proto/u;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V

    .line 217
    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/d;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 88
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;->b()Lcom/google/checkout/inapp/proto/z;

    move-result-object v2

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/d;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v1

    .line 90
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v0

    const/16 v3, 0xb

    if-eq v0, v3, :cond_0

    move-object v0, v1

    .line 102
    :goto_0
    return-object v0

    .line 93
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;

    .line 94
    iget-object v3, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v3, :cond_1

    .line 95
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/ia/d;->c:Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;->a()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v5

    iget-object v6, v0, Lcom/google/checkout/inapp/proto/Service$CreateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/gms/wallet/cache/j;->a(Landroid/accounts/Account;ILcom/google/checkout/inapp/proto/a/b;)V

    .line 99
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/wallet/service/r;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 101
    iget-object v4, p0, Lcom/google/android/gms/wallet/service/ia/d;->b:Lcom/google/android/gms/wallet/service/r;

    invoke-virtual {v4, v3, v2, v0}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V

    move-object v0, v1

    .line 102
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 47
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;->b()Lcom/google/checkout/inapp/proto/aa;

    move-result-object v2

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/d;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v1

    .line 49
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v0

    const/4 v3, 0x7

    if-eq v0, v3, :cond_0

    move-object v0, v1

    .line 61
    :goto_0
    return-object v0

    .line 52
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;

    .line 53
    iget-object v3, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lcom/google/checkout/inapp/proto/j;

    if-eqz v3, :cond_1

    .line 54
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/ia/d;->c:Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;->a()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v5

    iget-object v6, v0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lcom/google/checkout/inapp/proto/j;

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/gms/wallet/cache/j;->a(Landroid/accounts/Account;ILcom/google/checkout/inapp/proto/j;)V

    .line 58
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/wallet/service/r;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 60
    iget-object v4, p0, Lcom/google/android/gms/wallet/service/ia/d;->b:Lcom/google/android/gms/wallet/service/r;

    invoke-virtual {v4, v3, v2, v0}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V

    move-object v0, v1

    .line 61
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 5

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/d;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    .line 162
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;->a()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 164
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ia/d;->b:Lcom/google/android/gms/wallet/service/r;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;->b()Lcom/google/checkout/inapp/proto/ab;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V

    .line 167
    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/d;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/d;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 6

    .prologue
    .line 223
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->a()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/service/r;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 225
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->b()Lcom/google/checkout/inapp/proto/ah;

    move-result-object v3

    .line 226
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/d;->b:Lcom/google/android/gms/wallet/service/r;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 228
    if-eqz v0, :cond_0

    .line 246
    :goto_0
    return-object v0

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/d;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v1

    .line 232
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v0

    const/16 v4, 0x1f

    if-ne v0, v4, :cond_2

    .line 233
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/ai;

    .line 234
    iget v4, v0, Lcom/google/checkout/inapp/proto/ai;->a:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    .line 235
    iget-object v4, p0, Lcom/google/android/gms/wallet/service/ia/d;->b:Lcom/google/android/gms/wallet/service/r;

    invoke-virtual {v4, v2, v3, v0}, Lcom/google/android/gms/wallet/service/r;->b(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V

    .line 236
    iget-object v2, v3, Lcom/google/checkout/inapp/proto/ah;->a:[I

    if-eqz v2, :cond_1

    iget-object v2, v3, Lcom/google/checkout/inapp/proto/ah;->a:[I

    array-length v2, v2

    if-eqz v2, :cond_1

    iget-object v2, v3, Lcom/google/checkout/inapp/proto/ah;->a:[I

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 240
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ia/d;->c:Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v4

    invoke-virtual {v2, v3, v4, v0}, Lcom/google/android/gms/wallet/cache/j;->a(Landroid/accounts/Account;ILcom/google/checkout/inapp/proto/ai;)V

    :cond_2
    move-object v0, v1

    .line 246
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 5

    .prologue
    .line 129
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->a()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/service/r;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 131
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->b()Lcom/google/checkout/inapp/proto/ak;

    move-result-object v2

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/d;->b:Lcom/google/android/gms/wallet/service/r;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 134
    if-eqz v0, :cond_1

    .line 141
    :cond_0
    :goto_0
    return-object v0

    .line 137
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/d;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 139
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/ia/d;->b:Lcom/google/android/gms/wallet/service/r;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v4

    invoke-virtual {v3, v1, v2, v4}, Lcom/google/android/gms/wallet/service/r;->b(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 5

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/d;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 148
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 149
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;->a()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 151
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ia/d;->b:Lcom/google/android/gms/wallet/service/r;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;->b()Lcom/google/checkout/inapp/proto/am;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V

    .line 154
    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 8

    .prologue
    .line 108
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->b()Lcom/google/checkout/inapp/proto/ao;

    move-result-object v2

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/d;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v1

    .line 110
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v0

    const/16 v3, 0xc

    if-eq v0, v3, :cond_0

    move-object v0, v1

    .line 123
    :goto_0
    return-object v0

    .line 113
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;

    .line 114
    iget-object v3, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v3, :cond_1

    .line 115
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/ia/d;->c:Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->a()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v5

    iget-object v6, v0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v7, v2, Lcom/google/checkout/inapp/proto/ao;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/google/android/gms/wallet/cache/j;->a(Landroid/accounts/Account;ILcom/google/checkout/inapp/proto/a/b;Ljava/lang/String;)V

    .line 120
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/wallet/service/r;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 122
    iget-object v4, p0, Lcom/google/android/gms/wallet/service/ia/d;->b:Lcom/google/android/gms/wallet/service/r;

    invoke-virtual {v4, v3, v2, v0}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V

    move-object v0, v1

    .line 123
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 8

    .prologue
    .line 67
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;->b()Lcom/google/checkout/inapp/proto/ap;

    move-result-object v2

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/d;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v1

    .line 69
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v0

    const/16 v3, 0x9

    if-eq v0, v3, :cond_0

    move-object v0, v1

    .line 82
    :goto_0
    return-object v0

    .line 72
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;

    .line 73
    iget-object v3, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->a:Lcom/google/checkout/inapp/proto/j;

    if-eqz v3, :cond_1

    .line 74
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/ia/d;->c:Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;->a()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v5

    iget-object v6, v0, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->a:Lcom/google/checkout/inapp/proto/j;

    iget-object v7, v2, Lcom/google/checkout/inapp/proto/ap;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/google/android/gms/wallet/cache/j;->a(Landroid/accounts/Account;ILcom/google/checkout/inapp/proto/j;Ljava/lang/String;)V

    .line 79
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/wallet/service/r;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 81
    iget-object v4, p0, Lcom/google/android/gms/wallet/service/ia/d;->b:Lcom/google/android/gms/wallet/service/r;

    invoke-virtual {v4, v3, v2, v0}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V

    move-object v0, v1

    .line 82
    goto :goto_0
.end method
