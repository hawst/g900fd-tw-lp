.class public final Lcom/google/android/gms/people/sync/r;
.super Lcom/google/android/gms/common/f/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/people/sync/v;

.field private final b:Lcom/google/android/gms/people/sync/u;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 76
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.google.android.gms.people"

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/common/f/a;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 50
    new-instance v0, Lcom/google/android/gms/people/sync/v;

    invoke-direct {v0}, Lcom/google/android/gms/people/sync/v;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/r;->a:Lcom/google/android/gms/people/sync/v;

    .line 79
    const-string v0, "PeopleSync"

    const-string v1, "PeopleSyncAdapter created."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-static {p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    .line 82
    return-void
.end method

.method private a(Landroid/accounts/Account;Landroid/os/Bundle;ILandroid/content/SyncResult;ZZZZZLcom/google/android/gms/people/sync/s;)V
    .locals 25

    .prologue
    .line 215
    const/4 v14, 0x0

    .line 216
    const/4 v13, 0x0

    .line 217
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 218
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v20

    .line 219
    const-wide/16 v18, 0x0

    .line 221
    const/16 v16, 0x1

    .line 222
    const/4 v15, 0x0

    .line 226
    const/4 v2, 0x1

    move/from16 v0, p3

    if-ne v0, v2, :cond_0

    .line 227
    :try_start_0
    sget-object v2, Lcom/google/android/gms/people/a/a;->u:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 229
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/people/sync/u;->b(Landroid/os/Bundle;)I
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 234
    :goto_0
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 236
    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 242
    :cond_0
    :goto_1
    if-nez p8, :cond_a

    .line 245
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/u;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_2

    const/4 v4, 0x3

    invoke-static {v4}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "PeopleSync"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Just made a mutation.  Waiting for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ms..."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 249
    :cond_2
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->a:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 265
    if-eqz p6, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/people/sync/u;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_4

    :cond_3
    if-nez p9, :cond_4

    .line 267
    new-instance v10, Lcom/google/android/gms/people/sync/y;

    invoke-direct {v10}, Lcom/google/android/gms/people/sync/y;-><init>()V
    :try_end_3
    .catch Lcom/google/android/gms/auth/q; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 269
    :try_start_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/people/sync/r;->a:Lcom/google/android/gms/people/sync/v;

    const/4 v9, 0x0

    move/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v8, p5

    move-object/from16 v11, p2

    move/from16 v12, p7

    invoke-static/range {v2 .. v12}, Lcom/google/android/gms/people/sync/z;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILandroid/content/SyncResult;Lcom/google/android/gms/people/sync/v;ZLcom/google/android/gms/people/sync/s;Lcom/google/android/gms/people/sync/y;Landroid/os/Bundle;Z)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 273
    :try_start_5
    move-object/from16 v0, v17

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 274
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Stats="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v3, v4, v5, v6}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->a:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 283
    if-eqz p5, :cond_5

    .line 284
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/c/f;->c(Ljava/lang/String;)Z

    move-result v2

    .line 286
    if-eqz v2, :cond_5

    .line 289
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "Account opted out from G+."

    invoke-static {v2, v3, v4, v5, v6}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->d(Landroid/content/Context;)V

    .line 295
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->a:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 299
    if-nez p3, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 301
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/people/a/a;->t:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v6, v2

    invoke-virtual {v3, v4, v6, v7}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;J)Ljava/util/List;

    move-result-object v2

    .line 306
    :goto_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;
    :try_end_5
    .catch Lcom/google/android/gms/auth/q; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    move-result-object v22

    move v5, v14

    :goto_3
    :try_start_6
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 307
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 308
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v7, "***Sync start***"

    invoke-static {v2, v3, v6, v4, v7}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    new-instance v10, Lcom/google/android/gms/people/sync/y;

    invoke-direct {v10}, Lcom/google/android/gms/people/sync/y;-><init>()V
    :try_end_6
    .catch Lcom/google/android/gms/auth/q; {:try_start_6 .. :try_end_6} :catch_1e
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_6 .. :try_end_6} :catch_1c
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    .line 312
    add-int/lit8 v14, v5, 0x1

    .line 314
    :try_start_7
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/people/sync/r;->a:Lcom/google/android/gms/people/sync/v;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v12, 0x0

    move/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v11, p2

    invoke-static/range {v2 .. v12}, Lcom/google/android/gms/people/sync/z;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILandroid/content/SyncResult;Lcom/google/android/gms/people/sync/v;ZLcom/google/android/gms/people/sync/s;Lcom/google/android/gms/people/sync/y;Landroid/os/Bundle;Z)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 319
    :try_start_8
    move-object/from16 v0, v17

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 320
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Stats="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v3, v5, v4, v6}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->a:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/v;->c()V

    move v5, v14

    .line 324
    goto :goto_3

    .line 231
    :pswitch_0
    sget-object v2, Lcom/google/android/gms/people/a/a;->v:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto/16 :goto_0

    .line 247
    :catch_0
    move-exception v2

    new-instance v2, Lcom/google/android/gms/people/sync/x;

    invoke-direct {v2}, Lcom/google/android/gms/people/sync/x;-><init>()V

    throw v2
    :try_end_8
    .catch Lcom/google/android/gms/auth/q; {:try_start_8 .. :try_end_8} :catch_1
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 340
    :catch_1
    move-exception v2

    move-object v12, v13

    move v5, v14

    .line 342
    :goto_4
    const/4 v3, 0x0

    .line 343
    :try_start_9
    throw v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 351
    :catchall_0
    move-exception v2

    move v4, v3

    move-object v13, v12

    move v14, v5

    move-object v3, v2

    move v2, v15

    :goto_5
    :try_start_a
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/people/sync/r;->a(Landroid/accounts/Account;Z)V

    .line 353
    if-eqz v4, :cond_c

    .line 354
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, v4, Lcom/google/android/gms/people/sync/u;->b:Lcom/google/android/gms/people/a/c;

    invoke-virtual {v2, v5}, Lcom/google/android/gms/people/a/c;->b(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v6, v2, 0x1

    iget-object v2, v4, Lcom/google/android/gms/people/sync/u;->b:Lcom/google/android/gms/people/a/c;

    invoke-virtual {v2, v5, v6}, Lcom/google/android/gms/people/a/c;->b(Ljava/lang/String;I)V

    sget-object v2, Lcom/google/android/gms/people/a/a;->I:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lt v6, v2, :cond_6

    const-string v2, "Too many failures."

    invoke-virtual {v4, v5, v2}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    move-object/from16 v0, p4

    iput-boolean v2, v0, Landroid/content/SyncResult;->tooManyRetries:Z

    :cond_6
    sget-object v2, Lcom/google/android/gms/people/a/a;->J:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lt v6, v2, :cond_7

    const-string v2, "Too many failures.  Disabling sync."

    invoke-virtual {v4, v5, v2}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v4, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v2

    const-string v5, "com.google.android.gms.people"

    const/4 v6, 0x0

    invoke-virtual {v4, v2, v5, v6}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 362
    :cond_7
    :goto_6
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_a
    .catch Lcom/google/android/gms/auth/q; {:try_start_a .. :try_end_a} :catch_5
    .catch Lcom/android/volley/ac; {:try_start_a .. :try_end_a} :catch_18
    .catch Lcom/google/android/gms/people/sync/a/g; {:try_start_a .. :try_end_a} :catch_15
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a .. :try_end_a} :catch_12
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_a .. :try_end_a} :catch_f
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_c

    move-result-wide v4

    sub-long v10, v4, v20

    :try_start_b
    throw v3
    :try_end_b
    .catch Lcom/google/android/gms/auth/q; {:try_start_b .. :try_end_b} :catch_2
    .catch Lcom/android/volley/ac; {:try_start_b .. :try_end_b} :catch_19
    .catch Lcom/google/android/gms/people/sync/a/g; {:try_start_b .. :try_end_b} :catch_16
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_b} :catch_13
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_b .. :try_end_b} :catch_10
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_d

    .line 371
    :catch_2
    move-exception v8

    move-object v12, v13

    move v5, v14

    .line 372
    :goto_7
    const-string v2, "PeopleSync"

    const-string v3, "Cannot authenticate"

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, v3, v4, v8}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 373
    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v14, 0x1

    add-long/2addr v6, v14

    iput-wide v6, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 375
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "***Auth error***"

    invoke-static {v2, v3, v4, v6, v7}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    const/4 v7, 0x3

    move-object/from16 v4, p1

    move-object/from16 v6, p2

    move-object/from16 v9, v17

    invoke-virtual/range {v3 .. v12}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;ILandroid/os/Bundle;ILjava/lang/Exception;Ljava/util/List;JLcom/google/android/gms/people/d/p;)V

    .line 451
    :cond_8
    :goto_8
    return-void

    .line 273
    :catchall_1
    move-exception v2

    :try_start_c
    move-object/from16 v0, v17

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 274
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Stats="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v4, v5, v6, v7}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v2
    :try_end_c
    .catch Lcom/google/android/gms/auth/q; {:try_start_c .. :try_end_c} :catch_1
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 344
    :catch_3
    move-exception v2

    .line 345
    :goto_9
    const/4 v3, 0x1

    .line 347
    const/4 v4, 0x0

    .line 348
    :try_start_d
    throw v2
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 351
    :catchall_2
    move-exception v2

    move-object/from16 v23, v2

    move v2, v3

    move-object/from16 v3, v23

    goto/16 :goto_5

    .line 304
    :cond_9
    :try_start_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/sync/u;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    goto/16 :goto_2

    .line 319
    :catchall_3
    move-exception v2

    move-object/from16 v0, v17

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 320
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v5, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Stats="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v5, v6, v4, v7}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v2
    :try_end_e
    .catch Lcom/google/android/gms/auth/q; {:try_start_e .. :try_end_e} :catch_1
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    .line 351
    :catchall_4
    move-exception v2

    move-object v3, v2

    move/from16 v4, v16

    move v2, v15

    goto/16 :goto_5

    :cond_a
    move v5, v14

    .line 329
    :cond_b
    if-nez p7, :cond_11

    .line 332
    :try_start_f
    new-instance v2, Lcom/google/android/gms/people/sync/a/a;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/people/sync/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 334
    new-instance v12, Lcom/google/android/gms/people/d/p;

    invoke-direct {v12}, Lcom/google/android/gms/people/d/p;-><init>()V
    :try_end_f
    .catch Lcom/google/android/gms/auth/q; {:try_start_f .. :try_end_f} :catch_1e
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_f .. :try_end_f} :catch_1c
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    .line 335
    :try_start_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/r;->a:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v2, v3, v12}, Lcom/google/android/gms/people/sync/a/a;->a(Lcom/google/android/gms/people/sync/v;Lcom/google/android/gms/people/d/p;)Lcom/google/android/gms/people/d/p;

    .line 337
    :goto_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->a:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/v;->c()V
    :try_end_10
    .catch Lcom/google/android/gms/auth/q; {:try_start_10 .. :try_end_10} :catch_1f
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_10 .. :try_end_10} :catch_1d
    .catchall {:try_start_10 .. :try_end_10} :catchall_6

    .line 339
    const/4 v2, 0x0

    :try_start_11
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/people/sync/r;->a(Landroid/accounts/Account;Z)V

    .line 353
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/sync/u;->c(Ljava/lang/String;)V

    .line 362
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_11
    .catch Lcom/google/android/gms/auth/q; {:try_start_11 .. :try_end_11} :catch_1b
    .catch Lcom/android/volley/ac; {:try_start_11 .. :try_end_11} :catch_6
    .catch Lcom/google/android/gms/people/sync/a/g; {:try_start_11 .. :try_end_11} :catch_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11 .. :try_end_11} :catch_8
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_11 .. :try_end_11} :catch_9
    .catch Ljava/lang/RuntimeException; {:try_start_11 .. :try_end_11} :catch_a

    move-result-wide v2

    sub-long v10, v2, v20

    .line 365
    :try_start_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v2, Lcom/google/android/gms/people/sync/u;->b:Lcom/google/android/gms/people/a/c;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/people/a/c;->a(Ljava/lang/String;I)V

    .line 368
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object/from16 v4, p1

    move-object/from16 v6, p2

    move-object/from16 v9, v17

    invoke-virtual/range {v3 .. v12}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;ILandroid/os/Bundle;ILjava/lang/Exception;Ljava/util/List;JLcom/google/android/gms/people/d/p;)V
    :try_end_12
    .catch Lcom/google/android/gms/auth/q; {:try_start_12 .. :try_end_12} :catch_4
    .catch Lcom/android/volley/ac; {:try_start_12 .. :try_end_12} :catch_1a
    .catch Lcom/google/android/gms/people/sync/a/g; {:try_start_12 .. :try_end_12} :catch_17
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_12 .. :try_end_12} :catch_14
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_12 .. :try_end_12} :catch_11
    .catch Ljava/lang/RuntimeException; {:try_start_12 .. :try_end_12} :catch_e

    goto/16 :goto_8

    .line 371
    :catch_4
    move-exception v8

    goto/16 :goto_7

    .line 359
    :cond_c
    :try_start_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/people/sync/u;->c(Ljava/lang/String;)V
    :try_end_13
    .catch Lcom/google/android/gms/auth/q; {:try_start_13 .. :try_end_13} :catch_5
    .catch Lcom/android/volley/ac; {:try_start_13 .. :try_end_13} :catch_18
    .catch Lcom/google/android/gms/people/sync/a/g; {:try_start_13 .. :try_end_13} :catch_15
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_13 .. :try_end_13} :catch_12
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_13 .. :try_end_13} :catch_f
    .catch Ljava/lang/RuntimeException; {:try_start_13 .. :try_end_13} :catch_c

    goto/16 :goto_6

    .line 371
    :catch_5
    move-exception v8

    move-wide/from16 v10, v18

    move-object v12, v13

    move v5, v14

    goto/16 :goto_7

    .line 382
    :catch_6
    move-exception v7

    move-wide/from16 v9, v18

    move-object v11, v12

    move v14, v5

    .line 383
    :goto_b
    const-string v2, "PeopleSync"

    const-string v3, "Network request failed"

    invoke-static {v2, v3, v7}, Lcom/google/android/gms/people/f/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 386
    invoke-static {v7}, Lcom/google/android/gms/people/f/n;->a(Lcom/android/volley/ac;)V

    .line 388
    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v12, 0x1

    add-long/2addr v4, v12

    iput-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    .line 390
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "***VolleyError***: "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Lcom/google/android/gms/people/f/n;->a(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 393
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v7}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v4, Lcom/google/android/gms/people/sync/u;->b:Lcom/google/android/gms/people/a/c;

    invoke-virtual {v2, v5}, Lcom/google/android/gms/people/a/c;->a(Ljava/lang/String;)I

    move-result v3

    invoke-static {v7}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/Exception;)I

    move-result v6

    invoke-static {v6}, Lcom/google/android/gms/people/a/a;->a(I)I

    move-result v2

    invoke-static {v6}, Lcom/google/android/gms/people/a/a;->b(I)Z

    move-result v6

    const/4 v8, 0x3

    invoke-static {v8}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v8

    if-eqz v8, :cond_d

    const-string v8, "PeopleSync"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "backoff: lb="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " bos="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " exp="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v8, v12}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    if-lez v3, :cond_10

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    if-eqz v6, :cond_10

    mul-int/lit8 v2, v2, 0x2

    move v3, v2

    :goto_c
    sget-object v2, Lcom/google/android/gms/people/a/a;->E:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v3, v4, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    const-string v6, "PeopleSync"

    const/4 v8, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "backoff="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v3, v6, v5, v8, v12}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, v4, Lcom/google/android/gms/people/sync/u;->b:Lcom/google/android/gms/people/a/c;

    invoke-virtual {v3, v5, v2}, Lcom/google/android/gms/people/a/c;->a(Ljava/lang/String;I)V

    if-lez v2, :cond_e

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v12, 0x3e8

    div-long/2addr v4, v12

    int-to-long v12, v2

    add-long/2addr v4, v12

    const-string v3, "PeopleSync"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "Delaying "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    int-to-double v12, v2

    const-wide v18, 0x40ac200000000000L    # 3600.0

    div-double v12, v12, v18

    invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " hours"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p4

    iget-wide v2, v0, Landroid/content/SyncResult;->delayUntil:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    move-object/from16 v0, p4

    iput-wide v2, v0, Landroid/content/SyncResult;->delayUntil:J

    .line 396
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    const/4 v6, 0x4

    move-object/from16 v3, p1

    move v4, v14

    move-object/from16 v5, p2

    move-object/from16 v8, v17

    invoke-virtual/range {v2 .. v11}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;ILandroid/os/Bundle;ILjava/lang/Exception;Ljava/util/List;JLcom/google/android/gms/people/d/p;)V

    goto/16 :goto_8

    .line 400
    :catch_7
    move-exception v7

    move-wide/from16 v9, v18

    move-object v11, v12

    move v14, v5

    .line 405
    :goto_d
    const-string v2, "PeopleSync"

    const-string v3, "ContactsProvider operation failed"

    invoke-static {v2, v3, v7}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 406
    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v12, 0x1

    add-long/2addr v4, v12

    iput-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    .line 407
    const/4 v2, 0x1

    move-object/from16 v0, p4

    iput-boolean v2, v0, Landroid/content/SyncResult;->databaseError:Z

    .line 409
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "***CP2 error***"

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 412
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    const/4 v6, 0x5

    move-object/from16 v3, p1

    move v4, v14

    move-object/from16 v5, p2

    move-object/from16 v8, v17

    invoke-virtual/range {v2 .. v11}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;ILandroid/os/Bundle;ILjava/lang/Exception;Ljava/util/List;JLcom/google/android/gms/people/d/p;)V

    goto/16 :goto_8

    .line 416
    :catch_8
    move-exception v7

    move-wide/from16 v9, v18

    move-object v11, v12

    move v14, v5

    .line 417
    :goto_e
    const-string v2, "PeopleSync"

    const-string v3, "Database operation failed"

    invoke-static {v2, v3, v7}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 418
    const/4 v2, 0x1

    move-object/from16 v0, p4

    iput-boolean v2, v0, Landroid/content/SyncResult;->databaseError:Z

    .line 420
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "***DB error***"

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 425
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/people/f/n;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 426
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    const/4 v6, 0x6

    move-object/from16 v3, p1

    move v4, v14

    move-object/from16 v5, p2

    move-object/from16 v8, v17

    invoke-virtual/range {v2 .. v11}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;ILandroid/os/Bundle;ILjava/lang/Exception;Ljava/util/List;JLcom/google/android/gms/people/d/p;)V

    goto/16 :goto_8

    .line 431
    :catch_9
    move-exception v8

    move-wide/from16 v10, v18

    .line 433
    :goto_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    const/4 v7, 0x2

    move-object/from16 v4, p1

    move-object/from16 v6, p2

    move-object/from16 v9, v17

    invoke-virtual/range {v3 .. v12}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;ILandroid/os/Bundle;ILjava/lang/Exception;Ljava/util/List;JLcom/google/android/gms/people/d/p;)V

    .line 436
    throw v8

    .line 437
    :catch_a
    move-exception v7

    move-wide/from16 v9, v18

    move-object v11, v12

    move v14, v5

    .line 438
    :goto_10
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "***Unknown error***"

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 442
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/people/f/n;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 443
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    const/16 v6, 0x64

    move-object/from16 v3, p1

    move v4, v14

    move-object/from16 v5, p2

    move-object/from16 v8, v17

    invoke-virtual/range {v2 .. v11}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;ILandroid/os/Bundle;ILjava/lang/Exception;Ljava/util/List;JLcom/google/android/gms/people/d/p;)V

    .line 448
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, v7, v3}, Lcom/google/android/gms/people/f/n;->a(Landroid/content/Context;Ljava/lang/RuntimeException;Ljava/lang/String;)V

    goto/16 :goto_8

    :catch_b
    move-exception v2

    goto/16 :goto_1

    .line 437
    :catch_c
    move-exception v7

    move-wide/from16 v9, v18

    move-object v11, v13

    goto :goto_10

    :catch_d
    move-exception v7

    move-wide/from16 v23, v10

    move-wide/from16 v9, v23

    move-object v11, v13

    goto :goto_10

    :catch_e
    move-exception v7

    move v14, v5

    move-wide/from16 v23, v10

    move-wide/from16 v9, v23

    move-object v11, v12

    goto :goto_10

    .line 431
    :catch_f
    move-exception v8

    move-wide/from16 v10, v18

    move-object v12, v13

    move v5, v14

    goto :goto_f

    :catch_10
    move-exception v8

    move-object v12, v13

    move v5, v14

    goto :goto_f

    :catch_11
    move-exception v8

    goto :goto_f

    .line 416
    :catch_12
    move-exception v7

    move-wide/from16 v9, v18

    move-object v11, v13

    goto/16 :goto_e

    :catch_13
    move-exception v7

    move-wide/from16 v23, v10

    move-wide/from16 v9, v23

    move-object v11, v13

    goto/16 :goto_e

    :catch_14
    move-exception v7

    move v14, v5

    move-wide/from16 v23, v10

    move-wide/from16 v9, v23

    move-object v11, v12

    goto/16 :goto_e

    .line 400
    :catch_15
    move-exception v7

    move-wide/from16 v9, v18

    move-object v11, v13

    goto/16 :goto_d

    :catch_16
    move-exception v7

    move-wide/from16 v23, v10

    move-wide/from16 v9, v23

    move-object v11, v13

    goto/16 :goto_d

    :catch_17
    move-exception v7

    move v14, v5

    move-wide/from16 v23, v10

    move-wide/from16 v9, v23

    move-object v11, v12

    goto/16 :goto_d

    .line 382
    :catch_18
    move-exception v7

    move-wide/from16 v9, v18

    move-object v11, v13

    goto/16 :goto_b

    :catch_19
    move-exception v7

    move-wide/from16 v23, v10

    move-wide/from16 v9, v23

    move-object v11, v13

    goto/16 :goto_b

    :catch_1a
    move-exception v7

    move v14, v5

    move-wide/from16 v23, v10

    move-wide/from16 v9, v23

    move-object v11, v12

    goto/16 :goto_b

    .line 371
    :catch_1b
    move-exception v8

    move-wide/from16 v10, v18

    goto/16 :goto_7

    .line 351
    :catchall_5
    move-exception v2

    move-object v3, v2

    move/from16 v4, v16

    move v14, v5

    move v2, v15

    goto/16 :goto_5

    :catchall_6
    move-exception v2

    move-object v3, v2

    move/from16 v4, v16

    move-object v13, v12

    move v14, v5

    move v2, v15

    goto/16 :goto_5

    .line 344
    :catch_1c
    move-exception v2

    move v14, v5

    goto/16 :goto_9

    :catch_1d
    move-exception v2

    move-object v13, v12

    move v14, v5

    goto/16 :goto_9

    .line 340
    :catch_1e
    move-exception v2

    move-object v12, v13

    goto/16 :goto_4

    :catch_1f
    move-exception v2

    goto/16 :goto_4

    :cond_10
    move v3, v2

    goto/16 :goto_c

    :cond_11
    move-object v12, v13

    goto/16 :goto_a

    .line 229
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private a(Landroid/accounts/Account;Z)V
    .locals 5

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/sync/u;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 456
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 457
    invoke-virtual {p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/people/service/h;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;

    move-result-object v3

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    if-eqz p2, :cond_0

    const/16 v1, 0x80

    :goto_1
    invoke-virtual {v3, v4, v0, v1}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_0
    const/16 v1, 0x40

    goto :goto_1

    .line 462
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/service/h;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/service/h;->b()V

    .line 463
    return-void
.end method

.method private a(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/SyncResult;)Z
    .locals 19

    .prologue
    .line 103
    const-string v2, "PeopleSync"

    const-string v3, "onPerformSync()"

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 104
    invoke-static {}, Lcom/google/android/gms/people/c/f;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 105
    const/4 v2, 0x0

    .line 197
    :goto_0
    return v2

    .line 107
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 108
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    const-string v3, "Disabling sync for restricted user."

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v3}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 109
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    const-string v3, "com.google.android.gms.people"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 110
    const/4 v2, 0x0

    goto :goto_0

    .line 112
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/people/sync/u;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    .line 113
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/people/sync/u;->c(Landroid/os/Bundle;)I

    move-result v5

    .line 114
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v16

    .line 115
    const-wide/16 v14, -0x1

    .line 117
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->a:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/v;->b()V
    :try_end_0
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_0 .. :try_end_0} :catch_1

    .line 119
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/c/f;->g()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_1 .. :try_end_1} :catch_1

    .line 125
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/google/android/gms/people/sync/u;->a(Z)Z

    .line 127
    if-eqz p2, :cond_4

    const-string v2, "initialize"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_5

    .line 132
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/c/f;->n()Z

    .line 134
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    iget-object v2, v4, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    const-string v6, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "initializeSyncState"

    invoke-static {v2, v6, v7, v8, v9}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "com.google.android.gms.people"

    iget-object v6, v4, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/gms/people/sync/a;->a(Landroid/content/Context;)Lcom/google/android/gms/people/sync/a;

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/people/sync/a;->b(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v2

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "initializeSyncState: syncable="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v6}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;Ljava/lang/String;)V

    if-gez v2, :cond_2

    const/4 v2, 0x4

    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    const-string v2, "com.google.android.gms.people"

    const/4 v6, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v2, v6}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;Ljava/lang/String;I)V

    const-string v2, "com.google.android.gms.people"

    const/4 v6, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v2, v6}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;Ljava/lang/String;Z)V

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;)V

    sget-object v2, Lcom/google/android/gms/people/a/a;->K:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v4, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    const-string v6, "PeopleSync"

    const-string v7, "requestSyncNow"

    move-object/from16 v0, p1

    invoke-static {v2, v6, v0, v7}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/String;)V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v6, "force"

    const/4 v7, 0x1

    invoke-virtual {v2, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v6, "expedited"

    const/4 v7, 0x1

    invoke-virtual {v2, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v6, "com.google.android.gms.people"

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v6, v2}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 136
    :cond_2
    sget-object v2, Lcom/google/android/gms/people/a/a;->L:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_5

    .line 138
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 122
    :catch_0
    move-exception v2

    new-instance v2, Lcom/google/android/gms/people/sync/x;

    invoke-direct {v2}, Lcom/google/android/gms/people/sync/x;-><init>()V

    throw v2
    :try_end_2
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_2 .. :try_end_2} :catch_1

    .line 189
    :catch_1
    move-exception v2

    move-wide v2, v14

    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v6, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "***Sync canceled***, duration: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v6, v7, v2}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    :goto_3
    if-eqz v5, :cond_3

    .line 194
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.google.android.gms.people.BROADCAST_CIRCLES_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "com.google.android.gms.permission.INTERNAL_BROADCAST"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 197
    :cond_3
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 127
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 141
    :cond_5
    :try_start_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/google/android/gms/people/service/o;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    const/4 v7, 0x1

    .line 143
    :goto_4
    if-eqz p2, :cond_8

    const-string v2, "gms.people.contacts_sync"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v8, 0x1

    .line 144
    :goto_5
    if-eqz p2, :cond_9

    const-string v2, "page_only"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_9

    const/4 v9, 0x1

    .line 145
    :goto_6
    if-eqz p2, :cond_a

    const-string v2, "gms.people.skip_main_sync"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_a

    const/4 v10, 0x1

    .line 146
    :goto_7
    const/4 v11, 0x0

    .line 148
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v4, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v12, 0x0

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v18, "***Sync start***: feed="

    move-object/from16 v0, v18

    invoke-direct {v13, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v18, " cannotHavePeople="

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v18, " mode="

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v18, " contactOnly="

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v18, " pageOnly="

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v18, " skipMain="

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v2, v4, v6, v12, v13}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/people/sync/u;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 153
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;)V

    .line 156
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/r;->a:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 159
    if-nez v5, :cond_d

    .line 160
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/people/a/a;->t:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v12, v2

    const/4 v2, 0x0

    invoke-virtual {v4, v6, v2, v12, v13}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {v4, v6, v12, v13}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;J)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_b

    const/4 v2, 0x1

    :goto_8
    if-eqz v2, :cond_c

    .line 162
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "Data still fresh; skip periodic sync."

    invoke-static {v2, v3, v4, v6, v7}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 141
    :cond_7
    const/4 v7, 0x0

    goto/16 :goto_4

    .line 143
    :cond_8
    const/4 v8, 0x0

    goto/16 :goto_5

    .line 144
    :cond_9
    const/4 v9, 0x0

    goto/16 :goto_6

    .line 145
    :cond_a
    const/4 v10, 0x0

    goto/16 :goto_7

    .line 160
    :cond_b
    const/4 v2, 0x0

    goto :goto_8

    .line 168
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/r;->b:Lcom/google/android/gms/people/sync/u;

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v11, 0x0

    sget-object v2, Lcom/google/android/gms/people/a/a;->t:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v12, v2

    invoke-virtual {v4, v6, v11, v12, v13}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v11

    .line 174
    :cond_d
    const-string v2, "PeopleSync"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Starting sync, feed="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v2, v3, v4, v6}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_3 .. :try_end_3} :catch_1

    .line 179
    const/4 v12, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v6, p3

    :try_start_4
    invoke-direct/range {v2 .. v12}, Lcom/google/android/gms/people/sync/r;->a(Landroid/accounts/Account;Landroid/os/Bundle;ILandroid/content/SyncResult;ZZZZZLcom/google/android/gms/people/sync/s;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 183
    :try_start_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_5
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_5 .. :try_end_5} :catch_1

    move-result-wide v2

    sub-long v2, v2, v16

    .line 185
    :try_start_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/people/sync/r;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v6, "PeopleSync"

    move-object/from16 v0, p1

    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "***Sync finished***, duration: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v6, v7, v8}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_3

    .line 189
    :catch_2
    move-exception v4

    goto/16 :goto_2

    .line 183
    :catchall_0
    move-exception v4

    :try_start_7
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_7
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_7 .. :try_end_7} :catch_1

    move-result-wide v2

    sub-long v2, v2, v16

    :try_start_8
    throw v4
    :try_end_8
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_8 .. :try_end_8} :catch_2
.end method


# virtual methods
.method public final onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 87
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/sync/r;->a(Z)V

    .line 89
    :try_start_0
    invoke-direct {p0, p1, p2, p5}, Lcom/google/android/gms/people/sync/r;->a(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/SyncResult;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    invoke-virtual {p0, v1}, Lcom/google/android/gms/people/sync/r;->a(Z)V

    .line 92
    return-void

    .line 91
    :catchall_0
    move-exception v0

    invoke-virtual {p0, v1}, Lcom/google/android/gms/people/sync/r;->a(Z)V

    throw v0
.end method

.method public final onSyncCanceled()V
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/google/android/gms/people/sync/r;->a:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/v;->a()V

    .line 479
    invoke-super {p0}, Lcom/google/android/gms/common/f/a;->onSyncCanceled()V

    .line 480
    return-void
.end method
