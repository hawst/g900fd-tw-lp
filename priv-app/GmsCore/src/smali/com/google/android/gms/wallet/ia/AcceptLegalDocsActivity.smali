.class public Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;
.super Lcom/google/android/gms/wallet/common/ui/ds;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/wallet/common/ui/bf;


# static fields
.field private static final h:Ljava/lang/String;


# instance fields
.field protected a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field protected b:Landroid/accounts/Account;

.field c:Landroid/widget/TextView;

.field d:Landroid/widget/ProgressBar;

.field e:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

.field f:Lcom/google/android/gms/wallet/common/ui/bb;

.field g:Lcom/google/android/gms/wallet/common/ui/bb;

.field private i:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private j:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

.field private k:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

.field private l:Lcom/google/checkout/inapp/proto/ag;

.field private m:Z

.field private n:Z

.field private o:Lcom/google/android/gms/wallet/common/ui/dh;

.field private p:I

.field private final q:Lcom/google/android/gms/wallet/service/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-string v0, "legalDocs"

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ds;-><init>()V

    .line 78
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->m:Z

    .line 79
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->n:Z

    .line 81
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->p:I

    .line 342
    new-instance v0, Lcom/google/android/gms/wallet/ia/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ia/a;-><init>(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->q:Lcom/google/android/gms/wallet/service/l;

    return-void
.end method

.method private a(Lcom/google/android/gms/wallet/common/ui/bb;Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/bb;
    .locals 2

    .prologue
    .line 231
    if-eqz p1, :cond_0

    .line 232
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 236
    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    .line 238
    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 239
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 240
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;Lcom/google/checkout/inapp/proto/ag;)Lcom/google/checkout/inapp/proto/ag;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->l:Lcom/google/checkout/inapp/proto/ag;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 209
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->k:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    .line 210
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->b()Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->CH:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/w;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 212
    sget v1, Lcom/google/android/gms/p;->Cs:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->Cr:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/common/w;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 215
    sget v2, Lcom/google/android/gms/p;->CL:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 217
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->c:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/common/ui/ClickSpan;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 219
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->e:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/view/View$OnClickListener;)V

    .line 220
    invoke-direct {p0, v4}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(Z)V

    .line 221
    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 316
    if-eqz p1, :cond_0

    .line 317
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 321
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->c:Landroid/widget/TextView;

    if-nez p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 322
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->e:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    if-nez p1, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 323
    return-void

    .line 319
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->d:Landroid/widget/ProgressBar;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 321
    goto :goto_1

    :cond_2
    move v1, v2

    .line 322
    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->n:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V
    .locals 2

    .prologue
    .line 50
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V
    .locals 2

    .prologue
    .line 50
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V
    .locals 2

    .prologue
    .line 50
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 278
    new-instance v0, Lcom/google/checkout/inapp/proto/af;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/af;-><init>()V

    .line 279
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->j:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    if-eqz v1, :cond_0

    .line 280
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->j:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/n;->a([Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;)[Lcom/google/checkout/inapp/proto/y;

    move-result-object v1

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/af;->b:[Lcom/google/checkout/inapp/proto/y;

    .line 285
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->g()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/checkout/inapp/proto/af;)V

    .line 287
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(Z)V

    .line 288
    return-void

    .line 283
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->i:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/af;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V
    .locals 2

    .prologue
    .line 50
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)Lcom/google/checkout/inapp/proto/ag;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->l:Lcom/google/checkout/inapp/proto/ag;

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 291
    new-instance v0, Lcom/google/checkout/inapp/proto/ac;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/ac;-><init>()V

    .line 292
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->j:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    if-eqz v1, :cond_1

    .line 293
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->j:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/n;->a([Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;)[Lcom/google/checkout/inapp/proto/y;

    move-result-object v1

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/ac;->c:[Lcom/google/checkout/inapp/proto/y;

    .line 298
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->k:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->c()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 299
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->k:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->c()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/ac;->b:[Ljava/lang/String;

    .line 301
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->g()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/checkout/inapp/proto/ac;)V

    .line 303
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(Z)V

    .line 304
    return-void

    .line 296
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->i:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/ac;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method private g()Lcom/google/android/gms/wallet/common/ui/dh;
    .locals 2

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->o:Lcom/google/android/gms/wallet/common/ui/dh;

    if-nez v0, :cond_0

    .line 309
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dh;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->o:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->o:Lcom/google/android/gms/wallet/common/ui/dh;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->k:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 334
    iget v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->p:I

    if-gez v0, :cond_0

    .line 335
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->g()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->q:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->c(Lcom/google/android/gms/wallet/service/l;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->p:I

    .line 340
    :cond_0
    return-void
.end method

.method static synthetic h(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)Z
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->m:Z

    return v0
.end method

.method static synthetic i(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    const-string v1, "inapp.AcceptLegalDocsActivity.EnrollWithBrokerNetworkErrorDialog"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(Lcom/google/android/gms/wallet/common/ui/bb;Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    return-void
.end method

.method static synthetic j(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    const-string v1, "inapp.AcceptLegalDocsActivity.LegalDocsNetworkErrorDialog"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(Lcom/google/android/gms/wallet/common/ui/bb;Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    return-void
.end method

.method static synthetic k(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V
    .locals 2

    .prologue
    .line 50
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    .prologue
    .line 257
    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_2

    .line 258
    packed-switch p1, :pswitch_data_0

    .line 270
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected button "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 260
    :pswitch_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(ILandroid/content/Intent;)V

    .line 268
    :goto_0
    return-void

    .line 263
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->l:Lcom/google/checkout/inapp/proto/ag;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->k:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    if-eqz v0, :cond_1

    .line 264
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->f()V

    goto :goto_0

    .line 266
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->e()V

    goto :goto_0

    .line 273
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected errorCode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 258
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 226
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->f()V

    .line 227
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 96
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/ds;->onCreate(Landroid/os/Bundle;)V

    .line 98
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dg;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/dg;

    .line 99
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ck;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/ck;

    .line 101
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 102
    const-string v0, "com.google.android.gms.wallet.brokerId"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.google.android.gms.wallet.brokerAndRelationships"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v1

    :goto_0
    const-string v3, "Activity requires brokerId extra!"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 106
    const-string v0, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 107
    const-string v0, "com.google.android.gms.wallet.account"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->b:Landroid/accounts/Account;

    .line 108
    const-string v0, "com.google.android.gms.wallet.brokerId"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->i:Ljava/lang/String;

    .line 109
    const-string v0, "com.google.android.gms.wallet.brokerAndRelationships"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    .line 111
    if-eqz v0, :cond_1

    array-length v3, v0

    if-lez v3, :cond_1

    .line 112
    array-length v3, v0

    const-class v4, [Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    invoke-static {v0, v3, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->j:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    .line 115
    :cond_1
    const-string v0, "legalDocsForCountry"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->k:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v2, Lcom/google/android/gms/wallet/common/ui/dt;->b:Lcom/google/android/gms/wallet/common/ui/dz;

    invoke-static {p0, v0, v2}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/common/ui/dz;)V

    .line 120
    sget v0, Lcom/google/android/gms/l;->fV:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->setContentView(I)V

    .line 122
    sget v0, Lcom/google/android/gms/j;->ld:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->c:Landroid/widget/TextView;

    .line 123
    sget v0, Lcom/google/android/gms/j;->pN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->d:Landroid/widget/ProgressBar;

    .line 124
    sget v0, Lcom/google/android/gms/j;->cj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->e:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    .line 126
    if-eqz p1, :cond_4

    .line 127
    const-string v0, "pendingEnrollRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->m:Z

    .line 129
    const-string v0, "serviceConnectionSavePoint"

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->p:I

    .line 131
    const-string v0, "legalDocumentsResponse"

    const-class v2, Lcom/google/checkout/inapp/proto/ag;

    invoke-static {p1, v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/ag;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->l:Lcom/google/checkout/inapp/proto/ag;

    .line 141
    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->g()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    if-nez v0, :cond_2

    .line 142
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->b:Landroid/accounts/Account;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/wallet/common/ui/dh;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->o:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 144
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->o:Lcom/google/android/gms/wallet/common/ui/dh;

    sget-object v2, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 148
    :cond_2
    return-void

    .line 102
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 135
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v2

    const-string v3, "accept_legal_docs"

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 189
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/ds;->onPause()V

    .line 190
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->n:Z

    .line 191
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->h()V

    .line 192
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 152
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/ds;->onResume()V

    .line 153
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->n:Z

    .line 154
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->g()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->q:Lcom/google/android/gms/wallet/service/l;

    iget v2, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->p:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/service/k;->b(Lcom/google/android/gms/wallet/service/l;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->p:I

    .line 155
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "inapp.AcceptLegalDocsActivity.LegalDocsNetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 157
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "inapp.AcceptLegalDocsActivity.EnrollWithBrokerNetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 161
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_2

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 181
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_1

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 184
    :cond_1
    iput-boolean v3, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->n:Z

    .line 185
    return-void

    .line 166
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->l:Lcom/google/checkout/inapp/proto/ag;

    if-eqz v0, :cond_3

    .line 168
    iput-boolean v3, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->n:Z

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->q:Lcom/google/android/gms/wallet/service/l;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->l:Lcom/google/checkout/inapp/proto/ag;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/inapp/proto/ag;)V

    .line 176
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->m:Z

    if-eqz v0, :cond_0

    .line 177
    invoke-direct {p0, v3}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(Z)V

    goto :goto_0

    .line 170
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->k:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    if-eqz v0, :cond_4

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->k:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;)V

    goto :goto_1

    .line 174
    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->e()V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 196
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/ds;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 198
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->h()V

    .line 200
    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->p:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 201
    const-string v0, "pendingEnrollRequest"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->m:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 202
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->l:Lcom/google/checkout/inapp/proto/ag;

    if-eqz v0, :cond_0

    .line 203
    const-string v0, "legalDocumentsResponse"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->l:Lcom/google/checkout/inapp/proto/ag;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 206
    :cond_0
    return-void
.end method
