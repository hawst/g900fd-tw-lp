.class final Lcom/google/android/gms/wallet/service/ow/ae;
.super Lcom/google/android/gms/wallet/service/o;
.source "SourceFile"


# instance fields
.field final synthetic c:Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;

.field final synthetic d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field final synthetic e:Lcom/google/android/gms/wallet/service/ow/y;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/service/ow/y;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/ae;->e:Lcom/google/android/gms/wallet/service/ow/y;

    iput-object p4, p0, Lcom/google/android/gms/wallet/service/ow/ae;->c:Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;

    iput-object p5, p0, Lcom/google/android/gms/wallet/service/ow/ae;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/wallet/service/o;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/a/b;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/ae;->c:Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;->b()Lcom/google/aa/b/a/a/a/a/g;

    move-result-object v4

    .line 299
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/ae;->e:Lcom/google/android/gms/wallet/service/ow/y;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/y;->b(Lcom/google/android/gms/wallet/service/ow/y;)Lcom/google/android/gms/wallet/service/ow/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/ae;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/service/ow/a;->a(Ljava/lang/String;)Lcom/google/aa/b/a/a/a/a/b;

    move-result-object v0

    .line 301
    if-nez v0, :cond_0

    .line 302
    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    .line 308
    :goto_0
    return-object v0

    .line 304
    :cond_0
    iput-object v0, v4, Lcom/google/aa/b/a/a/a/a/g;->a:Lcom/google/aa/b/a/a/a/a/b;

    .line 305
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/ae;->e:Lcom/google/android/gms/wallet/service/ow/y;

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/ae;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/y;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Z

    move-result v0

    iput-boolean v0, v4, Lcom/google/aa/b/a/a/a/a/g;->b:Z

    .line 308
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/ae;->e:Lcom/google/android/gms/wallet/service/ow/y;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/y;->c(Lcom/google/android/gms/wallet/service/ow/y;)Lcom/google/android/gms/wallet/service/ow/ak;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/ae;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v1

    const-string v2, "/online/v2/wallet/sdk/v1/createWalletObjects"

    const/16 v5, 0x1e

    const-string v6, "create_wallet_objects"

    move-object v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/wallet/service/ow/ak;->a(ILjava/lang/String;Lcom/google/android/gms/wallet/a/b;Lcom/google/protobuf/nano/j;ILjava/lang/String;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method
