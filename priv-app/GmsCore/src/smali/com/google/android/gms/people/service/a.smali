.class public abstract Lcom/google/android/gms/people/service/a;
.super Lcom/google/android/gms/common/internal/be;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Ljava/lang/String;

.field protected final c:I

.field public final d:Ljava/lang/String;

.field public e:Z

.field public final f:Ljava/util/Set;

.field private final g:Ljava/util/Set;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/be;-><init>()V

    .line 28
    iput-boolean v0, p0, Lcom/google/android/gms/people/service/a;->e:Z

    .line 29
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/people/service/a;->g:Ljava/util/Set;

    .line 30
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/people/service/a;->f:Ljava/util/Set;

    .line 35
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 37
    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 38
    iput-object p1, p0, Lcom/google/android/gms/people/service/a;->a:Landroid/content/Context;

    .line 39
    iput-object p2, p0, Lcom/google/android/gms/people/service/a;->b:Ljava/lang/String;

    .line 40
    iput p3, p0, Lcom/google/android/gms/people/service/a;->c:I

    .line 41
    iput-object p4, p0, Lcom/google/android/gms/people/service/a;->d:Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 102
    if-eqz p1, :cond_0

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/people/service/a;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 105
    :cond_0
    return-object p1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/people/service/a;->e:Z

    .line 124
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 78
    if-eqz p1, :cond_1

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/people/service/a;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    .line 80
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/a;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/f/m;

    .line 83
    invoke-virtual {v0}, Lcom/google/android/gms/people/f/m;->close()V

    goto :goto_1

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/service/a;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/f/m;

    .line 88
    invoke-virtual {v0}, Lcom/google/android/gms/people/f/m;->b()V

    goto :goto_2

    .line 92
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/service/a;->b(Z)V

    .line 93
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/people/service/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method protected b(Z)V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public final c()Lcom/google/android/gms/common/server/ClientContext;
    .locals 5

    .prologue
    .line 54
    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    iget v1, p0, Lcom/google/android/gms/people/service/a;->c:I

    iget-object v2, p0, Lcom/google/android/gms/people/service/a;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/people/service/a;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/service/a;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected final d()Landroid/content/Context;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/people/service/a;->a:Landroid/content/Context;

    return-object v0
.end method

.method public abstract e()V
.end method
