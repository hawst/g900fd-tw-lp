.class public abstract Lcom/google/android/gms/drive/ui/a;
.super Lcom/google/android/gms/drive/ui/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/ui/e;


# instance fields
.field private b:Z

.field private c:Lcom/google/android/gms/drive/ui/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/h;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/a;->b:Z

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/h;->onCreate(Landroid/os/Bundle;)V

    .line 26
    new-instance v0, Lcom/google/android/gms/drive/ui/b;

    new-instance v1, Lcom/google/android/gms/drive/ui/f;

    invoke-direct {v1}, Lcom/google/android/gms/drive/ui/f;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/drive/ui/b;-><init>(Lcom/google/android/gms/drive/ui/e;Lcom/google/android/gms/drive/ui/g;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/a;->c:Lcom/google/android/gms/drive/ui/b;

    .line 27
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/a;->c:Lcom/google/android/gms/drive/ui/b;

    iget-object v0, v0, Lcom/google/android/gms/drive/ui/b;->e:Lcom/google/android/gms/drive/g/al;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/al;->b()V

    .line 45
    invoke-super {p0}, Lcom/google/android/gms/drive/ui/h;->onDestroy()V

    .line 46
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/a;->b:Z

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/a;->c:Lcom/google/android/gms/drive/ui/b;

    iget-object v1, v0, Lcom/google/android/gms/drive/ui/b;->b:Landroid/database/ContentObserver;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/drive/ui/b;->d:Lcom/google/android/gms/drive/ui/g;

    iget-object v2, v0, Lcom/google/android/gms/drive/ui/b;->b:Landroid/database/ContentObserver;

    invoke-interface {v1, p0, v2}, Lcom/google/android/gms/drive/ui/g;->b(Landroid/content/Context;Landroid/database/ContentObserver;)V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/drive/ui/b;->b:Landroid/database/ContentObserver;

    .line 39
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/drive/ui/h;->onPause()V

    .line 40
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 31
    invoke-super {p0}, Lcom/google/android/gms/drive/ui/h;->onResume()V

    .line 32
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/a;->c:Lcom/google/android/gms/drive/ui/b;

    iget-boolean v1, p0, Lcom/google/android/gms/drive/ui/a;->b:Z

    iget-object v2, v0, Lcom/google/android/gms/drive/ui/b;->b:Landroid/database/ContentObserver;

    if-nez v2, :cond_0

    new-instance v2, Lcom/google/android/gms/drive/ui/d;

    iget-object v3, v0, Lcom/google/android/gms/drive/ui/b;->a:Landroid/os/Handler;

    invoke-direct {v2, v0, v3}, Lcom/google/android/gms/drive/ui/d;-><init>(Lcom/google/android/gms/drive/ui/b;Landroid/os/Handler;)V

    iput-object v2, v0, Lcom/google/android/gms/drive/ui/b;->b:Landroid/database/ContentObserver;

    iget-object v2, v0, Lcom/google/android/gms/drive/ui/b;->d:Lcom/google/android/gms/drive/ui/g;

    iget-object v3, v0, Lcom/google/android/gms/drive/ui/b;->b:Landroid/database/ContentObserver;

    invoke-interface {v2, p0, v3}, Lcom/google/android/gms/drive/ui/g;->a(Landroid/content/Context;Landroid/database/ContentObserver;)V

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/drive/ui/b;->e:Lcom/google/android/gms/drive/g/al;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/al;->a()V

    .line 33
    :cond_0
    return-void
.end method
