.class public final Lcom/google/android/gms/audiomodem/m;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final A:Lcom/google/android/gms/common/a/d;

.field public static final a:Lcom/google/android/gms/common/a/d;

.field public static final b:Lcom/google/android/gms/common/a/d;

.field public static final c:Lcom/google/android/gms/common/a/d;

.field public static final d:Lcom/google/android/gms/common/a/d;

.field public static final e:Lcom/google/android/gms/common/a/d;

.field public static final f:Lcom/google/android/gms/common/a/d;

.field public static final g:Lcom/google/android/gms/common/a/d;

.field public static final h:Lcom/google/android/gms/common/a/d;

.field public static final i:Lcom/google/android/gms/common/a/d;

.field public static final j:Lcom/google/android/gms/common/a/d;

.field public static final k:Lcom/google/android/gms/common/a/d;

.field public static final l:Lcom/google/android/gms/common/a/d;

.field public static final m:Lcom/google/android/gms/common/a/d;

.field public static final n:Lcom/google/android/gms/common/a/d;

.field public static final o:Lcom/google/android/gms/common/a/d;

.field public static final p:Lcom/google/android/gms/common/a/d;

.field public static final q:Lcom/google/android/gms/common/a/d;

.field public static final r:Lcom/google/android/gms/common/a/d;

.field public static final s:Lcom/google/android/gms/common/a/d;

.field public static final t:Lcom/google/android/gms/common/a/d;

.field public static final u:Lcom/google/android/gms/common/a/d;

.field public static final v:Lcom/google/android/gms/common/a/d;

.field public static final w:Lcom/google/android/gms/common/a/d;

.field public static final x:Lcom/google/android/gms/common/a/d;

.field public static final y:Lcom/google/android/gms/common/a/d;

.field public static final z:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v3, 0x5

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 29
    const-string v0, "audiomodem:enable_receiver"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->a:Lcom/google/android/gms/common/a/d;

    .line 35
    const-string v0, "audiomodem:enable_broadcaster"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->b:Lcom/google/android/gms/common/a/d;

    .line 42
    const-string v0, "audiomodem:max_token_guesses_dsss"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->c:Lcom/google/android/gms/common/a/d;

    .line 49
    const-string v0, "audiomodem:max_token_guesses_dtmf"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->d:Lcom/google/android/gms/common/a/d;

    .line 55
    const-string v0, "audiomodem:decoding_buffer_size_seconds"

    const/high16 v1, 0x40400000    # 3.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->e:Lcom/google/android/gms/common/a/d;

    .line 62
    const-string v0, "audiomodem:broadcaster_max_ultrasound_volume"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->f:Lcom/google/android/gms/common/a/d;

    .line 68
    const-string v0, "audiomodem:broadcaster_low_volume"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->g:Lcom/google/android/gms/common/a/d;

    .line 75
    const-string v0, "audiomodem:decoding_period_millis"

    const-wide/16 v2, 0x1f4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->h:Lcom/google/android/gms/common/a/d;

    .line 82
    const-string v0, "audiomodem:detect_broadcaster_period_millis"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->i:Lcom/google/android/gms/common/a/d;

    .line 89
    const-string v0, "audiomodem:processing_period_millis"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->j:Lcom/google/android/gms/common/a/d;

    .line 95
    const-string v0, "audiomodem:should_record_stereo"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->k:Lcom/google/android/gms/common/a/d;

    .line 101
    const-string v0, "audiomodem:recording_sample_rate"

    const v1, 0xac44

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->l:Lcom/google/android/gms/common/a/d;

    .line 107
    const-string v0, "audiomodem:broadcaster_stopped_threshold_millis"

    const-wide/16 v2, 0x96

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->m:Lcom/google/android/gms/common/a/d;

    .line 113
    const-string v0, "audiomodem:recording_audio_source"

    const-string v1, "DEFAULT"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->n:Lcom/google/android/gms/common/a/d;

    .line 120
    const-string v0, "audiomodem:recording_channel_config"

    const-string v1, "CHANNEL_IN_DEFAULT"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->o:Lcom/google/android/gms/common/a/d;

    .line 126
    const-string v0, "audiomodem:should_log_token_audio"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->p:Lcom/google/android/gms/common/a/d;

    .line 132
    const-string v0, "audiomodem:should_log_recorded_audio"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->q:Lcom/google/android/gms/common/a/d;

    .line 138
    const-string v0, "audiomodem:should_log_decoded_tokens"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->r:Lcom/google/android/gms/common/a/d;

    .line 144
    const-string v0, "audiomodem:diagnostic_storage_directory"

    const-string v1, "whispernet"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->s:Lcom/google/android/gms/common/a/d;

    .line 152
    const-string v0, "audiomodem:max_diagnostic_storage_bytes"

    const/high16 v1, 0xa00000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->t:Lcom/google/android/gms/common/a/d;

    .line 159
    const-string v0, "audiomodem:piped_input_stream_buffer_size"

    const/16 v1, 0x2000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->u:Lcom/google/android/gms/common/a/d;

    .line 166
    const-string v0, "audiomodem:record_buffer_size_multiplier"

    const/high16 v1, 0x41000000    # 8.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->v:Lcom/google/android/gms/common/a/d;

    .line 173
    const-string v0, "audiomodem:record_buffer_size_multiplier"

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->w:Lcom/google/android/gms/common/a/d;

    .line 180
    const-string v0, "audiomodem:max_playback_sample_rate_hz"

    const v1, 0xbb80

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->x:Lcom/google/android/gms/common/a/d;

    .line 188
    const-string v0, "audiomodem:dsss_num_token_reps_in_decoding_buffer"

    const/high16 v1, 0x40e00000    # 7.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->y:Lcom/google/android/gms/common/a/d;

    .line 191
    const-string v0, "audiomodem:dtmf_buffer_size_seconds_per_token_byte"

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->z:Lcom/google/android/gms/common/a/d;

    .line 197
    const-string v0, "audiomodem:should_use_odp"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/audiomodem/m;->A:Lcom/google/android/gms/common/a/d;

    return-void
.end method
