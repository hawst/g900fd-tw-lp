.class public final Lcom/google/android/gms/games/ui/common/matches/h;
.super Lcom/google/android/gms/games/ui/card/b;
.source "SourceFile"


# instance fields
.field private final h:Lcom/google/android/gms/games/ui/common/matches/i;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/common/matches/i;)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/common/matches/h;-><init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/common/matches/i;I)V

    .line 67
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/common/matches/i;I)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/b;-><init>(Landroid/content/Context;)V

    .line 72
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/i;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/h;->h:Lcom/google/android/gms/games/ui/common/matches/i;

    .line 74
    sget v0, Lcom/google/android/gms/k;->m:I

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/games/ui/common/matches/h;->e(II)V

    .line 75
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/matches/h;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/h;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/matches/h;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/h;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/common/matches/h;)Lcom/google/android/gms/games/ui/common/matches/i;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/h;->h:Lcom/google/android/gms/games/ui/common/matches/i;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/data/d;)V
    .locals 1

    .prologue
    .line 90
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/google/android/gms/common/data/x;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 91
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/card/b;->a(Lcom/google/android/gms/common/data/d;)V

    .line 92
    return-void

    .line 90
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;ZLjava/util/ArrayList;)V
    .locals 7

    .prologue
    const/4 v3, -0x1

    .line 110
    const/4 v0, 0x0

    .line 113
    iget-object v1, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    .line 114
    const/4 v2, 0x0

    invoke-interface {v1}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v4

    :goto_0
    if-ge v2, v4, :cond_5

    .line 115
    invoke-interface {v1, v2}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    .line 116
    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v4, v0

    .line 123
    :goto_1
    if-eq v2, v3, :cond_1

    .line 157
    :goto_2
    return-void

    .line 114
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 128
    :cond_1
    if-eqz p2, :cond_2

    move-object v0, v1

    .line 130
    check-cast v0, Lcom/google/android/gms/common/data/x;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/data/x;->b(Ljava/lang/Object;)V

    goto :goto_2

    .line 135
    :cond_2
    invoke-virtual {v4}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->d()Ljava/util/ArrayList;

    move-result-object v3

    .line 138
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_3
    if-ltz v1, :cond_4

    .line 139
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->f()Ljava/lang/String;

    move-result-object v0

    .line 140
    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 144
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 148
    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 138
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_3

    .line 152
    :cond_4
    invoke-virtual {v4, v3}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->a(Ljava/util/ArrayList;)V

    .line 155
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/common/matches/h;->h(I)V

    goto :goto_2

    :cond_5
    move v2, v3

    move-object v4, v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/matches/h;->i:Ljava/lang/String;

    .line 85
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/matches/h;->j:Ljava/lang/String;

    .line 86
    return-void
.end method

.method protected final b(Landroid/view/View;)Lcom/google/android/gms/games/ui/card/c;
    .locals 1

    .prologue
    .line 161
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/j;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/j;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method protected final w()I
    .locals 1

    .prologue
    .line 166
    sget v0, Lcom/google/android/gms/j;->gB:I

    return v0
.end method

.method protected final x()I
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x3

    return v0
.end method
