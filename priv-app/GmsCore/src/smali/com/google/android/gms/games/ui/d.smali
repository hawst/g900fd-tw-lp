.class public abstract Lcom/google/android/gms/games/ui/d;
.super Lcom/google/android/gms/games/ui/ac;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/common/data/f;
.implements Lcom/google/android/gms/games/ui/a;


# static fields
.field private static final h:I

.field private static final i:I

.field private static final j:I


# instance fields
.field protected final c:Landroid/content/Context;

.field public d:Lcom/google/android/gms/common/data/d;

.field protected final e:Landroid/view/LayoutInflater;

.field public f:Z

.field private k:Z

.field private l:Z

.field private m:I

.field private n:Lcom/google/android/gms/games/ui/g;

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Landroid/view/View;

.field private s:Z

.field private t:I

.field private u:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    sget v0, Lcom/google/android/gms/j;->gy:I

    sput v0, Lcom/google/android/gms/games/ui/d;->h:I

    .line 37
    sget v0, Lcom/google/android/gms/j;->gx:I

    sput v0, Lcom/google/android/gms/games/ui/d;->i:I

    .line 39
    sget v0, Lcom/google/android/gms/j;->gz:I

    sput v0, Lcom/google/android/gms/games/ui/d;->j:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/ac;-><init>()V

    .line 44
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/d;->k:Z

    .line 52
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/d;->l:Z

    .line 63
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->f:Z

    .line 64
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    .line 69
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->s:Z

    .line 75
    iput v0, p0, Lcom/google/android/gms/games/ui/d;->t:I

    .line 80
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/gms/games/ui/d;->u:I

    .line 83
    iput-object p1, p0, Lcom/google/android/gms/games/ui/d;->c:Landroid/content/Context;

    .line 84
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d;->e:Landroid/view/LayoutInflater;

    .line 85
    return-void
.end method

.method private A()Z
    .locals 1

    .prologue
    .line 562
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->z()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(IZ)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 631
    if-nez p2, :cond_0

    const/4 v0, 0x1

    .line 632
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 650
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "pageDirection needs to be NEXT or PREV"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v1

    .line 631
    goto :goto_0

    .line 634
    :pswitch_0
    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/d;->p:Z

    if-eq v2, v0, :cond_1

    .line 635
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->p:Z

    .line 636
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->A()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 637
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/d;->c(I)V

    .line 652
    :cond_1
    :goto_1
    return-void

    .line 642
    :pswitch_1
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/d;->q:Z

    if-eq v1, v0, :cond_1

    .line 643
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->q:Z

    .line 644
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->f:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->A()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 645
    iget v0, p0, Lcom/google/android/gms/games/ui/d;->m:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/d;->c(I)V

    goto :goto_1

    .line 632
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private a(ZZI)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1026
    if-eqz p2, :cond_3

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->f:Z

    if-nez v0, :cond_3

    .line 1027
    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/d;->e(I)V

    .line 1028
    add-int/lit8 p3, p3, -0x1

    move v0, p3

    .line 1031
    :goto_0
    if-eqz p1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    if-nez v1, :cond_0

    .line 1032
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/d;->e(I)V

    .line 1033
    add-int/lit8 v0, v0, -0x1

    .line 1036
    :cond_0
    if-nez p1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    if-eqz v1, :cond_1

    .line 1037
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/d;->d(I)V

    .line 1038
    add-int/lit8 v0, v0, 0x1

    .line 1041
    :cond_1
    if-nez p2, :cond_2

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/d;->f:Z

    if-eqz v1, :cond_2

    .line 1042
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/d;->d(I)V

    .line 1044
    :cond_2
    return-void

    :cond_3
    move v0, p3

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/d;)Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->p:Z

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/d;I)Z
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/d;->j(I)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/d;)Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->q:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/d;I)Z
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/d;->k(I)Z

    move-result v0

    return v0
.end method

.method private i(I)V
    .locals 2

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->n:Lcom/google/android/gms/games/ui/g;

    if-eqz v0, :cond_0

    .line 582
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->n:Lcom/google/android/gms/games/ui/g;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/g;->a(I)V

    .line 583
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/d;->a(IZ)V

    .line 592
    :goto_0
    return-void

    .line 589
    :cond_0
    const-string v0, "DBRecyclerAdapter"

    const-string v1, "Reached the end of a paginated DataBuffer, but no OnEndOfWindowReachedListener registered!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private j(I)Z
    .locals 1

    .prologue
    .line 599
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k(I)Z
    .locals 1

    .prologue
    .line 603
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->f:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/d;->m:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l(I)V
    .locals 2

    .prologue
    .line 1053
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->a()I

    move-result v0

    .line 1056
    if-ge v0, p1, :cond_2

    .line 1058
    sub-int v1, p1, v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/d;->d(II)V

    .line 1066
    :cond_0
    :goto_0
    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1067
    if-lez v0, :cond_1

    .line 1068
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/ui/d;->a(II)V

    .line 1070
    :cond_1
    return-void

    .line 1059
    :cond_2
    if-ge p1, v0, :cond_0

    .line 1061
    sub-int v1, v0, p1

    invoke-virtual {p0, p1, v1}, Lcom/google/android/gms/games/ui/d;->c(II)V

    goto :goto_0
.end method

.method private w()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    if-nez v0, :cond_1

    .line 129
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    .line 130
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/d;->f:Z

    .line 131
    iput v2, p0, Lcom/google/android/gms/games/ui/d;->m:I

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->k:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    invoke-static {v0}, Lcom/google/android/gms/common/data/k;->b(Lcom/google/android/gms/common/data/d;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    .line 134
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->k:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    invoke-static {v0}, Lcom/google/android/gms/common/data/k;->a(Lcom/google/android/gms/common/data/d;)Z

    move-result v0

    if-eqz v0, :cond_4

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/d;->f:Z

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    if-nez v0, :cond_5

    :goto_3
    iput v2, p0, Lcom/google/android/gms/games/ui/d;->m:I

    .line 137
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    if-eqz v0, :cond_2

    .line 138
    iget v0, p0, Lcom/google/android/gms/games/ui/d;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/d;->m:I

    .line 140
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->f:Z

    if-eqz v0, :cond_0

    .line 141
    iget v0, p0, Lcom/google/android/gms/games/ui/d;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/d;->m:I

    goto :goto_0

    :cond_3
    move v0, v2

    .line 133
    goto :goto_1

    :cond_4
    move v1, v2

    .line 134
    goto :goto_2

    .line 136
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/games/ui/d;->t:I

    sub-int/2addr v0, v1

    if-gez v0, :cond_6

    :goto_4
    iget v0, p0, Lcom/google/android/gms/games/ui/d;->u:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto :goto_3

    :cond_6
    move v2, v0

    goto :goto_4
.end method

.method private x()I
    .locals 2

    .prologue
    .line 158
    iget v0, p0, Lcom/google/android/gms/games/ui/d;->m:I

    .line 159
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    if-eqz v1, :cond_0

    .line 160
    add-int/lit8 v0, v0, -0x1

    .line 162
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/d;->f:Z

    if-eqz v1, :cond_1

    .line 163
    add-int/lit8 v0, v0, -0x1

    .line 165
    :cond_1
    return v0
.end method

.method private y()Z
    .locals 1

    .prologue
    .line 554
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->s:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->r:Landroid/view/View;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/d;->m:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private z()Z
    .locals 1

    .prologue
    .line 558
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->s:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 335
    const/4 v0, 0x0

    .line 341
    :goto_0
    return v0

    .line 337
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 339
    const/4 v0, 0x1

    goto :goto_0

    .line 341
    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/ui/d;->m:I

    goto :goto_0
.end method

.method public final a(I)I
    .locals 1

    .prologue
    .line 424
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->s:Z

    if-eqz v0, :cond_0

    .line 425
    sget v0, Lcom/google/android/gms/games/ui/d;->j:I

    .line 436
    :goto_0
    return v0

    .line 427
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 428
    sget v0, Lcom/google/android/gms/games/ui/d;->i:I

    goto :goto_0

    .line 430
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/d;->j(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 431
    sget v0, Lcom/google/android/gms/games/ui/d;->h:I

    goto :goto_0

    .line 433
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/d;->k(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 434
    sget v0, Lcom/google/android/gms/games/ui/d;->h:I

    goto :goto_0

    .line 436
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->h()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/cs;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 28
    sget v0, Lcom/google/android/gms/games/ui/d;->j:I

    if-ne p2, v0, :cond_0

    new-instance v0, Lcom/google/android/gms/games/ui/i;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/d;->e:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/gms/l;->ba:I

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/i;-><init>(Landroid/view/View;)V

    :goto_0
    return-object v0

    :cond_0
    sget v0, Lcom/google/android/gms/games/ui/d;->i:I

    if-ne p2, v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/ui/i;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/d;->r:Landroid/view/View;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/i;-><init>(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    sget v0, Lcom/google/android/gms/games/ui/d;->h:I

    if-ne p2, v0, :cond_2

    new-instance v0, Lcom/google/android/gms/games/ui/e;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/d;->e:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/gms/l;->bn:I

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/e;-><init>(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/games/ui/d;->b(Landroid/view/ViewGroup;I)Lcom/google/android/gms/games/ui/f;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Landroid/support/v7/widget/cs;I)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lcom/google/android/gms/games/ui/f;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/games/ui/d;->a(Lcom/google/android/gms/games/ui/f;I)V

    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->a()I

    move-result v0

    .line 543
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->y()Z

    move-result v1

    .line 545
    iput-object p1, p0, Lcom/google/android/gms/games/ui/d;->r:Landroid/view/View;

    .line 547
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->y()Z

    move-result v2

    .line 548
    if-eq v1, v2, :cond_0

    .line 549
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/d;->l(I)V

    .line 551
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/gms/common/data/d;)V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->a()I

    move-result v1

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    if-eqz v0, :cond_3

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    instance-of v0, v0, Lcom/google/android/gms/common/data/g;

    if-eqz v0, :cond_2

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    check-cast v0, Lcom/google/android/gms/common/data/g;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/data/g;->b(Lcom/google/android/gms/common/data/f;)V

    .line 112
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->w_()V

    .line 115
    :cond_3
    iput-object p1, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    .line 116
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->w()V

    .line 117
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->s:Z

    .line 119
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/d;->l(I)V

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    instance-of v0, v0, Lcom/google/android/gms/common/data/g;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    check-cast v0, Lcom/google/android/gms/common/data/g;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/data/g;->a(Lcom/google/android/gms/common/data/f;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/games/ui/f;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 486
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/ui/d;->k(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 487
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->q:Z

    if-nez v0, :cond_0

    .line 490
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->n:Lcom/google/android/gms/games/ui/g;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/google/android/gms/games/ui/g;->a(I)V

    .line 493
    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/ui/d;->j(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 494
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->p:Z

    if-nez v0, :cond_1

    .line 497
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->n:Lcom/google/android/gms/games/ui/g;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Lcom/google/android/gms/games/ui/g;->a(I)V

    .line 500
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->s:Z

    if-eqz v0, :cond_2

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, p0, p2, v0}, Lcom/google/android/gms/games/ui/f;->a(Lcom/google/android/gms/games/ui/ac;ILjava/lang/Object;)V

    .line 501
    return-void

    .line 500
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->y()Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v1

    goto :goto_0

    :cond_3
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/ui/d;->k(I)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v1

    goto :goto_0

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    if-eqz v0, :cond_7

    invoke-direct {p0, p2}, Lcom/google/android/gms/games/ui/d;->j(I)Z

    move-result v0

    if-eqz v0, :cond_5

    move-object v0, v1

    goto :goto_0

    :cond_5
    add-int/lit8 v0, p2, -0x1

    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    if-nez v2, :cond_6

    move-object v0, v1

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    iget v2, p0, Lcom/google/android/gms/games/ui/d;->t:I

    add-int/2addr v0, v2

    invoke-interface {v1, v0}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_7
    move v0, p2

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/ui/g;)V
    .locals 0

    .prologue
    .line 576
    iput-object p1, p0, Lcom/google/android/gms/games/ui/d;->n:Lcom/google/android/gms/games/ui/g;

    .line 577
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 404
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->a()I

    move-result v0

    .line 405
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->g()Z

    move-result v1

    .line 406
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/d;->l:Z

    .line 407
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->g()Z

    move-result v2

    .line 409
    if-eq v1, v2, :cond_0

    .line 410
    if-eqz v2, :cond_1

    .line 411
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->a()I

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/google/android/gms/games/ui/d;->c(II)V

    .line 416
    :cond_0
    :goto_0
    return-void

    .line 413
    :cond_1
    invoke-virtual {p0, v3, v0}, Lcom/google/android/gms/games/ui/d;->d(II)V

    goto :goto_0
.end method

.method protected abstract b(Landroid/view/ViewGroup;I)Lcom/google/android/gms/games/ui/f;
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/d;->a(Lcom/google/android/gms/common/data/d;)V

    .line 94
    return-void
.end method

.method public final b_(II)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 843
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    .line 844
    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/d;->f:Z

    .line 845
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->a()I

    move-result v3

    .line 846
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->x()I

    move-result v4

    .line 847
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->y()Z

    move-result v5

    .line 850
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->w()V

    .line 853
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->g()Z

    move-result v6

    if-nez v6, :cond_1

    .line 905
    :cond_0
    :goto_0
    return-void

    .line 858
    :cond_1
    if-eqz v5, :cond_2

    .line 862
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->y()Z

    move-result v0

    if-nez v0, :cond_0

    .line 867
    invoke-direct {p0, v3}, Lcom/google/android/gms/games/ui/d;->l(I)V

    goto :goto_0

    .line 873
    :cond_2
    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/gms/games/ui/d;->a(ZZI)V

    .line 876
    iget v1, p0, Lcom/google/android/gms/games/ui/d;->t:I

    sub-int v1, p1, v1

    .line 879
    new-instance v2, Lcom/google/android/gms/games/ui/h;

    invoke-direct {v2}, Lcom/google/android/gms/games/ui/h;-><init>()V

    .line 883
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v2, Lcom/google/android/gms/games/ui/h;->a:I

    .line 884
    iget v1, v2, Lcom/google/android/gms/games/ui/h;->a:I

    add-int/2addr v1, p2

    iget v3, p0, Lcom/google/android/gms/games/ui/d;->u:I

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v2, Lcom/google/android/gms/games/ui/h;->b:I

    .line 888
    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/h;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 893
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    .line 896
    :cond_3
    iget v1, v2, Lcom/google/android/gms/games/ui/h;->a:I

    add-int/2addr v1, v0

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/h;->b()I

    move-result v3

    invoke-virtual {p0, v1, v3}, Lcom/google/android/gms/games/ui/d;->c(II)V

    .line 899
    new-instance v1, Lcom/google/android/gms/games/ui/h;

    iget v3, p0, Lcom/google/android/gms/games/ui/d;->u:I

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/h;->b()I

    move-result v2

    add-int/2addr v2, v4

    invoke-direct {v1, v3, v2}, Lcom/google/android/gms/games/ui/h;-><init>(II)V

    .line 902
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/h;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 903
    iget v2, v1, Lcom/google/android/gms/games/ui/h;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/h;->b()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/d;->d(II)V

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 303
    iget v0, p0, Lcom/google/android/gms/games/ui/d;->u:I

    return v0
.end method

.method public final c_(II)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 911
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    .line 912
    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/d;->f:Z

    .line 913
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->a()I

    move-result v3

    .line 914
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->x()I

    move-result v4

    .line 915
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->y()Z

    move-result v5

    .line 918
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->w()V

    .line 921
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->g()Z

    move-result v6

    if-nez v6, :cond_1

    .line 978
    :cond_0
    :goto_0
    return-void

    .line 926
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->y()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 928
    if-nez v5, :cond_0

    .line 933
    invoke-direct {p0, v3}, Lcom/google/android/gms/games/ui/d;->l(I)V

    goto :goto_0

    .line 939
    :cond_2
    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/gms/games/ui/d;->a(ZZI)V

    .line 942
    iget v1, p0, Lcom/google/android/gms/games/ui/d;->t:I

    sub-int v1, p1, v1

    .line 945
    new-instance v2, Lcom/google/android/gms/games/ui/h;

    invoke-direct {v2}, Lcom/google/android/gms/games/ui/h;-><init>()V

    .line 949
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v2, Lcom/google/android/gms/games/ui/h;->a:I

    .line 950
    iget v1, v2, Lcom/google/android/gms/games/ui/h;->a:I

    add-int/2addr v1, p2

    iget v3, p0, Lcom/google/android/gms/games/ui/d;->u:I

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v2, Lcom/google/android/gms/games/ui/h;->b:I

    .line 954
    invoke-virtual {v2, v4}, Lcom/google/android/gms/games/ui/h;->a(I)V

    .line 958
    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/h;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 963
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    .line 966
    :cond_3
    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/h;->b()I

    move-result v1

    .line 967
    iget v2, v2, Lcom/google/android/gms/games/ui/h;->a:I

    add-int/2addr v2, v0

    invoke-virtual {p0, v2, v1}, Lcom/google/android/gms/games/ui/d;->d(II)V

    .line 970
    new-instance v2, Lcom/google/android/gms/games/ui/h;

    iget v3, p0, Lcom/google/android/gms/games/ui/d;->u:I

    sub-int v1, v3, v1

    iget v3, p0, Lcom/google/android/gms/games/ui/d;->u:I

    invoke-direct {v2, v1, v3}, Lcom/google/android/gms/games/ui/h;-><init>(II)V

    .line 971
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->x()I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/ui/h;->a(I)V

    .line 975
    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/h;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 976
    iget v1, v2, Lcom/google/android/gms/games/ui/h;->a:I

    add-int/2addr v0, v1

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/h;->b()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/d;->c(II)V

    goto :goto_0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 315
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    .line 316
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/d;->f:Z

    .line 317
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->a()I

    move-result v2

    .line 319
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/d;->k:Z

    .line 321
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->w()V

    .line 323
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->A()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 324
    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/d;->a(ZZI)V

    .line 326
    :cond_0
    return-void
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(II)V
    .locals 5

    .prologue
    .line 249
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 252
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 253
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 254
    if-gtz v1, :cond_2

    .line 255
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "numColumns must be at least 1."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 258
    :cond_2
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 259
    if-gtz v0, :cond_3

    .line 260
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxNumRows must be at least 1."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 263
    :cond_3
    mul-int/2addr v0, v1

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->x()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->a()I

    move-result v2

    iget-boolean v3, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    iget-boolean v4, p0, Lcom/google/android/gms/games/ui/d;->f:Z

    iput v0, p0, Lcom/google/android/gms/games/ui/d;->u:I

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->w()V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v3, v4, v2}, Lcom/google/android/gms/games/ui/d;->a(ZZI)V

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->x()I

    move-result v2

    if-le v2, v1, :cond_5

    add-int/2addr v0, v1

    sub-int v1, v2, v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/d;->c(II)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    if-ge v2, v1, :cond_0

    add-int/2addr v0, v2

    sub-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/d;->d(II)V

    goto :goto_0
.end method

.method public final f()Lcom/google/android/gms/common/data/d;
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    return-object v0
.end method

.method public final f(I)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 180
    iget v0, p0, Lcom/google/android/gms/games/ui/d;->t:I

    if-ne v0, p1, :cond_1

    .line 238
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    iget v3, p0, Lcom/google/android/gms/games/ui/d;->t:I

    .line 186
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->x()I

    move-result v4

    .line 187
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->a()I

    move-result v5

    .line 188
    iget-boolean v6, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    .line 189
    iget-boolean v7, p0, Lcom/google/android/gms/games/ui/d;->f:Z

    .line 191
    if-ltz p1, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 192
    iput p1, p0, Lcom/google/android/gms/games/ui/d;->t:I

    .line 193
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->w()V

    .line 195
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    invoke-direct {p0, v6, v7, v5}, Lcom/google/android/gms/games/ui/d;->a(ZZI)V

    .line 198
    new-instance v5, Lcom/google/android/gms/games/ui/h;

    add-int v0, v3, v4

    invoke-direct {v5, v3, v0}, Lcom/google/android/gms/games/ui/h;-><init>(II)V

    .line 199
    new-instance v6, Lcom/google/android/gms/games/ui/h;

    iget v0, p0, Lcom/google/android/gms/games/ui/d;->t:I

    iget v3, p0, Lcom/google/android/gms/games/ui/d;->t:I

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->x()I

    move-result v7

    add-int/2addr v3, v7

    invoke-direct {v6, v0, v3}, Lcom/google/android/gms/games/ui/h;-><init>(II)V

    .line 201
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    if-eqz v0, :cond_4

    move v0, v1

    .line 205
    :goto_2
    iget v3, v5, Lcom/google/android/gms/games/ui/h;->a:I

    iget v7, v6, Lcom/google/android/gms/games/ui/h;->a:I

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v7, v5, Lcom/google/android/gms/games/ui/h;->b:I

    iget v8, v6, Lcom/google/android/gms/games/ui/h;->b:I

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    if-ge v3, v7, :cond_5

    move v3, v1

    :goto_3
    if-nez v3, :cond_6

    .line 206
    invoke-virtual {v5}, Lcom/google/android/gms/games/ui/h;->b()I

    move-result v1

    .line 207
    if-lez v1, :cond_2

    .line 208
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/d;->d(II)V

    .line 210
    :cond_2
    invoke-virtual {v6}, Lcom/google/android/gms/games/ui/h;->b()I

    move-result v1

    .line 211
    if-lez v1, :cond_0

    .line 212
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/d;->c(II)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 191
    goto :goto_1

    :cond_4
    move v0, v2

    .line 201
    goto :goto_2

    :cond_5
    move v3, v2

    .line 205
    goto :goto_3

    .line 214
    :cond_6
    iget v3, v6, Lcom/google/android/gms/games/ui/h;->a:I

    iget v7, v5, Lcom/google/android/gms/games/ui/h;->a:I

    if-ge v3, v7, :cond_8

    .line 217
    iget v1, v5, Lcom/google/android/gms/games/ui/h;->b:I

    iget v2, v6, Lcom/google/android/gms/games/ui/h;->b:I

    sub-int/2addr v1, v2

    .line 218
    if-lez v1, :cond_7

    .line 219
    iget v2, v6, Lcom/google/android/gms/games/ui/h;->b:I

    iget v3, v5, Lcom/google/android/gms/games/ui/h;->a:I

    sub-int/2addr v2, v3

    add-int/2addr v2, v0

    invoke-virtual {p0, v2, v1}, Lcom/google/android/gms/games/ui/d;->d(II)V

    .line 222
    :cond_7
    iget v1, v5, Lcom/google/android/gms/games/ui/h;->a:I

    iget v2, v6, Lcom/google/android/gms/games/ui/h;->a:I

    sub-int/2addr v1, v2

    .line 223
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/d;->c(II)V

    goto/16 :goto_0

    .line 227
    :cond_8
    iget v3, v6, Lcom/google/android/gms/games/ui/h;->b:I

    iget v7, v5, Lcom/google/android/gms/games/ui/h;->b:I

    sub-int/2addr v3, v7

    .line 228
    if-lez v3, :cond_9

    .line 229
    add-int/2addr v4, v0

    invoke-virtual {p0, v4, v3}, Lcom/google/android/gms/games/ui/d;->c(II)V

    .line 232
    :cond_9
    iget v3, v6, Lcom/google/android/gms/games/ui/h;->a:I

    iget v4, v5, Lcom/google/android/gms/games/ui/h;->a:I

    sub-int/2addr v3, v4

    .line 234
    if-lez v3, :cond_a

    :goto_4
    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 235
    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/games/ui/d;->d(II)V

    goto/16 :goto_0

    :cond_a
    move v1, v2

    .line 234
    goto :goto_4
.end method

.method protected final g(I)I
    .locals 2

    .prologue
    .line 448
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->r()I

    move-result v0

    .line 449
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/d;->s:Z

    if-eqz v1, :cond_1

    .line 453
    :cond_0
    :goto_0
    return v0

    .line 450
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->y()Z

    move-result v1

    if-nez v1, :cond_0

    .line 451
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/d;->j(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 452
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/d;->k(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 453
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 395
    invoke-super {p0}, Lcom/google/android/gms/games/ui/ac;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract h()I
.end method

.method public final h(I)V
    .locals 3

    .prologue
    .line 821
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 823
    new-instance v1, Lcom/google/android/gms/games/ui/h;

    iget v0, p0, Lcom/google/android/gms/games/ui/d;->t:I

    sub-int v0, p1, v0

    iget v2, p0, Lcom/google/android/gms/games/ui/d;->t:I

    sub-int v2, p1, v2

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/games/ui/h;-><init>(II)V

    .line 826
    iget v0, p0, Lcom/google/android/gms/games/ui/d;->u:I

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/h;->a(I)V

    .line 830
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/h;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 832
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->o:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 834
    :goto_0
    iget v2, v1, Lcom/google/android/gms/games/ui/h;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/h;->b()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/d;->a(II)V

    .line 837
    :cond_0
    return-void

    .line 832
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 517
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->s:Z

    if-eq v1, v0, :cond_0

    .line 518
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->a()I

    move-result v0

    .line 519
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/d;->s:Z

    .line 521
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 522
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/d;->l(I)V

    .line 525
    :cond_0
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 607
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d;->f:Z

    return v0
.end method

.method public final k()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 627
    invoke-direct {p0, v0, v0}, Lcom/google/android/gms/games/ui/d;->a(IZ)V

    .line 628
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1075
    const/4 v0, 0x1

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 660
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->qs:I

    if-ne v0, v1, :cond_0

    .line 661
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 662
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/d;->j(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 663
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/d;->i(I)V

    .line 668
    :cond_0
    :goto_0
    return-void

    .line 664
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/d;->k(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 665
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/d;->i(I)V

    goto :goto_0
.end method
