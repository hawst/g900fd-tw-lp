.class public Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/internal/model/apps/b;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/common/people/data/Audience;

.field private final d:Ljava/util/ArrayList;

.field private final e:Z

.field private final f:Z

.field private final g:Z

.field private final h:Z

.field private final i:I

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/gms/plus/internal/model/apps/b;

    invoke-direct {v0}, Lcom/google/android/gms/plus/internal/model/apps/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->CREATOR:Lcom/google/android/gms/plus/internal/model/apps/b;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Lcom/google/android/gms/common/people/data/Audience;Ljava/util/ArrayList;ZZZZILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    iput p1, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->a:I

    .line 159
    iput-object p2, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->b:Ljava/lang/String;

    .line 160
    iput-object p3, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->c:Lcom/google/android/gms/common/people/data/Audience;

    .line 161
    iput-object p4, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d:Ljava/util/ArrayList;

    .line 162
    iput-boolean p5, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->e:Z

    .line 163
    iput-boolean p6, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f:Z

    .line 164
    iput-boolean p7, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->g:Z

    .line 165
    iput-boolean p8, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->h:Z

    .line 166
    iput p9, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->i:I

    .line 167
    iput-object p10, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->j:Ljava/lang/String;

    .line 168
    iput-object p11, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->k:Ljava/lang/String;

    .line 169
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->a:I

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/google/android/gms/common/people/data/Audience;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->c:Lcom/google/android/gms/common/people/data/Audience;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 271
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->c:Lcom/google/android/gms/common/people/data/Audience;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 303
    instance-of v1, p1, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    if-nez v1, :cond_1

    .line 308
    :cond_0
    :goto_0
    return v0

    .line 307
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    .line 308
    iget v1, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->a:I

    iget v2, p1, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->a:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->c:Lcom/google/android/gms/common/people/data/Audience;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->c:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d:Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->e:Z

    iget-boolean v2, p1, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->e:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f:Z

    iget-boolean v2, p1, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->g:Z

    iget-boolean v2, p1, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->g:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->h:Z

    iget-boolean v2, p1, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->h:Z

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->i:I

    iget v2, p1, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->i:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->j:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->k:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->k:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final f()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 214
    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 222
    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->e:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 296
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->c:Lcom/google/android/gms/common/people/data/Audience;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d:Ljava/util/ArrayList;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->g:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->h:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->i:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->j:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->k:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 230
    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f:Z

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 237
    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->g:Z

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 244
    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->h:Z

    return v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 251
    iget v0, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->i:I

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 3

    .prologue
    .line 280
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 281
    const-class v1, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 282
    const-string v1, "\n   verbs: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 283
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 284
    const-string v1, "\n   pacl:  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->c:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 286
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 287
    const-string v1, "\n   facl:  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 288
    const-string v1, "\n   all_circles: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 289
    const-string v1, "\n   all_contacts: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 291
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 276
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/internal/model/apps/b;->a(Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;Landroid/os/Parcel;I)V

    .line 277
    return-void
.end method
