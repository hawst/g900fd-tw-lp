.class public Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;
.super Lcom/google/android/gms/auth/setup/d2d/e;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field private static final b:Lcom/google/android/gms/auth/d/a;


# instance fields
.field private c:Ljava/util/ArrayList;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Landroid/webkit/CookieManager;

.field private h:Ljava/lang/String;

.field private i:Ljava/util/Collection;

.field private j:I

.field private k:Ljava/util/concurrent/Semaphore;

.field private l:Landroid/os/ResultReceiver;

.field private m:Lcom/google/android/setupwizard/util/b;

.field private n:Landroid/webkit/WebView;

.field private o:Landroid/view/View;

.field private p:Z

.field private q:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

.field private r:Ljava/util/concurrent/atomic/AtomicInteger;

.field private s:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "D2D"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "TargetAddAccountsActivity"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->b:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/e;-><init>()V

    .line 94
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->r:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 95
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->s:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "accounts"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "restoreAccount"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "restoreToken"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Ljava/util/concurrent/Semaphore;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->k:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->k()V

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->k:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->b:Lcom/google/android/gms/auth/d/a;

    const-string v1, "This should not happen."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->f(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->r:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->l:Landroid/os/ResultReceiver;

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/gms/auth/setup/d2d/AddAccountIntentService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/ResultReceiver;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->j()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Landroid/webkit/CookieManager;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->g:Landroid/webkit/CookieManager;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->p:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->n:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->q:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->j:I

    return v0
.end method

.method static synthetic h()Lcom/google/android/gms/auth/d/a;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->b:Lcom/google/android/gms/auth/d/a;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->e:Ljava/lang/String;

    return-object v0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->m:Lcom/google/android/setupwizard/util/b;

    sget v1, Lcom/google/android/gms/p;->aY:I

    sget v2, Lcom/google/android/gms/p;->aM:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/setupwizard/util/b;->c(II)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->o:Landroid/view/View;

    .line 174
    return-void
.end method

.method static synthetic j(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->f:Ljava/lang/String;

    return-object v0
.end method

.method private j()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 177
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->k()V

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 181
    const-string v1, "name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 182
    const-string v2, "credential"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 183
    const-string v3, "url"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 184
    iput-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->d:Ljava/lang/String;

    .line 186
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 187
    const-string v3, "firstName"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 188
    const-string v4, "lastName"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 189
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->k()V

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->k:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->b:Lcom/google/android/gms/auth/d/a;

    const-string v1, "This should not happen."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->f(Ljava/lang/String;)V

    .line 255
    :goto_0
    return-void

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->r:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    iget-object v5, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->l:Landroid/os/ResultReceiver;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/auth/setup/d2d/AddAccountIntentService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/ResultReceiver;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 190
    :cond_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 191
    iput-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->h:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->b:Lcom/google/android/gms/auth/d/a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Starting challenge for account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->s:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->g:Landroid/webkit/CookieManager;

    new-instance v1, Lcom/google/android/gms/auth/setup/d2d/ac;

    invoke-direct {v1, p0, v3}, Lcom/google/android/gms/auth/setup/d2d/ac;-><init>(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/webkit/CookieManager;->removeAllCookies(Landroid/webkit/ValueCallback;)V

    goto :goto_0

    .line 193
    :cond_2
    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->b:Lcom/google/android/gms/auth/d/a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Account has no credential nor URL: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/d/a;->f(Ljava/lang/String;)V

    goto :goto_0

    .line 197
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->i:Ljava/util/Collection;

    new-instance v1, Lcom/google/android/gms/auth/setup/d2d/ab;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/setup/d2d/ab;-><init>(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)V

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/setup/d2d/ab;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method static synthetic k(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->r:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 259
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->p:Z

    .line 260
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 261
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->q:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->b()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 262
    return-void
.end method

.method static synthetic l(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->s:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 353
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->onBackPressed()V

    .line 354
    return-void
.end method

.method public final a(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2

    .prologue
    .line 340
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/setup/d2d/e;->a(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V

    .line 341
    iput-object p1, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->q:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    .line 344
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->q:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->a()Landroid/widget/Button;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 347
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->q:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->b()Landroid/widget/Button;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->co:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 348
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->q:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->b()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 349
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->aV:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->aT:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->aS:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->aU:I

    new-instance v2, Lcom/google/android/gms/auth/setup/d2d/ad;

    invoke-direct {v2, p0}, Lcom/google/android/gms/auth/setup/d2d/ad;-><init>(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 365
    :goto_0
    return-void

    .line 364
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->j()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 370
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 152
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/setup/d2d/e;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->m:Lcom/google/android/setupwizard/util/b;

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/b;->removeView(Landroid/view/View;)V

    .line 154
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->i()V

    .line 155
    iget-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->p:Z

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 160
    :goto_0
    return-void

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->n:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->bringToFront()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 99
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/setup/d2d/e;->onCreate(Landroid/os/Bundle;)V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "restoreAccount"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->e:Ljava/lang/String;

    .line 102
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "restoreToken"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->f:Ljava/lang/String;

    .line 103
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "accounts"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->c:Ljava/util/ArrayList;

    .line 105
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->g:Landroid/webkit/CookieManager;

    .line 106
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->i:Ljava/util/Collection;

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->j:I

    .line 108
    new-instance v0, Ljava/util/concurrent/Semaphore;

    iget v1, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->j:I

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->k:Ljava/util/concurrent/Semaphore;

    .line 109
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity$1;-><init>(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->l:Landroid/os/ResultReceiver;

    .line 118
    new-instance v0, Lcom/google/android/setupwizard/util/b;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->m:Lcom/google/android/setupwizard/util/b;

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->m:Lcom/google/android/setupwizard/util/b;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->setContentView(Landroid/view/View;)V

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    new-instance v0, Landroid/webkit/WebView;

    invoke-direct {v0, p0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->n:Landroid/webkit/WebView;

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->n:Landroid/webkit/WebView;

    new-instance v1, Lcom/google/android/gms/auth/setup/d2d/aa;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/setup/d2d/aa;-><init>(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->m:Lcom/google/android/setupwizard/util/b;

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->n:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/b;->a(Landroid/view/View;)V

    .line 144
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->i()V

    .line 147
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->j()V

    .line 148
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->i:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/AsyncTask;

    .line 165
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    goto :goto_0

    .line 167
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/auth/setup/d2d/e;->onDestroy()V

    .line 168
    return-void
.end method
