.class public final Lcom/google/android/gms/plus/audience/FaclSelectionActivity;
.super Lcom/google/android/gms/plus/audience/CircleSelectionActivity;
.source "SourceFile"


# static fields
.field private static final t:Ljava/util/Comparator;


# instance fields
.field private A:I

.field private B:Ljava/lang/String;

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lcom/google/android/gms/plus/audience/bv;

    invoke-direct {v0}, Lcom/google/android/gms/plus/audience/bv;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->t:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;-><init>()V

    .line 225
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/FaclSelectionActivity;Z)V
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->x:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/FaclSelectionActivity;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->v:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/audience/FaclSelectionActivity;Z)V
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->y:Z

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/plus/audience/FaclSelectionActivity;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->x:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/plus/audience/FaclSelectionActivity;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->u:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/plus/audience/FaclSelectionActivity;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->w:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/plus/audience/FaclSelectionActivity;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->y:Z

    return v0
.end method

.method static synthetic q()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->t:Ljava/util/Comparator;

    return-object v0
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 172
    sget v0, Lcom/google/android/gms/p;->tf:I

    return v0
.end method

.method protected final synthetic a(Landroid/content/Intent;Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/plus/audience/al;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->n()Lcom/google/android/gms/plus/audience/ac;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 95
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->a(Landroid/os/Bundle;)V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 98
    if-nez p1, :cond_4

    .line 99
    const-string v3, "HAS_SHOW_CIRCLES"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->u:Z

    .line 100
    const-string v3, "SHOW_ALL_CONTACTS_CHECKBOX"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->v:Z

    .line 101
    const-string v3, "SHOW_ALL_CIRCLES_CHECKBOX"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->w:Z

    .line 102
    const-string v3, "ALL_CONTACTS_CHECKED"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->x:Z

    .line 103
    const-string v3, "ALL_CIRCLES_CHECKED"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->y:Z

    .line 105
    const-string v3, "TITLE_LOGO"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/a/a;->a(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->A:I

    .line 108
    iget-boolean v3, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->w:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->y:Z

    if-eqz v3, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->z:Z

    .line 120
    :goto_0
    iget v0, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->A:I

    if-nez v0, :cond_2

    .line 121
    sget v0, Lcom/google/android/gms/j;->sW:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget v1, Lcom/google/android/gms/h;->cj:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 126
    :cond_2
    const-string v0, "DESCRIPTION_TEXT"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->B:Ljava/lang/String;

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->B:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 128
    sget v0, Lcom/google/android/gms/p;->tB:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->B:Ljava/lang/String;

    .line 130
    :cond_3
    return-void

    .line 110
    :cond_4
    const-string v3, "FaclSelectionActivity.HasShowCircles"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->u:Z

    .line 111
    const-string v3, "FaclSelectionActivity.ShowContacts"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->v:Z

    .line 112
    const-string v3, "FaclSelectionActivity.ShowCircles"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->w:Z

    .line 113
    const-string v3, "FaclSelectionActivity.Contacts"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->x:Z

    .line 114
    const-string v3, "FaclSelectionActivity.Circles"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->y:Z

    .line 115
    const-string v3, "FaclSelectionActivity.Title"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->A:I

    .line 116
    iget-boolean v3, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->w:Z

    if-eqz v3, :cond_5

    const-string v3, "FaclSelectionActivity.CirclesHidden"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_5
    move v0, v1

    :cond_6
    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->z:Z

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 219
    iput-boolean p1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->z:Z

    .line 220
    return-void
.end method

.method protected final d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    .prologue
    .line 521
    sget-object v0, Lcom/google/android/gms/common/analytics/d;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method protected final e()Lcom/google/android/gms/common/audience/a/d;
    .locals 2

    .prologue
    .line 177
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->e()Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->y:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/d;->a(Z)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->x:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/d;->b(Z)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    return-object v0
.end method

.method protected final f()Z
    .locals 1

    .prologue
    .line 516
    const/4 v0, 0x0

    return v0
.end method

.method protected final n()Lcom/google/android/gms/plus/audience/ac;
    .locals 17

    .prologue
    .line 147
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/audience/n;->b:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/plus/audience/n;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/plus/audience/n;->c:Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v1 .. v16}, Lcom/google/android/gms/plus/audience/ac;->a(Ljava/lang/String;Ljava/lang/String;ZZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIIILjava/util/List;)Landroid/os/Bundle;

    move-result-object v1

    .line 165
    new-instance v2, Lcom/google/android/gms/plus/audience/bw;

    invoke-direct {v2}, Lcom/google/android/gms/plus/audience/bw;-><init>()V

    .line 166
    invoke-virtual {v2, v1}, Lcom/google/android/gms/plus/audience/bw;->setArguments(Landroid/os/Bundle;)V

    .line 167
    return-object v2
.end method

.method final o()Z
    .locals 1

    .prologue
    .line 211
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->z:Z

    return v0
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 134
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 135
    const-string v0, "FaclSelectionActivity.HasShowCircles"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 136
    const-string v0, "FaclSelectionActivity.ShowContacts"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 137
    const-string v0, "FaclSelectionActivity.ShowCircles"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->w:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 138
    const-string v0, "FaclSelectionActivity.Contacts"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->x:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 139
    const-string v0, "FaclSelectionActivity.Circles"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->y:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 140
    const-string v0, "FaclSelectionActivity.Title"

    iget v1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->A:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 141
    const-string v0, "FaclSelectionActivity.CirclesHidden"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->z:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 142
    return-void
.end method

.method final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->B:Ljava/lang/String;

    return-object v0
.end method
