.class final Lcom/google/android/gms/cast/b/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/b/u;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/b/u;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/google/android/gms/cast/b/v;->a:Lcom/google/android/gms/cast/b/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 100
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/v;->a:Lcom/google/android/gms/cast/b/u;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/u;->a(Lcom/google/android/gms/cast/b/u;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/cast/b/v;->a:Lcom/google/android/gms/cast/b/u;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/u;->b(Lcom/google/android/gms/cast/b/u;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/cast/b/v;->a:Lcom/google/android/gms/cast/b/u;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/u;->c(Lcom/google/android/gms/cast/b/u;)Lcom/google/android/gms/cast/internal/k;

    move-result-object v0

    const-string v1, "**** selector loop thread exiting"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/cast/b/v;->a:Lcom/google/android/gms/cast/b/u;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/u;->e(Lcom/google/android/gms/cast/b/u;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/cast/b/v;->a:Lcom/google/android/gms/cast/b/u;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/u;->f(Lcom/google/android/gms/cast/b/u;)Ljava/lang/Thread;

    .line 110
    :goto_0
    return-void

    .line 102
    :catch_0
    move-exception v0

    .line 103
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/cast/b/v;->a:Lcom/google/android/gms/cast/b/u;

    invoke-static {v1}, Lcom/google/android/gms/cast/b/u;->c(Lcom/google/android/gms/cast/b/u;)Lcom/google/android/gms/cast/internal/k;

    move-result-object v1

    const-string v2, "Unexpected throwable in selector loop"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/internal/k;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    iget-object v1, p0, Lcom/google/android/gms/cast/b/v;->a:Lcom/google/android/gms/cast/b/u;

    invoke-static {v1, v0}, Lcom/google/android/gms/cast/b/u;->a(Lcom/google/android/gms/cast/b/u;Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/cast/b/v;->a:Lcom/google/android/gms/cast/b/u;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/u;->d(Lcom/google/android/gms/cast/b/u;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/cast/b/v;->a:Lcom/google/android/gms/cast/b/u;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/u;->c(Lcom/google/android/gms/cast/b/u;)Lcom/google/android/gms/cast/internal/k;

    move-result-object v0

    const-string v1, "**** selector loop thread exiting"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/cast/b/v;->a:Lcom/google/android/gms/cast/b/u;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/u;->e(Lcom/google/android/gms/cast/b/u;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/cast/b/v;->a:Lcom/google/android/gms/cast/b/u;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/u;->f(Lcom/google/android/gms/cast/b/u;)Ljava/lang/Thread;

    goto :goto_0

    .line 107
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/cast/b/v;->a:Lcom/google/android/gms/cast/b/u;

    invoke-static {v1}, Lcom/google/android/gms/cast/b/u;->c(Lcom/google/android/gms/cast/b/u;)Lcom/google/android/gms/cast/internal/k;

    move-result-object v1

    const-string v2, "**** selector loop thread exiting"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    iget-object v1, p0, Lcom/google/android/gms/cast/b/v;->a:Lcom/google/android/gms/cast/b/u;

    invoke-static {v1}, Lcom/google/android/gms/cast/b/u;->e(Lcom/google/android/gms/cast/b/u;)V

    .line 109
    iget-object v1, p0, Lcom/google/android/gms/cast/b/v;->a:Lcom/google/android/gms/cast/b/u;

    invoke-static {v1}, Lcom/google/android/gms/cast/b/u;->f(Lcom/google/android/gms/cast/b/u;)Ljava/lang/Thread;

    throw v0
.end method
