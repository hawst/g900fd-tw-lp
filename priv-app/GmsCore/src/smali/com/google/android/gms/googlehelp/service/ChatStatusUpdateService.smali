.class public Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;
.super Lcom/google/android/gms/googlehelp/service/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/service/a;-><init>()V

    return-void
.end method

.method public static a(JILjava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 55
    const-string v0, "com.google.android.gms.googlehelp.service.ChatStatusUpdateService.START"

    invoke-static {v0, p0, p1, p2, p3}, Lcom/google/android/gms/googlehelp/service/a;->a(Ljava/lang/String;JILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 78
    invoke-static {p0, p1}, Lcom/google/android/gms/googlehelp/service/a;->b(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_IS_VIDEO"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_CHAT_CONVERSATION_ID"

    invoke-static {p1, p2}, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 86
    invoke-static {p0}, Lcom/google/android/gms/googlehelp/common/u;->c(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    const-string v0, "com.google.android.gms.googlehelp.service.ChatStatusUpdateService.START"

    invoke-static {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
    .locals 1

    .prologue
    .line 31
    const-string v0, "com.google.android.gms.googlehelp.service.ChatStatusUpdateService.START"

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/googlehelp/service/a;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    const-string v0, "com.google.android.gms.googlehelp.service.ChatStatusUpdateService.START"

    invoke-static {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public static f()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 64
    const-string v0, "com.google.android.gms.googlehelp.service.ChatStatusUpdateService.START"

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/service/a;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final a(JILjava/lang/String;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
    .locals 3

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    if-nez v0, :cond_1

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 174
    :cond_1
    invoke-virtual {p0, p5}, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->c(Lcom/google/android/gms/googlehelp/common/HelpConfig;)I

    move-result v0

    if-ge p3, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {p5}, Lcom/google/android/gms/googlehelp/common/u;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 177
    new-instance v0, Lcom/google/ad/a/a/b;

    invoke-direct {v0}, Lcom/google/ad/a/a/b;-><init>()V

    .line 178
    iput-wide p1, v0, Lcom/google/ad/a/a/b;->c:J

    .line 179
    iput p3, v0, Lcom/google/ad/a/a/b;->a:I

    .line 180
    iput-object p4, v0, Lcom/google/ad/a/a/b;->b:Ljava/lang/String;

    .line 181
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {p5, v1, v0}, Lcom/google/android/gms/googlehelp/common/e;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;Lcom/google/ad/a/a/b;)V

    goto :goto_0
.end method

.method final a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
    .locals 6

    .prologue
    const-wide/16 v2, -0x1

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    if-nez v0, :cond_0

    .line 134
    :goto_0
    new-instance v0, Lcom/google/android/gms/googlehelp/c/a/a;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-direct {v0, p0, p1, p0, v1}, Lcom/google/android/gms/googlehelp/c/a/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/c/a/p;Lcom/google/android/gms/googlehelp/common/a;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->e:Lcom/google/android/gms/googlehelp/c/a/n;

    .line 136
    iget-object v4, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->e:Lcom/google/android/gms/googlehelp/c/a/n;

    iget-object v5, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->e:Lcom/google/android/gms/googlehelp/c/a/n;

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/googlehelp/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;JLcom/android/volley/x;Lcom/android/volley/w;)V

    .line 138
    return-void

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {p1}, Lcom/google/android/gms/googlehelp/common/u;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;J)J

    move-result-wide v2

    goto :goto_0
.end method

.method final b(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final c(Lcom/google/android/gms/googlehelp/common/HelpConfig;)I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 148
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {p1}, Lcom/google/android/gms/googlehelp/common/u;->e(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method final d(Lcom/google/android/gms/googlehelp/common/HelpConfig;)J
    .locals 4

    .prologue
    const-wide/16 v0, -0x1

    .line 158
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {p1}, Lcom/google/android/gms/googlehelp/common/u;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 239
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/a;->b()Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    const-string v1, "hangout_was_opened"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;Z)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 245
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/googlehelp/service/a;->d()V

    .line 246
    return-void
.end method

.method final e(Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    if-nez v0, :cond_0

    .line 193
    :goto_0
    return-void

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {p1, v0}, Lcom/google/android/gms/googlehelp/common/e;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)V

    goto :goto_0
.end method

.method final f(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 202
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.googlehelp.HelpFragment.CHAT_READY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "EXTRA_CHAT_CONVERSATION_ID"

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {p1, v2}, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    const-string v0, "com.google.android.gms.googlehelp.service.ChatStatusUpdateService.START"

    return-object v0
.end method

.method final g(Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
    .locals 0

    .prologue
    .line 209
    invoke-static {p0, p1}, Lcom/google/android/gms/googlehelp/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    .line 210
    return-void
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 98
    const/16 v0, 0x7de

    return v0
.end method

.method final h(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method final i()I
    .locals 1

    .prologue
    .line 103
    const/16 v0, 0x1006

    return v0
.end method

.method final j()I
    .locals 1

    .prologue
    .line 108
    const/16 v0, 0x2775

    return v0
.end method

.method final k()I
    .locals 1

    .prologue
    .line 113
    sget v0, Lcom/google/android/gms/h;->aT:I

    return v0
.end method

.method final l()I
    .locals 1

    .prologue
    .line 118
    sget v0, Lcom/google/android/gms/p;->oy:I

    return v0
.end method

.method final m()I
    .locals 1

    .prologue
    .line 123
    sget v0, Lcom/google/android/gms/p;->oz:I

    return v0
.end method

.method final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    const-string v0, "com.google.android.gms.googlehelp.HelpFragment.CHAT_STATUS_UPDATE"

    return-object v0
.end method

.method protected final o()V
    .locals 3

    .prologue
    .line 219
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->f:Z

    if-nez v0, :cond_0

    .line 220
    invoke-super {p0}, Lcom/google/android/gms/googlehelp/service/a;->o()V

    .line 228
    :goto_0
    return-void

    .line 226
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->p()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_IS_HANGOUT_ENDED"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method
