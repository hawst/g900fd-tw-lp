.class public final Lcom/google/android/gms/auth/be/proximity/authorization/a/c/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/auth/authzen/keyservice/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 611
    new-instance v0, Lcom/google/android/gms/auth/authzen/keyservice/g;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/authzen/keyservice/g;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/c;->a:Lcom/google/android/gms/auth/authzen/keyservice/g;

    .line 612
    return-void
.end method


# virtual methods
.method public final a()Ljava/security/KeyPair;
    .locals 3

    .prologue
    .line 616
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/c;->a:Lcom/google/android/gms/auth/authzen/keyservice/g;

    const-string v1, "device_key"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/authzen/keyservice/g;->a(Ljava/lang/String;)Ljava/security/KeyPair;
    :try_end_0
    .catch Lcom/google/android/gms/auth/authzen/keyservice/h; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 618
    :catch_0
    move-exception v0

    .line 619
    :goto_0
    new-instance v1, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v2, "Failed to get AuthZen key."

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 618
    :catch_1
    move-exception v0

    goto :goto_0
.end method
