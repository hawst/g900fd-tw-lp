.class final Lcom/google/android/gms/car/support/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final a:I

.field b:I

.field c:I

.field d:Z

.field final synthetic e:Lcom/google/android/gms/car/support/u;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/support/u;I)V
    .locals 1

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/gms/car/support/v;->e:Lcom/google/android/gms/car/support/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/v;->d:Z

    .line 42
    iput p2, p0, Lcom/google/android/gms/car/support/v;->a:I

    .line 43
    invoke-virtual {p1}, Lcom/google/android/gms/car/support/u;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/v;->b:I

    .line 44
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 2

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/gms/car/support/v;->c:I

    iget v1, p0, Lcom/google/android/gms/car/support/v;->b:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/car/support/v;->e:Lcom/google/android/gms/car/support/u;

    iget v1, p0, Lcom/google/android/gms/car/support/v;->c:I

    iget v2, p0, Lcom/google/android/gms/car/support/v;->a:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/car/support/u;->a(II)Ljava/lang/Object;

    move-result-object v0

    .line 55
    iget v1, p0, Lcom/google/android/gms/car/support/v;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/gms/car/support/v;->c:I

    .line 56
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/car/support/v;->d:Z

    .line 57
    return-object v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/v;->d:Z

    if-nez v0, :cond_0

    .line 63
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 65
    :cond_0
    iget v0, p0, Lcom/google/android/gms/car/support/v;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/support/v;->c:I

    .line 66
    iget v0, p0, Lcom/google/android/gms/car/support/v;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/support/v;->b:I

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/v;->d:Z

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/car/support/v;->e:Lcom/google/android/gms/car/support/u;

    iget v1, p0, Lcom/google/android/gms/car/support/v;->c:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/u;->a(I)V

    .line 69
    return-void
.end method
