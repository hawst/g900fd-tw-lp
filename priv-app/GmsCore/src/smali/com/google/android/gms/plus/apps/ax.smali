.class public final Lcom/google/android/gms/plus/apps/ax;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lcom/google/android/gms/plus/apps/ay;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Ljava/util/ArrayList;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/plus/apps/ay;)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ax;->c:Ljava/util/ArrayList;

    .line 72
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ax;->b:Landroid/view/LayoutInflater;

    .line 73
    iput-object p2, p0, Lcom/google/android/gms/plus/apps/ax;->a:Lcom/google/android/gms/plus/apps/ay;

    .line 74
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ax;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ax;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ax;->notifyDataSetChanged()V

    .line 124
    return-void
.end method

.method public final a(Ljava/util/List;Z)V
    .locals 5

    .prologue
    .line 184
    iput-boolean p2, p0, Lcom/google/android/gms/plus/apps/ax;->d:Z

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ax;->a:Lcom/google/android/gms/plus/apps/ay;

    invoke-interface {v0}, Lcom/google/android/gms/plus/apps/ay;->e()Ljava/util/ArrayList;

    move-result-object v2

    .line 186
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 187
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 188
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;

    .line 189
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 191
    iget-object v4, p0, Lcom/google/android/gms/plus/apps/ax;->c:Ljava/util/ArrayList;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;->c()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 194
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ax;->notifyDataSetChanged()V

    .line 195
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 198
    const/4 v0, 0x0

    .line 199
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ax;->a:Lcom/google/android/gms/plus/apps/ay;

    invoke-interface {v1}, Lcom/google/android/gms/plus/apps/ay;->e()Ljava/util/ArrayList;

    move-result-object v2

    .line 200
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ax;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 203
    const/4 v0, 0x1

    move v1, v0

    goto :goto_0

    .line 206
    :cond_1
    if-eqz v1, :cond_2

    .line 207
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ax;->notifyDataSetChanged()V

    .line 209
    :cond_2
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ax;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 213
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ax;->d:Z

    .line 214
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ax;->notifyDataSetChanged()V

    .line 215
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ax;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 219
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ax;->d:Z

    .line 220
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ax;->notifyDataSetInvalidated()V

    .line 221
    return-void
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ax;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ax;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/apps/ax;->a(I)Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 141
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ax;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ax;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_6

    .line 152
    if-nez p2, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ax;->b:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->dP:I

    invoke-virtual {v0, v1, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ax;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;->ad_()Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;

    move-result-object v2

    sget v1, Lcom/google/android/gms/j;->jV:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;->b()Ljava/lang/String;

    move-result-object v2

    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/plus/apps/ax;->a:Lcom/google/android/gms/plus/apps/ay;

    invoke-interface {v4, v2}, Lcom/google/android/gms/plus/apps/ay;->a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    sget v1, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;->ac_()Z

    move-result v1

    if-eqz v1, :cond_3

    :try_start_0
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/plus/apps/x;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/apps/x;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/apps/x;->a()J

    move-result-wide v4

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v4, v5}, Lcom/google/android/gms/plus/apps/y;->a(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_1
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;->d()Lcom/google/android/gms/plus/service/v1whitelisted/models/t;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/t;->d()Ljava/lang/String;

    move-result-object v0

    :goto_2
    if-nez v1, :cond_5

    move-object v1, v0

    :cond_1
    :goto_3
    sget v0, Lcom/google/android/gms/j;->sN:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    :goto_4
    return-object p2

    :cond_2
    move-object v2, v3

    .line 156
    goto :goto_0

    :catch_0
    move-exception v1

    :cond_3
    move-object v1, v3

    goto :goto_1

    :cond_4
    move-object v0, v3

    goto :goto_2

    :cond_5
    if-eqz v0, :cond_1

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->tW:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v6

    const/4 v1, 0x1

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 159
    :cond_6
    if-nez p2, :cond_7

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ax;->b:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->dO:I

    invoke-virtual {v0, v1, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 163
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ax;->a:Lcom/google/android/gms/plus/apps/ay;

    invoke-interface {v0}, Lcom/google/android/gms/plus/apps/ay;->f()V

    goto :goto_4
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x2

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    return v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 180
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ax;->a:Lcom/google/android/gms/plus/apps/ay;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;

    invoke-interface {v1, v0}, Lcom/google/android/gms/plus/apps/ay;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ec;)V

    .line 181
    return-void
.end method
