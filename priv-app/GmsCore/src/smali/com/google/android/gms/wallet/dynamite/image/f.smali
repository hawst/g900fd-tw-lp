.class public final Lcom/google/android/gms/wallet/dynamite/image/f;
.super Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/dynamite/image/d;

.field private c:Ljava/lang/Object;

.field private final d:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/dynamite/image/d;Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    .line 236
    iput-object p1, p0, Lcom/google/android/gms/wallet/dynamite/image/f;->a:Lcom/google/android/gms/wallet/dynamite/image/d;

    .line 237
    invoke-virtual {p2}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;-><init>(Landroid/content/Context;)V

    .line 238
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/image/f;->d:Ljava/lang/ref/WeakReference;

    .line 239
    return-void
.end method

.method private a()Landroid/widget/ImageView;
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/image/f;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 289
    invoke-static {v0}, Lcom/google/android/gms/wallet/dynamite/image/d;->a(Landroid/widget/ImageView;)Lcom/google/android/gms/wallet/dynamite/image/f;

    move-result-object v1

    .line 291
    if-ne p0, v1, :cond_0

    .line 295
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/dynamite/image/f;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/image/f;->c:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 229
    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/image/f;->c:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/image/f;->c:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gms/wallet/dynamite/image/f;->a:Lcom/google/android/gms/wallet/dynamite/image/d;

    iget-object v2, v2, Lcom/google/android/gms/wallet/dynamite/image/d;->d:Lcom/google/android/gms/wallet/dynamite/image/a;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/dynamite/image/f;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/image/f;->a()Landroid/widget/ImageView;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/wallet/dynamite/image/f;->a:Lcom/google/android/gms/wallet/dynamite/image/d;

    iget-boolean v2, v2, Lcom/google/android/gms/wallet/dynamite/image/d;->a:Z

    if-nez v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/image/f;->a:Lcom/google/android/gms/wallet/dynamite/image/d;

    iget-object v0, v0, Lcom/google/android/gms/wallet/dynamite/image/d;->d:Lcom/google/android/gms/wallet/dynamite/image/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/dynamite/image/a;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/dynamite/image/f;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/image/f;->a()Landroid/widget/ImageView;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/wallet/dynamite/image/f;->a:Lcom/google/android/gms/wallet/dynamite/image/d;

    iget-boolean v2, v2, Lcom/google/android/gms/wallet/dynamite/image/d;->a:Z

    if-nez v2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/image/f;->a:Lcom/google/android/gms/wallet/dynamite/image/d;

    iget-object v2, p0, Lcom/google/android/gms/wallet/dynamite/image/f;->c:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/dynamite/image/d;->a(Ljava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/wallet/dynamite/image/f;->a:Lcom/google/android/gms/wallet/dynamite/image/d;

    iget-object v2, v2, Lcom/google/android/gms/wallet/dynamite/image/d;->d:Lcom/google/android/gms/wallet/dynamite/image/a;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/wallet/dynamite/image/f;->a:Lcom/google/android/gms/wallet/dynamite/image/d;

    iget-object v2, v2, Lcom/google/android/gms/wallet/dynamite/image/d;->d:Lcom/google/android/gms/wallet/dynamite/image/a;

    iget-object v3, v2, Lcom/google/android/gms/wallet/dynamite/image/a;->a:Lcom/google/android/gms/common/internal/a/b;

    invoke-virtual {v3, v1}, Lcom/google/android/gms/common/internal/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    iget-object v2, v2, Lcom/google/android/gms/wallet/dynamite/image/a;->a:Lcom/google/android/gms/common/internal/a/b;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/gms/common/internal/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 229
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/dynamite/image/f;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/image/f;->a:Lcom/google/android/gms/wallet/dynamite/image/d;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/dynamite/image/d;->a:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 p1, 0x0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/image/f;->a()Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/image/f;->a:Lcom/google/android/gms/wallet/dynamite/image/d;

    invoke-static {v1, v0, p1}, Lcom/google/android/gms/wallet/dynamite/image/d;->a(Lcom/google/android/gms/wallet/dynamite/image/d;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    :cond_2
    return-void
.end method
