.class final Lcom/google/android/gms/ads/internal/request/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/ads/internal/request/g;

.field final synthetic b:Lcom/google/android/gms/ads/internal/request/k;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/request/k;Lcom/google/android/gms/ads/internal/request/g;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/o;->b:Lcom/google/android/gms/ads/internal/request/k;

    iput-object p2, p0, Lcom/google/android/gms/ads/internal/request/o;->a:Lcom/google/android/gms/ads/internal/request/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/o;->b:Lcom/google/android/gms/ads/internal/request/k;

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/request/k;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 289
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/o;->b:Lcom/google/android/gms/ads/internal/request/k;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->e:I

    const/4 v2, -0x2

    if-eq v0, v2, :cond_0

    .line 290
    monitor-exit v1

    .line 295
    :goto_0
    return-void

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/o;->b:Lcom/google/android/gms/ads/internal/request/k;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->f()Lcom/google/android/gms/ads/internal/p/c;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/request/o;->b:Lcom/google/android/gms/ads/internal/request/k;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/ads/internal/p/c;->a(Lcom/google/android/gms/ads/internal/p/e;)V

    .line 294
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/o;->a:Lcom/google/android/gms/ads/internal/request/g;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/request/o;->b:Lcom/google/android/gms/ads/internal/request/k;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/ads/internal/request/g;->a(Lcom/google/android/gms/ads/internal/request/AdResponseParcel;)V

    .line 295
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
