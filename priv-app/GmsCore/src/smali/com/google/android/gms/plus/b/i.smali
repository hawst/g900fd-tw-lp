.class final Lcom/google/android/gms/plus/b/i;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(Landroid/content/res/Resources;Ljava/util/Locale;ZILjava/util/ArrayList;)Lcom/google/android/gms/plus/b/j;
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 99
    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    rsub-int/lit8 v0, v0, 0x4

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 102
    new-instance v4, Lcom/google/android/gms/plus/b/j;

    invoke-direct {v4}, Lcom/google/android/gms/plus/b/j;-><init>()V

    .line 105
    if-nez p2, :cond_2

    invoke-virtual {p4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 106
    invoke-static {p0, p1, p3}, Lcom/google/android/gms/plus/b/i;->a(Landroid/content/res/Resources;Ljava/util/Locale;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/plus/b/j;->d:Ljava/lang/String;

    .line 108
    if-nez p3, :cond_1

    .line 110
    sget v0, Lcom/google/android/gms/p;->uv:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/plus/b/j;->a:Ljava/lang/String;

    .line 112
    sget v0, Lcom/google/android/gms/p;->uw:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/plus/b/j;->b:Ljava/lang/String;

    move-object v0, v4

    .line 188
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 99
    goto :goto_0

    .line 116
    :cond_1
    sget v0, Lcom/google/android/gms/p;->uu:I

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v2

    sget v5, Lcom/google/android/gms/p;->uv:I

    invoke-virtual {p0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v1

    invoke-virtual {p0, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/plus/b/j;->a:Ljava/lang/String;

    .line 120
    sget v0, Lcom/google/android/gms/p;->uu:I

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v2

    sget v5, Lcom/google/android/gms/p;->uw:I

    invoke-virtual {p0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v1

    invoke-virtual {p0, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/plus/b/j;->b:Ljava/lang/String;

    .line 123
    sget v0, Lcom/google/android/gms/p;->ut:I

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/plus/b/j;->c:Ljava/lang/String;

    move-object v0, v4

    .line 125
    goto :goto_1

    .line 130
    :cond_2
    if-eqz p2, :cond_5

    move v0, v1

    :goto_2
    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/2addr v3, v0

    .line 131
    if-ge p3, v3, :cond_4

    .line 132
    const-string v0, "PlusOneStrings"

    const/4 v5, 0x3

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 133
    const-string v5, "PlusOneStrings"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "Global count is "

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " but +1 has been "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-eqz p2, :cond_6

    const-string v0, "set by viewer"

    :goto_3
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-eqz p2, :cond_7

    invoke-virtual {p4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, " and "

    :goto_4
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v8, "+1\'d by "

    invoke-direct {v0, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, " friend(s)"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move p3, v3

    .line 142
    :cond_4
    invoke-static {p0, p1, p3}, Lcom/google/android/gms/plus/b/i;->a(Landroid/content/res/Resources;Ljava/util/Locale;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/plus/b/j;->d:Ljava/lang/String;

    .line 145
    const/4 v0, 0x0

    .line 148
    if-eqz p2, :cond_a

    .line 149
    if-eqz p2, :cond_9

    if-ne p3, v1, :cond_9

    .line 151
    sget v0, Lcom/google/android/gms/p;->uJ:I

    new-array v1, v1, [Ljava/lang/Object;

    sget v3, Lcom/google/android/gms/p;->uK:I

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/plus/b/j;->a:Ljava/lang/String;

    .line 155
    sget v0, Lcom/google/android/gms/p;->uK:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/plus/b/j;->b:Ljava/lang/String;

    move-object v0, v4

    .line 156
    goto/16 :goto_1

    :cond_5
    move v0, v2

    .line 130
    goto/16 :goto_2

    .line 133
    :cond_6
    const-string v0, ""

    goto :goto_3

    :cond_7
    const-string v0, ""

    goto :goto_4

    :cond_8
    const-string v0, ""

    goto :goto_5

    .line 159
    :cond_9
    sget v0, Lcom/google/android/gms/p;->uH:I

    new-array v3, v1, [Ljava/lang/Object;

    sget v5, Lcom/google/android/gms/p;->uK:I

    invoke-virtual {p0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v2

    invoke-virtual {p0, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_a
    move v3, v2

    move-object v5, v0

    .line 164
    :goto_6
    if-ge v3, v6, :cond_c

    .line 166
    invoke-virtual {p4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 169
    if-nez v5, :cond_b

    .line 171
    sget v5, Lcom/google/android/gms/p;->uG:I

    new-array v7, v1, [Ljava/lang/Object;

    aput-object v0, v7, v2

    invoke-virtual {p0, v5, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 164
    :goto_7
    add-int/lit8 v3, v3, 0x1

    move-object v5, v0

    goto :goto_6

    .line 176
    :cond_b
    sget v7, Lcom/google/android/gms/p;->uI:I

    new-array v8, v9, [Ljava/lang/Object;

    aput-object v5, v8, v2

    aput-object v0, v8, v1

    invoke-virtual {p0, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 183
    :cond_c
    sget v0, Lcom/google/android/gms/p;->uu:I

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v2

    aput-object v5, v3, v1

    invoke-virtual {p0, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/plus/b/j;->a:Ljava/lang/String;

    .line 186
    sget v0, Lcom/google/android/gms/p;->ut:I

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/plus/b/j;->b:Ljava/lang/String;

    move-object v0, v4

    .line 188
    goto/16 :goto_1
.end method

.method private static a(Landroid/content/res/Resources;Ljava/util/Locale;I)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 201
    if-ltz p2, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Count must be non-negative."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 203
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p2}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v3, Ljava/math/MathContext;

    const/4 v4, 0x2

    invoke-direct {v3, v4}, Ljava/math/MathContext;-><init>(I)V

    invoke-virtual {v0, v3}, Ljava/math/BigDecimal;->round(Ljava/math/MathContext;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValue()I

    move-result v0

    .line 204
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    .line 205
    invoke-static {p1}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v4

    .line 207
    const/16 v5, 0x3e8

    if-ge p2, v5, :cond_1

    .line 208
    int-to-long v0, p2

    invoke-virtual {v4, v0, v1}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    .line 223
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 201
    goto :goto_0

    .line 209
    :cond_1
    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 210
    const/4 v5, 0x6

    if-gt v3, v5, :cond_2

    .line 211
    sget v3, Lcom/google/android/gms/p;->uz:I

    new-array v1, v1, [Ljava/lang/Object;

    int-to-double v6, v0

    const-wide v8, 0x408f400000000000L    # 1000.0

    div-double/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-virtual {p0, v3, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 213
    :cond_2
    const/16 v5, 0x9

    if-gt v3, v5, :cond_3

    .line 214
    sget v3, Lcom/google/android/gms/p;->uy:I

    new-array v1, v1, [Ljava/lang/Object;

    int-to-double v6, v0

    const-wide v8, 0x412e848000000000L    # 1000000.0

    div-double/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-virtual {p0, v3, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 217
    :cond_3
    sget v0, Lcom/google/android/gms/p;->uB:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 219
    :cond_4
    const/16 v0, 0x2710

    if-ge p2, v0, :cond_5

    .line 220
    int-to-long v0, p2

    invoke-virtual {v4, v0, v1}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 223
    :cond_5
    sget v0, Lcom/google/android/gms/p;->uA:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
