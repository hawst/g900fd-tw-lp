.class final Lcom/google/android/gms/wallet/ia/t;
.super Lcom/google/android/gms/wallet/service/l;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ia/q;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/ia/q;)V
    .locals 0

    .prologue
    .line 1221
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1430
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    .line 1431
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    const-string v1, "com.google.android.libraries.inapp.ERROR_CODE_CANNOT_AUTHENTICATE"

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ia/q;->d(Lcom/google/android/gms/wallet/ia/q;Ljava/lang/String;)V

    .line 1432
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/al;)V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v1, 0x0

    const/4 v12, 0x0

    .line 1224
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->j(Lcom/google/android/gms/wallet/ia/q;)Z

    .line 1225
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iput-object p1, v0, Lcom/google/android/gms/wallet/ia/q;->C:Lcom/google/checkout/inapp/proto/al;

    .line 1227
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/al;->c:Lcom/google/checkout/inapp/proto/b;

    if-eqz v0, :cond_0

    .line 1228
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/q;->a:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    iget-object v2, p1, Lcom/google/checkout/inapp/proto/al;->c:Lcom/google/checkout/inapp/proto/b;

    iget-object v2, v2, Lcom/google/checkout/inapp/proto/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(Ljava/lang/String;)V

    .line 1231
    :cond_0
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/al;->d:Lcom/google/checkout/inapp/proto/d;

    if-eqz v0, :cond_1

    .line 1232
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v2, p1, Lcom/google/checkout/inapp/proto/al;->d:Lcom/google/checkout/inapp/proto/d;

    iget-boolean v2, v2, Lcom/google/checkout/inapp/proto/d;->h:Z

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/android/gms/wallet/ia/q;Z)Z

    .line 1234
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v2, p1, Lcom/google/checkout/inapp/proto/al;->d:Lcom/google/checkout/inapp/proto/d;

    iget-object v2, v2, Lcom/google/checkout/inapp/proto/d;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/android/gms/wallet/ia/q;Ljava/lang/String;)Ljava/lang/String;

    .line 1236
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-boolean v2, p1, Lcom/google/checkout/inapp/proto/al;->h:Z

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/ia/q;->b(Lcom/google/android/gms/wallet/ia/q;Z)Z

    .line 1237
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {p1}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/checkout/inapp/proto/al;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/android/gms/wallet/ia/q;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 1239
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/ia/q;->b(Lcom/google/android/gms/wallet/ia/q;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 1243
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/al;->e:[Ljava/lang/String;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    .line 1244
    const/16 v5, 0x85

    invoke-static {v4}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->b(Ljava/lang/String;)I

    move-result v6

    if-eq v5, v6, :cond_2

    .line 1245
    iget-object v5, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v5}, Lcom/google/android/gms/wallet/ia/q;->i(Lcom/google/android/gms/wallet/ia/q;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1243
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1249
    :cond_3
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/al;->a:Lcom/google/checkout/inapp/proto/l;

    if-eqz v0, :cond_4

    .line 1250
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v2, p1, Lcom/google/checkout/inapp/proto/al;->a:Lcom/google/checkout/inapp/proto/l;

    iget-object v2, v2, Lcom/google/checkout/inapp/proto/l;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/ia/q;->b(Lcom/google/android/gms/wallet/ia/q;Ljava/lang/String;)Ljava/lang/String;

    .line 1254
    :cond_4
    iget v0, p1, Lcom/google/checkout/inapp/proto/al;->i:I

    packed-switch v0, :pswitch_data_0

    .line 1284
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->l(Lcom/google/android/gms/wallet/ia/q;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1285
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v2, p1, Lcom/google/checkout/inapp/proto/al;->d:Lcom/google/checkout/inapp/proto/d;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/android/gms/wallet/ia/q;Lcom/google/checkout/inapp/proto/d;)V

    .line 1288
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v2, p1, Lcom/google/checkout/inapp/proto/al;->m:[Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/android/gms/wallet/ia/q;[Ljava/lang/String;)V

    .line 1289
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v2, p1, Lcom/google/checkout/inapp/proto/al;->f:[Lcom/google/checkout/inapp/proto/h;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/al;->j:Ljava/lang/String;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/android/gms/wallet/ia/q;[Lcom/google/checkout/inapp/proto/h;Ljava/lang/String;)V

    .line 1290
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v2, p1, Lcom/google/checkout/inapp/proto/al;->b:[Lcom/google/checkout/inapp/proto/n;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/android/gms/wallet/ia/q;[Lcom/google/checkout/inapp/proto/n;)V

    .line 1291
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->m(Lcom/google/android/gms/wallet/ia/q;)V

    .line 1296
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/q;->r:Lcom/google/checkout/inapp/proto/n;

    if-eqz v0, :cond_6

    .line 1297
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ia/q;->a()V

    .line 1301
    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->n(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->o(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1303
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->n(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/q;->o(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/android/apps/common/a/a/h;

    move-result-object v2

    new-array v3, v13, [Ljava/lang/String;

    const-string v4, "create_to_ui_populated"

    aput-object v4, v3, v1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/common/a/a/i;->a(Lcom/google/android/apps/common/a/a/h;[Ljava/lang/String;)Z

    .line 1305
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ia/q;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/q;->n(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/apps/common/a/a/i;)V

    .line 1306
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->p(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/android/apps/common/a/a/i;

    .line 1307
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->q(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/android/apps/common/a/a/h;

    .line 1309
    :cond_7
    return-void

    .line 1256
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/q;->b(Lcom/google/android/gms/wallet/ia/q;)Landroid/accounts/Account;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v3}, Lcom/google/android/gms/wallet/ia/q;->c(Lcom/google/android/gms/wallet/ia/q;)Z

    move-result v3

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v4}, Lcom/google/android/gms/wallet/ia/q;->c(Lcom/google/android/gms/wallet/ia/q;)Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v5}, Lcom/google/android/gms/wallet/ia/q;->h(Lcom/google/android/gms/wallet/ia/q;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p1, Lcom/google/checkout/inapp/proto/al;->l:[Lcom/google/checkout/inapp/proto/aj;

    invoke-static {v6}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/aj;)Ljava/util/ArrayList;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v7}, Lcom/google/android/gms/wallet/ia/q;->d(Lcom/google/android/gms/wallet/ia/q;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v8}, Lcom/google/android/gms/wallet/ia/q;->e(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/checkout/inapp/proto/q;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v9}, Lcom/google/android/gms/wallet/ia/q;->k(Lcom/google/android/gms/wallet/ia/q;)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    const-string v11, "com.google.android.gms"

    invoke-virtual {v10, v11}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v11, "com.google.android.gms.wallet.ACTION_CREATE_PROFILE"

    invoke-virtual {v10, v11}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v11, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v10, v11, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.account"

    invoke-virtual {v10, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.brokerAndRelationships"

    invoke-virtual {v10, v0, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.requiresInstrument"

    invoke-virtual {v10, v0, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.requiresCreditCardFullAddress"

    invoke-virtual {v10, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.phoneNumberRequired"

    invoke-virtual {v10, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.defaultCountryCode"

    invoke-virtual {v10, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.addressHints"

    invoke-static {v10, v0, v12}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V

    const-string v0, "legalDocsForCountries"

    invoke-virtual {v10, v0, v6}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.countrySpecificationsFilter"

    invoke-virtual {v10, v0, v12}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.unadjustedCartId"

    invoke-virtual {v10, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.accountReference"

    invoke-static {v10, v0, v8}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    const-string v0, "com.google.android.gms.wallet.infoText"

    invoke-virtual {v10, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1271
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    const/16 v2, 0x1f6

    invoke-virtual {v0, v10, v2}, Lcom/google/android/gms/wallet/ia/q;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 1274
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    sget v3, Lcom/google/android/gms/p;->An:I

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wallet/ia/q;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google.android.libraries.inapp.ERROR_PAYMENT_FAILED"

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/android/gms/wallet/ia/q;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1279
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    sget v3, Lcom/google/android/gms/p;->Cu:I

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wallet/ia/q;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google.android.libraries.inapp.ERROR_PAYMENT_FAILED"

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/android/gms/wallet/ia/q;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1254
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/checkout/inapp/proto/an;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x0

    .line 1313
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->m(Lcom/google/android/gms/wallet/ia/q;)V

    .line 1314
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    .line 1316
    iget v0, p1, Lcom/google/checkout/inapp/proto/an;->b:I

    packed-switch v0, :pswitch_data_0

    .line 1397
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    sget v2, Lcom/google/android/gms/p;->CQ:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/ia/q;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.libraries.inapp.ERROR_CODE_UNKNOWN"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/android/gms/wallet/ia/q;Ljava/lang/String;Ljava/lang/String;)V

    .line 1402
    :goto_0
    return-void

    .line 1319
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v1, p1, Lcom/google/checkout/inapp/proto/an;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ia/q;->c(Lcom/google/android/gms/wallet/ia/q;Ljava/lang/String;)V

    goto :goto_0

    .line 1322
    :pswitch_2
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/an;->c:[I

    array-length v0, v0

    if-nez v0, :cond_0

    .line 1323
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->r(Lcom/google/android/gms/wallet/ia/q;)V

    goto :goto_0

    .line 1328
    :cond_0
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/an;->c:[I

    aget v0, v0, v1

    .line 1329
    packed-switch v0, :pswitch_data_1

    .line 1347
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->r(Lcom/google/android/gms/wallet/ia/q;)V

    goto :goto_0

    .line 1331
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/q;->r:Lcom/google/checkout/inapp/proto/n;

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    .line 1333
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/q;->b(Lcom/google/android/gms/wallet/ia/q;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v4}, Lcom/google/android/gms/wallet/ia/q;->d(Lcom/google/android/gms/wallet/ia/q;)Ljava/lang/String;

    move-result-object v5

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v4}, Lcom/google/android/gms/wallet/ia/q;->e(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/checkout/inapp/proto/q;

    move-result-object v6

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v4}, Lcom/google/android/gms/wallet/ia/q;->f(Lcom/google/android/gms/wallet/ia/q;)Ljava/util/ArrayList;

    move-result-object v7

    move v4, v3

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/j;ZZLjava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;Lcom/google/checkout/inapp/proto/a/b;)Landroid/content/Intent;

    move-result-object v0

    .line 1343
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    const/16 v2, 0x1fa

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/wallet/ia/q;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1357
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v8, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    .line 1358
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iput-boolean v3, v0, Lcom/google/android/gms/wallet/ia/q;->v:Z

    .line 1359
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/q;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0, v8}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    .line 1360
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iput-object v8, v0, Lcom/google/android/gms/wallet/ia/q;->C:Lcom/google/checkout/inapp/proto/al;

    .line 1362
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->s(Lcom/google/android/gms/wallet/ia/q;)V

    .line 1365
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/ia/q;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->Ay:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/q;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ia/q;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1369
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    sget v2, Lcom/google/android/gms/p;->Cu:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/ia/q;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.libraries.inapp.ERROR_PAYMENT_FAILED"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/android/gms/wallet/ia/q;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1374
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    sget v2, Lcom/google/android/gms/p;->An:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/ia/q;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.libraries.inapp.ERROR_PAYMENT_FAILED"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/android/gms/wallet/ia/q;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1380
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->r(Lcom/google/android/gms/wallet/ia/q;)V

    goto/16 :goto_0

    .line 1383
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    const-string v1, "com.google.android.libraries.inapp.ERROR_DELIVERY_FAILED"

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ia/q;->d(Lcom/google/android/gms/wallet/ia/q;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1387
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    const-string v1, "com.google.android.libraries.inapp.ERROR_CODE_SELF_PURCHASE"

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ia/q;->d(Lcom/google/android/gms/wallet/ia/q;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1390
    :pswitch_a
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/ia/q;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->Ca:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/q;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ia/q;->e(Lcom/google/android/gms/wallet/ia/q;Ljava/lang/String;)V

    .line 1392
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/an;->d:Lcom/google/checkout/inapp/proto/al;

    .line 1394
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/t;->a(Lcom/google/checkout/inapp/proto/al;)V

    goto/16 :goto_0

    .line 1316
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_1
        :pswitch_0
        :pswitch_7
        :pswitch_6
        :pswitch_4
        :pswitch_4
        :pswitch_8
        :pswitch_5
        :pswitch_9
        :pswitch_2
        :pswitch_1
        :pswitch_a
    .end packed-switch

    .line 1329
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
    .end packed-switch
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1424
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    .line 1425
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    const-string v1, "com.google.android.libraries.inapp.ERROR_CODE_UNKNOWN"

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ia/q;->d(Lcom/google/android/gms/wallet/ia/q;Ljava/lang/String;)V

    .line 1426
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1413
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    .line 1414
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->t(Lcom/google/android/gms/wallet/ia/q;)Z

    .line 1415
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->u(Lcom/google/android/gms/wallet/ia/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1416
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->v(Lcom/google/android/gms/wallet/ia/q;)V

    .line 1420
    :goto_0
    return-void

    .line 1418
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->w(Lcom/google/android/gms/wallet/ia/q;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1407
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    .line 1408
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/t;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->r(Lcom/google/android/gms/wallet/ia/q;)V

    .line 1409
    return-void
.end method
