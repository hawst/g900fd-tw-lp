.class public final Lcom/google/android/gms/people/c/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:[Ljava/lang/String;

.field public b:[S

.field c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 10

    .prologue
    const/16 v9, 0x5200

    const/4 v0, 0x0

    .line 205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 207
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 208
    new-instance v5, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    sget v6, Lcom/google/android/gms/o;->e:I

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v5, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 212
    :try_start_0
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 213
    new-array v1, v6, [Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/people/c/a/b;->a:[Ljava/lang/String;

    move v1, v0

    .line 216
    :goto_0
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    .line 217
    if-nez v7, :cond_0

    .line 218
    if-eq v1, v6, :cond_1

    .line 219
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Mismatched counts."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    :catch_0
    move-exception v0

    .line 226
    :try_start_1
    const-string v1, "PeoplePinyin"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Malformed or missing input files for HanziPinyin."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/people/internal/as;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/people/c/a/b;->c:Z

    .line 229
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/people/c/a/b;->a:[Ljava/lang/String;

    .line 230
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/people/c/a/b;->b:[S
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 233
    invoke-static {v5}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 277
    :goto_1
    return-void

    .line 223
    :cond_0
    :try_start_2
    iget-object v8, p0, Lcom/google/android/gms/people/c/a/b;->a:[Ljava/lang/String;

    aput-object v7, v8, v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 215
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 233
    :cond_1
    invoke-static {v5}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 237
    new-instance v5, Ljava/io/BufferedInputStream;

    sget v1, Lcom/google/android/gms/o;->d:I

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v5, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 240
    const v1, 0xa400

    :try_start_3
    new-array v4, v1, [B

    move v1, v0

    .line 242
    :cond_2
    array-length v6, v4

    sub-int/2addr v6, v1

    invoke-virtual {v5, v4, v1, v6}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v6

    .line 245
    add-int/2addr v1, v6

    .line 246
    const/4 v7, -0x1

    if-eq v6, v7, :cond_3

    add-int/2addr v6, v1

    array-length v7, v4

    if-ne v6, v7, :cond_2

    .line 247
    :cond_3
    const/16 v1, 0x5200

    new-array v1, v1, [S

    iput-object v1, p0, Lcom/google/android/gms/people/c/a/b;->b:[S

    .line 251
    :goto_2
    if-ge v0, v9, :cond_6

    .line 252
    mul-int/lit8 v1, v0, 0x2

    aget-byte v1, v4, v1

    int-to-short v1, v1

    .line 253
    shl-int/lit8 v1, v1, 0x8

    int-to-short v1, v1

    .line 255
    mul-int/lit8 v6, v0, 0x2

    add-int/lit8 v6, v6, 0x1

    aget-byte v6, v4, v6

    int-to-short v6, v6

    and-int/lit16 v6, v6, 0xff

    or-int/2addr v1, v6

    int-to-short v1, v1

    .line 257
    if-ltz v1, :cond_4

    iget-object v6, p0, Lcom/google/android/gms/people/c/a/b;->a:[Ljava/lang/String;

    array-length v6, v6

    if-lt v1, v6, :cond_5

    .line 258
    :cond_4
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid character to pinyin index: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 262
    :catch_1
    move-exception v0

    .line 263
    :try_start_4
    const-string v1, "PeoplePinyin"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Malformed or missing indexes.txt for HanziPinyin."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/people/internal/as;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/people/c/a/b;->c:Z

    .line 266
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/people/c/a/b;->a:[Ljava/lang/String;

    .line 267
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/people/c/a/b;->b:[S
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 270
    invoke-static {v5}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    goto/16 :goto_1

    .line 233
    :catchall_0
    move-exception v0

    invoke-static {v5}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    throw v0

    .line 260
    :cond_5
    :try_start_5
    iget-object v6, p0, Lcom/google/android/gms/people/c/a/b;->b:[S

    aput-short v1, v6, v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 251
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 270
    :cond_6
    invoke-static {v5}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 272
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 273
    const-string v0, "PeoplePinyin"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "HanziToPinyin initialization took "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :cond_7
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/people/c/a/b;->c:Z

    goto/16 :goto_1

    .line 270
    :catchall_1
    move-exception v0

    invoke-static {v5}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    throw v0
.end method

.method static a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V
    .locals 2

    .prologue
    .line 399
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 400
    new-instance v1, Lcom/google/android/gms/people/c/a/c;

    invoke-direct {v1, p2, v0, v0}, Lcom/google/android/gms/people/c/a/c;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 401
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 402
    return-void
.end method
