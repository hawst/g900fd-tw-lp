.class public final Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;
.super Lcom/google/android/gms/games/ui/card/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/card/l;
.implements Lcom/google/android/gms/games/ui/card/m;


# instance fields
.field private h:Landroid/widget/FrameLayout;

.field private i:Landroid/widget/Button;

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:Lcom/google/android/gms/games/ui/card/r;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/a;-><init>(Landroid/content/Context;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/ui/card/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/card/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/card/r;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->k:Lcom/google/android/gms/games/ui/card/r;

    .line 99
    return-void
.end method

.method public final d(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 62
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 63
    if-eqz p1, :cond_0

    .line 64
    sget v2, Lcom/google/android/gms/g;->am:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 68
    iget-object v3, p0, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v2, v2, v4, v2}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 69
    sget v3, Lcom/google/android/gms/g;->al:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v3, v2

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 71
    sget v3, Lcom/google/android/gms/g;->al:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 78
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 79
    return-void

    .line 74
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v4, v4, v4, v4}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 75
    sget v2, Lcom/google/android/gms/g;->al:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0}, Lcom/google/android/gms/games/ui/card/a;->e()V

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->i:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->j:Landroid/graphics/drawable/Drawable;

    const/16 v1, 0x10

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->i:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 53
    :goto_0
    return-void

    .line 52
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->i:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final h(I)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->i:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 88
    return-void
.end method

.method public final i(I)V
    .locals 2

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->i:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 116
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->i:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->k:Lcom/google/android/gms/games/ui/card/r;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/card/r;->y()V

    .line 134
    :goto_0
    return-void

    .line 132
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/card/a;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected final onFinishInflate()V
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Lcom/google/android/gms/games/ui/card/a;->onFinishInflate()V

    .line 41
    sget v0, Lcom/google/android/gms/j;->kb:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->h:Landroid/widget/FrameLayout;

    .line 43
    sget v0, Lcom/google/android/gms/j;->pD:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->i:Landroid/widget/Button;

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->i:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->i:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMediumCardView;->j:Landroid/graphics/drawable/Drawable;

    .line 46
    return-void
.end method
