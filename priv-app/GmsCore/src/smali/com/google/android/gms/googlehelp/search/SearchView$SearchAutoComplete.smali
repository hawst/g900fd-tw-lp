.class public Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;
.super Landroid/widget/AutoCompleteTextView;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Lcom/google/android/gms/googlehelp/search/SearchView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 869
    invoke-direct {p0, p1}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 870
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getThreshold()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->a:I

    .line 871
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 874
    invoke-direct {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 875
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getThreshold()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->a:I

    .line 876
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 879
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 880
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getThreshold()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->a:I

    .line 881
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;)Z
    .locals 1

    .prologue
    .line 863
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a(Lcom/google/android/gms/googlehelp/search/SearchView;)V
    .locals 0

    .prologue
    .line 884
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->b:Lcom/google/android/gms/googlehelp/search/SearchView;

    .line 885
    return-void
.end method

.method public enoughToFilter()Z
    .locals 1

    .prologue
    .line 950
    iget v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->a:I

    if-lez v0, :cond_0

    invoke-super {p0}, Landroid/widget/AutoCompleteTextView;->enoughToFilter()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 940
    invoke-super {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 941
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->b:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->c()V

    .line 942
    return-void
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 955
    const/4 v1, 0x4

    if-ne p1, v1, :cond_3

    .line 958
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 959
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    .line 960
    if-eqz v1, :cond_0

    .line 961
    invoke-virtual {v1, p2, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    .line 976
    :cond_0
    :goto_0
    return v0

    .line 964
    :cond_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_3

    .line 965
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    .line 966
    if-eqz v1, :cond_2

    .line 967
    invoke-virtual {v1, p2}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    .line 969
    :cond_2
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_3

    .line 970
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->b:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/search/SearchView;->clearFocus()V

    .line 971
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->b:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-static {v1}, Lcom/google/android/gms/googlehelp/search/SearchView;->k(Lcom/google/android/gms/googlehelp/search/SearchView;)V

    goto :goto_0

    .line 976
    :cond_3
    invoke-super {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2

    .prologue
    .line 924
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->onWindowFocusChanged(Z)V

    .line 926
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->b:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 927
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 929
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 932
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 933
    invoke-static {}, Lcom/google/android/gms/googlehelp/search/SearchView;->d()Lcom/google/android/gms/googlehelp/search/o;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/googlehelp/search/o;->a(Landroid/widget/AutoCompleteTextView;)V

    .line 936
    :cond_0
    return-void
.end method

.method public performCompletion()V
    .locals 0

    .prologue
    .line 916
    return-void
.end method

.method protected replaceText(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 906
    return-void
.end method

.method public setThreshold(I)V
    .locals 0

    .prologue
    .line 889
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 890
    iput p1, p0, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->a:I

    .line 891
    return-void
.end method
