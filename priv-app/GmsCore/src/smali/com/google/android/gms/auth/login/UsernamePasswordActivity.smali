.class public Lcom/google/android/gms/auth/login/UsernamePasswordActivity;
.super Lcom/google/android/gms/auth/login/g;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;
.implements Lcom/android/setupwizard/navigationbar/a;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Z

.field private D:Z

.field protected q:Landroid/widget/EditText;

.field protected r:Landroid/widget/EditText;

.field protected s:Ljava/lang/String;

.field protected t:Ljava/lang/String;

.field private u:Landroid/view/View;

.field private v:Landroid/view/View;

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/g;-><init>()V

    .line 122
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->s:Ljava/lang/String;

    .line 123
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->t:Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/google/android/gms/auth/login/bf;
    .locals 4

    .prologue
    .line 105
    new-instance v0, Lcom/google/android/gms/auth/login/bf;

    const-string v1, "account_name"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "password"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/auth/login/bf;-><init>(Ljava/lang/String;Ljava/lang/String;B)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/login/UsernamePasswordActivity;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->l()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/login/UsernamePasswordActivity;Z)Z
    .locals 0

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->w:Z

    return p1
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 401
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v1, v2

    .line 402
    :goto_0
    if-eqz v1, :cond_1

    .line 403
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-static {v1}, Lcom/google/android/gms/auth/login/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    move-object v0, v1

    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x40

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    move-object v1, v2

    .line 407
    :cond_1
    return-object v1

    .line 401
    :cond_2
    const-string v0, "@"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v1, p1

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v0, Lcom/google/android/gms/p;->bx:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/auth/a/b;->c()Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "device_country"

    invoke-static {p0, v4, v2}, Lcom/google/android/gms/common/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "de"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    sget v0, Lcom/google/android/gms/p;->by:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_4
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Couldn\'t find gmail_host_name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 403
    :cond_6
    const-string v0, "@"

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v3, v0

    const/4 v4, 0x2

    if-ne v3, v4, :cond_7

    aget-object v3, v0, v6

    invoke-static {v3}, Lcom/google/android/gms/auth/login/g;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    aget-object v0, v0, v6

    goto :goto_1

    :cond_7
    const-string v0, ""

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/gms/auth/login/UsernamePasswordActivity;Z)Z
    .locals 0

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->x:Z

    return p1
.end method

.method private l()V
    .locals 1

    .prologue
    .line 453
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->setResult(I)V

    .line 454
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->finish()V

    .line 455
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 445
    return-void
.end method

.method public final a(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2

    .prologue
    .line 433
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->b()Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->u:Landroid/view/View;

    .line 434
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->u:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->a(Landroid/view/View;Z)V

    .line 436
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->a()Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->v:Landroid/view/View;

    .line 437
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->v:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->a(Landroid/view/View;)V

    .line 439
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->o:Z

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->o:Z

    invoke-virtual {p1, v0, v1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->a(ZZ)V

    .line 440
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 450
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 324
    invoke-super {p0}, Lcom/google/android/gms/auth/login/g;->c()V

    .line 326
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->C:Z

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Ljava/lang/String;

    .line 329
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Ljava/lang/String;

    .line 330
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 331
    const-string v1, "account_name"

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 332
    const-string v1, "password"

    iget-object v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 333
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->setResult(ILandroid/content/Intent;)V

    .line 334
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->finish()V

    .line 335
    return-void
.end method

.method protected final d()Z
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x1

    return v0
.end method

.method public final g()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 373
    invoke-super {p0}, Lcom/google/android/gms/auth/login/g;->g()V

    .line 374
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 375
    iget-object v3, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    .line 382
    iget-boolean v4, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->w:Z

    if-nez v4, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 384
    :goto_0
    iget-boolean v4, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->C:Z

    if-nez v4, :cond_0

    move v0, v1

    .line 387
    :cond_0
    iget-boolean v4, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->x:Z

    if-nez v4, :cond_3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    move v3, v1

    .line 389
    :goto_1
    if-eqz v0, :cond_4

    if-eqz v3, :cond_4

    .line 391
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->u:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 392
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 393
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 395
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 382
    goto :goto_0

    :cond_3
    move v3, v2

    .line 387
    goto :goto_1

    :cond_4
    move v1, v2

    .line 389
    goto :goto_2
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 143
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onCreate(Landroid/os/Bundle;)V

    .line 144
    if-nez p1, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    .line 147
    :cond_0
    const-string v0, "account_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Ljava/lang/String;

    .line 148
    const-string v0, "password"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Ljava/lang/String;

    .line 149
    const-string v0, "is_adding_account"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->C:Z

    .line 150
    const-string v0, "is_confirming_credentials"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->D:Z

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->s:Ljava/lang/String;

    .line 156
    :cond_1
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lcom/google/android/setupwizard/util/b;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->setContentView(Landroid/view/View;)V

    sget v1, Lcom/google/android/gms/p;->cm:I

    sget v2, Lcom/google/android/gms/l;->r:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/setupwizard/util/b;->b(II)Landroid/view/View;

    sget v0, Lcom/google/android/gms/j;->ce:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/gms/auth/login/bb;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/login/bb;-><init>(Lcom/google/android/gms/auth/login/UsernamePasswordActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    sget v0, Lcom/google/android/gms/j;->tK:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    sget v0, Lcom/google/android/gms/j;->nt:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->r:Landroid/widget/EditText;

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->C:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->D:Z

    if-nez v0, :cond_2

    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->ci:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    sget v0, Lcom/google/android/gms/j;->tL:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->r:Landroid/widget/EditText;

    new-array v1, v4, [Landroid/text/InputFilter;

    new-instance v2, Lcom/google/android/gms/auth/login/bd;

    invoke-direct {v2, p0}, Lcom/google/android/gms/auth/login/bd;-><init>(Lcom/google/android/gms/auth/login/UsernamePasswordActivity;)V

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->r:Landroid/widget/EditText;

    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->a(Landroid/view/View;Z)V

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->r:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 158
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->g()V

    .line 159
    return-void

    .line 156
    :cond_5
    sget v0, Lcom/google/android/gms/l;->q:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->setContentView(I)V

    sget v0, Lcom/google/android/gms/j;->mw:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->u:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->u:Landroid/view/View;

    invoke-virtual {p0, v0, v4}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->a(Landroid/view/View;Z)V

    sget v0, Lcom/google/android/gms/j;->bO:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->v:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->v:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->a(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    new-array v1, v4, [Landroid/text/InputFilter;

    new-instance v2, Lcom/google/android/gms/auth/login/bc;

    invoke-direct {v2, p0}, Lcom/google/android/gms/auth/login/bc;-><init>(Lcom/google/android/gms/auth/login/UsernamePasswordActivity;)V

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iput-boolean v4, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->y:Z

    goto/16 :goto_1

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 412
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 414
    sget v0, Lcom/google/android/gms/p;->cn:I

    invoke-interface {p1, v1, v2, v1, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 416
    sget v1, Lcom/google/android/gms/h;->i:I

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 417
    invoke-static {v0}, Lcom/google/android/gms/auth/a/b;->a(Landroid/view/MenuItem;)V

    .line 418
    return v2
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    if-ne p1, v0, :cond_4

    if-nez p2, :cond_4

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->C:Z

    if-eqz v0, :cond_4

    .line 340
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->w:Z

    if-eqz v0, :cond_1

    .line 341
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    sget v1, Lcom/google/android/gms/p;->bA:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 366
    :cond_0
    :goto_0
    return-void

    .line 343
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 344
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 345
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    sget v1, Lcom/google/android/gms/p;->bs:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 350
    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 351
    if-eqz v0, :cond_3

    .line 352
    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 353
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 355
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    sget v1, Lcom/google/android/gms/p;->bC:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 359
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->r:Landroid/widget/EditText;

    if-ne p1, v0, :cond_0

    if-nez p2, :cond_0

    .line 360
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->x:Z

    if-eqz v0, :cond_5

    .line 361
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->r:Landroid/widget/EditText;

    sget v1, Lcom/google/android/gms/p;->bB:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 362
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->r:Landroid/widget/EditText;

    sget v1, Lcom/google/android/gms/p;->bs:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 423
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 428
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 425
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->l()V

    goto :goto_0

    .line 423
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 163
    invoke-super {p0}, Lcom/google/android/gms/auth/login/g;->onPause()V

    .line 164
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->z:J

    .line 165
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 169
    invoke-super {p0}, Lcom/google/android/gms/auth/login/g;->onResume()V

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    if-nez v0, :cond_1

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 181
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextKeepState(Ljava/lang/CharSequence;)V

    .line 183
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->g()V

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 191
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->z:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->r:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextKeepState(Ljava/lang/CharSequence;)V

    .line 194
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->y:Z

    if-eqz v0, :cond_4

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 201
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->t:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->x:Z

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 197
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->q:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 135
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 136
    const-string v0, "account_name"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->A:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v0, "password"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v0, "is_adding_account"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->C:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 139
    const-string v0, "is_confirming_credentials"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->D:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 140
    return-void
.end method
