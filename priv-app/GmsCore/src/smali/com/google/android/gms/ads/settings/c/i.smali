.class public final Lcom/google/android/gms/ads/settings/c/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/w;
.implements Lcom/android/volley/x;


# static fields
.field private static final a:Ljava/lang/Object;

.field private static final b:Landroid/content/SharedPreferences;


# instance fields
.field private final c:Landroid/content/BroadcastReceiver;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/android/volley/s;

.field private final f:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/ads/settings/c/i;->a:Ljava/lang/Object;

    .line 36
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    const-string v1, "adid_clear_state"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/settings/c/i;->b:Landroid/content/SharedPreferences;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/c/i;->f:Ljava/util/Set;

    .line 59
    iput-object p1, p0, Lcom/google/android/gms/ads/settings/c/i;->d:Landroid/content/Context;

    .line 60
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/c/i;->e:Lcom/android/volley/s;

    .line 61
    new-instance v0, Lcom/google/android/gms/ads/settings/c/j;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/settings/c/j;-><init>(Lcom/google/android/gms/ads/settings/c/i;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/c/i;->c:Landroid/content/BroadcastReceiver;

    .line 71
    return-void
.end method

.method private static a(Z)V
    .locals 2

    .prologue
    .line 119
    sget-object v0, Lcom/google/android/gms/ads/settings/c/i;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "stop_scheduled"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 121
    return-void
.end method

.method public static c()Z
    .locals 3

    .prologue
    .line 103
    sget-object v0, Lcom/google/android/gms/ads/settings/c/i;->b:Landroid/content/SharedPreferences;

    const-string v1, "adid_clear_in_progress"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static d()Z
    .locals 2

    .prologue
    .line 107
    sget-object v0, Lcom/google/android/gms/ads/settings/c/i;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    const-string v1, "stop_scheduled"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static e()V
    .locals 2

    .prologue
    .line 113
    sget-object v1, Lcom/google/android/gms/ads/settings/c/i;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 114
    :try_start_0
    sget-object v0, Lcom/google/android/gms/ads/settings/c/i;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 115
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic g()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/google/android/gms/ads/settings/c/i;->a:Ljava/lang/Object;

    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 91
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/gms/ads/settings/c/i;->a(Z)V

    .line 93
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/c/i;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/ads/settings/c/i;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/c/i;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 178
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 74
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 75
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 76
    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 77
    iget-object v1, p0, Lcom/google/android/gms/ads/settings/c/i;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/ads/settings/c/i;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 78
    return-void
.end method

.method public final a(Lcom/android/volley/ac;)V
    .locals 2

    .prologue
    .line 151
    const-string v0, "AdvertisingIdState"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    const-string v0, "AdvertisingIdState"

    const-string v1, "Failed to clear Ad ID over network."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/ads/settings/c/i;->i()V

    .line 155
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 24
    check-cast p1, Ljava/lang/Boolean;

    const-string v0, "AdvertisingIdState"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AdvertisingIdState"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Successful response from server to clear Ad ID. (response="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v1, Lcom/google/android/gms/ads/settings/c/i;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/ads/settings/c/i;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "adid_clear_in_progress"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    sget-object v0, Lcom/google/android/gms/ads/settings/c/i;->b:Landroid/content/SharedPreferences;

    const-string v2, "stop_scheduled"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/ads/settings/c/i;->h()V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/google/android/gms/ads/settings/c/i;->i()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 81
    sget-object v1, Lcom/google/android/gms/ads/settings/c/i;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 82
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/ads/settings/c/i;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/ads/settings/c/i;->a(Z)V

    .line 87
    :goto_0
    monitor-exit v1

    return-void

    .line 85
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/ads/settings/c/i;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 140
    sget-object v1, Lcom/google/android/gms/ads/settings/c/i;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 141
    :try_start_0
    sget-object v0, Lcom/google/android/gms/ads/settings/c/i;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "adid_clear_in_progress"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 143
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    new-instance v0, Lcom/google/android/gms/ads/settings/c/k;

    iget-object v1, p0, Lcom/google/android/gms/ads/settings/c/i;->d:Landroid/content/Context;

    const-string v2, "mobile_clear"

    invoke-direct {v0, v1, v2, p0, p0}, Lcom/google/android/gms/ads/settings/c/k;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 146
    iget-object v1, p0, Lcom/google/android/gms/ads/settings/c/i;->e:Lcom/android/volley/s;

    invoke-virtual {v1, v0}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    .line 147
    return-void

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
