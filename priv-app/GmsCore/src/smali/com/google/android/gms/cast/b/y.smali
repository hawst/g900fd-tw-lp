.class public final Lcom/google/android/gms/cast/b/y;
.super Ljava/io/OutputStream;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/cast/b/w;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/b/w;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/google/android/gms/cast/b/y;->a:Lcom/google/android/gms/cast/b/w;

    .line 16
    return-void
.end method


# virtual methods
.method public final write(I)V
    .locals 3

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/gms/cast/b/y;->a:Lcom/google/android/gms/cast/b/w;

    and-int/lit16 v1, p1, 0xff

    int-to-byte v1, v1

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/w;->d()I

    move-result v2

    if-gtz v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 21
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Buffer overflow"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_0
    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/b/w;->a(B)V

    const/4 v0, 0x1

    goto :goto_0

    .line 23
    :cond_1
    return-void
.end method

.method public final write([BII)V
    .locals 5

    .prologue
    .line 27
    iget-object v1, p0, Lcom/google/android/gms/cast/b/y;->a:Lcom/google/android/gms/cast/b/w;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/b/w;->d()I

    move-result v0

    if-ge v0, p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_4

    .line 28
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Buffer overflow"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 27
    :cond_0
    iget-boolean v0, v1, Lcom/google/android/gms/cast/b/w;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, v1, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v0, v0

    :goto_1
    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v2, v1, Lcom/google/android/gms/cast/b/w;->a:[B

    iget v3, v1, Lcom/google/android/gms/cast/b/w;->c:I

    invoke-static {p1, p2, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/b/w;->b(I)V

    add-int v2, p2, v0

    sub-int v0, p3, v0

    if-lez v0, :cond_1

    iget-object v3, v1, Lcom/google/android/gms/cast/b/w;->a:[B

    iget v4, v1, Lcom/google/android/gms/cast/b/w;->c:I

    invoke-static {p1, v2, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/b/w;->b(I)V

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget v0, v1, Lcom/google/android/gms/cast/b/w;->c:I

    iget v2, v1, Lcom/google/android/gms/cast/b/w;->b:I

    if-ge v0, v2, :cond_3

    iget v0, v1, Lcom/google/android/gms/cast/b/w;->b:I

    iget v2, v1, Lcom/google/android/gms/cast/b/w;->c:I

    sub-int/2addr v0, v2

    goto :goto_1

    :cond_3
    iget-object v0, v1, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v0, v0

    iget v2, v1, Lcom/google/android/gms/cast/b/w;->c:I

    sub-int/2addr v0, v2

    goto :goto_1

    .line 30
    :cond_4
    return-void
.end method
