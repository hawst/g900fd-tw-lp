.class public final Lcom/google/android/gms/googlehelp/c/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

.field public final b:Landroid/support/v4/app/q;

.field public c:Landroid/view/View;

.field public d:Landroid/view/View;

.field public e:Landroid/view/View;

.field public f:Landroid/view/View;

.field public g:Landroid/widget/TextView;

.field public h:Landroid/view/View;

.field public i:Landroid/widget/TextView;

.field public j:Landroid/view/View;

.field public k:Landroid/content/BroadcastReceiver;

.field public l:Landroid/content/BroadcastReceiver;

.field public m:Landroid/app/NotificationManager;

.field private final n:Lcom/google/android/gms/googlehelp/common/HelpConfig;

.field private o:Landroid/support/v4/a/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
    .locals 2

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 86
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Neither the activity nor the helpConfig can be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_1
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    .line 91
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->b:Landroid/support/v4/app/q;

    .line 92
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/c/a/d;->n:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 93
    return-void
.end method

.method private d()Landroid/support/v4/a/m;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->o:Landroid/support/v4/a/m;

    if-nez v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->b:Landroid/support/v4/app/q;

    invoke-static {v0}, Landroid/support/v4/a/m;->a(Landroid/content/Context;)Landroid/support/v4/a/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->o:Landroid/support/v4/a/m;

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->o:Landroid/support/v4/a/m;

    return-object v0
.end method

.method private k(Z)V
    .locals 4

    .prologue
    .line 272
    if-eqz p1, :cond_0

    .line 273
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.gms.googlehelp.HelpFragment.VIDEO_CALL_READY"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 274
    const-string v1, "https"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 275
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.google.android.gms.googlehelp.HelpFragment.VC_STATUS_UPDATE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 277
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/a/d;->d()Landroid/support/v4/a/m;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/c/a/d;->l:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/support/v4/a/m;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 279
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/a/d;->d()Landroid/support/v4/a/m;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/c/a/d;->l:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/a/m;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 289
    :goto_0
    return-void

    .line 282
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.gms.googlehelp.HelpFragment.CHAT_READY"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 283
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.google.android.gms.googlehelp.HelpFragment.CHAT_STATUS_UPDATE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 284
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/a/d;->d()Landroid/support/v4/a/m;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/c/a/d;->k:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/support/v4/a/m;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 286
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/a/d;->d()Landroid/support/v4/a/m;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/c/a/d;->k:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/a/m;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/googlehelp/common/a;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 213
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/a/d;->a()Z

    move-result v0

    .line 214
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->c:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 219
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->b(I)V

    .line 221
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->b(I)V

    .line 223
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->n:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {v0, p1}, Lcom/google/android/gms/googlehelp/common/e;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)V

    .line 224
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->n:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {v0, p1}, Lcom/google/android/gms/googlehelp/common/e;->b(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)V

    .line 226
    :cond_0
    return-void

    .line 214
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/a/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    :goto_0
    return-void

    .line 160
    :cond_0
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->b:Landroid/support/v4/app/q;

    const-class v2, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 162
    const-string v0, "EXTRA_HELP_CONFIG"

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/c/a/d;->n:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 163
    const-string v2, "EXTRA_ESCALATION_TYPE"

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 171
    invoke-direct {p0, p1}, Lcom/google/android/gms/googlehelp/c/a/d;->k(Z)V

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->b:Landroid/support/v4/app/q;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 163
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method final a(ZLcom/google/android/gms/googlehelp/common/a;)V
    .locals 3

    .prologue
    const/16 v2, 0x3e9

    .line 411
    if-eqz p1, :cond_0

    .line 414
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->b:Landroid/support/v4/app/q;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a/d;->n:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {v0, v1, p2}, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 421
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a/d;->b:Landroid/support/v4/app/q;

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/q;->startActivityForResult(Landroid/content/Intent;I)V

    .line 427
    :goto_0
    return-void

    .line 417
    :catch_0
    move-exception v0

    invoke-virtual {p0, p1}, Lcom/google/android/gms/googlehelp/c/a/d;->g(Z)V

    goto :goto_0

    .line 423
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->b:Landroid/support/v4/app/q;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a/d;->n:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {v0, v1, p2}, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)Landroid/content/Intent;

    move-result-object v0

    .line 425
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a/d;->b:Landroid/support/v4/app/q;

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/q;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 134
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/a/d;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 135
    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/c/a/d;->k(Z)V

    .line 136
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a/d;->b:Landroid/support/v4/app/q;

    invoke-static {v1}, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->a(Landroid/content/Context;)V

    .line 143
    :goto_0
    return v0

    .line 138
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/a/d;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 139
    invoke-direct {p0, v1}, Lcom/google/android/gms/googlehelp/c/a/d;->k(Z)V

    .line 140
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a/d;->b:Landroid/support/v4/app/q;

    invoke-static {v1}, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->a(Landroid/content/Context;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 143
    goto :goto_0
.end method

.method final b(Lcom/google/android/gms/googlehelp/common/a;)V
    .locals 2

    .prologue
    .line 545
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/a/d;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 546
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->n:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {v0, p1}, Lcom/google/android/gms/googlehelp/common/e;->b(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)V

    .line 547
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->b(I)V

    .line 550
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/a/d;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 551
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->n:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {v0, p1}, Lcom/google/android/gms/googlehelp/common/e;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)V

    .line 552
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->b(I)V

    .line 555
    :cond_1
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 292
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/a/d;->d()Landroid/support/v4/a/m;

    move-result-object v1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->l:Landroid/content/BroadcastReceiver;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v4/a/m;->a(Landroid/content/BroadcastReceiver;)V

    .line 295
    return-void

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->k:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method final b(ZLcom/google/android/gms/googlehelp/common/a;)V
    .locals 2

    .prologue
    .line 474
    if-eqz p1, :cond_0

    .line 475
    invoke-virtual {p2}, Lcom/google/android/gms/googlehelp/common/a;->b()Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    const-string v1, "ongoing_video_request_pool_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 478
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->b(I)V

    .line 487
    :goto_0
    return-void

    .line 481
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/googlehelp/common/a;->b()Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    const-string v1, "ongoing_chat_request_pool_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 484
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->b(I)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x0

    .line 242
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->k()Lcom/google/android/gms/googlehelp/common/a;

    move-result-object v1

    .line 243
    if-nez v1, :cond_1

    .line 250
    :cond_0
    :goto_0
    return v0

    .line 247
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/c/a/d;->n:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {v2}, Lcom/google/android/gms/googlehelp/common/u;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 250
    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method final c(Z)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 328
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 331
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->g:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->oL:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->h:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->c:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a/d;->b:Landroid/support/v4/app/q;

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/f;->L:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->c:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a/d;->b:Landroid/support/v4/app/q;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->makeInAnimation(Landroid/content/Context;Z)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->c:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->i:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/gms/googlehelp/c/a/e;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/googlehelp/c/a/e;-><init>(Lcom/google/android/gms/googlehelp/c/a/d;Z)V

    invoke-static {v0, v1}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/widget/TextView;Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->j:Landroid/view/View;

    new-instance v1, Lcom/google/android/gms/googlehelp/c/a/f;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/googlehelp/c/a/f;-><init>(Lcom/google/android/gms/googlehelp/c/a/d;Z)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 333
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x0

    .line 255
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->k()Lcom/google/android/gms/googlehelp/common/a;

    move-result-object v1

    .line 256
    if-nez v1, :cond_1

    .line 263
    :cond_0
    :goto_0
    return v0

    .line 260
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/c/a/d;->n:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {v2}, Lcom/google/android/gms/googlehelp/common/u;->b(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 263
    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    .line 336
    invoke-virtual {p0, p1}, Lcom/google/android/gms/googlehelp/c/a/d;->i(Z)V

    .line 338
    if-eqz p1, :cond_0

    .line 339
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->b:Landroid/support/v4/app/q;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->b(Landroid/content/Context;)V

    .line 340
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->b(I)V

    .line 347
    :goto_0
    return-void

    .line 343
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->b:Landroid/support/v4/app/q;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->b(Landroid/content/Context;)V

    .line 344
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->b(I)V

    goto :goto_0
.end method

.method public final e(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 350
    invoke-virtual {p0, p1}, Lcom/google/android/gms/googlehelp/c/a/d;->c(Z)V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->e:Landroid/view/View;

    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    new-instance v1, Lcom/google/android/gms/googlehelp/c/a/h;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/googlehelp/c/a/h;-><init>(Lcom/google/android/gms/googlehelp/c/a/d;Z)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz p1, :cond_2

    sget v1, Lcom/google/android/gms/p;->oY:I

    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/c/a/d;->b:Landroid/support/v4/app/q;

    invoke-virtual {v2, v1}, Landroid/support/v4/app/q;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a/d;->g:Landroid/widget/TextView;

    if-eqz p1, :cond_3

    sget v0, Lcom/google/android/gms/p;->pp:I

    :goto_2
    invoke-static {v1, v0}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/widget/TextView;I)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->f:Landroid/view/View;

    new-instance v1, Lcom/google/android/gms/googlehelp/c/a/i;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/googlehelp/c/a/i;-><init>(Lcom/google/android/gms/googlehelp/c/a/d;Z)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 352
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->k()Lcom/google/android/gms/googlehelp/common/a;

    move-result-object v0

    .line 353
    if-eqz v0, :cond_0

    const-string v1, "hangout_was_opened"

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 357
    invoke-virtual {p0, p1}, Lcom/google/android/gms/googlehelp/c/a/d;->f(Z)V

    .line 359
    :cond_0
    return-void

    .line 350
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->d:Landroid/view/View;

    goto :goto_0

    :cond_2
    sget v1, Lcom/google/android/gms/p;->oR:I

    goto :goto_1

    :cond_3
    sget v0, Lcom/google/android/gms/p;->oy:I

    goto :goto_2
.end method

.method final f(Z)V
    .locals 2

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->k()Lcom/google/android/gms/googlehelp/common/a;

    move-result-object v0

    .line 393
    if-eqz v0, :cond_0

    .line 394
    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/googlehelp/c/a/d;->a(ZLcom/google/android/gms/googlehelp/common/a;)V

    .line 407
    :goto_0
    return-void

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    new-instance v1, Lcom/google/android/gms/googlehelp/c/a/j;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/googlehelp/c/a/j;-><init>(Lcom/google/android/gms/googlehelp/c/a/d;Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a(Lcom/google/android/gms/googlehelp/common/b;)V

    goto :goto_0
.end method

.method final g(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 430
    invoke-virtual {p0, p1}, Lcom/google/android/gms/googlehelp/c/a/d;->c(Z)V

    .line 432
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a/d;->g:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/gms/p;->pq:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 436
    if-eqz p1, :cond_1

    .line 437
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 441
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 443
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 444
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 446
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->j:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 447
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->c:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a/d;->b:Landroid/support/v4/app/q;

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/f;->K:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 450
    invoke-virtual {p0, p1}, Lcom/google/android/gms/googlehelp/c/a/d;->h(Z)V

    .line 451
    return-void

    .line 432
    :cond_0
    sget v0, Lcom/google/android/gms/p;->oz:I

    goto :goto_0

    .line 439
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method final h(Z)V
    .locals 2

    .prologue
    .line 455
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->k()Lcom/google/android/gms/googlehelp/common/a;

    move-result-object v0

    .line 456
    if-eqz v0, :cond_0

    .line 457
    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/googlehelp/c/a/d;->b(ZLcom/google/android/gms/googlehelp/common/a;)V

    .line 470
    :goto_0
    return-void

    .line 459
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    new-instance v1, Lcom/google/android/gms/googlehelp/c/a/k;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/googlehelp/c/a/k;-><init>(Lcom/google/android/gms/googlehelp/c/a/d;Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a(Lcom/google/android/gms/googlehelp/common/b;)V

    goto :goto_0
.end method

.method public final i(Z)V
    .locals 2

    .prologue
    .line 511
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 512
    invoke-virtual {p0, p1}, Lcom/google/android/gms/googlehelp/c/a/d;->b(Z)V

    .line 515
    if-eqz p1, :cond_0

    .line 516
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->m:Landroid/app/NotificationManager;

    const/16 v1, 0xed

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 520
    :goto_0
    return-void

    .line 518
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/d;->m:Landroid/app/NotificationManager;

    const/16 v1, 0x7de

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method

.method public final j(Z)Landroid/content/BroadcastReceiver;
    .locals 3

    .prologue
    .line 567
    if-eqz p1, :cond_0

    .line 568
    const-string v1, "com.google.android.gms.googlehelp.HelpFragment.VIDEO_CALL_READY"

    .line 569
    const-string v0, "com.google.android.gms.googlehelp.HelpFragment.VC_STATUS_UPDATE"

    .line 575
    :goto_0
    new-instance v2, Lcom/google/android/gms/googlehelp/c/a/m;

    invoke-direct {v2, p0, v1, p1, v0}, Lcom/google/android/gms/googlehelp/c/a/m;-><init>(Lcom/google/android/gms/googlehelp/c/a/d;Ljava/lang/String;ZLjava/lang/String;)V

    return-object v2

    .line 571
    :cond_0
    const-string v1, "com.google.android.gms.googlehelp.HelpFragment.CHAT_READY"

    .line 572
    const-string v0, "com.google.android.gms.googlehelp.HelpFragment.CHAT_STATUS_UPDATE"

    goto :goto_0
.end method
