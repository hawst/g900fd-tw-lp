.class Lcom/google/android/gms/auth/be/i;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final synthetic j:Z


# instance fields
.field public final a:Z

.field public b:Ljava/lang/String;

.field public c:Landroid/accounts/Account;

.field public d:Z

.field protected final e:Lcom/google/android/gms/auth/a/c;

.field public f:Z

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field protected final i:Lcom/google/android/gms/auth/be/h;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    const-class v0, Lcom/google/android/gms/auth/be/i;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/gms/auth/be/i;->j:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 215
    new-instance v0, Lcom/google/android/gms/auth/a/c;

    invoke-direct {v0, p1}, Lcom/google/android/gms/auth/a/c;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/auth/be/i;-><init>(Lcom/google/android/gms/auth/a/c;Ljava/lang/String;)V

    .line 216
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/auth/a/c;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/auth/be/i;-><init>(Lcom/google/android/gms/auth/a/c;Ljava/lang/String;B)V

    .line 220
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/auth/a/c;Ljava/lang/String;B)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233
    const-string v0, "GLSUser"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/be/i;->a:Z

    .line 234
    iput-object p1, p0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    .line 236
    invoke-static {}, Lcom/google/android/gms/auth/be/h;->a()Lcom/google/android/gms/auth/be/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    .line 237
    iput-object p2, p0, Lcom/google/android/gms/auth/be/i;->b:Ljava/lang/String;

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    iget-object v0, v0, Lcom/google/android/gms/auth/a/c;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    iget-object v2, v2, Lcom/google/android/gms/auth/a/c;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/util/a;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/be/i;->d:Z

    .line 245
    iget-object v0, p1, Lcom/google/android/gms/auth/a/c;->a:Landroid/content/Context;

    .line 246
    new-instance v1, Lcom/google/android/gms/common/c/a;

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/c/a;-><init>(Landroid/content/Context;)V

    .line 247
    iget-boolean v0, p0, Lcom/google/android/gms/auth/be/i;->a:Z

    if-eqz v0, :cond_0

    .line 248
    const-string v0, "[GLSUser]  Using [AuthUri: %s, SetupUri: %s, CreateProfileUri: %s]"

    .line 250
    const-string v1, "GLSUser"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/gms/auth/b/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lcom/google/android/gms/auth/b/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    sget-object v3, Lcom/google/android/gms/auth/b/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/util/a;->b(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/auth/be/i;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    iget-object v1, v1, Lcom/google/android/gms/auth/a/c;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/auth/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/a/d;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/auth/a/d;->d:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/auth/be/i;->h:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/gms/auth/a/d;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    .line 256
    :cond_2
    return-void
.end method

.method private a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/util/Map;Lcom/google/android/gms/auth/firstparty/shared/k;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 5

    .prologue
    .line 1589
    invoke-static {}, Lcom/google/android/gms/auth/be/h;->a()Lcom/google/android/gms/auth/be/h;

    .line 1590
    invoke-virtual {p1, p3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .line 1591
    if-eqz p2, :cond_1

    .line 1592
    sget-object v0, Lcom/google/android/gms/auth/be/l;->b:[I

    invoke-virtual {p3}, Lcom/google/android/gms/auth/firstparty/shared/k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1636
    :cond_0
    :goto_0
    sget-object v0, Lcom/google/android/gms/auth/firstparty/shared/k;->M:Lcom/google/android/gms/auth/firstparty/shared/k;

    if-ne p3, v0, :cond_5

    .line 1644
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    iget-object v0, v0, Lcom/google/android/gms/auth/a/c;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1645
    sget v1, Lcom/google/android/gms/p;->bj:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1649
    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->d(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/auth/be/w;->C:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->c(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .line 1652
    :cond_1
    return-object p1

    .line 1594
    :pswitch_0
    sget-object v0, Lcom/google/android/gms/auth/be/w;->A:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/auth/be/i;->d(Ljava/lang/String;)Lcom/google/android/gms/auth/login/ao;

    move-result-object v0

    .line 1597
    if-nez v0, :cond_2

    .line 1598
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1609
    :goto_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1610
    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Ljava/util/List;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    goto :goto_0

    .line 1604
    :cond_2
    iget v1, v0, Lcom/google/android/gms/auth/login/ao;->c:I

    invoke-virtual {p1, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(I)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .line 1605
    iget-boolean v1, v0, Lcom/google/android/gms/auth/login/ao;->b:Z

    invoke-virtual {p1, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b(Z)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .line 1606
    invoke-static {v0}, Lcom/google/android/gms/auth/be/i;->a(Lcom/google/android/gms/auth/login/ao;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_2

    .line 1612
    :cond_3
    const-string v0, "GLSUser"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No consent details provided for requests to: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1613
    const-string v0, "oauth2"

    invoke-virtual {p4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1618
    sget-object v0, Lcom/google/android/gms/auth/firstparty/shared/k;->H:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    goto :goto_0

    .line 1623
    :pswitch_1
    sget-object v0, Lcom/google/android/gms/auth/be/w;->r:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v0

    .line 1624
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1625
    if-eqz v0, :cond_0

    .line 1626
    iget-boolean v1, p0, Lcom/google/android/gms/auth/be/i;->a:Z

    if-eqz v1, :cond_4

    .line 1627
    const-string v1, "%s fillErrorResponse: BAD_AUTH response info: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "[GLSUser] "

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1629
    const-string v2, "GLSUser"

    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1631
    :cond_4
    sget-object v1, Lcom/google/android/gms/auth/firstparty/shared/k;->c:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/firstparty/shared/k;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1632
    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Z)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .line 1633
    if-eqz v0, :cond_0

    .line 1634
    sget-object v0, Lcom/google/android/gms/auth/firstparty/shared/k;->c:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    goto/16 :goto_0

    .line 1647
    :cond_5
    sget-object v0, Lcom/google/android/gms/auth/be/w;->f:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_1

    .line 1592
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;
    .locals 4

    .prologue
    .line 2262
    const-string v0, "http"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2263
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/be/i;->a:Z

    if-eqz v0, :cond_0

    .line 2264
    const-string v0, "GLSUser"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "captcha url is ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2266
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/auth/a/b;->a(I)V

    .line 2268
    :try_start_0
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, p2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 2271
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-virtual {v0, v1, p3}, Lcom/google/android/gms/auth/be/h;->a(Ljava/util/Map;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 2273
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2274
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2295
    :catch_0
    move-exception v0

    :try_start_1
    new-instance v0, Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;

    sget-object v1, Lcom/google/android/gms/auth/firstparty/shared/k;->m:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;-><init>(Lcom/google/android/gms/auth/firstparty/shared/k;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2297
    invoke-static {}, Lcom/google/android/gms/auth/a/b;->a()V

    :goto_2
    return-object v0

    .line 2262
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://www.google.com/accounts/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 2276
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/be/h;->b:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 2277
    iget-boolean v1, p0, Lcom/google/android/gms/auth/be/i;->a:Z

    if-eqz v1, :cond_3

    .line 2278
    const-string v1, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bitmap response is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2280
    :cond_3
    const-string v1, "X-Google-Captcha-Error"

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2283
    new-instance v0, Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;

    sget-object v1, Lcom/google/android/gms/auth/firstparty/shared/k;->m:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;-><init>(Lcom/google/android/gms/auth/firstparty/shared/k;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2297
    invoke-static {}, Lcom/google/android/gms/auth/a/b;->a()V

    goto :goto_2

    .line 2285
    :cond_4
    :try_start_3
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/http/util/EntityUtils;->toByteArray(Lorg/apache/http/HttpEntity;)[B

    move-result-object v0

    .line 2289
    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2293
    new-instance v0, Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;

    sget-object v2, Lcom/google/android/gms/auth/firstparty/shared/k;->a:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-direct {v0, v2, p1, v1}, Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;-><init>(Lcom/google/android/gms/auth/firstparty/shared/k;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2297
    invoke-static {}, Lcom/google/android/gms/auth/a/b;->a()V

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/google/android/gms/auth/a/b;->a()V

    throw v0
.end method

.method private static a(Lcom/google/android/gms/auth/login/ao;)Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 1660
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1661
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ao;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/login/ar;

    .line 1662
    invoke-static {v0}, Lcom/google/android/gms/auth/a/g;->a(Lcom/google/android/gms/auth/login/ar;)Lcom/google/android/gms/auth/firstparty/shared/ScopeDetail;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1664
    :cond_0
    return-object v1
.end method

.method private a(Landroid/accounts/Account;)V
    .locals 2

    .prologue
    .line 710
    new-instance v0, Lcom/google/android/gms/backup/b;

    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v1, v1, Lcom/google/android/gms/auth/be/h;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/backup/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/backup/b;->a(Landroid/accounts/Account;)V

    .line 711
    return-void
.end method

.method private a(Landroid/accounts/Account;ZLjava/lang/String;)V
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 740
    invoke-static {}, Landroid/content/ContentResolver;->getSyncAdapterTypes()[Landroid/content/SyncAdapterType;

    move-result-object v1

    .line 741
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 742
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 743
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 744
    iget-object v4, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    iget-object v7, v3, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 745
    iget-object v4, v3, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v8, v3, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v7, v8}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v4, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 747
    iget-object v4, v3, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v7, v3}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v5, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 743
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 752
    :cond_1
    new-instance v0, Lcom/google/android/gms/auth/be/j;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/auth/be/j;-><init>(Lcom/google/android/gms/auth/be/i;Landroid/accounts/Account;Ljava/lang/String;ZLjava/util/Map;Ljava/util/Map;)V

    .line 791
    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    iget-object v1, v1, Lcom/google/android/gms/auth/a/c;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/auth/be/q;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/q;

    move-result-object v1

    .line 793
    invoke-virtual {v1}, Lcom/google/android/gms/auth/be/q;->a()Ljava/lang/String;

    move-result-object v2

    .line 796
    :try_start_0
    new-instance v3, Landroid/os/HandlerThread;

    const-string v4, "rename callback thread"

    invoke-direct {v3, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 797
    invoke-virtual {v3}, Landroid/os/HandlerThread;->start()V

    .line 798
    new-instance v4, Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v4, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 799
    iget-object v3, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v3, v3, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v5, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v6, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v5, v6, v0, v4}, Landroid/accounts/AccountManager;->renameAccount(Landroid/accounts/Account;Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 805
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v3, 0x4

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v0, v3, v4}, Lcom/google/android/gms/auth/be/q;->a(Ljava/lang/String;ILjava/lang/String;)V

    .line 809
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v0, v3, v4}, Lcom/google/android/gms/auth/be/q;->a(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 814
    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/be/q;->a(Ljava/lang/String;)V

    .line 815
    return-void

    .line 814
    :catchall_0
    move-exception v0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/be/q;->a(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2512
    invoke-virtual {p0, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2513
    if-eqz v0, :cond_0

    .line 2514
    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2516
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/be/i;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/be/i;->a(Landroid/accounts/Account;)V

    return-void
.end method

.method private a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 348
    const-string v0, "weblogin:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 364
    :cond_0
    :goto_0
    return-void

    .line 351
    :cond_1
    if-eqz p2, :cond_0

    .line 354
    invoke-direct {p0, p1, p3}, Lcom/google/android/gms/auth/be/i;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 355
    if-nez v0, :cond_2

    .line 356
    const-string v0, "GLSUser"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[GLSUser]  not caching since unable to generate a cache key for uid "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", service "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 360
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v1, v1, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    invoke-virtual {v1, v2, v0, p4}, Landroid/accounts/AccountManager;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    if-eqz p5, :cond_0

    .line 362
    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v1, v1, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "EXP:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0, p5}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2099
    invoke-direct {p0, p1, p3}, Lcom/google/android/gms/auth/be/i;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2100
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v2, v0, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    .line 2101
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    invoke-virtual {v2, v0, v1, p2}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 2107
    if-nez p2, :cond_1

    .line 2110
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".pacl.visible_actions"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3, v5}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 2114
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".pacl.data"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3, v5}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 2118
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".all_circles_visible"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3, v5}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 2122
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".visible_graph"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3, v5}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 2126
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".all_contacts_visible"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3, v5}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 2130
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".has_show_circles"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3, v5}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 2134
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".show_circles"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3, v5}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 2138
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ".show_contacts"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1, v5}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 2203
    :cond_0
    :goto_0
    return-void

    .line 2149
    :cond_1
    if-eqz p4, :cond_3

    .line 2150
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".pacl.visible_actions"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p4}, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v3, v4}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 2154
    invoke-virtual {p4}, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;->b()Ljava/lang/String;

    move-result-object v0

    .line 2155
    if-nez v0, :cond_2

    .line 2156
    const-string v0, ""

    .line 2158
    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".pacl.data"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 2163
    :cond_3
    if-eqz p5, :cond_0

    .line 2164
    invoke-virtual {p5}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2165
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".all_circles_visible"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "1"

    invoke-virtual {v2, v0, v3, v4}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 2170
    :cond_4
    invoke-virtual {p5}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;->b()Ljava/lang/String;

    move-result-object v0

    .line 2171
    if-eqz v0, :cond_5

    .line 2172
    iget-object v3, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".visible_graph"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 2177
    :cond_5
    invoke-virtual {p5}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2178
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".all_contacts_visible"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "1"

    invoke-virtual {v2, v0, v3, v4}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 2183
    :cond_6
    invoke-virtual {p5}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;->f()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2184
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".has_show_circles"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "1"

    invoke-virtual {v2, v0, v3, v4}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 2189
    :cond_7
    invoke-virtual {p5}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;->d()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2190
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".show_circles"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "1"

    invoke-virtual {v2, v0, v3, v4}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 2195
    :cond_8
    invoke-virtual {p5}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2196
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ".show_contacts"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "1"

    invoke-virtual {v2, v0, v1, v3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private a(Ljava/util/List;Ljava/util/Map;Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 1178
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1179
    const-string v0, " Request: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1180
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 1181
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1182
    sget-object v4, Lcom/google/android/gms/auth/be/v;->b:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/be/v;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/google/android/gms/auth/be/v;->d:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/be/v;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/google/android/gms/auth/be/v;->R:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/be/v;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1185
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v3, "&"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 1188
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " RESULT: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " {"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1189
    if-eqz p2, :cond_2

    .line 1190
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1191
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v3, ";"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1194
    :cond_2
    if-eqz p3, :cond_3

    .line 1195
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " } Message: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1197
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/be/i;->c(Ljava/lang/String;)V

    .line 1198
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 4

    .prologue
    .line 2494
    const-string v0, "access_secret"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2495
    const-string v1, "parent_aid"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2496
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 2497
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 2498
    :cond_0
    const-string v0, "GLSUser"

    const-string v1, "importAccountForCloning: Secret or parent aid found null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2499
    const/4 v0, 0x0

    .line 2508
    :goto_0
    return v0

    .line 2501
    :cond_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2502
    const-string v3, "parent_aid"

    invoke-static {p2, v1, v3}, Lcom/google/android/gms/auth/be/i;->a(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 2503
    const-string v3, "oauthAccessToken"

    invoke-static {p2, v1, v3}, Lcom/google/android/gms/auth/be/i;->a(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 2504
    const-string v3, "sha1hash"

    invoke-static {p2, v1, v3}, Lcom/google/android/gms/auth/be/i;->a(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 2505
    sget-object v3, Lcom/google/android/gms/auth/be/w;->i:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v1, v3}, Lcom/google/android/gms/auth/be/i;->a(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 2506
    const-string v3, "flags"

    invoke-static {p2, v1, v3}, Lcom/google/android/gms/auth/be/i;->a(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 2507
    invoke-static {p1}, Lcom/google/android/gms/common/util/a;->b(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v3

    .line 2508
    invoke-virtual {v2, v3, v0, v1}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 274
    sget-boolean v0, Lcom/google/android/gms/auth/be/i;->j:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/auth/a/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 276
    if-nez v0, :cond_1

    .line 277
    const/4 v0, 0x0

    .line 285
    :goto_0
    return-object v0

    .line 279
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 280
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    const-string v0, ":"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 521
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 525
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 527
    iget-boolean v0, p0, Lcom/google/android/gms/auth/be/i;->f:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-gt v0, v1, :cond_0

    .line 528
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const-string v2, "oauthAccessToken"

    iget-object v3, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    :cond_0
    :goto_0
    return-void

    .line 536
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/gms/auth/be/i;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 537
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 538
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const-string v2, "sha1hash"

    iget-object v3, p0, Lcom/google/android/gms/auth/be/i;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b(Landroid/accounts/Account;ZLjava/lang/String;)V
    .locals 9

    .prologue
    .line 821
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 822
    const-string v0, "GLSUser"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[GLSUser] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " failed to clone account missing master token or encrypted password"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 920
    :goto_0
    return-void

    .line 829
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    .line 831
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v1, v1, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    .line 833
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 834
    const-string v3, "parent_aid"

    invoke-direct {p0}, Lcom/google/android/gms/auth/be/i;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 835
    const-string v3, "oauthAccessToken"

    iget-object v4, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const-string v5, "oauthAccessToken"

    invoke-virtual {v1, v4, v5}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    const-string v3, "sha1hash"

    iget-object v4, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const-string v5, "sha1hash"

    invoke-virtual {v1, v4, v5}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 839
    sget-object v3, Lcom/google/android/gms/auth/be/w;->i:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    sget-object v5, Lcom/google/android/gms/auth/be/w;->i:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v5}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 841
    const-string v3, "flags"

    iget-object v4, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const-string v5, "flags"

    invoke-virtual {v1, v4, v5}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    const-string v3, "GLSUser"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[GLSUser] "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " creating new account"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 844
    iget-object v3, p0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    iget-object v3, v3, Lcom/google/android/gms/auth/a/c;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/auth/be/q;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/q;

    move-result-object v3

    .line 846
    invoke-virtual {v3}, Lcom/google/android/gms/auth/be/q;->a()Ljava/lang/String;

    move-result-object v4

    .line 849
    :try_start_0
    invoke-virtual {v1, p1, v0, v2}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 851
    invoke-static {}, Landroid/content/ContentResolver;->getSyncAdapterTypes()[Landroid/content/SyncAdapterType;

    move-result-object v2

    .line 852
    array-length v5, v2

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v5, :cond_3

    aget-object v6, v2, v0

    .line 853
    iget-object v7, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    iget-object v8, v6, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 858
    iget-object v7, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v8, v6, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v7, v8}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v7

    .line 860
    iget-object v8, v6, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {p1, v8, v7}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 864
    iget-object v7, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v8, v6, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v7, v8}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v7

    .line 866
    iget-object v6, v6, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {p1, v6, v7}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 852
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 829
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->h:Ljava/lang/String;

    goto/16 :goto_1

    .line 871
    :cond_3
    :try_start_1
    const-string v0, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "[GLSUser] "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " removing old account"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 872
    new-instance v2, Lcom/google/android/gms/auth/be/k;

    invoke-direct {v2, p0, p3}, Lcom/google/android/gms/auth/be/k;-><init>(Lcom/google/android/gms/auth/be/i;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 888
    if-eqz p2, :cond_4

    .line 890
    :try_start_2
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/be/i;->a(Landroid/accounts/Account;)V

    .line 891
    const-string v0, "GLSUser"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[GLSUser] "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " set new account as backup account"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 899
    :cond_4
    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v2, v5}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 906
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v1, 0x4

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v0, v1, v2}, Lcom/google/android/gms/auth/be/q;->a(Ljava/lang/String;ILjava/lang/String;)V

    .line 910
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v0, v1, v2}, Lcom/google/android/gms/auth/be/q;->a(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 919
    :goto_4
    invoke-virtual {v3, v4}, Lcom/google/android/gms/auth/be/q;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 893
    :catch_0
    move-exception v0

    .line 894
    :try_start_4
    const-string v5, "GLSUser"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "[GLSUser] "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " failed to set backup account: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    .line 919
    :catchall_0
    move-exception v0

    invoke-virtual {v3, v4}, Lcom/google/android/gms/auth/be/q;->a(Ljava/lang/String;)V

    throw v0

    .line 916
    :cond_5
    :try_start_5
    const-string v0, "GLSUser"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[GLSUser] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " error adding new account"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_4
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2032
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "perm."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/auth/be/i;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/be/h;->a(Ljava/lang/String;)V

    .line 212
    :cond_0
    return-void
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 1201
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Ljava/lang/String;)Lcom/google/android/gms/auth/login/ao;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1668
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1675
    :goto_0
    return-object v0

    .line 1672
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/common/util/m;->c(Ljava/lang/String;)[B

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/auth/login/ao;

    invoke-direct {v2}, Lcom/google/android/gms/auth/login/ao;-><init>()V

    array-length v3, v0

    invoke-virtual {v2, v0, v3}, Lcom/google/protobuf/a/f;->a([BI)Lcom/google/protobuf/a/f;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/login/ao;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1673
    :catch_0
    move-exception v0

    .line 1674
    const-string v2, "GLSUser"

    const-string v3, "Failed to parse consent data"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 1675
    goto :goto_0
.end method

.method private d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1353
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const-string v2, "parent_aid"

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1354
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1357
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/common/c/a;

    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    iget-object v1, v1, Lcom/google/android/gms/auth/a/c;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/c/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/c/a;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private e()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1943
    iget-boolean v0, p0, Lcom/google/android/gms/auth/be/i;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1946
    :cond_0
    const-string v0, "GLSUser"

    const-string v1, "Resetting account secrets!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1948
    iput-object v2, p0, Lcom/google/android/gms/auth/be/i;->h:Ljava/lang/String;

    .line 1949
    iput-object v2, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    .line 1950
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 1951
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const-string v2, "sha1hash"

    const-string v3, ""

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 1955
    :cond_1
    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1972
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    sget-object v2, Lcom/google/android/gms/auth/be/w;->i:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1974
    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1989
    :cond_0
    return-void

    .line 1977
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    sget-object v2, Lcom/google/android/gms/auth/be/w;->i:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 1980
    invoke-static {}, Landroid/content/ContentResolver;->getSyncAdapterTypes()[Landroid/content/SyncAdapterType;

    move-result-object v1

    .line 1981
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1982
    iget-object v4, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->type:Ljava/lang/String;

    iget-object v5, v3, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1983
    iget-object v4, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v5, v3, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_2

    .line 1984
    iget-object v4, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v5, v3, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    const/4 v6, -0x1

    invoke-static {v4, v5, v6}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 1985
    iget-object v4, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    invoke-static {v4, v3, v5}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1981
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static f(Ljava/lang/String;)Ljava/util/Map;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2244
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2245
    const-string v0, "\n"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 2246
    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 2247
    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 2248
    if-ltz v6, :cond_0

    .line 2249
    invoke-virtual {v5, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2246
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2252
    :cond_1
    return-object v2
.end method


# virtual methods
.method public final a()Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 2460
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2461
    const/4 v0, 0x0

    .line 2476
    :goto_0
    return-object v0

    .line 2463
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    .line 2465
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v2, v1, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    .line 2466
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2467
    const-string v3, "access_secret"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2468
    const-string v0, "parent_aid"

    invoke-direct {p0}, Lcom/google/android/gms/auth/be/i;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2469
    const-string v0, "oauthAccessToken"

    iget-object v3, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const-string v4, "oauthAccessToken"

    invoke-virtual {v2, v3, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2472
    const-string v0, "sha1hash"

    iget-object v3, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const-string v4, "sha1hash"

    invoke-virtual {v2, v3, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2473
    sget-object v0, Lcom/google/android/gms/auth/be/w;->i:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    sget-object v4, Lcom/google/android/gms/auth/be/w;->i:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475
    const-string v0, "flags"

    iget-object v3, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const-string v4, "flags"

    invoke-virtual {v2, v3, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 2476
    goto :goto_0

    .line 2463
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->h:Ljava/lang/String;

    goto :goto_1
.end method

.method protected final a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 7

    .prologue
    .line 1522
    if-nez p5, :cond_0

    .line 1523
    invoke-virtual {p1, p4}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .line 1528
    :cond_0
    if-eqz p3, :cond_4

    .line 1529
    const-string v1, ","

    invoke-virtual {p3, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1530
    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 1531
    sget-object v5, Lcom/google/android/gms/auth/be/w;->B:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v5}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1532
    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->l()Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .line 1536
    :cond_1
    sget-object v5, Lcom/google/android/gms/auth/be/w;->D:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v5}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1537
    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->n()Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .line 1530
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1541
    :cond_3
    invoke-direct {p0, p3}, Lcom/google/android/gms/auth/be/i;->e(Ljava/lang/String;)V

    .line 1544
    :cond_4
    if-eqz p2, :cond_5

    .line 1545
    invoke-virtual {p1, p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .line 1549
    :cond_5
    if-eqz p6, :cond_6

    if-eqz p7, :cond_6

    .line 1550
    invoke-virtual {p1, p6}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->f(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v1

    invoke-virtual {v1, p7}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->g(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .line 1553
    :cond_6
    if-eqz p8, :cond_7

    .line 1554
    invoke-virtual {p1, p8}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->e(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .line 1557
    :cond_7
    if-eqz p9, :cond_8

    .line 1558
    move-object/from16 v0, p9

    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->h(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .line 1561
    :cond_8
    if-eqz p10, :cond_9

    .line 1562
    move-object/from16 v0, p10

    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->i(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .line 1564
    :cond_9
    sget-object v1, Lcom/google/android/gms/auth/firstparty/shared/k;->a:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v1

    return-object v1
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/util/Map;ILjava/lang/String;ZZLjava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 13

    .prologue
    .line 1689
    const-string v1, "SID"

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "LSID"

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move-object/from16 v1, p4

    :goto_0
    const-string v2, "GLSUser"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[GLSUser]  extracting token using key:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Ljava/lang/String;

    .line 1690
    sget-object v1, Lcom/google/android/gms/auth/be/w;->c:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Ljava/lang/String;

    .line 1693
    sget-object v1, Lcom/google/android/gms/auth/be/w;->o:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-nez v1, :cond_4

    if-eqz p5, :cond_2

    const/4 v1, 0x0

    .line 1704
    :goto_1
    if-eqz v1, :cond_7

    .line 1818
    :goto_2
    return-object p1

    .line 1689
    :cond_1
    sget-object v1, Lcom/google/android/gms/auth/be/w;->a:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1693
    :cond_2
    if-nez v12, :cond_3

    if-eqz v7, :cond_3

    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    sget-object v1, Lcom/google/android/gms/auth/firstparty/shared/k;->y:Lcom/google/android/gms/auth/firstparty/shared/k;

    move-object/from16 v0, p4

    invoke-direct {p0, p1, p2, v1, v0}, Lcom/google/android/gms/auth/be/i;->a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/util/Map;Lcom/google/android/gms/auth/firstparty/shared/k;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    const/4 v1, 0x1

    goto :goto_1

    :cond_4
    const-string v2, "auto"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[GLSUser]  Permission for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to access "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " will be managed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v1, "1"

    sget-object v2, Lcom/google/android/gms/auth/be/w;->p:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "remotely."

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    move-object v1, p0

    move-object/from16 v2, p4

    move-object/from16 v4, p7

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/auth/be/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)V

    :goto_3
    const-string v1, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[GLSUser] "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_1

    :cond_5
    const-string v1, "locally."

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "1"

    move-object v1, p0

    move-object/from16 v2, p4

    move-object/from16 v4, p7

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/auth/be/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)V

    goto :goto_3

    :cond_6
    sget-object v1, Lcom/google/android/gms/auth/firstparty/shared/k;->y:Lcom/google/android/gms/auth/firstparty/shared/k;

    move-object/from16 v0, p4

    invoke-direct {p0, p1, p2, v1, v0}, Lcom/google/android/gms/auth/be/i;->a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/util/Map;Lcom/google/android/gms/auth/firstparty/shared/k;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    const/4 v1, 0x1

    goto/16 :goto_1

    .line 1710
    :cond_7
    const/4 v1, 0x1

    invoke-virtual {p0, p2, v1}, Lcom/google/android/gms/auth/be/i;->a(Ljava/util/Map;Z)V

    .line 1712
    if-eqz v12, :cond_10

    .line 1715
    sget-object v1, Lcom/google/android/gms/auth/be/w;->t:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1716
    const-string v2, "0"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    const/4 v1, 0x1

    .line 1717
    :goto_4
    invoke-virtual {p1, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->c(Z)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .line 1718
    iget-boolean v2, p0, Lcom/google/android/gms/auth/be/i;->a:Z

    if-eqz v2, :cond_8

    .line 1719
    const-string v2, "GLSUser"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[GLSUser]  canUpgradePlusValue: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1722
    :cond_8
    iget-boolean v1, p0, Lcom/google/android/gms/auth/be/i;->d:Z

    if-eqz v1, :cond_c

    .line 1741
    const-string v1, "SID"

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, "LSID"

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 1746
    :cond_9
    const-string v1, "SID"

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1747
    const-string v1, "LSID"

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Ljava/lang/String;

    .line 1748
    if-eqz v5, :cond_a

    .line 1749
    const-string v2, "SID"

    const/4 v6, 0x0

    move-object v1, p0

    move/from16 v3, p3

    move-object/from16 v4, p7

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/auth/be/i;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1751
    :cond_a
    if-eqz v7, :cond_b

    .line 1752
    const-string v2, "LSID"

    const/4 v6, 0x0

    move-object v1, p0

    move/from16 v3, p3

    move-object/from16 v4, p7

    move-object v5, v7

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/auth/be/i;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1760
    :cond_b
    :goto_5
    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->k:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 1761
    invoke-direct {p0}, Lcom/google/android/gms/auth/be/i;->b()V

    .line 1762
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/auth/be/i;->k:Ljava/lang/String;

    .line 1765
    :cond_c
    sget-object v1, Lcom/google/android/gms/auth/be/w;->b:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1766
    sget-object v1, Lcom/google/android/gms/auth/be/w;->i:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1767
    sget-object v1, Lcom/google/android/gms/auth/be/w;->v:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 1768
    sget-object v1, Lcom/google/android/gms/auth/be/w;->w:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 1769
    sget-object v1, Lcom/google/android/gms/auth/be/w;->l:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 1770
    sget-object v1, Lcom/google/android/gms/auth/be/w;->x:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 1771
    sget-object v1, Lcom/google/android/gms/auth/be/w;->y:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 1772
    iget-boolean v1, p0, Lcom/google/android/gms/auth/be/i;->d:Z

    if-nez v1, :cond_f

    const/4 v6, 0x1

    :goto_6
    move-object v1, p0

    move-object v2, p1

    move-object v5, v12

    invoke-virtual/range {v1 .. v11}, Lcom/google/android/gms/auth/be/i;->a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object p1

    goto/16 :goto_2

    .line 1716
    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_4

    .line 1756
    :cond_e
    sget-object v1, Lcom/google/android/gms/auth/be/w;->q:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object v1, p0

    move-object/from16 v2, p4

    move/from16 v3, p3

    move-object/from16 v4, p7

    move-object v5, v12

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/auth/be/i;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1772
    :cond_f
    const/4 v6, 0x0

    goto :goto_6

    .line 1786
    :cond_10
    if-nez v7, :cond_11

    .line 1787
    const-string v1, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t get error message from reply:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1788
    sget-object v1, Lcom/google/android/gms/auth/firstparty/shared/k;->k:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/firstparty/shared/k;->a()Ljava/lang/String;

    move-result-object v7

    .line 1792
    :cond_11
    const-string v1, "badauth"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 1793
    sget-object v1, Lcom/google/android/gms/auth/firstparty/shared/k;->b:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/firstparty/shared/k;->a()Ljava/lang/String;

    move-result-object v7

    .line 1795
    :cond_12
    invoke-static {v7}, Lcom/google/android/gms/auth/a/h;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/k;

    move-result-object v1

    .line 1796
    move-object/from16 v0, p4

    invoke-direct {p0, p1, p2, v1, v0}, Lcom/google/android/gms/auth/be/i;->a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/util/Map;Lcom/google/android/gms/auth/firstparty/shared/k;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object p1

    .line 1798
    sget-object v2, Lcom/google/android/gms/auth/firstparty/shared/k;->l:Lcom/google/android/gms/auth/firstparty/shared/k;

    if-ne v1, v2, :cond_14

    .line 1799
    sget-object v1, Lcom/google/android/gms/auth/be/w;->e:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1800
    sget-object v2, Lcom/google/android/gms/auth/be/w;->g:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1801
    move-object/from16 v0, p7

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/gms/auth/be/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;

    move-result-object v1

    .line 1803
    invoke-virtual {p1, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .line 1817
    :cond_13
    :goto_7
    const-string v1, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GLS error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/auth/be/i;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1804
    :cond_14
    sget-object v2, Lcom/google/android/gms/auth/firstparty/shared/k;->b:Lcom/google/android/gms/auth/firstparty/shared/k;

    if-ne v1, v2, :cond_15

    if-nez p6, :cond_15

    .line 1809
    invoke-direct {p0}, Lcom/google/android/gms/auth/be/i;->e()V

    goto :goto_7

    .line 1810
    :cond_15
    sget-object v2, Lcom/google/android/gms/auth/firstparty/shared/k;->x:Lcom/google/android/gms/auth/firstparty/shared/k;

    if-ne v1, v2, :cond_13

    .line 1812
    invoke-direct {p0}, Lcom/google/android/gms/auth/be/i;->e()V

    .line 1814
    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v1, v1, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const-string v3, "oauthAccessToken"

    const-string v4, "1"

    invoke-virtual {v1, v2, v3, v4}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Ljava/lang/String;Z)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 19

    .prologue
    .line 1045
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a()Ljava/lang/String;

    move-result-object v18

    .line 1046
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v14

    .line 1050
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    iget-object v2, v2, Lcom/google/android/gms/auth/a/c;->a:Landroid/content/Context;

    move-object/from16 v0, v18

    invoke-static {v2, v0, v14}, Lcom/google/android/gms/common/util/a;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1054
    new-instance v2, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-direct {v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;-><init>()V

    sget-object v3, Lcom/google/android/gms/auth/firstparty/shared/k;->C:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v2

    .line 1154
    :cond_0
    :goto_0
    return-object v2

    .line 1056
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->b()Ljava/lang/String;

    move-result-object v2

    .line 1057
    if-nez v2, :cond_1f

    .line 1058
    const-string v2, "ac2dm"

    move-object v15, v2

    .line 1060
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->d()I

    move-result v16

    .line 1061
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v17

    .line 1065
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->h()Z

    move-result v2

    if-nez v2, :cond_5

    if-eqz p5, :cond_5

    const-string v2, "^^_account_id_^^"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1067
    if-nez v15, :cond_1e

    const-string v2, "ac2dm"

    :goto_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/gms/auth/be/i;->a:Z

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[GLSUser]  Looking for cached grant for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to access "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " on behalf of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/auth/be/i;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "GLSUser"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/gms/auth/be/i;->d:Z

    if-eqz v3, :cond_f

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/auth/be/i;->c()Z

    move-result v3

    if-eqz v3, :cond_f

    if-nez v2, :cond_9

    const/4 v6, 0x0

    :cond_3
    :goto_3
    if-eqz v6, :cond_f

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/gms/auth/be/i;->a:Z

    if-eqz v3, :cond_4

    const-string v3, "GLSUser"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[GLSUser]  Return from cache "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    new-instance v3, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-direct {v3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;-><init>()V

    invoke-virtual {v3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->u()Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/auth/be/i;->b:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v12}, Lcom/google/android/gms/auth/be/i;->a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v2

    .line 1068
    :goto_4
    if-nez v2, :cond_0

    .line 1072
    :cond_5
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->d()Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;

    move-result-object v6

    .line 1073
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->e()Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;

    move-result-object v7

    .line 1074
    sget-object v2, Lcom/google/android/gms/auth/be/l;->a:[I

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->l()Lcom/google/android/gms/auth/firstparty/dataservice/bg;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/auth/firstparty/dataservice/bg;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1100
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_10

    if-nez v16, :cond_10

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/gms/auth/be/i;->a:Z

    if-eqz v2, :cond_6

    const-string v2, "GLSUser"

    const-string v3, "[GLSUser]  has_permission because running in pre SDK 11 AccountManager"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    const/4 v9, 0x1

    .line 1101
    :cond_7
    :goto_5
    if-eqz v9, :cond_1d

    .line 1102
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v2, v2, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    .line 1103
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v15, v1}, Lcom/google/android/gms/auth/be/i;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1104
    if-nez v6, :cond_1c

    .line 1106
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".pacl.visible_actions"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1109
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ".pacl.data"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1112
    if-eqz v4, :cond_1c

    .line 1113
    new-instance v6, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;

    invoke-direct {v6, v3, v4}, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v16, v6

    .line 1116
    :goto_6
    if-nez v7, :cond_1b

    .line 1118
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".all_circles_visible"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1121
    const-string v4, "1"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 1122
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".all_contacts_visible"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1125
    const-string v5, "1"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 1126
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, ".visible_graph"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v4, v6}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1129
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ".show_circles"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v6, v10}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1132
    const-string v10, "1"

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 1133
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ".show_contacts"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v10, v11}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1136
    const-string v11, "1"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    .line 1137
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v12, ".has_show_circles"

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v11, v8}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1140
    const-string v8, "1"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    .line 1142
    if-nez v3, :cond_8

    if-nez v5, :cond_8

    if-eqz v4, :cond_1b

    .line 1143
    :cond_8
    new-instance v2, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;

    move v7, v10

    invoke-direct/range {v2 .. v8}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;-><init>(ZLjava/lang/String;ZZZZ)V

    move-object/from16 v17, v2

    .line 1154
    :goto_7
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->g()Z

    move-result v7

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->f()Z

    move-result v8

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->h()Z

    move-result v12

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->c()Landroid/os/Bundle;

    move-result-object v14

    move-object/from16 v2, p0

    move-object v3, v15

    move-object/from16 v4, v18

    move-object/from16 v13, p1

    move-object/from16 v15, p3

    move-object/from16 v18, p4

    invoke-virtual/range {v2 .. v18}, Lcom/google/android/gms/auth/be/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZZLcom/google/android/gms/auth/firstparty/shared/AppDescription;Landroid/os/Bundle;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v2

    goto/16 :goto_0

    .line 1067
    :cond_9
    if-nez v16, :cond_a

    const/4 v6, 0x0

    goto/16 :goto_3

    :cond_a
    const-string v3, "weblogin:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    const/4 v6, 0x0

    goto/16 :goto_3

    :cond_b
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_c

    const/4 v6, 0x0

    goto/16 :goto_3

    :cond_c
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/auth/be/i;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_e

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/gms/auth/be/i;->a:Z

    if-eqz v3, :cond_d

    const-string v3, "[GLSUser] No cache hit because unable to get cache key for uid: %s, service: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "GLSUser"

    invoke-static {v4, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    const/4 v6, 0x0

    goto/16 :goto_3

    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v4, v4, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    invoke-virtual {v4, v5, v3}, Landroid/accounts/AccountManager;->peekAuthToken(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v4, v4, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "EXP:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    cmp-long v3, v4, v8

    if-gez v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v3, v3, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v3, v4, v6}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v6, 0x0

    goto/16 :goto_3

    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 1077
    :pswitch_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->d()I

    const-string v4, "1"

    move-object/from16 v2, p0

    move-object v5, v14

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/auth/be/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)V

    .line 1084
    const/4 v9, 0x1

    move-object/from16 v17, v7

    move-object/from16 v16, v6

    .line 1085
    goto/16 :goto_7

    .line 1087
    :pswitch_1
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->d()I

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v8, p0

    move-object v11, v14

    invoke-direct/range {v8 .. v13}, Lcom/google/android/gms/auth/be/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)V

    .line 1094
    const/4 v9, 0x0

    move-object/from16 v17, v7

    move-object/from16 v16, v6

    .line 1095
    goto/16 :goto_7

    .line 1100
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v2, v2, Lcom/google/android/gms/auth/be/h;->d:Ljava/lang/String;

    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/gms/auth/be/i;->a:Z

    if-eqz v2, :cond_11

    const-string v2, "[GLSUser]  Not has_permission because %s = %s"

    const-string v3, "GLSUser"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v15, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v8, v8, Lcom/google/android/gms/auth/be/h;->d:Ljava/lang/String;

    aput-object v8, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11
    const/4 v9, 0x0

    goto/16 :goto_5

    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v2, v2, Lcom/google/android/gms/auth/be/h;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget v3, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    move/from16 v0, v16

    if-ne v0, v3, :cond_15

    const/4 v2, 0x1

    :goto_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    move/from16 v0, v16

    invoke-virtual {v4, v3, v0}, Lcom/google/android/gms/auth/a/c;->a(II)Z

    move-result v3

    if-nez v2, :cond_13

    if-eqz v3, :cond_16

    :cond_13
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/gms/auth/be/i;->a:Z

    if-eqz v4, :cond_14

    const-string v4, "[GLSUser]  has_permission because either isSameUser (%b) or isSameSignature (%b)"

    const-string v5, "GLSUser"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v8, v9

    const/4 v2, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v8, v2

    invoke-static {v4, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_14
    const/4 v9, 0x1

    goto/16 :goto_5

    :cond_15
    const/4 v2, 0x0

    goto :goto_8

    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v2, v2, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v15, v1}, Lcom/google/android/gms/auth/be/i;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v2, "1"

    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Lcom/google/android/gms/auth/a/c;->a(I)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/a/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_17
    if-nez v2, :cond_18

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    :cond_18
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "perm."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v3, v3, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    invoke-virtual {v3, v4, v2}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v2, "1"

    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v8, p0

    move-object v9, v15

    move-object/from16 v11, v17

    invoke-direct/range {v8 .. v13}, Lcom/google/android/gms/auth/be/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)V

    :cond_19
    const-string v2, "1"

    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "perm."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v3, v3, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    invoke-virtual {v3, v4, v2}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v2, "1"

    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v8, p0

    move-object v9, v15

    move-object/from16 v11, v17

    invoke-direct/range {v8 .. v13}, Lcom/google/android/gms/auth/be/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)V

    :cond_1a
    const-string v2, "1"

    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/gms/auth/be/i;->a:Z

    if-eqz v2, :cond_7

    const-string v2, "[GLSUser]  has_permission: %b - tokenType: %s, uid: %d, packageName:  %s)"

    const-string v3, "GLSUser"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x1

    aput-object v15, v4, v5

    const/4 v5, 0x2

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x3

    aput-object v17, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :cond_1b
    move-object/from16 v17, v7

    goto/16 :goto_7

    :cond_1c
    move-object/from16 v16, v6

    goto/16 :goto_6

    :cond_1d
    move-object/from16 v17, v7

    move-object/from16 v16, v6

    goto/16 :goto_7

    :cond_1e
    move-object v2, v15

    goto/16 :goto_2

    :cond_1f
    move-object v15, v2

    goto/16 :goto_1

    .line 1074
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 18

    .prologue
    .line 413
    const-string v2, "ac2dm"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/a/c;->a()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v12

    new-instance v13, Landroid/os/Bundle;

    invoke-direct {v13}, Landroid/os/Bundle;-><init>()V

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v1, p0

    move-object/from16 v5, p1

    move-object/from16 v14, p2

    invoke-virtual/range {v1 .. v17}, Lcom/google/android/gms/auth/be/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZZLcom/google/android/gms/auth/firstparty/shared/AppDescription;Landroid/os/Bundle;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v1

    .line 431
    invoke-virtual {v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 432
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/auth/be/i;->b()V

    .line 435
    new-instance v2, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-direct {v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;-><init>()V

    sget-object v3, Lcom/google/android/gms/auth/firstparty/shared/k;->a:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v1

    .line 439
    :cond_0
    return-object v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 18

    .prologue
    .line 451
    const-string v2, "ac2dm"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/a/c;->a()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v12

    new-instance v13, Landroid/os/Bundle;

    invoke-direct {v13}, Landroid/os/Bundle;-><init>()V

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v1, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v14, p3

    invoke-virtual/range {v1 .. v17}, Lcom/google/android/gms/auth/be/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZZLcom/google/android/gms/auth/firstparty/shared/AppDescription;Landroid/os/Bundle;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v1

    .line 469
    invoke-virtual {v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 470
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/auth/be/i;->b()V

    .line 472
    new-instance v2, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-direct {v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;-><init>()V

    sget-object v3, Lcom/google/android/gms/auth/firstparty/shared/k;->a:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v1

    .line 476
    :cond_0
    return-object v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZLandroid/os/Bundle;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 19

    .prologue
    .line 1382
    :try_start_0
    const-string v3, "LSID"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/a/c;->a()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v13

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    move/from16 v7, p8

    move/from16 v8, p9

    move-object/from16 v14, p10

    invoke-virtual/range {v2 .. v18}, Lcom/google/android/gms/auth/be/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZZLcom/google/android/gms/auth/firstparty/shared/AppDescription;Landroid/os/Bundle;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v3

    .line 1399
    invoke-virtual {v3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->c()Ljava/lang/String;

    move-result-object v2

    .line 1400
    if-nez v2, :cond_1

    .line 1401
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/gms/auth/be/i;->a:Z

    if-eqz v2, :cond_0

    .line 1402
    const-string v2, "%s LSID wasn\'t retrieved due to status: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "[GLSUser] "

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Lcom/google/android/gms/auth/firstparty/shared/k;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/auth/firstparty/shared/k;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1406
    const-string v3, "GLSUser"

    invoke-static {v3, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1408
    :cond_0
    new-instance v2, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-direct {v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;-><init>()V

    sget-object v3, Lcom/google/android/gms/auth/firstparty/shared/k;->b:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v2

    .line 1504
    :goto_0
    return-object v2

    .line 1410
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v4, v4, Lcom/google/android/gms/auth/be/h;->a:Landroid/content/Context;

    const-string v5, "device_country"

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/common/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1415
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    iget-object v5, v5, Lcom/google/android/gms/auth/a/c;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v5, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1418
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v6, v6, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const-string v8, "parent_aid"

    invoke-virtual {v6, v7, v8}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1426
    new-instance v7, Lcom/google/android/gms/auth/be/b;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    invoke-direct {v7, v8}, Lcom/google/android/gms/auth/be/b;-><init>(Lcom/google/android/gms/auth/a/c;)V

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/google/android/gms/auth/be/b;->e(Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    move-result-object v7

    new-instance v8, Lcom/google/android/gms/common/c/a;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    iget-object v9, v9, Lcom/google/android/gms/auth/a/c;->a:Landroid/content/Context;

    invoke-direct {v8, v9}, Lcom/google/android/gms/common/c/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8}, Lcom/google/android/gms/common/c/a;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/gms/auth/be/b;->c(Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/google/android/gms/auth/be/b;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/android/gms/auth/be/b;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    move-result-object v4

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/gms/auth/be/i;->f:Z

    invoke-virtual {v4, v5}, Lcom/google/android/gms/auth/be/b;->a(Z)Lcom/google/android/gms/auth/be/b;

    move-result-object v4

    const-string v5, "LSID"

    invoke-virtual {v4, v5, v2}, Lcom/google/android/gms/auth/be/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/auth/be/v;->z:Lcom/google/android/gms/auth/be/v;

    move-object/from16 v0, p2

    invoke-virtual {v2, v4, v0}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/auth/be/v;->y:Lcom/google/android/gms/auth/be/v;

    move-object/from16 v0, p3

    invoke-virtual {v2, v4, v0}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/auth/be/v;->B:Lcom/google/android/gms/auth/be/v;

    move-object/from16 v0, p4

    invoke-virtual {v2, v4, v0}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/auth/be/v;->L:Lcom/google/android/gms/auth/be/v;

    move/from16 v0, p5

    invoke-virtual {v2, v4, v0}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Z)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/auth/be/v;->K:Lcom/google/android/gms/auth/be/v;

    move/from16 v0, p6

    invoke-virtual {v2, v4, v0}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Z)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/auth/be/v;->G:Lcom/google/android/gms/auth/be/v;

    move-object/from16 v0, p7

    invoke-virtual {v2, v4, v0}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    move/from16 v0, p8

    invoke-virtual {v2, v0}, Lcom/google/android/gms/auth/be/b;->b(Z)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/google/android/gms/auth/be/b;->d(Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    .line 1442
    new-instance v4, Lorg/json/JSONStringer;

    invoke-direct {v4}, Lorg/json/JSONStringer;-><init>()V

    .line 1448
    invoke-virtual {v4}, Lorg/json/JSONStringer;->object()Lorg/json/JSONStringer;

    .line 1449
    iget-object v2, v2, Lcom/google/android/gms/auth/be/b;->a:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/http/NameValuePair;

    .line 1450
    invoke-interface {v2}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v6

    invoke-interface {v2}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1500
    :catch_0
    move-exception v2

    .line 1501
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/gms/auth/be/i;->a:Z

    if-eqz v3, :cond_2

    .line 1502
    const-string v3, "GLSUser"

    const-string v4, "[GLSUser]  createProfile JSONException: "

    invoke-static {v3, v4, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1504
    :cond_2
    new-instance v2, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-direct {v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;-><init>()V

    sget-object v3, Lcom/google/android/gms/auth/firstparty/shared/k;->m:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v2

    goto/16 :goto_0

    .line 1454
    :cond_3
    :try_start_1
    sget-object v2, Lcom/google/android/gms/auth/be/d;->aA:Lcom/google/android/gms/auth/be/d;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/d;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v2

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    int-to-long v6, v5

    invoke-virtual {v2, v6, v7}, Lorg/json/JSONStringer;->value(J)Lorg/json/JSONStringer;

    .line 1455
    sget-object v2, Lcom/google/android/gms/auth/be/d;->aB:Lcom/google/android/gms/auth/be/d;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/d;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    iget v5, v5, Lcom/google/android/gms/auth/a/c;->d:I

    int-to-long v6, v5

    invoke-virtual {v2, v6, v7}, Lorg/json/JSONStringer;->value(J)Lorg/json/JSONStringer;

    .line 1457
    invoke-virtual {v4}, Lorg/json/JSONStringer;->endObject()Lorg/json/JSONStringer;

    .line 1462
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5}, Ljava/util/LinkedHashMap;-><init>()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    iget-object v6, v6, Lcom/google/android/gms/auth/a/c;->c:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Lcom/google/android/gms/auth/be/h;->a(Ljava/util/Map;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v5

    .line 1466
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    sget-object v2, Lcom/google/android/gms/auth/b/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v7, "createProfile"

    invoke-virtual {v6, v2, v4, v7, v5}, Lcom/google/android/gms/auth/be/h;->b(Ljava/lang/String;Lorg/json/JSONStringer;Ljava/lang/String;Ljava/util/Map;)Lorg/json/JSONObject;

    move-result-object v2

    .line 1472
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1473
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[GLSUser]  createProfile: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/gms/auth/be/i;->c(Ljava/lang/String;)V

    .line 1474
    invoke-static {v2}, Lcom/google/android/gms/auth/a/h;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/auth/firstparty/shared/k;

    move-result-object v4

    .line 1475
    sget-object v5, Lcom/google/android/gms/auth/firstparty/shared/k;->a:Lcom/google/android/gms/auth/firstparty/shared/k;

    if-eq v5, v4, :cond_4

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/gms/auth/be/i;->a:Z

    if-eqz v5, :cond_4

    .line 1476
    const-string v5, "%s createProfile SetupServlet request failed with status: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "[GLSUser] "

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual {v4}, Lcom/google/android/gms/auth/firstparty/shared/k;->a()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1480
    const-string v6, "GLSUser"

    invoke-static {v6, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1488
    :cond_4
    sget-object v5, Lcom/google/android/gms/auth/be/w;->i:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v5}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1489
    sget-object v5, Lcom/google/android/gms/auth/be/w;->i:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v5}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1493
    if-eqz v2, :cond_5

    .line 1494
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gms/auth/be/i;->e(Ljava/lang/String;)V

    .line 1497
    :cond_5
    invoke-virtual {v3, v4}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .line 1498
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v2, v3

    .line 1499
    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZZLcom/google/android/gms/auth/firstparty/shared/AppDescription;Landroid/os/Bundle;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 18

    .prologue
    .line 940
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/gms/auth/be/i;->a:Z

    if-eqz v2, :cond_0

    .line 941
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[GLSUser]  Requesting Google to grant "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p11 .. p11}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " access to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " on behalf of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 948
    const-string v3, "GLSUser"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 950
    :cond_0
    const/4 v2, 0x0

    .line 951
    if-eqz p3, :cond_2

    .line 952
    const/4 v2, 0x1

    .line 953
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    .line 954
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/be/i;->a(Ljava/lang/String;)V

    move/from16 v17, v2

    :goto_0
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p7

    move/from16 v8, p6

    move/from16 v9, p8

    move/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v16, p16

    .line 960
    invoke-virtual/range {v2 .. v16}, Lcom/google/android/gms/auth/be/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZLcom/google/android/gms/auth/firstparty/shared/AppDescription;Landroid/os/Bundle;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v4

    .line 975
    new-instance v3, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-direct {v3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;-><init>()V

    invoke-virtual/range {p11 .. p11}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->d()I

    move-result v5

    invoke-virtual/range {p11 .. p11}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v2, p0

    move-object/from16 v6, p1

    move/from16 v7, p7

    move/from16 v8, p9

    move-object/from16 v10, p14

    move-object/from16 v11, p15

    invoke-virtual/range {v2 .. v11}, Lcom/google/android/gms/auth/be/i;->a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/util/Map;ILjava/lang/String;ZZLjava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v2

    .line 986
    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->c()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    if-eqz v17, :cond_1

    .line 987
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/auth/be/i;->b()V

    .line 989
    :cond_1
    return-object v2

    .line 955
    :cond_2
    if-eqz p4, :cond_3

    .line 956
    const/4 v2, 0x1

    .line 957
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    .line 958
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/gms/auth/be/i;->h:Ljava/lang/String;

    :cond_3
    move/from16 v17, v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ZLandroid/os/Bundle;ZLjava/lang/String;ZZLcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;Z)Ljava/util/List;
    .locals 8

    .prologue
    .line 1224
    new-instance v3, Lcom/google/android/gms/auth/be/b;

    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    invoke-direct {v3, v1}, Lcom/google/android/gms/auth/be/b;-><init>(Lcom/google/android/gms/auth/a/c;)V

    .line 1225
    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v1, v1, Lcom/google/android/gms/auth/be/h;->a:Landroid/content/Context;

    const-string v2, "device_country"

    const/4 v4, 0x0

    invoke-static {v1, v2, v4}, Lcom/google/android/gms/common/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1229
    new-instance v1, Lcom/google/android/gms/common/c/a;

    iget-object v4, p0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    iget-object v4, v4, Lcom/google/android/gms/auth/a/c;->a:Landroid/content/Context;

    invoke-direct {v1, v4}, Lcom/google/android/gms/common/c/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/google/android/gms/common/c/a;->a()Ljava/lang/String;

    move-result-object v4

    .line 1230
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1232
    if-nez p9, :cond_0

    if-nez p5, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v1, v1, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v6, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const-string v7, "GoogleUserId"

    invoke-virtual {v1, v6, v7}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_a

    :cond_0
    const/4 v1, 0x1

    .line 1235
    :goto_0
    invoke-virtual {v3, v2}, Lcom/google/android/gms/auth/be/b;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    sget-object v6, Lcom/google/android/gms/auth/be/v;->D:Lcom/google/android/gms/auth/be/v;

    move/from16 v0, p10

    invoke-virtual {v2, v6, v0}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Z)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/google/android/gms/auth/be/b;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v6, Lcom/google/android/gms/auth/be/v;->I:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v2, v6, v5}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;I)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    iget v5, v5, Lcom/google/android/gms/auth/a/c;->d:I

    sget-object v6, Lcom/google/android/gms/auth/be/v;->s:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v2, v6, v5}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;I)Lcom/google/android/gms/auth/be/b;

    const-string v5, "HOSTED_OR_GOOGLE"

    const-string v6, "accountType"

    invoke-virtual {v2, v6, v5}, Lcom/google/android/gms/auth/be/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    invoke-virtual {v5, p3}, Lcom/google/android/gms/auth/a/c;->b(I)Z

    move-result v5

    sget-object v6, Lcom/google/android/gms/auth/be/v;->t:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v2, v6, v5}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Z)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/android/gms/auth/be/b;->e(Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    sget-object v5, Lcom/google/android/gms/auth/be/v;->p:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v2, v5, p7}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Z)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    sget-object v5, Lcom/google/android/gms/auth/be/v;->C:Lcom/google/android/gms/auth/be/v;

    move/from16 v0, p9

    invoke-virtual {v2, v5, v0}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Z)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    invoke-virtual {v2, p5}, Lcom/google/android/gms/auth/be/b;->b(Z)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    sget-object v5, Lcom/google/android/gms/auth/be/v;->c:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v2, v5, p1}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    const-string v5, "android"

    sget-object v6, Lcom/google/android/gms/auth/be/v;->i:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v2, v6, v5}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/android/gms/auth/be/b;->c(Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    iget-boolean v4, p0, Lcom/google/android/gms/auth/be/i;->f:Z

    invoke-virtual {v2, v4}, Lcom/google/android/gms/auth/be/b;->a(Z)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/auth/be/v;->E:Lcom/google/android/gms/auth/be/v;

    move/from16 v0, p15

    invoke-virtual {v2, v4, v0}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Z)Lcom/google/android/gms/auth/be/b;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/auth/be/v;->F:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v2, v4, v1}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Z)Lcom/google/android/gms/auth/be/b;

    .line 1252
    if-eqz p11, :cond_1

    .line 1253
    invoke-virtual/range {p11 .. p11}, Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p11 .. p11}, Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1257
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    if-eqz v1, :cond_2

    .line 1264
    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v1, v1, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const-string v4, "parent_aid"

    invoke-virtual {v1, v2, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1265
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1266
    invoke-virtual {v3, v1}, Lcom/google/android/gms/auth/be/b;->d(Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    .line 1275
    :cond_2
    if-eqz p12, :cond_3

    .line 1276
    invoke-virtual/range {p12 .. p12}, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/auth/be/v;->M:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v3, v2, v1}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    .line 1280
    invoke-virtual/range {p12 .. p12}, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v2, v3, Lcom/google/android/gms/auth/be/b;->a:Ljava/util/List;

    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v5, Lcom/google/android/gms/auth/be/v;->N:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v5}, Lcom/google/android/gms/auth/be/v;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1282
    :cond_3
    if-eqz p13, :cond_6

    .line 1283
    invoke-virtual/range {p13 .. p13}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;->f()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual/range {p13 .. p13}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;->d()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1284
    :cond_4
    invoke-virtual/range {p13 .. p13}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;->a()Z

    move-result v4

    invoke-virtual/range {p13 .. p13}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;->b()Ljava/lang/String;

    move-result-object v2

    sget-object v5, Lcom/google/android/gms/auth/be/v;->P:Lcom/google/android/gms/auth/be/v;

    if-eqz v4, :cond_c

    const-string v1, "1"

    :goto_2
    invoke-virtual {v3, v5, v1}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/auth/be/v;->O:Lcom/google/android/gms/auth/be/v;

    if-eqz v4, :cond_d

    const-string v1, ""

    :goto_3
    invoke-virtual {v5, v6, v1}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    .line 1289
    :cond_5
    invoke-virtual/range {p13 .. p13}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;->e()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1290
    invoke-virtual/range {p13 .. p13}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;->c()Z

    move-result v1

    sget-object v2, Lcom/google/android/gms/auth/be/v;->Q:Lcom/google/android/gms/auth/be/v;

    if-eqz v1, :cond_e

    const-string v1, "1"

    :goto_4
    invoke-virtual {v3, v2, v1}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    .line 1295
    :cond_6
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_7

    if-eqz p3, :cond_10

    .line 1296
    :cond_7
    sget-object v1, Lcom/google/android/gms/auth/be/v;->q:Lcom/google/android/gms/auth/be/v;

    sget-object v2, Lcom/google/android/gms/auth/be/v;->r:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v3, v1, v2, p4}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Lcom/google/android/gms/auth/be/v;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    .line 1302
    if-eqz p6, :cond_10

    .line 1303
    sget-object v1, Lcom/google/android/gms/auth/be/v;->v:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/be/v;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/auth/be/v;->v:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v3, v2, v1}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    .line 1310
    const-string v1, "clientPackageName"

    invoke-virtual {p6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1312
    if-eqz v1, :cond_8

    .line 1313
    sget-object v2, Lcom/google/android/gms/auth/be/v;->S:Lcom/google/android/gms/auth/be/v;

    sget-object v4, Lcom/google/android/gms/auth/be/v;->T:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v3, v2, v4, v1}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Lcom/google/android/gms/auth/be/v;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    .line 1315
    :cond_8
    invoke-virtual {p6}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1317
    const-string v4, "_opt_"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 1318
    invoke-virtual {p6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1319
    const-string v5, "_opt_"

    const-string v6, ""

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5, v4}, Lcom/google/android/gms/auth/be/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    .line 1320
    invoke-virtual {v3, v1, v4}, Lcom/google/android/gms/auth/be/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    goto :goto_5

    .line 1232
    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 1253
    :cond_b
    sget-object v4, Lcom/google/android/gms/auth/be/v;->k:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v3, v4, v1}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    move-result-object v1

    sget-object v4, Lcom/google/android/gms/auth/be/v;->j:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v1, v4, v2}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    goto/16 :goto_1

    .line 1284
    :cond_c
    const-string v1, "0"

    goto/16 :goto_2

    :cond_d
    move-object v1, v2

    goto/16 :goto_3

    .line 1290
    :cond_e
    const-string v1, "0"

    goto :goto_4

    .line 1321
    :cond_f
    sget-object v4, Lcom/google/android/gms/auth/be/v;->u:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/be/v;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1323
    invoke-virtual {p6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Lcom/google/android/gms/auth/be/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    goto :goto_5

    .line 1328
    :cond_10
    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->h:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 1330
    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->h:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/auth/be/v;->d:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v3, v2, v1}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    .line 1343
    :goto_6
    sget-object v1, Lcom/google/android/gms/auth/be/v;->R:Lcom/google/android/gms/auth/be/v;

    move-object/from16 v0, p14

    invoke-virtual {v3, v1, v0}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    .line 1345
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v3, Lcom/google/android/gms/auth/be/b;->a:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v1

    .line 1333
    :cond_11
    if-nez p8, :cond_12

    .line 1337
    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/auth/be/v;->b:Lcom/google/android/gms/auth/be/v;

    invoke-virtual {v3, v2, v1}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    goto :goto_6

    .line 1340
    :cond_12
    invoke-static/range {p8 .. p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_13

    sget-object v1, Lcom/google/android/gms/auth/be/v;->w:Lcom/google/android/gms/auth/be/v;

    const/4 v2, 0x1

    invoke-virtual {v3, v1, v2}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Z)Lcom/google/android/gms/auth/be/b;

    :cond_13
    sget-object v1, Lcom/google/android/gms/auth/be/v;->b:Lcom/google/android/gms/auth/be/v;

    move-object/from16 v0, p8

    invoke-virtual {v3, v1, v0}, Lcom/google/android/gms/auth/be/b;->a(Lcom/google/android/gms/auth/be/v;Ljava/lang/String;)Lcom/google/android/gms/auth/be/b;

    goto :goto_6
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZLcom/google/android/gms/auth/firstparty/shared/AppDescription;Landroid/os/Bundle;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;)Ljava/util/Map;
    .locals 20

    .prologue
    .line 564
    new-instance v18, Ljava/util/HashMap;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashMap;-><init>()V

    .line 565
    const/16 v19, 0x0

    .line 567
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/auth/be/i;->c()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 579
    const-string v3, "[GLSUser] No secrets, returning BAD_AUTH for %s %s without consulting the cloud. [mastertoken null? %s, password null? %s]"

    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/i;->h:Ljava/lang/String;

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/auth/be/i;->b:Ljava/lang/String;

    aput-object v5, v4, v2

    const/4 v2, 0x3

    aput-object p1, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 585
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/auth/be/h;->a(Ljava/lang/String;)V

    .line 586
    sget-object v2, Lcom/google/android/gms/auth/firstparty/shared/k;->b:Lcom/google/android/gms/auth/firstparty/shared/k;

    .line 589
    sget-object v3, Lcom/google/android/gms/auth/firstparty/shared/k;->T:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/shared/k;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v2, v18

    .line 685
    :goto_2
    return-object v2

    .line 579
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 594
    :cond_2
    if-nez p8, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    if-eqz v2, :cond_b

    sget-object v2, Lcom/google/android/gms/auth/b/a;->U:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_b

    .line 597
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v3, v2, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    .line 598
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const-string v4, "com.google.android.gms.auth.email_check_time"

    invoke-virtual {v3, v2, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 599
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 600
    if-eqz v2, :cond_3

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long v6, v4, v6

    const-wide/16 v8, 0x3e8

    sget-object v2, Lcom/google/android/gms/auth/b/a;->U:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    mul-long/2addr v8, v10

    cmp-long v2, v6, v8

    if-lez v2, :cond_b

    .line 602
    :cond_3
    const/16 p8, 0x1

    .line 603
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const-string v6, "com.google.android.gms.auth.email_check_time"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v6, v4}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    move/from16 v17, p8

    .line 608
    :goto_3
    invoke-virtual/range {p9 .. p9}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->d()I

    move-result v5

    invoke-virtual/range {p9 .. p9}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move/from16 v7, p4

    move-object/from16 v8, p10

    move/from16 v9, p5

    move-object/from16 v10, p3

    move/from16 v11, p6

    move/from16 v12, p7

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move-object/from16 v15, p13

    move-object/from16 v16, p14

    invoke-virtual/range {v2 .. v17}, Lcom/google/android/gms/auth/be/i;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ZLandroid/os/Bundle;ZLjava/lang/String;ZZLcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v8

    .line 624
    :try_start_0
    new-instance v4, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    invoke-direct {v4, v8}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 636
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-virtual/range {p9 .. p9}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Lcom/google/android/gms/auth/be/h;->a(Ljava/util/Map;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v7

    .line 639
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    sget-object v3, Lcom/google/android/gms/auth/b/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/auth/be/i;->b:Ljava/lang/String;

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, ":"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/auth/be/h;->a(Ljava/lang/String;Lorg/apache/http/HttpEntity;Lorg/apache/http/Header;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    .line 646
    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v3

    .line 648
    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 649
    const v4, 0x320d2

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/android/gms/auth/a/b;->a(ILjava/lang/String;)V

    .line 650
    invoke-static {v3}, Lcom/google/android/gms/auth/be/i;->f(Ljava/lang/String;)Ljava/util/Map;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 651
    :try_start_2
    const-string v4, "x-status"

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 653
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v3, v4, v2}, Lcom/google/android/gms/auth/be/i;->a(Ljava/util/List;Ljava/util/Map;Ljava/lang/String;I)V

    .line 655
    sget-object v2, Lcom/google/android/gms/auth/be/w;->c:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 656
    if-eqz v2, :cond_a

    .line 657
    invoke-static {v2}, Lcom/google/android/gms/auth/a/h;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/k;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    .line 660
    :goto_4
    if-nez p6, :cond_4

    :try_start_3
    sget-object v2, Lcom/google/android/gms/auth/be/w;->b:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v2, Lcom/google/android/gms/auth/be/w;->b:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 663
    const-string v5, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "[GLSUser]  email changed from "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " to "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v2, Lcom/google/android/gms/auth/be/w;->b:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 665
    sget-object v2, Lcom/google/android/gms/auth/be/w;->b:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v5, v5, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    const-string v5, "updateAccountName %s -> %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v9, v9, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v9, v6, v7

    const/4 v7, 0x1

    aput-object v2, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Landroid/accounts/Account;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v7, v7, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-direct {v6, v2, v7}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    new-instance v7, Lcom/google/android/gms/backup/b;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v9, v9, Lcom/google/android/gms/auth/be/h;->a:Landroid/content/Context;

    invoke-direct {v7, v9}, Lcom/google/android/gms/backup/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7}, Lcom/google/android/gms/backup/b;->a()Landroid/accounts/Account;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/16 v7, 0x15

    invoke-static {v7}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v7

    if-eqz v7, :cond_6

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v2, v5}, Lcom/google/android/gms/auth/be/i;->a(Landroid/accounts/Account;ZLjava/lang/String;)V

    .line 667
    :cond_4
    :goto_5
    sget-object v2, Lcom/google/android/gms/auth/be/w;->k:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 669
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v5, v2, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const-string v7, "GoogleUserId"

    sget-object v2, Lcom/google/android/gms/auth/be/w;->k:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v5, v6, v7, v2}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 681
    :cond_5
    const v5, 0x320d2

    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/Object;

    const/4 v7, 0x0

    if-nez v4, :cond_7

    const/4 v2, 0x0

    :goto_6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v7

    const/4 v2, 0x1

    aput-object p1, v6, v2

    invoke-static {v5, v6}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    .line 685
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "[GLSUser]  getAuthtoken("

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ") -> status: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gms/auth/be/i;->c(Ljava/lang/String;)V

    move-object v2, v3

    goto/16 :goto_2

    .line 628
    :catch_0
    move-exception v2

    .line 629
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 665
    :cond_6
    :try_start_4
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v2, v5}, Lcom/google/android/gms/auth/be/i;->b(Landroid/accounts/Account;ZLjava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_5

    .line 673
    :catch_1
    move-exception v2

    .line 674
    :goto_7
    :try_start_5
    sget-object v4, Lcom/google/android/gms/auth/firstparty/shared/k;->m:Lcom/google/android/gms/auth/firstparty/shared/k;

    .line 675
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[GLSUser]  IOException in getAuthtoken("

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " -> "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/gms/auth/be/i;->c(Ljava/lang/String;)V

    .line 677
    sget-object v5, Lcom/google/android/gms/auth/be/w;->c:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v5}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/auth/firstparty/shared/k;->m:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v6}, Lcom/google/android/gms/auth/firstparty/shared/k;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 678
    const/4 v5, 0x0

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v5, v2, v6}, Lcom/google/android/gms/auth/be/i;->a(Ljava/util/List;Ljava/util/Map;Ljava/lang/String;I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 681
    const v5, 0x320d2

    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/Object;

    const/4 v7, 0x0

    if-nez v4, :cond_8

    const/4 v2, 0x0

    :goto_8
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v7

    const/4 v2, 0x1

    aput-object p1, v6, v2

    invoke-static {v5, v6}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    .line 685
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "[GLSUser]  getAuthtoken("

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ") -> status: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gms/auth/be/i;->c(Ljava/lang/String;)V

    move-object v2, v3

    goto/16 :goto_2

    .line 681
    :cond_7
    sget-object v2, Lcom/google/android/gms/auth/firstparty/shared/k;->m:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/shared/k;->ordinal()I

    move-result v2

    goto/16 :goto_6

    :cond_8
    sget-object v2, Lcom/google/android/gms/auth/firstparty/shared/k;->m:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/shared/k;->ordinal()I

    move-result v2

    goto :goto_8

    :catchall_0
    move-exception v2

    move-object v3, v2

    move-object/from16 v4, v19

    :goto_9
    const v5, 0x320d2

    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/Object;

    const/4 v7, 0x0

    if-nez v4, :cond_9

    const/4 v2, 0x0

    :goto_a
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v7

    const/4 v2, 0x1

    aput-object p1, v6, v2

    invoke-static {v5, v6}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    .line 685
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "[GLSUser]  getAuthtoken("

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ") -> status: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gms/auth/be/i;->c(Ljava/lang/String;)V

    throw v3

    .line 681
    :cond_9
    sget-object v2, Lcom/google/android/gms/auth/firstparty/shared/k;->m:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/shared/k;->ordinal()I

    move-result v2

    goto :goto_a

    :catchall_1
    move-exception v2

    move-object v3, v2

    goto :goto_9

    .line 673
    :catch_2
    move-exception v2

    move-object/from16 v4, v19

    move-object/from16 v3, v18

    goto/16 :goto_7

    :catch_3
    move-exception v2

    move-object/from16 v4, v19

    goto/16 :goto_7

    :cond_a
    move-object/from16 v4, v19

    goto/16 :goto_4

    :cond_b
    move/from16 v17, p8

    goto/16 :goto_3
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 373
    if-nez p1, :cond_2

    move v0, v1

    .line 374
    :goto_0
    const-string v3, ""

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 375
    if-nez v0, :cond_0

    if-eqz v3, :cond_1

    .line 376
    :cond_0
    const-string v4, "Setting password with invalid value! isNull? %s, isEmpty? %s."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v5, v2

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 380
    const-string v1, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[GLSUser] "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/be/h;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->b:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/auth/a/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/be/i;->h:Ljava/lang/String;

    .line 384
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->b:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/gms/auth/a/e;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/be/i;->l:Ljava/lang/String;

    .line 385
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/i;->k:Ljava/lang/String;

    .line 386
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    .line 387
    return-void

    :cond_2
    move v0, v2

    .line 373
    goto :goto_0
.end method

.method final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 484
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 485
    const-string v0, "flags"

    const-string v2, "1"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    .line 487
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/gms/auth/be/i;->f:Z

    if-eqz v2, :cond_0

    .line 488
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x8

    if-gt v2, v3, :cond_5

    .line 489
    const-string v2, "oauthAccessToken"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/auth/be/i;->l:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 495
    const-string v2, "sha1hash"

    iget-object v3, p0, Lcom/google/android/gms/auth/be/i;->l:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 499
    const-string v2, "GoogleUserId"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    :cond_2
    if-eqz p1, :cond_3

    .line 503
    sget-object v2, Lcom/google/android/gms/auth/be/w;->i:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v2, v2, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v3, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    invoke-virtual {v2, v3, v0, v1}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 507
    iput-object v4, p0, Lcom/google/android/gms/auth/be/i;->k:Ljava/lang/String;

    .line 509
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->e:Lcom/google/android/gms/auth/a/c;

    iget-object v0, v0, Lcom/google/android/gms/auth/a/c;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/auth/be/q;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/q;

    move-result-object v1

    .line 511
    invoke-virtual {v1}, Lcom/google/android/gms/auth/be/q;->a()Ljava/lang/String;

    move-result-object v2

    .line 513
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v3, v4}, Lcom/google/android/gms/auth/be/q;->a(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 516
    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/be/q;->a(Ljava/lang/String;)V

    .line 517
    return-void

    .line 486
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->h:Ljava/lang/String;

    goto :goto_0

    .line 491
    :cond_5
    const-string v2, "oauthAccessToken"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 516
    :catchall_0
    move-exception v0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/be/q;->a(Ljava/lang/String;)V

    throw v0
.end method

.method protected final a(Ljava/util/Map;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1829
    sget-object v0, Lcom/google/android/gms/auth/be/w;->s:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v0

    .line 1830
    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1860
    :cond_0
    :goto_0
    return-void

    .line 1834
    :cond_1
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1835
    if-nez v0, :cond_3

    const/4 v1, 0x1

    .line 1836
    :goto_1
    const-string v2, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 1841
    if-eqz v1, :cond_4

    .line 1842
    const-string v1, "GLSUser"

    const-string v2, "[GLSUser]  Received null token!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1846
    :cond_2
    :goto_2
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1847
    iput-object v0, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    .line 1848
    iput-object v3, p0, Lcom/google/android/gms/auth/be/i;->h:Ljava/lang/String;

    .line 1849
    iput-object v3, p0, Lcom/google/android/gms/auth/be/i;->l:Ljava/lang/String;

    .line 1850
    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    .line 1851
    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v1, v1, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    invoke-virtual {v1, v2, v0}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 1852
    iget-boolean v0, p0, Lcom/google/android/gms/auth/be/i;->f:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-gt v0, v1, :cond_0

    .line 1854
    iget-object v0, p0, Lcom/google/android/gms/auth/be/i;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/be/i;->c:Landroid/accounts/Account;

    const-string v2, "oauthAccessToken"

    iget-object v3, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1835
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 1843
    :cond_4
    if-eqz v2, :cond_2

    .line 1844
    const-string v1, "GLSUser"

    const-string v2, "[GLSUser]  Received empty token!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 391
    iput-object p1, p0, Lcom/google/android/gms/auth/be/i;->g:Ljava/lang/String;

    .line 392
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/be/i;->h:Ljava/lang/String;

    .line 393
    iput-object p1, p0, Lcom/google/android/gms/auth/be/i;->k:Ljava/lang/String;

    .line 394
    return-void
.end method
