.class public Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/ae;


# instance fields
.field final a:I

.field final b:[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/appdatasearch/ae;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/ae;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/ae;

    return-void
.end method

.method constructor <init>(I[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput p1, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->a:I

    .line 41
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->b:[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    .line 42
    return-void
.end method


# virtual methods
.method public final a()[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->b:[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    invoke-virtual {v0}, [Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/ae;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/ae;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/ae;->a(Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;Landroid/os/Parcel;I)V

    .line 69
    return-void
.end method
