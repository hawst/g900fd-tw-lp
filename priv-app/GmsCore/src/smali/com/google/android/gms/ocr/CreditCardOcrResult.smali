.class public final Lcom/google/android/gms/ocr/CreditCardOcrResult;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field a:Ljava/lang/String;

.field b:I

.field c:I

.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/ocr/b;

    invoke-direct {v0}, Lcom/google/android/gms/ocr/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/ocr/CreditCardOcrResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;II)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput p1, p0, Lcom/google/android/gms/ocr/CreditCardOcrResult;->d:I

    .line 56
    iput-object p2, p0, Lcom/google/android/gms/ocr/CreditCardOcrResult;->a:Ljava/lang/String;

    .line 57
    iput p3, p0, Lcom/google/android/gms/ocr/CreditCardOcrResult;->b:I

    .line 58
    iput p4, p0, Lcom/google/android/gms/ocr/CreditCardOcrResult;->c:I

    .line 59
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/gms/ocr/CreditCardOcrResult;-><init>(ILjava/lang/String;II)V

    .line 79
    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/google/android/gms/ocr/CreditCardOcrResult;
    .locals 1

    .prologue
    .line 67
    if-eqz p0, :cond_0

    const-string v0, "com.google.android.gms.ocr.CREDIT_CARD_OCR_RESULT"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 68
    :cond_0
    const/4 v0, 0x0

    .line 71
    :goto_0
    return-object v0

    .line 70
    :cond_1
    const-class v0, Lcom/google/android/gms/ocr/CreditCardOcrResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    .line 71
    const-string v0, "com.google.android.gms.ocr.CREDIT_CARD_OCR_RESULT"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ocr/CreditCardOcrResult;

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/android/gms/ocr/CreditCardOcrResult;->d:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/gms/ocr/CreditCardOcrResult;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/google/android/gms/ocr/CreditCardOcrResult;->b:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/google/android/gms/ocr/CreditCardOcrResult;->c:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/google/android/gms/ocr/CreditCardOcrResult;->c:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/ocr/CreditCardOcrResult;->b:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 37
    invoke-static {p0, p1}, Lcom/google/android/gms/ocr/b;->a(Lcom/google/android/gms/ocr/CreditCardOcrResult;Landroid/os/Parcel;)V

    .line 38
    return-void
.end method
