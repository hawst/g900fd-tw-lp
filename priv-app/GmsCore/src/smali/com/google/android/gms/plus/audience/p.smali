.class final Lcom/google/android/gms/plus/audience/p;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/Object;

.field final synthetic c:Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;

.field final synthetic d:Z

.field final synthetic e:Lcom/google/android/gms/plus/audience/o;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/audience/o;Ljava/lang/String;Ljava/lang/Object;Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;Z)V
    .locals 0

    .prologue
    .line 853
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/p;->e:Lcom/google/android/gms/plus/audience/o;

    iput-object p2, p0, Lcom/google/android/gms/plus/audience/p;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/plus/audience/p;->b:Ljava/lang/Object;

    iput-object p4, p0, Lcom/google/android/gms/plus/audience/p;->c:Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;

    iput-boolean p5, p0, Lcom/google/android/gms/plus/audience/p;->d:Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 857
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/p;->e:Lcom/google/android/gms/plus/audience/o;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/o;->a(Lcom/google/android/gms/plus/audience/o;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/p;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 860
    if-eqz v0, :cond_0

    .line 861
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 868
    :goto_0
    return-object v0

    .line 863
    :catch_0
    move-exception v0

    .line 866
    const-string v1, "AudienceSelectionAdapter"

    const-string v2, "Exception opening ContactsDB avatar"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 868
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 853
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/p;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 853
    check-cast p1, Landroid/graphics/Bitmap;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/p;->e:Lcom/google/android/gms/plus/audience/o;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/o;->b(Lcom/google/android/gms/plus/audience/o;)Lcom/google/android/gms/common/internal/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/internal/a/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/p;->b:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/p;->c:Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->g()Ljava/lang/Object;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/p;->e:Lcom/google/android/gms/plus/audience/o;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/p;->c:Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/p;->d:Z

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/plus/audience/o;->a(Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;Landroid/graphics/Bitmap;Z)V

    :cond_0
    return-void
.end method
