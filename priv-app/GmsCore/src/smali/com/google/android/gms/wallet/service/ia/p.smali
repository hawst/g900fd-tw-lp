.class final Lcom/google/android/gms/wallet/service/ia/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/util/w;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/google/android/gms/wallet/a/b;

.field final synthetic d:Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Lcom/google/android/gms/wallet/service/ia/o;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/service/ia/o;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/a/b;Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ia/p;->g:Lcom/google/android/gms/wallet/service/ia/o;

    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ia/p;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/wallet/service/ia/p;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/wallet/service/ia/p;->c:Lcom/google/android/gms/wallet/a/b;

    iput-object p5, p0, Lcom/google/android/gms/wallet/service/ia/p;->d:Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;

    iput-object p6, p0, Lcom/google/android/gms/wallet/service/ia/p;->e:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/wallet/service/ia/p;->f:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ia/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/inapp/api/v1/create_address"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/p;->g:Lcom/google/android/gms/wallet/service/ia/o;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ia/p;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/wallet/service/ia/p;->c:Lcom/google/android/gms/wallet/a/b;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/ia/p;->d:Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;->b()Lcom/google/checkout/inapp/proto/z;

    move-result-object v4

    const/16 v5, 0xb

    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ia/p;->e:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/wallet/service/ia/p;->f:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/wallet/service/ia/o;->a(Lcom/google/android/gms/wallet/service/ia/o;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/a/b;Lcom/google/protobuf/nano/j;ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method
