.class final Lcom/google/android/gms/wallet/common/ui/cy;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field private final a:I

.field private b:I

.field private final c:I

.field private final d:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;II[Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 119
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    .line 120
    iput p2, p0, Lcom/google/android/gms/wallet/common/ui/cy;->a:I

    .line 121
    iput p3, p0, Lcom/google/android/gms/wallet/common/ui/cy;->c:I

    .line 122
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cy;->d:Landroid/view/LayoutInflater;

    .line 123
    return-void
.end method

.method private a(ILandroid/view/View;Landroid/view/ViewGroup;IZ)Landroid/view/View;
    .locals 2

    .prologue
    .line 143
    if-nez p2, :cond_0

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cy;->d:Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    invoke-virtual {v0, p4, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 146
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/cy;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 147
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 148
    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/cy;->c:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;

    .line 150
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0, p5}, Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;->a(IZ)V

    .line 152
    return-object p2
.end method


# virtual methods
.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 133
    iget v4, p0, Lcom/google/android/gms/wallet/common/ui/cy;->b:I

    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/common/ui/cy;->a(ILandroid/view/View;Landroid/view/ViewGroup;IZ)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 138
    iget v4, p0, Lcom/google/android/gms/wallet/common/ui/cy;->a:I

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/common/ui/cy;->a(ILandroid/view/View;Landroid/view/ViewGroup;IZ)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final setDropDownViewResource(I)V
    .locals 0

    .prologue
    .line 127
    invoke-super {p0, p1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 128
    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/cy;->b:I

    .line 129
    return-void
.end method
