.class public final Lcom/google/android/gms/udc/e/u;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/google/android/gms/udc/e/v;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 181
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 182
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/udc/e/u;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/u;->b:Lcom/google/android/gms/udc/e/v;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/udc/e/u;->c:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/udc/e/u;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/udc/e/u;->e:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/u;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/udc/e/u;->cachedSize:I

    .line 183
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 280
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 281
    iget-object v1, p0, Lcom/google/android/gms/udc/e/u;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 282
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/udc/e/u;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 285
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/udc/e/u;->b:Lcom/google/android/gms/udc/e/v;

    if-eqz v1, :cond_1

    .line 286
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/udc/e/u;->b:Lcom/google/android/gms/udc/e/v;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 289
    :cond_1
    iget v1, p0, Lcom/google/android/gms/udc/e/u;->c:I

    if-eqz v1, :cond_2

    .line 290
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/udc/e/u;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 293
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/udc/e/u;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 294
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/udc/e/u;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 297
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/udc/e/u;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 298
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/udc/e/u;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 301
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 198
    if-ne p1, p0, :cond_1

    .line 199
    const/4 v0, 0x1

    .line 238
    :cond_0
    :goto_0
    return v0

    .line 201
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/udc/e/u;

    if-eqz v1, :cond_0

    .line 204
    check-cast p1, Lcom/google/android/gms/udc/e/u;

    .line 205
    iget-object v1, p0, Lcom/google/android/gms/udc/e/u;->a:Ljava/lang/String;

    if-nez v1, :cond_6

    .line 206
    iget-object v1, p1, Lcom/google/android/gms/udc/e/u;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 212
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/udc/e/u;->b:Lcom/google/android/gms/udc/e/v;

    if-nez v1, :cond_7

    .line 213
    iget-object v1, p1, Lcom/google/android/gms/udc/e/u;->b:Lcom/google/android/gms/udc/e/v;

    if-nez v1, :cond_0

    .line 221
    :cond_3
    iget v1, p0, Lcom/google/android/gms/udc/e/u;->c:I

    iget v2, p1, Lcom/google/android/gms/udc/e/u;->c:I

    if-ne v1, v2, :cond_0

    .line 224
    iget-object v1, p0, Lcom/google/android/gms/udc/e/u;->d:Ljava/lang/String;

    if-nez v1, :cond_8

    .line 225
    iget-object v1, p1, Lcom/google/android/gms/udc/e/u;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 231
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/udc/e/u;->e:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 232
    iget-object v1, p1, Lcom/google/android/gms/udc/e/u;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 238
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/android/gms/udc/e/u;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 209
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/udc/e/u;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/u;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 217
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/udc/e/u;->b:Lcom/google/android/gms/udc/e/v;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/u;->b:Lcom/google/android/gms/udc/e/v;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/v;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 228
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/udc/e/u;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/u;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 235
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/udc/e/u;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/u;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 243
    iget-object v0, p0, Lcom/google/android/gms/udc/e/u;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 246
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/u;->b:Lcom/google/android/gms/udc/e/v;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 248
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/udc/e/u;->c:I

    add-int/2addr v0, v2

    .line 249
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/u;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 251
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/udc/e/u;->e:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 253
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/udc/e/u;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 254
    return v0

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/u;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 246
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/u;->b:Lcom/google/android/gms/udc/e/v;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/v;->hashCode()I

    move-result v0

    goto :goto_1

    .line 249
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/u;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 251
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/udc/e/u;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/udc/e/u;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/e/u;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/u;->b:Lcom/google/android/gms/udc/e/v;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/udc/e/v;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/v;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/u;->b:Lcom/google/android/gms/udc/e/v;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/u;->b:Lcom/google/android/gms/udc/e/v;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/udc/e/u;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/e/u;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/e/u;->e:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/gms/udc/e/u;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 261
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/udc/e/u;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/u;->b:Lcom/google/android/gms/udc/e/v;

    if-eqz v0, :cond_1

    .line 264
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/udc/e/u;->b:Lcom/google/android/gms/udc/e/v;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 266
    :cond_1
    iget v0, p0, Lcom/google/android/gms/udc/e/u;->c:I

    if-eqz v0, :cond_2

    .line 267
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/udc/e/u;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 269
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/u;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 270
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/udc/e/u;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 272
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/udc/e/u;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 273
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/udc/e/u;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 275
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 276
    return-void
.end method
