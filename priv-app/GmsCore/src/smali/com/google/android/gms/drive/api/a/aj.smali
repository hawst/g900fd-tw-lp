.class public final Lcom/google/android/gms/drive/api/a/aj;
.super Lcom/google/android/gms/drive/api/a/b;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/internal/SetResourceParentsRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/SetResourceParentsRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 1

    .prologue
    .line 25
    const/16 v0, 0x14

    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;I)V

    .line 26
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/aj;->e:Lcom/google/android/gms/drive/internal/SetResourceParentsRequest;

    .line 27
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
    .locals 4

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/aj;->e:Lcom/google/android/gms/drive/internal/SetResourceParentsRequest;

    const-string v1, "Invalid set parents request."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/aj;->e:Lcom/google/android/gms/drive/internal/SetResourceParentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/SetResourceParentsRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    const-string v1, "Invalid set parents request: no target id provided."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/aj;->e:Lcom/google/android/gms/drive/internal/SetResourceParentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/SetResourceParentsRequest;->b()Ljava/util/List;

    move-result-object v0

    const-string v1, "Invalid set parents request: no parent id list provided."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/aj;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/aj;->e:Lcom/google/android/gms/drive/internal/SetResourceParentsRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/SetResourceParentsRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/aj;->e:Lcom/google/android/gms/drive/internal/SetResourceParentsRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/SetResourceParentsRequest;->b()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/drive/api/a/aj;->c:Lcom/google/android/gms/drive/c/a;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/drive/api/d;->a(Lcom/google/android/gms/drive/DriveId;Ljava/util/List;Lcom/google/android/gms/drive/c/a;)V

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/aj;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-interface {v0}, Lcom/google/android/gms/drive/internal/ca;->a()V

    .line 40
    return-void
.end method
