.class public final Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;
.super Lcom/google/android/gms/common/server/response/FastJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/lso/d;

.field private static final a:Ljava/util/HashMap;


# instance fields
.field private final b:Ljava/util/Set;

.field private final c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 652
    new-instance v0, Lcom/google/android/gms/plus/service/lso/d;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/lso/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->CREATOR:Lcom/google/android/gms/plus/service/lso/d;

    .line 669
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 672
    sput-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->a:Ljava/util/HashMap;

    const-string v1, "android_package_name"

    const-string v2, "android_package_name"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 673
    sget-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->a:Ljava/util/HashMap;

    const-string v1, "certificate"

    const-string v2, "certificate"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 674
    sget-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->a:Ljava/util/HashMap;

    const-string v1, "client_id"

    const-string v2, "client_id"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 675
    sget-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->a:Ljava/util/HashMap;

    const-string v1, "ios_app_store_id"

    const-string v2, "ios_app_store_id"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 676
    sget-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->a:Ljava/util/HashMap;

    const-string v1, "ios_bundle_name"

    const-string v2, "ios_bundle_name"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 677
    sget-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->a:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 678
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 715
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 716
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->c:I

    .line 717
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->b:Ljava/util/Set;

    .line 718
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 730
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 731
    iput-object p1, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->b:Ljava/util/Set;

    .line 732
    iput p2, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->c:I

    .line 733
    iput-object p3, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->d:Ljava/lang/String;

    .line 734
    iput-object p4, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->e:Ljava/lang/String;

    .line 735
    iput-object p5, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->f:Ljava/lang/String;

    .line 736
    iput-object p6, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->g:Ljava/lang/String;

    .line 737
    iput-object p7, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->h:Ljava/lang/String;

    .line 738
    iput-object p8, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->i:Ljava/lang/String;

    .line 739
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 682
    sget-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->a:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 920
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 921
    packed-switch v0, :pswitch_data_0

    .line 941
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 923
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->d:Ljava/lang/String;

    .line 944
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->b:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 945
    return-void

    .line 926
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->e:Ljava/lang/String;

    goto :goto_0

    .line 929
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->f:Ljava/lang/String;

    goto :goto_0

    .line 932
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->g:Ljava/lang/String;

    goto :goto_0

    .line 935
    :pswitch_4
    iput-object p3, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->h:Ljava/lang/String;

    goto :goto_0

    .line 938
    :pswitch_5
    iput-object p3, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->i:Ljava/lang/String;

    goto :goto_0

    .line 921
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 870
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->b:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 875
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 889
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 877
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->d:Ljava/lang/String;

    .line 887
    :goto_0
    return-object v0

    .line 879
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->e:Ljava/lang/String;

    goto :goto_0

    .line 881
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->f:Ljava/lang/String;

    goto :goto_0

    .line 883
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->g:Ljava/lang/String;

    goto :goto_0

    .line 885
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->h:Ljava/lang/String;

    goto :goto_0

    .line 887
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->i:Ljava/lang/String;

    goto :goto_0

    .line 875
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected final b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 860
    const/4 v0, 0x0

    return-object v0
.end method

.method final b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 765
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->b:Ljava/util/Set;

    return-object v0
.end method

.method final c()I
    .locals 1

    .prologue
    .line 772
    iget v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->c:I

    return v0
.end method

.method protected final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 865
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 777
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 849
    sget-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->CREATOR:Lcom/google/android/gms/plus/service/lso/d;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 784
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->b:Ljava/util/Set;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 962
    instance-of v0, p1, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;

    if-nez v0, :cond_0

    move v0, v1

    .line 993
    :goto_0
    return v0

    .line 967
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 968
    goto :goto_0

    .line 971
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;

    .line 972
    sget-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 973
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 974
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 976
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 978
    goto :goto_0

    :cond_3
    move v0, v1

    .line 983
    goto :goto_0

    .line 986
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 988
    goto :goto_0

    :cond_5
    move v0, v2

    .line 993
    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 789
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 801
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 813
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 949
    const/4 v0, 0x0

    .line 950
    sget-object v1, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->a:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 951
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 952
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 953
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 955
    goto :goto_0

    .line 956
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 825
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 837
    iget-object v0, p0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 854
    sget-object v0, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->CREATOR:Lcom/google/android/gms/plus/service/lso/d;

    invoke-static {p0, p1}, Lcom/google/android/gms/plus/service/lso/d;->a(Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;Landroid/os/Parcel;)V

    .line 855
    return-void
.end method
