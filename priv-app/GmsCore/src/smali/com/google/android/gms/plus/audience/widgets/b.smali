.class final Lcom/google/android/gms/plus/audience/widgets/b;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

.field private final b:Ljava/lang/String;

.field private final c:Landroid/widget/ImageView;

.field private final d:Landroid/os/ParcelFileDescriptor;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;Landroid/os/ParcelFileDescriptor;Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 398
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/widgets/b;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 399
    iput-object p2, p0, Lcom/google/android/gms/plus/audience/widgets/b;->d:Landroid/os/ParcelFileDescriptor;

    .line 400
    iput-object p3, p0, Lcom/google/android/gms/plus/audience/widgets/b;->b:Ljava/lang/String;

    .line 401
    iput-object p4, p0, Lcom/google/android/gms/plus/audience/widgets/b;->c:Landroid/widget/ImageView;

    .line 402
    return-void
.end method

.method private varargs a()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 407
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/b;->d:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, Lcom/google/android/gms/people/af;->a(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 408
    if-nez v0, :cond_0

    .line 409
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/b;->d:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 419
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 413
    :cond_0
    :try_start_2
    invoke-static {v0}, Lcom/google/android/gms/common/util/ac;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 416
    :try_start_3
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/widgets/b;->d:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_1

    .line 415
    :catchall_0
    move-exception v0

    .line 416
    :try_start_4
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/widgets/b;->d:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 419
    :goto_2
    throw v0

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 393
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/b;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 393
    check-cast p1, Landroid/graphics/Bitmap;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/b;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a(Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;)Lcom/google/android/gms/common/internal/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/widgets/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/internal/a/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/b;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/b;->c:Landroid/widget/ImageView;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V

    :cond_0
    return-void
.end method
