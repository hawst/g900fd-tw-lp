.class final Lcom/google/android/gms/appstate/c/c;
.super Lcom/google/android/gms/appstate/c/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 1

    .prologue
    .line 207
    invoke-direct {p0}, Lcom/google/android/gms/appstate/c/a;-><init>()V

    .line 208
    const-string v0, "Holder must not be null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/m;

    iput-object v0, p0, Lcom/google/android/gms/appstate/c/c;->a:Lcom/google/android/gms/common/api/m;

    .line 209
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 213
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    .line 214
    iget-object v1, p0, Lcom/google/android/gms/appstate/c/c;->a:Lcom/google/android/gms/common/api/m;

    invoke-interface {v1, v0}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 215
    return-void
.end method
