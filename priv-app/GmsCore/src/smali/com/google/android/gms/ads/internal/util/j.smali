.class public final Lcom/google/android/gms/ads/internal/util/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Future;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Ljava/lang/Object;

.field private b:Ljava/lang/Object;

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/util/j;->a:Ljava/lang/Object;

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/util/j;->b:Ljava/lang/Object;

    .line 38
    iput-boolean v1, p0, Lcom/google/android/gms/ads/internal/util/j;->c:Z

    .line 39
    iput-boolean v1, p0, Lcom/google/android/gms/ads/internal/util/j;->d:Z

    .line 40
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 47
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/util/j;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 48
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/util/j;->c:Z

    if-eqz v0, :cond_0

    .line 49
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Provided CallbackFuture with multiple values."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 52
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/util/j;->c:Z

    .line 53
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/util/j;->b:Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/util/j;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 55
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final cancel(Z)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 104
    if-nez p1, :cond_0

    .line 114
    :goto_0
    return v0

    .line 107
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/util/j;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 108
    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/gms/ads/internal/util/j;->c:Z

    if-eqz v3, :cond_1

    .line 109
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 111
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/util/j;->d:Z

    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/util/j;->c:Z

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/util/j;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 114
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    goto :goto_0
.end method

.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 60
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/util/j;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 61
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/util/j;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 63
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/util/j;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 68
    :cond_0
    :goto_0
    :try_start_2
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/util/j;->d:Z

    if-eqz v0, :cond_1

    .line 69
    new-instance v0, Ljava/util/concurrent/CancellationException;

    const-string v2, "CallbackFuture was cancelled."

    invoke-direct {v0, v2}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 71
    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/util/j;->b:Ljava/lang/Object;

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-object v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 77
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/util/j;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 78
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/util/j;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 83
    :try_start_1
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 84
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/util/j;->a:Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    :cond_0
    :goto_0
    :try_start_2
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/util/j;->c:Z

    if-nez v0, :cond_1

    .line 92
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v2, "CallbackFuture timed out."

    invoke-direct {v0, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 94
    :cond_1
    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/util/j;->d:Z

    if-eqz v0, :cond_2

    .line 95
    new-instance v0, Ljava/util/concurrent/CancellationException;

    const-string v2, "CallbackFuture was cancelled."

    invoke-direct {v0, v2}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/util/j;->b:Ljava/lang/Object;

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-object v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final isCancelled()Z
    .locals 2

    .prologue
    .line 120
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/util/j;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 121
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/util/j;->d:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final isDone()Z
    .locals 2

    .prologue
    .line 127
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/util/j;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 128
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/util/j;->c:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
