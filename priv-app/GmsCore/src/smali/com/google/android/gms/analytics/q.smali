.class public final enum Lcom/google/android/gms/analytics/q;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/analytics/q;

.field public static final enum b:Lcom/google/android/gms/analytics/q;

.field private static final synthetic c:[Lcom/google/android/gms/analytics/q;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, Lcom/google/android/gms/analytics/q;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/analytics/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/analytics/q;->a:Lcom/google/android/gms/analytics/q;

    new-instance v0, Lcom/google/android/gms/analytics/q;

    const-string v1, "GZIP"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/analytics/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/analytics/q;->b:Lcom/google/android/gms/analytics/q;

    .line 8
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/analytics/q;

    sget-object v1, Lcom/google/android/gms/analytics/q;->a:Lcom/google/android/gms/analytics/q;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/analytics/q;->b:Lcom/google/android/gms/analytics/q;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/gms/analytics/q;->c:[Lcom/google/android/gms/analytics/q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/analytics/q;
    .locals 1

    .prologue
    .line 12
    const-string v0, "GZIP"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13
    sget-object v0, Lcom/google/android/gms/analytics/q;->b:Lcom/google/android/gms/analytics/q;

    .line 15
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/analytics/q;->a:Lcom/google/android/gms/analytics/q;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/analytics/q;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/google/android/gms/analytics/q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/analytics/q;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/analytics/q;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/google/android/gms/analytics/q;->c:[Lcom/google/android/gms/analytics/q;

    invoke-virtual {v0}, [Lcom/google/android/gms/analytics/q;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/analytics/q;

    return-object v0
.end method
