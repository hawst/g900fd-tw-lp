.class final Lcom/google/android/gms/common/images/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/images/h;


# instance fields
.field private final a:Landroid/net/Uri;

.field private final b:Landroid/os/ResultReceiver;

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/net/Uri;Landroid/os/ResultReceiver;I)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/google/android/gms/common/images/i;->a:Landroid/net/Uri;

    .line 71
    iput-object p2, p0, Lcom/google/android/gms/common/images/i;->b:Landroid/os/ResultReceiver;

    .line 72
    iput p3, p0, Lcom/google/android/gms/common/images/i;->c:I

    .line 73
    return-void
.end method

.method private a(Landroid/os/ParcelFileDescriptor;)V
    .locals 3

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/gms/common/images/i;->b:Landroid/os/ResultReceiver;

    if-nez v0, :cond_0

    .line 103
    :goto_0
    return-void

    .line 100
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 101
    const-string v1, "com.google.android.gms.extra.fileDescriptor"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 102
    iget-object v1, p0, Lcom/google/android/gms/common/images/i;->b:Landroid/os/ResultReceiver;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/images/a;)V
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/gms/common/images/i;->a:Landroid/net/Uri;

    invoke-virtual {p2, p1, v0}, Lcom/google/android/gms/common/images/a;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    .line 79
    if-nez v0, :cond_1

    .line 80
    const-string v1, "ImageMultiThreadedInten"

    const-string v2, "Failed LoadImageOperation"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/gms/common/images/i;->a(Landroid/os/ParcelFileDescriptor;)V

    .line 86
    :goto_0
    if-eqz v0, :cond_0

    .line 88
    :try_start_0
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :cond_0
    :goto_1
    return-void

    .line 83
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/common/images/i;->a(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    const-string v1, "ImageMultiThreadedInten"

    const-string v2, "Failed to close file"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
