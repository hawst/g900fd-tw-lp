.class public final Lcom/google/android/gms/wallet/dynamite/fragment/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:I

.field public final k:I

.field public final l:I

.field public final m:I


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->a:I

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->b:I

    .line 47
    iput v2, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->c:I

    .line 48
    iput v2, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->d:I

    .line 49
    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->e:I

    .line 50
    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->f:I

    .line 51
    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->g:I

    .line 52
    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->h:I

    .line 53
    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->i:I

    .line 54
    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->j:I

    .line 55
    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->k:I

    .line 56
    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->l:I

    .line 57
    iput v2, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->m:I

    .line 58
    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/wallet/fragment/WalletFragmentStyle;Landroid/util/DisplayMetrics;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/fragment/WalletFragmentStyle;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 62
    const-string v1, "buyButtonHeight"

    const/4 v2, -0x2

    invoke-virtual {p1, v1, p2, v2}, Lcom/google/android/gms/wallet/fragment/WalletFragmentStyle;->a(Ljava/lang/String;Landroid/util/DisplayMetrics;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->a:I

    .line 64
    const-string v1, "buyButtonWidth"

    const/4 v2, -0x1

    invoke-virtual {p1, v1, p2, v2}, Lcom/google/android/gms/wallet/fragment/WalletFragmentStyle;->a(Ljava/lang/String;Landroid/util/DisplayMetrics;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->b:I

    .line 66
    const-string v1, "buyButtonText"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->c:I

    .line 68
    const-string v1, "buyButtonAppearance"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->d:I

    .line 71
    const-string v1, "maskedWalletDetailsTextAppearance"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->e:I

    .line 74
    const-string v1, "maskedWalletDetailsHeaderTextAppearance"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->f:I

    .line 77
    const-string v1, "maskedWalletDetailsBackgroundColor"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->g:I

    .line 79
    const-string v1, "maskedWalletDetailsBackgroundResource"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->h:I

    .line 82
    const-string v1, "maskedWalletDetailsButtonTextAppearance"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->i:I

    .line 85
    const-string v1, "maskedWalletDetailsButtonBackgroundColor"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->j:I

    .line 88
    const-string v1, "maskedWalletDetailsButtonBackgroundResource"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->k:I

    .line 91
    const-string v1, "maskedWalletDetailsLogoTextColor"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->l:I

    .line 93
    const-string v1, "maskedWalletDetailsLogoImageType"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->m:I

    .line 96
    return-void
.end method
