.class public final Lcom/google/android/gms/fitness/sensors/e/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/fitness/sensors/e/c;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/fitness/sensors/e/d;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Lcom/google/android/gms/fitness/sensors/e/c;

    invoke-direct {v0, p1}, Lcom/google/android/gms/fitness/sensors/e/c;-><init>(Lcom/google/android/gms/fitness/sensors/e/d;)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/e/a;->a:Lcom/google/android/gms/fitness/sensors/e/c;

    .line 42
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/fitness/sensors/e/e;
    .locals 20

    .prologue
    .line 49
    const-wide v4, 0x7fffffffffffffffL

    .line 50
    const-wide v6, 0x7fffffffffffffffL

    .line 51
    const-wide v8, 0x7fffffffffffffffL

    .line 52
    const-wide/high16 v13, -0x8000000000000000L

    .line 53
    const/4 v10, 0x1

    .line 54
    new-instance v11, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/fitness/sensors/e/a;->a:Lcom/google/android/gms/fitness/sensors/e/c;

    invoke-static {v2}, Lcom/google/android/gms/fitness/sensors/e/c;->a(Lcom/google/android/gms/fitness/sensors/e/c;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-direct {v11, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 56
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/fitness/sensors/e/a;->a:Lcom/google/android/gms/fitness/sensors/e/c;

    invoke-static {v2}, Lcom/google/android/gms/fitness/sensors/e/c;->a(Lcom/google/android/gms/fitness/sensors/e/c;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/sensors/e/e;

    .line 57
    iget-wide v0, v2, Lcom/google/android/gms/fitness/sensors/e/e;->a:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 58
    iget-wide v0, v2, Lcom/google/android/gms/fitness/sensors/e/e;->b:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    .line 59
    iget v12, v2, Lcom/google/android/gms/fitness/sensors/e/e;->c:I

    invoke-static {v12, v10}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 60
    iget-object v12, v2, Lcom/google/android/gms/fitness/sensors/e/e;->d:Ljava/util/List;

    if-eqz v12, :cond_1

    .line 61
    iget-object v12, v2, Lcom/google/android/gms/fitness/sensors/e/e;->d:Ljava/util/List;

    invoke-interface {v11, v12}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 63
    :cond_1
    iget-wide v0, v2, Lcom/google/android/gms/fitness/sensors/e/e;->f:J

    move-wide/from16 v16, v0

    .line 64
    cmp-long v12, v16, v8

    if-gez v12, :cond_0

    .line 66
    iget-wide v8, v2, Lcom/google/android/gms/fitness/sensors/e/e;->g:J

    invoke-static {v8, v9, v13, v14}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v13

    move-wide/from16 v8, v16

    goto :goto_0

    .line 69
    :cond_2
    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 70
    const/4 v11, 0x0

    .line 74
    :cond_3
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 75
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/fitness/sensors/e/a;->a:Lcom/google/android/gms/fitness/sensors/e/c;

    invoke-static {v2}, Lcom/google/android/gms/fitness/sensors/e/c;->a(Lcom/google/android/gms/fitness/sensors/e/c;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/sensors/e/e;

    .line 76
    iget-wide v0, v2, Lcom/google/android/gms/fitness/sensors/e/e;->a:J

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x2

    mul-long v18, v18, v4

    cmp-long v15, v16, v18

    if-gtz v15, :cond_4

    .line 77
    iget-object v2, v2, Lcom/google/android/gms/fitness/sensors/e/e;->e:Ljava/util/List;

    invoke-interface {v12, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 81
    :cond_5
    new-instance v3, Lcom/google/android/gms/fitness/sensors/e/e;

    const/4 v15, 0x0

    invoke-direct/range {v3 .. v15}, Lcom/google/android/gms/fitness/sensors/e/e;-><init>(JJJILjava/util/List;Ljava/util/List;JB)V

    return-object v3
.end method

.method public final a(Lcom/google/android/gms/fitness/sensors/e/e;)Z
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/sensors/e/a;->a()Lcom/google/android/gms/fitness/sensors/e/e;

    move-result-object v0

    .line 90
    invoke-virtual {v0, p1}, Lcom/google/android/gms/fitness/sensors/e/e;->a(Lcom/google/android/gms/fitness/sensors/e/e;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/e/a;->a:Lcom/google/android/gms/fitness/sensors/e/c;

    invoke-static {v0}, Lcom/google/android/gms/fitness/sensors/e/c;->a(Lcom/google/android/gms/fitness/sensors/e/c;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 143
    const-string v0, "CompositeRegistration{%s %s}"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/e/a;->a:Lcom/google/android/gms/fitness/sensors/e/c;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/sensors/e/a;->a()Lcom/google/android/gms/fitness/sensors/e/e;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
