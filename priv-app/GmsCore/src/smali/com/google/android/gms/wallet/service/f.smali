.class final Lcom/google/android/gms/wallet/service/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/service/e;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/service/e;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/f;->a:Lcom/google/android/gms/wallet/service/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/f;->a:Lcom/google/android/gms/wallet/service/e;

    invoke-static {p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Landroid/os/IBinder;)Lcom/google/android/gms/wallet/service/ia/l;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/wallet/service/e;->j:Lcom/google/android/gms/wallet/service/ia/l;

    .line 142
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/f;->a:Lcom/google/android/gms/wallet/service/e;

    iget-object v0, v0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/16 v1, 0x7fff

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 144
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/f;->a:Lcom/google/android/gms/wallet/service/e;

    iget-object v1, v1, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 145
    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    .line 151
    const-string v0, "NetworkPaymentServiceConnection"

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/f;->a:Lcom/google/android/gms/wallet/service/e;

    iget-object v0, v0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const v1, 0x8000

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 154
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/f;->a:Lcom/google/android/gms/wallet/service/e;

    iget-object v1, v1, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/f;->a:Lcom/google/android/gms/wallet/service/e;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/wallet/service/e;->j:Lcom/google/android/gms/wallet/service/ia/l;

    .line 156
    return-void
.end method
