.class public final Lcom/google/android/gms/games/g/c;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/android/gms/games/g/c;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3914
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3915
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/c;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/c;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/g/c;->c:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/c;->cachedSize:I

    .line 3916
    return-void
.end method

.method public static a()[Lcom/google/android/gms/games/g/c;
    .locals 2

    .prologue
    .line 3894
    sget-object v0, Lcom/google/android/gms/games/g/c;->d:[Lcom/google/android/gms/games/g/c;

    if-nez v0, :cond_1

    .line 3895
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 3897
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/g/c;->d:[Lcom/google/android/gms/games/g/c;

    if-nez v0, :cond_0

    .line 3898
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/games/g/c;

    sput-object v0, Lcom/google/android/gms/games/g/c;->d:[Lcom/google/android/gms/games/g/c;

    .line 3900
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3902
    :cond_1
    sget-object v0, Lcom/google/android/gms/games/g/c;->d:[Lcom/google/android/gms/games/g/c;

    return-object v0

    .line 3900
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3983
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 3984
    iget-object v1, p0, Lcom/google/android/gms/games/g/c;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3985
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/g/c;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3988
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/g/c;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3989
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/g/c;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3992
    :cond_1
    iget v1, p0, Lcom/google/android/gms/games/g/c;->c:I

    if-eqz v1, :cond_2

    .line 3993
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/games/g/c;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3996
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3928
    if-ne p1, p0, :cond_1

    .line 3952
    :cond_0
    :goto_0
    return v0

    .line 3931
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/c;

    if-nez v2, :cond_2

    move v0, v1

    .line 3932
    goto :goto_0

    .line 3934
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/c;

    .line 3935
    iget-object v2, p0, Lcom/google/android/gms/games/g/c;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 3936
    iget-object v2, p1, Lcom/google/android/gms/games/g/c;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 3937
    goto :goto_0

    .line 3939
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/c;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/c;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 3940
    goto :goto_0

    .line 3942
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/g/c;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 3943
    iget-object v2, p1, Lcom/google/android/gms/games/g/c;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 3944
    goto :goto_0

    .line 3946
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/g/c;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/c;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 3947
    goto :goto_0

    .line 3949
    :cond_6
    iget v2, p0, Lcom/google/android/gms/games/g/c;->c:I

    iget v3, p1, Lcom/google/android/gms/games/g/c;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 3950
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3957
    iget-object v0, p0, Lcom/google/android/gms/games/g/c;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3960
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/g/c;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 3962
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/g/c;->c:I

    add-int/2addr v0, v1

    .line 3963
    return v0

    .line 3957
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3960
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/g/c;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3880
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/c;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/c;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/g/c;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3969
    iget-object v0, p0, Lcom/google/android/gms/games/g/c;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3970
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/g/c;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3972
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/c;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3973
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/g/c;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3975
    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/g/c;->c:I

    if-eqz v0, :cond_2

    .line 3976
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/games/g/c;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3978
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3979
    return-void
.end method
