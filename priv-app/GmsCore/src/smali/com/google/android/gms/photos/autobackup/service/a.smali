.class final Lcom/google/android/gms/photos/autobackup/service/a;
.super Lcom/google/android/gms/photos/autobackup/a/ag;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/photos/autobackup/service/a/b;

.field private final b:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/photos/autobackup/service/a/b;)V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/google/android/gms/photos/autobackup/a/ag;-><init>()V

    .line 107
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/service/a;->b:Landroid/content/Context;

    .line 108
    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/service/a;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    .line 109
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/photos/autobackup/service/a;)Lcom/google/android/gms/photos/autobackup/service/a/b;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/photos/autobackup/a/ac;)V
    .locals 2

    .prologue
    .line 113
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a;->b:Landroid/content/Context;

    const-class v1, Lcom/google/android/gms/photos/autobackup/service/a/a;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/service/a/a;

    .line 117
    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/service/a/a;->b()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/service/a;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    if-nez v1, :cond_1

    .line 118
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/service/a/a;->a()V

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/u;

    invoke-direct {v1, p1}, Lcom/google/android/gms/photos/autobackup/u;-><init>(Lcom/google/android/gms/photos/autobackup/a/ac;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 149
    :goto_0
    return-void

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/service/b;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/photos/autobackup/service/b;-><init>(Lcom/google/android/gms/photos/autobackup/service/a;Lcom/google/android/gms/photos/autobackup/a/ac;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/photos/autobackup/service/a/b;->a(Lcom/google/android/gms/photos/autobackup/service/a/g;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/photos/autobackup/a/ac;Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;)V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/ab;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/photos/autobackup/ab;-><init>(Lcom/google/android/gms/photos/autobackup/a/ac;Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 228
    return-void
.end method

.method public final a(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 169
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/service/a;->b:Landroid/content/Context;

    const-class v2, Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 173
    const-string v1, ".photos.autobackup.ui.Extras.Account.NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 174
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/service/a;->b:Landroid/content/Context;

    invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 176
    const/4 v1, 0x0

    :try_start_0
    invoke-interface {p1, v1, v0}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(ILandroid/app/PendingIntent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    :goto_0
    return-void

    .line 179
    :catch_0
    move-exception v0

    const-string v0, "AutoBackupService"

    const-string v1, "Failed trying to deliver failure"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;Lcom/google/android/gms/photos/autobackup/model/LocalFolder;)V
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/ad;

    invoke-direct {v1, p1, p3}, Lcom/google/android/gms/photos/autobackup/ad;-><init>(Lcom/google/android/gms/photos/autobackup/a/ac;Lcom/google/android/gms/photos/autobackup/model/LocalFolder;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 203
    return-void
.end method

.method public final a(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;Lcom/google/android/gms/photos/autobackup/model/SyncSettings;)V
    .locals 2

    .prologue
    .line 249
    new-instance v0, Lcom/google/android/gms/photos/autobackup/ae;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/photos/autobackup/ae;-><init>(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;Lcom/google/android/gms/photos/autobackup/model/SyncSettings;)V

    .line 251
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/service/a;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 252
    return-void
.end method

.method public final b(Lcom/google/android/gms/photos/autobackup/a/ac;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 153
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/service/a;->b:Landroid/content/Context;

    const-class v2, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 156
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/service/a;->b:Landroid/content/Context;

    const/high16 v2, 0x8000000

    invoke-static {v1, v3, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 159
    const/4 v1, 0x0

    :try_start_0
    invoke-interface {p1, v1, v0}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(ILandroid/app/PendingIntent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    :goto_0
    return-void

    .line 162
    :catch_0
    move-exception v0

    const-string v0, "AutoBackupService"

    const-string v1, "Failed trying to deliver success"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 192
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/w;

    invoke-direct {v1, p1}, Lcom/google/android/gms/photos/autobackup/w;-><init>(Lcom/google/android/gms/photos/autobackup/a/ac;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 195
    return-void
.end method

.method public final c(Lcom/google/android/gms/photos/autobackup/a/ac;)V
    .locals 2

    .prologue
    .line 185
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/p;

    invoke-direct {v1, p1}, Lcom/google/android/gms/photos/autobackup/p;-><init>(Lcom/google/android/gms/photos/autobackup/a/ac;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 188
    return-void
.end method

.method public final c(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/b/a;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/photos/autobackup/b/a;-><init>(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 211
    return-void
.end method

.method public final d(Lcom/google/android/gms/photos/autobackup/a/ac;)V
    .locals 2

    .prologue
    .line 215
    const-string v0, "AutoBackupService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    const-string v0, "AutoBackupService"

    const-string v1, "retrying failed uploads"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/aa;

    invoke-direct {v1, p1}, Lcom/google/android/gms/photos/autobackup/aa;-><init>(Lcom/google/android/gms/photos/autobackup/a/ac;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 221
    return-void
.end method

.method public final d(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 233
    new-instance v0, Lcom/google/android/gms/photos/autobackup/o;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/photos/autobackup/o;-><init>(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V

    .line 234
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/service/a;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 235
    return-void
.end method

.method public final e(Lcom/google/android/gms/photos/autobackup/a/ac;)V
    .locals 3

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a;->b:Landroid/content/Context;

    const-class v1, Lcom/google/android/gms/photos/autobackup/service/a/a;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/service/a/a;

    .line 258
    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/gms/photos/autobackup/model/MigrationStatus;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/service/a/a;->b()Z

    move-result v0

    invoke-direct {v2, v0}, Lcom/google/android/gms/photos/autobackup/model/MigrationStatus;-><init>(Z)V

    invoke-interface {p1, v1, v2}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(ILcom/google/android/gms/photos/autobackup/model/MigrationStatus;)V

    .line 260
    return-void
.end method

.method public final e(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 240
    new-instance v0, Lcom/google/android/gms/photos/autobackup/s;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/photos/autobackup/s;-><init>(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V

    .line 242
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/service/a;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 243
    return-void
.end method
