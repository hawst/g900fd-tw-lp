.class public final Lcom/google/android/gms/plus/service/v1whitelisted/models/cg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/List;

.field private final b:Ljava/util/Set;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cg;->b:Ljava/util/Set;

    .line 36
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/plus/service/v1whitelisted/models/cf;
    .locals 3

    .prologue
    .line 54
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientLoggedRhsComponentEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cg;->b:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cg;->a:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientLoggedRhsComponentEntity;-><init>(Ljava/util/Set;Ljava/util/List;)V

    return-object v0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/cg;
    .locals 2

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cg;->a:Ljava/util/List;

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cg;->b:Ljava/util/Set;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 47
    return-object p0
.end method
