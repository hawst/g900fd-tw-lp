.class final Lcom/google/android/gms/plus/sharebox/ar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/sharebox/aq;

.field private final b:Lcom/google/android/gms/plus/sharebox/aa;

.field private final c:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/sharebox/aq;Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    new-instance v0, Lcom/google/android/gms/plus/sharebox/aa;

    invoke-direct {v0}, Lcom/google/android/gms/plus/sharebox/aa;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ar;->b:Lcom/google/android/gms/plus/sharebox/aa;

    .line 107
    sget v0, Lcom/google/android/gms/g;->bJ:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/gms/plus/sharebox/ar;->c:I

    .line 109
    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/aq;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/aq;->a(Lcom/google/android/gms/plus/sharebox/aq;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    if-nez v0, :cond_1

    .line 162
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 147
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/aq;->c(Lcom/google/android/gms/plus/sharebox/aq;)Lcom/google/android/gms/plus/sharebox/as;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bg;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/aq;->d(Lcom/google/android/gms/plus/sharebox/aq;)Z

    .line 157
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/aq;->e(Lcom/google/android/gms/plus/sharebox/aq;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/aq;->f(Lcom/google/android/gms/plus/sharebox/aq;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/aq;->c(Lcom/google/android/gms/plus/sharebox/aq;)Lcom/google/android/gms/plus/sharebox/as;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/aq;->g(Lcom/google/android/gms/plus/sharebox/aq;)Z

    .line 161
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/aq;->c(Lcom/google/android/gms/plus/sharebox/aq;)Lcom/google/android/gms/plus/sharebox/as;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->p()V

    goto :goto_0

    .line 151
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/aq;->c(Lcom/google/android/gms/plus/sharebox/aq;)Lcom/google/android/gms/plus/sharebox/as;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/bg;->h()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/aq;->c(Lcom/google/android/gms/plus/sharebox/aq;)Lcom/google/android/gms/plus/sharebox/as;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/plus/sharebox/bg;->s:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 153
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/aq;->d(Lcom/google/android/gms/plus/sharebox/aq;)Z

    goto :goto_1
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 113
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/aq;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/aq;->a(Lcom/google/android/gms/plus/sharebox/aq;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    if-nez v0, :cond_1

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getSelectionEnd()I

    move-result v0

    .line 121
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ar;->b:Lcom/google/android/gms/plus/sharebox/aa;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/gms/plus/sharebox/aa;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    iget-object v2, v2, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getThreshold()I

    move-result v2

    add-int/2addr v1, v2

    if-gt v1, v0, :cond_0

    .line 124
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 125
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    iget-object v1, v1, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getLocationOnScreen([I)V

    .line 126
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 127
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/aq;->a(Lcom/google/android/gms/plus/sharebox/aq;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 129
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 130
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    iget-object v2, v2, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a()I

    move-result v2

    add-int/2addr v0, v2

    .line 131
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 133
    sub-int v0, v1, v0

    iget v1, p0, Lcom/google/android/gms/plus/sharebox/ar;->c:I

    if-ge v0, v1, :cond_0

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/aq;->b(Lcom/google/android/gms/plus/sharebox/aq;)Landroid/widget/ScrollView;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ar;->a:Lcom/google/android/gms/plus/sharebox/aq;

    iget-object v2, v2, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    goto :goto_0
.end method
