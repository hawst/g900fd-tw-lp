.class final Lcom/google/android/gms/games/ui/cz;
.super Lcom/google/android/gms/games/ui/bk;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic b:Lcom/google/android/gms/games/ui/cy;

.field private final c:Landroid/view/View;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/view/View;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/view/View;

.field private final i:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/cy;Lcom/google/android/gms/games/ui/o;Landroid/view/View;)V
    .locals 10
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/16 v9, 0xb

    const/16 v8, 0xa

    const/4 v7, -0x2

    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 127
    iput-object p1, p0, Lcom/google/android/gms/games/ui/cz;->b:Lcom/google/android/gms/games/ui/cy;

    .line 128
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/ui/bk;-><init>(Lcom/google/android/gms/games/ui/o;)V

    .line 129
    iput-object p3, p0, Lcom/google/android/gms/games/ui/cz;->c:Landroid/view/View;

    .line 130
    sget v0, Lcom/google/android/gms/j;->ka:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/cz;->d:Landroid/widget/ImageView;

    .line 131
    sget v0, Lcom/google/android/gms/j;->sy:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/cz;->e:Landroid/widget/TextView;

    .line 132
    sget v0, Lcom/google/android/gms/j;->ey:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/cz;->f:Landroid/view/View;

    .line 133
    sget v0, Lcom/google/android/gms/j;->ci:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/cz;->g:Landroid/widget/TextView;

    .line 134
    sget v0, Lcom/google/android/gms/j;->sL:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/cz;->h:Landroid/view/View;

    .line 135
    sget v0, Lcom/google/android/gms/j;->cl:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/cz;->i:Landroid/view/View;

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->c:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/cz;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    invoke-static {p1}, Lcom/google/android/gms/games/ui/cy;->a(Lcom/google/android/gms/games/ui/cy;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/cz;->c:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/gms/games/ui/cz;->c:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 141
    invoke-static {p1}, Lcom/google/android/gms/games/ui/cy;->b(Lcom/google/android/gms/games/ui/cy;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 143
    sget v0, Lcom/google/android/gms/j;->sJ:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 146
    invoke-static {p1}, Lcom/google/android/gms/games/ui/cy;->c(Lcom/google/android/gms/games/ui/cy;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 147
    sget v3, Lcom/google/android/gms/g;->aJ:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 150
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 153
    const/16 v0, 0x11

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->g:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/h;->ak:I

    invoke-virtual {v0, v1, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    .line 210
    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/google/android/gms/games/ui/cy;->d(Lcom/google/android/gms/games/ui/cy;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 211
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->h:Landroid/view/View;

    invoke-static {p1}, Lcom/google/android/gms/games/ui/cy;->e(Lcom/google/android/gms/games/ui/cy;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->h:Landroid/view/View;

    invoke-static {p1}, Lcom/google/android/gms/games/ui/cy;->d(Lcom/google/android/gms/games/ui/cy;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 215
    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/games/ui/cy;->f(Lcom/google/android/gms/games/ui/cy;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 216
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->i:Landroid/view/View;

    invoke-static {p1}, Lcom/google/android/gms/games/ui/cy;->e(Lcom/google/android/gms/games/ui/cy;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 217
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->i:Landroid/view/View;

    invoke-static {p1}, Lcom/google/android/gms/games/ui/cy;->f(Lcom/google/android/gms/games/ui/cy;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 219
    :cond_2
    return-void

    .line 157
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->g:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/h;->ak:I

    invoke-virtual {v0, v1, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0

    .line 160
    :cond_4
    invoke-static {p1}, Lcom/google/android/gms/games/ui/cy;->b(Lcom/google/android/gms/games/ui/cy;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 164
    iput v7, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 165
    iput v7, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 166
    invoke-virtual {v0, v9, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 167
    invoke-virtual {v0, v8, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 169
    invoke-static {p1}, Lcom/google/android/gms/games/ui/cy;->c(Lcom/google/android/gms/games/ui/cy;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/g;->aK:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 171
    iget-object v2, p0, Lcom/google/android/gms/games/ui/cz;->d:Landroid/widget/ImageView;

    invoke-virtual {v2, v5, v1, v1, v5}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 172
    iget-object v1, p0, Lcom/google/android/gms/games/ui/cz;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 176
    iput v6, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 178
    iget-object v1, p0, Lcom/google/android/gms/games/ui/cz;->f:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 179
    :cond_5
    invoke-static {p1}, Lcom/google/android/gms/games/ui/cy;->b(Lcom/google/android/gms/games/ui/cy;)I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 181
    sget v0, Lcom/google/android/gms/j;->sJ:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 183
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 184
    invoke-static {p1}, Lcom/google/android/gms/games/ui/cy;->c(Lcom/google/android/gms/games/ui/cy;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 185
    sget v3, Lcom/google/android/gms/g;->aI:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 188
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 193
    iput v7, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 194
    iput v7, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 195
    invoke-virtual {v0, v9, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 196
    invoke-virtual {v0, v8, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 198
    invoke-static {p1}, Lcom/google/android/gms/games/ui/cy;->c(Lcom/google/android/gms/games/ui/cy;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/g;->aK:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 200
    iget-object v2, p0, Lcom/google/android/gms/games/ui/cz;->d:Landroid/widget/ImageView;

    invoke-virtual {v2, v5, v1, v1, v5}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 201
    iget-object v1, p0, Lcom/google/android/gms/games/ui/cz;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 205
    iput v6, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 207
    iget-object v1, p0, Lcom/google/android/gms/games/ui/cz;->f:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0
.end method

.method private a(Z)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 253
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v2

    .line 254
    if-lez v2, :cond_0

    .line 255
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 257
    int-to-float v2, v2

    const/high16 v3, 0x40400000    # 3.0f

    div-float/2addr v2, v3

    float-to-int v2, v2

    neg-int v2, v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 258
    iget-object v2, p0, Lcom/google/android/gms/games/ui/cz;->d:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 259
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 260
    const/4 v0, 0x1

    .line 268
    :goto_0
    return v0

    .line 262
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->d:Landroid/widget/ImageView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 265
    if-eqz p1, :cond_1

    .line 266
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_1
    move v0, v1

    .line 268
    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->d:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/cz;->b:Lcom/google/android/gms/games/ui/cy;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/cy;->g(Lcom/google/android/gms/games/ui/cy;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 224
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->b:Lcom/google/android/gms/games/ui/cy;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/cy;->b(Lcom/google/android/gms/games/ui/cy;)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->b:Lcom/google/android/gms/games/ui/cy;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/cy;->b(Lcom/google/android/gms/games/ui/cy;)I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 225
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/cz;->a(Z)Z

    .line 230
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/cz;->b:Lcom/google/android/gms/games/ui/cy;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/cy;->h(Lcom/google/android/gms/games/ui/cy;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 231
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->h:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/cz;->b:Lcom/google/android/gms/games/ui/cy;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/cy;->c(Lcom/google/android/gms/games/ui/cy;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/cz;->b:Lcom/google/android/gms/games/ui/cy;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/cy;->h(Lcom/google/android/gms/games/ui/cy;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->b:Lcom/google/android/gms/games/ui/cy;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/cy;->i(Lcom/google/android/gms/games/ui/cy;)I

    move-result v0

    if-lez v0, :cond_1

    .line 234
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 236
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/cz;->b:Lcom/google/android/gms/games/ui/cy;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/cy;->i(Lcom/google/android/gms/games/ui/cy;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 237
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->i:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/cz;->b:Lcom/google/android/gms/games/ui/cy;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/cy;->c(Lcom/google/android/gms/games/ui/cy;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/cz;->b:Lcom/google/android/gms/games/ui/cy;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/cy;->i(Lcom/google/android/gms/games/ui/cy;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 243
    :goto_1
    return-void

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 240
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->f:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final onGlobalLayout()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->b:Lcom/google/android/gms/games/ui/cy;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/cy;->b(Lcom/google/android/gms/games/ui/cy;)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->b:Lcom/google/android/gms/games/ui/cy;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/cy;->b(Lcom/google/android/gms/games/ui/cy;)I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 277
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/cz;->a(Z)Z

    move-result v0

    .line 278
    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cz;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 280
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 281
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 287
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method
