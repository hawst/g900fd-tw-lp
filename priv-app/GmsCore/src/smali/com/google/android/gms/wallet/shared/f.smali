.class public final Lcom/google/android/gms/wallet/shared/f;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sparse-switch p0, :sswitch_data_0

    .line 37
    sget-object v0, Lcom/google/android/gms/wallet/b/b;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    .line 35
    :sswitch_0
    sget-object v0, Lcom/google/android/gms/wallet/b/b;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 31
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_0
        0x15 -> :sswitch_0
    .end sparse-switch
.end method

.method public static b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    sparse-switch p0, :sswitch_data_0

    .line 87
    const-string v0, "https://wallet.google.com"

    :goto_0
    return-object v0

    .line 80
    :sswitch_0
    const-string v0, "https://wallet-web.sandbox.google.com"

    goto :goto_0

    .line 82
    :sswitch_1
    const-string v0, "https://wallet-web.sandbox.google.com/dev"

    goto :goto_0

    .line 84
    :sswitch_2
    const-string v0, "https://starlightdemo.corp.google.com"

    goto :goto_0

    .line 77
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_0
        0x14 -> :sswitch_2
        0x15 -> :sswitch_1
    .end sparse-switch
.end method
