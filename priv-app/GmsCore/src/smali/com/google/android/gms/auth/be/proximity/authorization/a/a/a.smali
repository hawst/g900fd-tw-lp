.class public Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/be/proximity/authorization/c;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/gms/auth/firstparty/proximity/a;

.field private final c:Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;

.field private final d:Lcom/google/android/gms/auth/trustagent/at;

.field private final e:Lcom/google/android/gms/auth/be/proximity/authorization/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/auth/be/proximity/authorization/h;)V
    .locals 3

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/gms/auth/firstparty/proximity/a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/auth/firstparty/proximity/a;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/at;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/trustagent/at;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2, p2}, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;-><init>(Lcom/google/android/gms/auth/firstparty/proximity/a;Lcom/google/android/gms/auth/trustagent/at;Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;Lcom/google/android/gms/auth/be/proximity/authorization/h;)V

    .line 66
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/google/android/gms/auth/be/proximity/authorization/h;B)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/auth/be/proximity/authorization/h;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/auth/firstparty/proximity/a;Lcom/google/android/gms/auth/trustagent/at;Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;Lcom/google/android/gms/auth/be/proximity/authorization/h;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/proximity/a;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;->b:Lcom/google/android/gms/auth/firstparty/proximity/a;

    .line 75
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/at;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;->d:Lcom/google/android/gms/auth/trustagent/at;

    .line 76
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;->c:Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;

    .line 77
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/proximity/authorization/h;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;->e:Lcom/google/android/gms/auth/be/proximity/authorization/h;

    .line 78
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;->e:Lcom/google/android/gms/auth/be/proximity/authorization/h;

    new-instance v1, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/e;

    invoke-direct {v1}, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/e;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/be/proximity/authorization/h;->a(Lcom/google/android/gms/auth/be/proximity/authorization/k;)V

    .line 83
    return-void
.end method

.method public final a(Lcom/google/android/gms/auth/be/proximity/authorization/d;)V
    .locals 5

    .prologue
    .line 97
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;->a:Ljava/lang/String;

    const-string v1, "Handling authorization incoming message V1."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-byte v0, p1, Lcom/google/android/gms/auth/be/proximity/a/a;->d:B

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 99
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Received unexpected incoming message."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/c;

    if-eqz v0, :cond_1

    .line 103
    check-cast p1, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/c;

    .line 107
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;->d:Lcom/google/android/gms/auth/trustagent/at;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/at;->c()Z
    :try_end_0
    .catch Lcom/google/android/gms/auth/trustagent/az; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 112
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;->c:Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;->a()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    .line 114
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;->a:Ljava/lang/String;

    const-string v1, "Failed to authorize because the user is not present."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;->e:Lcom/google/android/gms/auth/be/proximity/authorization/h;

    new-instance v1, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/d;

    iget-object v2, p1, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/c;->b:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/d;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/be/proximity/authorization/h;->a(Lcom/google/android/gms/auth/be/proximity/authorization/k;)V

    .line 137
    :cond_1
    :goto_0
    return-void

    .line 108
    :catch_0
    move-exception v0

    .line 109
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Cannot get trust state."

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 121
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;->b:Lcom/google/android/gms/auth/firstparty/proximity/a;

    new-instance v1, Lcom/google/android/gms/auth/firstparty/proximity/data/AuthorizationRequest;

    iget-object v2, p1, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/c;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/c;->c:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/c;->a:[B

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/auth/firstparty/proximity/data/AuthorizationRequest;-><init>(Ljava/lang/String;Ljava/lang/String;[B)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/proximity/a;->a(Lcom/google/android/gms/auth/firstparty/proximity/data/AuthorizationRequest;)Lcom/google/android/gms/auth/firstparty/proximity/data/Authorization;

    move-result-object v0

    .line 126
    sget-object v1, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;->a:Ljava/lang/String;

    const-string v2, "Succeeded to authorize the request."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v1, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;->e:Lcom/google/android/gms/auth/be/proximity/authorization/h;

    new-instance v2, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/proximity/data/Authorization;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/proximity/data/Authorization;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/proximity/data/Authorization;->c()[B

    move-result-object v0

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/f;-><init>(Ljava/lang/String;Ljava/lang/String;[B)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/be/proximity/authorization/h;->a(Lcom/google/android/gms/auth/be/proximity/authorization/k;)V
    :try_end_1
    .catch Lcom/google/android/gms/auth/firstparty/proximity/g; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 131
    :catch_1
    move-exception v0

    .line 132
    sget-object v1, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;->a:Ljava/lang/String;

    const-string v2, "Failed to authorize the request."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;->e:Lcom/google/android/gms/auth/be/proximity/authorization/h;

    new-instance v1, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/d;

    iget-object v2, p1, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/c;->b:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/d;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/be/proximity/authorization/h;->a(Lcom/google/android/gms/auth/be/proximity/authorization/k;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/util/Set;)V
    .locals 3

    .prologue
    .line 89
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/a;->e:Lcom/google/android/gms/auth/be/proximity/authorization/h;

    new-instance v1, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/e;

    invoke-direct {v1}, Lcom/google/android/gms/auth/be/proximity/authorization/a/a/e;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/be/proximity/authorization/h;->a(Lcom/google/android/gms/auth/be/proximity/authorization/k;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :goto_0
    return-void

    .line 90
    :catch_0
    move-exception v0

    .line 91
    new-instance v1, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v2, "Cannot send retry message"

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
