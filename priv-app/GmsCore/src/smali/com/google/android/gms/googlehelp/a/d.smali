.class public final Lcom/google/android/gms/googlehelp/a/d;
.super Lcom/google/android/gms/googlehelp/a/i;
.source "SourceFile"


# instance fields
.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 7

    .prologue
    .line 49
    invoke-virtual {p2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->g()Landroid/accounts/Account;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p7

    move-object v6, p8

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/googlehelp/a/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Landroid/accounts/Account;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 50
    iput-object p4, p0, Lcom/google/android/gms/googlehelp/a/d;->i:Ljava/lang/String;

    .line 51
    iput-object p5, p0, Lcom/google/android/gms/googlehelp/a/d;->j:Ljava/lang/String;

    .line 52
    iput-object p6, p0, Lcom/google/android/gms/googlehelp/a/d;->k:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 8

    .prologue
    .line 90
    new-instance v0, Lcom/google/android/gms/googlehelp/a/e;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/googlehelp/a/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/a/e;->a([Ljava/lang/Object;)V

    .line 100
    return-void
.end method

.method static synthetic t()Ljava/lang/String;
    .locals 3

    .prologue
    .line 23
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "hl"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/android/volley/m;)Lcom/android/volley/v;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 64
    iget v0, p1, Lcom/android/volley/m;->a:I

    const/16 v1, 0xca

    if-eq v0, v1, :cond_0

    iget v0, p1, Lcom/android/volley/m;->a:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_1

    .line 66
    :cond_0
    invoke-static {v2, v2}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;

    move-result-object v0

    .line 68
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/android/volley/ac;

    invoke-direct {v0, p1}, Lcom/android/volley/ac;-><init>(Lcom/android/volley/m;)V

    invoke-static {v0}, Lcom/android/volley/v;->a(Lcom/android/volley/ac;)Lcom/android/volley/v;

    move-result-object v0

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/googlehelp/a/l;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/d;->i:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/android/gms/googlehelp/a/l;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/d;->j:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/android/gms/googlehelp/a/l;->g:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/d;->k:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/android/gms/googlehelp/a/l;->h:Ljava/lang/String;

    .line 60
    return-void
.end method
