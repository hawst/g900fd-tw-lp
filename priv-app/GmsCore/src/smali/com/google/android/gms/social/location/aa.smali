.class public final Lcom/google/android/gms/social/location/aa;
.super Landroid/support/v7/widget/bv;
.source "SourceFile"


# instance fields
.field final c:Landroid/content/Context;

.field final d:Landroid/support/v4/app/v;

.field e:Lcom/google/android/gms/social/location/DeviceLocationSettings;

.field f:Z

.field private g:Lcom/google/android/gms/social/location/q;

.field private h:Lcom/google/android/gms/social/location/am;

.field private final i:Lcom/google/android/gms/common/internal/a/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/social/location/q;Lcom/google/android/gms/social/location/am;Lcom/google/android/gms/common/internal/a/b;Landroid/support/v4/app/v;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/support/v7/widget/bv;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/google/android/gms/social/location/aa;->c:Landroid/content/Context;

    .line 65
    iput-object p2, p0, Lcom/google/android/gms/social/location/aa;->g:Lcom/google/android/gms/social/location/q;

    .line 66
    iput-object p3, p0, Lcom/google/android/gms/social/location/aa;->h:Lcom/google/android/gms/social/location/am;

    .line 67
    iput-object p4, p0, Lcom/google/android/gms/social/location/aa;->i:Lcom/google/android/gms/common/internal/a/b;

    .line 68
    iput-object p5, p0, Lcom/google/android/gms/social/location/aa;->d:Landroid/support/v4/app/v;

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/social/location/aa;->e:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    .line 72
    return-void
.end method

.method private a(Lcom/google/android/gms/common/people/data/Audience;)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 262
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    .line 263
    invoke-static {v0}, Lcom/google/android/gms/social/location/aa;->a(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 264
    const-string v0, ""

    .line 317
    :goto_0
    return-object v0

    .line 266
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/social/location/aa;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 267
    const v1, 0x104000e

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 268
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 269
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 271
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 273
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->j()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 274
    sget v0, Lcom/google/android/gms/p;->uD:I

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v3, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_1

    .line 275
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->h()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 276
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    .line 277
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 278
    :goto_2
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 277
    goto :goto_2

    .line 280
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    .line 281
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 282
    :goto_3
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 281
    goto :goto_3

    .line 285
    :cond_5
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v9, :cond_6

    move v1, v2

    .line 286
    :goto_4
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 287
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 309
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 310
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 311
    if-eqz v1, :cond_7

    .line 314
    sget v1, Lcom/google/android/gms/p;->fv:I

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v6, v5, v3

    aput-object v0, v5, v2

    invoke-virtual {v4, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    move v1, v3

    .line 285
    goto :goto_4

    .line 289
    :pswitch_0
    const-string v0, ""

    goto/16 :goto_0

    .line 292
    :pswitch_1
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 293
    sget v1, Lcom/google/android/gms/p;->fx:I

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {v4, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 296
    :pswitch_2
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 297
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 298
    sget v5, Lcom/google/android/gms/p;->fC:I

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v1, v6, v3

    aput-object v0, v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 302
    :pswitch_3
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 303
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 304
    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 305
    sget v5, Lcom/google/android/gms/p;->fB:I

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v1, v7, v3

    aput-object v6, v7, v2

    aput-object v0, v7, v9

    invoke-virtual {v4, v5, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 317
    :cond_7
    sget v1, Lcom/google/android/gms/p;->fw:I

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v6, v7, v3

    aput-object v0, v7, v2

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v9

    invoke-virtual {v4, v1, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 287
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/gms/social/location/aa;)V
    .locals 3

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/social/location/aa;->e:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->e()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/social/location/aa;->f:Z

    invoke-virtual {p0}, Lcom/google/android/gms/social/location/aa;->a()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v2, v1, -0x2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/social/location/aa;->e(I)V

    add-int/lit8 v1, v1, -0x2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/social/location/aa;->c(II)V

    return-void
.end method

.method private static a(Ljava/util/List;)Z
    .locals 1

    .prologue
    .line 325
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/social/location/aa;->e:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    if-nez v0, :cond_1

    .line 91
    const/4 v0, 0x0

    .line 106
    :cond_0
    :goto_0
    return v0

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/social/location/aa;->e:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 94
    const/4 v0, 0x3

    .line 95
    if-lez v1, :cond_0

    .line 97
    iget-boolean v0, p0, Lcom/google/android/gms/social/location/aa;->f:Z

    if-eqz v0, :cond_2

    .line 99
    add-int/lit8 v0, v1, 0x4

    goto :goto_0

    .line 102
    :cond_2
    const/4 v0, 0x2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x5

    goto :goto_0
.end method

.method public final a(I)I
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 111
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/aa;->a()I

    move-result v2

    .line 112
    add-int/lit8 v2, v2, -0x1

    .line 113
    iget-object v3, p0, Lcom/google/android/gms/social/location/aa;->e:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v3}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->e()Ljava/util/List;

    move-result-object v3

    .line 114
    if-eqz p1, :cond_0

    if-ne p1, v2, :cond_2

    .line 115
    :cond_0
    const/4 v0, 0x0

    .line 124
    :cond_1
    :goto_0
    return v0

    .line 116
    :cond_2
    add-int/lit8 v4, v2, -0x1

    if-eq p1, v4, :cond_1

    .line 118
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_3

    if-ne p1, v0, :cond_3

    move v0, v1

    .line 119
    goto :goto_0

    .line 120
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/social/location/aa;->f:Z

    if-nez v0, :cond_4

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_4

    add-int/lit8 v0, v2, -0x2

    if-ne p1, v0, :cond_4

    .line 122
    const/4 v0, 0x3

    goto :goto_0

    .line 124
    :cond_4
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final synthetic a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/cs;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 27
    packed-switch p2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown view type given in onCreateViewHolder: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/social/location/aa;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->cB:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    new-instance v1, Lcom/google/android/gms/social/location/ae;

    invoke-direct {v1, v0}, Lcom/google/android/gms/social/location/ae;-><init>(Landroid/view/View;)V

    return-object v1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/social/location/aa;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->xdpi:F

    const/high16 v2, 0x43200000    # 160.0f

    div-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    new-instance v0, Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/gms/social/location/aa;->c:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x1

    invoke-direct {v3, v4, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget v2, Lcom/google/android/gms/f;->j:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/social/location/aa;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->cG:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->km:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/social/location/ac;

    invoke-direct {v2, p0}, Lcom/google/android/gms/social/location/ac;-><init>(Lcom/google/android/gms/social/location/aa;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/social/location/aa;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->cI:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/social/location/aa;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->cH:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final synthetic a(Landroid/support/v7/widget/cs;I)V
    .locals 4

    .prologue
    .line 27
    check-cast p1, Lcom/google/android/gms/social/location/ae;

    invoke-virtual {p0, p2}, Lcom/google/android/gms/social/location/aa;->a(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p1, Lcom/google/android/gms/social/location/ae;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;

    if-nez p2, :cond_0

    sget-object v1, Lcom/google/android/gms/social/location/af;->a:Lcom/google/android/gms/social/location/af;

    iget-object v2, p0, Lcom/google/android/gms/social/location/aa;->g:Lcom/google/android/gms/social/location/q;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->a(Lcom/google/android/gms/social/location/af;Lcom/google/android/gms/social/location/q;)V

    iget-object v1, p0, Lcom/google/android/gms/social/location/aa;->e:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v1}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->c()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/social/location/aa;->a(Lcom/google/android/gms/common/people/data/Audience;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/social/location/aa;->e:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v2}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->c()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/social/location/aa;->a(Ljava/util/List;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/google/android/gms/social/location/af;->b:Lcom/google/android/gms/social/location/af;

    iget-object v2, p0, Lcom/google/android/gms/social/location/aa;->g:Lcom/google/android/gms/social/location/q;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->a(Lcom/google/android/gms/social/location/af;Lcom/google/android/gms/social/location/q;)V

    iget-object v1, p0, Lcom/google/android/gms/social/location/aa;->e:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v1}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/social/location/aa;->a(Lcom/google/android/gms/common/people/data/Audience;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/social/location/aa;->e:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v2}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/social/location/aa;->a(Ljava/util/List;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :pswitch_2
    add-int/lit8 v0, p2, -0x2

    iget-object v1, p0, Lcom/google/android/gms/social/location/aa;->e:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v1}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/social/location/TimeShare;

    iget-object v1, p1, Lcom/google/android/gms/social/location/ae;->a:Landroid/view/View;

    check-cast v1, Lcom/google/android/gms/social/location/TimeShareView;

    iget-object v2, p0, Lcom/google/android/gms/social/location/aa;->h:Lcom/google/android/gms/social/location/am;

    iget-object v3, p0, Lcom/google/android/gms/social/location/aa;->i:Lcom/google/android/gms/common/internal/a/b;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/social/location/TimeShareView;->a(Lcom/google/android/gms/social/location/TimeShare;Lcom/google/android/gms/social/location/am;Lcom/google/android/gms/common/internal/a/b;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p1, Lcom/google/android/gms/social/location/ae;->a:Landroid/view/View;

    new-instance v1, Lcom/google/android/gms/social/location/ab;

    invoke-direct {v1, p0}, Lcom/google/android/gms/social/location/ab;-><init>(Lcom/google/android/gms/social/location/aa;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
