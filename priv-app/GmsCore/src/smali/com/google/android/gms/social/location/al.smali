.class final Lcom/google/android/gms/social/location/al;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/social/location/TimeShareView;

.field private final b:Ljava/lang/ref/WeakReference;

.field private final c:Lcom/google/android/gms/common/internal/a/b;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/social/location/TimeShareView;Landroid/widget/ImageView;Lcom/google/android/gms/common/internal/a/b;)V
    .locals 1

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/gms/social/location/al;->a:Lcom/google/android/gms/social/location/TimeShareView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 140
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/social/location/al;->b:Ljava/lang/ref/WeakReference;

    .line 141
    iput-object p3, p0, Lcom/google/android/gms/social/location/al;->c:Lcom/google/android/gms/common/internal/a/b;

    .line 142
    return-void
.end method

.method private varargs a([Lcom/google/android/gms/social/location/TimeShare;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 147
    const/4 v0, 0x0

    aget-object v1, p1, v0

    .line 148
    if-eqz v1, :cond_1

    .line 150
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-virtual {v1}, Lcom/google/android/gms/social/location/TimeShare;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 151
    invoke-virtual {v0}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 152
    iget-object v2, p0, Lcom/google/android/gms/social/location/al;->a:Lcom/google/android/gms/social/location/TimeShareView;

    invoke-static {v2}, Lcom/google/android/gms/social/location/TimeShareView;->c(Lcom/google/android/gms/social/location/TimeShareView;)Lcom/google/android/gms/social/location/c;

    move-result-object v2

    iput-object v0, v2, Lcom/google/android/gms/social/location/c;->a:Landroid/graphics/Bitmap;

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/social/location/al;->a:Lcom/google/android/gms/social/location/TimeShareView;

    invoke-static {v0}, Lcom/google/android/gms/social/location/TimeShareView;->c(Lcom/google/android/gms/social/location/TimeShareView;)Lcom/google/android/gms/social/location/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/social/location/c;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 154
    iget-object v2, p0, Lcom/google/android/gms/social/location/al;->c:Lcom/google/android/gms/common/internal/a/b;

    monitor-enter v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    :try_start_1
    iget-object v3, p0, Lcom/google/android/gms/social/location/al;->c:Lcom/google/android/gms/common/internal/a/b;

    invoke-virtual {v1}, Lcom/google/android/gms/social/location/TimeShare;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/common/internal/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    .line 156
    iget-object v3, p0, Lcom/google/android/gms/social/location/al;->c:Lcom/google/android/gms/common/internal/a/b;

    invoke-virtual {v1}, Lcom/google/android/gms/social/location/TimeShare;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1, v0}, Lcom/google/android/gms/common/internal/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164
    :goto_0
    return-object v0

    .line 158
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 160
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 164
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 134
    check-cast p1, [Lcom/google/android/gms/social/location/TimeShare;

    invoke-direct {p0, p1}, Lcom/google/android/gms/social/location/al;->a([Lcom/google/android/gms/social/location/TimeShare;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 134
    check-cast p1, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/gms/social/location/al;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/social/location/al;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method
