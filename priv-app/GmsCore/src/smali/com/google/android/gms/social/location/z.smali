.class final Lcom/google/android/gms/social/location/z;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)V
    .locals 0

    .prologue
    .line 558
    iput-object p1, p0, Lcom/google/android/gms/social/location/z;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;B)V
    .locals 0

    .prologue
    .line 558
    invoke-direct {p0, p1}, Lcom/google/android/gms/social/location/z;-><init>(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 558
    check-cast p1, [Lcom/google/android/gms/social/location/af;

    new-instance v0, Lcom/google/android/gms/social/location/ah;

    iget-object v1, p0, Lcom/google/android/gms/social/location/z;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/social/location/z;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/social/location/ah;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    array-length v1, p1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/social/location/z;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->c(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Lcom/google/android/gms/social/location/DeviceLocationSettings;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/social/location/ah;->a(Lcom/google/android/gms/social/location/DeviceLocationSettings;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    aget-object v1, p1, v4

    sget-object v2, Lcom/google/android/gms/social/location/w;->a:[I

    invoke-virtual {v1}, Lcom/google/android/gms/social/location/af;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    iget-object v2, p0, Lcom/google/android/gms/social/location/z;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v2}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->c(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Lcom/google/android/gms/social/location/DeviceLocationSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->c()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/social/location/ah;->a(Lcom/google/android/gms/social/location/af;Lcom/google/android/gms/common/people/data/Audience;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/google/android/gms/social/location/z;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v2}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->c(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Lcom/google/android/gms/social/location/DeviceLocationSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/social/location/ah;->a(Lcom/google/android/gms/social/location/af;Lcom/google/android/gms/common/people/data/Audience;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 558
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/social/location/z;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->e(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Lcom/google/android/gms/social/location/DeviceLocationSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/social/location/z;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->d(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)V

    iget-object v1, p0, Lcom/google/android/gms/social/location/z;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->e(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Lcom/google/android/gms/social/location/DeviceLocationSettings;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->a(Lcom/google/android/gms/social/location/DeviceLocationSettings;)V

    iget-object v1, p0, Lcom/google/android/gms/social/location/z;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->c(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Lcom/google/android/gms/social/location/DeviceLocationSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->b()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eq v0, v1, :cond_0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/social/location/z;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->f(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/social/location/z;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    const-string v1, "req_pending"

    invoke-static {v0, v1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->a(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/social/location/z;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->a(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/social/location/z;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->g(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Lcom/google/android/gms/social/location/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/social/location/j;->a()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/social/location/z;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    sget v1, Lcom/google/android/gms/p;->qu:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/gms/social/location/z;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    iget-object v1, p0, Lcom/google/android/gms/social/location/z;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->e(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Lcom/google/android/gms/social/location/DeviceLocationSettings;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->a(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;Lcom/google/android/gms/social/location/DeviceLocationSettings;)Lcom/google/android/gms/social/location/DeviceLocationSettings;

    goto :goto_0
.end method

.method protected final onPreExecute()V
    .locals 2

    .prologue
    .line 562
    iget-object v0, p0, Lcom/google/android/gms/social/location/z;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    sget v1, Lcom/google/android/gms/p;->qv:I

    invoke-static {v0, v1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->a(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;I)V

    .line 563
    return-void
.end method
