.class public final Lcom/google/android/gms/drive/a/ad;
.super Lcom/google/android/gms/drive/a/b;
.source "SourceFile"


# instance fields
.field private final d:J

.field private final e:Lcom/google/android/gms/drive/auth/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;JLcom/google/android/gms/drive/auth/a;Lcom/google/android/gms/drive/a/ac;)V
    .locals 6

    .prologue
    .line 49
    sget-object v1, Lcom/google/android/gms/drive/a/e;->b:Lcom/google/android/gms/drive/a/e;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/a/b;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/a/ac;)V

    .line 51
    iput-wide p4, p0, Lcom/google/android/gms/drive/a/ad;->d:J

    .line 52
    invoke-static {p6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/a;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/ad;->e:Lcom/google/android/gms/drive/auth/a;

    .line 53
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 57
    sget-object v0, Lcom/google/android/gms/drive/a/e;->b:Lcom/google/android/gms/drive/a/e;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/drive/a/b;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    .line 58
    const-string v0, "packagingId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/a/ad;->d:J

    .line 59
    const-string v0, "isAuthorized"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/drive/auth/a;->a:Lcom/google/android/gms/drive/auth/a;

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/drive/a/ad;->e:Lcom/google/android/gms/drive/auth/a;

    .line 61
    return-void

    .line 59
    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/auth/a;->b:Lcom/google/android/gms/drive/auth/a;

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;B)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/a/ad;-><init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/a/c;
    .locals 8

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/google/android/gms/drive/a/ad;->d:J

    iget-object v2, p0, Lcom/google/android/gms/drive/a/ad;->e:Lcom/google/android/gms/drive/auth/a;

    invoke-interface {p1, p2, v0, v1, v2}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/ah;JLcom/google/android/gms/drive/auth/a;)Lcom/google/android/gms/drive/auth/a;

    move-result-object v6

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/drive/a/ad;->e:Lcom/google/android/gms/drive/auth/a;

    invoke-virtual {v6, v0}, Lcom/google/android/gms/drive/auth/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    new-instance v0, Lcom/google/android/gms/drive/a/z;

    iget-object v1, p3, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, p3, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    sget-object v3, Lcom/google/android/gms/drive/a/ac;->b:Lcom/google/android/gms/drive/a/ac;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/a/z;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/a/ac;)V

    .line 71
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/a/ad;

    iget-object v1, p3, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, p3, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-object v3, p0, Lcom/google/android/gms/drive/a/b;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    iget-wide v4, p0, Lcom/google/android/gms/drive/a/ad;->d:J

    sget-object v7, Lcom/google/android/gms/drive/a/ac;->b:Lcom/google/android/gms/drive/a/ac;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/drive/a/ad;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;JLcom/google/android/gms/drive/auth/a;Lcom/google/android/gms/drive/a/ac;)V

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/drive/g/aw;)V
    .locals 4

    .prologue
    .line 80
    sget-object v0, Lcom/google/android/gms/drive/a/ae;->a:[I

    iget-object v1, p0, Lcom/google/android/gms/drive/a/ad;->e:Lcom/google/android/gms/drive/auth/a;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/auth/a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 88
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 82
    :pswitch_0
    invoke-virtual {p3}, Lcom/google/android/gms/drive/g/aw;->o()Lcom/google/android/gms/drive/d/f;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/ad;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1, p2}, Lcom/google/android/gms/drive/d/f;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/d/d;

    .line 84
    return-void

    .line 86
    :pswitch_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "No server API to deauthorize files."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 120
    if-ne p0, p1, :cond_1

    .line 127
    :cond_0
    :goto_0
    return v0

    .line 123
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/drive/a/ad;

    if-nez v2, :cond_2

    move v0, v1

    .line 124
    goto :goto_0

    .line 126
    :cond_2
    check-cast p1, Lcom/google/android/gms/drive/a/ad;

    .line 127
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/a/ad;->a(Lcom/google/android/gms/drive/a/a;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/ad;->d:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/a/ad;->d:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/drive/a/ad;->e:Lcom/google/android/gms/drive/auth/a;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/ad;->e:Lcom/google/android/gms/drive/auth/a;

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 94
    invoke-super {p0}, Lcom/google/android/gms/drive/a/b;->f()Lorg/json/JSONObject;

    move-result-object v0

    .line 95
    const-string v1, "packagingId"

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/ad;->d:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 96
    const-string v1, "isAuthorized"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/ad;->e:Lcom/google/android/gms/drive/auth/a;

    sget-object v3, Lcom/google/android/gms/drive/auth/a;->a:Lcom/google/android/gms/drive/auth/a;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/auth/a;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 97
    return-object v0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/drive/a/b;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/EntrySpec;->hashCode()I

    move-result v0

    .line 113
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/ad;->d:J

    iget-wide v4, p0, Lcom/google/android/gms/drive/a/ad;->d:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 114
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/drive/a/ad;->e:Lcom/google/android/gms/drive/auth/a;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/auth/a;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 115
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 102
    const-string v0, "SetAppAuthStateAction [%s, appPackagingId=%s, authState=%s]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/ad;->h()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/google/android/gms/drive/a/ad;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/drive/a/ad;->e:Lcom/google/android/gms/drive/auth/a;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
