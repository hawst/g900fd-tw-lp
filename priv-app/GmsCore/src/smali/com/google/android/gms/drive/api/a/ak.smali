.class public Lcom/google/android/gms/drive/api/a/ak;
.super Lcom/google/android/gms/drive/api/a/c;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/internal/QueryRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/QueryRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p3}, Lcom/google/android/gms/drive/api/a/c;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V

    .line 31
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/ak;->e:Lcom/google/android/gms/drive/internal/QueryRequest;

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/auth/g;)V
    .locals 5

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ak;->e:Lcom/google/android/gms/drive/internal/QueryRequest;

    const-string v1, "Invalid query request: no request"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ak;->e:Lcom/google/android/gms/drive/internal/QueryRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/QueryRequest;->a()Lcom/google/android/gms/drive/query/Query;

    move-result-object v0

    const-string v1, "Invalid query request: no query"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ak;->e:Lcom/google/android/gms/drive/internal/QueryRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/QueryRequest;->a()Lcom/google/android/gms/drive/query/Query;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/ak;->a:Lcom/google/android/gms/drive/api/d;

    new-instance v2, Lcom/google/android/gms/drive/api/a/al;

    iget-object v3, p0, Lcom/google/android/gms/drive/api/a/ak;->b:Lcom/google/android/gms/drive/internal/ca;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/drive/api/a/al;-><init>(Lcom/google/android/gms/drive/internal/ca;B)V

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/drive/api/d;->a(Lcom/google/android/gms/drive/query/Query;Lcom/google/android/gms/drive/api/a/al;)V

    .line 44
    return-void
.end method
