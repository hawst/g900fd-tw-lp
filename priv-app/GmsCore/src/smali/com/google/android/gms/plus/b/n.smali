.class public final Lcom/google/android/gms/plus/b/n;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/plus/service/v1whitelisted/j;


# instance fields
.field private final b:Lcom/google/android/gms/plus/service/v1whitelisted/a;

.field private final c:Lcom/google/android/gms/plus/service/v1whitelisted/b;

.field private final d:Lcom/google/android/gms/plus/service/v1whitelisted/c;

.field private final e:Lcom/google/android/gms/plus/service/v1whitelisted/f;

.field private final f:Lcom/google/android/gms/plus/service/v1whitelisted/h;

.field private final g:Lcom/google/android/gms/plus/service/v1whitelisted/i;

.field private final h:Lcom/google/android/gms/plus/service/v1whitelisted/k;

.field private final i:Lcom/google/android/gms/plus/service/v1whitelisted/l;

.field private final j:Lcom/google/android/gms/common/server/n;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 113
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/j;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/j;-><init>()V

    const-string v1, "id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/j;

    const-string v1, "displayName"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/j;

    sput-object v0, Lcom/google/android/gms/plus/b/n;->a:Lcom/google/android/gms/plus/service/v1whitelisted/j;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/server/n;)V
    .locals 1

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    iput-object p1, p0, Lcom/google/android/gms/plus/b/n;->j:Lcom/google/android/gms/common/server/n;

    .line 145
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/plus/service/v1whitelisted/a;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/b/n;->b:Lcom/google/android/gms/plus/service/v1whitelisted/a;

    .line 146
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/b;

    invoke-direct {v0, p1}, Lcom/google/android/gms/plus/service/v1whitelisted/b;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/b/n;->c:Lcom/google/android/gms/plus/service/v1whitelisted/b;

    .line 147
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/f;

    invoke-direct {v0, p1}, Lcom/google/android/gms/plus/service/v1whitelisted/f;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/b/n;->e:Lcom/google/android/gms/plus/service/v1whitelisted/f;

    .line 148
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/k;

    invoke-direct {v0, p1}, Lcom/google/android/gms/plus/service/v1whitelisted/k;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/b/n;->h:Lcom/google/android/gms/plus/service/v1whitelisted/k;

    .line 149
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/c;

    invoke-direct {v0, p1}, Lcom/google/android/gms/plus/service/v1whitelisted/c;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/b/n;->d:Lcom/google/android/gms/plus/service/v1whitelisted/c;

    .line 150
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/h;

    invoke-direct {v0, p1}, Lcom/google/android/gms/plus/service/v1whitelisted/h;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/b/n;->f:Lcom/google/android/gms/plus/service/v1whitelisted/h;

    .line 152
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/i;

    invoke-direct {v0, p1}, Lcom/google/android/gms/plus/service/v1whitelisted/i;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/b/n;->g:Lcom/google/android/gms/plus/service/v1whitelisted/i;

    .line 153
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/l;

    invoke-direct {v0, p1}, Lcom/google/android/gms/plus/service/v1whitelisted/l;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/b/n;->i:Lcom/google/android/gms/plus/service/v1whitelisted/l;

    .line 154
    return-void
.end method

.method private static a(Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 212
    new-instance v1, Landroid/content/ContentValues;

    sget-object v0, Lcom/google/android/gms/plus/data/a/a;->a:[Ljava/lang/String;

    array-length v0, v0

    invoke-direct {v1, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 213
    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lcom/google/android/gms/plus/data/a/a;->a:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 214
    sget-object v2, Lcom/google/android/gms/plus/data/a/a;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 213
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 217
    :cond_0
    const-string v0, "url"

    invoke-virtual {v1, v0, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    return-object v1
.end method

.method private static a(Landroid/content/pm/PackageManager;Ljava/security/MessageDigest;Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Landroid/content/pm/ApplicationInfo;
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 393
    .line 394
    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->b()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 395
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v2, v1

    :goto_0
    move v6, v3

    move-object v4, v5

    .line 396
    :goto_1
    if-ge v6, v2, :cond_0

    .line 397
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;

    .line 398
    const-string v3, "android"

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 401
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->c()Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x0

    invoke-virtual {p0, v3, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 403
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->c()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x40

    invoke-virtual {p0, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v7

    .line 405
    iget-object v8, v7, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v8, :cond_4

    iget-object v8, v7, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v8, v8

    if-lez v8, :cond_4

    .line 407
    iget-object v7, v7, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    invoke-virtual {v7}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v7

    .line 409
    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/google/android/gms/common/util/e;->a([BZ)Ljava/lang/String;

    move-result-object v7

    .line 410
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_4

    .line 411
    if-eqz v4, :cond_2

    move-object v4, v5

    .line 425
    :cond_0
    return-object v4

    :cond_1
    move v2, v3

    .line 395
    goto :goto_0

    :cond_2
    move-object v1, v3

    :goto_2
    move-object v4, v1

    .line 396
    :cond_3
    :goto_3
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_3

    :cond_4
    move-object v1, v4

    goto :goto_2
.end method

.method private static a(Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ae;
    .locals 2

    .prologue
    .line 189
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;-><init>()V

    .line 192
    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/an;

    .line 193
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 194
    invoke-static {p1}, Lcom/google/android/gms/plus/sharebox/a/a;->a(Landroid/os/Bundle;)Lcom/google/android/gms/plus/sharebox/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/sharebox/a/a;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/an;)Lcom/google/android/gms/plus/service/v1whitelisted/models/an;

    .line 198
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 200
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/ai;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 202
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/au;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/au;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/au;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/au;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/ag;

    move-result-object v0

    .line 205
    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ag;)Lcom/google/android/gms/plus/service/v1whitelisted/models/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/ae;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/ContentValues;Lcom/google/android/gms/plus/service/v1whitelisted/models/ae;)V
    .locals 3

    .prologue
    .line 246
    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ae;->e()Lcom/google/android/gms/plus/service/v1whitelisted/models/ag;

    move-result-object v0

    .line 247
    if-nez v0, :cond_0

    .line 248
    new-instance v0, Lcom/android/volley/ac;

    const-string v1, "Link preview requires object."

    invoke-direct {v0, v1}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 251
    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ag;->d()Ljava/util/List;

    move-result-object v0

    .line 252
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 253
    :cond_1
    new-instance v0, Lcom/android/volley/ac;

    const-string v1, "Link preview requires object.attachments[]."

    invoke-direct {v0, v1}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :cond_2
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity;

    .line 259
    if-nez v0, :cond_3

    .line 260
    new-instance v0, Lcom/android/volley/ac;

    const-string v1, "Link preview requires attachments."

    invoke-direct {v0, v1}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 263
    :cond_3
    const-string v1, "title"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const-string v1, "type"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const-string v1, "description"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity;->e()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 269
    const-string v1, "callToActionDisplayName"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity;->d()Lcom/google/android/gms/plus/service/v1whitelisted/models/aj;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aj;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity;->h()Lcom/google/android/gms/plus/service/v1whitelisted/models/aq;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ImageEntity;

    .line 276
    if-eqz v0, :cond_5

    .line 277
    const-string v1, "thumbnailUrl"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ImageEntity;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    :cond_5
    return-void
.end method

.method private static a(Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 1

    .prologue
    .line 283
    invoke-static {}, Lcom/google/android/gms/plus/b/f;->a()Lcom/google/android/gms/plus/b/f;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/gms/plus/b/f;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 284
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/model/posts/Post;)Landroid/os/Bundle;
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 434
    invoke-virtual {p3}, Lcom/google/android/gms/plus/model/posts/Post;->q()Ljava/lang/String;

    move-result-object v2

    .line 435
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "me"

    .line 438
    :cond_0
    invoke-virtual {p3}, Lcom/google/android/gms/plus/model/posts/Post;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 439
    invoke-virtual {p3}, Lcom/google/android/gms/plus/model/posts/Post;->e()Ljava/lang/String;

    move-result-object v10

    .line 440
    invoke-virtual {p3}, Lcom/google/android/gms/plus/model/posts/Post;->k()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v10, v0}, Lcom/google/android/gms/plus/b/n;->a(Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ae;

    move-result-object v9

    .line 443
    iget-object v0, p0, Lcom/google/android/gms/plus/b/n;->c:Lcom/google/android/gms/plus/service/v1whitelisted/b;

    invoke-virtual {p3}, Lcom/google/android/gms/plus/model/posts/Post;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/google/android/gms/plus/e/d;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    check-cast v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;

    move-object v1, p2

    move-object v8, v7

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/gms/plus/service/v1whitelisted/b;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;

    move-result-object v0

    .line 455
    invoke-static {v10}, Lcom/google/android/gms/plus/b/n;->a(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v1

    .line 456
    invoke-static {v1, v0}, Lcom/google/android/gms/plus/b/n;->a(Landroid/content/ContentValues;Lcom/google/android/gms/plus/service/v1whitelisted/models/ae;)V

    .line 457
    invoke-static {v10, v1}, Lcom/google/android/gms/plus/b/n;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 458
    new-instance v0, Lcom/google/android/gms/plus/data/a/a;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/data/a/a;-><init>(Landroid/content/ContentValues;)V

    invoke-virtual {v0}, Lcom/google/android/gms/plus/data/a/a;->f()Landroid/os/Bundle;

    move-result-object v7

    .line 460
    :cond_1
    return-object v7
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;)Landroid/util/Pair;
    .locals 11

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/gms/plus/b/n;->d:Lcom/google/android/gms/plus/service/v1whitelisted/c;

    const-string v2, "me"

    invoke-static {p1}, Lcom/google/android/gms/plus/e/d;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v1, p2

    move-object v3, p3

    move-object/from16 v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/service/v1whitelisted/c;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/Applications;

    move-result-object v3

    .line 339
    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Applications;->b()Ljava/util/List;

    move-result-object v4

    .line 340
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    .line 341
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 344
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 345
    const-string v0, "SHA1"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->b(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v8

    .line 346
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_3

    .line 347
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    .line 348
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 349
    const-string v1, "display_name"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->c()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    const-string v1, "application_id"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->e()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    const-string v10, "icon_url"

    if-nez v0, :cond_1

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v9, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    const-string v1, "is_aspen"

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v9, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 354
    invoke-static {v7, v8, v0}, Lcom/google/android/gms/plus/b/n;->a(Landroid/content/pm/PackageManager;Ljava/security/MessageDigest;Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 355
    if-eqz v0, :cond_0

    .line 357
    const-string v1, "display_name"

    invoke-virtual {v7, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    const-string v1, "application_info"

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/model/apps/c;->a(Landroid/content/pm/ApplicationInfo;)[B

    move-result-object v0

    invoke-virtual {v9, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 362
    :cond_0
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 346
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 351
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->d()Lcom/google/android/gms/plus/service/v1whitelisted/models/Application$Icon;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application$Icon;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 366
    :cond_3
    new-instance v0, Lcom/google/android/gms/plus/b/o;

    invoke-direct {v0}, Lcom/google/android/gms/plus/b/o;-><init>()V

    invoke-static {v6, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 369
    sget-object v0, Lcom/google/android/gms/plus/internal/model/apps/c;->a:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lcom/google/android/gms/common/data/m;

    move-result-object v2

    .line 370
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v5, :cond_4

    .line 371
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/data/m;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/common/data/m;

    .line 370
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 374
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/data/m;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Applications;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/plus/data/a/a;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 166
    invoke-static {p3, v3}, Lcom/google/android/gms/plus/b/n;->a(Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ae;

    move-result-object v9

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/plus/b/n;->c:Lcom/google/android/gms/plus/service/v1whitelisted/b;

    const-string v2, "me"

    invoke-static {p1}, Lcom/google/android/gms/plus/e/d;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    check-cast v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;

    move-object v1, p2

    move-object v7, v3

    move-object v8, v3

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/gms/plus/service/v1whitelisted/b;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;

    move-result-object v0

    .line 178
    invoke-static {p3}, Lcom/google/android/gms/plus/b/n;->a(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v1

    .line 179
    invoke-static {v1, v0}, Lcom/google/android/gms/plus/b/n;->a(Landroid/content/ContentValues;Lcom/google/android/gms/plus/service/v1whitelisted/models/ae;)V

    .line 180
    invoke-static {p3, v1}, Lcom/google/android/gms/plus/b/n;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 181
    new-instance v0, Lcom/google/android/gms/plus/data/a/a;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/data/a/a;-><init>(Landroid/content/ContentValues;)V

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;
    .locals 14

    .prologue
    .line 581
    iget-object v0, p0, Lcom/google/android/gms/plus/b/n;->j:Lcom/google/android/gms/common/server/n;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/n;->c()V

    .line 582
    invoke-static {p1}, Lcom/google/android/gms/plus/e/d;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 584
    new-instance v13, Lcom/google/android/gms/common/util/f;

    invoke-direct {v13}, Lcom/google/android/gms/common/util/f;-><init>()V

    .line 585
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->c()Ljava/lang/String;

    move-result-object v2

    .line 588
    invoke-virtual {v13}, Lcom/google/android/gms/common/util/f;->a()Lcom/google/android/gms/common/util/f;

    move-result-object v5

    .line 589
    iget-object v0, p0, Lcom/google/android/gms/plus/b/n;->b:Lcom/google/android/gms/plus/service/v1whitelisted/a;

    const-string v3, "shared"

    move-object/from16 v1, p2

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 593
    invoke-virtual {v13}, Lcom/google/android/gms/common/util/f;->a()Lcom/google/android/gms/common/util/f;

    move-result-object v11

    .line 594
    iget-object v6, p0, Lcom/google/android/gms/plus/b/n;->b:Lcom/google/android/gms/plus/service/v1whitelisted/a;

    const-string v9, "visible"

    move-object/from16 v7, p2

    move-object v8, v2

    move-object v10, v4

    move-object v12, v11

    invoke-virtual/range {v6 .. v12}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 599
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/b/n;->j:Lcom/google/android/gms/common/server/n;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/n;->e()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 602
    :goto_0
    invoke-virtual {v13}, Lcom/google/android/gms/common/util/f;->b()V

    .line 603
    invoke-virtual {v5}, Lcom/google/android/gms/common/util/f;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v11}, Lcom/google/android/gms/common/util/f;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 604
    :cond_0
    new-instance v0, Lcom/android/volley/ac;

    const-string v1, "Interrupted."

    invoke-direct {v0, v1}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 608
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Lcom/google/android/gms/common/util/f;->a(I)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    .line 609
    invoke-static {v0}, Lcom/google/android/gms/common/people/data/c;->b(Lcom/google/android/gms/plus/service/v1whitelisted/models/t;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v5

    .line 612
    const/4 v1, 0x0

    invoke-virtual {v11, v1}, Lcom/google/android/gms/common/util/f;->a(I)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    .line 613
    const/4 v4, 0x0

    .line 614
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->g()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->f()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v6, 0x1

    if-ne v3, v6, :cond_3

    .line 615
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->f()Ljava/util/List;

    move-result-object v3

    const/4 v6, 0x0

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;

    .line 616
    const-string v6, "allCircles"

    invoke-interface {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 617
    const/4 v3, 0x1

    .line 620
    :goto_1
    if-eqz v3, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 625
    :goto_2
    new-instance v4, Lcom/google/android/gms/plus/internal/model/apps/a;

    invoke-direct {v4}, Lcom/google/android/gms/plus/internal/model/apps/a;-><init>()V

    iput-object v5, v4, Lcom/google/android/gms/plus/internal/model/apps/a;->b:Lcom/google/android/gms/common/people/data/Audience;

    iput-object v1, v4, Lcom/google/android/gms/plus/internal/model/apps/a;->c:Ljava/util/ArrayList;

    iput-boolean v3, v4, Lcom/google/android/gms/plus/internal/model/apps/a;->d:Z

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/plus/internal/model/apps/a;->a:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/internal/model/apps/a;->a()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v0

    .line 632
    new-instance v1, Lcom/google/android/gms/plus/internal/model/acls/b;

    invoke-direct {v1}, Lcom/google/android/gms/plus/internal/model/acls/b;-><init>()V

    iput-object v0, v1, Lcom/google/android/gms/plus/internal/model/acls/b;->b:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    iput-object v2, v1, Lcom/google/android/gms/plus/internal/model/acls/b;->a:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/acls/b;->a()Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;

    move-result-object v0

    return-object v0

    .line 620
    :cond_2
    invoke-static {v1}, Lcom/google/android/gms/common/people/data/c;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/t;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_2

    :catch_0
    move-exception v0

    goto/16 :goto_0

    :cond_3
    move v3, v4

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;)Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;
    .locals 15

    .prologue
    .line 665
    iget-object v0, p0, Lcom/google/android/gms/plus/b/n;->j:Lcom/google/android/gms/common/server/n;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/n;->c()V

    .line 666
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/plus/e/d;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 668
    new-instance v13, Lcom/google/android/gms/common/util/f;

    invoke-direct {v13}, Lcom/google/android/gms/common/util/f;-><init>()V

    .line 671
    invoke-virtual {v13}, Lcom/google/android/gms/common/util/f;->a()Lcom/google/android/gms/common/util/f;

    move-result-object v5

    .line 672
    iget-object v0, p0, Lcom/google/android/gms/plus/b/n;->b:Lcom/google/android/gms/plus/service/v1whitelisted/a;

    const-string v3, "shared"

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 676
    invoke-virtual {v13}, Lcom/google/android/gms/common/util/f;->a()Lcom/google/android/gms/common/util/f;

    move-result-object v11

    .line 679
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->h()[Ljava/lang/String;

    .line 680
    const-string v9, "visible"

    .line 681
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 682
    const-string v9, "connected"

    .line 684
    :cond_0
    iget-object v6, p0, Lcom/google/android/gms/plus/b/n;->b:Lcom/google/android/gms/plus/service/v1whitelisted/a;

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object v10, v4

    move-object v12, v11

    invoke-virtual/range {v6 .. v12}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 689
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/b/n;->j:Lcom/google/android/gms/common/server/n;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/n;->e()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 692
    :goto_0
    invoke-virtual {v13}, Lcom/google/android/gms/common/util/f;->b()V

    .line 693
    invoke-virtual {v5}, Lcom/google/android/gms/common/util/f;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v11}, Lcom/google/android/gms/common/util/f;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 694
    :cond_1
    new-instance v0, Lcom/android/volley/ac;

    const-string v1, "Interrupted."

    invoke-direct {v0, v1}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 698
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Lcom/google/android/gms/common/util/f;->a(I)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    .line 699
    invoke-static {v0}, Lcom/google/android/gms/common/people/data/c;->b(Lcom/google/android/gms/plus/service/v1whitelisted/models/t;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v6

    .line 702
    const/4 v1, 0x0

    invoke-virtual {v11, v1}, Lcom/google/android/gms/common/util/f;->a(I)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    .line 703
    const/4 v4, 0x0

    .line 704
    const/4 v3, 0x0

    .line 705
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->m()Z

    move-result v7

    .line 706
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->n()Z

    move-result v8

    .line 707
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/a/a;->a(Ljava/lang/String;)I

    move-result v9

    .line 708
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->d()Ljava/lang/String;

    move-result-object v10

    .line 709
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->l()Ljava/lang/String;

    move-result-object v11

    .line 711
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->g()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 712
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->f()Ljava/util/List;

    move-result-object v12

    .line 713
    const/4 v2, 0x0

    move v5, v4

    move v4, v3

    move v3, v2

    :goto_1
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_5

    .line 714
    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;

    .line 715
    invoke-interface {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->f()Ljava/lang/String;

    move-result-object v13

    const/4 v2, -0x1

    invoke-virtual {v13}, Ljava/lang/String;->hashCode()I

    move-result v14

    sparse-switch v14, :sswitch_data_0

    :cond_3
    :goto_2
    packed-switch v2, :pswitch_data_0

    .line 713
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 715
    :sswitch_0
    const-string v14, "allCircles"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    const/4 v2, 0x0

    goto :goto_2

    :sswitch_1
    const-string v14, "allContacts"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    const/4 v2, 0x1

    goto :goto_2

    .line 717
    :pswitch_0
    const/4 v5, 0x1

    .line 718
    goto :goto_3

    .line 720
    :pswitch_1
    const/4 v4, 0x1

    goto :goto_3

    :cond_4
    move v5, v4

    move v4, v3

    .line 726
    :cond_5
    if-eqz v5, :cond_6

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 731
    :goto_4
    new-instance v2, Lcom/google/android/gms/plus/internal/model/apps/a;

    invoke-direct {v2}, Lcom/google/android/gms/plus/internal/model/apps/a;-><init>()V

    iput-object v10, v2, Lcom/google/android/gms/plus/internal/model/apps/a;->i:Ljava/lang/String;

    iput-object v11, v2, Lcom/google/android/gms/plus/internal/model/apps/a;->j:Ljava/lang/String;

    iput-object v6, v2, Lcom/google/android/gms/plus/internal/model/apps/a;->b:Lcom/google/android/gms/common/people/data/Audience;

    iput-object v1, v2, Lcom/google/android/gms/plus/internal/model/apps/a;->c:Ljava/util/ArrayList;

    iput-boolean v5, v2, Lcom/google/android/gms/plus/internal/model/apps/a;->d:Z

    iput-boolean v4, v2, Lcom/google/android/gms/plus/internal/model/apps/a;->e:Z

    iput-boolean v7, v2, Lcom/google/android/gms/plus/internal/model/apps/a;->f:Z

    iput-boolean v8, v2, Lcom/google/android/gms/plus/internal/model/apps/a;->g:Z

    iput v9, v2, Lcom/google/android/gms/plus/internal/model/apps/a;->h:I

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/plus/internal/model/apps/a;->a:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/model/apps/a;->a()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v0

    return-object v0

    .line 726
    :cond_6
    invoke-static {v1}, Lcom/google/android/gms/common/people/data/c;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/t;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_4

    :catch_0
    move-exception v0

    goto/16 :goto_0

    .line 715
    :sswitch_data_0
    .sparse-switch
        -0x7e6aeb3e -> :sswitch_0
        -0x1775f2ec -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;Ljava/util/List;ZZZZ)Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;
    .locals 9

    .prologue
    .line 757
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 760
    if-eqz p8, :cond_0

    .line 761
    if-eqz p6, :cond_4

    .line 762
    new-instance v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;

    invoke-direct {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;-><init>()V

    const-string v3, "allCircles"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;->b(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 771
    :cond_0
    :goto_0
    const-string v4, "visible"

    .line 772
    new-instance v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;

    invoke-direct {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;-><init>()V

    .line 773
    invoke-virtual {p4}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->m()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 774
    const-string v4, "connected"

    .line 777
    if-eqz p7, :cond_1

    if-eqz p9, :cond_1

    .line 778
    new-instance v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;

    invoke-direct {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;-><init>()V

    const-string v5, "allContacts"

    invoke-virtual {v3, v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;->b(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 781
    :cond_1
    move/from16 v0, p8

    iput-boolean v0, v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->a:Z

    iget-object v3, v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->c:Ljava/util/Set;

    const/16 v5, 0xa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 782
    move/from16 v0, p9

    iput-boolean v0, v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->b:Z

    iget-object v3, v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->c:Ljava/util/Set;

    const/16 v5, 0xb

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 784
    :cond_2
    invoke-virtual {v2, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/u;

    .line 786
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/t;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    .line 787
    invoke-static {p1}, Lcom/google/android/gms/plus/e/d;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 789
    iget-object v1, p0, Lcom/google/android/gms/plus/b/n;->b:Lcom/google/android/gms/plus/service/v1whitelisted/a;

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    move-result-object v5

    .line 792
    const/4 v3, 0x0

    .line 793
    const/4 v2, 0x0

    .line 795
    invoke-virtual {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->g()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 796
    invoke-virtual {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->f()Ljava/util/List;

    move-result-object v6

    .line 797
    const/4 v1, 0x0

    move v4, v3

    move v3, v2

    move v2, v1

    :goto_1
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_6

    .line 798
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;

    .line 799
    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->f()Ljava/lang/String;

    move-result-object v7

    const/4 v1, -0x1

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_3
    :goto_2
    packed-switch v1, :pswitch_data_0

    .line 797
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 765
    :cond_4
    invoke-static {p5}, Lcom/google/android/gms/common/people/data/c;->a(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0

    .line 799
    :sswitch_0
    const-string v8, "allCircles"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v1, 0x0

    goto :goto_2

    :sswitch_1
    const-string v8, "allContacts"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v1, 0x1

    goto :goto_2

    .line 801
    :pswitch_0
    const/4 v4, 0x1

    .line 802
    goto :goto_3

    .line 804
    :pswitch_1
    const/4 v3, 0x1

    goto :goto_3

    :cond_5
    move v4, v3

    move v3, v2

    .line 810
    :cond_6
    if-eqz v4, :cond_7

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 814
    :goto_4
    new-instance v2, Lcom/google/android/gms/plus/internal/model/apps/a;

    invoke-direct {v2}, Lcom/google/android/gms/plus/internal/model/apps/a;-><init>()V

    invoke-virtual {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->d()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/google/android/gms/plus/internal/model/apps/a;->i:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->l()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/google/android/gms/plus/internal/model/apps/a;->j:Ljava/lang/String;

    iput-object v1, v2, Lcom/google/android/gms/plus/internal/model/apps/a;->c:Ljava/util/ArrayList;

    iput-boolean v4, v2, Lcom/google/android/gms/plus/internal/model/apps/a;->d:Z

    iput-boolean v3, v2, Lcom/google/android/gms/plus/internal/model/apps/a;->e:Z

    invoke-virtual {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->m()Z

    move-result v1

    iput-boolean v1, v2, Lcom/google/android/gms/plus/internal/model/apps/a;->f:Z

    invoke-virtual {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->n()Z

    move-result v1

    iput-boolean v1, v2, Lcom/google/android/gms/plus/internal/model/apps/a;->g:Z

    invoke-virtual {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/a/a;->a(Ljava/lang/String;)I

    move-result v1

    iput v1, v2, Lcom/google/android/gms/plus/internal/model/apps/a;->h:I

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/model/apps/a;->a()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v1

    return-object v1

    .line 810
    :cond_7
    invoke-static {v5}, Lcom/google/android/gms/common/people/data/c;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/t;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_4

    .line 799
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7e6aeb3e -> :sswitch_0
        -0x1775f2ec -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/model/posts/Comment;)Lcom/google/android/gms/plus/model/posts/Comment;
    .locals 16

    .prologue
    .line 288
    new-instance v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/cw;

    invoke-direct {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cw;-><init>()V

    .line 289
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/plus/model/posts/Comment;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 290
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/plus/model/posts/Comment;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/cw;->a:Ljava/lang/String;

    iget-object v1, v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/cw;->b:Ljava/util/Set;

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 293
    :cond_0
    new-instance v10, Lcom/google/android/gms/plus/service/v1whitelisted/models/ct;

    invoke-direct {v10}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ct;-><init>()V

    .line 294
    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$ObjectEntity;

    iget-object v3, v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/cw;->b:Ljava/util/Set;

    iget-object v2, v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/cw;->a:Ljava/lang/String;

    invoke-direct {v1, v3, v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$ObjectEntity;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$ObjectEntity;

    iput-object v1, v10, Lcom/google/android/gms/plus/service/v1whitelisted/models/ct;->d:Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$ObjectEntity;

    iget-object v1, v10, Lcom/google/android/gms/plus/service/v1whitelisted/models/ct;->i:Ljava/util/Set;

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 296
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/plus/model/posts/Comment;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "me"

    move-object v11, v1

    .line 299
    :goto_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gms/plus/b/n;->e:Lcom/google/android/gms/plus/service/v1whitelisted/f;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/plus/model/posts/Comment;->b()Ljava/lang/String;

    move-result-object v13

    const-string v14, "html"

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/plus/model/posts/Comment;->e()Ljava/lang/String;

    move-result-object v15

    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity;

    iget-object v2, v10, Lcom/google/android/gms/plus/service/v1whitelisted/models/ct;->i:Ljava/util/Set;

    iget-object v3, v10, Lcom/google/android/gms/plus/service/v1whitelisted/models/ct;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$ActorEntity;

    iget-object v4, v10, Lcom/google/android/gms/plus/service/v1whitelisted/models/ct;->b:Ljava/lang/String;

    iget-object v5, v10, Lcom/google/android/gms/plus/service/v1whitelisted/models/ct;->c:Ljava/util/List;

    iget-object v6, v10, Lcom/google/android/gms/plus/service/v1whitelisted/models/ct;->d:Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$ObjectEntity;

    iget-object v7, v10, Lcom/google/android/gms/plus/service/v1whitelisted/models/ct;->e:Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$PlusonersEntity;

    iget-object v8, v10, Lcom/google/android/gms/plus/service/v1whitelisted/models/ct;->f:Ljava/lang/String;

    iget-object v9, v10, Lcom/google/android/gms/plus/service/v1whitelisted/models/ct;->g:Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$StatusForViewerEntity;

    iget-object v10, v10, Lcom/google/android/gms/plus/service/v1whitelisted/models/ct;->h:Ljava/lang/String;

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity;-><init>(Ljava/util/Set;Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$ActorEntity;Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$ObjectEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$PlusonersEntity;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$StatusForViewerEntity;Ljava/lang/String;)V

    move-object v5, v1

    check-cast v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity;

    const-string v1, "activities/%1$s/comments"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v13}, Lcom/google/android/gms/plus/service/v1whitelisted/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "contentFormat"

    invoke-static {v14}, Lcom/google/android/gms/plus/service/v1whitelisted/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v15, :cond_1

    const-string v1, "contextType"

    invoke-static {v15}, Lcom/google/android/gms/plus/service/v1whitelisted/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/google/android/gms/plus/service/v1whitelisted/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_1
    if-eqz v11, :cond_2

    const-string v1, "onBehalfOf"

    invoke-static {v11}, Lcom/google/android/gms/plus/service/v1whitelisted/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/google/android/gms/plus/service/v1whitelisted/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_2
    iget-object v1, v12, Lcom/google/android/gms/plus/service/v1whitelisted/f;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x1

    const-class v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity;

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity;

    .line 307
    if-eqz v1, :cond_4

    .line 308
    new-instance v7, Lcom/google/android/gms/plus/model/posts/a;

    move-object/from16 v0, p2

    invoke-direct {v7, v0}, Lcom/google/android/gms/plus/model/posts/a;-><init>(Lcom/google/android/gms/plus/model/posts/Comment;)V

    .line 309
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/google/android/gms/plus/model/posts/a;->b:Ljava/lang/String;

    new-instance v1, Lcom/google/android/gms/plus/model/posts/Comment;

    const/4 v2, 0x1

    iget-object v3, v7, Lcom/google/android/gms/plus/model/posts/a;->b:Ljava/lang/String;

    iget-object v4, v7, Lcom/google/android/gms/plus/model/posts/a;->a:Lcom/google/android/gms/plus/model/posts/Comment;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/model/posts/Comment;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v7, Lcom/google/android/gms/plus/model/posts/a;->a:Lcom/google/android/gms/plus/model/posts/Comment;

    invoke-virtual {v5}, Lcom/google/android/gms/plus/model/posts/Comment;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v7, Lcom/google/android/gms/plus/model/posts/a;->a:Lcom/google/android/gms/plus/model/posts/Comment;

    invoke-virtual {v6}, Lcom/google/android/gms/plus/model/posts/Comment;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v7, Lcom/google/android/gms/plus/model/posts/a;->a:Lcom/google/android/gms/plus/model/posts/Comment;

    invoke-virtual {v7}, Lcom/google/android/gms/plus/model/posts/Comment;->f()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/plus/model/posts/Comment;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    :goto_1
    return-object v1

    .line 296
    :cond_3
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/plus/model/posts/Comment;->f()Ljava/lang/String;

    move-result-object v1

    move-object v11, v1

    goto/16 :goto_0

    .line 312
    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;
    .locals 10

    .prologue
    .line 318
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "me"

    .line 319
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/b/n;->c:Lcom/google/android/gms/plus/service/v1whitelisted/b;

    invoke-static {p1}, Lcom/google/android/gms/plus/e/d;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    move-object v1, p2

    move-object v3, p4

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/gms/plus/service/v1whitelisted/b;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v2, p3

    .line 318
    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;
    .locals 8

    .prologue
    .line 854
    if-nez p5, :cond_5

    const/4 v0, 0x0

    .line 855
    :goto_0
    invoke-static {p1}, Lcom/google/android/gms/plus/e/d;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 856
    iget-object v2, p0, Lcom/google/android/gms/plus/b/n;->f:Lcom/google/android/gms/plus/service/v1whitelisted/h;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v3, "people/%1$s/moments/%2$s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p7}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static/range {p8 .. p8}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_0

    const-string v5, "language"

    invoke-static {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v5, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz v4, :cond_1

    const-string v1, "maxResults"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v1, v4}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz p4, :cond_2

    const-string v1, "pageToken"

    invoke-static {p4}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v1, v4}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    if-eqz v0, :cond_3

    const-string v1, "targetUrl"

    invoke-static {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v1, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_3
    if-eqz p6, :cond_4

    const-string v0, "type"

    invoke-static {p6}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_4
    iget-object v0, v2, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    .line 867
    return-object v0

    .line 854
    :cond_5
    invoke-virtual {p5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;IILjava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 951
    packed-switch p2, :pswitch_data_0

    .line 956
    const-string v6, "alphabetical"

    .line 962
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/plus/b/n;->g:Lcom/google/android/gms/plus/service/v1whitelisted/i;

    const-string v0, "me"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "people/%1$s/peopleForSharing"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v2, :cond_0

    const-string v3, "maxResults"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v3, v2}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const-string v2, "orderBy"

    invoke-static {v6}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz p4, :cond_1

    const-string v0, "pageToken"

    invoke-static {p4}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    iget-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 983
    :goto_1
    return-object v0

    .line 953
    :pswitch_0
    const-string v6, "best"

    goto :goto_0

    .line 970
    :catch_0
    move-exception v0

    .line 971
    const-string v1, "PlusWhitelistedAgent"

    const-string v2, "listForSharingBlocking VolleyError"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 972
    iget-object v0, p0, Lcom/google/android/gms/plus/b/n;->g:Lcom/google/android/gms/plus/service/v1whitelisted/i;

    const-string v2, "me"

    const-string v3, "circled"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v1, p1

    move-object v4, v7

    move-object v7, p4

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    move-result-object v0

    goto :goto_1

    .line 951
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;
    .locals 6

    .prologue
    .line 917
    invoke-static {p1}, Lcom/google/android/gms/plus/e/d;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 918
    iget-object v2, p0, Lcom/google/android/gms/plus/b/n;->h:Lcom/google/android/gms/plus/service/v1whitelisted/k;

    const-string v0, "androidGms"

    const-string v3, "me"

    const-string v4, "rpc/upgradeAccount"

    const-string v5, "client"

    invoke-static {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v5, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz p3, :cond_0

    const-string v4, "gpsrc"

    invoke-static {p3}, Lcom/google/android/gms/plus/service/v1whitelisted/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v4, v5}, Lcom/google/android/gms/plus/service/v1whitelisted/k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-eqz v1, :cond_1

    const-string v4, "language"

    invoke-static {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    const-string v1, "userId"

    invoke-static {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v2, Lcom/google/android/gms/plus/service/v1whitelisted/k;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    move-object v1, p2

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 6

    .prologue
    .line 747
    invoke-static {p4}, Lcom/google/android/gms/common/people/data/c;->a(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/android/gms/plus/service/v1whitelisted/models/t;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    .line 748
    invoke-static {p1}, Lcom/google/android/gms/plus/e/d;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 749
    iget-object v0, p0, Lcom/google/android/gms/plus/b/n;->b:Lcom/google/android/gms/plus/service/v1whitelisted/a;

    const-string v3, "shared"

    move-object v1, p2

    move-object v2, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    .line 751
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)V
    .locals 4

    .prologue
    .line 640
    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->d()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/people/data/c;->a(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/android/gms/plus/service/v1whitelisted/models/t;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    .line 642
    iget-object v1, p0, Lcom/google/android/gms/plus/b/n;->b:Lcom/google/android/gms/plus/service/v1whitelisted/a;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->c()Ljava/lang/String;

    move-result-object v2

    const-string v3, "shared"

    invoke-virtual {v1, p1, v2, v3, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    .line 645
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 847
    iget-object v0, p0, Lcom/google/android/gms/plus/b/n;->f:Lcom/google/android/gms/plus/service/v1whitelisted/h;

    const-string v1, "moments/%1$s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v2, v1, v3}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    .line 848
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 835
    if-eqz p3, :cond_0

    .line 836
    iget-object v0, p0, Lcom/google/android/gms/plus/b/n;->f:Lcom/google/android/gms/plus/service/v1whitelisted/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    .line 839
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/b/n;->d:Lcom/google/android/gms/plus/service/v1whitelisted/c;

    const-string v1, "applications/%1$s/disconnect"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/plus/service/v1whitelisted/c;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v5, v1, v2}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    .line 840
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/List;IZLjava/lang/String;)V
    .locals 8

    .prologue
    .line 232
    if-eqz p4, :cond_1

    const-string v0, "10"

    .line 233
    :goto_0
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    .line 234
    new-instance v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/cp;

    invoke-direct {v6}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cp;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/cp;->a:Ljava/lang/String;

    iget-object v1, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/cp;->f:Ljava/util/Set;

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput-object p2, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/cp;->b:Ljava/util/List;

    iget-object v1, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/cp;->f:Ljava/util/Set;

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput-object v0, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/cp;->c:Ljava/lang/String;

    iget-object v0, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/cp;->f:Ljava/util/Set;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/cp;->d:Ljava/lang/String;

    iget-object v0, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/cp;->f:Ljava/util/Set;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput-wide v2, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/cp;->e:J

    iget-object v0, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/cp;->f:Ljava/util/Set;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientOzExtensionEntity;

    iget-object v1, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/cp;->f:Ljava/util/Set;

    iget-object v2, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/cp;->a:Ljava/lang/String;

    iget-object v3, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/cp;->b:Ljava/util/List;

    iget-object v4, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/cp;->c:Ljava/lang/String;

    iget-object v5, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/cp;->d:Ljava/lang/String;

    iget-wide v6, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/cp;->e:J

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientOzExtensionEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;J)V

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientOzExtensionEntity;

    .line 241
    iget-object v2, p0, Lcom/google/android/gms/plus/b/n;->h:Lcom/google/android/gms/plus/service/v1whitelisted/k;

    const-string v1, "rpc/insertLog"

    if-eqz p5, :cond_0

    const-string v3, "onBehalfOf"

    invoke-static {p5}, Lcom/google/android/gms/plus/service/v1whitelisted/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/plus/service/v1whitelisted/k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_0
    iget-object v2, v2, Lcom/google/android/gms/plus/service/v1whitelisted/k;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x1

    invoke-virtual {v2, p1, v3, v1, v0}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    .line 242
    return-void

    .line 232
    :cond_1
    const-string v0, "4"

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/model/posts/Post;)Lcom/google/android/gms/plus/model/posts/Settings;
    .locals 15

    .prologue
    .line 465
    new-instance v6, Lcom/google/android/gms/common/util/f;

    invoke-direct {v6}, Lcom/google/android/gms/common/util/f;-><init>()V

    .line 466
    iget-object v0, p0, Lcom/google/android/gms/plus/b/n;->j:Lcom/google/android/gms/common/server/n;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/n;->c()V

    .line 468
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/plus/e/d;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 472
    invoke-virtual {v6}, Lcom/google/android/gms/common/util/f;->a()Lcom/google/android/gms/common/util/f;

    move-result-object v4

    .line 473
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/plus/model/posts/Post;->q()Ljava/lang/String;

    move-result-object v11

    .line 474
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "me"

    .line 475
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/plus/b/n;->i:Lcom/google/android/gms/plus/service/v1whitelisted/l;

    const-string v2, "sharing"

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/plus/model/posts/Post;->o()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-static {v7, v0, v2, v5, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/l;->a(Lcom/google/android/gms/plus/service/v1whitelisted/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, v3, Lcom/google/android/gms/plus/service/v1whitelisted/l;->a:Lcom/google/android/gms/common/server/n;

    const-class v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;

    move-object/from16 v1, p2

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Class;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 483
    invoke-virtual {v6}, Lcom/google/android/gms/common/util/f;->a()Lcom/google/android/gms/common/util/f;

    move-result-object v9

    .line 484
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/plus/model/posts/Post;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v2

    .line 485
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 488
    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 489
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    .line 490
    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v0

    .line 492
    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 493
    iget-object v5, p0, Lcom/google/android/gms/plus/b/n;->g:Lcom/google/android/gms/plus/service/v1whitelisted/i;

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v6, Lcom/google/android/gms/plus/b/n;->a:Lcom/google/android/gms/plus/service/v1whitelisted/j;

    invoke-static {v6, v0, v11}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Lcom/google/android/gms/plus/service/v1whitelisted/j;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iget-object v5, v5, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a:Lcom/google/android/gms/common/server/n;

    const-class v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    move-object/from16 v6, p2

    move-object v10, v9

    invoke-virtual/range {v5 .. v10}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Class;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 489
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move-object v0, v11

    .line 474
    goto :goto_0

    .line 504
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/b/n;->j:Lcom/google/android/gms/common/server/n;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/n;->e()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 511
    invoke-virtual {v4}, Lcom/google/android/gms/common/util/f;->b()V

    .line 514
    const/4 v2, 0x0

    .line 515
    const/4 v3, 0x0

    .line 516
    sget-object v0, Lcom/google/android/gms/plus/c/a;->X:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 517
    sget-object v0, Lcom/google/android/gms/plus/c/a;->Y:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 518
    sget-object v0, Lcom/google/android/gms/plus/c/a;->Z:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 519
    sget-object v0, Lcom/google/android/gms/plus/c/a;->aa:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 520
    invoke-virtual {v4}, Lcom/google/android/gms/common/util/f;->c()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 521
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/common/util/f;->a(I)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;->b()Ljava/util/List;

    move-result-object v8

    .line 522
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v10

    .line 525
    const/4 v0, 0x0

    move v4, v1

    move v1, v7

    move v7, v0

    move v13, v5

    move v5, v3

    move v3, v13

    move v14, v6

    move-object v6, v2

    move v2, v14

    :goto_2
    if-ge v7, v10, :cond_8

    .line 526
    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;

    .line 527
    const-string v11, "sharing.defaultAccess"

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->h()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->e()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 529
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->d()Lcom/google/android/gms/plus/service/v1whitelisted/models/t;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/people/data/c;->b(Lcom/google/android/gms/plus/service/v1whitelisted/models/t;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    move v13, v1

    move v1, v2

    move v2, v3

    move v3, v4

    move v4, v5

    move-object v5, v0

    move v0, v13

    .line 525
    :goto_3
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    move-object v6, v5

    move v5, v4

    move v4, v3

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_2

    .line 505
    :catch_0
    move-exception v0

    .line 506
    const-string v1, "PlusWhitelistedAgent"

    const-string v2, "Network operation interrupted"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 507
    new-instance v1, Lcom/android/volley/ac;

    invoke-direct {v1, v0}, Lcom/android/volley/ac;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 531
    :cond_3
    const-string v11, "sharing.underageWarning"

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->h()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->g()Z

    move-result v11

    if-eqz v11, :cond_4

    .line 533
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->f()Z

    move-result v0

    move-object v5, v6

    move v13, v2

    move v2, v3

    move v3, v4

    move v4, v0

    move v0, v1

    move v1, v13

    goto :goto_3

    .line 534
    :cond_4
    const-string v11, "sharing.showAclPickerFirst"

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->h()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->g()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 536
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->f()Z

    move-result v0

    move v4, v5

    move-object v5, v6

    move v13, v3

    move v3, v0

    move v0, v1

    move v1, v2

    move v2, v13

    goto :goto_3

    .line 537
    :cond_5
    const-string v11, "sharing.showcasedSuggestionCount"

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->h()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->j()Z

    move-result v11

    if-eqz v11, :cond_6

    .line 539
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->i()I

    move-result v0

    move v3, v4

    move v4, v5

    move-object v5, v6

    move v13, v1

    move v1, v2

    move v2, v0

    move v0, v13

    goto :goto_3

    .line 540
    :cond_6
    const-string v11, "sharing.suggestionCount"

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->h()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->j()Z

    move-result v11

    if-eqz v11, :cond_7

    .line 542
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->i()I

    move-result v0

    move v2, v3

    move v3, v4

    move v4, v5

    move-object v5, v6

    move v13, v1

    move v1, v0

    move v0, v13

    goto/16 :goto_3

    .line 543
    :cond_7
    const-string v11, "sharing.clientSuggestionCount"

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->h()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->j()Z

    move-result v11

    if-eqz v11, :cond_c

    .line 545
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->i()I

    move-result v0

    move v1, v2

    move v2, v3

    move v3, v4

    move v4, v5

    move-object v5, v6

    goto/16 :goto_3

    :cond_8
    move v7, v1

    move-object v1, v6

    move v6, v2

    move v13, v5

    move v5, v3

    move v3, v13

    .line 551
    :goto_4
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 552
    invoke-virtual {v9}, Lcom/google/android/gms/common/util/f;->c()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 554
    const/4 v0, 0x0

    move v2, v0

    :goto_5
    iget-object v0, v9, Lcom/google/android/gms/common/util/f;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    .line 555
    invoke-virtual {v9, v2}, Lcom/google/android/gms/common/util/f;->a(I)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    .line 556
    if-eqz v0, :cond_9

    .line 557
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->g()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v11, 0x0

    invoke-static {v10, v0, v11}, Lcom/google/android/gms/common/people/data/AudienceMember;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 554
    :cond_9
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 563
    :cond_a
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v2, 0x0

    .line 569
    :goto_6
    new-instance v0, Lcom/google/android/gms/plus/model/posts/Settings;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/model/posts/Settings;-><init>(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/Audience;ZZIII)V

    return-object v0

    .line 563
    :cond_b
    new-instance v0, Lcom/google/android/gms/common/people/data/a;

    invoke-direct {v0}, Lcom/google/android/gms/common/people/data/a;-><init>()V

    invoke-virtual {v0, v8}, Lcom/google/android/gms/common/people/data/a;->a(Ljava/util/Collection;)Lcom/google/android/gms/common/people/data/a;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->b()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/people/data/a;->a(I)Lcom/google/android/gms/common/people/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/a;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v2

    goto :goto_6

    :cond_c
    move v0, v1

    move v1, v2

    move v2, v3

    move v3, v4

    move v4, v5

    move-object v5, v6

    goto/16 :goto_3

    :cond_d
    move v4, v1

    move-object v1, v2

    goto :goto_4
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 906
    invoke-static {p1}, Lcom/google/android/gms/plus/e/d;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 907
    iget-object v1, p0, Lcom/google/android/gms/plus/b/n;->b:Lcom/google/android/gms/plus/service/v1whitelisted/a;

    const-string v3, "shared"

    const-string v4, "moments/%1$s/acl/%2$s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v6, 0x1

    invoke-static {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_0

    const-string v4, "language"

    invoke-static {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    return-object v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 889
    invoke-static {p1}, Lcom/google/android/gms/plus/e/d;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 890
    iget-object v1, p0, Lcom/google/android/gms/plus/b/n;->f:Lcom/google/android/gms/plus/service/v1whitelisted/h;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v3, "applications/%1$s/moments"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_0

    const-string v5, "language"

    invoke-static {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v5, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz v4, :cond_1

    const-string v0, "maxResults"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz p5, :cond_2

    const-string v0, "pageToken"

    invoke-static {p5}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    iget-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;
    .locals 1

    .prologue
    .line 932
    iget-object v0, p0, Lcom/google/android/gms/plus/b/n;->g:Lcom/google/android/gms/plus/service/v1whitelisted/i;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)V
    .locals 4

    .prologue
    .line 649
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 650
    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->d()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 651
    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;-><init>()V

    const-string v2, "allCircles"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;->b(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fb;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 657
    :cond_0
    :goto_0
    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/t;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    .line 658
    iget-object v1, p0, Lcom/google/android/gms/plus/b/n;->b:Lcom/google/android/gms/plus/service/v1whitelisted/a;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->c()Ljava/lang/String;

    move-result-object v2

    const-string v3, "visible"

    invoke-virtual {v1, p1, v2, v3, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    .line 661
    return-void

    .line 653
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->d()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 654
    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->d()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/people/data/c;->a(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
