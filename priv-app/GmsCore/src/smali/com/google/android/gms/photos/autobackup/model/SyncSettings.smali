.class public Lcom/google/android/gms/photos/autobackup/model/SyncSettings;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field private b:Z

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/google/android/gms/photos/autobackup/model/h;

    invoke-direct {v0}, Lcom/google/android/gms/photos/autobackup/model/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/photos/autobackup/model/SyncSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IZZ)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput p1, p0, Lcom/google/android/gms/photos/autobackup/model/SyncSettings;->a:I

    .line 41
    iput-boolean p2, p0, Lcom/google/android/gms/photos/autobackup/model/SyncSettings;->b:Z

    .line 42
    iput-boolean p3, p0, Lcom/google/android/gms/photos/autobackup/model/SyncSettings;->c:Z

    .line 43
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/SyncSettings;->b:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/SyncSettings;->c:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 66
    invoke-static {p0, p1}, Lcom/google/android/gms/photos/autobackup/model/h;->a(Lcom/google/android/gms/photos/autobackup/model/SyncSettings;Landroid/os/Parcel;)V

    .line 67
    return-void
.end method
