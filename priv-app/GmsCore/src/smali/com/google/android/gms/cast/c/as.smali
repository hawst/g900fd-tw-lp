.class public final Lcom/google/android/gms/cast/c/as;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/cast/internal/k;


# instance fields
.field private final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "MdnsResponseDecoder"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/cast/c/as;->a:Lcom/google/android/gms/cast/internal/k;

    return-void
.end method

.method public constructor <init>([Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/gms/cast/c/as;->b:[Ljava/lang/String;

    .line 41
    return-void
.end method

.method private static a(Ljava/util/List;[Ljava/lang/String;)Lcom/google/android/gms/cast/c/aq;
    .locals 3

    .prologue
    .line 238
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/aq;

    .line 239
    iget-object v2, v0, Lcom/google/android/gms/cast/c/aq;->a:Lcom/google/android/gms/cast/c/ao;

    .line 240
    if-eqz v2, :cond_0

    .line 241
    iget-object v2, v2, Lcom/google/android/gms/cast/c/ao;->a:[Ljava/lang/String;

    invoke-static {v2, p1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 245
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/cast/c/am;)V
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/am;->b(I)V

    .line 233
    invoke-virtual {p0}, Lcom/google/android/gms/cast/c/am;->c()I

    move-result v0

    .line 234
    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/am;->b(I)V

    .line 235
    return-void
.end method

.method private static b(Ljava/util/List;[Ljava/lang/String;)Lcom/google/android/gms/cast/c/aq;
    .locals 3

    .prologue
    .line 250
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/aq;

    .line 251
    iget-object v2, v0, Lcom/google/android/gms/cast/c/aq;->b:Lcom/google/android/gms/cast/c/at;

    .line 252
    if-eqz v2, :cond_0

    .line 253
    iget-object v2, v2, Lcom/google/android/gms/cast/c/at;->d:[Ljava/lang/String;

    invoke-static {v2, p1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 257
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/net/DatagramPacket;Ljava/util/List;)I
    .locals 13

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x2

    const/4 v0, 0x1

    const/4 v2, 0x4

    const/4 v4, 0x0

    .line 51
    if-nez p2, :cond_0

    .line 52
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must preallocate \'responses\'"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_0
    if-nez p1, :cond_1

    .line 55
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "DatagramPacket cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_1
    new-instance v5, Lcom/google/android/gms/cast/c/am;

    invoke-direct {v5, p1}, Lcom/google/android/gms/cast/c/am;-><init>(Ljava/net/DatagramPacket;)V

    .line 61
    :try_start_0
    invoke-virtual {v5}, Lcom/google/android/gms/cast/c/am;->c()I

    .line 62
    invoke-virtual {v5}, Lcom/google/android/gms/cast/c/am;->c()I

    move-result v6

    .line 63
    const v7, 0xf80f

    and-int/2addr v6, v7

    const v7, 0x8000

    if-eq v6, v7, :cond_2

    .line 228
    :goto_0
    return v0

    .line 67
    :cond_2
    invoke-virtual {v5}, Lcom/google/android/gms/cast/c/am;->c()I

    move-result v0

    .line 68
    invoke-virtual {v5}, Lcom/google/android/gms/cast/c/am;->c()I

    move-result v6

    .line 69
    invoke-virtual {v5}, Lcom/google/android/gms/cast/c/am;->c()I

    move-result v7

    .line 70
    invoke-virtual {v5}, Lcom/google/android/gms/cast/c/am;->c()I

    move-result v8

    .line 72
    sget-object v9, Lcom/google/android/gms/cast/c/as;->a:Lcom/google/android/gms/cast/internal/k;

    const-string v10, "num questions: %d, num answers: %d, num authority: %d, num records: %d"

    const/4 v11, 0x4

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v11, v12

    const/4 v0, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v0

    const/4 v0, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v0

    const/4 v0, 0x3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v0

    invoke-virtual {v9, v10, v11}, Lcom/google/android/gms/cast/internal/k;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    if-gtz v6, :cond_3

    move v0, v1

    .line 76
    goto :goto_0

    .line 79
    :cond_3
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_9

    move v0, v4

    .line 81
    :goto_1
    add-int v9, v6, v7

    add-int/2addr v9, v8

    if-ge v0, v9, :cond_6

    .line 84
    :try_start_1
    invoke-virtual {v5}, Lcom/google/android/gms/cast/c/am;->e()[Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_9

    move-result-object v9

    .line 88
    :try_start_2
    invoke-virtual {v5}, Lcom/google/android/gms/cast/c/am;->c()I
    :try_end_2
    .catch Ljava/io/EOFException; {:try_start_2 .. :try_end_2} :catch_9

    move-result v10

    .line 90
    sparse-switch v10, :sswitch_data_0

    .line 158
    :try_start_3
    invoke-static {v5}, Lcom/google/android/gms/cast/c/as;->a(Lcom/google/android/gms/cast/c/am;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/io/EOFException; {:try_start_3 .. :try_end_3} :catch_9

    .line 81
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 86
    :catch_0
    move-exception v0

    move v0, v3

    goto :goto_0

    .line 93
    :sswitch_0
    :try_start_4
    new-instance v10, Lcom/google/android/gms/cast/c/ak;

    invoke-direct {v10, v9, v5}, Lcom/google/android/gms/cast/c/ak;-><init>([Ljava/lang/String;Lcom/google/android/gms/cast/c/am;)V

    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/EOFException; {:try_start_4 .. :try_end_4} :catch_9

    goto :goto_2

    .line 95
    :catch_1
    move-exception v0

    move v0, v2

    goto :goto_0

    .line 102
    :sswitch_1
    :try_start_5
    new-instance v10, Lcom/google/android/gms/cast/c/al;

    invoke-direct {v10, v9, v5}, Lcom/google/android/gms/cast/c/al;-><init>([Ljava/lang/String;Lcom/google/android/gms/cast/c/am;)V

    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/EOFException; {:try_start_5 .. :try_end_5} :catch_9

    goto :goto_2

    .line 104
    :catch_2
    move-exception v0

    const/4 v0, 0x5

    goto :goto_0

    .line 114
    :sswitch_2
    :try_start_6
    iget-object v10, p0, Lcom/google/android/gms/cast/c/as;->b:[Ljava/lang/String;

    invoke-static {v9, v10}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/io/EOFException; {:try_start_6 .. :try_end_6} :catch_9

    move-result v10

    if-eqz v10, :cond_4

    .line 116
    :try_start_7
    new-instance v10, Lcom/google/android/gms/cast/c/ao;

    invoke-direct {v10, v9, v5}, Lcom/google/android/gms/cast/c/ao;-><init>([Ljava/lang/String;Lcom/google/android/gms/cast/c/am;)V

    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/EOFException; {:try_start_7 .. :try_end_7} :catch_9

    goto :goto_2

    .line 118
    :catch_3
    move-exception v0

    const/4 v0, 0x6

    goto/16 :goto_0

    .line 122
    :cond_4
    :try_start_8
    invoke-static {v5}, Lcom/google/android/gms/cast/c/as;->a(Lcom/google/android/gms/cast/c/am;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/io/EOFException; {:try_start_8 .. :try_end_8} :catch_9

    goto :goto_2

    .line 124
    :catch_4
    move-exception v0

    const/4 v0, 0x7

    goto/16 :goto_0

    .line 131
    :sswitch_3
    :try_start_9
    array-length v10, v9
    :try_end_9
    .catch Ljava/io/EOFException; {:try_start_9 .. :try_end_9} :catch_9

    if-ne v10, v2, :cond_5

    .line 133
    :try_start_a
    new-instance v10, Lcom/google/android/gms/cast/c/at;

    invoke-direct {v10, v9, v5}, Lcom/google/android/gms/cast/c/at;-><init>([Ljava/lang/String;Lcom/google/android/gms/cast/c/am;)V

    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/io/EOFException; {:try_start_a .. :try_end_a} :catch_9

    goto :goto_2

    .line 135
    :catch_5
    move-exception v0

    const/16 v0, 0x8

    goto/16 :goto_0

    .line 139
    :cond_5
    :try_start_b
    invoke-static {v5}, Lcom/google/android/gms/cast/c/as;->a(Lcom/google/android/gms/cast/c/am;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6
    .catch Ljava/io/EOFException; {:try_start_b .. :try_end_b} :catch_9

    goto :goto_2

    .line 141
    :catch_6
    move-exception v0

    const/16 v0, 0x9

    goto/16 :goto_0

    .line 149
    :sswitch_4
    :try_start_c
    new-instance v10, Lcom/google/android/gms/cast/c/au;

    invoke-direct {v10, v9, v5}, Lcom/google/android/gms/cast/c/au;-><init>([Ljava/lang/String;Lcom/google/android/gms/cast/c/am;)V

    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_7
    .catch Ljava/io/EOFException; {:try_start_c .. :try_end_c} :catch_9

    goto :goto_2

    .line 151
    :catch_7
    move-exception v0

    const/16 v0, 0xa

    goto/16 :goto_0

    .line 160
    :catch_8
    move-exception v0

    const/16 v0, 0xb

    goto/16 :goto_0

    .line 166
    :catch_9
    move-exception v0

    const/16 v0, 0xc

    goto/16 :goto_0

    .line 185
    :cond_6
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/ap;

    .line 186
    instance-of v3, v0, Lcom/google/android/gms/cast/c/ao;

    if-eqz v3, :cond_7

    .line 187
    new-instance v3, Lcom/google/android/gms/cast/c/aq;

    invoke-virtual {p1}, Ljava/net/DatagramPacket;->getSocketAddress()Ljava/net/SocketAddress;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/google/android/gms/cast/c/aq;-><init>(Ljava/net/SocketAddress;)V

    .line 188
    check-cast v0, Lcom/google/android/gms/cast/c/ao;

    iput-object v0, v3, Lcom/google/android/gms/cast/c/aq;->a:Lcom/google/android/gms/cast/c/ao;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/cast/c/aq;->a(Lcom/google/android/gms/cast/c/ap;)V

    .line 189
    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 194
    :cond_8
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/ap;

    .line 195
    instance-of v3, v0, Lcom/google/android/gms/cast/c/at;

    if-eqz v3, :cond_a

    .line 196
    check-cast v0, Lcom/google/android/gms/cast/c/at;

    .line 197
    invoke-virtual {v0}, Lcom/google/android/gms/cast/c/at;->a()[Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Lcom/google/android/gms/cast/c/as;->a(Ljava/util/List;[Ljava/lang/String;)Lcom/google/android/gms/cast/c/aq;

    move-result-object v3

    .line 199
    if-eqz v3, :cond_9

    .line 200
    iput-object v0, v3, Lcom/google/android/gms/cast/c/aq;->b:Lcom/google/android/gms/cast/c/at;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/cast/c/aq;->a(Lcom/google/android/gms/cast/c/ap;)V

    goto :goto_4

    .line 202
    :cond_a
    instance-of v3, v0, Lcom/google/android/gms/cast/c/au;

    if-eqz v3, :cond_9

    .line 203
    check-cast v0, Lcom/google/android/gms/cast/c/au;

    .line 204
    invoke-virtual {v0}, Lcom/google/android/gms/cast/c/au;->a()[Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Lcom/google/android/gms/cast/c/as;->a(Ljava/util/List;[Ljava/lang/String;)Lcom/google/android/gms/cast/c/aq;

    move-result-object v3

    .line 205
    if-eqz v3, :cond_9

    .line 206
    iput-object v0, v3, Lcom/google/android/gms/cast/c/aq;->c:Lcom/google/android/gms/cast/c/au;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/cast/c/aq;->a(Lcom/google/android/gms/cast/c/ap;)V

    goto :goto_4

    .line 212
    :cond_b
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_c
    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/ap;

    .line 213
    instance-of v2, v0, Lcom/google/android/gms/cast/c/ak;

    if-eqz v2, :cond_d

    .line 214
    check-cast v0, Lcom/google/android/gms/cast/c/ak;

    .line 215
    invoke-virtual {v0}, Lcom/google/android/gms/cast/c/ak;->a()[Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, v2}, Lcom/google/android/gms/cast/c/as;->b(Ljava/util/List;[Ljava/lang/String;)Lcom/google/android/gms/cast/c/aq;

    move-result-object v2

    .line 216
    if-eqz v2, :cond_c

    .line 217
    iput-object v0, v2, Lcom/google/android/gms/cast/c/aq;->d:Lcom/google/android/gms/cast/c/ak;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/c/aq;->a(Lcom/google/android/gms/cast/c/ap;)V

    goto :goto_5

    .line 219
    :cond_d
    instance-of v2, v0, Lcom/google/android/gms/cast/c/al;

    if-eqz v2, :cond_c

    .line 220
    check-cast v0, Lcom/google/android/gms/cast/c/al;

    .line 221
    invoke-virtual {v0}, Lcom/google/android/gms/cast/c/al;->a()[Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, v2}, Lcom/google/android/gms/cast/c/as;->b(Ljava/util/List;[Ljava/lang/String;)Lcom/google/android/gms/cast/c/aq;

    move-result-object v2

    .line 222
    if-eqz v2, :cond_c

    .line 223
    iput-object v0, v2, Lcom/google/android/gms/cast/c/aq;->e:Lcom/google/android/gms/cast/c/al;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/c/aq;->a(Lcom/google/android/gms/cast/c/ap;)V

    goto :goto_5

    :cond_e
    move v0, v4

    .line 228
    goto/16 :goto_0

    .line 90
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xc -> :sswitch_2
        0x10 -> :sswitch_4
        0x1c -> :sswitch_1
        0x21 -> :sswitch_3
    .end sparse-switch
.end method
