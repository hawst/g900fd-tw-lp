.class final Lcom/google/android/gms/droidguard/d/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/droidguard/d/q;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/droidguard/d/z;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/droidguard/d/z;)V
    .locals 1

    .prologue
    .line 60
    const-string v0, "https://www.googleapis.com/androidantiabuse/v1/x/create?alt=PROTO&key=AIzaSyBofcZsgLSS7BOnBjZPEkk4rYwzOIz-lTI"

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/gms/droidguard/d/v;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/droidguard/d/z;)V

    .line 61
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/droidguard/d/z;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/google/android/gms/droidguard/d/v;->a:Landroid/content/Context;

    .line 55
    iput-object p2, p0, Lcom/google/android/gms/droidguard/d/v;->b:Ljava/lang/String;

    .line 56
    iput-object p3, p0, Lcom/google/android/gms/droidguard/d/v;->c:Lcom/google/android/gms/droidguard/d/z;

    .line 57
    return-void
.end method

.method private a(Lcom/google/android/gms/droidguard/d/y;)Lcom/google/android/gms/droidguard/d/k;
    .locals 9

    .prologue
    .line 64
    new-instance v8, Lcom/google/android/gms/droidguard/d/k;

    invoke-direct {v8}, Lcom/google/android/gms/droidguard/d/k;-><init>()V

    .line 66
    :try_start_0
    new-instance v2, Lcom/google/android/gms/droidguard/d;

    invoke-direct {v2}, Lcom/google/android/gms/droidguard/d;-><init>()V

    new-instance v3, Lcom/google/android/gms/droidguard/h;

    invoke-direct {v3}, Lcom/google/android/gms/droidguard/h;-><init>()V

    iget-object v4, p1, Lcom/google/android/gms/droidguard/d/y;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/droidguard/h;->a(Ljava/lang/String;)Lcom/google/android/gms/droidguard/h;

    iget-object v4, p1, Lcom/google/android/gms/droidguard/d/y;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/droidguard/h;->b(Ljava/lang/String;)Lcom/google/android/gms/droidguard/h;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/droidguard/d;->a(Lcom/google/android/gms/droidguard/h;)Lcom/google/android/gms/droidguard/d;

    const-string v3, "6.7.77 (1747363-000)"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/droidguard/d;->a(Ljava/lang/String;)Lcom/google/android/gms/droidguard/d;

    const-string v3, "BOARD"

    sget-object v4, Landroid/os/Build;->BOARD:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "BOOTLOADER"

    sget-object v4, Landroid/os/Build;->BOOTLOADER:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "BRAND"

    sget-object v4, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "CPU_ABI"

    sget-object v4, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "CPU_ABI2"

    sget-object v4, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "DEVICE"

    sget-object v4, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "DISPLAY"

    sget-object v4, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "FINGERPRINT"

    sget-object v4, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "HARDWARE"

    sget-object v4, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "HOST"

    sget-object v4, Landroid/os/Build;->HOST:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "ID"

    sget-object v4, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "MANUFACTURER"

    sget-object v4, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "MODEL"

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "PRODUCT"

    sget-object v4, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "RADIO"

    sget-object v4, Landroid/os/Build;->RADIO:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "SERIAL"

    sget-object v4, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "TAGS"

    sget-object v4, Landroid/os/Build;->TAGS:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "TIME"

    sget-wide v4, Landroid/os/Build;->TIME:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "TYPE"

    sget-object v4, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "USER"

    sget-object v4, Landroid/os/Build;->USER:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "VERSION.CODENAME"

    sget-object v4, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "VERSION.INCREMENTAL"

    sget-object v4, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "VERSION.RELEASE"

    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "VERSION.SDK"

    sget-object v4, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "VERSION.SDK_INT"

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/droidguard/d/v;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/common/util/a;->a(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/gms/droidguard/d;->a(Z)Lcom/google/android/gms/droidguard/d;

    .line 67
    :cond_0
    invoke-direct {p0, v2}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d;)Lcom/google/android/gms/droidguard/e;

    move-result-object v2

    .line 68
    iget-object v3, p0, Lcom/google/android/gms/droidguard/d/v;->c:Lcom/google/android/gms/droidguard/d/z;

    iget-object v4, v2, Lcom/google/android/gms/droidguard/e;->b:Lcom/google/protobuf/a/a;

    invoke-virtual {v4}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v4

    iget-object v5, v2, Lcom/google/android/gms/droidguard/e;->d:Lcom/google/protobuf/a/a;

    invoke-virtual {v5}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/google/android/gms/droidguard/d/z;->a([B[B)Z

    move-result v3

    if-nez v3, :cond_1

    .line 70
    const-string v2, "Program signature verification failed."

    invoke-virtual {v8, v2}, Lcom/google/android/gms/droidguard/d/k;->a(Ljava/lang/String;)Lcom/google/android/gms/droidguard/d/k;

    move-result-object v2

    .line 77
    :goto_0
    return-object v2

    .line 72
    :cond_1
    iget-boolean v3, v2, Lcom/google/android/gms/droidguard/e;->a:Z

    if-nez v3, :cond_2

    new-instance v2, Lcom/google/android/gms/droidguard/d/x;

    const-string v3, "program"

    invoke-direct {v2, v3}, Lcom/google/android/gms/droidguard/d/x;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :catch_0
    move-exception v2

    .line 75
    invoke-virtual {v8, v2}, Lcom/google/android/gms/droidguard/d/k;->a(Ljava/lang/Throwable;)Lcom/google/android/gms/droidguard/d/k;

    :goto_1
    move-object v2, v8

    .line 77
    goto :goto_0

    .line 72
    :cond_2
    :try_start_1
    iget-boolean v3, v2, Lcom/google/android/gms/droidguard/e;->c:Z

    if-nez v3, :cond_3

    new-instance v2, Lcom/google/android/gms/droidguard/d/x;

    const-string v3, "signature"

    invoke-direct {v2, v3}, Lcom/google/android/gms/droidguard/d/x;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    iget-object v2, v2, Lcom/google/android/gms/droidguard/e;->b:Lcom/google/protobuf/a/a;

    invoke-virtual {v2}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/droidguard/g;

    invoke-direct {v3}, Lcom/google/android/gms/droidguard/g;-><init>()V

    array-length v4, v2

    invoke-virtual {v3, v2, v4}, Lcom/google/protobuf/a/f;->a([BI)Lcom/google/protobuf/a/f;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/android/gms/droidguard/g;

    move-object v6, v0

    iget-boolean v2, v6, Lcom/google/android/gms/droidguard/g;->a:Z

    if-nez v2, :cond_4

    new-instance v2, Lcom/google/android/gms/droidguard/d/x;

    const-string v3, "byteCode"

    invoke-direct {v2, v3}, Lcom/google/android/gms/droidguard/d/x;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    iget-boolean v2, v6, Lcom/google/android/gms/droidguard/g;->c:Z

    if-nez v2, :cond_5

    new-instance v2, Lcom/google/android/gms/droidguard/d/x;

    const-string v3, "vmUrl"

    invoke-direct {v2, v3}, Lcom/google/android/gms/droidguard/d/x;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_5
    iget-boolean v2, v6, Lcom/google/android/gms/droidguard/g;->e:Z

    if-nez v2, :cond_6

    new-instance v2, Lcom/google/android/gms/droidguard/d/x;

    const-string v3, "vmChecksum"

    invoke-direct {v2, v3}, Lcom/google/android/gms/droidguard/d/x;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_6
    iget-boolean v2, v6, Lcom/google/android/gms/droidguard/g;->g:Z

    if-nez v2, :cond_7

    new-instance v2, Lcom/google/android/gms/droidguard/d/x;

    const-string v3, "expiryTimeSecs"

    invoke-direct {v2, v3}, Lcom/google/android/gms/droidguard/d/x;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_7
    new-instance v2, Lcom/google/android/gms/droidguard/d/s;

    iget-object v3, v6, Lcom/google/android/gms/droidguard/g;->b:Lcom/google/protobuf/a/a;

    invoke-virtual {v3}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v3

    iget-object v4, v6, Lcom/google/android/gms/droidguard/g;->f:Lcom/google/protobuf/a/a;

    invoke-virtual {v4}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/google/android/gms/common/util/e;->a([BZ)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v6, Lcom/google/android/gms/droidguard/g;->d:Ljava/lang/String;

    iget v6, v6, Lcom/google/android/gms/droidguard/g;->h:I

    int-to-long v6, v6

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/droidguard/d/s;-><init>([BLjava/lang/String;Ljava/lang/String;J)V

    .line 73
    invoke-virtual {v8, v2}, Lcom/google/android/gms/droidguard/d/k;->a(Ljava/lang/Object;)Lcom/google/android/gms/droidguard/d/k;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private a(Lcom/google/android/gms/droidguard/d;)Lcom/google/android/gms/droidguard/e;
    .locals 5

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/v;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/p;->a(Landroid/content/Context;)Lcom/google/android/gms/http/GoogleHttpClient;

    move-result-object v1

    .line 83
    :try_start_0
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    iget-object v2, p0, Lcom/google/android/gms/droidguard/d/v;->b:Ljava/lang/String;

    invoke-direct {v0, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 84
    new-instance v2, Lcom/google/android/gms/droidguard/d/w;

    invoke-direct {v2, p1}, Lcom/google/android/gms/droidguard/d/w;-><init>(Lcom/google/android/gms/droidguard/d;)V

    invoke-virtual {v0, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 86
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 88
    :try_start_1
    invoke-virtual {v1, v0}, Lcom/google/android/gms/http/GoogleHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 90
    :try_start_2
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 92
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 93
    const/16 v3, 0xc8

    if-eq v2, v3, :cond_0

    .line 94
    new-instance v0, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received status code ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] instead of [200]."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 107
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    throw v0

    .line 90
    :catchall_1
    move-exception v0

    :try_start_3
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0

    .line 97
    :cond_0
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 98
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 99
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 101
    const/4 v3, 0x0

    :try_start_4
    invoke-static {v0, v2, v3}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Z)J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 103
    :try_start_5
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 105
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/droidguard/e;

    invoke-direct {v2}, Lcom/google/android/gms/droidguard/e;-><init>()V

    array-length v3, v0

    invoke-virtual {v2, v0, v3}, Lcom/google/protobuf/a/f;->a([BI)Lcom/google/protobuf/a/f;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/droidguard/e;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 107
    invoke-virtual {v1}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    return-object v0

    .line 103
    :catchall_2
    move-exception v2

    :try_start_6
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method private static a(Lcom/google/android/gms/droidguard/d;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 176
    new-instance v0, Lcom/google/android/gms/droidguard/f;

    invoke-direct {v0}, Lcom/google/android/gms/droidguard/f;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/droidguard/f;->a(Ljava/lang/String;)Lcom/google/android/gms/droidguard/f;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/droidguard/f;->b(Ljava/lang/String;)Lcom/google/android/gms/droidguard/f;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/droidguard/d;->a(Lcom/google/android/gms/droidguard/f;)Lcom/google/android/gms/droidguard/d;

    .line 177
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Lcom/google/android/gms/droidguard/d/k;
    .locals 1

    .prologue
    .line 38
    check-cast p1, Lcom/google/android/gms/droidguard/d/y;

    invoke-direct {p0, p1}, Lcom/google/android/gms/droidguard/d/v;->a(Lcom/google/android/gms/droidguard/d/y;)Lcom/google/android/gms/droidguard/d/k;

    move-result-object v0

    return-object v0
.end method
