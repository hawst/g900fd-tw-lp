.class public final Lcom/google/android/gms/drive/database/model/ao;
.super Lcom/google/android/gms/drive/database/model/ad;
.source "SourceFile"


# static fields
.field public static a:Lcom/google/android/gms/drive/g/i;

.field public static final b:Landroid/net/Uri;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Z

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:J

.field private G:Z

.field private H:Z

.field private I:Ljava/lang/String;

.field private J:J

.field private K:Ljava/lang/String;

.field private L:Ljava/lang/String;

.field private M:Ljava/lang/String;

.field private N:Z

.field private O:Ljava/lang/Long;

.field private P:Ljava/lang/String;

.field private Q:Z

.field private R:I

.field private S:Ljava/lang/String;

.field private T:Ljava/lang/Long;

.field private U:Ljava/lang/String;

.field private V:Ljava/lang/Long;

.field private W:Ljava/lang/Long;

.field private X:Ljava/lang/Boolean;

.field private Y:Ljava/lang/Long;

.field private Z:Ljava/lang/Long;

.field private aa:J

.field private ab:Z

.field private ac:J

.field private ad:Ljava/lang/String;

.field private ae:Z

.field private c:Ljava/lang/String;

.field private d:J

.field private g:Ljava/lang/String;

.field private h:Ljava/util/Date;

.field private i:Ljava/util/Date;

.field private j:Ljava/util/Date;

.field private k:Ljava/util/Date;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/util/Collection;

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Lcom/google/android/gms/drive/database/model/ap;

.field private v:Z

.field private final w:J

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/gms/drive/g/j;->a:Lcom/google/android/gms/drive/g/j;

    sput-object v0, Lcom/google/android/gms/drive/database/model/ao;->a:Lcom/google/android/gms/drive/g/i;

    .line 222
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/drive/database/model/ao;->b:Landroid/net/Uri;

    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/drive/database/i;JLjava/lang/String;J)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 229
    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/drive/database/model/ao;->b:Landroid/net/Uri;

    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/gms/drive/database/model/ad;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)V

    .line 130
    const-string v1, ""

    iput-object v1, p0, Lcom/google/android/gms/drive/database/model/ao;->g:Ljava/lang/String;

    .line 131
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v1, p0, Lcom/google/android/gms/drive/database/model/ao;->h:Ljava/util/Date;

    .line 132
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v1, p0, Lcom/google/android/gms/drive/database/model/ao;->i:Ljava/util/Date;

    .line 133
    iput-object v3, p0, Lcom/google/android/gms/drive/database/model/ao;->j:Ljava/util/Date;

    .line 135
    iput-object v3, p0, Lcom/google/android/gms/drive/database/model/ao;->l:Ljava/lang/String;

    .line 142
    iput-object v3, p0, Lcom/google/android/gms/drive/database/model/ao;->m:Ljava/lang/String;

    .line 143
    iput-object v3, p0, Lcom/google/android/gms/drive/database/model/ao;->n:Ljava/lang/String;

    .line 144
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/drive/database/model/ao;->o:Ljava/util/Collection;

    .line 145
    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->p:Z

    .line 146
    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->q:Z

    .line 147
    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->r:Z

    .line 148
    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->s:Z

    .line 150
    sget-object v1, Lcom/google/android/gms/drive/database/model/ap;->a:Lcom/google/android/gms/drive/database/model/ap;

    iput-object v1, p0, Lcom/google/android/gms/drive/database/model/ao;->u:Lcom/google/android/gms/drive/database/model/ap;

    .line 171
    const-string v1, "owner"

    iput-object v1, p0, Lcom/google/android/gms/drive/database/model/ao;->P:Ljava/lang/String;

    .line 172
    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->Q:Z

    .line 173
    iput v0, p0, Lcom/google/android/gms/drive/database/model/ao;->R:I

    .line 179
    iput-object v3, p0, Lcom/google/android/gms/drive/database/model/ao;->X:Ljava/lang/Boolean;

    .line 190
    iput-object v3, p0, Lcom/google/android/gms/drive/database/model/ao;->Y:Ljava/lang/Long;

    .line 197
    iput-object v3, p0, Lcom/google/android/gms/drive/database/model/ao;->Z:Ljava/lang/Long;

    .line 199
    iput-wide v4, p0, Lcom/google/android/gms/drive/database/model/ao;->aa:J

    .line 230
    iput-object p4, p0, Lcom/google/android/gms/drive/database/model/ao;->c:Ljava/lang/String;

    .line 231
    iput-wide p2, p0, Lcom/google/android/gms/drive/database/model/ao;->d:J

    .line 232
    iput-wide p5, p0, Lcom/google/android/gms/drive/database/model/ao;->aa:J

    .line 233
    const-wide/16 v2, 0x1

    cmp-long v1, p5, v2

    if-eqz v1, :cond_0

    const-wide/16 v2, 0x2

    cmp-long v1, p5, v2

    if-eqz v1, :cond_0

    cmp-long v1, p5, v4

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid locality: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 236
    new-instance v0, Ljava/util/Date;

    sget-object v1, Lcom/google/android/gms/drive/database/model/ao;->a:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v1}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->k:Ljava/util/Date;

    .line 237
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/i;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/database/model/ao;->w:J

    .line 238
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)V
    .locals 7

    .prologue
    .line 241
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->s:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {p2}, Lcom/google/android/gms/drive/database/model/ar;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->k:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/database/model/ao;-><init>(Lcom/google/android/gms/drive/database/i;JLjava/lang/String;J)V

    .line 244
    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ar;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/database/model/ao;->d(J)V

    .line 246
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->l:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/database/model/ao;->b(Ljava/lang/String;)V

    .line 247
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->a:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->g:Ljava/lang/String;

    .line 248
    new-instance v0, Ljava/util/Date;

    sget-object v1, Lcom/google/android/gms/drive/database/model/as;->b:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->h:Ljava/util/Date;

    .line 249
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->B:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->n:Ljava/lang/String;

    .line 251
    :try_start_0
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->C:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/l;->a(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->o:Ljava/util/Collection;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    new-instance v0, Ljava/util/Date;

    sget-object v1, Lcom/google/android/gms/drive/database/model/as;->c:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->i:Ljava/util/Date;

    .line 260
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->d:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    .line 261
    if-eqz v0, :cond_0

    .line 262
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v1, p0, Lcom/google/android/gms/drive/database/model/ao;->j:Ljava/util/Date;

    .line 265
    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->e:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->Y:Ljava/lang/Long;

    .line 266
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->g:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->Z:Ljava/lang/Long;

    .line 270
    new-instance v0, Ljava/util/Date;

    sget-object v1, Lcom/google/android/gms/drive/database/model/as;->h:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->k:Ljava/util/Date;

    .line 272
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->i:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->m:Ljava/lang/String;

    .line 273
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->Y:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->A:Ljava/lang/String;

    .line 276
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->n:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->d(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->p:Z

    .line 277
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->f:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->d(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->q:Z

    .line 278
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->o:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/ap;->a(J)Lcom/google/android/gms/drive/database/model/ap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/database/model/ao;->a(Lcom/google/android/gms/drive/database/model/ap;)V

    .line 279
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->p:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->d(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->r:Z

    .line 280
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->u:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->d(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->v:Z

    .line 281
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->q:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->d(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->t:Z

    .line 283
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->m:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->d(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->ab:Z

    .line 284
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->r:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->d(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/database/model/ao;->k(Z)V

    .line 285
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->t:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->c(Landroid/database/Cursor;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/database/model/ao;->ac:J

    .line 286
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->w:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->ad:Ljava/lang/String;

    .line 287
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->z:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->x:Ljava/lang/String;

    .line 288
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->D:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->B:Ljava/lang/String;

    .line 289
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->E:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->f(Landroid/database/Cursor;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->C:Z

    .line 290
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->F:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->D:Ljava/lang/String;

    .line 291
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->G:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->E:Ljava/lang/String;

    .line 292
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->H:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/database/model/ao;->F:J

    .line 293
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->I:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->f(Landroid/database/Cursor;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->G:Z

    .line 294
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->J:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->f(Landroid/database/Cursor;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->H:Z

    .line 295
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->K:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->I:Ljava/lang/String;

    .line 296
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->L:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/database/model/ao;->J:J

    .line 297
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->M:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->K:Ljava/lang/String;

    .line 298
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->N:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->L:Ljava/lang/String;

    .line 299
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->O:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->M:Ljava/lang/String;

    .line 300
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->P:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->f(Landroid/database/Cursor;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->N:Z

    .line 302
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->A:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->O:Ljava/lang/Long;

    .line 303
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->Q:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/database/model/ao;->r(Ljava/lang/String;)V

    .line 304
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->R:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->e(Landroid/database/Cursor;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->Q:Z

    .line 305
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->S:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/database/model/ao;->R:I

    .line 307
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->T:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->z:Ljava/lang/String;

    .line 308
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->ad:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->y:Ljava/lang/String;

    .line 311
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->W:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 312
    sget-object v1, Lcom/google/android/gms/drive/database/model/as;->X:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v1

    .line 314
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/database/model/ao;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 315
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->Z:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->U:Ljava/lang/String;

    .line 317
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->ab:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->V:Ljava/lang/Long;

    .line 318
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->aa:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->W:Ljava/lang/Long;

    .line 319
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->ac:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->d(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->X:Ljava/lang/Boolean;

    .line 320
    return-void

    .line 252
    :catch_0
    move-exception v0

    .line 253
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid JSON ownerNames string array"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/ao;
    .locals 1

    .prologue
    .line 1056
    new-instance v0, Lcom/google/android/gms/drive/database/model/ao;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/drive/database/model/ao;-><init>(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)V

    return-object v0
.end method

.method private a(Lcom/google/android/gms/drive/database/model/ap;)V
    .locals 1

    .prologue
    .line 549
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ap;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->u:Lcom/google/android/gms/drive/database/model/ap;

    .line 550
    return-void
.end method


# virtual methods
.method public final A()Z
    .locals 2

    .prologue
    .line 578
    sget-object v0, Lcom/google/android/gms/drive/database/model/ap;->b:Lcom/google/android/gms/drive/database/model/ap;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ao;->u:Lcom/google/android/gms/drive/database/model/ap;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ap;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final B()V
    .locals 1

    .prologue
    .line 585
    sget-object v0, Lcom/google/android/gms/drive/database/model/ap;->a:Lcom/google/android/gms/drive/database/model/ap;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/database/model/ao;->a(Lcom/google/android/gms/drive/database/model/ap;)V

    .line 586
    return-void
.end method

.method public final C()Z
    .locals 2

    .prologue
    .line 590
    sget-object v0, Lcom/google/android/gms/drive/database/model/ap;->a:Lcom/google/android/gms/drive/database/model/ap;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ao;->u:Lcom/google/android/gms/drive/database/model/ap;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ap;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final D()Z
    .locals 1

    .prologue
    .line 603
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->t:Z

    return v0
.end method

.method public final E()Z
    .locals 1

    .prologue
    .line 614
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->ab:Z

    return v0
.end method

.method public final F()Ljava/lang/String;
    .locals 1

    .prologue
    .line 622
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final G()Z
    .locals 1

    .prologue
    .line 630
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->C:Z

    return v0
.end method

.method public final H()Ljava/lang/String;
    .locals 1

    .prologue
    .line 638
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->D:Ljava/lang/String;

    return-object v0
.end method

.method public final I()Ljava/lang/String;
    .locals 1

    .prologue
    .line 646
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->E:Ljava/lang/String;

    return-object v0
.end method

.method public final J()J
    .locals 2

    .prologue
    .line 654
    iget-wide v0, p0, Lcom/google/android/gms/drive/database/model/ao;->F:J

    return-wide v0
.end method

.method public final K()Z
    .locals 1

    .prologue
    .line 662
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->G:Z

    return v0
.end method

.method public final L()Z
    .locals 1

    .prologue
    .line 670
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->H:Z

    return v0
.end method

.method public final M()Ljava/lang/String;
    .locals 1

    .prologue
    .line 678
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->I:Ljava/lang/String;

    return-object v0
.end method

.method public final N()J
    .locals 2

    .prologue
    .line 686
    iget-wide v0, p0, Lcom/google/android/gms/drive/database/model/ao;->J:J

    return-wide v0
.end method

.method public final O()Ljava/lang/String;
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->K:Ljava/lang/String;

    return-object v0
.end method

.method public final P()Ljava/lang/String;
    .locals 1

    .prologue
    .line 702
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->L:Ljava/lang/String;

    return-object v0
.end method

.method public final Q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 710
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->M:Ljava/lang/String;

    return-object v0
.end method

.method public final R()Z
    .locals 1

    .prologue
    .line 718
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->N:Z

    return v0
.end method

.method public final S()Z
    .locals 1

    .prologue
    .line 729
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->s:Z

    return v0
.end method

.method public final T()V
    .locals 2

    .prologue
    .line 744
    const-wide/16 v0, 0x2

    iput-wide v0, p0, Lcom/google/android/gms/drive/database/model/ao;->aa:J

    .line 745
    return-void
.end method

.method public final U()J
    .locals 2

    .prologue
    .line 752
    iget-wide v0, p0, Lcom/google/android/gms/drive/database/model/ao;->ac:J

    return-wide v0
.end method

.method public final V()Z
    .locals 1

    .prologue
    .line 764
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->Q:Z

    return v0
.end method

.method public final W()Z
    .locals 2

    .prologue
    .line 879
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->l:Ljava/lang/String;

    const-string v1, "application/vnd.google-apps.folder"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final X()Z
    .locals 2

    .prologue
    .line 887
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->l:Ljava/lang/String;

    const-string v1, "application/vnd.google-apps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final Y()Z
    .locals 1

    .prologue
    .line 930
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->ae:Z

    return v0
.end method

.method public final Z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 946
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 658
    iput-wide p1, p0, Lcom/google/android/gms/drive/database/model/ao;->F:J

    .line 659
    return-void
.end method

.method protected final a(Landroid/content/ContentValues;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 780
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->g:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 781
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->a:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->i:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 783
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->j:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 784
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->k:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v4, p0, Lcom/google/android/gms/drive/database/model/ao;->aa:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 785
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->n:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->p:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 787
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->f:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->q:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 788
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->o:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->u:Lcom/google/android/gms/drive/database/model/ap;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ap;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 789
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->p:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->r:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 790
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->u:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->v:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 791
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->q:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->t:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 794
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->ab:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 795
    sget-object v4, Lcom/google/android/gms/drive/database/model/as;->m:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    if-nez v0, :cond_5

    move-object v0, v3

    :goto_5
    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 797
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->r:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->s:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 798
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->b:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->h:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 799
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->c:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->i:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 802
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->j:Ljava/util/Date;

    if-nez v0, :cond_8

    move-object v0, v3

    .line 803
    :goto_7
    sget-object v4, Lcom/google/android/gms/drive/database/model/as;->d:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 804
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->e:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->Y:Ljava/lang/Long;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 805
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->g:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->Z:Ljava/lang/Long;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 806
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->h:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->k:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 807
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->s:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v4, p0, Lcom/google/android/gms/drive/database/model/ao;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 808
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->l:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 809
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->B:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 810
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->C:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ao;->g()Ljava/util/Collection;

    move-result-object v0

    if-nez v0, :cond_9

    move-object v0, v3

    :goto_8
    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->t:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v4, p0, Lcom/google/android/gms/drive/database/model/ao;->ac:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 813
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->Y:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->A:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->w:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->ad:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->D:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->E:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->C:Z

    if-eqz v0, :cond_a

    move v0, v1

    :goto_9
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 818
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->F:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->D:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->G:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->E:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 820
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->H:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v4, p0, Lcom/google/android/gms/drive/database/model/ao;->F:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 821
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->I:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->G:Z

    if-eqz v0, :cond_b

    move v0, v1

    :goto_a
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 822
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->J:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->H:Z

    if-eqz v0, :cond_c

    move v0, v1

    :goto_b
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 823
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->K:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->I:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 824
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->L:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v4, p0, Lcom/google/android/gms/drive/database/model/ao;->J:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 825
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->M:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->K:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 826
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->N:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->L:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 827
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->O:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->M:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->P:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->N:Z

    if-eqz v0, :cond_d

    move v0, v1

    :goto_c
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 829
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->Q:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->P:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 830
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->R:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->Q:Z

    if-eqz v0, :cond_e

    move v0, v1

    :goto_d
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 832
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->T:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->z:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 833
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->ad:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->y:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 835
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->W:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->S:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->Z:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->U:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 839
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->O:Ljava/lang/Long;

    if-eqz v0, :cond_f

    .line 840
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->A:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->O:Ljava/lang/Long;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 846
    :goto_e
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->x:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 847
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->z:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->x:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    :goto_f
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->T:Ljava/lang/Long;

    if-eqz v0, :cond_11

    .line 853
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->X:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->T:Ljava/lang/Long;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 859
    :goto_10
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->V:Ljava/lang/Long;

    if-eqz v0, :cond_12

    .line 860
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->ab:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->V:Ljava/lang/Long;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 865
    :goto_11
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->W:Ljava/lang/Long;

    if-eqz v0, :cond_13

    .line 866
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->aa:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->W:Ljava/lang/Long;

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 871
    :goto_12
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->ac:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->X:Ljava/lang/Boolean;

    if-nez v4, :cond_14

    :goto_13
    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 873
    return-void

    :cond_0
    move v0, v2

    .line 785
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 787
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 789
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 790
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 791
    goto/16 :goto_4

    .line 795
    :cond_5
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_14
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_5

    :cond_6
    move v0, v2

    goto :goto_14

    :cond_7
    move v0, v2

    .line 797
    goto/16 :goto_6

    .line 802
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->j:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_7

    .line 810
    :cond_9
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5, v0}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v5}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_8

    :cond_a
    move v0, v2

    .line 817
    goto/16 :goto_9

    :cond_b
    move v0, v2

    .line 821
    goto/16 :goto_a

    :cond_c
    move v0, v2

    .line 822
    goto/16 :goto_b

    :cond_d
    move v0, v2

    .line 828
    goto/16 :goto_c

    :cond_e
    move v0, v2

    .line 830
    goto/16 :goto_d

    .line 843
    :cond_f
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->A:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 849
    :cond_10
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->z:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_f

    .line 856
    :cond_11
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->X:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_10

    .line 862
    :cond_12
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->ab:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_11

    .line 869
    :cond_13
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->aa:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_12

    .line 871
    :cond_14
    iget-object v3, p0, Lcom/google/android/gms/drive/database/model/ao;->X:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_15

    :goto_15
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/16 :goto_13

    :cond_15
    move v1, v2

    goto :goto_15
.end method

.method public final a(Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 435
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->Y:Ljava/lang/Long;

    .line 436
    return-void
.end method

.method final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 337
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->g:Ljava/lang/String;

    .line 338
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1074
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez p2, :cond_0

    move v0, v1

    :goto_0
    if-eq v3, v0, :cond_1

    .line 1075
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "An entry must have both uniqueIdentifier & creatorPackagingId or none, provided values: %s & %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    aput-object p2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 1074
    goto :goto_0

    .line 1079
    :cond_1
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->S:Ljava/lang/String;

    .line 1080
    iput-object p2, p0, Lcom/google/android/gms/drive/database/model/ao;->T:Ljava/lang/Long;

    .line 1081
    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 0

    .prologue
    .line 383
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->o:Ljava/util/Collection;

    .line 384
    return-void
.end method

.method public final a(Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 355
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->h:Ljava/util/Date;

    .line 356
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 532
    iput-boolean p1, p0, Lcom/google/android/gms/drive/database/model/ao;->v:Z

    .line 533
    return-void
.end method

.method public final aa()Ljava/lang/String;
    .locals 1

    .prologue
    .line 962
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final ab()Ljava/lang/String;
    .locals 1

    .prologue
    .line 979
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->z:Ljava/lang/String;

    return-object v0
.end method

.method public final ac()Ljava/lang/String;
    .locals 1

    .prologue
    .line 997
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->A:Ljava/lang/String;

    return-object v0
.end method

.method public final ad()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1017
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->P:Ljava/lang/String;

    return-object v0
.end method

.method public final ae()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1065
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->S:Ljava/lang/String;

    return-object v0
.end method

.method public final af()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 1104
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->T:Ljava/lang/Long;

    return-object v0
.end method

.method public final ag()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 1114
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->X:Ljava/lang/Boolean;

    return-object v0
.end method

.method final ah()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1125
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->c:Ljava/lang/String;

    return-object v0
.end method

.method final ai()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 1133
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->W:Ljava/lang/Long;

    return-object v0
.end method

.method final aj()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 1141
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->V:Ljava/lang/Long;

    return-object v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 344
    iget-wide v0, p0, Lcom/google/android/gms/drive/database/model/ao;->d:J

    return-wide v0
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 690
    iput-wide p1, p0, Lcom/google/android/gms/drive/database/model/ao;->J:J

    .line 691
    return-void
.end method

.method public final b(Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 449
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->Z:Ljava/lang/Long;

    .line 450
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 371
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->l:Ljava/lang/String;

    .line 372
    return-void
.end method

.method public final b(Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 406
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->i:Ljava/util/Date;

    .line 407
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 541
    iput-boolean p1, p0, Lcom/google/android/gms/drive/database/model/ao;->p:Z

    .line 542
    return-void
.end method

.method public final c()Ljava/util/Date;
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->h:Ljava/util/Date;

    return-object v0
.end method

.method final c(J)V
    .locals 1

    .prologue
    .line 756
    iput-wide p1, p0, Lcom/google/android/gms/drive/database/model/ao;->ac:J

    .line 757
    return-void
.end method

.method public final c(Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 511
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->O:Ljava/lang/Long;

    .line 512
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 379
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->n:Ljava/lang/String;

    .line 380
    return-void
.end method

.method public final c(Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 421
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->j:Ljava/util/Date;

    .line 422
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 545
    iput-boolean p1, p0, Lcom/google/android/gms/drive/database/model/ao;->q:Z

    .line 546
    return-void
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 359
    iget v0, p0, Lcom/google/android/gms/drive/database/model/ao;->R:I

    int-to-long v0, v0

    return-wide v0
.end method

.method final d(Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 1137
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->W:Ljava/lang/Long;

    .line 1138
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 463
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->m:Ljava/lang/String;

    .line 464
    return-void
.end method

.method public final d(Z)V
    .locals 0

    .prologue
    .line 594
    iput-boolean p1, p0, Lcom/google/android/gms/drive/database/model/ao;->r:Z

    .line 595
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->l:Ljava/lang/String;

    return-object v0
.end method

.method final e(Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 1145
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->V:Ljava/lang/Long;

    .line 1146
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 470
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ao;->w()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 471
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ao;->W()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 472
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->c:Ljava/lang/String;

    .line 473
    return-void
.end method

.method public final e(Z)V
    .locals 0

    .prologue
    .line 610
    iput-boolean p1, p0, Lcom/google/android/gms/drive/database/model/ao;->t:Z

    .line 611
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 626
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->B:Ljava/lang/String;

    .line 627
    return-void
.end method

.method public final f(Z)V
    .locals 0

    .prologue
    .line 618
    iput-boolean p1, p0, Lcom/google/android/gms/drive/database/model/ao;->ab:Z

    .line 619
    return-void
.end method

.method public final g()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->o:Ljava/util/Collection;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->o:Ljava/util/Collection;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 642
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->D:Ljava/lang/String;

    .line 643
    return-void
.end method

.method public final g(Z)V
    .locals 0

    .prologue
    .line 634
    iput-boolean p1, p0, Lcom/google/android/gms/drive/database/model/ao;->C:Z

    .line 635
    return-void
.end method

.method public final h()Ljava/util/Date;
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->i:Ljava/util/Date;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 650
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->E:Ljava/lang/String;

    .line 651
    return-void
.end method

.method public final h(Z)V
    .locals 0

    .prologue
    .line 666
    iput-boolean p1, p0, Lcom/google/android/gms/drive/database/model/ao;->G:Z

    .line 667
    return-void
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 682
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->I:Ljava/lang/String;

    .line 683
    return-void
.end method

.method public final i(Z)V
    .locals 0

    .prologue
    .line 674
    iput-boolean p1, p0, Lcom/google/android/gms/drive/database/model/ao;->H:Z

    .line 675
    return-void
.end method

.method public final j(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 698
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->K:Ljava/lang/String;

    .line 699
    return-void
.end method

.method public final j(Z)V
    .locals 0

    .prologue
    .line 722
    iput-boolean p1, p0, Lcom/google/android/gms/drive/database/model/ao;->N:Z

    .line 723
    return-void
.end method

.method public final k(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 706
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->L:Ljava/lang/String;

    .line 707
    return-void
.end method

.method public final k(Z)V
    .locals 1

    .prologue
    .line 737
    iput-boolean p1, p0, Lcom/google/android/gms/drive/database/model/ao;->s:Z

    .line 738
    if-eqz p1, :cond_0

    .line 739
    const-string v0, "unknown_as_place_holder"

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->m:Ljava/lang/String;

    .line 741
    :cond_0
    return-void
.end method

.method public final l()Ljava/util/Date;
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->j:Ljava/util/Date;

    return-object v0
.end method

.method public final l(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 714
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->M:Ljava/lang/String;

    .line 715
    return-void
.end method

.method public final l(Z)V
    .locals 0

    .prologue
    .line 772
    iput-boolean p1, p0, Lcom/google/android/gms/drive/database/model/ao;->Q:Z

    .line 773
    return-void
.end method

.method public final m()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->Y:Ljava/lang/Long;

    return-object v0
.end method

.method public final m(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 915
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->ad:Ljava/lang/String;

    .line 916
    return-void
.end method

.method public final m(Z)V
    .locals 0

    .prologue
    .line 922
    iput-boolean p1, p0, Lcom/google/android/gms/drive/database/model/ao;->ae:Z

    .line 923
    return-void
.end method

.method public final n()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->Z:Ljava/lang/Long;

    return-object v0
.end method

.method public final n(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 938
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->x:Ljava/lang/String;

    .line 939
    return-void
.end method

.method public final n(Z)V
    .locals 1

    .prologue
    .line 1121
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->X:Ljava/lang/Boolean;

    .line 1122
    return-void
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final o(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 953
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->y:Ljava/lang/String;

    .line 954
    return-void
.end method

.method public final p(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 970
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->z:Ljava/lang/String;

    .line 971
    return-void
.end method

.method public final p()Z
    .locals 4

    .prologue
    .line 480
    iget-wide v0, p0, Lcom/google/android/gms/drive/database/model/ao;->aa:J

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 988
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->A:Ljava/lang/String;

    .line 989
    return-void
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 487
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->p:Z

    return v0
.end method

.method public final r(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1007
    if-eqz p1, :cond_0

    const-string v0, "owner"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "reader"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "writer"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1009
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->P:Ljava/lang/String;

    return-void

    .line 1011
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unaccepted user role value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 491
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->q:Z

    return v0
.end method

.method public final s(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1087
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->U:Ljava/lang/String;

    .line 1088
    return-void
.end method

.method public final s()Z
    .locals 2

    .prologue
    .line 495
    sget-object v0, Lcom/google/android/gms/drive/database/model/ap;->a:Lcom/google/android/gms/drive/database/model/ap;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ao;->u:Lcom/google/android/gms/drive/database/model/ap;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ap;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final t(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1129
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ao;->c:Ljava/lang/String;

    .line 1130
    return-void
.end method

.method public final t()Z
    .locals 1

    .prologue
    .line 499
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->r:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 899
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%sEntryRow %s (account %d) with resource id: %s"

    const/4 v0, 0x4

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ao;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Local-only "

    :goto_0
    aput-object v0, v3, v4

    const/4 v0, 0x1

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->g:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v0, 0x2

    iget-wide v4, p0, Lcom/google/android/gms/drive/database/model/ao;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x3

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ao;->c:Ljava/lang/String;

    aput-object v4, v3, v0

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final u()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->O:Ljava/lang/Long;

    return-object v0
.end method

.method public final v()I
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w()Z
    .locals 1

    .prologue
    .line 523
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ao;->O:Ljava/lang/Long;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()Z
    .locals 1

    .prologue
    .line 527
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/ao;->v:Z

    return v0
.end method

.method public final y()V
    .locals 1

    .prologue
    .line 560
    sget-object v0, Lcom/google/android/gms/drive/database/model/ap;->c:Lcom/google/android/gms/drive/database/model/ap;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/database/model/ao;->a(Lcom/google/android/gms/drive/database/model/ap;)V

    .line 561
    return-void
.end method

.method public final z()V
    .locals 1

    .prologue
    .line 573
    sget-object v0, Lcom/google/android/gms/drive/database/model/ap;->b:Lcom/google/android/gms/drive/database/model/ap;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/database/model/ao;->a(Lcom/google/android/gms/drive/database/model/ap;)V

    .line 574
    return-void
.end method
