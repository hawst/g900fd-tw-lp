.class public Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;
.super Lcom/google/android/gms/plus/audience/ao;
.source "SourceFile"


# static fields
.field private static b:Landroid/graphics/Bitmap;


# instance fields
.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/ImageView;

.field private f:Lcom/google/android/gms/plus/audience/aq;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/audience/ao;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->f:Lcom/google/android/gms/plus/audience/aq;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->f:Lcom/google/android/gms/plus/audience/aq;

    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/aq;->a()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->f:Lcom/google/android/gms/plus/audience/aq;

    .line 49
    :cond_0
    sget v0, Lcom/google/android/gms/h;->Q:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a(I)V

    .line 51
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->a()V

    .line 52
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 81
    sget v0, Lcom/google/android/gms/h;->Q:I

    if-ne p1, v0, :cond_1

    .line 82
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->e:Landroid/widget/ImageView;

    sget-object v0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->b:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/h;->Q:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/ac;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->b:Landroid/graphics/Bitmap;

    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 86
    :goto_0
    return-void

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 75
    if-eqz p1, :cond_0

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 78
    :cond_0
    return-void
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/plus/audience/ap;)V
    .locals 0

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->a(Lcom/google/android/gms/plus/audience/ap;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/audience/aq;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->f:Lcom/google/android/gms/plus/audience/aq;

    .line 90
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    return-void
.end method

.method public final bridge synthetic a(Z)V
    .locals 0

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->a(Z)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    :cond_0
    return-void
.end method

.method public final bridge synthetic b(Z)V
    .locals 0

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->b(Z)V

    return-void
.end method

.method public final bridge synthetic c(Z)V
    .locals 0

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->c(Z)V

    return-void
.end method

.method public final bridge synthetic d(Z)V
    .locals 0

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->d(Z)V

    return-void
.end method

.method public final bridge synthetic d()Z
    .locals 1

    .prologue
    .line 20
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->d()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic e()V
    .locals 0

    .prologue
    .line 20
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->e()V

    return-void
.end method

.method public final e(Z)V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 70
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->d:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 72
    :cond_0
    return-void

    .line 70
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final bridge synthetic f()V
    .locals 0

    .prologue
    .line 20
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->f()V

    return-void
.end method

.method public final bridge synthetic g()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->g()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic isChecked()Z
    .locals 1

    .prologue
    .line 20
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->isChecked()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .prologue
    .line 20
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/audience/ao;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    return-void
.end method

.method public bridge synthetic onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->onClick(Landroid/view/View;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 35
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->onFinishInflate()V

    .line 37
    sget v0, Lcom/google/android/gms/j;->be:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->c:Landroid/widget/TextView;

    .line 38
    sget v0, Lcom/google/android/gms/j;->bf:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->d:Landroid/widget/TextView;

    .line 39
    sget v0, Lcom/google/android/gms/j;->bd:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->e:Landroid/widget/ImageView;

    .line 40
    return-void
.end method

.method public bridge synthetic setChecked(Z)V
    .locals 0

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->setChecked(Z)V

    return-void
.end method

.method public bridge synthetic toggle()V
    .locals 0

    .prologue
    .line 20
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->toggle()V

    return-void
.end method
