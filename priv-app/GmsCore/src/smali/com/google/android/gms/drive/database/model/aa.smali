.class public final enum Lcom/google/android/gms/drive/database/model/aa;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/aa;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/aa;

.field public static final enum c:Lcom/google/android/gms/drive/database/model/aa;

.field public static final enum d:Lcom/google/android/gms/drive/database/model/aa;

.field public static final enum e:Lcom/google/android/gms/drive/database/model/aa;

.field private static final synthetic g:[Lcom/google/android/gms/drive/database/model/aa;


# instance fields
.field private final f:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/16 v8, 0xf

    const/4 v7, 0x1

    .line 43
    new-instance v0, Lcom/google/android/gms/drive/database/model/aa;

    const-string v1, "ENTRY_ID"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/z;->d()Lcom/google/android/gms/drive/database/model/z;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "entryId"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v4

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/gms/drive/database/model/av;->a:Lcom/google/android/gms/drive/database/model/av;

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;Lcom/google/android/gms/drive/database/model/av;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lcom/google/android/gms/drive/database/model/aa;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/aa;->a:Lcom/google/android/gms/drive/database/model/aa;

    .line 56
    new-instance v0, Lcom/google/android/gms/drive/database/model/aa;

    const-string v1, "PACKAGING_ID"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/z;->d()Lcom/google/android/gms/drive/database/model/z;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "appId"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/gms/drive/database/model/aa;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/aa;->b:Lcom/google/android/gms/drive/database/model/aa;

    .line 63
    new-instance v0, Lcom/google/android/gms/drive/database/model/aa;

    const-string v1, "KEY"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/z;->d()Lcom/google/android/gms/drive/database/model/z;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "key"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    new-array v4, v10, [Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/drive/database/model/aa;->a:Lcom/google/android/gms/drive/database/model/aa;

    iget-object v5, v5, Lcom/google/android/gms/drive/database/model/aa;->f:Lcom/google/android/gms/drive/database/model/ab;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    sget-object v5, Lcom/google/android/gms/drive/database/model/aa;->b:Lcom/google/android/gms/drive/database/model/aa;

    iget-object v5, v5, Lcom/google/android/gms/drive/database/model/aa;->f:Lcom/google/android/gms/drive/database/model/ab;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a([Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lcom/google/android/gms/drive/database/model/aa;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/aa;->c:Lcom/google/android/gms/drive/database/model/aa;

    .line 74
    new-instance v0, Lcom/google/android/gms/drive/database/model/aa;

    const-string v1, "VISIBILITY"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/z;->d()Lcom/google/android/gms/drive/database/model/z;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "visibility"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, Lcom/google/android/gms/drive/database/model/aa;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/aa;->d:Lcom/google/android/gms/drive/database/model/aa;

    .line 82
    new-instance v0, Lcom/google/android/gms/drive/database/model/aa;

    const-string v1, "VALUE"

    const/4 v2, 0x4

    invoke-static {}, Lcom/google/android/gms/drive/database/model/z;->d()Lcom/google/android/gms/drive/database/model/z;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "value"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    iput-boolean v7, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/aa;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/aa;->e:Lcom/google/android/gms/drive/database/model/aa;

    .line 39
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/aa;

    sget-object v1, Lcom/google/android/gms/drive/database/model/aa;->a:Lcom/google/android/gms/drive/database/model/aa;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/gms/drive/database/model/aa;->b:Lcom/google/android/gms/drive/database/model/aa;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/gms/drive/database/model/aa;->c:Lcom/google/android/gms/drive/database/model/aa;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/gms/drive/database/model/aa;->d:Lcom/google/android/gms/drive/database/model/aa;

    aput-object v1, v0, v11

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/gms/drive/database/model/aa;->e:Lcom/google/android/gms/drive/database/model/aa;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/database/model/aa;->g:[Lcom/google/android/gms/drive/database/model/aa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 91
    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/aa;->f:Lcom/google/android/gms/drive/database/model/ab;

    .line 92
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/aa;
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/google/android/gms/drive/database/model/aa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/aa;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/aa;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/gms/drive/database/model/aa;->g:[Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/aa;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/aa;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/aa;->f:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/database/model/ab;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/aa;->f:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
