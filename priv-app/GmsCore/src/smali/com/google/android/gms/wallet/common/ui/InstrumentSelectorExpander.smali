.class public Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;
.super Lcom/google/android/gms/wallet/common/ui/bq;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/gms/wallet/common/ui/cc;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field a:Lcom/google/android/gms/wallet/common/ui/ce;

.field private g:Lcom/google/checkout/inapp/proto/j;

.field private h:Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/bq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-direct {v0, p1, p0, p2}, Lcom/google/android/gms/wallet/common/ui/ce;-><init>(Landroid/content/Context;Landroid/widget/AdapterView;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    .line 51
    invoke-super {p0, p0}, Lcom/google/android/gms/wallet/common/ui/bq;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 52
    return-void
.end method


# virtual methods
.method public final a(IZ)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 215
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 216
    instance-of v1, v0, Lcom/google/checkout/inapp/proto/j;

    if-eqz v1, :cond_3

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 218
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->g:Lcom/google/checkout/inapp/proto/j;

    if-eq v1, v0, :cond_4

    .line 220
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->g:Lcom/google/checkout/inapp/proto/j;

    if-eqz v1, :cond_0

    .line 221
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->getSelectedView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/gms/j;->qm:I

    invoke-static {v1, v2, v4, p2}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;IZZ)Z

    .line 225
    :cond_0
    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->g:Lcom/google/checkout/inapp/proto/j;

    .line 228
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->g:Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_1

    .line 229
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->getSelectedView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->qm:I

    invoke-static {v0, v1, v3, p2}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;IZZ)Z

    .line 231
    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->h(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 232
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->BM:I

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->g:Lcom/google/checkout/inapp/proto/j;

    iget-object v3, v3, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 238
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->l()V

    .line 243
    :cond_2
    :goto_1
    return-void

    .line 216
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 239
    :cond_4
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->g:Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_2

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->c:Lcom/google/android/gms/wallet/common/ui/bl;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/bl;->b()V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/cd;)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ce;->a(Lcom/google/android/gms/wallet/common/ui/cd;)V

    .line 171
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/j;)V
    .locals 6

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/wallet/common/ui/ce;->a(Lcom/google/checkout/inapp/proto/j;Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->getSelectedView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->getSelectedItemPosition()I

    move-result v3

    const-wide/16 v4, -0x1

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/wallet/common/ui/ce;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 68
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ce;->a(Z)V

    .line 96
    return-void
.end method

.method public final a([I)V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ce;->a([I)V

    .line 112
    return-void
.end method

.method public final a([Lcom/google/checkout/inapp/proto/j;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ce;->a([Lcom/google/checkout/inapp/proto/j;)V

    .line 89
    return-void
.end method

.method public final a([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ce;->a([Ljava/lang/String;)V

    .line 122
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ce;->a()Z

    move-result v0

    return v0
.end method

.method public final b([I)V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ce;->b([I)V

    .line 117
    return-void
.end method

.method public final b_(Z)V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ce;->b(Z)V

    .line 101
    return-void
.end method

.method protected final c()V
    .locals 4

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->g:Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_0

    .line 266
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->getSelectedView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->qm:I

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;IZZ)Z

    .line 268
    :cond_0
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ce;->c(Z)V

    .line 106
    return-void
.end method

.method protected final d()I
    .locals 4

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->getSelectedView()Landroid/view/View;

    move-result-object v1

    .line 277
    const/4 v0, 0x0

    .line 278
    if-eqz v1, :cond_1

    .line 280
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    .line 281
    sget v2, Lcom/google/android/gms/j;->ky:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->e:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->e:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->ky:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 284
    sget v2, Lcom/google/android/gms/j;->ky:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->e:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->ky:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 291
    :cond_0
    :goto_0
    return v0

    .line 287
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->e:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 289
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    neg-int v0, v0

    goto :goto_0
.end method

.method protected final e()I
    .locals 3

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    .line 298
    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->f:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 299
    add-int/lit8 v0, v0, -0x1

    .line 301
    :cond_0
    return v0
.end method

.method protected final f()I
    .locals 1

    .prologue
    .line 306
    sget v0, Lcom/google/android/gms/n;->B:I

    return v0
.end method

.method protected final g()I
    .locals 1

    .prologue
    .line 311
    sget v0, Lcom/google/android/gms/p;->Aq:I

    return v0
.end method

.method public getSelectedItem()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cg;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cg;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedItemPosition()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->g:Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cg;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->g:Lcom/google/checkout/inapp/proto/j;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/cg;->a(Lcom/google/checkout/inapp/proto/j;)I

    move-result v0

    .line 183
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 184
    add-int/lit8 v0, v0, 0x1

    .line 190
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->f:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    .line 253
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 254
    instance-of v1, v0, Lcom/google/checkout/inapp/proto/j;

    if-eqz v1, :cond_0

    .line 255
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/wallet/common/ui/ce;->a(Lcom/google/checkout/inapp/proto/j;Z)V

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->h:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz v0, :cond_1

    .line 258
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->h:Landroid/widget/AdapterView$OnItemClickListener;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 260
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/wallet/common/ui/ce;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 261
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->isEnabled()Z

    move-result v0

    if-eq p1, v0, :cond_0

    .line 127
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/bq;->setEnabled(Z)V

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cg;

    .line 129
    if-eqz v0, :cond_0

    .line 130
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/cg;->notifyDataSetChanged()V

    .line 133
    :cond_0
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->h:Landroid/widget/AdapterView$OnItemClickListener;

    .line 57
    return-void
.end method

.method public setSelection(I)V
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->a(IZ)V

    .line 206
    return-void
.end method
