.class final Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;
.super Ljava/io/FilterInputStream;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;

.field private final b:J

.field private final c:J

.field private d:J

.field private e:J

.field private f:Z


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;JJ)V
    .locals 3

    .prologue
    const-wide/16 v0, 0x0

    .line 28
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 29
    iput-object p2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;

    .line 30
    iput-wide p3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->b:J

    .line 31
    iput-wide p5, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->d:J

    .line 32
    iput-wide v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->e:J

    .line 35
    cmp-long v2, p3, v0

    if-lez v2, :cond_0

    const-wide/16 v0, 0xc8

    div-long v0, p3, v0

    :cond_0
    iput-wide v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->c:J

    .line 36
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 100
    invoke-static {}, Lcom/google/android/gms/drive/g/f;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    new-instance v0, Lcom/google/android/gms/drive/b/b/i;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - thread interrupted"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/b/b/i;-><init>(Ljava/lang/String;)V

    .line 103
    iget-wide v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->d:J

    long-to-int v1, v2

    iput v1, v0, Lcom/google/android/gms/drive/b/b/i;->bytesTransferred:I

    .line 104
    throw v0

    .line 106
    :cond_0
    return-void
.end method

.method private a(ZJ)V
    .locals 6

    .prologue
    .line 87
    if-eqz p1, :cond_0

    .line 88
    iget-wide v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->d:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->d:J

    .line 92
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->d:J

    iget-wide v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->e:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->c:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->d:J

    iget-wide v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->b:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;

    iget-wide v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->d:J

    iget-wide v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->b:J

    invoke-interface {v0, v2, v3, v4, v5}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;->a(JJ)V

    .line 95
    iget-wide v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->d:J

    iput-wide v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->e:J

    .line 97
    :cond_2
    return-void
.end method


# virtual methods
.method public final read()I
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 40
    iget-boolean v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->f:Z

    .line 41
    iput-boolean v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->f:Z

    .line 43
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->a()V

    .line 44
    invoke-super {p0}, Ljava/io/FilterInputStream;->read()I

    move-result v2

    .line 45
    invoke-direct {p0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->a()V

    .line 47
    if-nez v1, :cond_0

    :goto_0
    const-wide/16 v4, 0x1

    invoke-direct {p0, v0, v4, v5}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->a(ZJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    iput-boolean v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->f:Z

    return v2

    .line 47
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 48
    :catchall_0
    move-exception v0

    iput-boolean v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->f:Z

    throw v0
.end method

.method public final read([B)I
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 56
    iget-boolean v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->f:Z

    .line 57
    iput-boolean v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->f:Z

    .line 59
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->a()V

    .line 60
    invoke-super {p0, p1}, Ljava/io/FilterInputStream;->read([B)I

    move-result v2

    .line 61
    invoke-direct {p0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->a()V

    .line 63
    if-nez v1, :cond_0

    :goto_0
    int-to-long v4, v2

    invoke-direct {p0, v0, v4, v5}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->a(ZJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    iput-boolean v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->f:Z

    return v2

    .line 63
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v0

    iput-boolean v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->f:Z

    throw v0
.end method

.method public final read([BII)I
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 72
    iget-boolean v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->f:Z

    .line 73
    iput-boolean v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->f:Z

    .line 75
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->a()V

    .line 76
    invoke-super {p0, p1, p2, p3}, Ljava/io/FilterInputStream;->read([BII)I

    move-result v2

    .line 77
    invoke-direct {p0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->a()V

    .line 79
    if-nez v1, :cond_0

    :goto_0
    int-to-long v4, v2

    invoke-direct {p0, v0, v4, v5}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->a(ZJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    iput-boolean v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->f:Z

    return v2

    .line 79
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 80
    :catchall_0
    move-exception v0

    iput-boolean v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;->f:Z

    throw v0
.end method
