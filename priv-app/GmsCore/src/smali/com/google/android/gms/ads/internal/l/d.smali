.class public final Lcom/google/android/gms/ads/internal/l/d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# static fields
.field static final a:Ljava/util/Set;


# instance fields
.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:Z

.field private g:Ljava/lang/String;

.field private final h:Lcom/google/android/gms/ads/internal/p/a;

.field private final i:Ljava/util/Map;

.field private final j:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 62
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "top-left"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "top-right"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "top-center"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "center"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "bottom-left"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "bottom-right"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "bottom-center"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/google/android/gms/ads/internal/l/d;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/ads/internal/p/a;Ljava/util/Map;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput v0, p0, Lcom/google/android/gms/ads/internal/l/d;->b:I

    .line 75
    iput v0, p0, Lcom/google/android/gms/ads/internal/l/d;->c:I

    .line 76
    iput v1, p0, Lcom/google/android/gms/ads/internal/l/d;->d:I

    .line 77
    iput v1, p0, Lcom/google/android/gms/ads/internal/l/d;->e:I

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/l/d;->f:Z

    .line 79
    const-string v0, "top-right"

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->g:Ljava/lang/String;

    .line 86
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/l/d;->h:Lcom/google/android/gms/ads/internal/p/a;

    .line 87
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/l/d;->i:Ljava/util/Map;

    .line 88
    invoke-virtual {p1}, Lcom/google/android/gms/ads/internal/p/a;->k()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->j:Landroid/content/Context;

    .line 89
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/16 v5, 0x32

    const/4 v7, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 136
    const-string v0, "PLEASE IMPLEMENT mraid.resize()"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->j:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 139
    const-string v0, "Not an activity context. Cannot resize."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 198
    :goto_0
    return-void

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->h:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->e()Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-eqz v0, :cond_1

    .line 144
    const-string v0, "Is interstitial. Cannot resize an interstitial."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 148
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->h:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 149
    const-string v0, "Is expanded. Cannot resize an expanded banner."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 153
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->j:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/g;->d(Landroid/content/Context;)[I

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->i:Ljava/util/Map;

    const-string v4, "width"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->i:Ljava/util/Map;

    const-string v4, "width"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/g;->b(Ljava/lang/String;)I

    move-result v4

    aget v0, v3, v2

    if-lt v4, v5, :cond_9

    if-ge v4, v0, :cond_9

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    iput v4, p0, Lcom/google/android/gms/ads/internal/l/d;->b:I

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->i:Ljava/util/Map;

    const-string v4, "height"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->i:Ljava/util/Map;

    const-string v4, "height"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/g;->b(Ljava/lang/String;)I

    move-result v4

    aget v0, v3, v1

    if-lt v4, v5, :cond_a

    if-ge v4, v0, :cond_a

    move v0, v1

    :goto_2
    if-eqz v0, :cond_4

    iput v4, p0, Lcom/google/android/gms/ads/internal/l/d;->c:I

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->i:Ljava/util/Map;

    const-string v3, "offsetX"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->i:Ljava/util/Map;

    const-string v3, "offsetX"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/g;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/ads/internal/l/d;->d:I

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->i:Ljava/util/Map;

    const-string v3, "offsetY"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->i:Ljava/util/Map;

    const-string v3, "offsetY"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/g;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/ads/internal/l/d;->e:I

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->i:Ljava/util/Map;

    const-string v3, "allowOffscreen"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->i:Ljava/util/Map;

    const-string v3, "allowOffscreen"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/l/d;->f:Z

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->i:Ljava/util/Map;

    const-string v3, "customClosePosition"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    sget-object v3, Lcom/google/android/gms/ads/internal/l/d;->a:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->g:Ljava/lang/String;

    .line 154
    :cond_8
    iget v0, p0, Lcom/google/android/gms/ads/internal/l/d;->b:I

    if-ltz v0, :cond_b

    iget v0, p0, Lcom/google/android/gms/ads/internal/l/d;->c:I

    if-ltz v0, :cond_b

    move v0, v1

    :goto_3
    if-nez v0, :cond_c

    .line 155
    const-string v0, "Invalid width and height options. Cannot resize."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    move v0, v2

    .line 153
    goto/16 :goto_1

    :cond_a
    move v0, v2

    goto/16 :goto_2

    :cond_b
    move v0, v2

    .line 154
    goto :goto_3

    .line 159
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->j:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 161
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 162
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 165
    iget v0, p0, Lcom/google/android/gms/ads/internal/l/d;->b:I

    invoke-static {v3, v0}, Lcom/google/android/gms/ads/internal/util/client/a;->a(Landroid/util/DisplayMetrics;I)I

    move-result v0

    add-int/lit8 v4, v0, 0x10

    .line 166
    iget v0, p0, Lcom/google/android/gms/ads/internal/l/d;->c:I

    invoke-static {v3, v0}, Lcom/google/android/gms/ads/internal/util/client/a;->a(Landroid/util/DisplayMetrics;I)I

    move-result v0

    add-int/lit8 v3, v0, 0x10

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->h:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 170
    if-eqz v0, :cond_d

    instance-of v5, v0, Landroid/view/ViewGroup;

    if-eqz v5, :cond_d

    .line 171
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/google/android/gms/ads/internal/l/d;->h:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 174
    :cond_d
    new-instance v5, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->j:Landroid/content/Context;

    invoke-direct {v5, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 175
    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 176
    new-instance v6, Landroid/widget/PopupWindow;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->j:Landroid/content/Context;

    invoke-direct {v6, v0}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    .line 177
    invoke-virtual {v6, v3}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 178
    invoke-virtual {v6, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 182
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/l/d;->f:Z

    if-nez v0, :cond_e

    move v0, v1

    :goto_4
    invoke-virtual {v6, v0}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    .line 183
    invoke-virtual {v6, v5}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->h:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v5, v0, v7, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;II)V

    .line 188
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->j:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/ads/internal/l/d;->d:I

    iget v3, p0, Lcom/google/android/gms/ads/internal/l/d;->e:I

    invoke-virtual {v6, v0, v2, v1, v3}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/d;->h:Lcom/google/android/gms/ads/internal/p/a;

    new-instance v1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/l/d;->j:Landroid/content/Context;

    new-instance v3, Lcom/google/android/gms/ads/b;

    iget v4, p0, Lcom/google/android/gms/ads/internal/l/d;->b:I

    iget v5, p0, Lcom/google/android/gms/ads/internal/l/d;->c:I

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/ads/b;-><init>(II)V

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/b;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/p/a;->a(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V

    .line 193
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "x"

    iget v2, p0, Lcom/google/android/gms/ads/internal/l/d;->d:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "y"

    iget v2, p0, Lcom/google/android/gms/ads/internal/l/d;->e:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "width"

    iget v2, p0, Lcom/google/android/gms/ads/internal/l/d;->b:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "height"

    iget v2, p0, Lcom/google/android/gms/ads/internal/l/d;->c:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/l/d;->h:Lcom/google/android/gms/ads/internal/p/a;

    const-string v2, "onSizeChanged"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/ads/internal/p/a;->b(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 194
    :goto_5
    :try_start_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "state"

    const-string v2, "resized"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/l/d;->h:Lcom/google/android/gms/ads/internal/p/a;

    const-string v2, "onStateChanged"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/ads/internal/p/a;->b(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Error occured while dispatching state change."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_e
    move v0, v2

    .line 182
    goto :goto_4

    .line 193
    :catch_1
    move-exception v0

    const-string v1, "Error occured while dispatching size change."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5
.end method
