.class public final Lcom/google/android/gms/drive/database/model/bv;
.super Lcom/google/android/gms/drive/database/model/ad;
.source "SourceFile"


# instance fields
.field public final a:J

.field public b:J

.field private final c:Lcom/google/android/gms/drive/database/model/EntrySpec;

.field private final d:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/EntrySpec;JIJ)V
    .locals 2

    .prologue
    .line 31
    invoke-static {}, Lcom/google/android/gms/drive/database/model/bx;->a()Lcom/google/android/gms/drive/database/model/bx;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/database/model/ad;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)V

    .line 32
    iput-object p2, p0, Lcom/google/android/gms/drive/database/model/bv;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    .line 33
    iput-wide p3, p0, Lcom/google/android/gms/drive/database/model/bv;->a:J

    .line 34
    iput p5, p0, Lcom/google/android/gms/drive/database/model/bv;->d:I

    .line 35
    iput-wide p6, p0, Lcom/google/android/gms/drive/database/model/bv;->b:J

    .line 36
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/bv;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/bv;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 75
    :goto_0
    sget-object v1, Lcom/google/android/gms/drive/database/model/by;->a:Lcom/google/android/gms/drive/database/model/by;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/by;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 76
    sget-object v0, Lcom/google/android/gms/drive/database/model/by;->b:Lcom/google/android/gms/drive/database/model/by;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/by;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/drive/database/model/bv;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 77
    sget-object v0, Lcom/google/android/gms/drive/database/model/by;->f:Lcom/google/android/gms/drive/database/model/by;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/by;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/bv;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 78
    sget-object v0, Lcom/google/android/gms/drive/database/model/by;->g:Lcom/google/android/gms/drive/database/model/by;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/by;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/bv;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 79
    return-void

    .line 74
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Subscription [entrySpec="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/bv;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", appAuthMetadataId= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/bv;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", eventType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/drive/database/model/bv;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lastUpdateTime ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/bv;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
