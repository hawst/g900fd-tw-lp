.class public final Lcom/google/android/gms/plus/audience/t;
.super Lcom/google/android/gms/common/ui/widget/c;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/audience/o;

.field private final c:Ljava/util/List;

.field private final d:I

.field private final e:I

.field private final f:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/audience/o;Ljava/util/List;)V
    .locals 6

    .prologue
    .line 1182
    const/4 v3, 0x0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/audience/t;-><init>(Lcom/google/android/gms/plus/audience/o;Ljava/util/List;III)V

    .line 1183
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/plus/audience/o;Ljava/util/List;III)V
    .locals 1

    .prologue
    .line 1186
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/t;->a:Lcom/google/android/gms/plus/audience/o;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/ui/widget/c;-><init>(Lcom/google/android/gms/common/ui/widget/a;)V

    .line 1187
    iput-object p2, p0, Lcom/google/android/gms/plus/audience/t;->c:Ljava/util/List;

    .line 1188
    iput p3, p0, Lcom/google/android/gms/plus/audience/t;->d:I

    .line 1189
    sub-int v0, p4, p3

    iput v0, p0, Lcom/google/android/gms/plus/audience/t;->e:I

    .line 1190
    iput p5, p0, Lcom/google/android/gms/plus/audience/t;->f:I

    .line 1191
    return-void
.end method

.method private c(I)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 2

    .prologue
    .line 1221
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/t;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/android/gms/plus/audience/t;->d:I

    add-int/2addr v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1195
    iget v0, p0, Lcom/google/android/gms/plus/audience/t;->e:I

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 13

    .prologue
    .line 1201
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/audience/t;->c(I)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v1

    .line 1202
    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "selectionSource"

    iget v3, p0, Lcom/google/android/gms/plus/audience/t;->f:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1204
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/t;->a:Lcom/google/android/gms/plus/audience/o;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "secondaryText"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "contactsAvatarUri"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/plus/audience/t;->a:Lcom/google/android/gms/plus/audience/o;

    invoke-static {v7}, Lcom/google/android/gms/plus/audience/o;->d(Lcom/google/android/gms/plus/audience/o;)Lcom/google/android/gms/plus/audience/bg;

    move-result-object v7

    iget-object v7, v7, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    const-string v8, "Audience must not be null."

    invoke-static {v7, v8}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v8, "Audience member must not be null."

    invoke-static {v1, v8}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    sget v8, Lcom/google/android/gms/l;->dw:I

    const/4 v11, 0x0

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "checkboxEnabled"

    const/4 v12, 0x1

    invoke-virtual {v9, v10, v12}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    move-object v9, p2

    move/from16 v10, p4

    invoke-virtual/range {v0 .. v12}, Lcom/google/android/gms/plus/audience/o;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILandroid/view/View;ZZZ)Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1173
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/audience/t;->c(I)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 1216
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/t;->a:Lcom/google/android/gms/plus/audience/o;

    iget v0, v0, Lcom/google/android/gms/plus/audience/o;->l:I

    return v0
.end method
