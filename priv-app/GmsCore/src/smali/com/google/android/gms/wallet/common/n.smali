.class public final Lcom/google/android/gms/wallet/common/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/wallet/common/p;

.field private final b:Lcom/google/android/gms/wallet/common/o;

.field private final c:Lcom/google/android/gms/wallet/common/q;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/o;Lcom/google/android/gms/wallet/common/p;Lcom/google/android/gms/wallet/common/q;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/n;->b:Lcom/google/android/gms/wallet/common/o;

    .line 53
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/n;->a:Lcom/google/android/gms/wallet/common/p;

    .line 54
    iput-object p3, p0, Lcom/google/android/gms/wallet/common/n;->c:Lcom/google/android/gms/wallet/common/q;

    .line 55
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 80
    instance-of v1, p1, Lcom/google/android/gms/wallet/common/n;

    if-eqz v1, :cond_0

    .line 81
    check-cast p1, Lcom/google/android/gms/wallet/common/n;

    .line 82
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/n;->b:Lcom/google/android/gms/wallet/common/o;

    iget-object v2, p1, Lcom/google/android/gms/wallet/common/n;->b:Lcom/google/android/gms/wallet/common/o;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/n;->a:Lcom/google/android/gms/wallet/common/p;

    iget-object v2, p1, Lcom/google/android/gms/wallet/common/n;->a:Lcom/google/android/gms/wallet/common/p;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/n;->c:Lcom/google/android/gms/wallet/common/q;

    iget-object v2, p1, Lcom/google/android/gms/wallet/common/n;->c:Lcom/google/android/gms/wallet/common/q;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 86
    :cond_0
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "google/payments/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/n;->b:Lcom/google/android/gms/wallet/common/o;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/o;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/n;->a:Lcom/google/android/gms/wallet/common/p;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/p;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/n;->c:Lcom/google/android/gms/wallet/common/q;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/q;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
