.class public final Lcom/google/android/gms/icing/c/a/g;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile f:[Lcom/google/android/gms/icing/c/a/g;


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Z

.field public d:D

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4335
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 4336
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/g;->a:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/icing/c/a/g;->b:I

    iput-boolean v1, p0, Lcom/google/android/gms/icing/c/a/g;->c:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/c/a/g;->d:D

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/g;->e:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/g;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/g;->cachedSize:I

    .line 4337
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/c/a/g;
    .locals 2

    .prologue
    .line 4309
    sget-object v0, Lcom/google/android/gms/icing/c/a/g;->f:[Lcom/google/android/gms/icing/c/a/g;

    if-nez v0, :cond_1

    .line 4310
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 4312
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/c/a/g;->f:[Lcom/google/android/gms/icing/c/a/g;

    if-nez v0, :cond_0

    .line 4313
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/c/a/g;

    sput-object v0, Lcom/google/android/gms/icing/c/a/g;->f:[Lcom/google/android/gms/icing/c/a/g;

    .line 4315
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4317
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/c/a/g;->f:[Lcom/google/android/gms/icing/c/a/g;

    return-object v0

    .line 4315
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 4429
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 4430
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/g;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4431
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/g;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4434
    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/c/a/g;->b:I

    if-eqz v1, :cond_1

    .line 4435
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/icing/c/a/g;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4438
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/g;->c:Z

    if-eqz v1, :cond_2

    .line 4439
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/gms/icing/c/a/g;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4442
    :cond_2
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/g;->d:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 4444
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/g;->d:D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 4447
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/g;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 4448
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/g;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4451
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 4352
    if-ne p1, p0, :cond_1

    .line 4353
    const/4 v0, 0x1

    .line 4385
    :cond_0
    :goto_0
    return v0

    .line 4355
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/g;

    if-eqz v1, :cond_0

    .line 4358
    check-cast p1, Lcom/google/android/gms/icing/c/a/g;

    .line 4359
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/g;->a:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 4360
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/g;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4366
    :cond_2
    iget v1, p0, Lcom/google/android/gms/icing/c/a/g;->b:I

    iget v2, p1, Lcom/google/android/gms/icing/c/a/g;->b:I

    if-ne v1, v2, :cond_0

    .line 4369
    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/g;->c:Z

    iget-boolean v2, p1, Lcom/google/android/gms/icing/c/a/g;->c:Z

    if-ne v1, v2, :cond_0

    .line 4373
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/g;->d:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 4374
    iget-wide v4, p1, Lcom/google/android/gms/icing/c/a/g;->d:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 4378
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/g;->e:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 4379
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/g;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4385
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/g;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 4363
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/g;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/g;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 4382
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/g;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/g;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 4390
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/g;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 4393
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/icing/c/a/g;->b:I

    add-int/2addr v0, v2

    .line 4394
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/c/a/g;->c:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v2

    .line 4396
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/g;->d:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 4397
    mul-int/lit8 v0, v0, 0x1f

    const/16 v4, 0x20

    ushr-long v4, v2, v4

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 4399
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/g;->e:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 4401
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/g;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 4402
    return v0

    .line 4390
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/g;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 4394
    :cond_1
    const/16 v0, 0x4d5

    goto :goto_1

    .line 4399
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/g;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 4303
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/g;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/g;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/c/a/g;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/c/a/g;->c:Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/c/a/g;->d:D

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/g;->e:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x21 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 4408
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/g;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4409
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/g;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4411
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/c/a/g;->b:I

    if-eqz v0, :cond_1

    .line 4412
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/icing/c/a/g;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4414
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/icing/c/a/g;->c:Z

    if-eqz v0, :cond_2

    .line 4415
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/g;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 4417
    :cond_2
    iget-wide v0, p0, Lcom/google/android/gms/icing/c/a/g;->d:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 4419
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/g;->d:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 4421
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/g;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 4422
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/g;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4424
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4425
    return-void
.end method
