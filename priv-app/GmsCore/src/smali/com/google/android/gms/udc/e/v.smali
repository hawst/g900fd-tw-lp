.class public final Lcom/google/android/gms/udc/e/v;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 41
    iput v0, p0, Lcom/google/android/gms/udc/e/v;->a:I

    iput v0, p0, Lcom/google/android/gms/udc/e/v;->b:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/udc/e/v;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/udc/e/v;->cachedSize:I

    .line 42
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 93
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 94
    iget v1, p0, Lcom/google/android/gms/udc/e/v;->a:I

    if-eqz v1, :cond_0

    .line 95
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/udc/e/v;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    :cond_0
    iget v1, p0, Lcom/google/android/gms/udc/e/v;->b:I

    if-eqz v1, :cond_1

    .line 99
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/udc/e/v;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 54
    if-ne p1, p0, :cond_1

    .line 55
    const/4 v0, 0x1

    .line 67
    :cond_0
    :goto_0
    return v0

    .line 57
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/udc/e/v;

    if-eqz v1, :cond_0

    .line 60
    check-cast p1, Lcom/google/android/gms/udc/e/v;

    .line 61
    iget v1, p0, Lcom/google/android/gms/udc/e/v;->a:I

    iget v2, p1, Lcom/google/android/gms/udc/e/v;->a:I

    if-ne v1, v2, :cond_0

    .line 64
    iget v1, p0, Lcom/google/android/gms/udc/e/v;->b:I

    iget v2, p1, Lcom/google/android/gms/udc/e/v;->b:I

    if-ne v1, v2, :cond_0

    .line 67
    invoke-virtual {p0, p1}, Lcom/google/android/gms/udc/e/v;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 72
    iget v0, p0, Lcom/google/android/gms/udc/e/v;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 74
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/udc/e/v;->b:I

    add-int/2addr v0, v1

    .line 75
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/udc/e/v;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 9
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/udc/e/v;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/udc/e/v;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/udc/e/v;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 82
    iget v0, p0, Lcom/google/android/gms/udc/e/v;->a:I

    if-eqz v0, :cond_0

    .line 83
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/udc/e/v;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 85
    :cond_0
    iget v0, p0, Lcom/google/android/gms/udc/e/v;->b:I

    if-eqz v0, :cond_1

    .line 86
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/udc/e/v;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 88
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 89
    return-void
.end method
