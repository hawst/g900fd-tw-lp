.class public abstract Lcom/google/android/gms/auth/login/g;
.super Lcom/google/android/gms/auth/login/m;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# static fields
.field public static i:Ljava/lang/String;


# instance fields
.field protected a:Landroid/widget/FrameLayout;

.field protected b:Lcom/google/android/gms/auth/login/o;

.field protected c:Landroid/view/View;

.field protected d:Z

.field protected e:Landroid/widget/TextView;

.field protected f:I

.field protected g:I

.field protected h:Landroid/view/View$OnClickListener;

.field private q:Z

.field private r:Ljava/lang/Boolean;

.field private s:Landroid/view/View$OnClickListener;

.field private t:Landroid/widget/TextView$OnEditorActionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 667
    const-string v0, "SetupWizardAccountInfoSharedPrefs"

    sput-object v0, Lcom/google/android/gms/auth/login/g;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/m;-><init>()V

    .line 238
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/g;->d:Z

    .line 248
    const/16 v0, -0x65

    iput v0, p0, Lcom/google/android/gms/auth/login/g;->g:I

    .line 251
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/g;->q:Z

    .line 267
    new-instance v0, Lcom/google/android/gms/auth/login/h;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/login/h;-><init>(Lcom/google/android/gms/auth/login/g;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/g;->h:Landroid/view/View$OnClickListener;

    .line 301
    new-instance v0, Lcom/google/android/gms/auth/login/i;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/login/i;-><init>(Lcom/google/android/gms/auth/login/g;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/g;->s:Landroid/view/View$OnClickListener;

    .line 325
    new-instance v0, Lcom/google/android/gms/auth/login/j;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/login/j;-><init>(Lcom/google/android/gms/auth/login/g;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/g;->t:Landroid/widget/TextView$OnEditorActionListener;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/login/g;)Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/g;->q:Z

    return v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/16 v2, 0x2e

    .line 611
    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-lez v0, :cond_0

    const/16 v0, 0x40

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/auth/login/g;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/auth/login/g;->s:Landroid/view/View$OnClickListener;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 285
    if-eqz p1, :cond_0

    .line 286
    iget-object v0, p0, Lcom/google/android/gms/auth/login/g;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 289
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/g;->d:Z

    .line 291
    :cond_0
    return-void
.end method

.method protected final a(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 354
    if-eqz p2, :cond_0

    .line 355
    iput-object p1, p0, Lcom/google/android/gms/auth/login/g;->c:Landroid/view/View;

    .line 356
    :cond_0
    instance-of v0, p1, Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 357
    check-cast p1, Landroid/widget/EditText;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/g;->t:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 361
    :goto_0
    return-void

    .line 359
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/g;->s:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 545
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/g;->g()V

    .line 546
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 536
    return-void
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 299
    return-void
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 475
    const/4 v0, 0x0

    return v0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 371
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 372
    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    const/16 v4, 0x52

    if-ne v3, v4, :cond_3

    move v3, v1

    .line 373
    :goto_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_4

    move v4, v1

    .line 374
    :goto_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    const/16 v6, 0x18

    if-ne v5, v6, :cond_5

    move v5, v1

    .line 375
    :goto_3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0x19

    if-ne v6, v7, :cond_0

    move v2, v1

    .line 377
    :cond_0
    if-eqz v0, :cond_6

    .line 379
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/g;->d:Z

    if-eqz v0, :cond_1

    .line 380
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/m;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    .line 388
    :cond_1
    :goto_4
    return v1

    :cond_2
    move v0, v2

    .line 371
    goto :goto_0

    :cond_3
    move v3, v2

    .line 372
    goto :goto_1

    :cond_4
    move v4, v2

    .line 373
    goto :goto_2

    :cond_5
    move v5, v2

    .line 374
    goto :goto_3

    .line 382
    :cond_6
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v0

    if-eqz v0, :cond_7

    if-nez v3, :cond_7

    if-nez v4, :cond_7

    if-nez v5, :cond_7

    if-eqz v2, :cond_1

    .line 384
    :cond_7
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/m;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_4
.end method

.method public e()I
    .locals 1

    .prologue
    .line 487
    const/4 v0, 0x0

    return v0
.end method

.method protected final f()Landroid/view/View;
    .locals 2

    .prologue
    .line 530
    iget-object v0, p0, Lcom/google/android/gms/auth/login/g;->a:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/g;->a:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 723
    invoke-super {p0}, Lcom/google/android/gms/auth/login/m;->finish()V

    .line 724
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 725
    const/high16 v0, 0x10a0000

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/g;->overridePendingTransition(II)V

    .line 727
    :cond_0
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 554
    return-void
.end method

.method protected h()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 674
    invoke-static {}, Lcom/google/android/gms/auth/a/b;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 677
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/auth/a/b;->a(Landroid/view/ViewConfiguration;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final i()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 684
    iput v1, p0, Lcom/google/android/gms/auth/login/g;->g:I

    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/auth/login/g;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/g;->finish()V

    .line 685
    return-void
.end method

.method protected final j()V
    .locals 4

    .prologue
    const v3, 0x106000d

    .line 733
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 742
    :cond_0
    :goto_0
    return-void

    .line 737
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/g;->o:Z

    if-eqz v0, :cond_3

    .line 738
    iget-object v0, p0, Lcom/google/android/gms/auth/login/g;->r:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/g;->r:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/g;->r:Ljava/lang/Boolean;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/g;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/auth/login/k;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/auth/login/k;-><init>(Lcom/google/android/gms/auth/login/g;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v1

    or-int/lit16 v1, v1, 0x1602

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/g;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, -0x80000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/g;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Window;->setStatusBarColor(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/g;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Window;->setNavigationBarColor(I)V

    goto :goto_0

    .line 740
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/g;->k()V

    goto :goto_0
.end method

.method protected final k()V
    .locals 3

    .prologue
    .line 745
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 750
    :cond_0
    :goto_0
    return-void

    .line 749
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/g;->r:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/g;->r:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/g;->r:Ljava/lang/Boolean;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/g;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/auth/login/l;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/auth/login/l;-><init>(Lcom/google/android/gms/auth/login/g;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v1

    and-int/lit16 v1, v1, -0x1603

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/g;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, -0x80000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 426
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/m;->onCreate(Landroid/os/Bundle;)V

    .line 427
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/g;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472
    :goto_0
    return-void

    .line 433
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/g;->h()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/g;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 434
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/g;->requestWindowFeature(I)Z

    .line 435
    invoke-static {p0}, Lcom/google/android/gms/auth/a/b;->a(Landroid/app/Activity;)V

    .line 436
    invoke-static {p0}, Lcom/google/android/gms/auth/a/b;->b(Landroid/app/Activity;)V

    .line 445
    :goto_1
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/g;->a:Landroid/widget/FrameLayout;

    .line 449
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/g;->j()V

    .line 451
    if-eqz p1, :cond_1

    .line 452
    const-string v0, "nextRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/auth/login/g;->f:I

    .line 455
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-le v0, v1, :cond_4

    .line 456
    sget v0, Lcom/google/android/gms/j;->mw:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/g;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 457
    if-eqz v0, :cond_2

    .line 458
    sget v1, Lcom/google/android/gms/h;->b:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 460
    :cond_2
    sget v0, Lcom/google/android/gms/j;->bO:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/g;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 461
    if-eqz v0, :cond_3

    .line 462
    sget v1, Lcom/google/android/gms/h;->c:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 464
    :cond_3
    sget v0, Lcom/google/android/gms/j;->cs:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/g;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 465
    if-eqz v0, :cond_4

    .line 466
    sget v1, Lcom/google/android/gms/h;->c:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 471
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/auth/login/g;->a:Landroid/widget/FrameLayout;

    invoke-super {p0, v0}, Lcom/google/android/gms/auth/login/m;->setContentView(Landroid/view/View;)V

    goto :goto_0

    .line 440
    :cond_5
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/g;->requestWindowFeature(I)Z

    goto :goto_1
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 393
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/m;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 394
    const-string v0, "currentFocus"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 395
    if-eq v0, v1, :cond_0

    .line 396
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/g;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 397
    if-eqz v0, :cond_0

    .line 398
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 400
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 404
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/m;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 405
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/g;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 406
    const-string v1, "currentFocus"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 407
    const-string v0, "nextRequest"

    iget v1, p0, Lcom/google/android/gms/auth/login/g;->f:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 408
    return-void

    .line 406
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 541
    return-void
.end method

.method public setContentView(I)V
    .locals 2

    .prologue
    .line 518
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/g;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 519
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 520
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/g;->setContentView(Landroid/view/View;)V

    .line 521
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 525
    iget-object v0, p0, Lcom/google/android/gms/auth/login/g;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 526
    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/g;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/g;->e:Landroid/widget/TextView;

    .line 527
    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .prologue
    .line 495
    invoke-virtual {p0, p1}, Lcom/google/android/gms/auth/login/g;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/g;->setTitle(Ljava/lang/CharSequence;)V

    .line 496
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 500
    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/g;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/g;->e:Landroid/widget/TextView;

    .line 501
    iget-object v0, p0, Lcom/google/android/gms/auth/login/g;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 502
    iget-object v0, p0, Lcom/google/android/gms/auth/login/g;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 506
    :goto_0
    return-void

    .line 505
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/m;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 707
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/m;->startActivity(Landroid/content/Intent;)V

    .line 708
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 709
    const/high16 v0, 0x10a0000

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/g;->overridePendingTransition(II)V

    .line 711
    :cond_0
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 715
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/auth/login/m;->startActivityForResult(Landroid/content/Intent;I)V

    .line 716
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 717
    const/high16 v0, 0x10a0000

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/g;->overridePendingTransition(II)V

    .line 719
    :cond_0
    return-void
.end method
