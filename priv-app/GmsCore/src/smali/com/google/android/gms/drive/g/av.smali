.class public final Lcom/google/android/gms/drive/g/av;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final f:J


# instance fields
.field private final a:J

.field private final b:Landroid/text/format/Time;

.field private final c:Landroid/content/Context;

.field private final d:Landroid/text/format/Time;

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 33
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x6

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/drive/g/av;->f:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/text/format/Time;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/gms/drive/g/av;->c:Landroid/content/Context;

    .line 40
    iput-object p2, p0, Lcom/google/android/gms/drive/g/av;->b:Landroid/text/format/Time;

    .line 41
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/g/av;->a:J

    .line 42
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/av;->d:Landroid/text/format/Time;

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/drive/g/av;->c:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->fW:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/g/av;->e:Ljava/lang/String;

    .line 44
    return-void
.end method


# virtual methods
.method public final a(J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/drive/g/av;->d:Landroid/text/format/Time;

    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/drive/g/av;->d:Landroid/text/format/Time;

    invoke-static {v0}, Landroid/text/format/Time;->isEpoch(Landroid/text/format/Time;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/drive/g/av;->e:Ljava/lang/String;

    .line 82
    :goto_0
    return-object v0

    .line 66
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/drive/g/av;->a:J

    sget-wide v2, Lcom/google/android/gms/drive/g/av;->f:J

    sub-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_3

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/drive/g/av;->d:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->year:I

    iget-object v1, p0, Lcom/google/android/gms/drive/g/av;->b:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->year:I

    if-eq v0, v1, :cond_2

    .line 73
    const v0, 0x10a14

    .line 82
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/drive/g/av;->c:Landroid/content/Context;

    invoke-static {v1, p1, p2, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 66
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 74
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/g/av;->d:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->yearDay:I

    iget-object v1, p0, Lcom/google/android/gms/drive/g/av;->b:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->yearDay:I

    if-eq v0, v1, :cond_3

    .line 76
    const v0, 0x10a18

    goto :goto_2

    .line 79
    :cond_3
    const v0, 0x10a01

    goto :goto_2
.end method
