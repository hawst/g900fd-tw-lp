.class public abstract Lcom/google/android/gms/googlehelp/d/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Landroid/database/sqlite/SQLiteDatabase;

.field private b:I

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput v0, p0, Lcom/google/android/gms/googlehelp/d/a;->b:I

    .line 37
    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/d/a;->c:Z

    .line 39
    return-void
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/a;->a:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/a;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected abstract a()Landroid/database/sqlite/SQLiteDatabase;
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 47
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/d/a;->d()Z

    move-result v0

    .line 48
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/d/a;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/a;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/googlehelp/d/a;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 50
    iget v1, p0, Lcom/google/android/gms/googlehelp/d/a;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/gms/googlehelp/d/a;->b:I

    .line 57
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/googlehelp/d/a;->c:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_2

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/a;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->acquireReference()V

    .line 59
    iget v0, p0, Lcom/google/android/gms/googlehelp/d/a;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/googlehelp/d/a;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    :cond_2
    monitor-exit p0

    return-void

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 68
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/d/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    iget v0, p0, Lcom/google/android/gms/googlehelp/d/a;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/googlehelp/d/a;->b:I

    if-nez v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/a;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 74
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/d/a;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 76
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/a;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->releaseReference()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
