.class public final Lcom/google/android/gms/wallet/common/ui/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final a:Landroid/widget/EditText;

.field private final b:Lcom/google/android/gms/wallet/common/ui/am;

.field private final c:Lcom/google/android/gms/wallet/common/ui/dn;

.field private final d:Z

.field private final e:Landroid/os/Handler;

.field private f:Lcom/google/android/gms/wallet/common/ui/ah;


# direct methods
.method public constructor <init>(Landroid/widget/EditText;Lcom/google/android/gms/wallet/common/ui/am;Lcom/google/android/gms/wallet/common/ui/dn;Z)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/a/b;->a:Landroid/widget/EditText;

    .line 38
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ui/a/b;->b:Lcom/google/android/gms/wallet/common/ui/am;

    .line 39
    iput-object p3, p0, Lcom/google/android/gms/wallet/common/ui/a/b;->c:Lcom/google/android/gms/wallet/common/ui/dn;

    .line 40
    iput-boolean p4, p0, Lcom/google/android/gms/wallet/common/ui/a/b;->d:Z

    .line 41
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/a/b;->e:Landroid/os/Handler;

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/common/ui/ah;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/a/b;->f:Lcom/google/android/gms/wallet/common/ui/ah;

    .line 88
    return-void
.end method

.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 80
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 84
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/a/b;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-nez v0, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    if-lez p4, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/a/b;->b:Lcom/google/android/gms/wallet/common/ui/am;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/am;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/a/b;->c:Lcom/google/android/gms/wallet/common/ui/dn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/a/b;->c:Lcom/google/android/gms/wallet/common/ui/dn;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/dn;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/a/b;->f:Lcom/google/android/gms/wallet/common/ui/ah;

    if-eqz v0, :cond_2

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/a/b;->f:Lcom/google/android/gms/wallet/common/ui/ah;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/a/b;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ah;->a(I)V

    .line 55
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/a/b;->a:Landroid/widget/EditText;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    .line 56
    if-eqz v0, :cond_0

    .line 57
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/a/b;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/util/e;->h(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 58
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/a/b;->e:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/wallet/common/ui/a/c;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/wallet/common/ui/a/c;-><init>(Lcom/google/android/gms/wallet/common/ui/a/b;Landroid/view/View;)V

    const-wide/16 v4, 0x2ee

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 65
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    .line 69
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/a/b;->d:Z

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/a/b;->a:Landroid/widget/EditText;

    const/16 v1, 0x21

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    .line 72
    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0
.end method
