.class public final Lcom/google/android/gms/audiomodem/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:[Landroid/media/AudioTrack;

.field final b:Landroid/media/AudioManager;

.field final c:Landroid/os/Handler;

.field final d:[Landroid/os/Handler;

.field final e:Lcom/google/android/gms/audiomodem/x;

.field f:[Z

.field volatile g:[Z

.field volatile h:J

.field i:[I

.field j:I

.field private final k:Landroid/content/Context;

.field private l:Z

.field private m:Z

.field private final n:Landroid/content/IntentFilter;

.field private final o:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/audiomodem/x;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Lcom/google/android/gms/audiomodem/u;

    invoke-direct {v0, p0}, Lcom/google/android/gms/audiomodem/u;-><init>(Lcom/google/android/gms/audiomodem/t;)V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/t;->o:Landroid/content/BroadcastReceiver;

    .line 95
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/t;->k:Landroid/content/Context;

    .line 96
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/t;->b:Landroid/media/AudioManager;

    .line 98
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/t;->c:Landroid/os/Handler;

    .line 100
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/t;->n:Landroid/content/IntentFilter;

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/t;->n:Landroid/content/IntentFilter;

    const-string v1, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 103
    new-array v0, v4, [Landroid/media/AudioTrack;

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/t;->a:[Landroid/media/AudioTrack;

    .line 104
    new-array v0, v4, [Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/t;->d:[Landroid/os/Handler;

    .line 105
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    .line 106
    new-instance v1, Landroid/os/HandlerThread;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AudioTrackThread"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 107
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 108
    iget-object v2, p0, Lcom/google/android/gms/audiomodem/t;->d:[Landroid/os/Handler;

    new-instance v3, Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v3, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    aput-object v3, v2, v0

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 111
    :cond_0
    iput-object p2, p0, Lcom/google/android/gms/audiomodem/t;->e:Lcom/google/android/gms/audiomodem/x;

    .line 113
    new-array v0, v4, [Z

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/t;->f:[Z

    .line 114
    new-array v0, v4, [Z

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/t;->g:[Z

    .line 115
    new-array v0, v4, [I

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/t;->i:[I

    .line 116
    return-void
.end method

.method public static a(I)Landroid/media/AudioTrack;
    .locals 7

    .prologue
    const/4 v3, 0x4

    const/4 v4, 0x2

    .line 122
    new-instance v0, Landroid/media/AudioTrack;

    const/4 v1, 0x3

    invoke-static {p0, v3, v4}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v5

    const/4 v6, 0x1

    move v2, p0

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    .line 129
    invoke-virtual {v0}, Landroid/media/AudioTrack;->getState()I

    move-result v1

    if-nez v1, :cond_1

    .line 130
    const-string v1, "audioModem"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 131
    const-string v1, "AudioPlayer: Failed to initialize AudioTrack"

    const-string v2, "audioModem"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    :cond_0
    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V

    .line 134
    const/4 v0, 0x0

    .line 136
    :cond_1
    return-object v0
.end method

.method private c()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 244
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/audiomodem/t;->a:[Landroid/media/AudioTrack;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 245
    iget-object v2, p0, Lcom/google/android/gms/audiomodem/t;->f:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/t;->i:[I

    aget v2, v2, v0

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 246
    const/4 v1, 0x1

    .line 249
    :cond_0
    return v1

    .line 244
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method final a()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v3, -0x1

    const/4 v5, 0x3

    const/4 v1, 0x0

    .line 291
    move v0, v1

    move v2, v3

    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/audiomodem/t;->a:[Landroid/media/AudioTrack;

    array-length v4, v4

    if-ge v0, v4, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/audiomodem/t;->f:[Z

    aget-boolean v4, v4, v0

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/gms/audiomodem/t;->i:[I

    aget v4, v4, v0

    if-ne v4, v3, :cond_4

    move v2, v3

    :cond_0
    if-ne v2, v3, :cond_6

    iget-boolean v0, p0, Lcom/google/android/gms/audiomodem/t;->l:Z

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/google/android/gms/audiomodem/t;->l:Z

    iget-object v0, p0, Lcom/google/android/gms/audiomodem/t;->b:Landroid/media/AudioManager;

    iget v2, p0, Lcom/google/android/gms/audiomodem/t;->j:I

    invoke-virtual {v0, v5, v2, v1}, Landroid/media/AudioManager;->setStreamVolume(III)V

    const-string v0, "audioModem"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "AudioPlayer: Setting stream volume to "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/audiomodem/t;->j:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "audioModem"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/audiomodem/t;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/google/android/gms/audiomodem/t;->m:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/audiomodem/t;->k:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/audiomodem/t;->o:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/t;->n:Landroid/content/IntentFilter;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/audiomodem/t;->c:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    iput-boolean v6, p0, Lcom/google/android/gms/audiomodem/t;->m:Z

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/audiomodem/t;->b()V

    .line 293
    :cond_3
    :goto_2
    return-void

    .line 291
    :cond_4
    iget-object v4, p0, Lcom/google/android/gms/audiomodem/t;->i:[I

    aget v4, v4, v0

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/t;->b:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    if-nez v0, :cond_1

    iput-boolean v6, p0, Lcom/google/android/gms/audiomodem/t;->l:Z

    iget-object v0, p0, Lcom/google/android/gms/audiomodem/t;->b:Landroid/media/AudioManager;

    invoke-virtual {v0, v5}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/audiomodem/t;->j:I

    iget-object v0, p0, Lcom/google/android/gms/audiomodem/t;->b:Landroid/media/AudioManager;

    invoke-virtual {v0, v5, v2, v1}, Landroid/media/AudioManager;->setStreamVolume(III)V

    const-string v0, "audioModem"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "AudioPlayer: Setting stream volume to "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "audioModem"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 292
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/gms/audiomodem/t;->m:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/audiomodem/t;->k:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/t;->o:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-boolean v1, p0, Lcom/google/android/gms/audiomodem/t;->m:Z

    goto :goto_2
.end method

.method final b()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    .line 351
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/t;->b:Landroid/media/AudioManager;

    invoke-virtual {v0, v8}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    int-to-float v1, v0

    .line 352
    const/4 v0, 0x0

    cmpl-float v0, v1, v0

    if-nez v0, :cond_1

    .line 369
    :cond_0
    return-void

    .line 355
    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/audiomodem/t;->a:[Landroid/media/AudioTrack;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 356
    iget-object v2, p0, Lcom/google/android/gms/audiomodem/t;->f:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/t;->i:[I

    aget v2, v2, v0

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 357
    iget-object v2, p0, Lcom/google/android/gms/audiomodem/t;->a:[Landroid/media/AudioTrack;

    aget-object v2, v2, v0

    .line 360
    iget-object v3, p0, Lcom/google/android/gms/audiomodem/t;->i:[I

    aget v3, v3, v0

    int-to-float v3, v3

    div-float/2addr v3, v1

    .line 361
    float-to-double v4, v3

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpg-double v4, v4, v6

    if-gtz v4, :cond_2

    .line 362
    invoke-virtual {v2, v3, v3}, Landroid/media/AudioTrack;->setStereoVolume(FF)I

    .line 363
    const-string v2, "audioModem"

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 364
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "AudioPlayer: Setting stereo volume on track "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " to "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "audioModem"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/t;->f:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/t;->g:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p1

    .line 146
    :cond_0
    return-void
.end method
