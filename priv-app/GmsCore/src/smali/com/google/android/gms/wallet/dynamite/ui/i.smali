.class final Lcom/google/android/gms/wallet/dynamite/ui/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/dynamite/ui/b;


# instance fields
.field a:Ljava/lang/String;

.field b:Landroid/graphics/Paint;

.field c:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;Landroid/graphics/Canvas;I)V
    .locals 4

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/i;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    .line 81
    iget v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/i;->c:I

    int-to-float v1, v1

    iget v2, v0, Landroid/graphics/Paint$FontMetrics;->descent:F

    sub-float/2addr v1, v2

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 82
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/i;->a:Ljava/lang/String;

    int-to-float v2, p3

    int-to-float v0, v0

    iget-object v3, p0, Lcom/google/android/gms/wallet/dynamite/ui/i;->b:Landroid/graphics/Paint;

    invoke-virtual {p2, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 83
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/i;->a:Ljava/lang/String;

    return-object v0
.end method
