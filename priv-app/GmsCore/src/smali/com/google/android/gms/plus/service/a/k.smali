.class public final Lcom/google/android/gms/plus/service/a/k;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/plus/internal/f;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/f;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/k;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 32
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/k;->b:Lcom/google/android/gms/plus/internal/f;

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 38
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/k;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/plus/b/b;->a(Lcom/google/android/gms/common/server/ClientContext;)I

    move-result v0

    .line 39
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/k;->b:Lcom/google/android/gms/plus/internal/f;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;I)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    .line 60
    :goto_0
    return-void

    .line 40
    :catch_0
    move-exception v0

    .line 41
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 42
    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v5, v0, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 43
    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/k;->b:Lcom/google/android/gms/plus/internal/f;

    invoke-interface {v0, v6, v1, v4}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;I)V

    goto :goto_0

    .line 49
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/k;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/k;->b:Lcom/google/android/gms/plus/internal/f;

    invoke-interface {v1, v6, v0, v4}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;I)V

    goto :goto_0

    .line 55
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/k;->b:Lcom/google/android/gms/plus/internal/f;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v7, v4}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 4

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/k;->b:Lcom/google/android/gms/plus/internal/f;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/k;->b:Lcom/google/android/gms/plus/internal/f;

    const/16 v1, 0x8

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;I)V

    .line 71
    :cond_0
    return-void
.end method
