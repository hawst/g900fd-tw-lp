.class public final Lcom/google/android/gms/plus/service/a/u;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/plus/internal/f;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Landroid/net/Uri;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/f;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/u;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 40
    iput p2, p0, Lcom/google/android/gms/plus/service/a/u;->c:I

    .line 41
    iput-object p3, p0, Lcom/google/android/gms/plus/service/a/u;->d:Ljava/lang/String;

    .line 42
    iput-object p4, p0, Lcom/google/android/gms/plus/service/a/u;->e:Landroid/net/Uri;

    .line 43
    iput-object p5, p0, Lcom/google/android/gms/plus/service/a/u;->f:Ljava/lang/String;

    .line 44
    iput-object p6, p0, Lcom/google/android/gms/plus/service/a/u;->g:Ljava/lang/String;

    .line 45
    iput-object p7, p0, Lcom/google/android/gms/plus/service/a/u;->h:Ljava/lang/String;

    .line 46
    iput-object p8, p0, Lcom/google/android/gms/plus/service/a/u;->b:Lcom/google/android/gms/plus/internal/f;

    .line 47
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 52
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/u;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget v3, p0, Lcom/google/android/gms/plus/service/a/u;->c:I

    iget-object v4, p0, Lcom/google/android/gms/plus/service/a/u;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/plus/service/a/u;->e:Landroid/net/Uri;

    iget-object v6, p0, Lcom/google/android/gms/plus/service/a/u;->f:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/plus/service/a/u;->g:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/gms/plus/service/a/u;->h:Ljava/lang/String;

    iget-object v0, p2, Lcom/google/android/gms/plus/b/b;->c:Lcom/google/android/gms/plus/b/n;

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/plus/b/n;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/u;->b:Lcom/google/android/gms/plus/internal/f;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    .line 66
    :goto_0
    return-void

    .line 55
    :catch_0
    move-exception v0

    .line 56
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 57
    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v10, v0, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 58
    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/u;->b:Lcom/google/android/gms/plus/internal/f;

    invoke-interface {v0, v11, v1, v9}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V

    goto :goto_0

    .line 61
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/u;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/u;->b:Lcom/google/android/gms/plus/internal/f;

    invoke-interface {v1, v11, v0, v9}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V

    goto :goto_0

    .line 64
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/u;->b:Lcom/google/android/gms/plus/internal/f;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v9, v9}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/u;->b:Lcom/google/android/gms/plus/internal/f;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/u;->b:Lcom/google/android/gms/plus/internal/f;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V

    .line 73
    :cond_0
    return-void
.end method
