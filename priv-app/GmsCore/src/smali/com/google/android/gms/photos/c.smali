.class public final Lcom/google/android/gms/photos/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/autobackup/a;


# instance fields
.field private final a:Lcom/google/android/gms/photos/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-class v0, Lcom/google/android/gms/photos/e;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/e;

    iput-object v0, p0, Lcom/google/android/gms/photos/c;->a:Lcom/google/android/gms/photos/e;

    .line 20
    return-void
.end method

.method private static d(I)Lcom/google/android/gms/photos/a/a/a/c;
    .locals 3

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/gms/photos/a/a/a/c;

    invoke-direct {v0}, Lcom/google/android/gms/photos/a/a/a/c;-><init>()V

    .line 45
    new-instance v1, Lcom/google/android/gms/photos/a/a/a/b;

    invoke-direct {v1}, Lcom/google/android/gms/photos/a/a/a/b;-><init>()V

    iput-object v1, v0, Lcom/google/android/gms/photos/a/a/a/c;->a:Lcom/google/android/gms/photos/a/a/a/b;

    .line 46
    iget-object v1, v0, Lcom/google/android/gms/photos/a/a/a/c;->a:Lcom/google/android/gms/photos/a/a/a/b;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/photos/a/a/a/b;->a:Ljava/lang/Integer;

    .line 47
    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/photos/c;->a:Lcom/google/android/gms/photos/e;

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/google/android/gms/photos/c;->d(I)Lcom/google/android/gms/photos/a/a/a/c;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/photos/e;->a(ILcom/google/android/gms/photos/a/a/a/c;)V

    .line 27
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/photos/c;->a:Lcom/google/android/gms/photos/e;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/google/android/gms/photos/c;->d(I)Lcom/google/android/gms/photos/a/a/a/c;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/photos/e;->a(ILcom/google/android/gms/photos/a/a/a/c;)V

    .line 34
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/photos/c;->a:Lcom/google/android/gms/photos/e;

    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/gms/photos/c;->d(I)Lcom/google/android/gms/photos/a/a/a/c;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/photos/e;->a(ILcom/google/android/gms/photos/a/a/a/c;)V

    .line 41
    return-void
.end method
