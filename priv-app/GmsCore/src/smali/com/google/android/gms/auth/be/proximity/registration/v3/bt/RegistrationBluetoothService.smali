.class public Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/util/UUID;

.field private static final c:Ljava/lang/String;


# instance fields
.field private d:Lcom/google/android/gms/auth/be/proximity/f;

.field private e:Landroid/app/KeyguardManager;

.field private f:Landroid/app/NotificationManager;

.field private g:Lcom/google/android/gms/auth/g/a;

.field private h:Lcom/google/android/gms/auth/gencode/authzen/server/api/f;

.field private i:Lcom/google/android/gms/auth/authzen/keyservice/g;

.field private j:Lcom/google/android/gms/auth/be/proximity/g;

.field private k:Lcom/google/android/gms/auth/trustagent/at;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    .line 56
    const-string v0, "29422880-D56D-11E3-9C1A-0800200C9A66"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->b:Ljava/util/UUID;

    .line 59
    const-class v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 137
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/gms/auth/be/proximity/f;Landroid/app/KeyguardManager;Landroid/app/NotificationManager;Lcom/google/android/gms/auth/g/a;Lcom/google/android/gms/auth/gencode/authzen/server/api/f;Lcom/google/android/gms/auth/authzen/keyservice/g;Lcom/google/android/gms/auth/be/proximity/g;Lcom/google/android/gms/auth/trustagent/at;)V
    .locals 1

    .prologue
    .line 149
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 151
    iput-object p1, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->d:Lcom/google/android/gms/auth/be/proximity/f;

    .line 152
    iput-object p2, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->e:Landroid/app/KeyguardManager;

    .line 153
    iput-object p3, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->f:Landroid/app/NotificationManager;

    .line 154
    iput-object p4, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->g:Lcom/google/android/gms/auth/g/a;

    .line 155
    iput-object p5, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->h:Lcom/google/android/gms/auth/gencode/authzen/server/api/f;

    .line 156
    iput-object p6, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->i:Lcom/google/android/gms/auth/authzen/keyservice/g;

    .line 157
    iput-object p7, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->j:Lcom/google/android/gms/auth/be/proximity/g;

    .line 158
    iput-object p8, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->k:Lcom/google/android/gms/auth/trustagent/at;

    .line 159
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 90
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 91
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    const-string v1, "bluetooth_mac_address"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    const-string v1, "turn_bluetooth_off_on_registration_failure"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 96
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;[B)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 112
    :try_start_0
    new-instance v0, Lcom/google/ab/b/a/e/r;

    invoke-direct {v0}, Lcom/google/ab/b/a/e/r;-><init>()V

    array-length v2, p2

    invoke-virtual {v0, p2, v2}, Lcom/google/protobuf/a/f;->a([BI)Lcom/google/protobuf/a/f;

    move-result-object v0

    check-cast v0, Lcom/google/ab/b/a/e/r;
    :try_end_0
    .catch Lcom/google/protobuf/a/e; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    iget-boolean v2, v0, Lcom/google/ab/b/a/e/r;->a:Z

    if-nez v2, :cond_0

    .line 120
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v2, "Missing Bluetooth address in DeviceProximityCallback."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 123
    :goto_0
    return-object v0

    .line 114
    :catch_0
    move-exception v0

    .line 115
    sget-object v2, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v3, "Unable to decode the proto."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 116
    goto :goto_0

    .line 123
    :cond_0
    iget-object v0, v0, Lcom/google/ab/b/a/e/r;->b:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method private a()Lcom/google/android/gms/auth/be/proximity/registration/v3/a;
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 414
    const/4 v0, 0x2

    .line 415
    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->g:Lcom/google/android/gms/auth/g/a;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_0

    .line 416
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->e:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 422
    :cond_0
    :goto_0
    const/16 v2, 0xc

    .line 423
    iget-object v3, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->k:Lcom/google/android/gms/auth/trustagent/at;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/trustagent/at;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 426
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->k:Lcom/google/android/gms/auth/trustagent/at;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/trustagent/at;->b()Z
    :try_end_0
    .catch Lcom/google/android/gms/auth/trustagent/az; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 431
    :goto_1
    if-eqz v1, :cond_2

    const/16 v1, 0xa

    .line 435
    :goto_2
    invoke-static {v0, v1}, Lcom/google/android/gms/auth/be/proximity/registration/v3/a;->a(II)Lcom/google/android/gms/auth/be/proximity/registration/v3/a;

    move-result-object v0

    return-object v0

    .line 416
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 427
    :catch_0
    move-exception v2

    .line 428
    sget-object v3, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v4, "Cannot get coffee enabled state."

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 431
    :cond_2
    const/16 v1, 0xb

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method private a(II)V
    .locals 5

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->f:Landroid/app/NotificationManager;

    sget-object v1, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->c:Ljava/lang/String;

    const/4 v2, 0x1

    new-instance v3, Landroid/support/v4/app/bk;

    invoke-direct {v3, p0}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/google/android/gms/h;->j:I

    invoke-virtual {v3, v4}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v3

    invoke-virtual {p0, p1}, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v3

    invoke-virtual {p0, p2}, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 301
    return-void
.end method

.method private a(Lcom/google/android/gms/auth/testability/android/bluetooth/BluetoothDevice;Lcom/google/android/gms/auth/testability/android/bluetooth/g;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 311
    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/gms/auth/testability/android/bluetooth/g;->b()Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/auth/be/proximity/registration/v3/e;->a(Ljava/io/InputStream;)Lcom/google/android/gms/auth/be/proximity/registration/v3/d;

    move-result-object v2

    .line 312
    invoke-virtual {p2}, Lcom/google/android/gms/auth/testability/android/bluetooth/g;->c()Ljava/io/OutputStream;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/auth/be/proximity/registration/v3/g;->a(Ljava/io/OutputStream;)Lcom/google/android/gms/auth/be/proximity/registration/v3/f;

    move-result-object v3

    .line 313
    sget-object v1, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v4, "Connection to %s created."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/auth/testability/android/bluetooth/BluetoothDevice;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 322
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a()Lcom/google/android/gms/auth/be/proximity/registration/v3/a;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/google/android/gms/auth/be/proximity/registration/v3/f;->a(Lcom/google/android/gms/auth/be/proximity/registration/v3/h;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 330
    :try_start_2
    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/proximity/registration/v3/d;->a()Lcom/google/android/gms/auth/be/proximity/registration/v3/c;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    .line 336
    if-nez v1, :cond_0

    .line 337
    :try_start_3
    sget-object v1, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v4, "Failed to read message via Bluetooth."

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 338
    :try_start_4
    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/proximity/registration/v3/d;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_9

    .line 361
    :goto_0
    :try_start_5
    invoke-virtual {v3}, Lcom/google/android/gms/auth/be/proximity/registration/v3/f;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_a

    .line 364
    :goto_1
    return v0

    .line 314
    :catch_0
    move-exception v1

    .line 315
    sget-object v2, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v3, "Failed to create connection."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 323
    :catch_1
    move-exception v1

    .line 324
    :try_start_6
    sget-object v4, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v5, "Failed to write message."

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 325
    :try_start_7
    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/proximity/registration/v3/d;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7

    .line 361
    :goto_2
    :try_start_8
    invoke-virtual {v3}, Lcom/google/android/gms/auth/be/proximity/registration/v3/f;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto :goto_1

    :catch_2
    move-exception v1

    goto :goto_1

    .line 331
    :catch_3
    move-exception v1

    .line 332
    :try_start_9
    sget-object v4, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v5, "Error when reading message via Bluetooth."

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 333
    :try_start_a
    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/proximity/registration/v3/d;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    .line 361
    :goto_3
    :try_start_b
    invoke-virtual {v3}, Lcom/google/android/gms/auth/be/proximity/registration/v3/f;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    goto :goto_1

    :catch_4
    move-exception v1

    goto :goto_1

    .line 341
    :cond_0
    :try_start_c
    invoke-direct {p0, p3}, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a(Ljava/lang/String;)Z
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-result v0

    .line 343
    if-eqz v0, :cond_1

    .line 344
    :try_start_d
    new-instance v1, Lcom/google/android/gms/auth/be/proximity/registration/v3/b;

    const/4 v4, 0x0

    invoke-direct {v1, v4}, Lcom/google/android/gms/auth/be/proximity/registration/v3/b;-><init>(I)V

    invoke-virtual {v3, v1}, Lcom/google/android/gms/auth/be/proximity/registration/v3/f;->a(Lcom/google/android/gms/auth/be/proximity/registration/v3/h;)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_6
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 353
    :goto_4
    :try_start_e
    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/proximity/registration/v3/d;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_b

    .line 361
    :goto_5
    :try_start_f
    invoke-virtual {v3}, Lcom/google/android/gms/auth/be/proximity/registration/v3/f;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_5

    goto :goto_1

    :catch_5
    move-exception v1

    goto :goto_1

    .line 347
    :cond_1
    :try_start_10
    new-instance v1, Lcom/google/android/gms/auth/be/proximity/registration/v3/b;

    const/4 v4, 0x1

    invoke-direct {v1, v4}, Lcom/google/android/gms/auth/be/proximity/registration/v3/b;-><init>(I)V

    invoke-virtual {v3, v1}, Lcom/google/android/gms/auth/be/proximity/registration/v3/f;->a(Lcom/google/android/gms/auth/be/proximity/registration/v3/h;)V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_6
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto :goto_4

    .line 350
    :catch_6
    move-exception v1

    .line 351
    :try_start_11
    sget-object v4, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v5, "Failed to write message."

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto :goto_4

    .line 355
    :catchall_0
    move-exception v0

    .line 356
    :try_start_12
    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/proximity/registration/v3/d;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_c

    .line 361
    :goto_6
    :try_start_13
    invoke-virtual {v3}, Lcom/google/android/gms/auth/be/proximity/registration/v3/f;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_d

    .line 364
    :goto_7
    throw v0

    :catch_7
    move-exception v1

    goto :goto_2

    :catch_8
    move-exception v1

    goto :goto_3

    :catch_9
    move-exception v1

    goto :goto_0

    :catch_a
    move-exception v1

    goto :goto_1

    :catch_b
    move-exception v1

    goto :goto_5

    :catch_c
    move-exception v1

    goto :goto_6

    :catch_d
    move-exception v1

    goto :goto_7
.end method

.method private a(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 374
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->i:Lcom/google/android/gms/auth/authzen/keyservice/g;

    const-string v1, "device_key"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/authzen/keyservice/g;->a(Ljava/lang/String;)Ljava/security/KeyPair;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ab/b/a/e/j;->a(Ljava/security/PublicKey;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/m;->b([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/authzen/keyservice/h; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    .line 381
    new-instance v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/bh;

    invoke-direct {v1}, Lcom/google/android/gms/auth/gencode/authzen/server/api/bh;-><init>()V

    iput-boolean v3, v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/bh;->b:Z

    iget-object v3, v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/bh;->d:Ljava/util/Set;

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput-object v0, v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/bh;->c:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/bh;->d:Ljava/util/Set;

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;

    iget-object v3, v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/bh;->d:Ljava/util/Set;

    iget-boolean v4, v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/bh;->a:Z

    iget-boolean v5, v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/bh;->b:Z

    iget-object v1, v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/bh;->c:Ljava/lang/String;

    invoke-direct {v0, v3, v4, v5, v1}, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;-><init>(Ljava/util/Set;ZZLjava/lang/String;)V

    check-cast v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;

    .line 388
    :try_start_1
    iget-object v3, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->h:Lcom/google/android/gms/auth/gencode/authzen/server/api/f;

    new-instance v4, Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {p0}, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v1, p1, p1, v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/auth/b/a;->ai:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    const-string v1, "deviceSync/toggleeasyunlock"

    iget-object v3, v3, Lcom/google/android/gms/auth/gencode/authzen/server/api/f;->a:Lcom/google/android/gms/common/server/n;

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5, v1, v0}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_1

    .line 395
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->j:Lcom/google/android/gms/auth/be/proximity/g;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/be/proximity/g;->a(Ljava/lang/String;)Z

    move-result v0

    .line 396
    if-nez v0, :cond_0

    .line 397
    sget-object v1, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v2, "Failed to sync device state with server."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    :cond_0
    :goto_0
    return v0

    .line 376
    :catch_0
    move-exception v0

    .line 377
    :goto_1
    sget-object v1, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v3, "Failed to get the public key of current device."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    .line 378
    goto :goto_0

    .line 390
    :catch_1
    move-exception v0

    .line 391
    :goto_2
    sget-object v1, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v3, "Failed to make the API call to toggle EasyUnlock."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    .line 392
    goto :goto_0

    .line 390
    :catch_2
    move-exception v0

    goto :goto_2

    .line 376
    :catch_3
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public onCreate()V
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 163
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 165
    new-instance v0, Lcom/google/android/gms/auth/be/proximity/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/be/proximity/f;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->d:Lcom/google/android/gms/auth/be/proximity/f;

    .line 166
    const-string v0, "keyguard"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->e:Landroid/app/KeyguardManager;

    .line 167
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->f:Landroid/app/NotificationManager;

    .line 169
    new-instance v0, Lcom/google/android/gms/auth/g/a;

    invoke-direct {v0}, Lcom/google/android/gms/auth/g/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->g:Lcom/google/android/gms/auth/g/a;

    .line 170
    new-instance v8, Lcom/google/android/gms/auth/gencode/authzen/server/api/f;

    new-instance v0, Lcom/google/android/gms/common/server/n;

    sget-object v1, Lcom/google/android/gms/auth/b/a;->ah:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "/cryptauth/v1/"

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v1, p0

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/server/n;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    invoke-direct {v8, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/f;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v8, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->h:Lcom/google/android/gms/auth/gencode/authzen/server/api/f;

    .line 178
    new-instance v0, Lcom/google/android/gms/auth/authzen/keyservice/g;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/authzen/keyservice/g;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->i:Lcom/google/android/gms/auth/authzen/keyservice/g;

    .line 179
    invoke-static {p0}, Lcom/google/android/gms/auth/be/proximity/g;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/proximity/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->j:Lcom/google/android/gms/auth/be/proximity/g;

    .line 180
    invoke-static {p0}, Lcom/google/android/gms/auth/trustagent/at;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/trustagent/at;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->k:Lcom/google/android/gms/auth/trustagent/at;

    .line 181
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 185
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v2, "Received onHandleIntent() call: %s"

    new-array v3, v4, [Ljava/lang/Object;

    aput-object p1, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    const-string v0, "turn_bluetooth_off_on_registration_failure"

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 190
    const-string v0, "bluetooth_mac_address"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 191
    if-nez v0, :cond_1

    .line 192
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v1, "RegistrationBluetoothService started with empty bluetooth mac address."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    const-string v3, "account_name"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 197
    if-nez v3, :cond_2

    .line 198
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v1, "RegistrationBluetoothService started with empty account name."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 202
    :cond_2
    iget-object v4, p0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->d:Lcom/google/android/gms/auth/be/proximity/f;

    invoke-static {}, Lcom/google/android/gms/auth/be/proximity/f;->a()Z

    move-result v4

    if-nez v4, :cond_3

    .line 203
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v1, "Proximity feature is not enabled."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 207
    :cond_3
    invoke-static {}, Lcom/google/android/gms/auth/testability/android/bluetooth/b;->a()Lcom/google/android/gms/auth/testability/android/bluetooth/a;

    move-result-object v4

    .line 208
    if-nez v4, :cond_4

    .line 209
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v1, "Bluetooth is not supported on this device."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 215
    :cond_4
    :try_start_0
    iget-object v5, v4, Lcom/google/android/gms/auth/testability/android/bluetooth/a;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v5

    if-nez v5, :cond_5

    .line 216
    sget-object v5, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v6, "Bluetooth is not enabled on this device, trying to start it."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    invoke-static {p0, v3, v0}, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothStartService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    if-eqz v2, :cond_0

    .line 274
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v1, "Turning back off bluetooth registration failure."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    iget-object v0, v4, Lcom/google/android/gms/auth/testability/android/bluetooth/a;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    goto :goto_0

    .line 221
    :cond_5
    :try_start_1
    invoke-static {v0}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 222
    sget-object v3, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v5, "Bluetooth address received is invalid: \'%s\'."

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 273
    if-eqz v2, :cond_0

    .line 274
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v1, "Turning back off bluetooth registration failure."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    iget-object v0, v4, Lcom/google/android/gms/auth/testability/android/bluetooth/a;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    goto :goto_0

    .line 227
    :cond_6
    :try_start_2
    invoke-virtual {v4, v0}, Lcom/google/android/gms/auth/testability/android/bluetooth/a;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/testability/android/bluetooth/BluetoothDevice;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v5

    .line 232
    :try_start_3
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->b:Ljava/util/UUID;

    invoke-virtual {v5, v0}, Lcom/google/android/gms/auth/testability/android/bluetooth/BluetoothDevice;->a(Ljava/util/UUID;)Lcom/google/android/gms/auth/testability/android/bluetooth/g;

    move-result-object v6

    .line 233
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v7, "Socket to %s created."

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Lcom/google/android/gms/auth/testability/android/bluetooth/BluetoothDevice;->a()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 242
    :try_start_4
    invoke-virtual {v6}, Lcom/google/android/gms/auth/testability/android/bluetooth/g;->a()V

    .line 243
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v7, "Connected to %s."

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Lcom/google/android/gms/auth/testability/android/bluetooth/BluetoothDevice;->a()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 250
    :try_start_5
    sget v0, Lcom/google/android/gms/p;->cf:I

    sget v7, Lcom/google/android/gms/p;->ce:I

    invoke-direct {p0, v0, v7}, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a(II)V

    .line 254
    invoke-direct {p0, v5, v6, v3}, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a(Lcom/google/android/gms/auth/testability/android/bluetooth/BluetoothDevice;Lcom/google/android/gms/auth/testability/android/bluetooth/g;Ljava/lang/String;)Z

    move-result v1

    .line 255
    if-nez v1, :cond_8

    .line 256
    sget v0, Lcom/google/android/gms/p;->cd:I

    sget v3, Lcom/google/android/gms/p;->cc:I

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a(II)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 267
    :try_start_6
    invoke-virtual {v6}, Lcom/google/android/gms/auth/testability/android/bluetooth/g;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 273
    :goto_1
    if-nez v1, :cond_0

    if-eqz v2, :cond_0

    .line 274
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v1, "Turning back off bluetooth registration failure."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    iget-object v0, v4, Lcom/google/android/gms/auth/testability/android/bluetooth/a;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    goto/16 :goto_0

    .line 234
    :catch_0
    move-exception v0

    .line 235
    :try_start_7
    sget-object v3, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v6, "Failed to create RFCOMM socket to device %s."

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v5}, Lcom/google/android/gms/auth/testability/android/bluetooth/BluetoothDevice;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 273
    if-eqz v2, :cond_0

    .line 274
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v1, "Turning back off bluetooth registration failure."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    iget-object v0, v4, Lcom/google/android/gms/auth/testability/android/bluetooth/a;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    goto/16 :goto_0

    .line 244
    :catch_1
    move-exception v0

    .line 245
    :try_start_8
    sget-object v3, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v5, "Socket failed to connect."

    invoke-static {v3, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 273
    if-eqz v2, :cond_0

    .line 274
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v1, "Turning back off bluetooth registration failure."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    iget-object v0, v4, Lcom/google/android/gms/auth/testability/android/bluetooth/a;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    goto/16 :goto_0

    .line 268
    :catch_2
    move-exception v0

    .line 269
    :try_start_9
    sget-object v3, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v5, "Failed to close the socket."

    invoke-static {v3, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_1

    .line 273
    :catchall_0
    move-exception v0

    if-nez v1, :cond_7

    if-eqz v2, :cond_7

    .line 274
    sget-object v1, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v2, "Turning back off bluetooth registration failure."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    iget-object v1, v4, Lcom/google/android/gms/auth/testability/android/bluetooth/a;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    :cond_7
    throw v0

    .line 262
    :cond_8
    :try_start_a
    sget v0, Lcom/google/android/gms/p;->cb:I

    sget v3, Lcom/google/android/gms/p;->ca:I

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a(II)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 267
    :try_start_b
    invoke-virtual {v6}, Lcom/google/android/gms/auth/testability/android/bluetooth/g;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 273
    :goto_2
    if-nez v1, :cond_0

    if-eqz v2, :cond_0

    .line 274
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v1, "Turning back off bluetooth registration failure."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    iget-object v0, v4, Lcom/google/android/gms/auth/testability/android/bluetooth/a;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    goto/16 :goto_0

    .line 268
    :catch_3
    move-exception v0

    .line 269
    :try_start_c
    sget-object v3, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v5, "Failed to close the socket."

    invoke-static {v3, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_2

    .line 266
    :catchall_1
    move-exception v0

    .line 267
    :try_start_d
    invoke-virtual {v6}, Lcom/google/android/gms/auth/testability/android/bluetooth/g;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 270
    :goto_3
    :try_start_e
    throw v0

    .line 268
    :catch_4
    move-exception v3

    .line 269
    sget-object v5, Lcom/google/android/gms/auth/be/proximity/registration/v3/bt/RegistrationBluetoothService;->a:Ljava/lang/String;

    const-string v6, "Failed to close the socket."

    invoke-static {v5, v6, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto :goto_3
.end method
