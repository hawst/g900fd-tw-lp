.class public final Lcom/google/android/gms/appstate/c/b;
.super Lcom/google/android/gms/common/internal/aj;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 77
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;[Ljava/lang/String;)V

    .line 78
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/appstate/c/b;->a:Ljava/lang/String;

    .line 79
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    .prologue
    .line 34
    invoke-static {p1}, Lcom/google/android/gms/appstate/c/l;->a(Landroid/os/IBinder;)Lcom/google/android/gms/appstate/c/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;)V
    .locals 2

    .prologue
    .line 470
    invoke-virtual {p0}, Lcom/google/android/gms/appstate/c/b;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appstate/c/k;

    new-instance v1, Lcom/google/android/gms/appstate/c/c;

    invoke-direct {v1, p1}, Lcom/google/android/gms/appstate/c/c;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/appstate/c/k;->c(Lcom/google/android/gms/appstate/c/h;)V

    .line 471
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/internal/bj;Lcom/google/android/gms/common/internal/an;)V
    .locals 6

    .prologue
    .line 326
    const v2, 0x6768a8

    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/appstate/c/b;->a:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/common/internal/aj;->d:[Ljava/lang/String;

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/common/internal/bj;->a(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 329
    return-void
.end method

.method protected final varargs a([Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 97
    move v0, v1

    move v2, v1

    .line 98
    :goto_0
    array-length v4, p1

    if-ge v0, v4, :cond_1

    .line 99
    aget-object v4, p1, v0

    .line 100
    const-string v5, "https://www.googleapis.com/auth/appstate"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    .line 98
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    :cond_1
    const-string v0, "App State APIs requires %s to function."

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "https://www.googleapis.com/auth/appstate"

    aput-object v4, v3, v1

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 107
    return-void
.end method

.method protected final a_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    const-string v0, "com.google.android.gms.appstate.service.START"

    return-object v0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    const-string v0, "com.google.android.gms.appstate.internal.IAppStateService"

    return-object v0
.end method
