.class public Lcom/google/android/gms/drive/ui/picker/view/CustomListView;
.super Landroid/widget/ListView;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/drive/ui/picker/a/a/l;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/ui/picker/a/a/l;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/view/CustomListView;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    .line 40
    return-void
.end method

.method public isVerticalScrollBarEnabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 44
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/view/CustomListView;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    if-nez v1, :cond_1

    .line 48
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/view/CustomListView;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/ui/picker/a/a/l;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected layoutChildren()V
    .locals 3

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/CustomListView;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    .line 87
    const/4 v1, 0x0

    .line 88
    instance-of v2, v0, Lcom/google/android/gms/drive/ui/picker/view/k;

    if-eqz v2, :cond_0

    .line 89
    check-cast v0, Lcom/google/android/gms/drive/ui/picker/view/k;

    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/picker/view/k;->a()Ljava/lang/Integer;

    move-result-object v0

    move-object v1, v0

    .line 91
    :cond_0
    invoke-super {p0}, Landroid/widget/ListView;->layoutChildren()V

    .line 92
    if-eqz v1, :cond_1

    .line 93
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/CustomListView;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    .line 94
    instance-of v2, v0, Lcom/google/android/gms/drive/ui/picker/view/k;

    if-eqz v2, :cond_1

    .line 95
    check-cast v0, Lcom/google/android/gms/drive/ui/picker/view/k;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/ui/picker/view/k;->a(I)V

    .line 98
    :cond_1
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/CustomListView;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/CustomListView;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/ui/picker/a/a/l;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    const/4 v0, 0x1

    .line 75
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/CustomListView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/CustomListView;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/CustomListView;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/ui/picker/a/a/l;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    const/4 v0, 0x1

    .line 64
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
