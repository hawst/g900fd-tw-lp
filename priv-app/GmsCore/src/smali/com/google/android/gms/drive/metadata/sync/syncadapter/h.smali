.class public abstract Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;


# instance fields
.field final a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    .line 26
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    invoke-interface {v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;->a()V

    .line 41
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/d/g;)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;->a(Lcom/google/android/gms/drive/d/g;)V

    .line 36
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/d/g;Lcom/google/android/gms/drive/d/d;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;->a(Lcom/google/android/gms/drive/d/g;Lcom/google/android/gms/drive/d/d;)V

    .line 51
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;->a(Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method public final b(Lcom/google/android/gms/drive/d/g;)V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;->b(Lcom/google/android/gms/drive/d/g;)V

    .line 31
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 59
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ForwardingFeedProcessor[%s]"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
