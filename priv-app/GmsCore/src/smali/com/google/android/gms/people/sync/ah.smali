.class abstract Lcom/google/android/gms/people/sync/ah;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/sync/z;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/people/sync/z;)V
    .locals 0

    .prologue
    .line 1942
    iput-object p1, p0, Lcom/google/android/gms/people/sync/ah;->a:Lcom/google/android/gms/people/sync/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/people/sync/z;B)V
    .locals 0

    .prologue
    .line 1942
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/sync/ah;-><init>(Lcom/google/android/gms/people/sync/z;)V

    return-void
.end method


# virtual methods
.method protected abstract a(Ljava/lang/Object;)Ljava/lang/String;
.end method

.method protected abstract a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/util/List;
.end method

.method protected abstract a()V
.end method

.method public final a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/lang/String;Lcom/google/android/gms/people/internal/bf;)V
    .locals 8

    .prologue
    .line 1946
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/sync/ah;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/util/List;

    move-result-object v2

    .line 1947
    invoke-static {v2}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v3

    .line 1949
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    .line 1951
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 1952
    if-eqz v0, :cond_0

    .line 1953
    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/sync/ah;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1956
    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/sync/ah;->b(Ljava/lang/Object;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v0

    .line 1957
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    if-eqz v0, :cond_0

    .line 1958
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->d()Ljava/lang/String;

    move-result-object v5

    .line 1961
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1962
    invoke-virtual {p0}, Lcom/google/android/gms/people/sync/ah;->b()I

    move-result v0

    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    new-instance v6, Lcom/google/android/gms/people/sync/b;

    invoke-direct {v6, p2, v4, v0}, Lcom/google/android/gms/people/sync/b;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p3, Lcom/google/android/gms/people/internal/bf;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p3, Lcom/google/android/gms/people/internal/bf;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1968
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/people/sync/ah;->a()V

    .line 1949
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1962
    :cond_1
    instance-of v4, v0, Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    const/4 v7, 0x4

    invoke-direct {v4, v7}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p3, Lcom/google/android/gms/people/internal/bf;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1970
    :cond_3
    return-void
.end method

.method protected abstract b()I
.end method

.method protected abstract b(Ljava/lang/Object;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;
.end method
