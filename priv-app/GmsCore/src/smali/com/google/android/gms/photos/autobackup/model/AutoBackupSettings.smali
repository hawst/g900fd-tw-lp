.class public Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Lcom/google/android/gms/photos/autobackup/model/UserQuota;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/google/android/gms/photos/autobackup/model/b;

    invoke-direct {v0}, Lcom/google/android/gms/photos/autobackup/model/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->a:I

    .line 86
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;ZZZZZZZZLcom/google/android/gms/photos/autobackup/model/UserQuota;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput p1, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->a:I

    .line 72
    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->b:Ljava/lang/String;

    .line 73
    iput-boolean p3, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->c:Z

    .line 74
    iput-boolean p4, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->d:Z

    .line 75
    iput-boolean p5, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->e:Z

    .line 76
    iput-boolean p6, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->f:Z

    .line 77
    iput-boolean p7, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->g:Z

    .line 78
    iput-boolean p8, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->h:Z

    .line 79
    iput-boolean p9, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->i:Z

    .line 80
    iput-boolean p10, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->j:Z

    .line 81
    iput-object p11, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->k:Lcom/google/android/gms/photos/autobackup/model/UserQuota;

    .line 82
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->c:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->d:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->e:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->f:Z

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->g:Z

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->h:Z

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->i:Z

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->j:Z

    return v0
.end method

.method public final j()Lcom/google/android/gms/photos/autobackup/model/UserQuota;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->k:Lcom/google/android/gms/photos/autobackup/model/UserQuota;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 95
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/photos/autobackup/model/b;->a(Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;Landroid/os/Parcel;I)V

    .line 96
    return-void
.end method
