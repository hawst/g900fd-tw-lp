.class final Lcom/google/android/gms/cast/c/al;
.super Lcom/google/android/gms/cast/c/ap;
.source "SourceFile"


# instance fields
.field private a:Ljava/net/Inet6Address;


# direct methods
.method public constructor <init>([Ljava/lang/String;Lcom/google/android/gms/cast/c/am;)V
    .locals 1

    .prologue
    .line 20
    const/16 v0, 0x1c

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/gms/cast/c/ap;-><init>([Ljava/lang/String;ILcom/google/android/gms/cast/c/am;)V

    .line 21
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/cast/c/am;)V
    .locals 1

    .prologue
    .line 32
    const/16 v0, 0x10

    new-array v0, v0, [B

    .line 33
    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/c/am;->a([B)V

    .line 35
    :try_start_0
    invoke-static {v0}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v0

    check-cast v0, Ljava/net/Inet6Address;

    iput-object v0, p0, Lcom/google/android/gms/cast/c/al;->a:Ljava/net/Inet6Address;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/cast/c/an;)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/cast/c/al;->a:Ljava/net/Inet6Address;

    invoke-virtual {v0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v0

    .line 42
    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/c/an;->a([B)V

    .line 43
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 47
    const-string v0, "AAAA: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/cast/c/al;->a:Ljava/net/Inet6Address;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
