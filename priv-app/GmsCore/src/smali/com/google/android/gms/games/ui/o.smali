.class public abstract Lcom/google/android/gms/games/ui/o;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/m;


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/o;->a:Z

    return-void
.end method


# virtual methods
.method public final a(ZZ)V
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/o;->a:Z

    if-eq v0, p1, :cond_0

    .line 46
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/o;->a:Z

    .line 47
    if-eqz p2, :cond_0

    .line 48
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/o;->notifyDataSetChanged()V

    .line 51
    :cond_0
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/o;->a:Z

    return v0
.end method

.method public final m()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-virtual {p0, v0, v0}, Lcom/google/android/gms/games/ui/o;->a(ZZ)V

    .line 22
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 26
    invoke-virtual {p0, v0, v0}, Lcom/google/android/gms/games/ui/o;->a(ZZ)V

    .line 27
    return-void
.end method
