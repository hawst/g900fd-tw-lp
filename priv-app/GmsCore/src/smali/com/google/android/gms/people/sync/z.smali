.class public final Lcom/google/android/gms/people/sync/z;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/plus/service/v2whitelisted/c;

.field private static final b:Lcom/google/android/gms/plus/service/v2whitelisted/d;

.field private static final c:Lcom/google/android/gms/plus/service/v2whitelisted/d;

.field private static final d:Lcom/google/android/gms/plus/service/v2whitelisted/c;


# instance fields
.field private final A:Lcom/google/android/gms/plus/service/v1whitelisted/a;

.field private final B:Lcom/google/android/gms/people/sync/s;

.field private final C:Lcom/google/android/gms/people/sync/ai;

.field private final D:Lcom/google/android/gms/people/sync/an;

.field private final E:Lcom/google/android/gms/people/sync/ao;

.field private final e:Landroid/content/Context;

.field private final f:Lcom/google/android/gms/common/util/p;

.field private final g:Landroid/content/SyncResult;

.field private final h:Lcom/google/android/gms/people/sync/v;

.field private final i:Lcom/google/android/gms/people/sync/y;

.field private final j:I

.field private final k:Z

.field private final l:Z

.field private final m:Landroid/os/Bundle;

.field private final n:Ljava/lang/String;

.field private final o:Ljava/lang/String;

.field private final p:Lcom/google/android/gms/people/service/e;

.field private final q:Lcom/google/android/gms/people/e/a;

.field private final r:Lcom/google/android/gms/people/service/g;

.field private final s:Lcom/google/android/gms/people/service/g;

.field private final t:Lcom/google/android/gms/people/sync/ap;

.field private final u:Lcom/google/android/gms/people/service/h;

.field private final v:Lcom/google/android/gms/plus/service/v2whitelisted/b;

.field private final w:Lcom/google/android/gms/plus/service/v2whitelisted/a;

.field private final x:Lcom/google/android/gms/plus/service/v1whitelisted/i;

.field private final y:Lcom/google/android/gms/plus/service/v1whitelisted/d;

.field private final z:Lcom/google/android/gms/plus/service/v1whitelisted/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 93
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/c;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/c;-><init>()V

    const-string v1, "etag"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/c;

    const-string v1, "id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/c;

    const-string v1, "names"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/c;

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/c;

    const-string v1, "emails"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/c;

    const-string v1, "phoneNumbers"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/c;

    const-string v1, "addresses"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/c;

    const-string v1, "metadata/ownerId"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/c;

    const-string v1, "metadata/ownerUserTypes"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/c;

    const-string v1, "coverPhotos"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/c;

    sput-object v0, Lcom/google/android/gms/people/sync/z;->a:Lcom/google/android/gms/plus/service/v2whitelisted/c;

    .line 105
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/d;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/d;-><init>()V

    const-string v1, "items(etag,id,names,nicknames,images,urls,sortKeys,taglines,emails,phoneNumbers,addresses,metadata,memberships,legacyFields/mobileOwnerId)"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/d;

    const-string v1, "nextPageToken"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/d;

    const-string v1, "nextSyncToken"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/d;

    sput-object v0, Lcom/google/android/gms/people/sync/z;->b:Lcom/google/android/gms/plus/service/v2whitelisted/d;

    .line 112
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/d;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/d;-><init>()V

    const-string v1, "items(id,metadata)"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/d;

    const-string v1, "nextPageToken"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/d;

    sput-object v0, Lcom/google/android/gms/people/sync/z;->c:Lcom/google/android/gms/plus/service/v2whitelisted/d;

    .line 117
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/c;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/c;-><init>()V

    const-string v1, "id,coverPhotos"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/c;

    sput-object v0, Lcom/google/android/gms/people/sync/z;->d:Lcom/google/android/gms/plus/service/v2whitelisted/c;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILandroid/content/SyncResult;Lcom/google/android/gms/people/sync/v;Lcom/google/android/gms/people/sync/s;Lcom/google/android/gms/people/sync/y;Landroid/os/Bundle;ZZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2013
    new-instance v0, Lcom/google/android/gms/people/sync/ai;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/people/sync/ai;-><init>(Lcom/google/android/gms/people/sync/z;B)V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/z;->C:Lcom/google/android/gms/people/sync/ai;

    .line 2045
    new-instance v0, Lcom/google/android/gms/people/sync/an;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/people/sync/an;-><init>(Lcom/google/android/gms/people/sync/z;B)V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/z;->D:Lcom/google/android/gms/people/sync/an;

    .line 2077
    new-instance v0, Lcom/google/android/gms/people/sync/ao;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/people/sync/ao;-><init>(Lcom/google/android/gms/people/sync/z;B)V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/z;->E:Lcom/google/android/gms/people/sync/ao;

    .line 170
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->a(Landroid/content/Context;)Lcom/google/android/gms/common/util/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/z;->f:Lcom/google/android/gms/common/util/p;

    .line 172
    iput-object p5, p0, Lcom/google/android/gms/people/sync/z;->g:Landroid/content/SyncResult;

    .line 173
    iput-object p6, p0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    .line 174
    iput-object p8, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iput-object p2, v0, Lcom/google/android/gms/people/sync/y;->a:Ljava/lang/String;

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iput-object p3, v0, Lcom/google/android/gms/people/sync/y;->b:Ljava/lang/String;

    .line 177
    iput p4, p0, Lcom/google/android/gms/people/sync/z;->j:I

    .line 178
    iput-boolean p10, p0, Lcom/google/android/gms/people/sync/z;->k:Z

    .line 179
    iput-boolean p11, p0, Lcom/google/android/gms/people/sync/z;->l:Z

    .line 180
    iput-object p9, p0, Lcom/google/android/gms/people/sync/z;->m:Landroid/os/Bundle;

    .line 181
    iput-object p2, p0, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    .line 182
    iput-object p3, p0, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    .line 183
    iput-object p7, p0, Lcom/google/android/gms/people/sync/z;->B:Lcom/google/android/gms/people/sync/s;

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->m:Landroid/os/Bundle;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p2, p3, v1}, Lcom/google/android/gms/people/service/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/people/e/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/z;->q:Lcom/google/android/gms/people/e/a;

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->q:Lcom/google/android/gms/people/e/a;

    iget-object v1, v0, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    packed-switch p4, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/gms/people/service/g;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    .line 190
    new-instance v0, Lcom/google/android/gms/people/sync/ap;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    invoke-direct {v0, v1, p2, p3}, Lcom/google/android/gms/people/sync/ap;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/service/h;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/z;->u:Lcom/google/android/gms/people/service/h;

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/service/e;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/z;->p:Lcom/google/android/gms/people/service/e;

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->p:Lcom/google/android/gms/people/service/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/service/e;->a()Lcom/google/android/gms/people/service/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/z;->r:Lcom/google/android/gms/people/service/g;

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->p:Lcom/google/android/gms/people/service/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/service/e;->b()Lcom/google/android/gms/people/service/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/z;->s:Lcom/google/android/gms/people/service/g;

    .line 196
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/b;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->s:Lcom/google/android/gms/people/service/g;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/service/v2whitelisted/b;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/z;->v:Lcom/google/android/gms/plus/service/v2whitelisted/b;

    .line 197
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/i;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->r:Lcom/google/android/gms/people/service/g;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/i;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/z;->x:Lcom/google/android/gms/plus/service/v1whitelisted/i;

    .line 199
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/d;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->r:Lcom/google/android/gms/people/service/g;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/d;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/z;->y:Lcom/google/android/gms/plus/service/v1whitelisted/d;

    .line 200
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/a;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->s:Lcom/google/android/gms/people/service/g;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/service/v2whitelisted/a;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/z;->w:Lcom/google/android/gms/plus/service/v2whitelisted/a;

    .line 201
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/c;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->r:Lcom/google/android/gms/people/service/g;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/c;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/z;->z:Lcom/google/android/gms/plus/service/v1whitelisted/c;

    .line 202
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/a;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->r:Lcom/google/android/gms/people/service/g;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/a;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/z;->A:Lcom/google/android/gms/plus/service/v1whitelisted/a;

    .line 203
    return-void

    .line 187
    :pswitch_0
    const-string v0, "m"

    goto :goto_0

    :pswitch_1
    const-string v0, "p"

    goto :goto_0

    :pswitch_2
    const-string v0, "t"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    return-object v0
.end method

.method static final a(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 291
    if-nez p0, :cond_1

    const/4 v0, 0x0

    .line 293
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 294
    sget-object v0, Lcom/google/android/gms/common/analytics/a;->b:Ljava/lang/String;

    .line 296
    :cond_0
    return-object v0

    .line 291
    :cond_1
    const-string v0, "gms.people.request_app_id"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 18

    .prologue
    .line 246
    const/4 v4, 0x2

    invoke-static {v4}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 247
    const-string v4, "PeopleSync"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Circles sync for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " page="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v16

    .line 251
    new-instance v12, Lcom/google/android/gms/people/sync/y;

    invoke-direct {v12}, Lcom/google/android/gms/people/sync/y;-><init>()V

    .line 252
    new-instance v4, Lcom/google/android/gms/people/sync/z;

    const/4 v8, 0x2

    new-instance v9, Landroid/content/SyncResult;

    invoke-direct {v9}, Landroid/content/SyncResult;-><init>()V

    sget-object v10, Lcom/google/android/gms/people/sync/v;->a:Lcom/google/android/gms/people/sync/v;

    const/4 v11, 0x0

    new-instance v13, Landroid/os/Bundle;

    invoke-direct {v13}, Landroid/os/Bundle;-><init>()V

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    invoke-direct/range {v4 .. v15}, Lcom/google/android/gms/people/sync/z;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILandroid/content/SyncResult;Lcom/google/android/gms/people/sync/v;Lcom/google/android/gms/people/sync/s;Lcom/google/android/gms/people/sync/y;Landroid/os/Bundle;ZZ)V

    .line 257
    :try_start_0
    invoke-direct {v4}, Lcom/google/android/gms/people/sync/z;->d()V
    :try_end_0
    .catch Lcom/google/android/gms/common/server/ac; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v4, v4, v16

    iput-wide v4, v12, Lcom/google/android/gms/people/sync/y;->R:J

    .line 264
    const-string v4, "PeopleSync"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Stats="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v4, v1, v2, v5}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    return-void

    .line 258
    :catch_0
    move-exception v4

    .line 260
    :try_start_1
    const-string v5, "PeopleSync"

    const-string v6, "Cancelled in volley"

    move-object/from16 v0, p0

    invoke-static {v0, v5, v6, v4}, Lcom/google/android/gms/people/debug/c;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 261
    new-instance v4, Lcom/google/android/gms/people/sync/x;

    invoke-direct {v4}, Lcom/google/android/gms/people/sync/x;-><init>()V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 263
    :catchall_0
    move-exception v4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v6, v6, v16

    iput-wide v6, v12, Lcom/google/android/gms/people/sync/y;->R:J

    .line 264
    const-string v5, "PeopleSync"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Stats="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v5, v1, v2, v6}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v4
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILandroid/content/SyncResult;Lcom/google/android/gms/people/sync/v;ZLcom/google/android/gms/people/sync/s;Lcom/google/android/gms/people/sync/y;Landroid/os/Bundle;Z)V
    .locals 16

    .prologue
    .line 223
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    .line 225
    new-instance v2, Lcom/google/android/gms/people/sync/z;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move/from16 v12, p10

    move/from16 v13, p6

    invoke-direct/range {v2 .. v13}, Lcom/google/android/gms/people/sync/z;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILandroid/content/SyncResult;Lcom/google/android/gms/people/sync/v;Lcom/google/android/gms/people/sync/s;Lcom/google/android/gms/people/sync/y;Landroid/os/Bundle;ZZ)V

    .line 229
    :try_start_0
    iget-boolean v3, v2, Lcom/google/android/gms/people/sync/z;->k:Z

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    const-string v4, "isPageExistenceCheckOnly=true is not used with pages"

    invoke-static {v3, v4}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    :cond_0
    const-string v3, "PeopleSync"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Sync start: cannotHavePeople="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, v2, Lcom/google/android/gms/people/sync/z;->l:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " isPageExistenceCheckOnly="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, v2, Lcom/google/android/gms/people/sync/z;->k:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x3

    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "PeopleSync"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Sync for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v2, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " page="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v3, v2, Lcom/google/android/gms/people/sync/z;->f:Lcom/google/android/gms/common/util/p;

    invoke-interface {v3}, Lcom/google/android/gms/common/util/p;->a()J
    :try_end_0
    .catch Lcom/google/android/gms/common/server/ac; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v6

    const/4 v4, 0x0

    :try_start_1
    iget-boolean v3, v2, Lcom/google/android/gms/people/sync/z;->k:Z

    if-nez v3, :cond_2

    iget-object v3, v2, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const/4 v5, 0x0

    const/4 v8, 0x0

    iget-boolean v9, v2, Lcom/google/android/gms/people/sync/z;->l:Z

    invoke-virtual {v3, v5, v6, v7, v8}, Lcom/google/android/gms/people/sync/ap;->a(ZJLjava/lang/Long;)V

    iget-object v3, v2, Lcom/google/android/gms/people/sync/z;->u:Lcom/google/android/gms/people/service/h;

    iget-object v5, v2, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    iget-object v8, v2, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    const/16 v9, 0x10

    invoke-virtual {v3, v5, v8, v9}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v3, v2, Lcom/google/android/gms/people/sync/z;->u:Lcom/google/android/gms/people/service/h;

    invoke-virtual {v3}, Lcom/google/android/gms/people/service/h;->b()V

    :cond_2
    iget-object v3, v2, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v3}, Lcom/google/android/gms/people/sync/v;->c()V

    iget-object v3, v2, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v3}, Lcom/google/android/gms/people/sync/v;->c()V

    const/4 v3, 0x1

    iget-object v5, v2, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    if-nez v5, :cond_5

    iget-boolean v5, v2, Lcom/google/android/gms/people/sync/z;->k:Z

    if-eqz v5, :cond_3

    iget-object v5, v2, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v5

    iget-object v8, v2, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    invoke-virtual {v5, v8}, Lcom/google/android/gms/people/c/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_3
    invoke-direct {v2}, Lcom/google/android/gms/people/sync/z;->g()Z

    move-result v3

    :cond_4
    invoke-direct {v2}, Lcom/google/android/gms/people/sync/z;->a()Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "PeopleSync"

    const-string v8, "Sync inconsistency.  Resyncing pages."

    invoke-static {v5, v8}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v2, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const-string v8, "pages"

    invoke-virtual {v5, v8}, Lcom/google/android/gms/people/sync/ap;->c(Ljava/lang/String;)V

    invoke-direct {v2}, Lcom/google/android/gms/people/sync/z;->a()Z

    :cond_5
    iget-boolean v5, v2, Lcom/google/android/gms/people/sync/z;->k:Z

    if-nez v5, :cond_a

    iget-boolean v5, v2, Lcom/google/android/gms/people/sync/z;->l:Z

    if-nez v5, :cond_e

    if-eqz v3, :cond_e

    const/4 v3, 0x1

    :goto_0
    if-eqz v3, :cond_7

    invoke-direct {v2}, Lcom/google/android/gms/people/sync/z;->c()Z

    move-result v5

    if-nez v5, :cond_6

    const-string v5, "PeopleSync"

    const-string v8, "Sync inconsistency.  Resyncing groups."

    invoke-static {v5, v8}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v2, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const-string v8, "groups"

    invoke-virtual {v5, v8}, Lcom/google/android/gms/people/sync/ap;->c(Ljava/lang/String;)V

    invoke-direct {v2}, Lcom/google/android/gms/people/sync/z;->c()Z

    :cond_6
    invoke-direct {v2}, Lcom/google/android/gms/people/sync/z;->d()V

    :cond_7
    invoke-direct {v2, v3}, Lcom/google/android/gms/people/sync/z;->a(Z)Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v5, v2, Lcom/google/android/gms/people/sync/z;->f:Lcom/google/android/gms/common/util/p;

    invoke-interface {v5}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v8

    iget-object v5, v2, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v5, v8, v9}, Lcom/google/android/gms/people/sync/ap;->a(J)V

    :cond_8
    if-eqz v3, :cond_9

    iget-object v3, v2, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    if-nez v3, :cond_9

    invoke-direct {v2}, Lcom/google/android/gms/people/sync/z;->f()V

    :cond_9
    iget-object v3, v2, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v3}, Lcom/google/android/gms/people/sync/ap;->f()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v3, v8, v10

    if-nez v3, :cond_f

    const-string v3, "PeopleSync"

    const-string v5, "First sync. Sync sanity check not necessary."

    invoke-static {v3, v5}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    :goto_1
    const/4 v3, 0x2

    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, v2, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v3}, Lcom/google/android/gms/people/sync/ap;->a()V

    :cond_b
    iget-object v3, v2, Lcom/google/android/gms/people/sync/z;->B:Lcom/google/android/gms/people/sync/s;

    if-eqz v3, :cond_c

    iget-object v3, v2, Lcom/google/android/gms/people/sync/z;->B:Lcom/google/android/gms/people/sync/s;
    :try_end_1
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_c
    :try_start_2
    iget-object v3, v2, Lcom/google/android/gms/people/sync/z;->f:Lcom/google/android/gms/common/util/p;

    invoke-interface {v3}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    iget-boolean v3, v2, Lcom/google/android/gms/people/sync/z;->k:Z

    if-nez v3, :cond_d

    iget-object v3, v2, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const/4 v8, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    iget-boolean v10, v2, Lcom/google/android/gms/people/sync/z;->l:Z

    invoke-virtual {v3, v8, v6, v7, v9}, Lcom/google/android/gms/people/sync/ap;->a(ZJLjava/lang/Long;)V

    const/4 v3, 0x1

    const/4 v8, 0x0

    invoke-direct {v2, v3, v8}, Lcom/google/android/gms/people/sync/z;->a(ZZ)V

    :cond_d
    const-string v2, "PeopleSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v8, "Sync finished, successful=true"

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, ", took "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/google/android/gms/common/server/ac; {:try_start_2 .. :try_end_2} :catch_1

    .line 236
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, v14

    move-object/from16 v0, p8

    iput-wide v2, v0, Lcom/google/android/gms/people/sync/y;->R:J

    .line 237
    return-void

    .line 229
    :cond_e
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_f
    :try_start_3
    iget-object v3, v2, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/people/ah;->a()Lcom/google/android/gms/people/a/c;

    move-result-object v5

    iget-object v3, v2, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    iget-object v8, v2, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    iget-object v9, v5, Lcom/google/android/gms/people/a/c;->a:Landroid/content/SharedPreferences;

    invoke-static {v3, v8}, Lcom/google/android/gms/people/a/c;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-wide/16 v10, 0x0

    invoke-interface {v9, v3, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    sget-object v3, Lcom/google/android/gms/people/a/a;->Y:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    add-long/2addr v8, v10

    iget-object v3, v2, Lcom/google/android/gms/people/sync/z;->f:Lcom/google/android/gms/common/util/p;

    invoke-interface {v3}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v10

    cmp-long v3, v10, v8

    if-gez v3, :cond_11

    const-string v3, "PeopleSync"

    const-string v5, "Sync sanity check not necessary."

    invoke-static {v3, v5}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    :catch_0
    move-exception v3

    const/4 v4, 0x1

    :try_start_4
    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v3

    :try_start_5
    iget-object v5, v2, Lcom/google/android/gms/people/sync/z;->f:Lcom/google/android/gms/common/util/p;

    invoke-interface {v5}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v8

    iget-boolean v5, v2, Lcom/google/android/gms/people/sync/z;->k:Z

    if-nez v5, :cond_10

    iget-object v5, v2, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const/4 v10, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    iget-boolean v12, v2, Lcom/google/android/gms/people/sync/z;->l:Z

    invoke-virtual {v5, v10, v6, v7, v11}, Lcom/google/android/gms/people/sync/ap;->a(ZJLjava/lang/Long;)V

    const/4 v5, 0x0

    invoke-direct {v2, v5, v4}, Lcom/google/android/gms/people/sync/z;->a(ZZ)V

    :cond_10
    const-string v2, "PeopleSync"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Sync finished, successful=false"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ", took "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sub-long v6, v8, v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    throw v3
    :try_end_5
    .catch Lcom/google/android/gms/common/server/ac; {:try_start_5 .. :try_end_5} :catch_1

    .line 234
    :catch_1
    move-exception v2

    .line 232
    const-string v3, "PeopleSync"

    const-string v4, "Cancelled in volley"

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4, v2}, Lcom/google/android/gms/people/debug/c;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 233
    new-instance v2, Lcom/google/android/gms/people/sync/x;

    invoke-direct {v2}, Lcom/google/android/gms/people/sync/x;-><init>()V

    throw v2

    .line 229
    :cond_11
    :try_start_6
    iget-object v3, v2, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    iget-object v8, v2, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v9}, Lcom/google/android/gms/common/internal/bx;->c(Ljava/lang/String;)V

    iget-object v5, v5, Lcom/google/android/gms/people/a/c;->a:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-static {v3, v8}, Lcom/google/android/gms/people/a/c;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5, v3, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    sget-object v3, Lcom/google/android/gms/people/internal/at;->k:Ljava/security/SecureRandom;

    invoke-virtual {v3}, Ljava/security/SecureRandom;->nextFloat()F

    move-result v5

    sget-object v3, Lcom/google/android/gms/people/a/a;->Z:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    cmpl-float v3, v5, v3

    if-ltz v3, :cond_12

    const-string v3, "PeopleSync"

    const-string v5, "Skipping sanity check."

    invoke-static {v3, v5}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_12
    invoke-direct {v2}, Lcom/google/android/gms/people/sync/z;->j()V
    :try_end_6
    .catch Lcom/google/android/gms/people/sync/x; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_1
.end method

.method private a(Lcom/google/android/gms/people/sync/am;Lcom/google/android/gms/people/sync/al;Lcom/google/android/gms/people/sync/ag;Ljava/lang/String;Ljava/lang/String;)V
    .locals 18

    .prologue
    .line 1371
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    .line 1374
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1377
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    const-string v3, "PeopleSync"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    const-string v6, "Resume"

    invoke-static {v2, v3, v4, v5, v6}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1378
    const/4 v2, 0x2

    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1379
    const-string v2, "PeopleSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Resuming full sync.  Page token="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1383
    :cond_0
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/people/sync/am;->c:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/people/sync/am;->a:Z

    if-nez v2, :cond_1

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/people/sync/am;->b:Z

    if-nez v2, :cond_1

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    iget-object v2, v2, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/ap;->m()V

    .line 1384
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/people/sync/al;->d()V

    .line 1385
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/people/sync/ag;->a()V

    .line 1387
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/v;->c()V

    move-object/from16 v12, p5

    .line 1390
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 1392
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v16

    .line 1395
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->v:Lcom/google/android/gms/plus/service/v2whitelisted/b;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/z;->q:Lcom/google/android/gms/people/e/a;

    iget-object v3, v3, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    const-string v4, "me"

    const-string v5, "all"

    sget-object v6, Lcom/google/android/gms/people/a/a;->aH:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_a

    const-string v6, "menagerie"

    :goto_0
    sget-object v7, Lcom/google/android/gms/people/a/a;->ay:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v7}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_b

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    const-string v7, "gplusAutocomplete"

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v7, "chatAutocomplete"

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v7, "emailAutocomplete"

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v7, Lcom/google/android/gms/people/a/a;->aI:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v7}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_3

    const-string v7, "peopleAutocompleteSocial"

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v7, "fieldAutocompleteSocial"

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    move-object v7, v8

    :goto_1
    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    invoke-static {v9}, Lcom/google/android/gms/people/a/a;->d(Landroid/content/Context;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    sget-object v11, Lcom/google/android/gms/people/a/a;->V:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v11}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-static {v11}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    sget-object v14, Lcom/google/android/gms/people/sync/z;->b:Lcom/google/android/gms/plus/service/v2whitelisted/d;

    move-object/from16 v13, p4

    invoke-virtual/range {v2 .. v14}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/d;)Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1417
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget-wide v4, v3, Lcom/google/android/gms/people/sync/y;->S:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v6, v6, v16

    add-long/2addr v4, v6

    iput-wide v4, v3, Lcom/google/android/gms/people/sync/y;->S:J

    .line 1420
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v3}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 1423
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/google/android/gms/people/sync/am;->c:Z

    if-eqz v3, :cond_4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/sync/am;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;)V

    .line 1426
    :cond_4
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/sync/ag;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;)V

    .line 1431
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/sync/al;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;)V

    .line 1433
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;->d()Ljava/lang/String;

    move-result-object v3

    .line 1434
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;->c()Ljava/lang/String;

    move-result-object v12

    .line 1436
    if-eqz v15, :cond_5

    .line 1437
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const-string v4, "people_page"

    invoke-virtual {v2, v4, v12}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1440
    :cond_5
    const/4 v2, 0x2

    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1441
    const-string v2, "PeopleSync"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Next page token="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1443
    :cond_6
    if-nez v12, :cond_2

    .line 1446
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/people/sync/am;->c:Z

    if-eqz v2, :cond_7

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/people/sync/am;->a:Z

    if-nez v2, :cond_7

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    iget-object v2, v2, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/ap;->n()V

    .line 1450
    :cond_7
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/people/sync/al;->e()V

    .line 1453
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/people/sync/ag;->b()V

    .line 1456
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/people/sync/am;->c:Z

    if-eqz v2, :cond_8

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    iget-object v2, v2, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const-string v4, "people"

    invoke-virtual {v2, v4, v3}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1457
    :cond_8
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-direct {v2}, Lcom/google/android/gms/people/sync/z;->i()Z

    move-result v2

    if-eqz v2, :cond_9

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    iget-object v2, v2, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const-string v4, "gaiamap"

    invoke-virtual {v2, v4, v3}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1458
    :cond_9
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/gms/people/sync/ag;->d:Lcom/google/android/gms/people/sync/z;

    iget-object v2, v2, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const-string v4, "autocomplete"

    invoke-virtual {v2, v4, v3}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1459
    return-void

    .line 1395
    :cond_a
    const/4 v6, 0x0

    goto/16 :goto_0

    :cond_b
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 1417
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget-wide v4, v3, Lcom/google/android/gms/people/sync/y;->S:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v6, v6, v16

    add-long/2addr v4, v6

    iput-wide v4, v3, Lcom/google/android/gms/people/sync/y;->S:J

    throw v2
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1260
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    .line 1262
    sget-object v0, Lcom/google/android/gms/people/a/a;->az:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1263
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const-string v1, "people_page"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/sync/ap;->b(Ljava/lang/String;)V

    .line 1266
    :cond_0
    if-eqz v7, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const-string v1, "people_page"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1270
    :goto_0
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 1273
    :goto_1
    new-instance v1, Lcom/google/android/gms/people/sync/am;

    if-nez v7, :cond_3

    move v3, v2

    :goto_2
    invoke-direct {v1, p0, v3, p2, v0}, Lcom/google/android/gms/people/sync/am;-><init>(Lcom/google/android/gms/people/sync/z;ZZZ)V

    .line 1275
    if-eqz v7, :cond_4

    new-instance v3, Lcom/google/android/gms/people/sync/af;

    invoke-direct {v3, p0, p2, v0, v4}, Lcom/google/android/gms/people/sync/af;-><init>(Lcom/google/android/gms/people/sync/z;ZZB)V

    .line 1279
    :goto_3
    iget-object v8, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    invoke-direct {p0}, Lcom/google/android/gms/people/sync/z;->i()Z

    move-result v9

    iput-boolean v9, v8, Lcom/google/android/gms/people/sync/y;->x:Z

    .line 1280
    iget-object v8, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iput-boolean p2, v8, Lcom/google/android/gms/people/sync/y;->s:Z

    .line 1281
    iget-object v8, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iput-boolean v2, v8, Lcom/google/android/gms/people/sync/y;->C:Z

    .line 1283
    if-eqz v7, :cond_6

    .line 1284
    const-string v6, "PeopleSync"

    const-string v7, "Gaia-map/people sync (full)"

    invoke-static {v6, v7}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1286
    iget-object v6, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iput-boolean v2, v6, Lcom/google/android/gms/people/sync/y;->r:Z

    .line 1288
    iget-object v6, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v6}, Lcom/google/android/gms/people/sync/ap;->b()V

    .line 1290
    iget-object v6, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    :goto_4
    iput-boolean v2, v6, Lcom/google/android/gms/people/sync/y;->q:Z

    .line 1292
    new-instance v2, Lcom/google/android/gms/people/sync/ak;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/people/sync/ak;-><init>(Lcom/google/android/gms/people/sync/z;Z)V

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/am;Lcom/google/android/gms/people/sync/al;Lcom/google/android/gms/people/sync/ag;Ljava/lang/String;Ljava/lang/String;)V

    .line 1301
    :goto_5
    return-void

    :cond_1
    move-object v5, v6

    .line 1266
    goto :goto_0

    :cond_2
    move v0, v4

    .line 1270
    goto :goto_1

    :cond_3
    move v3, v4

    .line 1273
    goto :goto_2

    .line 1275
    :cond_4
    new-instance v3, Lcom/google/android/gms/people/sync/ae;

    invoke-direct {v3, p0, p2, v0, v4}, Lcom/google/android/gms/people/sync/ae;-><init>(Lcom/google/android/gms/people/sync/z;ZZB)V

    goto :goto_3

    :cond_5
    move v2, v4

    .line 1290
    goto :goto_4

    .line 1295
    :cond_6
    const-string v0, "PeopleSync"

    const-string v2, "Gaia-map/people sync (delta)"

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1298
    new-instance v2, Lcom/google/android/gms/people/sync/aj;

    invoke-direct {v2, p0}, Lcom/google/android/gms/people/sync/aj;-><init>(Lcom/google/android/gms/people/sync/z;)V

    move-object v0, p0

    move-object v4, p1

    move-object v5, v6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/am;Lcom/google/android/gms/people/sync/al;Lcom/google/android/gms/people/sync/ag;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5
.end method

.method private a(Ljava/util/Set;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1124
    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    if-nez v1, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 1125
    const-string v0, "PeopleSync"

    const-string v1, "Orphaned Pages"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1127
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->w()V

    .line 1130
    :try_start_0
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1131
    iget-object v2, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/sync/ap;->e(Ljava/lang/String;)V

    .line 1132
    iget-object v2, p0, Lcom/google/android/gms/people/sync/z;->u:Lcom/google/android/gms/people/service/h;

    iget-object v3, p0, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v0, v4}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1135
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->g:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numDeletes:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numDeletes:J

    .line 1136
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget v2, v0, Lcom/google/android/gms/people/sync/y;->e:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/android/gms/people/sync/y;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1141
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/ap;->y()V

    throw v0

    .line 1124
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1139
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->z()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1141
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->y()V

    .line 1144
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 1145
    return-void
.end method

.method private a(Ljava/util/Set;Z)V
    .locals 8

    .prologue
    .line 1168
    const-string v0, "PeopleSync"

    const-string v1, "Orphaned Groups and Circles"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1170
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->w()V

    .line 1173
    const/4 v0, 0x0

    .line 1174
    :try_start_0
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1175
    iget-object v2, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/sync/ap;->o(Ljava/lang/String;)V

    .line 1176
    const/4 v0, 0x1

    .line 1178
    iget-object v2, p0, Lcom/google/android/gms/people/sync/z;->g:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numDeletes:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Landroid/content/SyncStats;->numDeletes:J

    .line 1179
    if-eqz p2, :cond_0

    .line 1180
    iget-object v2, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget v3, v2, Lcom/google/android/gms/people/sync/y;->n:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/google/android/gms/people/sync/y;->n:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1191
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/ap;->y()V

    throw v0

    .line 1182
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget v3, v2, Lcom/google/android/gms/people/sync/y;->j:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/google/android/gms/people/sync/y;->j:I

    goto :goto_0

    .line 1186
    :cond_1
    if-eqz v0, :cond_2

    .line 1187
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->u:Lcom/google/android/gms/people/service/h;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1189
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->z()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1191
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->y()V

    .line 1193
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 1194
    return-void
.end method

.method private a(ZZ)V
    .locals 4

    .prologue
    .line 349
    if-eqz p1, :cond_0

    .line 350
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->u:Lcom/google/android/gms/people/service/h;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    const/16 v3, 0x20

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 359
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->u:Lcom/google/android/gms/people/service/h;

    invoke-virtual {v0}, Lcom/google/android/gms/people/service/h;->b()V

    .line 360
    return-void

    .line 352
    :cond_0
    if-eqz p2, :cond_1

    .line 353
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->u:Lcom/google/android/gms/people/service/h;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    const/16 v3, 0x80

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 356
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->u:Lcom/google/android/gms/people/service/h;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    const/16 v3, 0x40

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private a()Z
    .locals 12

    .prologue
    .line 414
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 416
    const/4 v1, 0x0

    .line 417
    const/4 v7, 0x0

    .line 418
    const-string v0, "PeopleSync"

    const-string v2, "Pages"

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    iget-object v2, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const-string v3, "pages"

    invoke-virtual {v0, v3}, Lcom/google/android/gms/people/sync/ap;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, v2, Lcom/google/android/gms/people/sync/y;->f:Z

    .line 422
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->e()Ljava/util/Set;

    move-result-object v9

    move v8, v1

    .line 425
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 428
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 430
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->x:Lcom/google/android/gms/plus/service/v1whitelisted/i;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->q:Lcom/google/android/gms/people/e/a;

    iget-object v1, v1, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    const-string v2, "me"

    const-string v3, "pages"

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/gms/people/a/a;->b(Landroid/content/Context;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 441
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget-wide v4, v0, Lcom/google/android/gms/people/sync/y;->S:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long/2addr v6, v10

    add-long/2addr v4, v6

    iput-wide v4, v0, Lcom/google/android/gms/people/sync/y;->S:J

    .line 444
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 446
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->c()Ljava/util/List;

    move-result-object v3

    .line 448
    invoke-static {v3}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v4

    .line 450
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->w()V

    .line 452
    if-lez v4, :cond_5

    .line 453
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pages."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 455
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v5, v0}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 456
    const-string v0, "PeopleSync"

    const-string v1, "    Up to date"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v4, :cond_5

    .line 459
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;

    .line 460
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->g()Ljava/lang/String;

    move-result-object v0

    .line 462
    invoke-interface {v9, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 463
    iget-object v5, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v5, v0}, Lcom/google/android/gms/people/sync/ap;->f(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_2

    .line 464
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->y()V

    const/4 v0, 0x0

    .line 517
    :goto_4
    return v0

    .line 414
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 420
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 441
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget-wide v2, v1, Lcom/google/android/gms/people/sync/y;->S:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v10

    add-long/2addr v2, v4

    iput-wide v2, v1, Lcom/google/android/gms/people/sync/y;->S:J

    throw v0

    .line 457
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 468
    :cond_3
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v0, v5}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    const/4 v0, 0x0

    move v1, v0

    :goto_5
    if-ge v1, v4, :cond_5

    .line 472
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;

    .line 473
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->g()Ljava/lang/String;

    move-result-object v5

    .line 475
    invoke-interface {v9, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 477
    iget-object v6, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v6, v5}, Lcom/google/android/gms/people/sync/ap;->f(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 478
    iget-object v6, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v6, v0}, Lcom/google/android/gms/people/sync/ap;->b(Lcom/google/android/gms/plus/service/v1whitelisted/models/em;)V

    .line 480
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->g:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v0, Landroid/content/SyncStats;->numUpdates:J

    const-wide/16 v10, 0x1

    add-long/2addr v6, v10

    iput-wide v6, v0, Landroid/content/SyncStats;->numUpdates:J

    .line 481
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget v6, v0, Lcom/google/android/gms/people/sync/y;->d:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v0, Lcom/google/android/gms/people/sync/y;->d:I

    .line 489
    :goto_6
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->u:Lcom/google/android/gms/people/service/h;

    iget-object v6, p0, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-virtual {v0, v6, v5, v7}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 470
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 483
    :cond_4
    iget-object v6, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v6, v0}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/em;)V

    .line 485
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->g:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v0, Landroid/content/SyncStats;->numInserts:J

    const-wide/16 v10, 0x1

    add-long/2addr v6, v10

    iput-wide v6, v0, Landroid/content/SyncStats;->numInserts:J

    .line 486
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget v6, v0, Lcom/google/android/gms/people/sync/y;->c:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v0, Lcom/google/android/gms/people/sync/y;->c:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_6

    .line 496
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/ap;->y()V

    throw v0

    .line 494
    :cond_5
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->z()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 496
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->y()V

    .line 498
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 500
    const/4 v0, 0x0

    move v1, v0

    :goto_7
    if-ge v1, v4, :cond_8

    .line 501
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->m()Lcom/google/android/gms/people/sync/c;

    move-result-object v5

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;

    iget-object v6, p0, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->g()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->h()Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;

    move-result-object v10

    const/4 v0, 0x0

    if-eqz v10, :cond_6

    invoke-interface {v10}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;->e()Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-interface {v10}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v6, v7, v0}, Lcom/google/android/gms/people/sync/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    :cond_6
    if-nez v0, :cond_7

    iget-object v0, v5, Lcom/google/android/gms/people/sync/c;->b:Lcom/google/android/gms/people/f/d;

    invoke-virtual {v0, v6, v7}, Lcom/google/android/gms/people/f/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 505
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 507
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->d()Ljava/lang/String;

    move-result-object v7

    .line 508
    add-int/lit8 v0, v8, 0x1

    .line 509
    if-nez v7, :cond_9

    .line 511
    invoke-direct {p0, v9}, Lcom/google/android/gms/people/sync/z;->a(Ljava/util/Set;)V

    .line 512
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/z;->h()V

    .line 515
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/z;->b()V

    .line 517
    const/4 v0, 0x1

    goto/16 :goto_4

    :cond_9
    move v8, v0

    goto/16 :goto_2
.end method

.method private a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1040
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->b()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v3

    move v2, v1

    .line 1041
    :goto_0
    if-ge v2, v3, :cond_1

    .line 1042
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;

    .line 1043
    const-string v4, "android"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1046
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/google/android/gms/common/util/e;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    .line 1049
    const/4 v0, 0x1

    .line 1056
    :goto_1
    return v0

    :catch_0
    move-exception v0

    .line 1041
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1056
    goto :goto_1
.end method

.method static a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z
    .locals 1

    .prologue
    .line 865
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->l()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->f()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 1236
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    const-string v4, "PeopleSync"

    iget-object v5, p0, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    const-string v7, "Periodic full sync"

    invoke-static {v0, v4, v5, v6, v7}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v3

    .line 1237
    :goto_0
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/people/sync/z;->a(Ljava/lang/String;Z)V

    .line 1238
    if-nez v0, :cond_4

    move v0, v1

    .line 1253
    :goto_1
    return v0

    .line 1236
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/z;->i()Z

    move-result v0

    iget-object v4, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const-string v5, "people"

    invoke-virtual {v4, v5}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const-string v6, "gaiamap"

    invoke-virtual {v5, v6}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const-string v7, "autocomplete"

    invoke-virtual {v6, v7}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/google/android/gms/common/util/g;

    const/4 v8, 0x0

    invoke-direct {v7, v8}, Lcom/google/android/gms/common/util/g;-><init>(B)V

    if-eqz p1, :cond_1

    invoke-interface {v7, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz v0, :cond_2

    invoke-interface {v7, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-interface {v7, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v1, :cond_3

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v0, "PeopleSync"

    const-string v4, "Gaia-map and people sync tokens unexpectedly different."

    invoke-static {v0, v4}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const-string v4, "people_page"

    invoke-virtual {v0, v4}, Lcom/google/android/gms/people/sync/ap;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v3

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1238
    goto :goto_1

    .line 1239
    :catch_0
    move-exception v0

    .line 1240
    iget-object v2, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v2, :cond_5

    iget-object v2, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v2, v2, Lcom/android/volley/m;->a:I

    const/16 v4, 0x19a

    if-eq v2, v4, :cond_6

    .line 1242
    :cond_5
    throw v0

    .line 1244
    :cond_6
    const-string v0, "PeopleSync"

    const-string v2, "Sync Token out of date, syncing all people/gaia-map"

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1246
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iput-boolean v1, v0, Lcom/google/android/gms/people/sync/y;->p:Z

    .line 1249
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const-string v2, "people_page"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/sync/ap;->b(Ljava/lang/String;)V

    .line 1252
    invoke-direct {p0, v3, p1}, Lcom/google/android/gms/people/sync/z;->a(Ljava/lang/String;Z)V

    move v0, v1

    .line 1253
    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/y;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    return-object v0
.end method

.method private b()V
    .locals 19

    .prologue
    .line 524
    const-string v2, "PeopleSync"

    const-string v3, "Page covers"

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    sget-object v2, Lcom/google/android/gms/people/a/a;->ad:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    .line 526
    const-string v2, "PeopleSync"

    const-string v3, "    Skipped"

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    :cond_0
    return-void

    .line 529
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/android/gms/people/sync/y;->g:Z

    .line 531
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/ap;->e()Ljava/util/Set;

    move-result-object v2

    .line 532
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v3

    if-eqz v3, :cond_0

    .line 536
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/z;->s:Lcom/google/android/gms/people/service/g;

    invoke-virtual {v3}, Lcom/google/android/gms/people/service/g;->c()V

    .line 541
    const/4 v3, 0x1

    new-array v12, v3, [Ljava/lang/Long;

    .line 543
    const/4 v3, 0x1

    new-array v13, v3, [Lcom/android/volley/ac;

    .line 544
    new-instance v14, Lcom/google/android/gms/people/sync/aa;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v12, v13}, Lcom/google/android/gms/people/sync/aa;-><init>(Lcom/google/android/gms/people/sync/z;[Ljava/lang/Long;[Lcom/android/volley/ac;)V

    .line 554
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 556
    new-instance v16, Lcom/google/android/gms/people/sync/ab;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v12, v10}, Lcom/google/android/gms/people/sync/ab;-><init>(Lcom/google/android/gms/people/sync/z;[Ljava/lang/Long;Ljava/lang/String;)V

    .line 569
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/people/sync/z;->v:Lcom/google/android/gms/plus/service/v2whitelisted/b;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->q:Lcom/google/android/gms/people/e/a;

    iget-object v0, v2, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v18, v0

    const-string v3, "me"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    sget-object v2, Lcom/google/android/gms/people/sync/z;->d:Lcom/google/android/gms/plus/service/v2whitelisted/c;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    invoke-static/range {v2 .. v11}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Lcom/google/android/gms/plus/service/v2whitelisted/c;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a:Lcom/google/android/gms/common/server/n;

    const-class v5, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    move-object/from16 v3, v18

    move-object/from16 v6, v16

    move-object v7, v14

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Class;Lcom/android/volley/x;Lcom/android/volley/w;)V

    goto :goto_0

    .line 586
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 588
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->s:Lcom/google/android/gms/people/service/g;

    invoke-virtual {v2}, Lcom/google/android/gms/people/service/g;->e()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 592
    const/4 v2, 0x0

    aget-object v2, v12, v2

    if-eqz v2, :cond_3

    .line 593
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget-wide v6, v2, Lcom/google/android/gms/people/sync/y;->S:J

    const/4 v3, 0x0

    aget-object v3, v12, v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long v4, v8, v4

    add-long/2addr v4, v6

    iput-wide v4, v2, Lcom/google/android/gms/people/sync/y;->S:J

    .line 597
    :cond_3
    const/4 v2, 0x0

    aget-object v2, v13, v2

    if-eqz v2, :cond_0

    .line 598
    const/4 v2, 0x0

    aget-object v2, v13, v2

    throw v2

    .line 589
    :catch_0
    move-exception v2

    .line 590
    :try_start_1
    new-instance v3, Lcom/google/android/gms/people/sync/x;

    invoke-direct {v3, v2}, Lcom/google/android/gms/people/sync/x;-><init>(Ljava/lang/Exception;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592
    :catchall_0
    move-exception v2

    const/4 v3, 0x0

    aget-object v3, v12, v3

    if-eqz v3, :cond_4

    .line 593
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget-wide v6, v3, Lcom/google/android/gms/people/sync/y;->S:J

    const/4 v8, 0x0

    aget-object v8, v12, v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long v4, v8, v4

    add-long/2addr v4, v6

    iput-wide v4, v3, Lcom/google/android/gms/people/sync/y;->S:J

    :cond_4
    throw v2
.end method

.method static b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z
    .locals 2

    .prologue
    .line 878
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->m()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->m()Ljava/util/List;

    move-result-object v0

    const-string v1, "myContacts"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/people/sync/z;)Landroid/content/SyncResult;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->g:Landroid/content/SyncResult;

    return-object v0
.end method

.method private c()Z
    .locals 18

    .prologue
    .line 622
    const/4 v5, 0x0

    .line 623
    const/4 v4, 0x0

    .line 624
    const/4 v7, 0x0

    .line 625
    const-string v2, "PeopleSync"

    const-string v3, "Groups"

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const-string v6, "groups"

    invoke-virtual {v2, v6}, Lcom/google/android/gms/people/sync/ap;->d(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, v3, Lcom/google/android/gms/people/sync/y;->k:Z

    .line 629
    const/4 v3, 0x0

    .line 630
    const/4 v2, 0x0

    .line 632
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v6}, Lcom/google/android/gms/people/sync/ap;->c()Ljava/util/Set;

    move-result-object v12

    move-object v8, v2

    move v9, v3

    move v10, v4

    move v11, v5

    .line 635
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 638
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    .line 640
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->y:Lcom/google/android/gms/plus/service/v1whitelisted/d;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/z;->q:Lcom/google/android/gms/people/e/a;

    iget-object v3, v3, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/z;->q:Lcom/google/android/gms/people/e/a;

    invoke-virtual {v4}, Lcom/google/android/gms/people/e/a;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/gms/people/a/a;->b(Landroid/content/Context;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/plus/service/v1whitelisted/d;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v13

    .line 648
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget-wide v4, v2, Lcom/google/android/gms/people/sync/y;->S:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long/2addr v6, v14

    add-long/2addr v4, v6

    iput-wide v4, v2, Lcom/google/android/gms/people/sync/y;->S:J

    .line 651
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 653
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/ap;->w()V

    .line 655
    :try_start_1
    invoke-virtual {v13}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;->c()Ljava/util/List;

    move-result-object v14

    .line 656
    invoke-static {v14}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v15

    .line 657
    if-lez v15, :cond_e

    .line 658
    const/4 v3, 0x0

    .line 660
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "groups."

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 661
    invoke-virtual {v13}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;->b()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v5, v2}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 662
    const-string v2, "PeopleSync"

    const-string v4, "    Up to date"

    invoke-static {v2, v4}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    const/4 v2, 0x0

    move v4, v2

    :goto_2
    if-ge v4, v15, :cond_3

    .line 664
    invoke-interface {v14, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;

    .line 665
    const-string v5, "circle"

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->b()Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 666
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->b()Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->f()Ljava/lang/String;

    move-result-object v2

    .line 667
    const/4 v5, 0x2

    invoke-static {v5}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 668
    const-string v5, "PeopleSync"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Group: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 670
    :cond_0
    invoke-interface {v12, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 671
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v5, v2}, Lcom/google/android/gms/people/sync/ap;->i(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-nez v2, :cond_2

    .line 672
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/ap;->y()V

    const/4 v2, 0x0

    .line 752
    :goto_3
    return v2

    .line 627
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 648
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget-wide v4, v3, Lcom/google/android/gms/people/sync/y;->S:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long/2addr v6, v14

    add-long/2addr v4, v6

    iput-wide v4, v3, Lcom/google/android/gms/people/sync/y;->S:J

    throw v2

    .line 663
    :cond_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_2

    .line 676
    :cond_3
    add-int v2, v11, v15

    move v4, v2

    .line 710
    :goto_4
    const/4 v2, 0x0

    move v7, v2

    move-object v5, v8

    move v6, v9

    :goto_5
    if-ge v7, v15, :cond_8

    .line 711
    :try_start_2
    invoke-interface {v14, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;

    .line 712
    const-string v8, "circle"

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->b()Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;

    move-result-object v9

    invoke-interface {v9}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->f()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_c

    .line 713
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->b()Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;

    move-result-object v2

    .line 714
    const-string v8, "domain"

    invoke-interface {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->f()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 715
    const/4 v5, 0x1

    .line 716
    invoke-interface {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->d()Ljava/lang/String;

    move-result-object v2

    .line 710
    :goto_6
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    move v6, v5

    move-object v5, v2

    goto :goto_5

    .line 678
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v13}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    const/4 v3, 0x1

    .line 681
    const/4 v2, 0x0

    move v4, v2

    move v2, v11

    :goto_7
    if-ge v4, v15, :cond_d

    .line 682
    add-int/lit8 v11, v2, 0x1

    .line 683
    invoke-interface {v14, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;

    .line 684
    const-string v5, "circle"

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->b()Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 685
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->b()Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->f()Ljava/lang/String;

    move-result-object v5

    .line 686
    const/4 v6, 0x2

    invoke-static {v6}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 687
    const-string v6, "PeopleSync"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v16, "Group: "

    move-object/from16 v0, v16

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    :cond_5
    invoke-interface {v12, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 692
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v6, v5}, Lcom/google/android/gms/people/sync/ap;->i(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 693
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v5, v2, v11}, Lcom/google/android/gms/people/sync/ap;->b(Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;I)V

    .line 695
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->g:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v2, Landroid/content/SyncStats;->numUpdates:J

    const-wide/16 v16, 0x1

    add-long v6, v6, v16

    iput-wide v6, v2, Landroid/content/SyncStats;->numUpdates:J

    .line 696
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget v5, v2, Lcom/google/android/gms/people/sync/y;->i:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v2, Lcom/google/android/gms/people/sync/y;->i:I

    .line 681
    :cond_6
    :goto_8
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v11

    goto :goto_7

    .line 698
    :cond_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v5, v2, v11}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;I)V

    .line 700
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->g:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v2, Landroid/content/SyncStats;->numInserts:J

    const-wide/16 v16, 0x1

    add-long v6, v6, v16

    iput-wide v6, v2, Landroid/content/SyncStats;->numInserts:J

    .line 701
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget v5, v2, Lcom/google/android/gms/people/sync/y;->h:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v2, Lcom/google/android/gms/people/sync/y;->h:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_8

    .line 729
    :catchall_1
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v3}, Lcom/google/android/gms/people/sync/ap;->y()V

    throw v2

    .line 721
    :cond_8
    if-eqz v3, :cond_9

    .line 722
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->u:Lcom/google/android/gms/people/service/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    const/4 v8, 0x2

    invoke-virtual {v2, v3, v7, v8}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_9
    move-object v2, v5

    move v3, v6

    move v5, v4

    .line 727
    :goto_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v4}, Lcom/google/android/gms/people/sync/ap;->z()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 729
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v4}, Lcom/google/android/gms/people/sync/ap;->y()V

    .line 732
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v4}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 734
    invoke-virtual {v13}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;->d()Ljava/lang/String;

    move-result-object v7

    .line 735
    add-int/lit8 v4, v10, 0x1

    .line 736
    if-nez v7, :cond_b

    .line 740
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    if-nez v4, :cond_a

    .line 742
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v4}, Lcom/google/android/gms/people/sync/ap;->w()V

    .line 744
    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v4, v3, v2}, Lcom/google/android/gms/people/sync/ap;->a(ZLjava/lang/String;)V

    .line 745
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/ap;->z()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 747
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/ap;->y()V

    .line 751
    :cond_a
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v2}, Lcom/google/android/gms/people/sync/z;->a(Ljava/util/Set;Z)V

    .line 752
    const/4 v2, 0x1

    goto/16 :goto_3

    .line 747
    :catchall_2
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v3}, Lcom/google/android/gms/people/sync/ap;->y()V

    throw v2

    :cond_b
    move-object v8, v2

    move v9, v3

    move v10, v4

    move v11, v5

    goto/16 :goto_1

    :cond_c
    move-object v2, v5

    move v5, v6

    goto/16 :goto_6

    :cond_d
    move v4, v2

    goto/16 :goto_4

    :cond_e
    move-object v2, v8

    move v3, v9

    move v5, v11

    goto :goto_9
.end method

.method static synthetic d(Lcom/google/android/gms/people/sync/z;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 757
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/z;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 758
    const-string v0, "PeopleSync"

    const-string v1, "Sync inconsistency.  Resyncing circles."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const-string v1, "circles"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/sync/ap;->c(Ljava/lang/String;)V

    .line 760
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/z;->e()Z

    .line 762
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/people/sync/z;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    return-object v0
.end method

.method private e()Z
    .locals 15

    .prologue
    .line 767
    const/4 v3, 0x0

    .line 768
    const/4 v2, 0x0

    .line 769
    const/4 v1, 0x0

    .line 770
    const-string v0, "PeopleSync"

    const-string v4, "Circles"

    invoke-static {v0, v4}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    iget-object v4, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const-string v5, "circles"

    invoke-virtual {v0, v5}, Lcom/google/android/gms/people/sync/ap;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v4, Lcom/google/android/gms/people/sync/y;->o:Z

    .line 774
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->d()Ljava/util/Set;

    move-result-object v8

    move-object v0, v1

    move v6, v2

    move v7, v3

    .line 777
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 780
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 782
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/people/sync/z;->w:Lcom/google/android/gms/plus/service/v2whitelisted/a;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->q:Lcom/google/android/gms/people/e/a;

    iget-object v1, v1, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Lcom/google/android/gms/people/sync/z;->q:Lcom/google/android/gms/people/e/a;

    invoke-virtual {v3}, Lcom/google/android/gms/people/e/a;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/people/a/a;->b(Landroid/content/Context;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "people/%1$s/circles"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v3}, Lcom/google/android/gms/plus/service/v2whitelisted/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v9, v12

    invoke-static {v5, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v4, :cond_0

    const-string v5, "maxResults"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v5, v4}, Lcom/google/android/gms/plus/service/v2whitelisted/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz v0, :cond_1

    const-string v4, "pageToken"

    invoke-static {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    iget-object v0, v2, Lcom/google/android/gms/plus/service/v2whitelisted/a;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 789
    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget-wide v2, v1, Lcom/google/android/gms/people/sync/y;->S:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v10

    add-long/2addr v2, v4

    iput-wide v2, v1, Lcom/google/android/gms/people/sync/y;->S:J

    .line 792
    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 794
    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/ap;->w()V

    .line 797
    :try_start_1
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->c()Ljava/util/List;

    move-result-object v4

    .line 798
    invoke-static {v4}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v5

    .line 800
    if-lez v5, :cond_a

    .line 801
    const/4 v2, 0x0

    .line 802
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "circles."

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 804
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v9, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v9, v1}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 805
    const-string v1, "PeopleSync"

    const-string v3, "    Up to date"

    invoke-static {v1, v3}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    const/4 v1, 0x0

    move v3, v1

    :goto_2
    if-ge v3, v5, :cond_4

    .line 807
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/a;

    .line 808
    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/a;->e()Ljava/lang/String;

    move-result-object v1

    .line 809
    invoke-interface {v8, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 810
    iget-object v9, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v9, v1}, Lcom/google/android/gms/people/sync/ap;->i(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-nez v1, :cond_3

    .line 811
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->y()V

    const/4 v0, 0x0

    .line 860
    :goto_3
    return v0

    .line 772
    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 789
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget-wide v2, v1, Lcom/google/android/gms/people/sync/y;->S:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v10

    add-long/2addr v2, v4

    iput-wide v2, v1, Lcom/google/android/gms/people/sync/y;->S:J

    throw v0

    .line 806
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 814
    :cond_4
    add-int v1, v7, v5

    move v14, v2

    move v2, v1

    move v1, v14

    .line 840
    :goto_4
    :try_start_2
    iget-object v3, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v3}, Lcom/google/android/gms/people/sync/ap;->v()V

    .line 842
    if-eqz v1, :cond_5

    .line 843
    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->u:Lcom/google/android/gms/people/service/h;

    iget-object v3, p0, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    const/4 v5, 0x2

    invoke-virtual {v1, v3, v4, v5}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 848
    :cond_5
    :goto_5
    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/ap;->z()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 850
    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/ap;->y()V

    .line 853
    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 855
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->d()Ljava/lang/String;

    move-result-object v0

    .line 856
    add-int/lit8 v1, v6, 0x1

    .line 857
    if-nez v0, :cond_8

    .line 859
    const/4 v0, 0x1

    invoke-direct {p0, v8, v0}, Lcom/google/android/gms/people/sync/z;->a(Ljava/util/Set;Z)V

    .line 860
    const/4 v0, 0x1

    goto :goto_3

    .line 816
    :cond_6
    :try_start_3
    iget-object v2, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 818
    const/4 v2, 0x1

    .line 819
    const/4 v1, 0x0

    move v3, v1

    move v1, v7

    :goto_6
    if-ge v3, v5, :cond_9

    .line 820
    add-int/lit8 v7, v1, 0x1

    .line 821
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/a;

    .line 822
    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/a;->e()Ljava/lang/String;

    move-result-object v9

    .line 824
    invoke-interface {v8, v9}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 826
    iget-object v10, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v10, v9}, Lcom/google/android/gms/people/sync/ap;->i(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 827
    iget-object v9, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v9, v1, v7}, Lcom/google/android/gms/people/sync/ap;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/a;I)V

    .line 829
    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->g:Landroid/content/SyncResult;

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v10, v1, Landroid/content/SyncStats;->numUpdates:J

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    iput-wide v10, v1, Landroid/content/SyncStats;->numUpdates:J

    .line 830
    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget v9, v1, Lcom/google/android/gms/people/sync/y;->m:I

    add-int/lit8 v9, v9, 0x1

    iput v9, v1, Lcom/google/android/gms/people/sync/y;->m:I

    .line 819
    :goto_7
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v7

    goto :goto_6

    .line 832
    :cond_7
    iget-object v9, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v9, v1, v7}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/a;I)V

    .line 834
    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->g:Landroid/content/SyncResult;

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v10, v1, Landroid/content/SyncStats;->numInserts:J

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    iput-wide v10, v1, Landroid/content/SyncStats;->numInserts:J

    .line 835
    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget v9, v1, Lcom/google/android/gms/people/sync/y;->l:I

    add-int/lit8 v9, v9, 0x1

    iput v9, v1, Lcom/google/android/gms/people/sync/y;->l:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_7

    .line 850
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/ap;->y()V

    throw v0

    :cond_8
    move v6, v1

    move v7, v2

    goto/16 :goto_1

    :cond_9
    move v14, v2

    move v2, v1

    move v1, v14

    goto/16 :goto_4

    :cond_a
    move v2, v7

    goto/16 :goto_5
.end method

.method static synthetic f(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/service/h;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->u:Lcom/google/android/gms/people/service/h;

    return-object v0
.end method

.method private f()V
    .locals 14

    .prologue
    const/4 v11, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 896
    sget-object v0, Lcom/google/android/gms/people/a/a;->x:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 897
    const-string v0, "PeopleSync"

    const-string v1, "Applications (Skipped)"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1036
    :cond_0
    return-void

    .line 901
    :cond_1
    const-string v0, "PeopleSync"

    const-string v1, "Applications"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 903
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 905
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->l()Ljava/util/Set;

    move-result-object v8

    .line 908
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    move-object v6, v4

    .line 911
    :cond_2
    const-string v0, "PeopleSync"

    const-string v1, "  Getting Connected Apps"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 912
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->z:Lcom/google/android/gms/plus/service/v1whitelisted/c;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->q:Lcom/google/android/gms/people/e/a;

    iget-object v1, v1, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    const-string v2, "me"

    const-string v3, "connected"

    iget-object v5, p0, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/gms/people/a/a;->b(Landroid/content/Context;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/service/v1whitelisted/c;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/Applications;

    move-result-object v2

    .line 921
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Applications;->b()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v3

    move v1, v7

    .line 922
    :goto_0
    if-ge v1, v3, :cond_4

    .line 923
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Applications;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    .line 924
    invoke-direct {p0, v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 925
    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 922
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 928
    :cond_4
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Applications;->c()Ljava/lang/String;

    move-result-object v6

    .line 929
    if-nez v6, :cond_2

    .line 934
    new-array v10, v11, [Ljava/lang/Long;

    .line 936
    new-array v11, v11, [Lcom/android/volley/ac;

    .line 937
    new-instance v6, Lcom/google/android/gms/people/sync/ac;

    invoke-direct {v6, p0, v10, v11}, Lcom/google/android/gms/people/sync/ac;-><init>(Lcom/google/android/gms/people/sync/z;[Ljava/lang/Long;[Lcom/android/volley/ac;)V

    .line 947
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->r:Lcom/google/android/gms/people/service/g;

    invoke-virtual {v0}, Lcom/google/android/gms/people/service/g;->c()V

    .line 948
    invoke-virtual {v9}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    .line 949
    new-instance v5, Lcom/google/android/gms/people/sync/ad;

    invoke-direct {v5, p0, v10, v2, v8}, Lcom/google/android/gms/people/sync/ad;-><init>(Lcom/google/android/gms/people/sync/z;[Ljava/lang/Long;Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;Ljava/util/Set;)V

    .line 1000
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->A:Lcom/google/android/gms/plus/service/v1whitelisted/a;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->q:Lcom/google/android/gms/people/e/a;

    iget-object v1, v1, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->e()Ljava/lang/String;

    move-result-object v2

    const-string v3, "visible"

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    goto :goto_1

    .line 1010
    :cond_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 1012
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->r:Lcom/google/android/gms/people/service/g;

    invoke-virtual {v0}, Lcom/google/android/gms/people/service/g;->e()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1016
    aget-object v0, v10, v7

    if-eqz v0, :cond_6

    .line 1017
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget-wide v4, v0, Lcom/google/android/gms/people/sync/y;->S:J

    aget-object v1, v10, v7

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    sub-long v2, v12, v2

    add-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/gms/people/sync/y;->S:J

    .line 1021
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->w()V

    .line 1023
    :try_start_1
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1024
    iget-object v2, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/sync/ap;->l(Ljava/lang/String;)V

    .line 1025
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget v2, v0, Lcom/google/android/gms/people/sync/y;->N:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/android/gms/people/sync/y;->N:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 1030
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/ap;->y()V

    throw v0

    .line 1013
    :catch_0
    move-exception v0

    .line 1014
    :try_start_2
    new-instance v1, Lcom/google/android/gms/people/sync/x;

    invoke-direct {v1, v0}, Lcom/google/android/gms/people/sync/x;-><init>(Ljava/lang/Exception;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1016
    :catchall_1
    move-exception v0

    aget-object v1, v10, v7

    if-eqz v1, :cond_7

    .line 1017
    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget-wide v4, v1, Lcom/google/android/gms/people/sync/y;->S:J

    aget-object v6, v10, v7

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long v2, v6, v2

    add-long/2addr v2, v4

    iput-wide v2, v1, Lcom/google/android/gms/people/sync/y;->S:J

    :cond_7
    throw v0

    .line 1028
    :cond_8
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->z()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1030
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->y()V

    .line 1033
    aget-object v0, v11, v7

    if-eqz v0, :cond_0

    .line 1034
    aget-object v0, v11, v7

    throw v0
.end method

.method static synthetic g(Lcom/google/android/gms/people/sync/z;)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/v;->c()V

    return-void
.end method

.method private g()Z
    .locals 12

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 1061
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v7

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 1062
    const-string v0, "PeopleSync"

    const-string v1, "Me"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1064
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 1066
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 1069
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->v:Lcom/google/android/gms/plus/service/v2whitelisted/b;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->q:Lcom/google/android/gms/people/e/a;

    iget-object v1, v1, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    const-string v2, "me"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "disabled"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "blocked"

    aput-object v6, v4, v5

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/gms/people/sync/z;->a:Lcom/google/android/gms/plus/service/v2whitelisted/c;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/c;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 1085
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget-wide v2, v0, Lcom/google/android/gms/people/sync/y;->S:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v10

    add-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/gms/people/sync/y;->S:J

    .line 1088
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 1090
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->w()V

    .line 1092
    :try_start_1
    invoke-virtual {v6}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const-string v2, "me"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1093
    const-string v0, "PeopleSync"

    const-string v1, "    Up to date"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->z()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1105
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->y()V

    .line 1107
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 1109
    iget-boolean v0, p0, Lcom/google/android/gms/people/sync/z;->k:Z

    if-nez v0, :cond_1

    .line 1110
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->m()Lcom/google/android/gms/people/sync/c;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v6}, Lcom/google/android/gms/people/sync/e;->c(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;->e()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;->b()Z

    move-result v3

    if-nez v3, :cond_7

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v9, v0}, Lcom/google/android/gms/people/sync/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    :goto_2
    if-nez v0, :cond_0

    iget-object v0, v1, Lcom/google/android/gms/people/sync/c;->b:Lcom/google/android/gms/people/f/d;

    invoke-virtual {v0, v2, v9}, Lcom/google/android/gms/people/f/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1112
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->n()Lcom/google/android/gms/people/sync/d;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/d;->a()V

    new-instance v0, Lcom/google/android/gms/people/sync/ap;

    iget-object v3, v2, Lcom/google/android/gms/people/sync/d;->a:Landroid/content/Context;

    invoke-direct {v0, v3, v1, v9}, Lcom/google/android/gms/people/sync/ap;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->k()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-static {v0}, Lcom/google/android/gms/people/f/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v2, Lcom/google/android/gms/people/sync/d;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/a/a;->a(Landroid/content/Context;)I

    move-result v5

    iget-object v0, v2, Lcom/google/android/gms/people/sync/d;->b:Lcom/google/android/gms/people/f/f;

    invoke-static {v3}, Lcom/google/android/gms/people/f/h;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v9, v4, v5}, Lcom/google/android/gms/people/f/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v3, v5}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iget-object v4, v2, Lcom/google/android/gms/people/sync/d;->a:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/people/service/m;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/m;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/android/gms/people/service/m;->a(Ljava/lang/String;)[B

    move-result-object v4

    if-eqz v4, :cond_1

    sget-object v0, Lcom/google/android/gms/people/service/a/a/e;->i:[B

    if-eq v4, v0, :cond_1

    sget-object v0, Lcom/google/android/gms/people/service/a/a/e;->j:[B

    if-eq v4, v0, :cond_1

    iget-object v0, v2, Lcom/google/android/gms/people/sync/d;->b:Lcom/google/android/gms/people/f/f;

    invoke-static {v3}, Lcom/google/android/gms/people/f/h;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v2, v9

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/people/f/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BI)Z

    .line 1116
    :cond_1
    :goto_4
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 1118
    invoke-virtual {v6}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v6}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->s()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v6}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->r()Ljava/util/List;

    move-result-object v0

    const-string v1, "googlePlus"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v6}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->r()Ljava/util/List;

    move-result-object v0

    const-string v1, "googlePlusDisabledByAdmin"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    :goto_5
    return v7

    :cond_2
    move v0, v8

    .line 1061
    goto/16 :goto_0

    .line 1085
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget-wide v2, v1, Lcom/google/android/gms/people/sync/y;->S:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v10

    add-long/2addr v2, v4

    iput-wide v2, v1, Lcom/google/android/gms/people/sync/y;->S:J

    throw v0

    .line 1095
    :cond_3
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    .line 1097
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->g:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numUpdates:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numUpdates:J

    .line 1098
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->u:Lcom/google/android/gms/people/service/h;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1100
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    const-string v1, "me"

    invoke-virtual {v6}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto/16 :goto_1

    .line 1105
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/ap;->y()V

    throw v0

    .line 1112
    :cond_4
    invoke-virtual {v0, v9}, Lcom/google/android/gms/people/sync/ap;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    :cond_5
    iget-object v0, v2, Lcom/google/android/gms/people/sync/d;->b:Lcom/google/android/gms/people/f/f;

    invoke-virtual {v0, v1, v9}, Lcom/google/android/gms/people/f/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_6
    move v7, v8

    .line 1118
    goto :goto_5

    :cond_7
    move v0, v8

    goto/16 :goto_2
.end method

.method private h()V
    .locals 5

    .prologue
    .line 1148
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 1149
    const-string v0, "PeopleSync"

    const-string v1, "Orphaned pages sync requests"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->w()V

    .line 1152
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->i()Ljava/util/List;

    move-result-object v0

    .line 1153
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1154
    iget-object v2, p0, Lcom/google/android/gms/people/sync/z;->u:Lcom/google/android/gms/people/service/h;

    iget-object v3, p0, Lcom/google/android/gms/people/sync/z;->n:Ljava/lang/String;

    const/16 v4, 0x40

    invoke-virtual {v2, v3, v0, v4}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1160
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/ap;->y()V

    throw v0

    .line 1148
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1157
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->h()V

    .line 1158
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->z()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1160
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->y()V

    .line 1162
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 1163
    return-void
.end method

.method static synthetic h(Lcom/google/android/gms/people/sync/z;)Z
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/z;->i()Z

    move-result v0

    return v0
.end method

.method private i()Z
    .locals 1

    .prologue
    .line 1788
    iget-object v0, p0, Lcom/google/android/gms/people/sync/z;->o:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 18

    .prologue
    .line 2228
    const-string v2, "PeopleSync"

    const-string v3, "Sync sanity check."

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2236
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/ap;->w()V

    .line 2238
    const/4 v12, 0x0

    .line 2240
    :cond_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 2242
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-wide v16

    .line 2245
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->v:Lcom/google/android/gms/plus/service/v2whitelisted/b;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/z;->q:Lcom/google/android/gms/people/e/a;

    iget-object v3, v3, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    const-string v4, "me"

    const-string v5, "all"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    invoke-static {v9}, Lcom/google/android/gms/people/a/a;->c(Landroid/content/Context;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/people/sync/z;->q:Lcom/google/android/gms/people/e/a;

    iget-object v10, v10, Lcom/google/android/gms/people/e/a;->b:Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v13, 0x0

    sget-object v14, Lcom/google/android/gms/people/sync/z;->c:Lcom/google/android/gms/plus/service/v2whitelisted/d;

    invoke-virtual/range {v2 .. v14}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/d;)Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 2266
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget-wide v6, v2, Lcom/google/android/gms/people/sync/y;->S:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    sub-long v8, v8, v16

    add-long/2addr v6, v8

    iput-wide v6, v2, Lcom/google/android/gms/people/sync/y;->S:J

    .line 2269
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->h:Lcom/google/android/gms/people/sync/v;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/v;->c()V

    .line 2271
    invoke-virtual {v4}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;->b()Ljava/util/List;

    move-result-object v5

    .line 2272
    invoke-static {v5}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v6

    .line 2274
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_1

    .line 2275
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    invoke-virtual {v7, v2, v8}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Lcom/google/android/gms/people/sync/y;)V

    .line 2274
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2266
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget-wide v4, v3, Lcom/google/android/gms/people/sync/y;->S:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v6, v6, v16

    add-long/2addr v4, v6

    iput-wide v4, v3, Lcom/google/android/gms/people/sync/y;->S:J

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2287
    :catchall_1
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v3}, Lcom/google/android/gms/people/sync/ap;->y()V

    throw v2

    .line 2278
    :cond_1
    :try_start_3
    invoke-virtual {v4}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;->c()Ljava/lang/String;

    move-result-object v12

    .line 2279
    if-nez v12, :cond_0

    .line 2281
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v3}, Lcom/google/android/gms/people/sync/ap;->s()I

    move-result v3

    iput v3, v2, Lcom/google/android/gms/people/sync/y;->U:I

    .line 2282
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v3}, Lcom/google/android/gms/people/sync/ap;->t()I

    move-result v3

    iput v3, v2, Lcom/google/android/gms/people/sync/y;->V:I

    .line 2283
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v3}, Lcom/google/android/gms/people/sync/ap;->u()I

    move-result v3

    iput v3, v2, Lcom/google/android/gms/people/sync/y;->W:I

    .line 2285
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/ap;->z()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2287
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->t:Lcom/google/android/gms/people/sync/ap;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/ap;->y()V

    .line 2290
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget v3, v2, Lcom/google/android/gms/people/sync/y;->T:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/google/android/gms/people/sync/y;->T:I

    .line 2292
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2293
    const-string v3, "PeopleSync"

    const-string v4, "Sanity-check: [%s] DB people count=%d/%d  Actual=%d  inconsistent-members=%d"

    const/4 v2, 0x5

    new-array v5, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    invoke-virtual {v2}, Lcom/google/android/gms/people/sync/y;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "OK"

    :goto_1
    aput-object v2, v5, v6

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget v6, v6, Lcom/google/android/gms/people/sync/y;->V:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget v6, v6, Lcom/google/android/gms/people/sync/y;->U:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget v6, v6, Lcom/google/android/gms/people/sync/y;->X:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v2, 0x4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    iget v6, v6, Lcom/google/android/gms/people/sync/y;->Z:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2301
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/z;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/ah;->k()Lcom/google/android/gms/people/f/b;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/gms/people/sync/z;->j:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/z;->i:Lcom/google/android/gms/people/sync/y;

    invoke-static {v4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v5, Lcom/google/android/gms/f/a/f;

    invoke-direct {v5}, Lcom/google/android/gms/f/a/f;-><init>()V

    new-instance v6, Lcom/google/android/gms/f/a/e;

    invoke-direct {v6}, Lcom/google/android/gms/f/a/e;-><init>()V

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iput-object v7, v6, Lcom/google/android/gms/f/a/e;->a:Ljava/lang/Boolean;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v6, Lcom/google/android/gms/f/a/e;->b:Ljava/lang/Integer;

    invoke-virtual {v4}, Lcom/google/android/gms/people/sync/y;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v6, Lcom/google/android/gms/f/a/e;->c:Ljava/lang/Boolean;

    invoke-virtual {v4}, Lcom/google/android/gms/people/sync/y;->b()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v6, Lcom/google/android/gms/f/a/e;->d:Ljava/lang/Boolean;

    iget v3, v4, Lcom/google/android/gms/people/sync/y;->U:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v6, Lcom/google/android/gms/f/a/e;->e:Ljava/lang/Integer;

    iget v3, v4, Lcom/google/android/gms/people/sync/y;->V:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v6, Lcom/google/android/gms/f/a/e;->f:Ljava/lang/Integer;

    iget v3, v4, Lcom/google/android/gms/people/sync/y;->W:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v6, Lcom/google/android/gms/f/a/e;->g:Ljava/lang/Integer;

    iget v3, v4, Lcom/google/android/gms/people/sync/y;->X:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v6, Lcom/google/android/gms/f/a/e;->h:Ljava/lang/Integer;

    iget v3, v4, Lcom/google/android/gms/people/sync/y;->Y:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v6, Lcom/google/android/gms/f/a/e;->i:Ljava/lang/Integer;

    iget v3, v4, Lcom/google/android/gms/people/sync/y;->V:I

    iget v7, v4, Lcom/google/android/gms/people/sync/y;->X:I

    sub-int/2addr v3, v7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v6, Lcom/google/android/gms/f/a/e;->j:Ljava/lang/Integer;

    iget v3, v4, Lcom/google/android/gms/people/sync/y;->W:I

    iget v7, v4, Lcom/google/android/gms/people/sync/y;->Y:I

    sub-int/2addr v3, v7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v6, Lcom/google/android/gms/f/a/e;->k:Ljava/lang/Integer;

    iget v3, v4, Lcom/google/android/gms/people/sync/y;->Z:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v6, Lcom/google/android/gms/f/a/e;->l:Ljava/lang/Integer;

    iget v3, v4, Lcom/google/android/gms/people/sync/y;->aa:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v6, Lcom/google/android/gms/f/a/e;->m:Ljava/lang/Integer;

    iget-boolean v3, v4, Lcom/google/android/gms/people/sync/y;->r:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v6, Lcom/google/android/gms/f/a/e;->n:Ljava/lang/Boolean;

    iput-object v6, v5, Lcom/google/android/gms/f/a/f;->c:Lcom/google/android/gms/f/a/e;

    iget-object v2, v2, Lcom/google/android/gms/people/f/b;->a:Lcom/google/android/gms/people/f/c;

    invoke-static {v5}, Lcom/google/android/gms/common/analytics/f;->a(Lcom/google/android/gms/f/a/f;)V

    .line 2302
    return-void

    .line 2293
    :cond_3
    const-string v2, "FAIL"

    goto/16 :goto_1
.end method


# virtual methods
.method final a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/util/Set;Lcom/google/android/gms/people/internal/bf;)V
    .locals 5

    .prologue
    .line 1900
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1936
    :cond_0
    :goto_0
    return-void

    .line 1904
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v2

    .line 1906
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->f()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v3

    .line 1908
    if-eqz v3, :cond_0

    .line 1914
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    .line 1915
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1916
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1917
    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1914
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1922
    :cond_3
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1929
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->q()Ljava/lang/String;

    move-result-object v0

    .line 1930
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1933
    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->C:Lcom/google/android/gms/people/sync/ai;

    invoke-virtual {v1, p1, v0, p3}, Lcom/google/android/gms/people/sync/ai;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/lang/String;Lcom/google/android/gms/people/internal/bf;)V

    .line 1934
    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->D:Lcom/google/android/gms/people/sync/an;

    invoke-virtual {v1, p1, v0, p3}, Lcom/google/android/gms/people/sync/an;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/lang/String;Lcom/google/android/gms/people/internal/bf;)V

    .line 1935
    iget-object v1, p0, Lcom/google/android/gms/people/sync/z;->E:Lcom/google/android/gms/people/sync/ao;

    invoke-virtual {v1, p1, v0, p3}, Lcom/google/android/gms/people/sync/ao;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/lang/String;Lcom/google/android/gms/people/internal/bf;)V

    goto :goto_0
.end method
