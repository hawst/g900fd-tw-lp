.class public final Lcom/google/android/gms/cast/b/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/cast/b/t;


# static fields
.field private static W:Ljava/util/HashMap;

.field private static final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final b:Ljava/lang/String;

.field private static final c:Z

.field private static final d:Z

.field private static final e:J

.field private static final m:Ljava/util/concurrent/atomic/AtomicLong;

.field private static w:Ljava/util/concurrent/atomic/AtomicLong;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Lcom/google/android/gms/cast/LaunchOptions;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private final F:J

.field private final G:Ljava/lang/Runnable;

.field private H:Z

.field private I:Z

.field private J:Z

.field private K:I

.field private L:J

.field private final M:Z

.field private final N:Lcom/google/android/gms/cast/d/a;

.field private final O:Lcom/google/android/gms/cast/d/d;

.field private final P:Lcom/google/android/gms/cast/b/aj;

.field private final Q:Ljava/lang/Runnable;

.field private R:Ljava/lang/String;

.field private S:Ljava/lang/String;

.field private final T:Landroid/os/PowerManager$WakeLock;

.field private U:Ljava/lang/String;

.field private V:Ljava/lang/String;

.field private final f:Lcom/google/android/gms/cast/e/h;

.field private g:Ljava/lang/Integer;

.field private final h:Landroid/content/Context;

.field private final i:Landroid/os/Handler;

.field private final j:Lcom/google/android/gms/cast/CastDevice;

.field private final k:Lcom/google/android/gms/cast/b/m;

.field private final l:Lcom/google/android/gms/cast/b/o;

.field private final n:Lcom/google/android/gms/cast/b/l;

.field private final o:Lcom/google/android/gms/cast/b/af;

.field private final p:Lcom/google/android/gms/cast/b/z;

.field private q:Lcom/google/android/gms/cast/b/aa;

.field private r:Lcom/google/android/gms/cast/b/ae;

.field private s:Lcom/google/android/gms/cast/b/ab;

.field private final t:Ljava/util/Set;

.field private final u:Ljava/util/Map;

.field private final v:Ljava/lang/String;

.field private x:Lcom/google/android/gms/cast/ApplicationMetadata;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 49
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/cast/b/e;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 51
    sget-object v0, Lcom/google/android/gms/cast/a/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/cast/b/e;->b:Ljava/lang/String;

    .line 52
    sget-object v0, Lcom/google/android/gms/cast/a/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/cast/b/e;->c:Z

    .line 54
    sget-object v0, Lcom/google/android/gms/cast/a/b;->n:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/cast/b/e;->d:Z

    .line 56
    sget-object v0, Lcom/google/android/gms/cast/a/b;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/cast/b/e;->e:J

    .line 66
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lcom/google/android/gms/cast/b/e;->m:Ljava/util/concurrent/atomic/AtomicLong;

    .line 76
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lcom/google/android/gms/cast/b/e;->w:Ljava/util/concurrent/atomic/AtomicLong;

    .line 107
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/cast/b/e;->W:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;ILjava/lang/String;JLcom/google/android/gms/cast/b/m;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    const-wide/16 v2, 0x2710

    iput-wide v2, p0, Lcom/google/android/gms/cast/b/e;->F:J

    .line 170
    new-instance v2, Lcom/google/android/gms/cast/e/h;

    const-string v3, "CastDeviceController"

    invoke-direct {v2, v3}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    .line 171
    sget-object v2, Lcom/google/android/gms/cast/b/e;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v2

    .line 172
    invoke-static/range {p10 .. p10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 173
    const-string v3, "controller-%04d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/cast/b/e;->U:Ljava/lang/String;

    .line 178
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    iget-object v3, p0, Lcom/google/android/gms/cast/b/e;->U:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;)V

    .line 179
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/cast/b/e;->g:Ljava/lang/Integer;

    .line 180
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/gms/cast/b/e;->J:Z

    .line 181
    iput-object p4, p0, Lcom/google/android/gms/cast/b/e;->V:Ljava/lang/String;

    .line 182
    iput-object p1, p0, Lcom/google/android/gms/cast/b/e;->h:Landroid/content/Context;

    .line 183
    iput-object p2, p0, Lcom/google/android/gms/cast/b/e;->i:Landroid/os/Handler;

    .line 184
    iput-object p3, p0, Lcom/google/android/gms/cast/b/e;->j:Lcom/google/android/gms/cast/CastDevice;

    .line 185
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    .line 186
    invoke-static {p5}, Lcom/google/android/gms/common/util/x;->b(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/gms/cast/b/e;->M:Z

    .line 187
    move-wide/from16 v0, p7

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/cast/b/e;->a(J)V

    .line 188
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/cast/b/e;->u:Ljava/util/Map;

    .line 189
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/cast/b/e;->t:Ljava/util/Set;

    .line 190
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/gms/cast/b/e;->K:I

    .line 191
    new-instance v2, Lcom/google/android/gms/cast/b/aj;

    invoke-direct {v2}, Lcom/google/android/gms/cast/b/aj;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/cast/b/e;->P:Lcom/google/android/gms/cast/b/aj;

    .line 192
    iget-object v2, p0, Lcom/google/android/gms/cast/b/e;->i:Landroid/os/Handler;

    invoke-static {p1, v2}, Lcom/google/android/gms/cast/d/a;->a(Landroid/content/Context;Landroid/os/Handler;)Lcom/google/android/gms/cast/d/a;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/cast/b/e;->N:Lcom/google/android/gms/cast/d/a;

    .line 193
    new-instance v2, Lcom/google/android/gms/cast/b/f;

    invoke-direct {v2, p0}, Lcom/google/android/gms/cast/b/f;-><init>(Lcom/google/android/gms/cast/b/e;)V

    iput-object v2, p0, Lcom/google/android/gms/cast/b/e;->O:Lcom/google/android/gms/cast/d/d;

    .line 230
    const-string v2, "power"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 231
    const/4 v3, 0x1

    const-string v4, "CastDeviceController"

    invoke-virtual {v2, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/cast/b/e;->T:Landroid/os/PowerManager$WakeLock;

    .line 233
    new-instance v2, Lcom/google/android/gms/cast/b/o;

    iget-object v3, p0, Lcom/google/android/gms/cast/b/e;->i:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/gms/cast/b/e;->U:Ljava/lang/String;

    invoke-direct {v2, p1, p0, v3, v4}, Lcom/google/android/gms/cast/b/o;-><init>(Landroid/content/Context;Lcom/google/android/gms/cast/b/t;Landroid/os/Handler;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/gms/cast/b/e;->l:Lcom/google/android/gms/cast/b/o;

    .line 234
    new-instance v2, Lcom/google/android/gms/cast/b/l;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/cast/b/l;-><init>(Lcom/google/android/gms/cast/b/e;B)V

    iput-object v2, p0, Lcom/google/android/gms/cast/b/e;->n:Lcom/google/android/gms/cast/b/l;

    .line 236
    new-instance v2, Lcom/google/android/gms/cast/b/g;

    const-string v3, "receiver-0"

    iget-object v4, p0, Lcom/google/android/gms/cast/b/e;->U:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/gms/cast/b/e;->M:Z

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/google/android/gms/cast/b/g;-><init>(Lcom/google/android/gms/cast/b/e;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v2, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    .line 294
    iget-object v2, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    invoke-virtual {p0, v2}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/internal/b;)V

    .line 296
    new-instance v2, Lcom/google/android/gms/cast/b/z;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/cast/b/e;->U:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/cast/b/e;->j:Lcom/google/android/gms/cast/CastDevice;

    invoke-direct {v2, v3, p6, v4, v5}, Lcom/google/android/gms/cast/b/z;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/cast/CastDevice;)V

    iput-object v2, p0, Lcom/google/android/gms/cast/b/e;->p:Lcom/google/android/gms/cast/b/z;

    .line 298
    iget-object v2, p0, Lcom/google/android/gms/cast/b/e;->p:Lcom/google/android/gms/cast/b/z;

    invoke-virtual {p0, v2}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/internal/b;)V

    .line 301
    const-string v2, "%s-%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p6, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcom/google/android/gms/cast/b/e;->w:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/cast/b/e;->v:Ljava/lang/String;

    .line 303
    new-instance v2, Lcom/google/android/gms/cast/b/h;

    invoke-direct {v2, p0}, Lcom/google/android/gms/cast/b/h;-><init>(Lcom/google/android/gms/cast/b/e;)V

    iput-object v2, p0, Lcom/google/android/gms/cast/b/e;->G:Ljava/lang/Runnable;

    .line 328
    new-instance v2, Lcom/google/android/gms/cast/b/i;

    invoke-direct {v2, p0}, Lcom/google/android/gms/cast/b/i;-><init>(Lcom/google/android/gms/cast/b/e;)V

    iput-object v2, p0, Lcom/google/android/gms/cast/b/e;->Q:Ljava/lang/Runnable;

    .line 337
    return-void

    .line 175
    :cond_0
    const-string v3, "controller-%04d %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x1

    aput-object p10, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/cast/b/e;->U:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private static a(Lcom/google/android/gms/cast/b/b;)Lcom/google/android/gms/cast/ApplicationMetadata;
    .locals 4

    .prologue
    .line 591
    invoke-virtual {p0}, Lcom/google/android/gms/cast/b/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/cast/ApplicationMetadata;->a(Ljava/lang/String;)Lcom/google/android/gms/cast/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/cast/b/b;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/gms/cast/a;->a:Lcom/google/android/gms/cast/ApplicationMetadata;

    invoke-static {v2, v0}, Lcom/google/android/gms/cast/ApplicationMetadata;->a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/cast/b/b;->g()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/WebImage;

    iget-object v3, v1, Lcom/google/android/gms/cast/a;->a:Lcom/google/android/gms/cast/ApplicationMetadata;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/cast/ApplicationMetadata;->a(Lcom/google/android/gms/common/images/WebImage;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/cast/b/b;->f()Ljava/util/List;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/gms/cast/a;->a:Lcom/google/android/gms/cast/ApplicationMetadata;

    invoke-static {v2, v0}, Lcom/google/android/gms/cast/ApplicationMetadata;->a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/util/List;)V

    .line 598
    invoke-virtual {p0}, Lcom/google/android/gms/cast/b/b;->d()Lcom/google/android/gms/cast/b/ak;

    move-result-object v0

    .line 600
    if-eqz v0, :cond_1

    .line 601
    iget-object v2, v0, Lcom/google/android/gms/cast/b/ak;->b:Ljava/lang/String;

    iget-object v3, v1, Lcom/google/android/gms/cast/a;->a:Lcom/google/android/gms/cast/ApplicationMetadata;

    invoke-static {v3, v2}, Lcom/google/android/gms/cast/ApplicationMetadata;->b(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/cast/b/ak;->c:Landroid/net/Uri;

    iget-object v2, v1, Lcom/google/android/gms/cast/a;->a:Lcom/google/android/gms/cast/ApplicationMetadata;

    invoke-static {v2, v0}, Lcom/google/android/gms/cast/ApplicationMetadata;->a(Lcom/google/android/gms/cast/ApplicationMetadata;Landroid/net/Uri;)V

    .line 605
    :cond_1
    iget-object v0, v1, Lcom/google/android/gms/cast/a;->a:Lcom/google/android/gms/cast/ApplicationMetadata;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/gms/cast/CastDevice;ILjava/lang/String;JLcom/google/android/gms/cast/b/m;Ljava/lang/String;)Lcom/google/android/gms/cast/b/e;
    .locals 13

    .prologue
    .line 143
    const/4 v5, 0x0

    .line 144
    sget-object v2, Lcom/google/android/gms/cast/b/e;->W:Ljava/util/HashMap;

    monitor-enter v2

    .line 145
    :try_start_0
    sget-object v0, Lcom/google/android/gms/cast/b/e;->W:Ljava/util/HashMap;

    invoke-virtual {p2}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 146
    if-eqz v0, :cond_0

    .line 147
    sget-boolean v1, Lcom/google/android/gms/cast/b/e;->d:Z

    if-eqz v1, :cond_1

    .line 148
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long/2addr v6, v8

    sget-wide v8, Lcom/google/android/gms/cast/b/e;->e:J

    cmp-long v1, v6, v8

    if-ltz v1, :cond_1

    .line 149
    sget-object v0, Lcom/google/android/gms/cast/b/e;->W:Ljava/util/HashMap;

    invoke-virtual {p2}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    :cond_0
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    new-instance v1, Lcom/google/android/gms/cast/b/e;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move/from16 v6, p3

    move-object/from16 v7, p4

    move-wide/from16 v8, p5

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    invoke-direct/range {v1 .. v11}, Lcom/google/android/gms/cast/b/e;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;ILjava/lang/String;JLcom/google/android/gms/cast/b/m;Ljava/lang/String;)V

    .line 163
    invoke-direct {v1}, Lcom/google/android/gms/cast/b/e;->s()V

    .line 164
    return-object v1

    .line 154
    :cond_1
    :try_start_1
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v5, v0

    goto :goto_0

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/e/h;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/cast/b/e;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/gms/cast/b/e;->V:Ljava/lang/String;

    return-object p1
.end method

.method private a(IZ)V
    .locals 13

    .prologue
    const-wide/16 v0, -0x1

    const/4 v12, 0x0

    const-wide/16 v2, 0x0

    .line 408
    iput-boolean v12, p0, Lcom/google/android/gms/cast/b/e;->H:Z

    .line 410
    iget-object v4, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v5, "handleConnectionFailure; statusCode=%d, mOriginalDisconnectStatusCode=%d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v12

    const/4 v7, 0x1

    iget v8, p0, Lcom/google/android/gms/cast/b/e;->K:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 413
    if-eqz p2, :cond_4

    .line 414
    iget-object v4, p0, Lcom/google/android/gms/cast/b/e;->P:Lcom/google/android/gms/cast/b/aj;

    iget-wide v6, v4, Lcom/google/android/gms/cast/b/aj;->d:J

    cmp-long v5, v6, v2

    if-nez v5, :cond_1

    .line 415
    :cond_0
    :goto_0
    cmp-long v2, v0, v2

    if-ltz v2, :cond_5

    .line 416
    iget-object v2, p0, Lcom/google/android/gms/cast/b/e;->i:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/gms/cast/b/e;->Q:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 430
    :goto_1
    return-void

    .line 414
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-wide v8, v4, Lcom/google/android/gms/cast/b/aj;->c:J

    cmp-long v5, v8, v2

    if-eqz v5, :cond_3

    iget-wide v8, v4, Lcom/google/android/gms/cast/b/aj;->d:J

    sub-long v8, v6, v8

    iget-wide v10, v4, Lcom/google/android/gms/cast/b/aj;->b:J

    cmp-long v5, v8, v10

    if-ltz v5, :cond_2

    iput-wide v2, v4, Lcom/google/android/gms/cast/b/aj;->d:J

    goto :goto_0

    :cond_2
    iget-wide v0, v4, Lcom/google/android/gms/cast/b/aj;->a:J

    iget-wide v4, v4, Lcom/google/android/gms/cast/b/aj;->c:J

    sub-long v4, v6, v4

    sub-long/2addr v0, v4

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    :cond_3
    move-wide v0, v2

    goto :goto_0

    .line 420
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->P:Lcom/google/android/gms/cast/b/aj;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/aj;->a()Z

    .line 425
    :cond_5
    iget v0, p0, Lcom/google/android/gms/cast/b/e;->K:I

    if-eqz v0, :cond_6

    .line 426
    iget p1, p0, Lcom/google/android/gms/cast/b/e;->K:I

    .line 427
    iput v12, p0, Lcom/google/android/gms/cast/b/e;->K:I

    .line 429
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    invoke-interface {v0, p1}, Lcom/google/android/gms/cast/b/m;->a(I)V

    goto :goto_1
.end method

.method private a(Lcom/google/android/gms/cast/b/b;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 534
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "connectToApplicationAndNotify"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 536
    invoke-virtual {p1}, Lcom/google/android/gms/cast/b/b;->c()Ljava/lang/String;

    move-result-object v0

    .line 538
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->p:Lcom/google/android/gms/cast/b/z;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/b/z;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 545
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v2, "setting current transport ID to %s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 547
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/b/af;->c(Ljava/lang/String;)V

    .line 549
    invoke-virtual {p1}, Lcom/google/android/gms/cast/b/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/e;->z:Ljava/lang/String;

    .line 550
    invoke-static {p1}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/b/b;)Lcom/google/android/gms/cast/ApplicationMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/e;->x:Lcom/google/android/gms/cast/ApplicationMetadata;

    .line 551
    invoke-virtual {p1}, Lcom/google/android/gms/cast/b/b;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/e;->y:Ljava/lang/String;

    .line 552
    invoke-virtual {p1}, Lcom/google/android/gms/cast/b/b;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/e;->R:Ljava/lang/String;

    .line 553
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->y:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/cast/b/e;->S:Ljava/lang/String;

    .line 556
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->P:Lcom/google/android/gms/cast/b/aj;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/aj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 557
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->Q:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 558
    invoke-direct {p0, v5}, Lcom/google/android/gms/cast/b/e;->e(Z)V

    .line 563
    :goto_0
    return-void

    .line 540
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/b/af;->c(Ljava/lang/String;)V

    .line 541
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/b/e;->h(I)V

    goto :goto_0

    .line 560
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->x:Lcom/google/android/gms/cast/ApplicationMetadata;

    iget-object v2, p0, Lcom/google/android/gms/cast/b/e;->z:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/cast/b/e;->y:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, p2}, Lcom/google/android/gms/cast/b/m;->a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/cast/b/e;I)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/gms/cast/b/e;->e(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast/b/e;IZ)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/cast/b/e;->a(IZ)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast/b/e;Lcom/google/android/gms/cast/b/b;)V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/b/b;Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast/b/e;Lcom/google/android/gms/cast/b/b;ZLcom/google/android/gms/cast/internal/DeviceStatus;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/16 v2, 0x7d5

    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v3, "processReceiverStatus: applicationInfo=%s"

    new-array v4, v8, [Ljava/lang/Object;

    aput-object p1, v4, v7

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/cast/b/b;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/cast/b/e;->z:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/e;->I:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/e;->M:Z

    if-eqz v0, :cond_4

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/b/b;)Lcom/google/android/gms/cast/ApplicationMetadata;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/google/android/gms/cast/internal/DeviceStatus;->a(Lcom/google/android/gms/cast/ApplicationMetadata;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    invoke-interface {v0, p3}, Lcom/google/android/gms/cast/b/m;->a(Lcom/google/android/gms/cast/internal/DeviceStatus;)V

    new-instance v0, Lcom/google/android/gms/cast/internal/ApplicationStatus;

    invoke-direct {v0}, Lcom/google/android/gms/cast/internal/ApplicationStatus;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/cast/b/e;->z:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/cast/internal/ApplicationStatus;->a(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    invoke-interface {v3, v0}, Lcom/google/android/gms/cast/b/m;->a(Lcom/google/android/gms/cast/internal/ApplicationStatus;)V

    :cond_1
    :goto_1
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->D:Ljava/lang/String;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/google/android/gms/cast/b/b;->a()Ljava/lang/String;

    move-result-object v0

    :goto_2
    iget-object v3, p0, Lcom/google/android/gms/cast/b/e;->D:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/google/android/gms/cast/b/e;->D:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v3, "application to join (%s) is NOT available!"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/gms/cast/b/e;->D:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->D:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/cast/b/e;->D:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/cast/b/e;->E:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/cast/b/e;->R:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/cast/b/e;->S:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/gms/cast/b/e;->B:Z

    if-eqz v3, :cond_6

    iput-boolean v7, p0, Lcom/google/android/gms/cast/b/e;->B:Z

    iget-object v2, p0, Lcom/google/android/gms/cast/b/e;->C:Lcom/google/android/gms/cast/LaunchOptions;

    iput-object v1, p0, Lcom/google/android/gms/cast/b/e;->C:Lcom/google/android/gms/cast/LaunchOptions;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/cast/b/e;->b(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    :cond_2
    :goto_3
    return-void

    :cond_3
    move-object v0, v1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    iget-object v3, p0, Lcom/google/android/gms/cast/b/e;->z:Ljava/lang/String;

    invoke-virtual {p3}, Lcom/google/android/gms/cast/internal/DeviceStatus;->b()D

    move-result-wide v4

    invoke-virtual {p3}, Lcom/google/android/gms/cast/internal/DeviceStatus;->c()Z

    move-result v6

    invoke-interface {v0, v3, v4, v5, v6}, Lcom/google/android/gms/cast/b/m;->a(Ljava/lang/String;DZ)V

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->P:Lcom/google/android/gms/cast/b/aj;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/aj;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-direct {p0, v7}, Lcom/google/android/gms/cast/b/e;->e(Z)V

    goto :goto_3

    :cond_7
    invoke-direct {p0, v2}, Lcom/google/android/gms/cast/b/e;->i(I)V

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->E:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->E:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/b/b;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->D:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/cast/b/e;->D:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/cast/b/e;->E:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/gms/cast/b/e;->B:Z

    if-eqz v3, :cond_9

    iput-boolean v7, p0, Lcom/google/android/gms/cast/b/e;->B:Z

    iget-object v2, p0, Lcom/google/android/gms/cast/b/e;->C:Lcom/google/android/gms/cast/LaunchOptions;

    iput-object v1, p0, Lcom/google/android/gms/cast/b/e;->C:Lcom/google/android/gms/cast/LaunchOptions;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/cast/b/e;->b(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    goto :goto_3

    :cond_9
    invoke-direct {p0, v2}, Lcom/google/android/gms/cast/b/e;->i(I)V

    goto :goto_3

    :cond_a
    if-eqz p1, :cond_b

    invoke-virtual {p1}, Lcom/google/android/gms/cast/b/b;->e()Ljava/lang/String;

    move-result-object v0

    :goto_4
    iput-object v0, p0, Lcom/google/android/gms/cast/b/e;->z:Ljava/lang/String;

    if-eqz p1, :cond_c

    invoke-virtual {p1}, Lcom/google/android/gms/cast/b/b;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    invoke-direct {p0, p1, v7}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/b/b;Z)V

    :goto_5
    iput-object v1, p0, Lcom/google/android/gms/cast/b/e;->D:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/cast/b/e;->E:Ljava/lang/String;

    iput-boolean v7, p0, Lcom/google/android/gms/cast/b/e;->B:Z

    iput-object v1, p0, Lcom/google/android/gms/cast/b/e;->C:Lcom/google/android/gms/cast/LaunchOptions;

    goto :goto_3

    :cond_b
    move-object v0, v1

    goto :goto_4

    :cond_c
    iget-object v3, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    const-string v0, ""

    iget-object v4, p0, Lcom/google/android/gms/cast/b/e;->D:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    move v0, v2

    :goto_6
    invoke-interface {v3, v0}, Lcom/google/android/gms/cast/b/m;->c(I)V

    goto :goto_5

    :cond_d
    const/16 v0, 0x7d4

    goto :goto_6
.end method

.method private a(Lcom/google/g/a/g;Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 734
    invoke-virtual {p1}, Lcom/google/g/a/g;->b()I

    move-result v0

    const/high16 v1, 0x10000

    if-le v0, v1, :cond_0

    .line 735
    new-instance v0, Lcom/google/android/gms/cast/b/ac;

    invoke-direct {v0}, Lcom/google/android/gms/cast/b/ac;-><init>()V

    throw v0

    .line 739
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->l:Lcom/google/android/gms/cast/b/o;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/cast/b/o;->a(Lcom/google/g/a/g;)V

    .line 740
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    invoke-interface {v0, p2, p3, p4}, Lcom/google/android/gms/cast/b/m;->a(Ljava/lang/String;J)V
    :try_end_0
    .catch Lcom/google/android/gms/cast/b/c; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/cast/b/ac; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    .line 749
    :goto_0
    return-void

    .line 742
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    const/16 v1, 0x7d7

    invoke-interface {v0, p2, p3, p4, v1}, Lcom/google/android/gms/cast/b/m;->a(Ljava/lang/String;JI)V

    goto :goto_0

    .line 745
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    const/16 v1, 0x7d6

    invoke-interface {v0, p2, p3, p4, v1}, Lcom/google/android/gms/cast/b/m;->a(Ljava/lang/String;JI)V

    goto :goto_0

    .line 747
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    const/4 v1, 0x7

    invoke-interface {v0, p2, p3, p4, v1}, Lcom/google/android/gms/cast/b/m;->a(Ljava/lang/String;JI)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/CastDevice;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->j:Lcom/google/android/gms/cast/CastDevice;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/cast/b/e;I)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/gms/cast/b/e;->i(I)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/cast/b/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->V:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/cast/b/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->y:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/ApplicationMetadata;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->x:Lcom/google/android/gms/cast/ApplicationMetadata;

    return-object v0
.end method

.method private e(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 569
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    iget-object v0, v0, Lcom/google/android/gms/cast/b/af;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 571
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->p:Lcom/google/android/gms/cast/b/z;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    iget-object v1, v1, Lcom/google/android/gms/cast/b/af;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/b/z;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 576
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/cast/b/af;->c(Ljava/lang/String;)V

    .line 579
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->y:Ljava/lang/String;

    .line 580
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->x:Lcom/google/android/gms/cast/ApplicationMetadata;

    if-eqz v1, :cond_1

    .line 581
    iput-object v4, p0, Lcom/google/android/gms/cast/b/e;->x:Lcom/google/android/gms/cast/ApplicationMetadata;

    .line 582
    iput-object v4, p0, Lcom/google/android/gms/cast/b/e;->y:Ljava/lang/String;

    .line 587
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    invoke-interface {v1, p1, v0}, Lcom/google/android/gms/cast/b/m;->a(ILjava/lang/String;)V

    .line 588
    return-void

    .line 572
    :catch_0
    move-exception v0

    .line 573
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Error while leaving application"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 574
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/b/e;->h(I)V

    goto :goto_0
.end method

.method private e(Z)V
    .locals 5

    .prologue
    .line 1386
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "calling Listener.onConnected(%b)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1387
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    invoke-interface {v0, p1}, Lcom/google/android/gms/cast/b/m;->a(Z)V

    .line 1388
    return-void
.end method

.method private f(I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 1139
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "finishDisconnecting; socketError=%d, mDisconnectStatusCode=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    iget v4, p0, Lcom/google/android/gms/cast/b/e;->K:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1141
    iput-boolean v5, p0, Lcom/google/android/gms/cast/b/e;->H:Z

    .line 1142
    iput-boolean v5, p0, Lcom/google/android/gms/cast/b/e;->I:Z

    .line 1144
    iput-object v6, p0, Lcom/google/android/gms/cast/b/e;->x:Lcom/google/android/gms/cast/ApplicationMetadata;

    .line 1145
    iput-object v6, p0, Lcom/google/android/gms/cast/b/e;->y:Ljava/lang/String;

    .line 1146
    iput-object v6, p0, Lcom/google/android/gms/cast/b/e;->z:Ljava/lang/String;

    .line 1149
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->G:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1152
    iget v0, p0, Lcom/google/android/gms/cast/b/e;->K:I

    if-eqz v0, :cond_0

    .line 1153
    iget v0, p0, Lcom/google/android/gms/cast/b/e;->K:I

    .line 1154
    iput v5, p0, Lcom/google/android/gms/cast/b/e;->K:I

    .line 1159
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->P:Lcom/google/android/gms/cast/b/aj;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/b/aj;->a()Z

    .line 1161
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    invoke-interface {v1, v0}, Lcom/google/android/gms/cast/b/m;->b(I)V

    .line 1162
    return-void

    .line 1156
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/cast/b/e;->g(I)I

    move-result v0

    goto :goto_0
.end method

.method static synthetic f(Lcom/google/android/gms/cast/b/e;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/e;->A:Z

    return v0
.end method

.method private static g(I)I
    .locals 1

    .prologue
    .line 1166
    if-nez p0, :cond_0

    .line 1167
    const/4 v0, 0x0

    .line 1176
    :goto_0
    return v0

    .line 1169
    :cond_0
    const/4 v0, 0x3

    if-ne p0, v0, :cond_1

    .line 1170
    const/16 v0, 0xf

    goto :goto_0

    .line 1172
    :cond_1
    const/4 v0, 0x4

    if-eq p0, v0, :cond_2

    const/4 v0, 0x5

    if-ne p0, v0, :cond_3

    .line 1174
    :cond_2
    const/16 v0, 0x7d0

    goto :goto_0

    .line 1176
    :cond_3
    const/4 v0, 0x7

    goto :goto_0
.end method

.method static synthetic g(Lcom/google/android/gms/cast/b/e;)Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/cast/b/e;->A:Z

    return v0
.end method

.method static synthetic h(Lcom/google/android/gms/cast/b/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/e;->R:Ljava/lang/String;

    return-object v0
.end method

.method private h(I)V
    .locals 1

    .prologue
    .line 1246
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->l:Lcom/google/android/gms/cast/b/o;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/o;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1247
    iput p1, p0, Lcom/google/android/gms/cast/b/e;->K:I

    .line 1248
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->l:Lcom/google/android/gms/cast/b/o;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/o;->c()V

    .line 1252
    :goto_0
    return-void

    .line 1250
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/cast/b/e;->f(I)V

    goto :goto_0
.end method

.method static synthetic i(Lcom/google/android/gms/cast/b/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/e;->S:Ljava/lang/String;

    return-object v0
.end method

.method private i(I)V
    .locals 5

    .prologue
    .line 1381
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "calling Listener.onApplicationConnectionFailed(%d)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1382
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    invoke-interface {v0, p1}, Lcom/google/android/gms/cast/b/m;->c(I)V

    .line 1383
    return-void
.end method

.method static synthetic j(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/b/m;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/gms/cast/b/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->D:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/b/aj;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->P:Lcom/google/android/gms/cast/b/aj;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/gms/cast/b/e;)V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/b/e;->e(Z)V

    return-void
.end method

.method static synthetic n(Lcom/google/android/gms/cast/b/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/e;->D:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/gms/cast/b/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/e;->E:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p(Lcom/google/android/gms/cast/b/e;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/e;->I:Z

    return v0
.end method

.method static synthetic q(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/b/ab;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->s:Lcom/google/android/gms/cast/b/ab;

    return-object v0
.end method

.method static synthetic q()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/google/android/gms/cast/b/e;->W:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic r()Ljava/util/concurrent/atomic/AtomicLong;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/google/android/gms/cast/b/e;->m:Ljava/util/concurrent/atomic/AtomicLong;

    return-object v0
.end method

.method static synthetic r(Lcom/google/android/gms/cast/b/e;)V
    .locals 1

    .prologue
    .line 42
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/b/e;->h(I)V

    return-void
.end method

.method static synthetic s(Lcom/google/android/gms/cast/b/e;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->G:Ljava/lang/Runnable;

    return-object v0
.end method

.method private declared-synchronized s()V
    .locals 1

    .prologue
    .line 1330
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/b/e;->c(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1331
    monitor-exit p0

    return-void

    .line 1330
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic t(Lcom/google/android/gms/cast/b/e;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->i:Landroid/os/Handler;

    return-object v0
.end method

.method private declared-synchronized t()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1370
    monitor-enter p0

    .line 1371
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->T:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1372
    add-int/lit8 v0, v0, 0x1

    .line 1373
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->T:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1370
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1375
    :cond_0
    if-lez v0, :cond_1

    .line 1376
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Unbalanced call in releasing the wake lock. Released %d locks."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1378
    :cond_1
    monitor-exit p0

    return-void
.end method

.method static synthetic u(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/b/af;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    return-object v0
.end method

.method static synthetic v(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/b/aa;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->q:Lcom/google/android/gms/cast/b/aa;

    return-object v0
.end method

.method static synthetic w(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/b/aa;
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/e;->q:Lcom/google/android/gms/cast/b/aa;

    return-object v0
.end method

.method static synthetic x(Lcom/google/android/gms/cast/b/e;)V
    .locals 7

    .prologue
    const/4 v6, 0x7

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "finishConnecting"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->p:Lcom/google/android/gms/cast/b/z;

    const-string v1, "receiver-0"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/b/z;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    sget-boolean v0, Lcom/google/android/gms/cast/b/e;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->j:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/CastDevice;->l()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/cast/b/k;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->U:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/cast/b/k;-><init>(Lcom/google/android/gms/cast/b/e;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/b/e;->r:Lcom/google/android/gms/cast/b/ae;

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->r:Lcom/google/android/gms/cast/b/ae;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/internal/b;)V

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->r:Lcom/google/android/gms/cast/b/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/ae;->a()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    :goto_0
    new-instance v0, Lcom/google/android/gms/cast/b/ab;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->U:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/b/ab;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/b/e;->s:Lcom/google/android/gms/cast/b/ab;

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->s:Lcom/google/android/gms/cast/b/ab;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/internal/b;)V

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->G:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iput-boolean v5, p0, Lcom/google/android/gms/cast/b/e;->I:Z

    iput-boolean v4, p0, Lcom/google/android/gms/cast/b/e;->H:Z

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->R:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->S:Ljava/lang/String;

    if-eqz v0, :cond_1

    iput-boolean v4, p0, Lcom/google/android/gms/cast/b/e;->B:Z

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->R:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->S:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/cast/b/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, v6}, Lcom/google/android/gms/cast/b/e;->h(I)V

    goto :goto_1

    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Failed to get proximity info"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->P:Lcom/google/android/gms/cast/b/aj;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/aj;->a()Z

    invoke-direct {p0, v5}, Lcom/google/android/gms/cast/b/e;->e(Z)V

    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/af;->b()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-direct {p0, v6}, Lcom/google/android/gms/cast/b/e;->h(I)V

    goto :goto_1
.end method

.method static synthetic y(Lcom/google/android/gms/cast/b/e;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->h:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/cast/CastDevice;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->j:Lcom/google/android/gms/cast/CastDevice;

    return-object v0
.end method

.method public final a(DDZ)V
    .locals 7

    .prologue
    .line 880
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->h:Landroid/content/Context;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;DDZ)V

    .line 881
    return-void
.end method

.method public final a(I)V
    .locals 5

    .prologue
    .line 1094
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onConnectionFailed; socketError=%d, disposed:%b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-boolean v4, p0, Lcom/google/android/gms/cast/b/e;->J:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1095
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->h:Landroid/content/Context;

    invoke-static {v0, p0, p1}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;I)V

    .line 1096
    return-void
.end method

.method public final a(J)V
    .locals 5

    .prologue
    .line 458
    iget-wide v0, p0, Lcom/google/android/gms/cast/b/e;->L:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    .line 466
    :goto_0
    return-void

    .line 462
    :cond_0
    iput-wide p1, p0, Lcom/google/android/gms/cast/b/e;->L:J

    .line 463
    iget-wide v0, p0, Lcom/google/android/gms/cast/b/e;->L:J

    const-wide/16 v2, 0x1

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 464
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/e/h;->a(Z)V

    .line 465
    invoke-static {v0}, Lcom/google/android/gms/cast/internal/k;->b(Z)V

    goto :goto_0

    .line 463
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/cast/internal/b;)V
    .locals 2

    .prologue
    .line 520
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->n:Lcom/google/android/gms/cast/b/l;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/internal/b;->a(Lcom/google/android/gms/cast/internal/n;)V

    .line 521
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->u:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/gms/cast/internal/b;->n:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 522
    return-void
.end method

.method public final a(Lcom/google/g/a/g;)V
    .locals 1

    .prologue
    .line 1182
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->h:Landroid/content/Context;

    invoke-static {v0, p0, p1}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;Lcom/google/g/a/g;)V

    .line 1183
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 849
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "stopApplication"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 850
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->h:Landroid/content/Context;

    invoke-static {v0, p0, p1}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;Ljava/lang/String;)V

    .line 851
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V
    .locals 1

    .prologue
    .line 752
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->h:Landroid/content/Context;

    invoke-static {v0, p0, p1, p2}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    .line 753
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "reconnectToDevice: lastApplicationId=%s, lastSessionId=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 365
    iput-object p1, p0, Lcom/google/android/gms/cast/b/e;->R:Ljava/lang/String;

    .line 366
    iput-object p2, p0, Lcom/google/android/gms/cast/b/e;->S:Ljava/lang/String;

    .line 367
    invoke-virtual {p0}, Lcom/google/android/gms/cast/b/e;->c()V

    .line 368
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7

    .prologue
    .line 908
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->h:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    iget-object v6, v1, Lcom/google/android/gms/cast/b/af;->b:Ljava/lang/String;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 910
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 915
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 916
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "ignoring attempt to send a text message with no destination ID"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 917
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    const/16 v1, 0x7d1

    invoke-interface {v0, p1, p3, p4, v1}, Lcom/google/android/gms/cast/b/m;->a(Ljava/lang/String;JI)V

    .line 927
    :goto_0
    return-void

    .line 922
    :cond_0
    if-nez p5, :cond_1

    :try_start_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The application has not launched yet."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 923
    :catch_0
    move-exception v0

    .line 924
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Error while sending message"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 925
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/b/e;->h(I)V

    goto :goto_0

    .line 922
    :cond_1
    :try_start_1
    new-instance v0, Lcom/google/g/a/g;

    invoke-direct {v0}, Lcom/google/g/a/g;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/g/a/g;->a(I)Lcom/google/g/a/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/g/a/g;->a(Ljava/lang/String;)Lcom/google/g/a/g;

    move-result-object v0

    invoke-virtual {v0, p5}, Lcom/google/g/a/g;->b(Ljava/lang/String;)Lcom/google/g/a/g;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/g/a/g;->c(Ljava/lang/String;)Lcom/google/g/a/g;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/g/a/g;->b(I)Lcom/google/g/a/g;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/g/a/g;->d(Ljava/lang/String;)Lcom/google/g/a/g;

    move-result-object v0

    invoke-direct {p0, v0, p1, p3, p4}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/g/a/g;Ljava/lang/String;J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[BJ)V
    .locals 7

    .prologue
    .line 930
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->h:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    iget-object v6, v1, Lcom/google/android/gms/cast/b/af;->b:Ljava/lang/String;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;Ljava/lang/String;[BJLjava/lang/String;)V

    .line 932
    return-void
.end method

.method public final a(Ljava/lang/String;[BJLjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 937
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 938
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "ignoring attempt to send a binary message with no destination ID"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 939
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    const/16 v1, 0x7d1

    invoke-interface {v0, p1, p3, p4, v1}, Lcom/google/android/gms/cast/b/m;->a(Ljava/lang/String;JI)V

    .line 949
    :goto_0
    return-void

    .line 944
    :cond_0
    if-nez p5, :cond_1

    :try_start_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The application has not launched yet."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 945
    :catch_0
    move-exception v0

    .line 946
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Error while sending message"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 947
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/b/e;->h(I)V

    goto :goto_0

    .line 944
    :cond_1
    :try_start_1
    new-instance v0, Lcom/google/g/a/g;

    invoke-direct {v0}, Lcom/google/g/a/g;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/g/a/g;->a(I)Lcom/google/g/a/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/g/a/g;->a(Ljava/lang/String;)Lcom/google/g/a/g;

    move-result-object v0

    invoke-virtual {v0, p5}, Lcom/google/g/a/g;->b(Ljava/lang/String;)Lcom/google/g/a/g;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/g/a/g;->c(Ljava/lang/String;)Lcom/google/g/a/g;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/g/a/g;->b(I)Lcom/google/g/a/g;

    move-result-object v0

    invoke-static {p2}, Lcom/google/protobuf/a/a;->a([B)Lcom/google/protobuf/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/g/a/g;->a(Lcom/google/protobuf/a/a;)Lcom/google/g/a/g;

    move-result-object v0

    invoke-direct {p0, v0, p1, p3, p4}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/g/a/g;Ljava/lang/String;J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 489
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->h:Landroid/content/Context;

    invoke-static {v0, p0, p1}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;Z)V

    .line 490
    return-void
.end method

.method public final a(ZDZ)V
    .locals 8

    .prologue
    .line 894
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->h:Landroid/content/Context;

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move v6, p4

    invoke-static/range {v1 .. v6}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;ZDZ)V

    .line 895
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/e/h;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(DDZ)V
    .locals 7

    .prologue
    .line 886
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/cast/b/af;->a(DDZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 891
    :goto_0
    return-void

    .line 887
    :catch_0
    move-exception v0

    .line 888
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Error while setting volume"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 889
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/b/e;->h(I)V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1100
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onSocketConnectionFailedInternal: socketError=%d"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1102
    invoke-static {p1}, Lcom/google/android/gms/cast/b/e;->g(I)I

    move-result v0

    invoke-direct {p0, v0, v5}, Lcom/google/android/gms/cast/b/e;->a(IZ)V

    .line 1103
    return-void
.end method

.method public final b(Lcom/google/android/gms/cast/internal/b;)V
    .locals 2

    .prologue
    .line 525
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/internal/b;->a(Lcom/google/android/gms/cast/internal/n;)V

    .line 526
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->u:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/gms/cast/internal/b;->n:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 527
    return-void
.end method

.method public final b(Lcom/google/g/a/g;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1188
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->s:Lcom/google/android/gms/cast/b/ab;

    if-eqz v0, :cond_0

    .line 1189
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->s:Lcom/google/android/gms/cast/b/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/ab;->a()V

    .line 1192
    :cond_0
    iget-object v1, p1, Lcom/google/g/a/g;->a:Ljava/lang/String;

    .line 1193
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1194
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Received a message with an empty or missing namespace"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1243
    :goto_0
    return-void

    .line 1198
    :cond_1
    iget v2, p1, Lcom/google/g/a/g;->b:I

    .line 1200
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v3, "onMessageReceived. namespace: %s payloadType: %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1202
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->u:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/internal/b;

    .line 1203
    if-eqz v0, :cond_2

    .line 1205
    packed-switch v2, :pswitch_data_0

    .line 1217
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Unknown payload type %s; discarding message"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v6

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1207
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v2, "onMessageReceived, Received message: %s"

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/google/g/a/g;->c:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1208
    iget-object v1, p1, Lcom/google/g/a/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/internal/b;->a_(Ljava/lang/String;)V

    goto :goto_0

    .line 1211
    :pswitch_1
    iget-object v1, p1, Lcom/google/g/a/g;->d:Lcom/google/protobuf/a/a;

    .line 1212
    invoke-virtual {v1}, Lcom/google/protobuf/a/a;->a()I

    move-result v2

    new-array v2, v2, [B

    .line 1213
    invoke-virtual {v1, v2}, Lcom/google/protobuf/a/a;->b([B)V

    .line 1214
    invoke-virtual {v0, v2}, Lcom/google/android/gms/cast/internal/b;->a([B)V

    goto :goto_0

    .line 1222
    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/cast/b/e;->t:Ljava/util/Set;

    monitor-enter v3

    .line 1223
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->t:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 1224
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1225
    if-eqz v0, :cond_3

    .line 1226
    packed-switch v2, :pswitch_data_1

    .line 1237
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Unknown payload type %s; discarding message"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v6

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1224
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 1228
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    iget-object v2, p1, Lcom/google/g/a/g;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/cast/b/m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1231
    :pswitch_3
    iget-object v0, p1, Lcom/google/g/a/g;->d:Lcom/google/protobuf/a/a;

    .line 1232
    invoke-virtual {v0}, Lcom/google/protobuf/a/a;->a()I

    move-result v2

    new-array v2, v2, [B

    .line 1233
    invoke-virtual {v0, v2}, Lcom/google/protobuf/a/a;->b([B)V

    .line 1234
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/cast/b/m;->a(Ljava/lang/String;[B)V

    goto/16 :goto_0

    .line 1240
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Ignoring message. Namespace \'%s\' has not been registered."

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1205
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 1226
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 855
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "stopApplicationInternal() sessionId=%s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 857
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/cast/b/e;->A:Z

    .line 858
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/cast/b/af;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 863
    :goto_0
    return-void

    .line 859
    :catch_0
    move-exception v0

    .line 860
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Error while stopping application"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 861
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/b/e;->h(I)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V
    .locals 6

    .prologue
    const/16 v5, 0x7d4

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 757
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "launchApplicationInternal() id=%s, options=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v4

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 759
    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 760
    :cond_0
    invoke-direct {p0, v5}, Lcom/google/android/gms/cast/b/e;->i(I)V

    .line 786
    :goto_0
    return-void

    .line 764
    :cond_1
    sget-object v0, Lcom/google/android/gms/cast/b/e;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/android/gms/cast/e/i;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 765
    invoke-direct {p0, v5}, Lcom/google/android/gms/cast/b/e;->i(I)V

    goto :goto_0

    .line 769
    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/google/android/gms/cast/LaunchOptions;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 772
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    invoke-virtual {p2}, Lcom/google/android/gms/cast/LaunchOptions;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/cast/b/af;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 774
    :catch_0
    move-exception v0

    .line 775
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Error while launching application"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 776
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/b/e;->h(I)V

    goto :goto_0

    .line 780
    :cond_3
    iput-boolean v3, p0, Lcom/google/android/gms/cast/b/e;->B:Z

    .line 781
    if-eqz p2, :cond_4

    :goto_1
    iput-object p2, p0, Lcom/google/android/gms/cast/b/e;->C:Lcom/google/android/gms/cast/LaunchOptions;

    .line 783
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->C:Lcom/google/android/gms/cast/LaunchOptions;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/cast/LaunchOptions;->a(Z)V

    .line 784
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/cast/b/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 781
    :cond_4
    new-instance p2, Lcom/google/android/gms/cast/LaunchOptions;

    invoke-direct {p2}, Lcom/google/android/gms/cast/LaunchOptions;-><init>()V

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 789
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "joinApplication"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 790
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->h:Landroid/content/Context;

    invoke-static {v0, p0, p1, p2}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    return-void
.end method

.method public final b(Z)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 493
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "disconnectFromDeviceInternal: disposed: %b isSocketConnected(): %b, explicit: %b"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget-boolean v3, p0, Lcom/google/android/gms/cast/b/e;->J:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    iget-object v3, p0, Lcom/google/android/gms/cast/b/e;->l:Lcom/google/android/gms/cast/b/o;

    invoke-virtual {v3}, Lcom/google/android/gms/cast/b/o;->d()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 495
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->P:Lcom/google/android/gms/cast/b/aj;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/aj;->a()Z

    .line 496
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->l:Lcom/google/android/gms/cast/b/o;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/o;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->l:Lcom/google/android/gms/cast/b/o;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/o;->c()V

    .line 517
    :goto_0
    return-void

    .line 498
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->l:Lcom/google/android/gms/cast/b/o;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/o;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 499
    if-eqz p1, :cond_1

    .line 502
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    iget-object v0, v0, Lcom/google/android/gms/cast/b/af;->b:Ljava/lang/String;

    .line 503
    if-eqz v0, :cond_1

    .line 505
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v2, "explicitly disconnecting from app %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 506
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->p:Lcom/google/android/gms/cast/b/z;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/b/z;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 513
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->l:Lcom/google/android/gms/cast/b/o;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/o;->c()V

    goto :goto_0

    .line 507
    :catch_0
    move-exception v0

    .line 508
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Error while disconnecting: %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 515
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Socket is NOT connected."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(ZDZ)V
    .locals 4

    .prologue
    .line 900
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/cast/b/af;->a(ZDZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 905
    :goto_0
    return-void

    .line 901
    :catch_0
    move-exception v0

    .line 902
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Error while setting mute state"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 903
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/b/e;->h(I)V

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 351
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->P:Lcom/google/android/gms/cast/b/aj;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/aj;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "already reconnecting; ignoring"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 357
    :goto_0
    return-void

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "calling CastOperationService.connectToDevice"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 356
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->h:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;)V

    goto :goto_0
.end method

.method public final c(I)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1111
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onDisconnected; socketError=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1113
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->s:Lcom/google/android/gms/cast/b/ab;

    if-eqz v0, :cond_0

    .line 1114
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->s:Lcom/google/android/gms/cast/b/ab;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/b/e;->b(Lcom/google/android/gms/cast/internal/b;)V

    .line 1115
    iput-object v5, p0, Lcom/google/android/gms/cast/b/e;->s:Lcom/google/android/gms/cast/b/ab;

    .line 1118
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->r:Lcom/google/android/gms/cast/b/ae;

    if-eqz v0, :cond_1

    .line 1119
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->r:Lcom/google/android/gms/cast/b/ae;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/b/e;->b(Lcom/google/android/gms/cast/internal/b;)V

    .line 1120
    iput-object v5, p0, Lcom/google/android/gms/cast/b/e;->r:Lcom/google/android/gms/cast/b/ae;

    .line 1123
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/e;->J:Z

    if-eqz v0, :cond_2

    .line 1124
    invoke-direct {p0, p1}, Lcom/google/android/gms/cast/b/e;->f(I)V

    .line 1129
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    iput-object v5, v0, Lcom/google/android/gms/cast/b/af;->b:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/android/gms/cast/b/af;->c:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/internal/p;->a()V

    iget-object v1, v0, Lcom/google/android/gms/cast/b/af;->d:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/internal/p;->a()V

    iget-object v1, v0, Lcom/google/android/gms/cast/b/af;->e:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/internal/p;->a()V

    iget-object v1, v0, Lcom/google/android/gms/cast/b/af;->f:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/internal/p;->a()V

    iget-object v1, v0, Lcom/google/android/gms/cast/b/af;->g:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/internal/p;->a()V

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/google/android/gms/cast/b/af;->h:D

    iput-boolean v4, v0, Lcom/google/android/gms/cast/b/af;->i:Z

    iput v6, v0, Lcom/google/android/gms/cast/b/af;->k:I

    iput v6, v0, Lcom/google/android/gms/cast/b/af;->l:I

    iput-boolean v4, v0, Lcom/google/android/gms/cast/b/af;->j:Z

    .line 1130
    return-void

    .line 1126
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->h:Landroid/content/Context;

    invoke-static {v0, p0, p1}, Lcom/google/android/gms/cast/service/CastOperationService;->b(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;I)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 952
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->h:Landroid/content/Context;

    invoke-static {v0, p0, p1}, Lcom/google/android/gms/cast/service/CastOperationService;->b(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;Ljava/lang/String;)V

    .line 953
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 795
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "joinApplicationInternal(%s, %s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v4

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 798
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->x:Lcom/google/android/gms/cast/ApplicationMetadata;

    if-eqz v0, :cond_3

    .line 799
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->x:Lcom/google/android/gms/cast/ApplicationMetadata;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/ApplicationMetadata;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->y:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 803
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "already connected to requested app, so skipping join logic"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 805
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->x:Lcom/google/android/gms/cast/ApplicationMetadata;

    iget-object v2, p0, Lcom/google/android/gms/cast/b/e;->z:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/cast/b/e;->y:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/gms/cast/b/m;->a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 830
    :goto_0
    return-void

    .line 808
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "clearing mLastConnected* variables"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 809
    iput-object v5, p0, Lcom/google/android/gms/cast/b/e;->R:Ljava/lang/String;

    .line 810
    iput-object v5, p0, Lcom/google/android/gms/cast/b/e;->S:Ljava/lang/String;

    .line 812
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->P:Lcom/google/android/gms/cast/b/aj;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/aj;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 813
    invoke-direct {p0, v4}, Lcom/google/android/gms/cast/b/e;->e(Z)V

    goto :goto_0

    .line 815
    :cond_2
    const/16 v0, 0x7d5

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/b/e;->i(I)V

    goto :goto_0

    .line 822
    :cond_3
    if-eqz p1, :cond_4

    :goto_1
    iput-object p1, p0, Lcom/google/android/gms/cast/b/e;->D:Ljava/lang/String;

    .line 823
    iput-object p2, p0, Lcom/google/android/gms/cast/b/e;->E:Ljava/lang/String;

    .line 825
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/af;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 826
    :catch_0
    move-exception v0

    .line 827
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Error while requesting device status for join"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 828
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/b/e;->h(I)V

    goto :goto_0

    .line 822
    :cond_4
    const-string p1, ""

    goto :goto_1
.end method

.method public final declared-synchronized c(Z)V
    .locals 5

    .prologue
    .line 1315
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->g:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/e;->g:Ljava/lang/Integer;

    .line 1320
    sget-boolean v0, Lcom/google/android/gms/cast/b/e;->c:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1321
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "acquireReference: Acquiring wake lock"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1322
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->T:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1327
    :goto_0
    monitor-exit p0

    return-void

    .line 1324
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "acquireReference: Not acquiring wake lock. shouldAcquireWakeLock: %b flag: %b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-boolean v4, Lcom/google/android/gms/cast/b/e;->c:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1315
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 372
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->l:Lcom/google/android/gms/cast/b/o;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/o;->h()I

    move-result v0

    .line 373
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v2, "connectToDeviceInternal; socket state = %d"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 374
    if-eq v0, v7, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 375
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Redundant call to connect to device"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 396
    :goto_0
    return-void

    .line 379
    :cond_1
    iput-boolean v7, p0, Lcom/google/android/gms/cast/b/e;->H:Z

    .line 380
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->P:Lcom/google/android/gms/cast/b/aj;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/aj;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 381
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->P:Lcom/google/android/gms/cast/b/aj;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/cast/b/aj;->c:J

    iput-wide v2, v0, Lcom/google/android/gms/cast/b/aj;->d:J

    .line 383
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->P:Lcom/google/android/gms/cast/b/aj;

    iget-wide v2, v0, Lcom/google/android/gms/cast/b/aj;->d:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/cast/b/aj;->c:J

    .line 385
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->j:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/CastDevice;->l()Z

    move-result v0

    if-nez v0, :cond_4

    .line 386
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->N:Lcom/google/android/gms/cast/d/a;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->j:Lcom/google/android/gms/cast/CastDevice;

    iget-object v2, p0, Lcom/google/android/gms/cast/b/e;->l:Lcom/google/android/gms/cast/b/o;

    iget-object v3, p0, Lcom/google/android/gms/cast/b/e;->O:Lcom/google/android/gms/cast/d/d;

    iget-object v4, p0, Lcom/google/android/gms/cast/b/e;->V:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/cast/b/e;->U:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/cast/d/a;->a(Lcom/google/android/gms/cast/CastDevice;Lcom/google/android/gms/cast/b/o;Lcom/google/android/gms/cast/d/d;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 389
    :cond_4
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "connecting socket now"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 390
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->l:Lcom/google/android/gms/cast/b/o;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->j:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/CastDevice;->c()Ljava/net/Inet4Address;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/cast/b/e;->j:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/CastDevice;->g()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/b/o;->a(Ljava/net/Inet4Address;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 391
    :catch_0
    move-exception v0

    .line 392
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v2, "connection exception"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 393
    const/4 v0, 0x7

    invoke-direct {p0, v0, v7}, Lcom/google/android/gms/cast/b/e;->a(IZ)V

    goto :goto_0
.end method

.method public final d(I)V
    .locals 5

    .prologue
    .line 1134
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onSocketDisconnectedInternal: socketError=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1135
    invoke-direct {p0, p1}, Lcom/google/android/gms/cast/b/e;->f(I)V

    .line 1136
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 956
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 962
    :goto_0
    return-void

    .line 960
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->t:Ljava/util/Set;

    monitor-enter v1

    .line 961
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->t:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 962
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final declared-synchronized d(Z)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1339
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v3, "releaseReference. shouldReleaseWakeLock: %b"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1342
    if-eqz p1, :cond_0

    .line 1343
    iget-object v2, p0, Lcom/google/android/gms/cast/b/e;->T:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1344
    iget-object v2, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v3, "releaseReference: Wake lock is held, and is now being released."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1345
    iget-object v2, p0, Lcom/google/android/gms/cast/b/e;->T:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1351
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/cast/b/e;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_4

    .line 1352
    iget-object v2, p0, Lcom/google/android/gms/cast/b/e;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/cast/b/e;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_3

    .line 1360
    :goto_1
    if-eqz v0, :cond_1

    .line 1361
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/cast/b/e;->J:Z

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "[%s] *** disposing ***"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/cast/b/e;->j:Lcom/google/android/gms/cast/CastDevice;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/gms/cast/b/e;->t()V

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->G:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->Q:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/b/e;->b(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1363
    :cond_1
    monitor-exit p0

    return-void

    .line 1347
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v3, "Unbalanced call in releasing the wake lock."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1339
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move v0, v1

    .line 1352
    goto :goto_1

    .line 1357
    :cond_4
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Unbalanced call to releaseReference(); mDisposed=%b"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/google/android/gms/cast/b/e;->J:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v1

    goto :goto_1
.end method

.method public final e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 966
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->h:Landroid/content/Context;

    invoke-static {v0, p0, p1}, Lcom/google/android/gms/cast/service/CastOperationService;->c(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;Ljava/lang/String;)V

    .line 967
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 433
    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/e;->I:Z

    return v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 970
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 976
    :goto_0
    return-void

    .line 974
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->t:Ljava/util/Set;

    monitor-enter v1

    .line 975
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->t:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 976
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 437
    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/e;->H:Z

    return v0
.end method

.method public final g()D
    .locals 2

    .prologue
    .line 445
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    iget-wide v0, v0, Lcom/google/android/gms/cast/b/af;->h:D

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final h()J
    .locals 2

    .prologue
    .line 469
    iget-wide v0, p0, Lcom/google/android/gms/cast/b/e;->L:J

    return-wide v0
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 833
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "leaveApplication"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 834
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->h:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/google/android/gms/cast/service/CastOperationService;->b(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;)V

    .line 835
    return-void
.end method

.method public final j()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 839
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "leaveApplicationInternal()"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 840
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->x:Lcom/google/android/gms/cast/ApplicationMetadata;

    if-nez v0, :cond_0

    .line 841
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->k:Lcom/google/android/gms/cast/b/m;

    invoke-interface {v0}, Lcom/google/android/gms/cast/b/m;->a()V

    .line 846
    :goto_0
    return-void

    .line 845
    :cond_0
    invoke-direct {p0, v3}, Lcom/google/android/gms/cast/b/e;->e(I)V

    goto :goto_0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 866
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->h:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/google/android/gms/cast/service/CastOperationService;->c(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;)V

    .line 867
    return-void
.end method

.method public final l()V
    .locals 4

    .prologue
    .line 872
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/af;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 877
    :goto_0
    return-void

    .line 873
    :catch_0
    move-exception v0

    .line 874
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Error while stopping application"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 875
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/b/e;->h(I)V

    goto :goto_0
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 982
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->h:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/google/android/gms/cast/service/CastOperationService;->d(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;)V

    .line 983
    return-void
.end method

.method public final n()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 987
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onSocketConnectedInternal"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 994
    new-instance v0, Lcom/google/android/gms/cast/b/j;

    const-string v1, "receiver-0"

    iget-object v2, p0, Lcom/google/android/gms/cast/b/e;->l:Lcom/google/android/gms/cast/b/o;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/b/o;->i()[B

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/cast/b/e;->U:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/gms/cast/b/j;-><init>(Lcom/google/android/gms/cast/b/e;Ljava/lang/String;[BLjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/b/e;->q:Lcom/google/android/gms/cast/b/aa;

    .line 1011
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->q:Lcom/google/android/gms/cast/b/aa;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/internal/b;)V

    .line 1014
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->q:Lcom/google/android/gms/cast/b/aa;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/aa;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1021
    :goto_0
    return-void

    .line 1015
    :catch_0
    move-exception v0

    .line 1016
    iget-object v1, p0, Lcom/google/android/gms/cast/b/e;->f:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Not able to authenticated Cast Device"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1017
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->q:Lcom/google/android/gms/cast/b/aa;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/b/e;->b(Lcom/google/android/gms/cast/internal/b;)V

    .line 1018
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/e;->q:Lcom/google/android/gms/cast/b/aa;

    .line 1019
    const/4 v0, 0x7

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/cast/b/e;->a(IZ)V

    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1307
    iget-object v0, p0, Lcom/google/android/gms/cast/b/e;->o:Lcom/google/android/gms/cast/b/af;

    iget-object v0, v0, Lcom/google/android/gms/cast/b/af;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final declared-synchronized p()V
    .locals 1

    .prologue
    .line 1366
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/b/e;->d(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1367
    monitor-exit p0

    return-void

    .line 1366
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
