.class final Lcom/google/android/gms/plus/apps/at;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/apps/as;

.field private final b:Lcom/google/android/gms/plus/model/a/a;

.field private final c:Ljava/lang/String;

.field private d:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/apps/as;Lcom/google/android/gms/plus/model/a/a;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/at;->a:Lcom/google/android/gms/plus/apps/as;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iput-object p2, p0, Lcom/google/android/gms/plus/apps/at;->b:Lcom/google/android/gms/plus/model/a/a;

    .line 92
    iput-object p3, p0, Lcom/google/android/gms/plus/apps/at;->c:Ljava/lang/String;

    .line 93
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/apps/at;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/at;->d:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/apps/at;)Lcom/google/android/gms/plus/model/a/a;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/at;->b:Lcom/google/android/gms/plus/model/a/a;

    return-object v0
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 98
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/at;->a:Lcom/google/android/gms/plus/apps/as;

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/as;->b(Lcom/google/android/gms/plus/apps/as;)Lcom/google/android/gms/common/server/n;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/at;->a:Lcom/google/android/gms/plus/apps/as;

    invoke-static {v1}, Lcom/google/android/gms/plus/apps/as;->a(Lcom/google/android/gms/plus/apps/as;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/at;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/server/n;->a(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v0

    .line 99
    if-nez v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/at;->a:Lcom/google/android/gms/plus/apps/as;

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/as;->c(Lcom/google/android/gms/plus/apps/as;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/at;->a:Lcom/google/android/gms/plus/apps/as;

    invoke-static {v1}, Lcom/google/android/gms/plus/apps/as;->c(Lcom/google/android/gms/plus/apps/as;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 117
    :goto_0
    return-void

    .line 104
    :cond_0
    const/4 v1, 0x0

    array-length v2, v0

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/at;->a:Lcom/google/android/gms/plus/apps/as;

    invoke-static {v3}, Lcom/google/android/gms/plus/apps/as;->d(Lcom/google/android/gms/plus/apps/as;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/at;->d:Landroid/graphics/Bitmap;

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/at;->d:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/at;->a:Lcom/google/android/gms/plus/apps/as;

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/as;->c(Lcom/google/android/gms/plus/apps/as;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/at;->a:Lcom/google/android/gms/plus/apps/as;

    invoke-static {v1}, Lcom/google/android/gms/plus/apps/as;->c(Lcom/google/android/gms/plus/apps/as;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 112
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/at;->a:Lcom/google/android/gms/plus/apps/as;

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/as;->c(Lcom/google/android/gms/plus/apps/as;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/at;->a:Lcom/google/android/gms/plus/apps/as;

    invoke-static {v1}, Lcom/google/android/gms/plus/apps/as;->c(Lcom/google/android/gms/plus/apps/as;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 110
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/at;->a:Lcom/google/android/gms/plus/apps/as;

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/as;->c(Lcom/google/android/gms/plus/apps/as;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/at;->a:Lcom/google/android/gms/plus/apps/as;

    invoke-static {v1}, Lcom/google/android/gms/plus/apps/as;->c(Lcom/google/android/gms/plus/apps/as;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 114
    :catch_1
    move-exception v0

    const-string v0, "ListAppsImageManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Malformed image URL: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/at;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/at;->a:Lcom/google/android/gms/plus/apps/as;

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/as;->c(Lcom/google/android/gms/plus/apps/as;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/at;->a:Lcom/google/android/gms/plus/apps/as;

    invoke-static {v1}, Lcom/google/android/gms/plus/apps/as;->c(Lcom/google/android/gms/plus/apps/as;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
