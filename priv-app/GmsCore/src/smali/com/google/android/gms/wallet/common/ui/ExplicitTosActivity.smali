.class public Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;
.super Lcom/google/android/gms/wallet/common/ui/ds;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/wallet/common/ui/bf;


# instance fields
.field a:Landroid/webkit/WebView;

.field b:Landroid/widget/TextView;

.field c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

.field d:Landroid/widget/ProgressBar;

.field e:Lcom/google/android/gms/wallet/common/ui/bw;

.field f:Lcom/google/android/gms/wallet/common/ui/bb;

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ds;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->g:Z

    .line 179
    return-void
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 57
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 58
    const-string v1, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 59
    const-string v1, "legalDocs"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 60
    const-string v1, "com.google.android.gms"

    const-class v2, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 62
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;)V
    .locals 3

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "ExplicitTosActivity.NetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 131
    if-eqz p1, :cond_0

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->a:Landroid/webkit/WebView;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 138
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->a:Landroid/webkit/WebView;

    if-nez p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/webkit/WebView;->setEnabled(Z)V

    .line 139
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->b:Landroid/widget/TextView;

    if-nez p1, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    if-nez p1, :cond_3

    :goto_3
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 141
    return-void

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->d:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 138
    goto :goto_1

    :cond_2
    move v0, v2

    .line 139
    goto :goto_2

    :cond_3
    move v1, v2

    .line 140
    goto :goto_3
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;)Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->g:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;)V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->a(Z)V

    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "legalDocs"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    .line 126
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 127
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->a(Z)V

    .line 128
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    .prologue
    .line 157
    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_0

    .line 158
    packed-switch p1, :pswitch_data_0

    .line 166
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected button "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :pswitch_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->a(ILandroid/content/Intent;)V

    .line 164
    :goto_0
    return-void

    .line 163
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->e()V

    goto :goto_0

    .line 169
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected errorCode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 158
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 176
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->a(ILandroid/content/Intent;)V

    .line 177
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 67
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/ds;->onCreate(Landroid/os/Bundle;)V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 70
    const-string v0, "legalDocs"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "Activity requires legalDocs extra"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 72
    const-string v0, "legalDocs"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    .line 74
    const-string v2, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 76
    sget-object v2, Lcom/google/android/gms/wallet/common/ui/dt;->b:Lcom/google/android/gms/wallet/common/ui/dz;

    invoke-static {p0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/common/ui/dz;)V

    .line 78
    sget v1, Lcom/google/android/gms/l;->gc:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->setContentView(I)V

    .line 80
    sget v1, Lcom/google/android/gms/j;->tc:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    .line 81
    sget v2, Lcom/google/android/gms/p;->BA:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(I)V

    .line 82
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/bw;

    invoke-direct {v1, p0}, Lcom/google/android/gms/wallet/common/ui/bw;-><init>(Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->e:Lcom/google/android/gms/wallet/common/ui/bw;

    .line 83
    sget v1, Lcom/google/android/gms/j;->ug:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/webkit/WebView;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->a:Landroid/webkit/WebView;

    .line 84
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->a:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->e:Lcom/google/android/gms/wallet/common/ui/bw;

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 85
    sget v1, Lcom/google/android/gms/j;->mT:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->b:Landroid/widget/TextView;

    .line 86
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->b()Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->Ci:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    sget v3, Lcom/google/android/gms/p;->CH:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/w;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 89
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->b:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/common/ui/ClickSpan;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 90
    sget v0, Lcom/google/android/gms/j;->cj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/view/View$OnClickListener;)V

    .line 92
    sget v0, Lcom/google/android/gms/j;->pO:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->d:Landroid/widget/ProgressBar;

    .line 94
    if-nez p1, :cond_1

    .line 95
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->e()V

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    const-string v0, "tosLoaded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->g:Z

    .line 98
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->g:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    move-result-object v0

    if-nez v0, :cond_0

    .line 99
    :cond_2
    iput-boolean v4, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->g:Z

    .line 100
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->e()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 107
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/ds;->onResume()V

    .line 109
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "ExplicitTosActivity.NetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 111
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 115
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/ds;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 117
    const-string v0, "tosLoaded"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 118
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->g:Z

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 121
    :cond_0
    return-void
.end method
