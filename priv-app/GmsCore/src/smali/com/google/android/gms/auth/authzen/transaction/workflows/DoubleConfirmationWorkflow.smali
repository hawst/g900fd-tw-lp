.class public Lcom/google/android/gms/auth/authzen/transaction/workflows/DoubleConfirmationWorkflow;
.super Lcom/google/android/gms/auth/authzen/transaction/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/gms/auth/authzen/transaction/a;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/ab/b/a/a/r;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 33
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/r;->c:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    iget-boolean v1, v1, Lcom/google/ab/b/a/a/q;->f:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    iget-object v1, v1, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    iget-boolean v1, v1, Lcom/google/ab/b/a/a/e;->c:Z

    if-nez v1, :cond_1

    .line 44
    :cond_0
    :goto_0
    return v0

    .line 39
    :cond_1
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/auth/authzen/transaction/b/i;->a(Lcom/google/ab/b/a/a/r;)Lcom/google/android/gms/auth/authzen/transaction/b/h;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/auth/authzen/transaction/b/h;->b()Landroid/os/Bundle;
    :try_end_0
    .catch Lcom/google/android/gms/auth/authzen/transaction/b/j; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    const/4 v0, 0x1

    goto :goto_0

    .line 42
    :catch_0
    move-exception v1

    iget-object v1, p0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    iget-object v1, v1, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    iget v1, v1, Lcom/google/ab/b/a/a/e;->d:I

    .line 43
    const-string v2, "AuthZen"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error while creating TextProvider for UseCase: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static b(Lcom/google/ab/b/a/a/r;Ljava/lang/String;[B)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 27
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/auth/authzen/transaction/workflows/DoubleConfirmationWorkflow;->a(Lcom/google/ab/b/a/a/r;Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    .line 28
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/authzen/transaction/workflows/DoubleConfirmationWorkflow;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 29
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/auth/authzen/transaction/a/f;I)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 50
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/auth/authzen/transaction/a;->a(Lcom/google/android/gms/auth/authzen/transaction/a/f;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    :goto_0
    return v2

    .line 53
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/auth/authzen/transaction/a/f;->a()Ljava/lang/String;

    move-result-object v0

    .line 54
    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/a/d;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 55
    if-nez p2, :cond_1

    .line 56
    invoke-virtual {p0, v3, v3}, Lcom/google/android/gms/auth/authzen/transaction/workflows/DoubleConfirmationWorkflow;->a(IZ)V

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/DoubleConfirmationWorkflow;->a:Landroid/os/Bundle;

    invoke-static {v0}, Lcom/google/android/gms/auth/authzen/transaction/a/c;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/authzen/transaction/a/c;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/DoubleConfirmationWorkflow;->a(Lcom/google/android/gms/auth/authzen/transaction/a/f;Lcom/google/android/gms/auth/authzen/transaction/a/f;)V

    goto :goto_0

    .line 60
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/gms/auth/authzen/transaction/workflows/DoubleConfirmationWorkflow;->a(Lcom/google/android/gms/auth/authzen/transaction/a/f;)V

    goto :goto_0

    .line 63
    :cond_2
    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/a/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 65
    if-nez p2, :cond_3

    .line 67
    invoke-virtual {p0, v3, v3}, Lcom/google/android/gms/auth/authzen/transaction/workflows/DoubleConfirmationWorkflow;->a(II)V

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/DoubleConfirmationWorkflow;->a:Landroid/os/Bundle;

    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/b/h;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/DoubleConfirmationWorkflow;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 71
    :cond_3
    const/16 v0, 0x64

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/auth/authzen/transaction/workflows/DoubleConfirmationWorkflow;->a(II)V

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/DoubleConfirmationWorkflow;->a:Landroid/os/Bundle;

    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/b/h;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 76
    :cond_4
    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/a/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 77
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/DoubleConfirmationWorkflow;->setResult(I)V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/DoubleConfirmationWorkflow;->finish()V

    goto :goto_0

    .line 81
    :cond_5
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment not supported in double confirm workflow: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
