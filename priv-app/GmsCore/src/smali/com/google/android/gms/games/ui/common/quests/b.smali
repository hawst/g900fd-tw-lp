.class public final Lcom/google/android/gms/games/ui/common/quests/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Z

.field final b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final c:Landroid/view/View;

.field final d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final e:Landroid/widget/TextView;

.field final f:Landroid/widget/ProgressBar;

.field final g:Landroid/widget/TextView;

.field final h:Landroid/database/CharArrayBuffer;

.field final i:Landroid/widget/TextView;

.field final j:Landroid/database/CharArrayBuffer;

.field final k:Landroid/widget/TextView;

.field final l:Landroid/widget/TextView;

.field final m:Landroid/widget/TextView;

.field final n:Landroid/widget/Button;

.field final o:Landroid/widget/Button;

.field final p:Landroid/view/View;

.field final q:Landroid/widget/Button;

.field final r:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    sget v0, Lcom/google/android/gms/j;->qc:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/b;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    .line 55
    sget v0, Lcom/google/android/gms/j;->bQ:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/b;->c:Landroid/view/View;

    .line 56
    sget v0, Lcom/google/android/gms/j;->qf:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/b;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    .line 57
    sget v0, Lcom/google/android/gms/j;->qj:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/b;->e:Landroid/widget/TextView;

    .line 58
    sget v0, Lcom/google/android/gms/j;->qi:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/b;->f:Landroid/widget/ProgressBar;

    .line 59
    sget v0, Lcom/google/android/gms/j;->qh:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/b;->g:Landroid/widget/TextView;

    .line 60
    new-instance v0, Landroid/database/CharArrayBuffer;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/b;->h:Landroid/database/CharArrayBuffer;

    .line 61
    sget v0, Lcom/google/android/gms/j;->qe:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/b;->i:Landroid/widget/TextView;

    .line 62
    new-instance v0, Landroid/database/CharArrayBuffer;

    const/16 v1, 0x1f4

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/b;->j:Landroid/database/CharArrayBuffer;

    .line 63
    sget v0, Lcom/google/android/gms/j;->qk:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/b;->k:Landroid/widget/TextView;

    .line 64
    sget v0, Lcom/google/android/gms/j;->qb:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/b;->l:Landroid/widget/TextView;

    .line 66
    sget v0, Lcom/google/android/gms/j;->qa:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/b;->m:Landroid/widget/TextView;

    .line 67
    sget v0, Lcom/google/android/gms/j;->d:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/b;->n:Landroid/widget/Button;

    .line 68
    sget v0, Lcom/google/android/gms/j;->cX:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/b;->o:Landroid/widget/Button;

    .line 69
    sget v0, Lcom/google/android/gms/j;->ey:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/b;->p:Landroid/view/View;

    .line 70
    sget v0, Lcom/google/android/gms/j;->oI:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/b;->q:Landroid/widget/Button;

    .line 72
    sget v0, Lcom/google/android/gms/j;->sK:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/b;->r:Landroid/view/View;

    .line 73
    return-void
.end method
