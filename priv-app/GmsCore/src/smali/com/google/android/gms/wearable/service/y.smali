.class public final Lcom/google/android/gms/wearable/service/y;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Z

.field private static b:Lcom/google/android/gms/wearable/node/bb;

.field private static c:Lcom/google/android/gms/wearable/node/a/a;

.field private static d:Lcom/google/android/gms/wearable/node/a/b;

.field private static final e:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 32
    sput-boolean v2, Lcom/google/android/gms/wearable/service/y;->a:Z

    .line 41
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.google.android.gms.wearable.ui.WearableManageSpaceActivity"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "com.google.android.gms.wearable.node.bluetooth.BluetoothClientService"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "com.google.android.gms.wearable.node.bluetooth.BluetoothServerService"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "com.google.android.gms.wearable.node.emulator.NetworkConnectionService"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "com.google.android.gms.wearable.service.WearableService"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "com.google.android.gms.wearable.service.WearableControlService"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "com.google.android.gms.wearable.service.AutoStarterReceiver"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/wearable/service/y;->e:[Ljava/lang/String;

    return-void
.end method

.method public static declared-synchronized a()Lcom/google/android/gms/wearable/node/bb;
    .locals 2

    .prologue
    .line 184
    const-class v0, Lcom/google/android/gms/wearable/service/y;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/android/gms/wearable/service/y;->b:Lcom/google/android/gms/wearable/node/bb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Z
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 63
    const-class v3, Lcom/google/android/gms/wearable/service/y;

    monitor-enter v3

    :try_start_0
    sget-boolean v0, Lcom/google/android/gms/wearable/service/y;->a:Z

    if-eqz v0, :cond_1

    .line 64
    const-string v0, "WearableService"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const-string v0, "WearableService"

    const-string v2, "Attempted to re-initialize wearable statics, ignoring"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    move v0, v1

    .line 147
    :goto_0
    monitor-exit v3

    return v0

    .line 71
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    sput-boolean v0, Lcom/google/android/gms/wearable/service/y;->a:Z

    .line 75
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v0, v4, :cond_5

    .line 76
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v4

    const-string v0, "user"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {v0, v4}, Landroid/os/UserManager;->getSerialNumberForUser(Landroid/os/UserHandle;)J

    move-result-wide v6

    invoke-virtual {v0}, Landroid/os/UserManager;->getUserProfiles()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    if-ne v5, v1, :cond_4

    :cond_2
    move v0, v1

    :goto_1
    const-string v8, "WearableInitializer"

    const/4 v9, 0x2

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "WearableInitializer"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "isRunningProfilePrimaryForUser: handle is "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, ", serialNumber is "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", profile count is "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", primaryForUser "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v8, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    if-nez v0, :cond_5

    .line 77
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v0

    .line 78
    const-string v1, "WearableInitializer"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "disabling android wear components for user "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-static {p0}, Lcom/google/android/gms/wearable/service/y;->b(Landroid/content/Context;)V

    move v0, v2

    .line 80
    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 76
    goto :goto_1

    .line 84
    :cond_5
    new-instance v4, Lcom/google/android/gms/wearable/b/a;

    sget-object v0, Lcom/google/android/gms/wearable/a/b;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    const-string v5, "goldfish"

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    move v2, v1

    :cond_6
    invoke-direct {v4, p0, v2}, Lcom/google/android/gms/wearable/b/a;-><init>(Landroid/content/Context;Z)V

    sput-object v4, Lcom/google/android/gms/wearable/b/a;->a:Lcom/google/android/gms/wearable/b/a;

    .line 87
    new-instance v0, Lcom/google/android/gms/wearable/node/ao;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wearable/node/ao;-><init>(Landroid/content/Context;)V

    .line 88
    invoke-interface {v0}, Lcom/google/android/gms/wearable/node/ba;->a()V

    .line 90
    new-instance v2, Lcom/google/android/gms/wearable/node/an;

    invoke-direct {v2, p0}, Lcom/google/android/gms/wearable/node/an;-><init>(Landroid/content/Context;)V

    .line 92
    invoke-static {p0}, Lcom/google/android/gms/wearable/node/ac;->a(Landroid/content/Context;)Lcom/google/android/gms/wearable/node/ac;

    move-result-object v4

    .line 93
    invoke-virtual {v4, v2}, Lcom/google/android/gms/wearable/node/ac;->a(Lcom/google/android/gms/wearable/node/e;)V

    .line 95
    new-instance v5, Lcom/google/android/gms/wearable/node/o;

    invoke-direct {v5}, Lcom/google/android/gms/wearable/node/o;-><init>()V

    .line 96
    sget-object v6, Lcom/google/android/gms/wearable/node/o;->a:Lcom/google/android/gms/wearable/node/o;

    if-eqz v6, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DataItemService singleton can only be set once."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 96
    :cond_7
    :try_start_2
    sput-object v5, Lcom/google/android/gms/wearable/node/o;->a:Lcom/google/android/gms/wearable/node/o;

    .line 97
    iput-object v4, v5, Lcom/google/android/gms/wearable/node/o;->b:Lcom/google/android/gms/wearable/node/ac;

    .line 98
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iput-object v4, v5, Lcom/google/android/gms/wearable/node/o;->f:Landroid/content/ContentResolver;

    .line 99
    iput-object v0, v5, Lcom/google/android/gms/wearable/node/o;->d:Lcom/google/android/gms/wearable/node/ba;

    .line 100
    const-string v4, "wearable.data_service.settings"

    const/4 v6, 0x0

    invoke-virtual {p0, v4, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    iput-object v4, v5, Lcom/google/android/gms/wearable/node/o;->e:Landroid/content/SharedPreferences;

    .line 102
    new-instance v4, Lcom/google/android/gms/wearable/node/au;

    invoke-direct {v4}, Lcom/google/android/gms/wearable/node/au;-><init>()V

    .line 103
    sput-object v4, Lcom/google/android/gms/wearable/node/au;->a:Lcom/google/android/gms/wearable/node/au;

    .line 106
    new-instance v6, Lcom/google/android/gms/wearable/node/bb;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, p0, v7}, Lcom/google/android/gms/wearable/node/bb;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v6, Lcom/google/android/gms/wearable/service/y;->b:Lcom/google/android/gms/wearable/node/bb;

    .line 108
    new-instance v6, Lcom/google/android/gms/wearable/node/t;

    invoke-direct {v6}, Lcom/google/android/gms/wearable/node/t;-><init>()V

    .line 109
    new-instance v7, Lcom/google/android/gms/wearable/node/h;

    invoke-direct {v7}, Lcom/google/android/gms/wearable/node/h;-><init>()V

    .line 110
    new-instance v8, Lcom/google/android/gms/wearable/node/aw;

    invoke-direct {v8}, Lcom/google/android/gms/wearable/node/aw;-><init>()V

    .line 112
    new-instance v9, Lcom/google/android/gms/wearable/node/am;

    invoke-direct {v9}, Lcom/google/android/gms/wearable/node/am;-><init>()V

    .line 113
    sget-object v10, Lcom/google/android/gms/wearable/node/am;->a:Lcom/google/android/gms/wearable/node/am;

    if-eqz v10, :cond_8

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "NodeService singleton can only be set once."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    sput-object v9, Lcom/google/android/gms/wearable/node/am;->a:Lcom/google/android/gms/wearable/node/am;

    .line 114
    iput-object v0, v9, Lcom/google/android/gms/wearable/node/am;->d:Lcom/google/android/gms/wearable/node/ba;

    .line 116
    iput-object v2, v5, Lcom/google/android/gms/wearable/node/o;->c:Lcom/google/android/gms/wearable/node/e;

    .line 118
    iput-object v5, v7, Lcom/google/android/gms/wearable/node/h;->a:Lcom/google/android/gms/wearable/node/o;

    iget-object v10, v7, Lcom/google/android/gms/wearable/node/h;->a:Lcom/google/android/gms/wearable/node/o;

    iput-object v7, v10, Lcom/google/android/gms/wearable/node/o;->g:Lcom/google/android/gms/wearable/node/h;

    .line 120
    iput-object v0, v6, Lcom/google/android/gms/wearable/node/t;->a:Lcom/google/android/gms/wearable/node/ba;

    .line 121
    iput-object v5, v6, Lcom/google/android/gms/wearable/node/t;->b:Lcom/google/android/gms/wearable/node/o;

    invoke-virtual {v5, v6}, Lcom/google/android/gms/wearable/node/o;->a(Lcom/google/android/gms/wearable/node/l;)V

    .line 122
    const-string v10, "wearable.data_transport.settings"

    const/4 v11, 0x0

    invoke-virtual {p0, v10, v11}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v10

    iput-object v10, v6, Lcom/google/android/gms/wearable/node/t;->c:Landroid/content/SharedPreferences;

    .line 125
    iput-object v0, v8, Lcom/google/android/gms/wearable/node/aw;->a:Lcom/google/android/gms/wearable/node/ba;

    .line 126
    iput-object v4, v8, Lcom/google/android/gms/wearable/node/aw;->b:Lcom/google/android/gms/wearable/node/au;

    .line 127
    iput-object v8, v4, Lcom/google/android/gms/wearable/node/au;->b:Lcom/google/android/gms/wearable/node/aw;

    .line 129
    sget-object v4, Lcom/google/android/gms/wearable/service/y;->b:Lcom/google/android/gms/wearable/node/bb;

    iput-object v0, v4, Lcom/google/android/gms/wearable/node/bb;->c:Lcom/google/android/gms/wearable/node/ba;

    .line 130
    sget-object v0, Lcom/google/android/gms/wearable/service/y;->b:Lcom/google/android/gms/wearable/node/bb;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/wearable/node/bb;->a(Lcom/google/android/gms/wearable/node/z;)V

    .line 131
    sget-object v0, Lcom/google/android/gms/wearable/service/y;->b:Lcom/google/android/gms/wearable/node/bb;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/wearable/node/bb;->a(Lcom/google/android/gms/wearable/node/z;)V

    .line 132
    sget-object v0, Lcom/google/android/gms/wearable/service/y;->b:Lcom/google/android/gms/wearable/node/bb;

    invoke-virtual {v0, v8}, Lcom/google/android/gms/wearable/node/bb;->a(Lcom/google/android/gms/wearable/node/z;)V

    .line 133
    sget-object v0, Lcom/google/android/gms/wearable/service/y;->b:Lcom/google/android/gms/wearable/node/bb;

    invoke-virtual {v0, v9}, Lcom/google/android/gms/wearable/node/bb;->a(Lcom/google/android/gms/wearable/node/z;)V

    .line 135
    new-instance v0, Lcom/google/android/gms/wearable/node/a/a;

    invoke-static {}, Lcom/google/android/gms/wearable/service/h;->a()Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-direct {v0, p0, v4}, Lcom/google/android/gms/wearable/node/a/a;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    sput-object v0, Lcom/google/android/gms/wearable/service/y;->c:Lcom/google/android/gms/wearable/node/a/a;

    .line 136
    new-instance v0, Lcom/google/android/gms/wearable/node/a/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wearable/node/a/b;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/wearable/service/y;->d:Lcom/google/android/gms/wearable/node/a/b;

    .line 137
    sget-object v0, Lcom/google/android/gms/wearable/service/y;->c:Lcom/google/android/gms/wearable/node/a/a;

    sget-object v4, Lcom/google/android/gms/wearable/service/y;->b:Lcom/google/android/gms/wearable/node/bb;

    iput-object v4, v0, Lcom/google/android/gms/wearable/node/a/a;->e:Lcom/google/android/gms/wearable/node/bb;

    .line 139
    const-string v0, "DataTransport"

    invoke-static {v0, v6}, Lcom/google/android/gms/wearable/service/WearableService;->a(Ljava/lang/String;Lcom/google/android/gms/wearable/d/c;)V

    .line 140
    const-string v0, "DataService"

    invoke-static {v0, v5}, Lcom/google/android/gms/wearable/service/WearableService;->a(Ljava/lang/String;Lcom/google/android/gms/wearable/d/c;)V

    .line 141
    const-string v0, "AssetTransport"

    invoke-static {v0, v7}, Lcom/google/android/gms/wearable/service/WearableService;->a(Ljava/lang/String;Lcom/google/android/gms/wearable/d/c;)V

    .line 142
    const-string v0, "AssetStorage"

    invoke-static {v0, v2}, Lcom/google/android/gms/wearable/service/WearableService;->a(Ljava/lang/String;Lcom/google/android/gms/wearable/d/c;)V

    .line 143
    const-string v0, "WearableTransport"

    sget-object v2, Lcom/google/android/gms/wearable/service/y;->b:Lcom/google/android/gms/wearable/node/bb;

    invoke-static {v0, v2}, Lcom/google/android/gms/wearable/service/WearableService;->a(Ljava/lang/String;Lcom/google/android/gms/wearable/d/c;)V

    .line 144
    const-string v0, "ConnectionServiceManager"

    sget-object v2, Lcom/google/android/gms/wearable/service/y;->c:Lcom/google/android/gms/wearable/node/a/a;

    invoke-static {v0, v2}, Lcom/google/android/gms/wearable/service/WearableService;->a(Ljava/lang/String;Lcom/google/android/gms/wearable/d/c;)V

    .line 145
    const-string v0, "ConnectionServiceHelper"

    sget-object v2, Lcom/google/android/gms/wearable/service/y;->d:Lcom/google/android/gms/wearable/node/a/b;

    invoke-static {v0, v2}, Lcom/google/android/gms/wearable/service/WearableService;->a(Ljava/lang/String;Lcom/google/android/gms/wearable/d/c;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v1

    .line 147
    goto/16 :goto_0
.end method

.method public static declared-synchronized b()Lcom/google/android/gms/wearable/node/a/b;
    .locals 2

    .prologue
    .line 188
    const-class v0, Lcom/google/android/gms/wearable/service/y;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/android/gms/wearable/service/y;->d:Lcom/google/android/gms/wearable/node/a/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static b(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 174
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 175
    sget-object v2, Lcom/google/android/gms/wearable/service/y;->e:[Ljava/lang/String;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 176
    new-instance v5, Landroid/content/ComponentName;

    const-string v6, "com.google.android.gms"

    invoke-direct {v5, v6, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x2

    const/4 v6, 0x1

    invoke-virtual {v1, v5, v4, v6}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 175
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181
    :cond_0
    return-void
.end method

.method public static declared-synchronized c()Lcom/google/android/gms/wearable/node/a/a;
    .locals 2

    .prologue
    .line 192
    const-class v0, Lcom/google/android/gms/wearable/service/y;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/android/gms/wearable/service/y;->c:Lcom/google/android/gms/wearable/node/a/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
