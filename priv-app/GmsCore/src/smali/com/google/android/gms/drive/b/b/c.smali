.class public final Lcom/google/android/gms/drive/b/b/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static a:Z


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/drive/b/b;

.field private final d:Ljava/lang/String;

.field private e:Ljava/net/HttpURLConnection;

.field private f:Ljava/io/ByteArrayOutputStream;

.field private g:I

.field private h:Z

.field private i:Ljava/io/DataOutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/drive/b/b/c;->a:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/net/URL;Ljava/lang/String;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/b/b;)V
    .locals 7

    .prologue
    .line 77
    invoke-virtual {p2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v3

    check-cast v3, Ljava/net/HttpURLConnection;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/b/b/c;-><init>(Landroid/content/Context;Ljava/net/URL;Ljava/net/HttpURLConnection;Ljava/lang/String;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/b/b;)V

    .line 79
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ljava/net/URL;Ljava/lang/String;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/b/b;B)V
    .locals 0

    .prologue
    .line 30
    invoke-direct/range {p0 .. p5}, Lcom/google/android/gms/drive/b/b/c;-><init>(Landroid/content/Context;Ljava/net/URL;Ljava/lang/String;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/b/b;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/net/URL;Ljava/net/HttpURLConnection;Ljava/lang/String;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/b/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-boolean v1, p0, Lcom/google/android/gms/drive/b/b/c;->h:Z

    .line 85
    iput-object p1, p0, Lcom/google/android/gms/drive/b/b/c;->b:Landroid/content/Context;

    .line 86
    iput-object p3, p0, Lcom/google/android/gms/drive/b/b/c;->e:Ljava/net/HttpURLConnection;

    .line 87
    iput-object p6, p0, Lcom/google/android/gms/drive/b/b/c;->c:Lcom/google/android/gms/drive/b/b;

    .line 88
    invoke-static {}, Lcom/google/android/gms/drive/b/b/c;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->d:Ljava/lang/String;

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->e:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->e:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p4}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->e:Ljava/net/HttpURLConnection;

    const-string v1, "Content-Type"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "multipart/related; boundary=\""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/drive/b/b/c;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->e:Ljava/net/HttpURLConnection;

    const-string v1, "Host"

    invoke-virtual {p2}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->e:Ljava/net/HttpURLConnection;

    const-string v1, "Connection"

    const-string v2, "close"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->c:Lcom/google/android/gms/drive/b/b;

    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->e:Ljava/net/HttpURLConnection;

    iget-object v1, p0, Lcom/google/android/gms/drive/b/b/c;->b:Landroid/content/Context;

    invoke-static {v0, p5, v1}, Lcom/google/android/gms/drive/b/b;->a(Ljava/net/HttpURLConnection;Lcom/google/android/gms/drive/auth/g;Landroid/content/Context;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->e:Ljava/net/HttpURLConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 100
    return-void
.end method

.method private c()Ljava/io/DataOutputStream;
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->i:Ljava/io/DataOutputStream;

    if-nez v0, :cond_0

    .line 173
    sget-boolean v0, Lcom/google/android/gms/drive/b/b/c;->a:Z

    if-eqz v0, :cond_1

    .line 174
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->f:Ljava/io/ByteArrayOutputStream;

    .line 175
    new-instance v0, Ljava/io/DataOutputStream;

    iget-object v1, p0, Lcom/google/android/gms/drive/b/b/c;->f:Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->i:Ljava/io/DataOutputStream;

    .line 180
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->i:Ljava/io/DataOutputStream;

    return-object v0

    .line 177
    :cond_1
    new-instance v0, Ljava/io/DataOutputStream;

    iget-object v1, p0, Lcom/google/android/gms/drive/b/b/c;->e:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->i:Ljava/io/DataOutputStream;

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 195
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/b/c;->c()Ljava/io/DataOutputStream;

    move-result-object v0

    .line 197
    iget v1, p0, Lcom/google/android/gms/drive/b/b/c;->g:I

    if-eqz v1, :cond_0

    .line 199
    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 201
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "--"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/drive/b/b/c;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 202
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Content-Type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 203
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->e:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Connection already closed"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 185
    return-void

    .line 184
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 188
    iget-boolean v0, p0, Lcom/google/android/gms/drive/b/b/c;->h:Z

    const-string v1, "Request not executed"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 189
    return-void
.end method

.method private static f()Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v5, 0x14

    .line 237
    new-array v1, v5, [C

    .line 239
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    .line 240
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_0

    .line 242
    const-string v3, "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

    const/16 v4, 0x40

    invoke-virtual {v2, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    aput-char v3, v1, v0

    .line 241
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 244
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    .line 245
    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 140
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/b/c;->d()V

    .line 141
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/b/c;->c()Ljava/io/DataOutputStream;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\r\n--"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/drive/b/b/c;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "--\r\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/gms/drive/b/b/c;->c()Ljava/io/DataOutputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    sget-boolean v0, Lcom/google/android/gms/drive/b/b/c;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->f:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/drive/b/b/c;->e:Ljava/net/HttpURLConnection;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-virtual {v0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/OutputStream;->write([B)V

    .line 142
    :cond_0
    iget v0, p0, Lcom/google/android/gms/drive/b/b/c;->g:I

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 143
    iput-boolean v1, p0, Lcom/google/android/gms/drive/b/b/c;->h:Z

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->e:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    return v0

    .line 142
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)J
    .locals 3

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/b/c;->d()V

    .line 133
    :try_start_0
    const-string v0, "application/json; charset=UTF-8"

    const-string v1, "UTF-8"

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/b/b/c;->a(Ljava/lang/String;[B)J
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    return-wide v0

    .line 134
    :catch_0
    move-exception v0

    .line 135
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Unable to encode metadata"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;Ljava/io/InputStream;)J
    .locals 3

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/b/c;->d()V

    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/b/b/c;->c(Ljava/lang/String;)V

    .line 110
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/b/c;->c()Ljava/io/DataOutputStream;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Z)J

    move-result-wide v0

    .line 111
    iget v2, p0, Lcom/google/android/gms/drive/b/b/c;->g:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/gms/drive/b/b/c;->g:I

    .line 112
    return-wide v0
.end method

.method public final a(Ljava/lang/String;[B)J
    .locals 2

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/b/c;->d()V

    .line 124
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/b/b/c;->c(Ljava/lang/String;)V

    .line 125
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/b/c;->c()Ljava/io/DataOutputStream;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/io/DataOutputStream;->write([B)V

    .line 126
    iget v0, p0, Lcom/google/android/gms/drive/b/b/c;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/b/b/c;->g:I

    .line 127
    array-length v0, p2

    int-to-long v0, v0

    return-wide v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/drive/b/a/p;)V
    .locals 11

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/b/c;->d()V

    .line 117
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/b/b/c;->c(Ljava/lang/String;)V

    .line 118
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/b/c;->c()Ljava/io/DataOutputStream;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/gms/drive/b/a/p;->a:Lcom/google/android/gms/drive/b/a/f;

    iget-object v3, p2, Lcom/google/android/gms/drive/b/a/p;->c:Ljava/io/InputStream;

    iget-object v4, p2, Lcom/google/android/gms/drive/b/a/p;->b:Lcom/google/android/gms/drive/b/a/i;

    iget-object v2, v1, Lcom/google/android/gms/drive/b/a/f;->b:Lcom/google/android/gms/drive/b/a/h;

    invoke-interface {v2, v0}, Lcom/google/android/gms/drive/b/a/h;->a(Ljava/io/OutputStream;)Lcom/google/android/gms/drive/b/a/g;

    move-result-object v5

    iget-object v0, v1, Lcom/google/android/gms/drive/b/a/f;->c:Lcom/google/android/gms/drive/b/a/n;

    iget v2, v4, Lcom/google/android/gms/drive/b/a/i;->b:I

    invoke-interface {v0, v2}, Lcom/google/android/gms/drive/b/a/n;->a(I)Lcom/google/android/gms/drive/b/a/m;

    move-result-object v6

    iget v0, v1, Lcom/google/android/gms/drive/b/a/f;->a:I

    new-array v7, v0, [B

    :cond_0
    invoke-virtual {v3, v7}, Ljava/io/InputStream;->read([B)I

    move-result v8

    const/4 v0, -0x1

    if-eq v8, v0, :cond_6

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v8, :cond_0

    aget-byte v0, v7, v2

    invoke-interface {v6, v0}, Lcom/google/android/gms/drive/b/a/m;->a(B)V

    invoke-interface {v6}, Lcom/google/android/gms/drive/b/a/m;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Lcom/google/android/gms/drive/b/a/m;->f()B

    move-result v0

    invoke-interface {v5, v0}, Lcom/google/android/gms/drive/b/a/g;->a(B)V

    :cond_1
    invoke-interface {v6}, Lcom/google/android/gms/drive/b/a/m;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x0

    iget-object v0, v4, Lcom/google/android/gms/drive/b/a/i;->a:Lcom/google/android/gms/drive/b/a/d;

    invoke-interface {v6}, Lcom/google/android/gms/drive/b/a/m;->d()I

    move-result v9

    invoke-virtual {v0, v9}, Lcom/google/android/gms/drive/b/a/d;->b(I)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, v4, Lcom/google/android/gms/drive/b/a/i;->a:Lcom/google/android/gms/drive/b/a/d;

    invoke-interface {v6}, Lcom/google/android/gms/drive/b/a/m;->d()I

    move-result v9

    invoke-virtual {v0, v9}, Lcom/google/android/gms/drive/b/a/d;->a(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/a/c;

    if-nez v1, :cond_3

    invoke-interface {v6}, Lcom/google/android/gms/drive/b/a/m;->e()Ljava/lang/String;

    move-result-object v1

    :cond_3
    iget-object v10, v0, Lcom/google/android/gms/drive/b/a/c;->b:Ljava/lang/String;

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    :goto_1
    if-eqz v0, :cond_4

    iget-wide v0, v0, Lcom/google/android/gms/drive/b/a/c;->a:J

    iget v9, v4, Lcom/google/android/gms/drive/b/a/i;->b:I

    invoke-interface {v5, v0, v1, v9}, Lcom/google/android/gms/drive/b/a/g;->a(JI)V

    invoke-interface {v6}, Lcom/google/android/gms/drive/b/a/m;->c()V

    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    invoke-interface {v6}, Lcom/google/android/gms/drive/b/a/m;->h()I

    move-result v1

    if-eqz v1, :cond_a

    const/4 v0, 0x0

    iget v2, v4, Lcom/google/android/gms/drive/b/a/i;->c:I

    if-eqz v2, :cond_8

    iget v2, v4, Lcom/google/android/gms/drive/b/a/i;->c:I

    invoke-interface {v6, v2}, Lcom/google/android/gms/drive/b/a/m;->a(I)I

    move-result v2

    iget-object v3, v4, Lcom/google/android/gms/drive/b/a/i;->d:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v2, v3, :cond_8

    iget v2, v4, Lcom/google/android/gms/drive/b/a/i;->c:I

    invoke-interface {v6, v2}, Lcom/google/android/gms/drive/b/a/m;->b(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v4, Lcom/google/android/gms/drive/b/a/i;->e:Lcom/google/android/gms/drive/b/a/c;

    iget-object v3, v3, Lcom/google/android/gms/drive/b/a/c;->b:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_7

    const/4 v0, 0x1

    iget v2, v4, Lcom/google/android/gms/drive/b/a/i;->c:I

    sub-int/2addr v1, v2

    :cond_7
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v1, :cond_9

    invoke-interface {v6, v2}, Lcom/google/android/gms/drive/b/a/m;->c(I)B

    move-result v3

    invoke-interface {v5, v3}, Lcom/google/android/gms/drive/b/a/g;->a(B)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_8
    const/4 v2, 0x0

    goto :goto_2

    :cond_9
    if-eqz v0, :cond_a

    iget-object v0, v4, Lcom/google/android/gms/drive/b/a/i;->e:Lcom/google/android/gms/drive/b/a/c;

    iget-wide v0, v0, Lcom/google/android/gms/drive/b/a/c;->a:J

    iget v2, v4, Lcom/google/android/gms/drive/b/a/i;->c:I

    invoke-interface {v5, v0, v1, v2}, Lcom/google/android/gms/drive/b/a/g;->a(JI)V

    :cond_a
    invoke-interface {v5}, Lcom/google/android/gms/drive/b/a/g;->a()V

    .line 119
    iget v0, p0, Lcom/google/android/gms/drive/b/b/c;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/b/b/c;->g:I

    .line 120
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/b/c;->d()V

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->e:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1, p2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method public final b()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/b/c;->d()V

    .line 149
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/b/c;->e()V

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->e:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/b/c;->d()V

    .line 155
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/b/c;->e()V

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->e:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 3

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->e:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 162
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/b/c;->c()Ljava/io/DataOutputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->e:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 167
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/c;->e:Ljava/net/HttpURLConnection;

    .line 169
    :cond_0
    return-void

    .line 163
    :catch_0
    move-exception v0

    .line 164
    const-string v1, "MultipartRequest"

    const-string v2, "Unable to close output stream"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method
