.class final Lcom/google/android/gms/plus/sharebox/c;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/c;->a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;B)V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/sharebox/c;-><init>(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;)V

    return-void
.end method

.method private a(I)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/c;->a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;)Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    return-object v0
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/c;->a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;)Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/sharebox/c;->a(I)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/sharebox/c;->a(I)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->hashCode()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 118
    if-nez p2, :cond_2

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/c;->a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->dl:I

    invoke-virtual {v0, v1, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 121
    new-instance v0, Lcom/google/android/gms/plus/sharebox/f;

    invoke-direct {v0, p2}, Lcom/google/android/gms/plus/sharebox/f;-><init>(Landroid/view/View;)V

    .line 122
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    .line 128
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/c;->a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/sharebox/c;->a(I)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v2

    .line 132
    iput p1, v1, Lcom/google/android/gms/plus/sharebox/f;->a:I

    .line 133
    iput-object v2, v1, Lcom/google/android/gms/plus/sharebox/f;->b:Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 134
    if-eqz v2, :cond_1

    .line 136
    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 137
    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v0

    .line 138
    iget-object v3, v1, Lcom/google/android/gms/plus/sharebox/f;->c:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 139
    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->i(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, v1, Lcom/google/android/gms/plus/sharebox/f;->c:Landroid/widget/ImageView;

    sget v3, Lcom/google/android/gms/h;->G:I

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 148
    :cond_0
    :goto_1
    iget-object v0, v1, Lcom/google/android/gms/plus/sharebox/f;->d:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    iget-object v0, v1, Lcom/google/android/gms/plus/sharebox/f;->e:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "checked"

    invoke-virtual {v1, v2, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 155
    :cond_1
    return-object p2

    .line 124
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/f;

    move-object v1, v0

    goto :goto_0

    .line 143
    :cond_3
    iget-object v0, v1, Lcom/google/android/gms/plus/sharebox/f;->c:Landroid/widget/ImageView;

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 144
    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->g()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Lcom/google/android/gms/plus/sharebox/f;->c:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/c;->a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->b(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;)Lcom/google/android/gms/common/internal/a/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/internal/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/android/gms/people/x;->g:Lcom/google/android/gms/people/o;

    iget-object v5, p0, Lcom/google/android/gms/plus/sharebox/c;->a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-static {v5}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->c(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;)Lcom/google/android/gms/common/api/v;

    move-result-object v5

    invoke-interface {v0, v5, v3, v6, v6}, Lcom/google/android/gms/people/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;II)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v5, Lcom/google/android/gms/plus/sharebox/e;

    iget-object v6, p0, Lcom/google/android/gms/plus/sharebox/c;->a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-direct {v5, v6, v3, v4}, Lcom/google/android/gms/plus/sharebox/e;-><init>(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;Ljava/lang/String;Landroid/widget/ImageView;)V

    invoke-interface {v0, v5}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_1

    :cond_4
    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method
