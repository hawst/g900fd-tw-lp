.class public final Lcom/google/android/gms/wallet/dynamite/ui/j;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/wallet/dynamite/ui/a;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:Landroid/util/DisplayMetrics;

.field private final f:Landroid/widget/ImageView;

.field private final g:Landroid/widget/ImageView;

.field private final h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:Lcom/google/android/gms/wallet/dynamite/ui/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/dynamite/fragment/a;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v0, -0x1

    const/4 v5, 0x1

    .line 55
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 49
    iput v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->i:I

    .line 50
    iput v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->j:I

    .line 51
    iput v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->k:I

    .line 56
    invoke-virtual {p0, v5}, Lcom/google/android/gms/wallet/dynamite/ui/j;->setClickable(Z)V

    .line 57
    invoke-virtual {p0, v5}, Lcom/google/android/gms/wallet/dynamite/ui/j;->setFocusable(Z)V

    .line 58
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    iget v1, p2, Lcom/google/android/gms/wallet/dynamite/fragment/a;->b:I

    iget v2, p2, Lcom/google/android/gms/wallet/dynamite/fragment/a;->a:I

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/dynamite/ui/j;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 60
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/dynamite/ui/j;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->e:Landroid/util/DisplayMetrics;

    .line 62
    const/high16 v1, 0x40800000    # 4.0f

    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/dynamite/ui/j;->a(F)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->d:I

    .line 63
    const/high16 v1, 0x42000000    # 32.0f

    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/dynamite/ui/j;->a(F)I

    move-result v1

    iget v2, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->d:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->b:I

    .line 64
    const/high16 v1, 0x42800000    # 64.0f

    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/dynamite/ui/j;->a(F)I

    move-result v1

    iget v2, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->d:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->c:I

    .line 65
    sget-object v1, Lcom/google/android/gms/wallet/dynamite/ui/c;->g:Lcom/google/android/gms/wallet/dynamite/ui/c;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/gms/wallet/dynamite/ui/c;

    invoke-direct {v1, p1}, Lcom/google/android/gms/wallet/dynamite/ui/c;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/google/android/gms/wallet/dynamite/ui/c;->g:Lcom/google/android/gms/wallet/dynamite/ui/c;

    :cond_0
    sget-object v1, Lcom/google/android/gms/wallet/dynamite/ui/c;->g:Lcom/google/android/gms/wallet/dynamite/ui/c;

    iput-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->l:Lcom/google/android/gms/wallet/dynamite/ui/c;

    .line 66
    iget v1, p2, Lcom/google/android/gms/wallet/dynamite/fragment/a;->d:I

    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->h:I

    .line 67
    iget v1, p2, Lcom/google/android/gms/wallet/dynamite/fragment/a;->c:I

    packed-switch v1, :pswitch_data_0

    .line 83
    new-instance v1, Lcom/google/android/gms/wallet/dynamite/ui/e;

    sget v2, Lcom/google/android/gms/p;->Ae:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/ui/j;->a()I

    move-result v2

    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/ui/j;->b()I

    move-result v3

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/gms/wallet/dynamite/ui/e;-><init>(Ljava/lang/String;II)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->a:Lcom/google/android/gms/wallet/dynamite/ui/a;

    .line 88
    :goto_0
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->f:Landroid/widget/ImageView;

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setDuplicateParentStateEnabled(Z)V

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->f:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->f:Landroid/widget/ImageView;

    iget v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->d:I

    iget v2, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->d:I

    iget v3, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->d:I

    iget v4, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->d:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->f:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/dynamite/ui/j;->addView(Landroid/view/View;)V

    .line 94
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 95
    new-array v1, v5, [I

    const v2, 0x10100a7

    aput v2, v1, v6

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/dynamite/ui/j;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/h;->dq:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 97
    new-array v1, v5, [I

    const v2, 0x101009c

    aput v2, v1, v6

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/dynamite/ui/j;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/h;->dm:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 99
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->g:Landroid/widget/ImageView;

    .line 100
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setDuplicateParentStateEnabled(Z)V

    .line 101
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 102
    const/16 v0, 0x13

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->g:Landroid/widget/ImageView;

    iget v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->d:I

    iget v2, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->d:I

    iget v3, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->d:I

    iget v4, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->d:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->g:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/dynamite/ui/j;->addView(Landroid/view/View;)V

    .line 106
    return-void

    .line 69
    :pswitch_0
    new-instance v1, Lcom/google/android/gms/wallet/dynamite/ui/h;

    sget v2, Lcom/google/android/gms/p;->Ad:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/ui/j;->a()I

    move-result v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/wallet/dynamite/ui/h;-><init>(Ljava/lang/String;I)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->a:Lcom/google/android/gms/wallet/dynamite/ui/a;

    goto/16 :goto_0

    .line 73
    :pswitch_1
    new-instance v1, Lcom/google/android/gms/wallet/dynamite/ui/h;

    sget v2, Lcom/google/android/gms/p;->Ac:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/ui/j;->a()I

    move-result v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/wallet/dynamite/ui/h;-><init>(Ljava/lang/String;I)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->a:Lcom/google/android/gms/wallet/dynamite/ui/a;

    goto/16 :goto_0

    .line 77
    :pswitch_2
    new-instance v1, Lcom/google/android/gms/wallet/dynamite/ui/e;

    sget v2, Lcom/google/android/gms/p;->Bd:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/ui/j;->a()I

    move-result v2

    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/ui/j;->b()I

    move-result v3

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/gms/wallet/dynamite/ui/e;-><init>(Ljava/lang/String;II)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->a:Lcom/google/android/gms/wallet/dynamite/ui/a;

    goto/16 :goto_0

    .line 67
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a()I
    .locals 2

    .prologue
    .line 215
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->h:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 216
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/dynamite/ui/j;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->aH:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 218
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private a(F)I
    .locals 2

    .prologue
    .line 191
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->e:Landroid/util/DisplayMetrics;

    invoke-static {v0, p1, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    .line 192
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private a(I)I
    .locals 4

    .prologue
    .line 122
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->d:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, p1, v0

    .line 123
    iget v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->i:I

    if-ne v0, v1, :cond_0

    iget v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->j:I

    if-gez v1, :cond_1

    .line 124
    :cond_0
    int-to-float v1, v0

    mul-int/lit8 v2, v0, 0x2

    int-to-float v2, v2

    const/high16 v3, 0x3ec00000    # 0.375f

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->a:Lcom/google/android/gms/wallet/dynamite/ui/a;

    invoke-interface {v2, v0}, Lcom/google/android/gms/wallet/dynamite/ui/a;->a(I)I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->d:I

    mul-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v0, v1

    .line 126
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->j:I

    .line 128
    :cond_1
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->j:I

    return v0
.end method

.method private static a(Landroid/widget/ImageView;I)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 198
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 199
    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 203
    :goto_0
    return-void

    .line 201
    :cond_0
    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setAlpha(I)V

    goto :goto_0
.end method

.method private b()I
    .locals 2

    .prologue
    .line 222
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->h:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 223
    sget v0, Lcom/google/android/gms/h;->du:I

    .line 225
    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/google/android/gms/h;->dv:I

    goto :goto_0
.end method


# virtual methods
.method protected final drawableStateChanged()V
    .locals 2

    .prologue
    .line 182
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 183
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/dynamite/ui/j;->getDrawableState()[I

    move-result-object v0

    const v1, 0x101009e

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->f:Landroid/widget/ImageView;

    const/16 v1, 0xff

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/dynamite/ui/j;->a(Landroid/widget/ImageView;I)V

    .line 188
    :goto_0
    return-void

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->f:Landroid/widget/ImageView;

    const/16 v1, 0x7f

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/dynamite/ui/j;->a(Landroid/widget/ImageView;I)V

    goto :goto_0
.end method

.method protected final getSuggestedMinimumHeight()I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->b:I

    return v0
.end method

.method protected final getSuggestedMinimumWidth()I
    .locals 2

    .prologue
    .line 115
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->k:I

    if-gez v0, :cond_0

    .line 116
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->b:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/dynamite/ui/j;->a(I)I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->d:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->k:I

    .line 118
    :cond_0
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->k:I

    return v0
.end method

.method protected final onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 162
    sub-int v0, p5, p3

    iget v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->b:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->c:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 163
    iget v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->d:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v2, v0, v1

    .line 164
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/dynamite/ui/j;->a(I)I

    move-result v1

    .line 165
    sub-int v3, p4, p2

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 166
    iget v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->d:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v1, v3, v1

    .line 167
    iget-object v4, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->f:Landroid/widget/ImageView;

    invoke-virtual {v4, v5, v5, v3, v0}, Landroid/widget/ImageView;->layout(IIII)V

    .line 168
    iget-object v4, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->g:Landroid/widget/ImageView;

    invoke-virtual {v4, v5, v5, v3, v0}, Landroid/widget/ImageView;->layout(IIII)V

    .line 169
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 178
    :goto_0
    return-void

    .line 172
    :cond_0
    sub-int v0, v1, v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->a:Lcom/google/android/gms/wallet/dynamite/ui/a;

    invoke-interface {v3, v2}, Lcom/google/android/gms/wallet/dynamite/ui/a;->a(I)I

    move-result v3

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    add-int v3, v2, v0

    .line 174
    new-instance v0, Lcom/google/android/gms/wallet/dynamite/ui/d;

    iget-object v4, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->a:Lcom/google/android/gms/wallet/dynamite/ui/a;

    invoke-interface {v4}, Lcom/google/android/gms/wallet/dynamite/ui/a;->a()Lcom/google/android/gms/wallet/dynamite/ui/b;

    move-result-object v4

    iget v5, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->h:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/dynamite/ui/d;-><init>(IIILcom/google/android/gms/wallet/dynamite/ui/b;I)V

    .line 177
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->l:Lcom/google/android/gms/wallet/dynamite/ui/c;

    iget-object v2, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/wallet/dynamite/ui/c;->a(Ljava/lang/Object;Landroid/widget/ImageView;)V

    goto :goto_0
.end method

.method protected final onMeasure(II)V
    .locals 4

    .prologue
    .line 133
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 134
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 135
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 136
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 140
    packed-switch v0, :pswitch_data_0

    .line 145
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->b:I

    .line 147
    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/dynamite/ui/j;->a(I)I

    move-result v1

    .line 148
    packed-switch v2, :pswitch_data_1

    .line 157
    :goto_1
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/dynamite/ui/j;->setMeasuredDimension(II)V

    .line 158
    return-void

    .line 142
    :pswitch_0
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->b:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/j;->c:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 150
    :pswitch_1
    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_1

    .line 140
    :pswitch_data_0
    .packed-switch 0x40000000
        :pswitch_0
    .end packed-switch

    .line 148
    :pswitch_data_1
    .packed-switch 0x40000000
        :pswitch_1
    .end packed-switch
.end method
