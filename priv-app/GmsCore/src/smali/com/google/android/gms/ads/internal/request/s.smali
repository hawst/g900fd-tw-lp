.class public abstract Lcom/google/android/gms/ads/internal/request/s;
.super Lcom/google/android/gms/ads/internal/util/a;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

.field private final b:Lcom/google/android/gms/ads/internal/request/r;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Lcom/google/android/gms/ads/internal/request/r;)V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/util/a;-><init>()V

    .line 142
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/s;->a:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    .line 143
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/request/s;->b:Lcom/google/android/gms/ads/internal/request/r;

    .line 144
    return-void
.end method

.method private static a(Lcom/google/android/gms/ads/internal/request/z;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)Lcom/google/android/gms/ads/internal/request/AdResponseParcel;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 191
    :try_start_0
    invoke-interface {p0, p1}, Lcom/google/android/gms/ads/internal/request/z;->a(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)Lcom/google/android/gms/ads/internal/request/AdResponseParcel;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    .line 211
    :goto_0
    return-object v0

    .line 192
    :catch_0
    move-exception v1

    .line 193
    const-string v2, "Could not fetch ad response from ad request service."

    invoke-static {v2, v1}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 195
    :catch_1
    move-exception v1

    .line 198
    const-string v2, "Could not fetch ad response from ad request service due to an Exception."

    invoke-static {v2, v1}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 200
    :catch_2
    move-exception v1

    .line 204
    const-string v2, "Could not fetch ad response from ad request service due to an Exception."

    invoke-static {v2, v1}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 206
    :catch_3
    move-exception v1

    invoke-static {v1}, Lcom/google/android/gms/ads/internal/o/e;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 164
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/request/s;->d()Lcom/google/android/gms/ads/internal/request/z;

    move-result-object v0

    .line 165
    if-nez v0, :cond_1

    .line 166
    new-instance v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 174
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/request/s;->c()V

    .line 176
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/s;->b:Lcom/google/android/gms/ads/internal/request/r;

    invoke-interface {v1, v0}, Lcom/google/android/gms/ads/internal/request/r;->a(Lcom/google/android/gms/ads/internal/request/AdResponseParcel;)V

    .line 177
    return-void

    .line 168
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/s;->a:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/request/s;->a(Lcom/google/android/gms/ads/internal/request/z;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-result-object v0

    .line 169
    if-nez v0, :cond_0

    .line 170
    new-instance v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 174
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/request/s;->c()V

    throw v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/request/s;->c()V

    .line 183
    return-void
.end method

.method public abstract c()V
.end method

.method public abstract d()Lcom/google/android/gms/ads/internal/request/z;
.end method
