.class final Lcom/google/android/gms/wallet/service/ow/aa;
.super Lcom/google/android/gms/wallet/service/o;
.source "SourceFile"


# instance fields
.field final synthetic c:Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;

.field final synthetic d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field final synthetic e:Lcom/google/android/gms/wallet/service/ow/y;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/service/ow/y;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/aa;->e:Lcom/google/android/gms/wallet/service/ow/y;

    iput-object p4, p0, Lcom/google/android/gms/wallet/service/ow/aa;->c:Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;

    iput-object p5, p0, Lcom/google/android/gms/wallet/service/ow/aa;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/wallet/service/o;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/a/b;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 10

    .prologue
    const/16 v5, 0xe

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/aa;->c:Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->b()Lcom/google/aa/b/a/a/a/a/k;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/aa/b/a/a/a/a/k;

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/aa;->e:Lcom/google/android/gms/wallet/service/ow/y;

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/aa;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/y;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Z

    move-result v0

    iput-boolean v0, v7, Lcom/google/aa/b/a/a/a/a/k;->l:Z

    .line 124
    invoke-static {}, Lcom/google/android/gms/wallet/service/ow/ao;->a()Lcom/google/aa/b/a/a/a/a/t;

    move-result-object v0

    iput-object v0, v7, Lcom/google/aa/b/a/a/a/a/k;->m:Lcom/google/aa/b/a/a/a/a/t;

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/aa;->e:Lcom/google/android/gms/wallet/service/ow/y;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/y;->a(Lcom/google/android/gms/wallet/service/ow/y;)Lcom/google/android/gms/wallet/common/ab;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/ow/aa;->a:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ow/aa;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v4, v6}, Lcom/google/android/gms/wallet/common/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_0

    .line 129
    iput-object v0, v7, Lcom/google/aa/b/a/a/a/a/k;->b:Ljava/lang/String;

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/aa;->e:Lcom/google/android/gms/wallet/service/ow/y;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/y;->d(Lcom/google/android/gms/wallet/service/ow/y;)Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/common/util/e;->a()J

    move-result-wide v8

    iput-wide v8, v7, Lcom/google/aa/b/a/a/a/a/k;->j:J

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/aa;->e:Lcom/google/android/gms/wallet/service/ow/y;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/y;->b(Lcom/google/android/gms/wallet/service/ow/y;)Lcom/google/android/gms/wallet/service/ow/a;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/ow/aa;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/service/ow/a;->a(Ljava/lang/String;)Lcom/google/aa/b/a/a/a/a/b;

    move-result-object v0

    .line 138
    if-nez v0, :cond_1

    .line 139
    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    .line 159
    :goto_0
    return-object v0

    .line 141
    :cond_1
    iget-object v4, v7, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    iput-object v0, v4, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    .line 145
    iget-object v0, v7, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    if-eqz v0, :cond_8

    .line 146
    iget-object v0, v7, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    .line 147
    iget-object v4, v0, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    if-eqz v4, :cond_8

    .line 148
    iget-object v6, v0, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    .line 149
    iget-object v0, v6, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    if-eqz v0, :cond_8

    .line 150
    iget-object v8, v6, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    .line 151
    iget-object v0, v8, Lcom/google/checkout/a/a/a/c;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 152
    iget-object v0, v8, Lcom/google/checkout/a/a/a/c;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    :goto_2
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 153
    iget-object v4, v8, Lcom/google/checkout/a/a/a/c;->a:Ljava/lang/String;

    .line 154
    iget-object v0, v8, Lcom/google/checkout/a/a/a/c;->b:Ljava/lang/String;

    .line 155
    iput-object v1, v6, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    move-object v6, v0

    move-object v8, v4

    .line 159
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/aa;->e:Lcom/google/android/gms/wallet/service/ow/y;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/y;->c(Lcom/google/android/gms/wallet/service/ow/y;)Lcom/google/android/gms/wallet/service/ow/ak;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/aa;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v1

    iget-object v4, v7, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    if-eqz v4, :cond_2

    iget-object v4, v7, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    iget-object v4, v4, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    if-eqz v4, :cond_2

    iget-object v4, v7, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    iget-object v4, v4, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iget-object v4, v4, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    if-nez v4, :cond_5

    move v4, v2

    :goto_4
    invoke-static {v4}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    :cond_2
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    :goto_5
    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "request"

    invoke-static {v7}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gms/common/util/m;->b([B)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v3, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "card_number"

    invoke-direct {v2, v3, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "cvn"

    invoke-direct {v2, v3, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v2, "/online-secure/v2/sdk/v1/getMaskedWalletForBuyerSelection?s7e=card_number;cvn"

    const-string v6, "get_masked_wallet_signup"

    move-object v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/wallet/service/ow/ak;->a(ILjava/lang/String;Lcom/google/android/gms/wallet/a/b;Ljava/util/ArrayList;ILjava/lang/String;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    move v0, v3

    .line 151
    goto/16 :goto_1

    :cond_4
    move v0, v3

    .line 152
    goto :goto_2

    :cond_5
    move v4, v3

    .line 159
    goto :goto_4

    :cond_6
    move v2, v3

    goto :goto_5

    :cond_7
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    const-string v2, "/online/v2/wallet/sdk/v1/getMaskedWalletForBuyerSelection"

    const-string v6, "get_masked_wallet"

    move-object v3, p1

    move-object v4, v7

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/wallet/service/ow/ak;->a(ILjava/lang/String;Lcom/google/android/gms/wallet/a/b;Lcom/google/protobuf/nano/j;ILjava/lang/String;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    move-object v6, v1

    move-object v8, v1

    goto/16 :goto_3
.end method
