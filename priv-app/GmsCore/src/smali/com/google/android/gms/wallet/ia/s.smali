.class final Lcom/google/android/gms/wallet/ia/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/w;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ia/q;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/ia/q;)V
    .locals 0

    .prologue
    .line 1151
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/s;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 1174
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/s;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/s;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/q;->b(Lcom/google/android/gms/wallet/ia/q;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/s;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/q;->h(Lcom/google/android/gms/wallet/ia/q;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/s;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v3}, Lcom/google/android/gms/wallet/ia/q;->i(Lcom/google/android/gms/wallet/ia/q;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/s;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v4}, Lcom/google/android/gms/wallet/ia/q;->d(Lcom/google/android/gms/wallet/ia/q;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/wallet/ia/s;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v5}, Lcom/google/android/gms/wallet/ia/q;->e(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/checkout/inapp/proto/q;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/wallet/ia/s;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v6}, Lcom/google/android/gms/wallet/ia/q;->f(Lcom/google/android/gms/wallet/ia/q;)Ljava/util/ArrayList;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;Lcom/google/checkout/inapp/proto/a/b;Z)Landroid/content/Intent;

    move-result-object v0

    .line 1184
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/s;->a:Lcom/google/android/gms/wallet/ia/q;

    const/16 v2, 0x1f8

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/wallet/ia/q;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1185
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/a/b;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 1154
    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1155
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/s;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->a(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/s;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/q;->b(Lcom/google/android/gms/wallet/ia/q;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/s;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/q;->i(Lcom/google/android/gms/wallet/ia/q;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/s;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/q;->d(Lcom/google/android/gms/wallet/ia/q;)Ljava/lang/String;

    move-result-object v4

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/s;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/q;->e(Lcom/google/android/gms/wallet/ia/q;)Lcom/google/checkout/inapp/proto/q;

    move-result-object v5

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/s;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/q;->f(Lcom/google/android/gms/wallet/ia/q;)Ljava/util/ArrayList;

    move-result-object v6

    move-object v2, p1

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/a/b;Ljava/util/ArrayList;Ljava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;Z)Landroid/content/Intent;

    move-result-object v0

    .line 1164
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/s;->a:Lcom/google/android/gms/wallet/ia/q;

    const/16 v2, 0x1f9

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/wallet/ia/q;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1170
    :goto_0
    return-void

    .line 1166
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/s;->a:Lcom/google/android/gms/wallet/ia/q;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/q;->u:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object p1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    .line 1167
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/s;->a:Lcom/google/android/gms/wallet/ia/q;

    if-nez p1, :cond_1

    :goto_1
    iput-boolean v7, v0, Lcom/google/android/gms/wallet/ia/q;->w:Z

    .line 1168
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/s;->a:Lcom/google/android/gms/wallet/ia/q;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/q;->g(Lcom/google/android/gms/wallet/ia/q;)V

    goto :goto_0

    .line 1167
    :cond_1
    const/4 v7, 0x0

    goto :goto_1
.end method
