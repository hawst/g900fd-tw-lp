.class public abstract Lcom/google/android/gms/googlehelp/service/a;
.super Landroid/app/Service;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/googlehelp/c/a/p;
.implements Lcom/google/android/gms/googlehelp/common/b;


# instance fields
.field a:Landroid/app/NotificationManager;

.field b:Landroid/support/v4/a/m;

.field protected c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

.field protected d:Lcom/google/android/gms/googlehelp/common/a;

.field protected e:Lcom/google/android/gms/googlehelp/c/a/n;

.field f:Z

.field private g:I

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method protected static a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 122
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_IS_GCM_SUPPORT_UNAVAILABLE_UPDATE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected static a(Ljava/lang/String;JILjava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 112
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_IS_GCM_STATUS_UPDATE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_HANGOUT_UPDATE_VERSION"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_HANGOUT_UPDATE_QUEUE_POSITION"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_HANGOUT_UPDATE_IDENTIFIER"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private varargs a(I[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 585
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 422
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/service/a;->h:Z

    .line 423
    iput-boolean v2, p0, Lcom/google/android/gms/googlehelp/service/a;->i:Z

    .line 424
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->p()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_IS_HANGOUT_REQUEST_CANCELLED"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 426
    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->a(Landroid/content/Intent;)V

    .line 428
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->a:Landroid/app/NotificationManager;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->i()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 429
    invoke-virtual {p0, p1}, Lcom/google/android/gms/googlehelp/service/a;->stopSelf(I)V

    .line 430
    return-void
.end method

.method protected static a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 82
    invoke-static {p2}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_HELP_CONFIG"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 85
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 86
    return-void
.end method

.method protected static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 94
    invoke-static {p1}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_NOTIFY_IF_UPDATING"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 96
    return-void
.end method

.method protected static b(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 135
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/googlehelp/helpactivities/OpenHangoutActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXTRA_HELP_CONFIG"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/content/Intent;)Landroid/util/Pair;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 258
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    if-nez v0, :cond_0

    .line 260
    invoke-static {p1}, Lcom/google/android/gms/googlehelp/GcmBroadcastReceiver;->a(Landroid/content/Intent;)Z

    .line 261
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 292
    :goto_0
    return-object v0

    .line 266
    :cond_0
    :try_start_0
    const-string v0, "EXTRA_HANGOUT_UPDATE_QUEUE_POSITION"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 268
    const-string v0, "EXTRA_HANGOUT_UPDATE_VERSION"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 273
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->c(Lcom/google/android/gms/googlehelp/common/HelpConfig;)I

    move-result v0

    if-ge v4, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->d(Lcom/google/android/gms/googlehelp/common/HelpConfig;)J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-lez v0, :cond_1

    .line 275
    const-string v0, "EXTRA_HANGOUT_UPDATE_IDENTIFIER"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/googlehelp/service/a;->a(JILjava/lang/String;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    .line 281
    if-nez v4, :cond_2

    .line 282
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 289
    :cond_1
    :goto_1
    invoke-static {p1}, Lcom/google/android/gms/googlehelp/GcmBroadcastReceiver;->a(Landroid/content/Intent;)Z

    .line 292
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 284
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 289
    :catchall_0
    move-exception v0

    invoke-static {p1}, Lcom/google/android/gms/googlehelp/GcmBroadcastReceiver;->a(Landroid/content/Intent;)Z

    .line 292
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0
.end method

.method protected static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 105
    invoke-static {p1}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_CANCEL_HANGOUT_REQUEST"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 107
    return-void
.end method

.method private c(Landroid/content/Intent;)Landroid/util/Pair;
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 297
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    if-nez v0, :cond_0

    .line 299
    invoke-static {p1}, Lcom/google/android/gms/googlehelp/GcmBroadcastReceiver;->a(Landroid/content/Intent;)Z

    .line 300
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 310
    :goto_0
    return-object v0

    .line 305
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 308
    invoke-static {p1}, Lcom/google/android/gms/googlehelp/GcmBroadcastReceiver;->a(Landroid/content/Intent;)Z

    .line 310
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 308
    :catchall_0
    move-exception v0

    invoke-static {p1}, Lcom/google/android/gms/googlehelp/GcmBroadcastReceiver;->a(Landroid/content/Intent;)Z

    .line 310
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0
.end method

.method private f()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 476
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 478
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 479
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 485
    :goto_0
    return-object v0

    .line 483
    :catch_0
    move-exception v0

    iget v0, p0, Lcom/google/android/gms/googlehelp/service/a;->g:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->a(I)V

    .line 485
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()Landroid/app/Notification;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 490
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->c(Lcom/google/android/gms/googlehelp/common/HelpConfig;)I

    move-result v0

    .line 491
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    sget v0, Lcom/google/android/gms/p;->oL:I

    new-array v1, v5, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/googlehelp/service/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 496
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/service/a;->t()Landroid/support/v4/app/bk;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/bk;->e(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->oK:I

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/service/a;->j:Ljava/lang/CharSequence;

    aput-object v3, v2, v5

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/googlehelp/service/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/service/a;->u()Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    sget v1, Lcom/google/android/gms/h;->bT:I

    sget v2, Lcom/google/android/gms/p;->dD:I

    new-array v3, v5, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/googlehelp/service/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/service/a;->r()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/bk;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/bk;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v6}, Landroid/support/v4/app/bk;->a(IZ)V

    iput v5, v0, Landroid/support/v4/app/bk;->j:I

    invoke-virtual {v0}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v0

    return-object v0

    .line 491
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/n;->p:I

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private r()Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 538
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_CANCEL_HANGOUT_REQUEST"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 542
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->j()I

    move-result v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private s()Landroid/app/Notification;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 548
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/service/a;->t()Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->m()I

    move-result v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/googlehelp/service/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->m()I

    move-result v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/googlehelp/service/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bk;->e(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->oK:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/service/a;->j:Ljava/lang/CharSequence;

    aput-object v3, v2, v4

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/googlehelp/service/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/service/a;->u()Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bk;->b(I)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bk;->a()Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method private t()Landroid/support/v4/app/bk;
    .locals 2

    .prologue
    .line 560
    new-instance v0, Landroid/support/v4/app/bk;

    invoke-direct {v0, p0}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    iput v1, v0, Landroid/support/v4/app/bk;->z:I

    sget v1, Lcom/google/android/gms/h;->aS:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v0

    return-object v0
.end method

.method private u()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 575
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->j()I

    move-result v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gms/googlehelp/helpactivities/OpenHelpActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "EXTRA_HELP_CONFIG"

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x10000000

    invoke-static {p0, v0, v1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 329
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/service/a;->h:Z

    if-nez v0, :cond_0

    .line 347
    :goto_0
    return-void

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->c(Lcom/google/android/gms/googlehelp/common/HelpConfig;)I

    move-result v0

    .line 336
    if-eqz v0, :cond_1

    .line 338
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/service/a;->a:Landroid/app/NotificationManager;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->i()I

    move-result v2

    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/service/a;->q()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 342
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->p()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "EXTRA_IS_HANGOUT_SUPPORT_STATUS_UPDATE"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "EXTRA_HANGOUT_SUPPORT_QUEUE_POSITION"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 346
    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method abstract a(JILjava/lang/String;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
.end method

.method protected final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->b:Landroid/support/v4/a/m;

    if-nez v0, :cond_0

    invoke-static {p0}, Landroid/support/v4/a/m;->a(Landroid/content/Context;)Landroid/support/v4/a/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->b:Landroid/support/v4/a/m;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->b:Landroid/support/v4/a/m;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/m;->a(Landroid/content/Intent;)Z

    .line 465
    return-void
.end method

.method abstract a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
.end method

.method public final a(Lcom/google/android/gms/googlehelp/common/a;)V
    .locals 1

    .prologue
    .line 321
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/service/a;->d:Lcom/google/android/gms/googlehelp/common/a;

    .line 322
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->e:Lcom/google/android/gms/googlehelp/c/a/n;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->e:Lcom/google/android/gms/googlehelp/c/a/n;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/googlehelp/c/a/n;->a(Lcom/google/android/gms/googlehelp/common/a;)V

    .line 325
    :cond_0
    return-void
.end method

.method abstract b(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 351
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/service/a;->h:Z

    .line 353
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->o()V

    .line 355
    iget v0, p0, Lcom/google/android/gms/googlehelp/service/a;->g:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->stopSelf(I)V

    .line 356
    return-void
.end method

.method abstract c(Lcom/google/android/gms/googlehelp/common/HelpConfig;)I
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 380
    iput-boolean v2, p0, Lcom/google/android/gms/googlehelp/service/a;->h:Z

    .line 383
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->d:Lcom/google/android/gms/googlehelp/common/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->d:Lcom/google/android/gms/googlehelp/common/a;

    const-string v1, "hangout_was_opened"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->a:Landroid/app/NotificationManager;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->h()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/service/a;->s()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 387
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->p()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_IS_HANGOUT_DATA_STALE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 394
    :goto_0
    if-eqz v0, :cond_1

    .line 395
    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->a(Landroid/content/Intent;)V

    .line 398
    :cond_1
    iget v0, p0, Lcom/google/android/gms/googlehelp/service/a;->g:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->stopSelf(I)V

    .line 399
    return-void

    .line 390
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->p()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_IS_HANGOUT_ENDED"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method abstract d(Lcom/google/android/gms/googlehelp/common/HelpConfig;)J
.end method

.method public d()V
    .locals 12

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 403
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/service/a;->a:Landroid/app/NotificationManager;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->i()I

    move-result v4

    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/service/a;->r()Landroid/app/PendingIntent;

    move-result-object v5

    const/4 v6, 0x2

    invoke-static {v6}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/googlehelp/service/a;->d:Lcom/google/android/gms/googlehelp/common/a;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/gms/googlehelp/service/a;->d:Lcom/google/android/gms/googlehelp/common/a;

    const-string v8, "hangout_was_opened"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_1

    :goto_0
    new-instance v7, Landroid/support/v4/app/bk;

    invoke-direct {v7, p0}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->k()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->l()I

    move-result v8

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-direct {p0, v8, v9}, Lcom/google/android/gms/googlehelp/service/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->l()I

    move-result v8

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-direct {p0, v8, v9}, Lcom/google/android/gms/googlehelp/service/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/support/v4/app/bk;->e(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v7

    sget v8, Lcom/google/android/gms/p;->oK:I

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/gms/googlehelp/service/a;->j:Ljava/lang/CharSequence;

    aput-object v11, v9, v10

    invoke-direct {p0, v8, v9}, Lcom/google/android/gms/googlehelp/service/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->j()I

    move-result v8

    iget-object v9, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p0, v9}, Lcom/google/android/gms/googlehelp/service/a;->h(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Landroid/content/Intent;

    move-result-object v9

    const/high16 v10, 0x10000000

    invoke-static {p0, v8, v9, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    iput-object v8, v7, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    sget v8, Lcom/google/android/gms/h;->bT:I

    sget v9, Lcom/google/android/gms/p;->dD:I

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-direct {p0, v9, v10}, Lcom/google/android/gms/googlehelp/service/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9, v5}, Landroid/support/v4/app/bk;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/bk;

    move-result-object v5

    iget-boolean v7, p0, Lcom/google/android/gms/googlehelp/service/a;->f:Z

    const/16 v8, 0x8

    invoke-virtual {v5, v8, v7}, Landroid/support/v4/app/bk;->a(IZ)V

    const/4 v7, 0x2

    invoke-virtual {v5, v7}, Landroid/support/v4/app/bk;->b(I)Landroid/support/v4/app/bk;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/support/v4/app/bk;->a(Landroid/net/Uri;)Landroid/support/v4/app/bk;

    move-result-object v5

    const/4 v6, 0x2

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/support/v4/app/bk;->a(IZ)V

    if-eqz v2, :cond_2

    :goto_1
    iput v0, v5, Landroid/support/v4/app/bk;->j:I

    invoke-virtual {v5}, Landroid/support/v4/app/bk;->a()Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 407
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/service/a;->f:Z

    .line 408
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->f(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 414
    if-eqz v0, :cond_0

    .line 415
    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->a(Landroid/content/Intent;)V

    .line 417
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v2, v0

    .line 403
    goto/16 :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 410
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->b()V

    goto :goto_2
.end method

.method abstract e(Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 440
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/service/a;->h:Z

    return v0
.end method

.method abstract f(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Landroid/content/Intent;
.end method

.method abstract g()Ljava/lang/String;
.end method

.method abstract g(Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
.end method

.method abstract h()I
.end method

.method abstract h(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Landroid/content/Intent;
.end method

.method abstract i()I
.end method

.method abstract j()I
.end method

.method abstract k()I
.end method

.method abstract l()I
.end method

.method abstract m()I
.end method

.method abstract n()Ljava/lang/String;
.end method

.method protected o()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 360
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->d:Lcom/google/android/gms/googlehelp/common/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->d:Lcom/google/android/gms/googlehelp/common/a;

    const-string v1, "hangout_was_opened"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 363
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->a:Landroid/app/NotificationManager;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->h()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/service/a;->s()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 365
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->p()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_IS_HANGOUT_SUPPORT_UNAVAILABLE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 373
    :goto_0
    if-eqz v0, :cond_1

    .line 374
    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->a(Landroid/content/Intent;)V

    .line 376
    :cond_1
    return-void

    .line 369
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->p()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_IS_HANGOUT_ENDED"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 591
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 177
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 178
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->a:Landroid/app/NotificationManager;

    .line 179
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    if-eqz v0, :cond_1

    .line 451
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/service/a;->i:Z

    if-eqz v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->g(Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    .line 454
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->e(Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    .line 456
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->stopForeground(Z)V

    .line 457
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 183
    iput p3, p0, Lcom/google/android/gms/googlehelp/service/a;->g:I

    .line 185
    if-nez p1, :cond_0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    .line 186
    :goto_0
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 187
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 209
    :goto_1
    return v0

    .line 185
    :cond_0
    const-string v0, "EXTRA_CANCEL_HANGOUT_REQUEST"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p3}, Lcom/google/android/gms/googlehelp/service/a;->a(I)V

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_1
    const-string v0, "EXTRA_NOTIFY_IF_UPDATING"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/service/a;->h:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->c(Lcom/google/android/gms/googlehelp/common/HelpConfig;)I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->d()V

    :goto_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->a()V

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->p()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_NO_PENDING_REQUEST"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->a(Landroid/content/Intent;)V

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_4
    const-string v0, "EXTRA_IS_GCM_STATUS_UPDATE"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0, p1}, Lcom/google/android/gms/googlehelp/service/a;->b(Landroid/content/Intent;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0

    :cond_5
    const-string v0, "EXTRA_IS_GCM_SUPPORT_UNAVAILABLE_UPDATE"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0, p1}, Lcom/google/android/gms/googlehelp/service/a;->c(Landroid/content/Intent;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0

    :cond_6
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0

    .line 190
    :cond_7
    const-string v0, "EXTRA_HELP_CONFIG"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->b(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 193
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempting to start a HangoutStatusUpdateService with no pool ID provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {p0, p0, v0}, Lcom/google/android/gms/googlehelp/common/a;->a(Lcom/google/android/gms/googlehelp/common/b;Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    .line 199
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/service/a;->f()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->j:Ljava/lang/CharSequence;

    .line 201
    iput-boolean v2, p0, Lcom/google/android/gms/googlehelp/service/a;->h:Z

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->a:Landroid/app/NotificationManager;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->h()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 204
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->i()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/service/a;->q()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/googlehelp/service/a;->startForeground(ILandroid/app/Notification;)V

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/a;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    move v0, v2

    .line 209
    goto/16 :goto_1
.end method

.method protected final p()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 460
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/service/a;->n()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
