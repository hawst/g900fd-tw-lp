.class final Lcom/google/android/gms/wallet/common/ui/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/af;->a:Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/af;->a:Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/af;->a:Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->f()Z

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/af;->a:Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->a(Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;)Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/af;->a:Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->a(Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;)Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemSelectedListener;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 184
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/af;->a:Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->b(Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/af;->a:Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->c(Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;)I

    move-result v0

    if-eq p3, v0, :cond_2

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/af;->a:Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    .line 186
    if-eqz v0, :cond_2

    .line 187
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 190
    :cond_2
    return-void
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/af;->a:Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/af;->a:Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->f()Z

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/af;->a:Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->a(Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;)Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 201
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/af;->a:Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->a(Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;)Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/widget/AdapterView$OnItemSelectedListener;->onNothingSelected(Landroid/widget/AdapterView;)V

    .line 203
    :cond_1
    return-void
.end method
