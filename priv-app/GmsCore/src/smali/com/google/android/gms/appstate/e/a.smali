.class public final Lcom/google/android/gms/appstate/e/a;
.super Landroid/support/v4/app/m;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;


# instance fields
.field private j:Landroid/widget/Spinner;

.field private k:[Landroid/accounts/Account;

.field private l:I

.field private m:Landroid/widget/TextView;

.field private n:Lcom/google/android/gms/common/api/v;

.field private o:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/gms/appstate/e/a;->o:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/appstate/e/a;->o:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 166
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/appstate/e/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 167
    if-eqz v0, :cond_1

    .line 168
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v0, Lcom/google/android/gms/p;->s:I

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 172
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 174
    :cond_1
    return-void
.end method

.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/google/android/gms/appstate/e/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    .line 56
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 58
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 63
    :goto_0
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 64
    sget v1, Lcom/google/android/gms/l;->W:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 65
    sget v0, Lcom/google/android/gms/j;->i:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/gms/appstate/e/a;->j:Landroid/widget/Spinner;

    .line 66
    sget v0, Lcom/google/android/gms/j;->eg:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/appstate/e/a;->m:Landroid/widget/TextView;

    .line 67
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->r:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 61
    :cond_0
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v3, 0x103000b

    invoke-direct {v0, v1, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/gms/appstate/e/a;->n:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/appstate/e/a;->n:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 179
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/appstate/e/a;->n:Lcom/google/android/gms/common/api/v;

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/appstate/e/a;->o:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/appstate/e/a;->o:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/appstate/e/a;->o:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 184
    :cond_1
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/appstate/e/a;->n:Lcom/google/android/gms/common/api/v;

    invoke-static {v0}, Lcom/google/android/gms/appstate/a;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/appstate/e/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/appstate/e/b;-><init>(Lcom/google/android/gms/appstate/e/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 155
    return-void
.end method

.method public final f_(I)V
    .locals 0

    .prologue
    .line 159
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 134
    packed-switch p2, :pswitch_data_0

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 136
    :pswitch_0
    iget-object v0, p0, Landroid/support/v4/app/m;->f:Landroid/app/Dialog;

    if-ne p1, v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/appstate/e/a;->n:Lcom/google/android/gms/common/api/v;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/appstate/e/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/appstate/e/a;->o:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/google/android/gms/appstate/e/a;->o:Landroid/app/ProgressDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v1, p0, Lcom/google/android/gms/appstate/e/a;->o:Landroid/app/ProgressDialog;

    sget v2, Lcom/google/android/gms/p;->t:I

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setTitle(I)V

    iget-object v1, p0, Lcom/google/android/gms/appstate/e/a;->o:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    new-instance v1, Lcom/google/android/gms/common/api/w;

    invoke-direct {v1, v0, p0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V

    sget-object v0, Lcom/google/android/gms/appstate/a;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/appstate/a;->b:Lcom/google/android/gms/common/api/Scope;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appstate/e/a;->k:[Landroid/accounts/Account;

    iget v2, p0, Lcom/google/android/gms/appstate/e/a;->l:I

    aget-object v1, v1, v2

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/appstate/e/a;->n:Lcom/google/android/gms/common/api/v;

    iget-object v0, p0, Lcom/google/android/gms/appstate/e/a;->n:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 138
    :cond_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    .line 142
    :pswitch_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    goto :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/gms/appstate/e/a;->j:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/appstate/e/a;->l:I

    .line 189
    return-void
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 193
    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 97
    invoke-super {p0}, Landroid/support/v4/app/m;->onResume()V

    .line 99
    invoke-virtual {p0}, Lcom/google/android/gms/appstate/e/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 100
    const-string v2, "com.google"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/appstate/e/a;->k:[Landroid/accounts/Account;

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/appstate/e/a;->k:[Landroid/accounts/Account;

    array-length v0, v0

    new-array v2, v0, [Ljava/lang/String;

    move v0, v1

    .line 102
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 103
    iget-object v3, p0, Lcom/google/android/gms/appstate/e/a;->k:[Landroid/accounts/Account;

    aget-object v3, v3, v0

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v2, v0

    .line 102
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 106
    :cond_0
    iget v0, p0, Lcom/google/android/gms/appstate/e/a;->l:I

    iget-object v3, p0, Lcom/google/android/gms/appstate/e/a;->k:[Landroid/accounts/Account;

    array-length v3, v3

    if-lt v0, v3, :cond_1

    .line 107
    iput v1, p0, Lcom/google/android/gms/appstate/e/a;->l:I

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/appstate/e/a;->k:[Landroid/accounts/Account;

    array-length v0, v0

    if-le v0, v5, :cond_2

    .line 110
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/gms/appstate/e/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    const v4, 0x1090008

    invoke-direct {v0, v3, v4, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 114
    const v2, 0x1090009

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 115
    iget-object v2, p0, Lcom/google/android/gms/appstate/e/a;->j:Landroid/widget/Spinner;

    invoke-virtual {v2, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 116
    iget-object v2, p0, Lcom/google/android/gms/appstate/e/a;->j:Landroid/widget/Spinner;

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/appstate/e/a;->j:Landroid/widget/Spinner;

    iget v2, p0, Lcom/google/android/gms/appstate/e/a;->l:I

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/appstate/e/a;->j:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 121
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/appstate/e/a;->k:[Landroid/accounts/Account;

    array-length v0, v0

    if-ne v0, v5, :cond_3

    .line 122
    invoke-virtual {p0}, Lcom/google/android/gms/appstate/e/a;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/p;->q:I

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/appstate/e/a;->k:[Landroid/accounts/Account;

    aget-object v4, v4, v1

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v3, v1

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 129
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/appstate/e/a;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    return-void

    .line 126
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/appstate/e/a;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->p:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
