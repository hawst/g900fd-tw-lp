.class public Lcom/google/android/gms/drive/api/a/s;
.super Lcom/google/android/gms/drive/api/a/d;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/internal/GetDriveIdFromUniqueIdentifierRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/GetDriveIdFromUniqueIdentifierRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/gms/drive/api/a/d;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;B)V

    .line 30
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/s;->e:Lcom/google/android/gms/drive/internal/GetDriveIdFromUniqueIdentifierRequest;

    .line 31
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/s;->e:Lcom/google/android/gms/drive/internal/GetDriveIdFromUniqueIdentifierRequest;

    const-string v1, "Invalid request."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/s;->e:Lcom/google/android/gms/drive/internal/GetDriveIdFromUniqueIdentifierRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/GetDriveIdFromUniqueIdentifierRequest;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Invalid request: null unique identifier provided."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/s;->e:Lcom/google/android/gms/drive/internal/GetDriveIdFromUniqueIdentifierRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/GetDriveIdFromUniqueIdentifierRequest;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Invalid request: empty unique identifier provided."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(ZLjava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/s;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/s;->e:Lcom/google/android/gms/drive/internal/GetDriveIdFromUniqueIdentifierRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/GetDriveIdFromUniqueIdentifierRequest;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/s;->e:Lcom/google/android/gms/drive/internal/GetDriveIdFromUniqueIdentifierRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/GetDriveIdFromUniqueIdentifierRequest;->b()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/api/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    .line 46
    if-nez v0, :cond_1

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/s;->b:Lcom/google/android/gms/drive/internal/ca;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x5de

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 51
    :goto_1
    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 49
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/s;->b:Lcom/google/android/gms/drive/internal/ca;

    new-instance v2, Lcom/google/android/gms/drive/internal/OnDriveIdResponse;

    invoke-direct {v2, v0}, Lcom/google/android/gms/drive/internal/OnDriveIdResponse;-><init>(Lcom/google/android/gms/drive/DriveId;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/drive/internal/OnDriveIdResponse;)V

    goto :goto_1
.end method
