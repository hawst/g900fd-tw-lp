.class public final Lcom/google/android/gms/wallet/shared/service/ServerResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

.field public static final b:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

.field public static final c:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

.field public static final d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;


# instance fields
.field final e:I

.field f:I

.field g:[B

.field h:Ljava/lang/String;

.field private i:Ljava/lang/Class;

.field private j:Lcom/google/protobuf/nano/j;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 57
    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/4 v1, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V

    sput-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    .line 60
    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/4 v1, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V

    sput-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    .line 63
    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v1, 0x16

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V

    sput-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->c:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    .line 66
    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V

    sput-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    .line 86
    new-instance v0, Lcom/google/android/gms/wallet/shared/service/a;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/shared/service/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->e:I

    .line 105
    iput v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    .line 106
    return-void
.end method

.method constructor <init>(II[BLjava/lang/String;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput p1, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->e:I

    .line 95
    iput p2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    .line 96
    iput-object p3, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->g:[B

    .line 97
    iput-object p4, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->h:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public constructor <init>(ILcom/google/protobuf/nano/j;)V
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->e:I

    .line 118
    iput p1, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    .line 119
    iput-object p2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->j:Lcom/google/protobuf/nano/j;

    .line 120
    if-eqz p2, :cond_0

    .line 121
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    .line 123
    :cond_0
    return-void
.end method

.method public constructor <init>(I[BLjava/lang/Class;)V
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->e:I

    .line 111
    iput p1, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    .line 112
    iput-object p2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->g:[B

    .line 113
    iput-object p3, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    .line 114
    return-void
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 165
    packed-switch p0, :pswitch_data_0

    .line 191
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 189
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 165
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private c()V
    .locals 4

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->j:Lcom/google/protobuf/nano/j;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    .line 213
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    if-eqz v0, :cond_2

    .line 215
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->g:[B

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/nano/j;

    iget-object v1, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->g:[B

    const/4 v2, 0x0

    array-length v3, v1

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/a;->a([BII)Lcom/google/protobuf/nano/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->j:Lcom/google/protobuf/nano/j;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 221
    :catch_0
    move-exception v0

    const-string v0, "ServerResponse"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to parse a known parcelable proto "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :goto_1
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    goto :goto_0

    .line 223
    :catch_1
    move-exception v0

    const-string v0, "ServerResponse"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to parse a known parcelable proto "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 225
    :catch_2
    move-exception v0

    const-string v0, "ServerResponse"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to parse a known parcelable proto "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 228
    :cond_2
    const-string v0, "ServerResponse"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown proto class type for responseType="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private d()Ljava/lang/Class;
    .locals 3

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    .line 252
    :goto_0
    return-object v0

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 240
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->h:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/protobuf/nano/j;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 244
    :catch_0
    move-exception v0

    const-string v0, "ServerResponse"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a proto"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 246
    :catch_1
    move-exception v0

    const-string v0, "ServerResponse"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " class not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 249
    :cond_1
    const-string v0, "ServerResponse"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No proto class instance and unknown proto class name for responseType="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 155
    invoke-direct {p0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->c()V

    .line 156
    iget v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    return v0
.end method

.method public final b()Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 204
    invoke-direct {p0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->c()V

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->j:Lcom/google/protobuf/nano/j;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 257
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->j:Lcom/google/protobuf/nano/j;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->g:[B

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->f:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->j:Lcom/google/protobuf/nano/j;

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->g:[B

    .line 263
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->i:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->h:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->h:Ljava/lang/String;

    .line 264
    invoke-static {p0, p1}, Lcom/google/android/gms/wallet/shared/service/a;->a(Lcom/google/android/gms/wallet/shared/service/ServerResponse;Landroid/os/Parcel;)V

    .line 265
    return-void

    .line 262
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->g:[B

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->j:Lcom/google/protobuf/nano/j;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->g:[B

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->j:Lcom/google/protobuf/nano/j;

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
