.class public Lcom/google/android/gms/ads/settings/AdsSettingsActivity;
.super Lcom/google/android/gms/common/widget/a/b;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Lcom/google/android/gms/common/widget/a/p;


# instance fields
.field private a:Lcom/google/android/gms/ads/settings/c/b;

.field private b:Lcom/google/android/gms/ads/settings/c/i;

.field private c:Lcom/google/android/gms/common/widget/a/o;

.field private d:Lcom/google/android/gms/common/widget/a/r;

.field private e:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/gms/common/widget/a/b;-><init>()V

    .line 158
    return-void
.end method

.method private a(Lcom/google/android/gms/common/widget/a/o;II)Lcom/google/android/gms/common/widget/a/o;
    .locals 0

    .prologue
    .line 320
    invoke-virtual {p1, p3}, Lcom/google/android/gms/common/widget/a/o;->b(I)V

    .line 321
    invoke-virtual {p1, p3}, Lcom/google/android/gms/common/widget/a/o;->c(I)V

    .line 322
    invoke-virtual {p1, p2}, Lcom/google/android/gms/common/widget/a/o;->a(I)V

    .line 323
    invoke-virtual {p1, p0}, Lcom/google/android/gms/common/widget/a/o;->a(Lcom/google/android/gms/common/widget/a/p;)V

    .line 324
    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;)Lcom/google/android/gms/common/widget/a/r;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->d:Lcom/google/android/gms/common/widget/a/r;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;Lcom/google/android/gms/common/ev;)V
    .locals 3

    .prologue
    .line 45
    invoke-virtual {p1}, Lcom/google/android/gms/common/ev;->a()I

    move-result v0

    const/4 v1, 0x1

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/common/ew;->a(ILandroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    invoke-static {v0, p0}, Lcom/google/android/gms/common/ez;->a(Landroid/app/Dialog;Landroid/content/DialogInterface$OnCancelListener;)Lcom/google/android/gms/common/ez;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "error_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/ez;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->c:Lcom/google/android/gms/common/widget/a/o;

    new-instance v1, Ljava/lang/StringBuilder;

    sget v2, Lcom/google/android/gms/p;->l:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/o;->b(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;Z)V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ad_settings_cache_lat"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 414
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "?vv=5"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/google/android/gms/ads/settings/e/a;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "&sig="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 415
    invoke-static {p0, v1}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 419
    const-class v0, Lcom/google/android/gms/common/activity/WebViewActivity;

    invoke-virtual {v1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 420
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 424
    :goto_0
    return-void

    .line 422
    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/widget/a/l;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 200
    iget-object v1, p1, Lcom/google/android/gms/common/widget/a/l;->c:Lcom/google/android/gms/common/widget/a/i;

    .line 202
    new-instance v0, Lcom/google/android/gms/common/widget/a/r;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/widget/a/r;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/gms/p;->g:I

    invoke-direct {p0, v0, v4, v2}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/o;II)Lcom/google/android/gms/common/widget/a/o;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/widget/a/r;

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->d:Lcom/google/android/gms/common/widget/a/r;

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->d:Lcom/google/android/gms/common/widget/a/r;

    sget v2, Lcom/google/android/gms/p;->h:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/widget/a/r;->d(I)V

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->d:Lcom/google/android/gms/common/widget/a/r;

    iget-object v2, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->e:Landroid/content/SharedPreferences;

    const-string v3, "ad_settings_cache_lat"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/widget/a/r;->setChecked(Z)V

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->d:Lcom/google/android/gms/common/widget/a/r;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/widget/a/i;->b(Lcom/google/android/gms/common/widget/a/e;)V

    .line 208
    new-instance v0, Lcom/google/android/gms/common/widget/a/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/widget/a/o;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x1

    sget v3, Lcom/google/android/gms/p;->i:I

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/o;II)Lcom/google/android/gms/common/widget/a/o;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/widget/a/i;->c(Lcom/google/android/gms/common/widget/a/e;)V

    .line 210
    new-instance v0, Lcom/google/android/gms/common/widget/a/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/widget/a/o;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x2

    sget v3, Lcom/google/android/gms/p;->f:I

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/o;II)Lcom/google/android/gms/common/widget/a/o;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/widget/a/i;->c(Lcom/google/android/gms/common/widget/a/e;)V

    .line 212
    new-instance v0, Lcom/google/android/gms/common/widget/a/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/widget/a/o;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->c:Lcom/google/android/gms/common/widget/a/o;

    .line 213
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->c:Lcom/google/android/gms/common/widget/a/o;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/widget/a/o;->a(I)V

    .line 214
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->c:Lcom/google/android/gms/common/widget/a/o;

    sget v2, Lcom/google/android/gms/p;->l:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/widget/a/o;->d(I)V

    .line 215
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->c:Lcom/google/android/gms/common/widget/a/o;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/widget/a/i;->b(Lcom/google/android/gms/common/widget/a/e;)V

    .line 216
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 302
    invoke-virtual {p0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->finish()V

    .line 303
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 293
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 294
    new-instance v0, Lcom/google/android/gms/ads/settings/a;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/ads/settings/a;-><init>(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;B)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/settings/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->b:Lcom/google/android/gms/ads/settings/c/i;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/c/i;->f()V

    .line 296
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;Lcom/google/android/gms/common/widget/a/o;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v5, 0x0

    .line 268
    invoke-virtual {p2}, Lcom/google/android/gms/common/widget/a/o;->g()I

    move-result v1

    .line 269
    sget v2, Lcom/google/android/gms/p;->g:I

    if-ne v2, v1, :cond_2

    .line 270
    iget-object v1, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->d:Lcom/google/android/gms/common/widget/a/r;

    invoke-virtual {v1}, Lcom/google/android/gms/common/widget/a/r;->toggle()V

    .line 271
    iget-object v1, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->d:Lcom/google/android/gms/common/widget/a/r;

    invoke-virtual {v1}, Lcom/google/android/gms/common/widget/a/r;->isChecked()Z

    move-result v1

    new-instance v2, Lcom/google/android/gms/ads/settings/c;

    invoke-direct {v2, p0, v5}, Lcom/google/android/gms/ads/settings/c;-><init>(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;B)V

    new-array v3, v0, [Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lcom/google/android/gms/ads/settings/c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v2, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->a:Lcom/google/android/gms/ads/settings/c/b;

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    :cond_0
    invoke-virtual {v2, v0}, Lcom/google/android/gms/ads/settings/c/b;->b(I)V

    .line 278
    :cond_1
    :goto_0
    return-void

    .line 272
    :cond_2
    sget v0, Lcom/google/android/gms/p;->i:I

    if-ne v0, v1, :cond_3

    .line 273
    new-instance v0, Lcom/google/android/gms/ads/settings/d/a;

    invoke-direct {v0}, Lcom/google/android/gms/ads/settings/d/a;-><init>()V

    .line 274
    invoke-virtual {p0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "reset_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/ads/settings/d/a;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    goto :goto_0

    .line 275
    :cond_3
    sget v0, Lcom/google/android/gms/p;->f:I

    if-ne v0, v1, :cond_1

    .line 276
    sget-object v0, Lcom/google/android/gms/ads/settings/a/b;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 220
    new-instance v0, Lcom/google/android/gms/ads/settings/c/b;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/settings/c/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->a:Lcom/google/android/gms/ads/settings/c/b;

    .line 221
    new-instance v0, Lcom/google/android/gms/ads/settings/c/i;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/settings/c/i;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->b:Lcom/google/android/gms/ads/settings/c/i;

    .line 222
    const-string v0, "ad_settings_cache"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->e:Landroid/content/SharedPreferences;

    .line 223
    invoke-static {p0}, Lcom/google/android/gms/common/a/d;->a(Landroid/content/Context;)V

    .line 225
    invoke-super {p0, p1}, Lcom/google/android/gms/common/widget/a/b;->onCreate(Landroid/os/Bundle;)V

    .line 227
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    .line 228
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 229
    invoke-virtual {v0, v2}, Landroid/support/v7/app/a;->a(Z)V

    .line 230
    invoke-virtual {v0}, Landroid/support/v7/app/a;->f()V

    .line 237
    :goto_0
    return-void

    .line 232
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 248
    invoke-virtual {p0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 249
    sget v1, Lcom/google/android/gms/m;->a:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 250
    invoke-super {p0, p1}, Lcom/google/android/gms/common/widget/a/b;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 282
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 283
    sget v1, Lcom/google/android/gms/j;->jL:I

    if-ne v0, v1, :cond_0

    .line 284
    sget-object v0, Lcom/google/android/gms/ads/settings/a/b;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->a(Ljava/lang/String;)V

    .line 285
    const/4 v0, 0x1

    .line 287
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/common/widget/a/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 255
    invoke-super {p0}, Lcom/google/android/gms/common/widget/a/b;->onResume()V

    .line 256
    new-instance v0, Lcom/google/android/gms/ads/settings/b;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/ads/settings/b;-><init>(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;B)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/settings/b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 257
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 241
    invoke-super {p0}, Lcom/google/android/gms/common/widget/a/b;->onStart()V

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->a:Lcom/google/android/gms/ads/settings/c/b;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/c/b;->a()V

    .line 243
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->b:Lcom/google/android/gms/ads/settings/c/i;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/c/i;->a()V

    .line 244
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 261
    invoke-super {p0}, Lcom/google/android/gms/common/widget/a/b;->onStop()V

    .line 262
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->a:Lcom/google/android/gms/ads/settings/c/b;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/c/b;->b()V

    .line 263
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->b:Lcom/google/android/gms/ads/settings/c/i;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/c/i;->b()V

    .line 264
    return-void
.end method
