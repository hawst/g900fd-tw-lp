.class public abstract Lcom/google/android/gms/games/ui/bk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/br;


# instance fields
.field protected final a:Lcom/google/android/gms/games/ui/o;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/o;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/android/gms/games/ui/bk;->a:Lcom/google/android/gms/games/ui/o;

    .line 18
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/gms/games/ui/bk;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;ILcom/google/android/gms/common/images/g;)V

    .line 33
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;ILcom/google/android/gms/common/images/g;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bk;->a:Lcom/google/android/gms/games/ui/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/bk;->a:Lcom/google/android/gms/games/ui/o;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/o;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 51
    :cond_0
    if-eqz p4, :cond_1

    .line 52
    invoke-virtual {p1, p4}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Lcom/google/android/gms/common/images/g;)V

    .line 54
    :cond_1
    invoke-virtual {p1, p2, p3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a()V

    goto :goto_0
.end method
