.class public final Lcom/google/android/gms/auth/login/ShowErrorActivity;
.super Lcom/google/android/gms/auth/login/g;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/setupwizard/navigationbar/a;


# instance fields
.field private A:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

.field q:I

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Lcom/google/android/gms/auth/firstparty/shared/k;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/widget/Button;

.field private z:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/g;-><init>()V

    .line 122
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->q:I

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/k;ZZZ)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 80
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/login/ShowErrorActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "isCreatingAccount"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "detail"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "accountName"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "status"

    invoke-virtual {p2}, Lcom/google/android/gms/auth/firstparty/shared/k;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "isAddingAccount"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "allowSkip"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 135
    const-string v0, "accountName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->r:Ljava/lang/String;

    .line 136
    const-string v0, "isCreatingAccount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->t:Z

    .line 137
    const-string v0, "isAddingAccount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->u:Z

    .line 138
    const-string v0, "allowSkip"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->v:Z

    .line 139
    const-string v0, "detail"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->s:Ljava/lang/String;

    .line 141
    const-string v0, "status"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/a/h;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->w:Lcom/google/android/gms/auth/firstparty/shared/k;

    .line 142
    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 449
    if-eqz p1, :cond_0

    .line 453
    sget v0, Lcom/google/android/gms/j;->sg:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 454
    sget v1, Lcom/google/android/gms/p;->cp:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 455
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 461
    :cond_0
    :goto_0
    return-void

    .line 457
    :cond_1
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 458
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 358
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 361
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 367
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 368
    if-eqz v2, :cond_0

    if-ne v2, v1, :cond_1

    :cond_0
    move v0, v1

    .line 371
    :cond_1
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private l()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 388
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->n:Z

    if-nez v0, :cond_2

    .line 394
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 395
    const-string v0, "com.google.android.apps.enterprise.dmagent"

    const v2, 0x8000

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 398
    const-string v0, "com.google.android.apps.enterprise.dmagent"

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    .line 400
    const-string v0, "com.google.android.apps.enterprise.dmagent"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 403
    :cond_0
    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.google.android.apps.enterprise.dmagent"

    const-string v3, "com.google.android.apps.enterprise.dmagent.DMAgentActivity"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Landroid/support/v4/a/e;->a(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 405
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 406
    const-string v0, "Couldn\'t find activity %s attempting to enable %s"

    .line 407
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "com.google.android.apps.enterprise.dmagent.DMAgentActivity"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "com.google.android.apps.enterprise.dmagent"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 408
    const-string v1, "GLSActivity"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    const-string v0, "com.google.android.apps.enterprise.dmagent"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/ay;->c(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 424
    :cond_1
    :goto_0
    :try_start_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 430
    :cond_2
    :goto_1
    invoke-virtual {p0, v4}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setResult(I)V

    .line 431
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->finish()V

    .line 432
    return-void

    .line 420
    :catch_0
    move-exception v0

    const-string v0, "GLSActivity"

    const-string v1, "Couldn\'t find package com.google.android.apps.enterprise.dmagent"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    const-string v0, "com.google.android.apps.enterprise.dmagent"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/ay;->c(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 427
    :catch_1
    move-exception v0

    const-string v0, "GLSActivity"

    const-string v1, "Market not found for dmagent"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 528
    return-void
.end method

.method public final a(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2

    .prologue
    .line 521
    iput-object p1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->A:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    .line 522
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->o:Z

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->o:Z

    invoke-virtual {p1, v0, v1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->a(ZZ)V

    .line 523
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 533
    return-void
.end method

.method protected final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 509
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/auth/login/g;->onActivityResult(IILandroid/content/Intent;)V

    .line 510
    packed-switch p1, :pswitch_data_0

    .line 515
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/auth/login/g;->onActivityResult(IILandroid/content/Intent;)V

    .line 517
    :goto_0
    return-void

    .line 512
    :pswitch_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setResult(I)V

    .line 513
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->finish()V

    goto :goto_0

    .line 510
    :pswitch_data_0
    .packed-switch 0x3fd
        :pswitch_0
    .end packed-switch
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 465
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->w:Lcom/google/android/gms/auth/firstparty/shared/k;

    .line 466
    sget-object v1, Lcom/google/android/gms/auth/firstparty/shared/k;->a:Lcom/google/android/gms/auth/firstparty/shared/k;

    iput-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->w:Lcom/google/android/gms/auth/firstparty/shared/k;

    .line 468
    sget-object v1, Lcom/google/android/gms/auth/login/az;->a:[I

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/shared/k;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 492
    :pswitch_0
    const-string v1, "GLSActivity"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 493
    const-string v1, "GLSActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Unhandled status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/shared/k;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->z:Landroid/widget/TextView;

    if-ne p1, v0, :cond_1

    .line 500
    invoke-virtual {p0, v4}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setResult(I)V

    .line 504
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->finish()V

    .line 505
    :goto_1
    return-void

    .line 477
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->l()V

    goto :goto_1

    .line 480
    :pswitch_2
    invoke-virtual {p0, v4}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setResult(I)V

    .line 481
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->finish()V

    goto :goto_1

    .line 484
    :pswitch_3
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->z:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    .line 485
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setResult(I)V

    .line 486
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->finish()V

    goto :goto_1

    .line 502
    :cond_1
    iget v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->q:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setResult(I)V

    goto :goto_0

    .line 468
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v5, 0x4

    const/4 v6, 0x2

    const/4 v7, 0x5

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 146
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onCreate(Landroid/os/Bundle;)V

    .line 147
    if-nez p1, :cond_4

    .line 148
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 149
    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->a(Landroid/os/Bundle;)V

    .line 154
    :goto_0
    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setResult(I)V

    .line 159
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 160
    new-instance v0, Lcom/google/android/setupwizard/util/b;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/b;-><init>(Landroid/content/Context;)V

    .line 161
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setContentView(Landroid/view/View;)V

    .line 162
    sget v3, Lcom/google/android/gms/p;->cm:I

    sget v4, Lcom/google/android/gms/l;->D:I

    invoke-virtual {v0, v3, v4}, Lcom/google/android/setupwizard/util/b;->b(II)Landroid/view/View;

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->A:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->b()Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->y:Landroid/widget/Button;

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->y:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->v:Z

    if-nez v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->y:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->A:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->a()Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->z:Landroid/widget/TextView;

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 187
    :goto_1
    sget v0, Lcom/google/android/gms/j;->fR:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->x:Landroid/widget/TextView;

    .line 189
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 191
    const-string v3, "title"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 192
    if-eqz v3, :cond_1

    .line 193
    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 196
    :cond_1
    const-string v3, "label"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 197
    if-eqz v0, :cond_2

    .line 198
    iget-object v3, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->y:Landroid/widget/Button;

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 200
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->w:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-static {v0}, Lcom/google/android/gms/auth/a/h;->a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/a/h;

    move-result-object v0

    .line 201
    const-string v3, "GLSActivity"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 202
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ShowError: %s %s %s"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 203
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->w:Lcom/google/android/gms/auth/firstparty/shared/k;

    aput-object v5, v4, v2

    iget v5, v0, Lcom/google/android/gms/auth/a/h;->f:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    iget-object v5, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->s:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 205
    const-string v4, "GLSActivity"

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :cond_3
    sget-object v3, Lcom/google/android/gms/auth/login/az;->a:[I

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->w:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/firstparty/shared/k;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 320
    :goto_2
    iget v1, v0, Lcom/google/android/gms/auth/a/h;->f:I

    if-eqz v1, :cond_12

    .line 322
    iget-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->x:Landroid/widget/TextView;

    iget v0, v0, Lcom/google/android/gms/auth/a/h;->f:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 336
    :goto_3
    return-void

    .line 151
    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->a(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 177
    :cond_5
    sget v0, Lcom/google/android/gms/l;->C:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setContentView(I)V

    .line 179
    sget v0, Lcom/google/android/gms/j;->mw:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->y:Landroid/widget/Button;

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->y:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    sget v0, Lcom/google/android/gms/j;->rM:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->z:Landroid/widget/TextView;

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 218
    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->n:Z

    if-nez v0, :cond_6

    const-string v0, "com.google.android.apps.enterprise.dmagent"

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "com.google.android.apps.enterprise.dmagent"

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->l()V

    goto :goto_3

    :cond_7
    sget v0, Lcom/google/android/gms/p;->bu:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->y:Landroid/widget/Button;

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    sget v0, Lcom/google/android/gms/p;->bt:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->r:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->x:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 222
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 223
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->z:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->as:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 224
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->x:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->bj:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 225
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->q:I

    goto/16 :goto_3

    .line 229
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 230
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->z:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->as:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 231
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->q:I

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->s:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->x:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->z:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->as:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_3

    .line 237
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->finish()V

    goto/16 :goto_3

    .line 242
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->x:Landroid/widget/TextView;

    sget v3, Lcom/google/android/gms/p;->bf:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    new-array v1, v1, [Ljava/lang/CharSequence;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->r:Ljava/lang/String;

    aput-object v4, v1, v2

    invoke-static {v3, v1}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    iput v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->q:I

    goto/16 :goto_3

    .line 248
    :pswitch_4
    sget v0, Lcom/google/android/gms/p;->cm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->y:Landroid/widget/Button;

    sget v3, Lcom/google/android/gms/p;->dn:I

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(I)V

    .line 251
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->u:Z

    if-eqz v0, :cond_9

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->x:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->bi:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 253
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->z:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->co:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 260
    :goto_4
    iput v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->q:I

    goto/16 :goto_3

    .line 255
    :cond_9
    iget-object v3, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->x:Landroid/widget/TextView;

    sget v0, Lcom/google/android/gms/p;->bD:I

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v4, "https://www.google.com/accounts/recovery/?hl=%s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-static {v4, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Landroid/text/SpannableString;

    invoke-direct {v4, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v4}, Landroid/text/SpannableString;->length()I

    move-result v0

    const-class v5, Landroid/text/Annotation;

    invoke-virtual {v4, v2, v0, v5}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/Annotation;

    aget-object v0, v0, v2

    invoke-virtual {v4, v0}, Landroid/text/SpannableString;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {v4, v0}, Landroid/text/SpannableString;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v4, v0}, Landroid/text/SpannableString;->removeSpan(Ljava/lang/Object;)V

    new-instance v0, Lcom/google/android/gms/auth/common/ux/URLSpanNoUnderline;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/common/ux/URLSpanNoUnderline;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v5, v6, v2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->x:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 258
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->z:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->az:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_4

    .line 264
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->x:Landroid/widget/TextView;

    sget v3, Lcom/google/android/gms/p;->bm:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    new-array v1, v1, [Ljava/lang/CharSequence;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->r:Ljava/lang/String;

    aput-object v4, v1, v2

    invoke-static {v3, v1}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    sget v0, Lcom/google/android/gms/p;->bn:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->a(Ljava/lang/CharSequence;)V

    .line 268
    iput v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->q:I

    goto/16 :goto_3

    .line 275
    :pswitch_6
    sget-object v0, Lcom/google/android/gms/auth/login/m;->p:Lcom/google/android/gms/auth/login/n;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/login/n;->a:Z

    if-nez v0, :cond_a

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/m;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    :goto_5
    if-nez v0, :cond_b

    move v0, v1

    .line 276
    :goto_6
    if-eqz v0, :cond_d

    .line 279
    const v0, 0x320ce

    const/4 v3, 0x0

    invoke-static {v0, v3}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 280
    iget-object v3, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->x:Landroid/widget/TextView;

    invoke-static {}, Lcom/google/android/gms/auth/a/b;->c()Z

    move-result v0

    if-eqz v0, :cond_c

    sget v0, Lcom/google/android/gms/p;->bN:I

    :goto_7
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 283
    sget v0, Lcom/google/android/gms/p;->bL:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->a(Ljava/lang/CharSequence;)V

    .line 296
    :goto_8
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->n:Z

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->A:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    if-eqz v0, :cond_10

    .line 297
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 298
    iput v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->q:I

    goto/16 :goto_3

    :cond_a
    move v0, v2

    .line 275
    goto :goto_5

    :cond_b
    move v0, v2

    goto :goto_6

    .line 280
    :cond_c
    sget v0, Lcom/google/android/gms/p;->bM:I

    goto :goto_7

    .line 284
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->w:Lcom/google/android/gms/auth/firstparty/shared/k;

    sget-object v3, Lcom/google/android/gms/auth/firstparty/shared/k;->m:Lcom/google/android/gms/auth/firstparty/shared/k;

    if-ne v0, v3, :cond_f

    .line 285
    const v0, 0x320cc

    const/4 v3, 0x0

    invoke-static {v0, v3}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 286
    iget-object v3, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->x:Landroid/widget/TextView;

    invoke-static {}, Lcom/google/android/gms/auth/a/b;->c()Z

    move-result v0

    if-eqz v0, :cond_e

    sget v0, Lcom/google/android/gms/p;->bI:I

    :goto_9
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 289
    sget v0, Lcom/google/android/gms/p;->bG:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->a(Ljava/lang/CharSequence;)V

    goto :goto_8

    .line 286
    :cond_e
    sget v0, Lcom/google/android/gms/p;->bH:I

    goto :goto_9

    .line 292
    :cond_f
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->x:Landroid/widget/TextView;

    sget v3, Lcom/google/android/gms/p;->cl:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 293
    sget v0, Lcom/google/android/gms/p;->ck:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->a(Ljava/lang/CharSequence;)V

    goto :goto_8

    .line 300
    :cond_10
    iput v2, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->q:I

    goto/16 :goto_3

    .line 306
    :pswitch_7
    sget v0, Lcom/google/android/gms/p;->bU:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 307
    iget-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->x:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->t:Z

    if-eqz v0, :cond_11

    sget v0, Lcom/google/android/gms/p;->bS:I

    :goto_a
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_3

    :cond_11
    sget v0, Lcom/google/android/gms/p;->bT:I

    goto :goto_a

    .line 316
    :pswitch_8
    iput v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->q:I

    goto/16 :goto_2

    .line 324
    :cond_12
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->s:Ljava/lang/String;

    if-nez v0, :cond_13

    .line 326
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->x:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->cl:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 327
    sget v0, Lcom/google/android/gms/p;->ck:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->a(Ljava/lang/CharSequence;)V

    .line 328
    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GAIA ERROR WITH NO RESOURCE STRING "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->w:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/shared/k;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 332
    :cond_13
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->x:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 209
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 126
    const-string v0, "accountName"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string v0, "isCreatingAccount"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->t:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 128
    const-string v0, "isAddingAccount"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 129
    const-string v0, "allowSkip"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 130
    const-string v0, "detail"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const-string v0, "status"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->w:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/firstparty/shared/k;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    return-void
.end method

.method public final setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 377
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 378
    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 382
    :goto_0
    return-void

    .line 380
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
