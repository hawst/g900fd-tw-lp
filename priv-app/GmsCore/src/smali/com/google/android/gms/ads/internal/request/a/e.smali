.class final Lcom/google/android/gms/ads/internal/request/a/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

.field final synthetic c:Lcom/google/android/gms/ads/internal/request/a/k;

.field final synthetic d:Lcom/google/android/gms/ads/internal/p/e;

.field final synthetic e:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Lcom/google/android/gms/ads/internal/request/a/k;Lcom/google/android/gms/ads/internal/p/e;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/a/e;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/ads/internal/request/a/e;->b:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iput-object p3, p0, Lcom/google/android/gms/ads/internal/request/a/e;->c:Lcom/google/android/gms/ads/internal/request/a/k;

    iput-object p4, p0, Lcom/google/android/gms/ads/internal/request/a/e;->d:Lcom/google/android/gms/ads/internal/p/e;

    iput-object p5, p0, Lcom/google/android/gms/ads/internal/request/a/e;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/a/e;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    invoke-direct {v1}, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;-><init>()V

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/request/a/e;->b:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v5, v3, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->k:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    move v3, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/p/a;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;ZZLcom/google/android/a/w;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)Lcom/google/android/gms/ads/internal/p/a;

    move-result-object v0

    .line 192
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/p/a;->setWillNotDraw(Z)V

    .line 196
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/a/e;->c:Lcom/google/android/gms/ads/internal/request/a/k;

    const-string v2, "setAdWebView must be called on the main thread."

    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    iput-object v0, v1, Lcom/google/android/gms/ads/internal/request/a/k;->e:Lcom/google/android/gms/ads/internal/p/a;

    .line 199
    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->f()Lcom/google/android/gms/ads/internal/p/c;

    move-result-object v1

    .line 200
    const-string v2, "/invalidRequest"

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/request/a/e;->c:Lcom/google/android/gms/ads/internal/request/a/k;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/request/a/k;->g:Lcom/google/android/gms/ads/internal/g/m;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/ads/internal/p/c;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 202
    const-string v2, "/loadAdURL"

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/request/a/e;->c:Lcom/google/android/gms/ads/internal/request/a/k;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/request/a/k;->h:Lcom/google/android/gms/ads/internal/g/m;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/ads/internal/p/c;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 204
    const-string v2, "/log"

    sget-object v3, Lcom/google/android/gms/ads/internal/g/c;->h:Lcom/google/android/gms/ads/internal/g/m;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/ads/internal/p/c;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 206
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/request/a/e;->d:Lcom/google/android/gms/ads/internal/p/e;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/internal/p/c;->a(Lcom/google/android/gms/ads/internal/p/e;)V

    .line 208
    const-string v1, "Loading the JS library."

    invoke-static {v1}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 209
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/a/e;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/p/a;->loadUrl(Ljava/lang/String;)V

    .line 210
    return-void
.end method
