.class public Lcom/google/android/gms/feedback/Screenshot;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 227
    new-instance v0, Lcom/google/android/gms/feedback/ah;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/ah;-><init>()V

    sput-object v0, Lcom/google/android/gms/feedback/Screenshot;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([BZ)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 86
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 87
    iput-boolean v0, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 88
    array-length v1, p0

    invoke-static {p0, v6, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 89
    iget v1, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    div-int/lit8 v1, v1, 0x2

    .line 91
    iget v3, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    div-int/lit8 v3, v3, 0x2

    .line 93
    if-eqz p1, :cond_1

    iget v4, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-gt v4, v3, :cond_0

    if-le v5, v1, :cond_1

    :cond_0
    int-to-float v0, v4

    int-to-float v3, v3

    div-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v3, v5

    int-to-float v1, v1

    div-float v1, v3, v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    if-le v0, v1, :cond_2

    :cond_1
    :goto_0
    iput v0, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 95
    iput-boolean v6, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 96
    array-length v0, p0

    invoke-static {p0, v6, v0, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v1

    .line 93
    goto :goto_0
.end method

.method public static a(Ljava/io/File;Ljava/lang/String;Lcom/google/android/gms/feedback/Screenshot;Z)Landroid/os/AsyncTask;
    .locals 6

    .prologue
    .line 188
    new-instance v0, Ljava/io/File;

    const-string v1, "reports"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 189
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".bmp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 190
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->canRead()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->canWrite()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    new-instance v0, Lcom/google/android/gms/feedback/ag;

    move v2, p3

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/feedback/ag;-><init>(Ljava/io/File;ZLjava/io/File;Ljava/lang/String;Lcom/google/android/gms/feedback/Screenshot;)V

    .line 209
    invoke-static {v0}, Lcom/google/android/gms/feedback/w;->a(Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    move-result-object v0

    .line 212
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/feedback/Screenshot;
    .locals 3

    .prologue
    .line 56
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    :cond_0
    const-string v0, "Gfeedback_screenshot"

    const-string v1, "Screenshot is either null or recycled"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    const/4 v0, 0x0

    .line 62
    :goto_0
    return-object v0

    .line 60
    :cond_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 61
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    invoke-virtual {p0, v1, v2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 62
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/feedback/Screenshot;->a([BII)Lcom/google/android/gms/feedback/Screenshot;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/os/Parcel;)Lcom/google/android/gms/feedback/Screenshot;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 66
    new-instance v0, Lcom/google/android/gms/feedback/Screenshot;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/Screenshot;-><init>()V

    .line 67
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/feedback/Screenshot;->c:Ljava/lang/String;

    .line 69
    :try_start_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/google/android/gms/feedback/Screenshot;->a:I

    .line 70
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/google/android/gms/feedback/Screenshot;->b:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :goto_0
    return-object v0

    .line 72
    :catch_0
    move-exception v1

    iput v2, v0, Lcom/google/android/gms/feedback/Screenshot;->a:I

    .line 73
    iput v2, v0, Lcom/google/android/gms/feedback/Screenshot;->b:I

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;II)Lcom/google/android/gms/feedback/Screenshot;
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/gms/feedback/Screenshot;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/Screenshot;-><init>()V

    .line 49
    iput p1, v0, Lcom/google/android/gms/feedback/Screenshot;->a:I

    .line 50
    iput p2, v0, Lcom/google/android/gms/feedback/Screenshot;->b:I

    .line 51
    iput-object p0, v0, Lcom/google/android/gms/feedback/Screenshot;->c:Ljava/lang/String;

    .line 52
    return-object v0
.end method

.method public static a([BII)Lcom/google/android/gms/feedback/Screenshot;
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/feedback/Screenshot;->a(Ljava/lang/String;II)Lcom/google/android/gms/feedback/Screenshot;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/File;Landroid/graphics/Bitmap;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 121
    new-instance v0, Ljava/io/File;

    const-string v2, "reports"

    invoke-direct {v0, p0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 122
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 123
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    .line 124
    const-string v0, "Gfeedback_screenshot"

    const-string v2, "failed to create reports directory"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 178
    :goto_0
    return-object v0

    .line 128
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 129
    :cond_1
    const-string v0, "Gfeedback_screenshot"

    const-string v2, "Cannot save. Bitmap is null or recycled."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 130
    goto :goto_0

    .line 132
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 133
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".bmp"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 136
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    array-length v5, v4

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v5, :cond_5

    aget-object v6, v4, v0

    .line 137
    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".bmp"

    invoke-virtual {v7, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 139
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 141
    :cond_3
    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    move-object v0, v2

    .line 143
    goto :goto_0

    .line 136
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 146
    :cond_5
    new-instance v0, Lcom/google/android/gms/feedback/af;

    invoke-direct {v0, v3, p1}, Lcom/google/android/gms/feedback/af;-><init>(Ljava/io/File;Landroid/graphics/Bitmap;)V

    .line 172
    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/feedback/w;->a(Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    move-result-object v3

    sget-object v0, Lcom/google/android/gms/feedback/a/a;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5, v0}, Landroid/os/AsyncTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v0, v2

    .line 178
    goto/16 :goto_0

    .line 174
    :catch_0
    move-exception v0

    .line 175
    :goto_2
    const-string v2, "Gfeedback_screenshot"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception while saving screenshot: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 176
    goto/16 :goto_0

    .line 174
    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/feedback/Screenshot;)Lcom/google/android/gms/feedback/Screenshot;
    .locals 1

    .prologue
    .line 34
    if-nez p1, :cond_0

    .line 35
    const/4 p0, 0x0

    .line 40
    :goto_0
    return-object p0

    .line 37
    :cond_0
    iget v0, p1, Lcom/google/android/gms/feedback/Screenshot;->a:I

    iput v0, p0, Lcom/google/android/gms/feedback/Screenshot;->a:I

    .line 38
    iget v0, p1, Lcom/google/android/gms/feedback/Screenshot;->b:I

    iput v0, p0, Lcom/google/android/gms/feedback/Screenshot;->b:I

    .line 39
    iget-object v0, p1, Lcom/google/android/gms/feedback/Screenshot;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/feedback/Screenshot;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/gms/feedback/Screenshot;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 223
    iget v0, p0, Lcom/google/android/gms/feedback/Screenshot;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 224
    iget v0, p0, Lcom/google/android/gms/feedback/Screenshot;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 225
    return-void
.end method
