.class final Lcom/google/android/gms/plus/internal/bh;
.super Lcom/google/android/gms/plus/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/internal/ay;

.field private final b:Lcom/google/android/gms/plus/internal/ah;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/ah;)V
    .locals 0

    .prologue
    .line 1007
    iput-object p1, p0, Lcom/google/android/gms/plus/internal/bh;->a:Lcom/google/android/gms/plus/internal/ay;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/b;-><init>()V

    .line 1008
    iput-object p2, p0, Lcom/google/android/gms/plus/internal/bh;->b:Lcom/google/android/gms/plus/internal/ah;

    .line 1009
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1013
    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->g()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->g()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "pendingIntent"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 1017
    :goto_0
    new-instance v3, Lcom/google/android/gms/common/c;

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v2

    invoke-direct {v3, v2, v0}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V

    .line 1018
    invoke-virtual {v3}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p1, :cond_2

    .line 1019
    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1020
    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_0
    move-object v4, v1

    .line 1024
    :goto_1
    iget-object v6, p0, Lcom/google/android/gms/plus/internal/bh;->a:Lcom/google/android/gms/plus/internal/ay;

    new-instance v0, Lcom/google/android/gms/plus/internal/bi;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/bh;->a:Lcom/google/android/gms/plus/internal/ay;

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/bh;->b:Lcom/google/android/gms/plus/internal/ah;

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/internal/bi;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/ah;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/plus/internal/ay;->a(Lcom/google/android/gms/common/internal/al;)V

    .line 1025
    return-void

    :cond_1
    move-object v0, v1

    .line 1013
    goto :goto_0

    :cond_2
    move-object v4, p1

    goto :goto_1
.end method
