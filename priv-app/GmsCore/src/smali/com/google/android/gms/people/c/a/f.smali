.class public final Lcom/google/android/gms/people/c/a/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final c:Ljava/util/regex/Pattern;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/people/c/f;

.field private final d:Landroid/content/ContentValues;

.field private final e:Lcom/google/android/gms/people/c/a/e;

.field private f:Z

.field private g:Z

.field private final h:Ljava/util/Set;

.field private final i:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-string v0, "[^a-zA-Z0-9\u0080-\uffff]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/c/a/f;->c:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/people/c/f;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/c/a/f;->d:Landroid/content/ContentValues;

    .line 74
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/c/a/f;->h:Ljava/util/Set;

    .line 77
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/people/c/a/f;->i:[Ljava/lang/String;

    .line 82
    iput-object p2, p0, Lcom/google/android/gms/people/c/a/f;->a:Landroid/content/Context;

    .line 83
    iput-object p1, p0, Lcom/google/android/gms/people/c/a/f;->b:Lcom/google/android/gms/people/c/f;

    .line 84
    new-instance v0, Lcom/google/android/gms/people/c/a/e;

    iget-object v1, p0, Lcom/google/android/gms/people/c/a/f;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/people/c/a/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/c/a/f;)V

    iput-object v0, p0, Lcom/google/android/gms/people/c/a/f;->e:Lcom/google/android/gms/people/c/a/e;

    .line 85
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/c/a/f;->a(Ljava/util/Locale;)V

    .line 86
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/gms/people/c/a/f;->b:Lcom/google/android/gms/people/c/f;

    const-string v1, "searchIndexVersion"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    return-void
.end method

.method private a(Lcom/google/android/gms/people/c/e;)V
    .locals 10

    .prologue
    const/16 v9, 0x32

    const/4 v6, 0x0

    .line 377
    const-string v0, "PeopleSearchIndexManage"

    const-string v1, "Rebuilding index..."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    iget-object v0, p0, Lcom/google/android/gms/people/c/a/f;->a:Landroid/content/Context;

    const-string v1, "PeopleSearchIndexManage"

    const-string v2, "Rebuilding index..."

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    iget-object v0, p1, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 385
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/c/a/f;->b:Lcom/google/android/gms/people/c/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/people/c/f;->a(Lcom/google/android/gms/people/c/e;)V

    .line 389
    const-string v0, "people"

    sget-object v1, Lcom/google/android/gms/people/c/a/g;->a:[Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 396
    :goto_0
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    move v7, v6

    .line 399
    :goto_1
    if-ge v7, v9, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v4, v0

    .line 401
    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 403
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    .line 404
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/c/a/f;->a(Lcom/google/android/gms/people/c/e;Ljava/lang/String;Ljava/lang/String;J)V

    .line 405
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    .line 409
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/people/c/e;->b()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 412
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0

    .line 421
    :catch_0
    move-exception v0

    .line 422
    iget-object v1, p0, Lcom/google/android/gms/people/c/a/f;->a:Landroid/content/Context;

    const-string v2, "PeopleSearchIndexManage"

    const-string v3, "Error rebuilding search index."

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 424
    const-string v1, "PeopleSearchIndexManage"

    const-string v2, "Error rebuilding search index."

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 426
    :goto_2
    return-void

    .line 412
    :cond_1
    :try_start_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 416
    iget-object v1, p0, Lcom/google/android/gms/people/c/a/f;->e:Lcom/google/android/gms/people/c/a/e;

    invoke-virtual {p1}, Lcom/google/android/gms/people/c/e;->d()V

    const-string v0, "PeopleASM"

    const-string v2, "Rebuilding autocomplete index..."

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "SELECT _id FROM ac_people"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v2

    move v0, v6

    :cond_2
    :goto_3
    :try_start_4
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v1, p1, v4, v5}, Lcom/google/android/gms/people/c/a/e;->a(Lcom/google/android/gms/people/c/e;J)V

    add-int/lit8 v0, v0, 0x1

    if-le v0, v9, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/people/c/e;->b()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move v0, v6

    goto :goto_3

    :cond_3
    :try_start_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 418
    iget-object v0, p0, Lcom/google/android/gms/people/c/a/f;->a:Landroid/content/Context;

    const-string v1, "PeopleSearchIndexManage"

    const-string v2, "Rebuilding index done."

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    const-string v0, "PeopleSearchIndexManage"

    const-string v1, "Rebuilding index done."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 416
    :catchall_1
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_5
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_5} :catch_0
.end method

.method private a(Lcom/google/android/gms/people/c/e;JILjava/lang/String;)V
    .locals 4

    .prologue
    .line 364
    iget-object v0, p0, Lcom/google/android/gms/people/c/a/f;->d:Landroid/content/ContentValues;

    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 365
    iget-object v0, p0, Lcom/google/android/gms/people/c/a/f;->d:Landroid/content/ContentValues;

    const-string v1, "person_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 366
    iget-object v0, p0, Lcom/google/android/gms/people/c/a/f;->d:Landroid/content/ContentValues;

    const-string v1, "kind"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 367
    iget-object v0, p0, Lcom/google/android/gms/people/c/a/f;->d:Landroid/content/ContentValues;

    const-string v1, "value"

    invoke-virtual {v0, v1, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    const-string v0, "search_index"

    iget-object v1, p0, Lcom/google/android/gms/people/c/a/f;->d:Landroid/content/ContentValues;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 369
    return-void
.end method

.method private a(Lcom/google/android/gms/people/c/e;JLjava/util/Set;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 579
    invoke-interface {p4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, p5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v4

    :goto_0
    if-nez v0, :cond_1

    .line 580
    invoke-interface {p4, p5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 581
    invoke-static {p5}, Lcom/google/android/gms/people/c/a/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/c/a/f;->a(Lcom/google/android/gms/people/c/e;JILjava/lang/String;)V

    .line 583
    :cond_1
    return-void

    .line 579
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/people/c/e;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 14

    .prologue
    .line 332
    const-string v0, "SELECT name as data, given_name as data2,family_name as data3,middle_name as data4,1 as kind FROM people WHERE owner_id=?1 AND qualified_id=?2 AND name IS NOT NULL UNION SELECT phone as data, NULL as data2,NULL as data3,NULL as data4,3 as kind FROM phones WHERE owner_id=?1 AND qualified_id=?2 UNION SELECT email as data,NULL as data2,NULL as data3,NULL as data4,2 as kind FROM emails WHERE owner_id=?1 AND qualified_id=?2;"

    invoke-static/range {p2 .. p3}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 340
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 341
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 342
    const/4 v1, 0x4

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 343
    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 344
    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 345
    const/4 v2, 0x2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 346
    const/4 v3, 0x3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 347
    iget-object v4, p0, Lcom/google/android/gms/people/c/a/f;->i:[Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    .line 348
    iget-object v1, p0, Lcom/google/android/gms/people/c/a/f;->i:[Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    .line 349
    iget-object v1, p0, Lcom/google/android/gms/people/c/a/f;->i:[Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v3, v1, v2

    .line 350
    iget-object v10, p0, Lcom/google/android/gms/people/c/a/f;->i:[Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v10, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/people/c/a/f;->h:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    sget-object v1, Lcom/google/android/gms/people/c/a/f;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget-object v4, p0, Lcom/google/android/gms/people/c/a/f;->h:Ljava/util/Set;

    move-object v0, p0

    move-object v1, p1

    move-wide/from16 v2, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/c/a/f;->a(Lcom/google/android/gms/people/c/e;JLjava/util/Set;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v5, v0}, Lcom/google/android/gms/people/c/a/f;->a(Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    if-lez v1, :cond_1

    iget-object v4, p0, Lcom/google/android/gms/people/c/a/f;->h:Ljava/util/Set;

    const-string v1, ""

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-wide/from16 v2, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/c/a/f;->a(Lcom/google/android/gms/people/c/e;JLjava/util/Set;Ljava/lang/String;)V

    :cond_1
    array-length v11, v10

    const/4 v0, 0x0

    move v8, v0

    :goto_1
    if-ge v8, v11, :cond_0

    aget-object v0, v10, v8

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x0

    invoke-direct {p0, v5, v0}, Lcom/google/android/gms/people/c/a/f;->a(Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object v6

    array-length v0, v6

    if-lez v0, :cond_3

    iget-object v4, p0, Lcom/google/android/gms/people/c/a/f;->h:Ljava/util/Set;

    move-object v0, p0

    move-object v1, p1

    move-wide/from16 v2, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/c/a/f;->a(Lcom/google/android/gms/people/c/e;JLjava/util/Set;Ljava/lang/String;)V

    move-object v7, v6

    :goto_2
    array-length v0, v7

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    iget-object v4, p0, Lcom/google/android/gms/people/c/a/f;->h:Ljava/util/Set;

    const-string v0, ""

    invoke-static {v0, v7}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-wide/from16 v2, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/c/a/f;->a(Lcom/google/android/gms/people/c/e;JLjava/util/Set;Ljava/lang/String;)V

    :cond_2
    array-length v12, v7

    const/4 v0, 0x0

    move v6, v0

    :goto_3
    if-ge v6, v12, :cond_4

    aget-object v5, v7, v6

    iget-object v4, p0, Lcom/google/android/gms/people/c/a/f;->h:Ljava/util/Set;

    move-object v0, p0

    move-object v1, p1

    move-wide/from16 v2, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/c/a/f;->a(Lcom/google/android/gms/people/c/e;JLjava/util/Set;Ljava/lang/String;)V

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_3

    :cond_3
    sget-object v0, Lcom/google/android/gms/people/c/a/f;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v5}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    goto :goto_2

    :cond_4
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_1

    .line 351
    :cond_5
    const/4 v2, 0x2

    if-ne v1, v2, :cond_7

    .line 352
    invoke-static {v0}, Lcom/google/android/gms/people/c/a/f;->b(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    array-length v8, v7

    const/4 v0, 0x0

    move v6, v0

    :goto_4
    if-ge v6, v8, :cond_0

    aget-object v0, v7, v6

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v4, 0x2

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-wide/from16 v2, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/c/a/f;->a(Lcom/google/android/gms/people/c/e;JILjava/lang/String;)V

    :cond_6
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_4

    .line 353
    :cond_7
    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 354
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x2b

    if-ne v1, v2, :cond_8

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_8
    const/4 v4, 0x3

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-wide/from16 v2, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/c/a/f;->a(Lcom/google/android/gms/people/c/e;JILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 358
    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_9
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 359
    return-void
.end method

.method public static a(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lcom/google/android/gms/people/internal/at;->f:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Z)[Ljava/lang/String;
    .locals 12

    .prologue
    .line 544
    iget-boolean v0, p0, Lcom/google/android/gms/people/c/a/f;->f:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/people/c/a/f;->g:Z

    if-nez v0, :cond_0

    .line 545
    sget-object v0, Lcom/google/android/gms/people/internal/at;->d:[Ljava/lang/String;

    .line 562
    :goto_0
    return-object v0

    .line 548
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/people/c/a/d;->b(Ljava/lang/String;)I

    move-result v0

    .line 552
    const/4 v1, 0x2

    if-ne v0, v1, :cond_10

    iget-boolean v1, p0, Lcom/google/android/gms/people/c/a/f;->f:Z

    if-eqz v1, :cond_10

    .line 553
    iget-object v0, p0, Lcom/google/android/gms/people/c/a/f;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->c()Lcom/google/android/gms/people/c/a/b;

    move-result-object v3

    .line 554
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-boolean v0, v3, Lcom/google/android/gms/people/c/a/b;->c:Z

    if-eqz v0, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    sget-object v0, Lcom/google/android/gms/people/internal/at;->d:[Ljava/lang/String;

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {}, Lcom/google/android/gms/people/internal/at;->a()Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v1, 0x0

    move v11, v1

    move v1, v2

    move v2, v11

    :goto_1
    if-ge v2, v5, :cond_d

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Lcom/google/android/gms/people/internal/at;->a(C)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_3

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/people/c/a/b;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    :cond_3
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    const/16 v7, 0x100

    if-ge v6, v7, :cond_6

    const/4 v7, 0x1

    if-eq v1, v7, :cond_5

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_5

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/people/c/a/b;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    :cond_5
    const/4 v1, 0x1

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_6
    new-instance v7, Lcom/google/android/gms/people/c/a/c;

    invoke-direct {v7}, Lcom/google/android/gms/people/c/a/c;-><init>()V

    invoke-static {v6}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/gms/people/c/a/c;->b:Ljava/lang/String;

    const/16 v9, 0x100

    if-ge v6, v9, :cond_9

    const/4 v9, 0x1

    iput v9, v7, Lcom/google/android/gms/people/c/a/c;->a:I

    iput-object v8, v7, Lcom/google/android/gms/people/c/a/c;->c:Ljava/lang/String;

    :cond_7
    :goto_3
    iget v8, v7, Lcom/google/android/gms/people/c/a/c;->a:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_b

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_8

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/people/c/a/b;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    :cond_8
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x2

    goto :goto_2

    :cond_9
    const v9, 0x9fff

    if-lt v9, v6, :cond_a

    const/16 v9, 0x4e00

    if-lt v6, v9, :cond_a

    const/4 v8, 0x2

    iput v8, v7, Lcom/google/android/gms/people/c/a/c;->a:I

    iget-object v8, v3, Lcom/google/android/gms/people/c/a/b;->a:[Ljava/lang/String;

    iget-object v9, v3, Lcom/google/android/gms/people/c/a/b;->b:[S

    add-int/lit16 v10, v6, -0x4e00

    aget-short v9, v9, v10

    aget-object v8, v8, v9

    iput-object v8, v7, Lcom/google/android/gms/people/c/a/c;->c:Ljava/lang/String;

    iget-object v8, v7, Lcom/google/android/gms/people/c/a/c;->c:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_7

    const/4 v8, 0x3

    iput v8, v7, Lcom/google/android/gms/people/c/a/c;->a:I

    iget-object v8, v7, Lcom/google/android/gms/people/c/a/c;->b:Ljava/lang/String;

    iput-object v8, v7, Lcom/google/android/gms/people/c/a/c;->c:Ljava/lang/String;

    goto :goto_3

    :cond_a
    const/4 v9, 0x3

    iput v9, v7, Lcom/google/android/gms/people/c/a/c;->a:I

    iput-object v8, v7, Lcom/google/android/gms/people/c/a/c;->c:Ljava/lang/String;

    goto :goto_3

    :cond_b
    iget v8, v7, Lcom/google/android/gms/people/c/a/c;->a:I

    if-eq v1, v8, :cond_c

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_c

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/people/c/a/b;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    :cond_c
    iget v1, v7, Lcom/google/android/gms/people/c/a/c;->a:I

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_e

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/people/c/a/b;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    :cond_e
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    move v1, v0

    :goto_4
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_f

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/c/a/c;

    iget-object v0, v0, Lcom/google/android/gms/people/c/a/c;->c:Ljava/lang/String;

    aput-object v0, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_f
    move-object v0, v2

    goto/16 :goto_0

    .line 559
    :cond_10
    const/4 v1, 0x5

    if-ne v0, v1, :cond_11

    iget-boolean v0, p0, Lcom/google/android/gms/people/c/a/f;->g:Z

    if-eqz v0, :cond_11

    .line 560
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/google/android/gms/people/c/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    goto/16 :goto_0

    .line 562
    :cond_11
    sget-object v0, Lcom/google/android/gms/people/internal/at;->d:[Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public static b(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/google/android/gms/people/c/a/f;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/google/android/gms/people/c/a/f;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    return v0
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 138
    invoke-static {p0}, Lcom/google/android/gms/people/c/a/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/people/c/a/f;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 245
    const-string v0, "PeopleSearchIndexManage"

    const-string v1, "Marking for index update."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, Lcom/google/android/gms/people/c/a/f;->b:Lcom/google/android/gms/people/c/f;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v0

    .line 248
    iget-object v0, v0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 250
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/c/a/f;->a(I)V

    .line 251
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/gms/people/c/a/f;->e:Lcom/google/android/gms/people/c/a/e;

    iget-object v1, p0, Lcom/google/android/gms/people/c/a/f;->b:Lcom/google/android/gms/people/c/f;

    invoke-virtual {v1}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/gms/people/c/a/e;->a(Lcom/google/android/gms/people/c/e;J)V

    .line 225
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 197
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 198
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 199
    iget-object v0, p0, Lcom/google/android/gms/people/c/a/f;->b:Lcom/google/android/gms/people/c/f;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v1

    .line 200
    invoke-virtual {v1}, Lcom/google/android/gms/people/c/e;->a()V

    .line 203
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/c/a/f;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/people/c/b;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    .line 204
    const-string v0, "SELECT _id FROM people WHERE owner_id=? AND qualified_id=?"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p3}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    .line 207
    const-string v0, "search_index"

    const-string v6, "person_id=?"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v0, v6, v7}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 210
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/c/a/f;->a(Lcom/google/android/gms/people/c/e;Ljava/lang/String;Ljava/lang/String;J)V

    .line 211
    invoke-virtual {v1}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 215
    invoke-virtual {v1, v8}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 216
    return-void

    .line 212
    :catch_0
    move-exception v0

    .line 213
    :try_start_1
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Specified person doesn\'t exist."

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 215
    :catchall_0
    move-exception v0

    invoke-virtual {v1, v8}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v0
.end method

.method public final a(Ljava/util/Locale;)V
    .locals 3

    .prologue
    .line 254
    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 255
    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 258
    sget-object v2, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Ljava/util/Locale;->TAIWAN:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/people/c/a/f;->f:Z

    .line 261
    sget-object v0, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/c/a/f;->g:Z

    .line 262
    return-void

    .line 258
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v8, 0x3

    .line 270
    iget-object v1, p0, Lcom/google/android/gms/people/c/a/f;->b:Lcom/google/android/gms/people/c/f;

    invoke-virtual {v1}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v2

    iget-object v1, v2, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/gms/people/c/a/f;->b:Lcom/google/android/gms/people/c/f;

    const-string v4, "indexIcuVersion"

    const-string v5, "unknown"

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.icu.library.version"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/people/internal/at;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v8}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "PeopleSearchIndexManage"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "ICU version: old="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " new="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "ICU version changed from "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "PeopleSearchIndexManage"

    invoke-static {v3, v1}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/people/c/a/f;->a:Landroid/content/Context;

    const-string v5, "PeopleSearchIndexManage"

    invoke-static {v3, v5, v1}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    :cond_1
    if-nez v1, :cond_3

    iget-object v3, p0, Lcom/google/android/gms/people/c/a/f;->b:Lcom/google/android/gms/people/c/f;

    const-string v5, "searchIndexVersion"

    const-string v6, "0"

    invoke-virtual {v3, v5, v6}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-eq v3, v8, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "Index version changed from "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "PeopleSearchIndexManage"

    invoke-static {v3, v1}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/people/c/a/f;->a:Landroid/content/Context;

    const-string v5, "PeopleSearchIndexManage"

    invoke-static {v3, v5, v1}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    if-eqz v0, :cond_2

    invoke-direct {p0, v2}, Lcom/google/android/gms/people/c/a/f;->a(Lcom/google/android/gms/people/c/e;)V

    invoke-direct {p0, v8}, Lcom/google/android/gms/people/c/a/f;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/people/c/a/f;->b:Lcom/google/android/gms/people/c/f;

    const-string v1, "indexIcuVersion"

    invoke-virtual {v0, v1, v4}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method
