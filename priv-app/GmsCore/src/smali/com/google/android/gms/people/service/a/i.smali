.class final Lcom/google/android/gms/people/service/a/i;
.super Lcom/google/android/gms/people/service/a/g;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/google/android/gms/people/service/a/f;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/people/service/a/f;)V
    .locals 1

    .prologue
    .line 197
    iput-object p1, p0, Lcom/google/android/gms/people/service/a/i;->b:Lcom/google/android/gms/people/service/a/f;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/people/service/a/g;-><init>(Lcom/google/android/gms/people/service/a/f;B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/people/service/a/f;B)V
    .locals 0

    .prologue
    .line 197
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/service/a/i;-><init>(Lcom/google/android/gms/people/service/a/f;)V

    return-void
.end method


# virtual methods
.method final a(Landroid/os/Bundle;Lcom/google/android/gms/people/c/e;Ljava/lang/String;)V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 207
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 208
    invoke-static {v11}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleService"

    const-string v1, "People Query: SELECT O.ordinal, P.gaia_id, P.name, P.name_verified, P.tagline, P.avatar, P.profile_type FROM people AS P JOIN temp_gaia_ordinal AS O ON O.qualified_id = P.qualified_id WHERE P.blocked = 0 AND P.owner_id = ? ORDER BY O.ordinal"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v0, "SELECT O.ordinal, P.gaia_id, P.name, P.name_verified, P.tagline, P.avatar, P.profile_type FROM people AS P JOIN temp_gaia_ordinal AS O ON O.qualified_id = P.qualified_id WHERE P.blocked = 0 AND P.owner_id = ? ORDER BY O.ordinal"

    invoke-static {p3}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Landroid/database/AbstractWindowedCursor;

    const-string v1, "SELECT O.ordinal, E.email, E.type FROM emails AS E JOIN temp_gaia_ordinal AS O ON O.qualified_id = E.qualified_id WHERE E.owner_id = ? ORDER BY O.ordinal"

    invoke-static {p3}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    check-cast v1, Landroid/database/AbstractWindowedCursor;

    const-string v2, "SELECT O.ordinal, P.phone, P.type FROM phones AS P JOIN temp_gaia_ordinal AS O ON O.qualified_id = P.qualified_id WHERE P.owner_id = ? ORDER BY O.ordinal"

    invoke-static {p3}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    check-cast v2, Landroid/database/AbstractWindowedCursor;

    const-string v3, "SELECT O.ordinal, A.postal_address, A.type FROM postal_address AS A JOIN temp_gaia_ordinal AS O ON O.qualified_id = A.qualified_id WHERE A.owner_id = ? ORDER BY O.ordinal"

    invoke-static {p3}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v3, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    check-cast v3, Landroid/database/AbstractWindowedCursor;

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string v6, "people"

    iget-object v7, p0, Lcom/google/android/gms/people/service/a/i;->b:Lcom/google/android/gms/people/service/a/f;

    new-instance v8, Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v8, v0, v9, v10}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    invoke-static {v7, v8}, Lcom/google/android/gms/people/service/a/f;->b(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "people_email"

    iget-object v6, p0, Lcom/google/android/gms/people/service/a/i;->b:Lcom/google/android/gms/people/service/a/f;

    new-instance v7, Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v7, v1, v9, v10}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    invoke-static {v6, v7}, Lcom/google/android/gms/people/service/a/f;->c(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "people_phone"

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/i;->b:Lcom/google/android/gms/people/service/a/f;

    new-instance v6, Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v6, v2, v9, v10}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    invoke-static {v1, v6}, Lcom/google/android/gms/people/service/a/f;->d(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "people_address"

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/i;->b:Lcom/google/android/gms/people/service/a/f;

    new-instance v2, Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v2, v3, v9, v10}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    invoke-static {v1, v2}, Lcom/google/android/gms/people/service/a/f;->e(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 209
    invoke-static {v11}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "PeopleService"

    const-string v1, "Circle Query: SELECT O.ordinal, C.circle_id, C.name, C.people_count FROM circle_members AS CM JOIN temp_gaia_ordinal AS O ON O.qualified_id = CM.qualified_id JOIN circles AS C ON C.circle_id = CM.circle_id AND C.owner_id = CM.owner_id WHERE C.owner_id = ? ORDER BY O.ordinal"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v0, "SELECT O.ordinal, C.circle_id, C.name, C.people_count FROM circle_members AS CM JOIN temp_gaia_ordinal AS O ON O.qualified_id = CM.qualified_id JOIN circles AS C ON C.circle_id = CM.circle_id AND C.owner_id = CM.owner_id WHERE C.owner_id = ? ORDER BY O.ordinal"

    invoke-static {p3}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Landroid/database/AbstractWindowedCursor;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "circles"

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/i;->b:Lcom/google/android/gms/people/service/a/f;

    new-instance v5, Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v5, v0, v9, v10}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    invoke-static {v3, v5}, Lcom/google/android/gms/people/service/a/f;->f(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 210
    invoke-static {v11}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PeopleService"

    const-string v1, "Owner Query: SELECT O.ordinal, U.gaia_id, U.display_name, U.avatar FROM owners AS U JOIN temp_gaia_ordinal AS O ON O.gaia_id = U.gaia_id WHERE U._id = ? ORDER BY O.ordinal"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const-string v0, "SELECT O.ordinal, U.gaia_id, U.display_name, U.avatar FROM owners AS U JOIN temp_gaia_ordinal AS O ON O.gaia_id = U.gaia_id WHERE U._id = ? ORDER BY O.ordinal"

    invoke-static {p3}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Landroid/database/AbstractWindowedCursor;

    const-string v1, "SELECT O.ordinal, E.email, E.type FROM owners AS U JOIN temp_gaia_ordinal AS O ON O.qualified_id = U.gaia_id JOIN owner_emails AS E ON U._id = E.owner_id WHERE U._id = ? ORDER BY O.ordinal"

    invoke-static {p3}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    check-cast v1, Landroid/database/AbstractWindowedCursor;

    const-string v2, "SELECT O.ordinal, E.phone, E.type FROM owners AS U JOIN temp_gaia_ordinal AS O ON O.qualified_id = U.gaia_id JOIN owner_phones AS E ON U._id = E.owner_id WHERE U._id = ? ORDER BY O.ordinal"

    invoke-static {p3}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    check-cast v2, Landroid/database/AbstractWindowedCursor;

    const-string v3, "SELECT O.ordinal, E.postal_address, E.type FROM owners AS U JOIN temp_gaia_ordinal AS O ON O.qualified_id = U.gaia_id JOIN owner_postal_address AS E ON U._id = E.owner_id WHERE U._id = ? ORDER BY O.ordinal"

    invoke-static {p3}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v3, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    check-cast v3, Landroid/database/AbstractWindowedCursor;

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string v6, "owner"

    iget-object v7, p0, Lcom/google/android/gms/people/service/a/i;->b:Lcom/google/android/gms/people/service/a/f;

    new-instance v8, Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v8, v0, v9, v10}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    invoke-static {v7, v8}, Lcom/google/android/gms/people/service/a/f;->g(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "owner_email"

    iget-object v6, p0, Lcom/google/android/gms/people/service/a/i;->b:Lcom/google/android/gms/people/service/a/f;

    new-instance v7, Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v7, v1, v9, v10}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    invoke-static {v6, v7}, Lcom/google/android/gms/people/service/a/f;->h(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "owner_phone"

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/i;->b:Lcom/google/android/gms/people/service/a/f;

    new-instance v6, Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v6, v2, v9, v10}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    invoke-static {v1, v6}, Lcom/google/android/gms/people/service/a/f;->i(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "owner_address"

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/i;->b:Lcom/google/android/gms/people/service/a/f;

    new-instance v2, Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v2, v3, v9, v10}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    invoke-static {v1, v2}, Lcom/google/android/gms/people/service/a/f;->j(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 213
    const-string v0, "db"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 214
    return-void
.end method
