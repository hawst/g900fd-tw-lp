.class public final Lcom/google/android/gms/drive/ui/picker/a/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;

.field c:Lcom/google/android/gms/drive/ui/picker/a/a/r;

.field final d:Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;

.field final e:Landroid/view/View;

.field final f:Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;

.field final g:Landroid/view/View;

.field final h:Landroid/widget/ImageView;

.field final i:Landroid/widget/ImageView;

.field final j:Landroid/widget/ImageView;

.field final k:Landroid/widget/ImageView;

.field final l:Ljava/util/List;

.field final m:Z

.field n:Z

.field private final o:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/h;->a:Landroid/content/Context;

    .line 150
    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/h;->b:Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;

    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/h;->c:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    .line 152
    sget v0, Lcom/google/android/gms/j;->jr:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/h;->d:Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;

    .line 153
    sget v0, Lcom/google/android/gms/j;->js:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/h;->e:Landroid/view/View;

    .line 154
    sget v0, Lcom/google/android/gms/j;->jt:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/h;->o:Landroid/view/View;

    .line 155
    sget v0, Lcom/google/android/gms/j;->kU:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/h;->f:Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;

    .line 157
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    sget v2, Lcom/google/android/gms/j;->eC:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v0, v1

    array-length v1, v0

    if-nez v1, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/h;->l:Ljava/util/List;

    .line 160
    sget v0, Lcom/google/android/gms/j;->eD:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/h;->h:Landroid/widget/ImageView;

    .line 161
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/h;->h:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/gms/drive/ui/picker/a/i;

    invoke-direct {v1, p0}, Lcom/google/android/gms/drive/ui/picker/a/i;-><init>(Lcom/google/android/gms/drive/ui/picker/a/h;)V

    invoke-static {v0, v1}, Landroid/support/v4/view/ay;->a(Landroid/view/View;Landroid/support/v4/view/a;)V

    .line 173
    sget v0, Lcom/google/android/gms/j;->rz:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/h;->i:Landroid/widget/ImageView;

    .line 174
    sget v0, Lcom/google/android/gms/j;->mM:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/h;->j:Landroid/widget/ImageView;

    .line 175
    sget v0, Lcom/google/android/gms/j;->sa:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/h;->k:Landroid/widget/ImageView;

    .line 178
    sget v0, Lcom/google/android/gms/j;->lB:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/h;->g:Landroid/view/View;

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/h;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/g/r;->a(Landroid/content/res/Resources;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/a/h;->m:Z

    .line 181
    return-void

    .line 157
    :cond_0
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/g/z;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
