.class public final Lcom/google/android/gms/photos/autobackup/ui/promo/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/a/a/b;
.implements Lcom/google/android/libraries/social/i/ak;
.implements Lcom/google/android/libraries/social/i/o;


# instance fields
.field a:Landroid/support/v4/app/Fragment;

.field b:Landroid/app/Activity;

.field c:Ljava/lang/String;

.field d:Lcom/google/android/gms/photos/autobackup/l;

.field e:Lcom/google/android/gms/photos/autobackup/ui/ae;

.field f:Lcom/google/android/gms/photos/autobackup/ui/promo/k;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/i/w;Landroid/support/v4/app/Fragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/h;->a:Landroid/support/v4/app/Fragment;

    .line 49
    iput-object p3, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/h;->c:Ljava/lang/String;

    .line 50
    invoke-virtual {p1, p0}, Lcom/google/android/libraries/social/i/w;->a(Lcom/google/android/libraries/social/i/ak;)Lcom/google/android/libraries/social/i/ak;

    .line 51
    return-void
.end method


# virtual methods
.method final a()V
    .locals 3

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/h;->e:Lcom/google/android/gms/photos/autobackup/ui/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/ui/ae;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 97
    sget-object v1, Lcom/google/android/gms/photos/autobackup/d;->c:Lcom/google/android/gms/photos/autobackup/f;

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/h;->d:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v2}, Lcom/google/android/gms/photos/autobackup/l;->c()Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/photos/autobackup/f;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/photos/autobackup/ui/promo/i;

    invoke-direct {v1, p0}, Lcom/google/android/gms/photos/autobackup/ui/promo/i;-><init>(Lcom/google/android/gms/photos/autobackup/ui/promo/h;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 105
    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/h;->b:Landroid/app/Activity;

    .line 56
    return-void
.end method

.method public final a(Lcom/google/android/libraries/social/a/a;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 60
    const-class v0, Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/l;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/h;->d:Lcom/google/android/gms/photos/autobackup/l;

    .line 61
    const-class v0, Lcom/google/android/gms/photos/autobackup/ui/ae;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/ui/ae;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/h;->e:Lcom/google/android/gms/photos/autobackup/ui/ae;

    .line 62
    const-class v0, Lcom/google/android/gms/photos/autobackup/ui/promo/k;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/ui/promo/k;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/h;->f:Lcom/google/android/gms/photos/autobackup/ui/promo/k;

    .line 63
    return-void
.end method
