.class public final Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/h;

.field private final d:Lcom/google/android/gms/drive/b/f;

.field private final e:Ljava/util/Map;

.field private final f:Lcom/google/android/gms/drive/database/r;

.field private final g:Lcom/google/android/gms/drive/b/d;

.field private final h:Lcom/google/android/gms/drive/b/b;

.field private final i:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 67
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lcom/google/android/gms/drive/ai;->T:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%sfiles/%s?alt=media"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/h;Lcom/google/android/gms/drive/b/f;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/b/d;Lcom/google/android/gms/drive/b/b;)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->e:Ljava/util/Map;

    .line 82
    sget-object v0, Lcom/google/android/gms/drive/ai;->G:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->i:Ljava/util/concurrent/ExecutorService;

    .line 92
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->b:Landroid/content/Context;

    .line 93
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/h;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/h;

    .line 94
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/f;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->d:Lcom/google/android/gms/drive/b/f;

    .line 95
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/r;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->f:Lcom/google/android/gms/drive/database/r;

    .line 96
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/d;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->g:Lcom/google/android/gms/drive/b/d;

    .line 97
    invoke-static {p6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/b;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->h:Lcom/google/android/gms/drive/b/b;

    .line 98
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/b/l;)Ljava/net/HttpURLConnection;
    .locals 5

    .prologue
    .line 215
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 216
    const/16 v1, 0x1388

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/google/android/gms/drive/b/l;->b()Lcom/google/android/gms/drive/b/k;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/b/k;->a()J

    move-result-wide v2

    const-string v1, "Range"

    new-instance v4, Lcom/google/android/gms/drive/b/b/b;

    invoke-direct {v4, v2, v3}, Lcom/google/android/gms/drive/b/b/b;-><init>(J)V

    invoke-virtual {v4}, Lcom/google/android/gms/drive/b/b/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->h:Lcom/google/android/gms/drive/b/b;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->b:Landroid/content/Context;

    invoke-static {v0, p2, v1}, Lcom/google/android/gms/drive/b/b;->a(Ljava/net/HttpURLConnection;Lcom/google/android/gms/drive/auth/g;Landroid/content/Context;)V

    .line 217
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)V
    .locals 16

    .prologue
    .line 61
    const/4 v10, 0x5

    :try_start_0
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->a(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->f:Lcom/google/android/gms/drive/database/r;

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v2, v3, v0}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v2, "FileDownloader"

    const-string v3, "File is no longer available or permission was denied: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_7

    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->e:Ljava/util/Map;

    monitor-enter v3

    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->e:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v4, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->b(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ah;->aj()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->b(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ah;->p()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v2, 0x6

    :cond_0
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->a(I)V

    :goto_0
    return-void

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_1
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->d:Lcom/google/android/gms/drive/b/f;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/drive/b/f;->a(Lcom/google/android/gms/drive/database/model/ah;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "FileDownloader"

    const-string v3, "Up-to-date file is already available locally: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->e:Ljava/util/Map;

    monitor-enter v3

    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->e:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->a(I)V

    goto :goto_0

    :catchall_1
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_2
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/h;

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->b(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v3

    iget-object v2, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/h;->b:Landroid/support/v4/g/h;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/j;

    if-eqz v2, :cond_4

    iget-object v4, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/j;->b:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ah;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/j;->a:Lcom/google/android/gms/drive/b/l;

    :goto_1
    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->a(Ljava/lang/String;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/b/l;)Ljava/net/HttpURLConnection;

    move-result-object v2

    const-string v4, "FileDownloader"

    const-string v5, "Executing download request. URI: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_7

    :try_start_5
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v4

    const/16 v5, 0x191

    if-ne v4, v5, :cond_3

    const-string v4, "FileDownloader"

    const-string v5, "Received UNAUTHORIZED response code. Trying to connect again."

    invoke-static {v4, v5}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v4, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->a(Ljava/lang/String;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/b/l;)Ljava/net/HttpURLConnection;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-result-object v2

    :cond_3
    const/4 v11, 0x0

    :try_start_6
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v4

    const-string v5, "FileDownloader"

    const-string v6, "Processing response. Status code: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    sparse-switch v4, :sswitch_data_0

    new-instance v3, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unexpected response code: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_6
    .catch Ljava/text/ParseException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    :catch_0
    move-exception v3

    move-object v4, v11

    :goto_2
    :try_start_7
    new-instance v5, Ljava/io/IOException;

    invoke-direct {v5, v3}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :catchall_2
    move-exception v3

    move-object v11, v4

    :goto_3
    :try_start_8
    invoke-static {v11}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    throw v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :catchall_3
    move-exception v3

    move-object v15, v3

    move-object v3, v2

    move-object v2, v15

    :try_start_9
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v2
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_7

    :catch_1
    move-exception v2

    :try_start_a
    const-string v3, "FileDownloader"

    const-string v4, "Error downloading: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-static {v3, v2, v4, v5}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->e:Ljava/util/Map;

    monitor-enter v3

    :try_start_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->e:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_6

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->b(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ah;->aj()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->b(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ah;->p()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_b

    const/4 v2, 0x6

    :goto_4
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->a(I)V

    goto/16 :goto_0

    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_1

    :sswitch_0
    :try_start_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/h;

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->b(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/j;

    iget-object v6, v3, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/h;->a:Lcom/google/android/gms/drive/b/f;

    const/high16 v7, 0x20000000

    invoke-virtual {v6, v7}, Lcom/google/android/gms/drive/b/f;->a(I)Lcom/google/android/gms/drive/b/l;

    move-result-object v6

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ah;->k()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v3, v6, v7}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/j;-><init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/h;Lcom/google/android/gms/drive/b/l;Ljava/lang/String;)V

    iget-object v3, v3, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/h;->b:Landroid/support/v4/g/h;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v4

    invoke-virtual {v3, v4, v5}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, v5, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/j;->a:Lcom/google/android/gms/drive/b/l;

    const-wide/16 v8, 0x0

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v4

    int-to-long v6, v4

    move-object v12, v3

    :goto_5
    const-wide/16 v4, 0x0

    cmp-long v3, v6, v4

    if-gez v3, :cond_5

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->b(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ah;->q()J

    move-result-wide v6

    const-string v3, "FileDownloader"

    const-string v4, "Falling back to expected size from metadata: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v5, v13

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_5
    const-string v3, "FileDownloader"

    const-string v4, "Initial bytes completed: %s. Expected size: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v5, v13

    const/4 v13, 0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v5, v13

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    new-instance v3, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;

    invoke-direct/range {v3 .. v9}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/k;-><init>(Ljava/io/InputStream;Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;JJ)V
    :try_end_c
    .catch Ljava/text/ParseException; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    :try_start_d
    invoke-virtual {v12}, Lcom/google/android/gms/drive/b/l;->b()Lcom/google/android/gms/drive/b/k;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/b/k;->a()J

    move-result-wide v6

    cmp-long v5, v8, v6

    if-lez v5, :cond_8

    new-instance v4, Ljava/io/IOException;

    const-string v5, "Range response starts after requested start."

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_d
    .catch Ljava/text/ParseException; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_9

    :catch_2
    move-exception v4

    move-object v15, v4

    move-object v4, v3

    move-object v3, v15

    goto/16 :goto_2

    :sswitch_1
    if-nez v3, :cond_6

    :try_start_e
    new-instance v3, Ljava/io/IOException;

    const-string v4, "Server returned partial content but full content was requested."

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    :catchall_4
    move-exception v3

    goto/16 :goto_3

    :cond_6
    const-string v4, "Content-Range"

    invoke-virtual {v2, v4}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_7

    new-instance v3, Ljava/io/IOException;

    const-string v4, "Partial response is missing range header."

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_7
    invoke-static {v4}, Lcom/google/android/gms/drive/b/b/b;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/b/b/b;

    move-result-object v4

    iget-wide v8, v4, Lcom/google/android/gms/drive/b/b/b;->c:J

    iget-wide v6, v4, Lcom/google/android/gms/drive/b/b/b;->d:J
    :try_end_e
    .catch Ljava/text/ParseException; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    move-object v12, v3

    goto/16 :goto_5

    :cond_8
    :goto_6
    :try_start_f
    invoke-virtual {v4}, Lcom/google/android/gms/drive/b/k;->a()J

    move-result-wide v6

    cmp-long v5, v8, v6

    if-gez v5, :cond_9

    invoke-virtual {v4}, Lcom/google/android/gms/drive/b/k;->a()J

    move-result-wide v6

    sub-long/2addr v6, v8

    invoke-virtual {v3, v6, v7}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v6

    add-long/2addr v8, v6

    goto :goto_6

    :cond_9
    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Z)J

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->b(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v11

    new-instance v4, Lcom/google/android/gms/drive/b/i;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->f:Lcom/google/android/gms/drive/database/r;

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v6

    invoke-virtual {v11}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v7

    invoke-virtual {v11}, Lcom/google/android/gms/drive/database/model/ah;->k()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v11}, Lcom/google/android/gms/drive/database/model/ah;->n()Ljava/lang/String;

    move-result-object v9

    invoke-direct/range {v4 .. v9}, Lcom/google/android/gms/drive/b/i;-><init>(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v12, v4}, Lcom/google/android/gms/drive/b/l;->a(Lcom/google/android/gms/drive/b/g;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/h;

    iget-object v4, v4, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/h;->b:Landroid/support/v4/g/h;

    invoke-virtual {v11}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/g/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->g:Lcom/google/android/gms/drive/b/d;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/b/d;->b()V
    :try_end_f
    .catch Ljava/text/ParseException; {:try_start_f .. :try_end_f} :catch_2
    .catchall {:try_start_f .. :try_end_f} :catchall_9

    :try_start_10
    invoke-static {v3}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    :try_start_11
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    const-string v2, "FileDownloader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Download complete for file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_1
    .catchall {:try_start_11 .. :try_end_11} :catchall_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->e:Ljava/util/Map;

    monitor-enter v3

    :try_start_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->e:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v3
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->a(I)V

    goto/16 :goto_0

    :catchall_5
    move-exception v2

    monitor-exit v3

    throw v2

    :catchall_6
    move-exception v2

    monitor-exit v3

    throw v2

    :catchall_7
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->e:Ljava/util/Map;

    monitor-enter v3

    :try_start_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->e:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v4, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v3
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_8

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->b(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ah;->aj()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->b(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ah;->p()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_a

    const/4 v10, 0x6

    :cond_a
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;

    invoke-virtual {v3, v10}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->a(I)V

    throw v2

    :catchall_8
    move-exception v2

    monitor-exit v3

    throw v2

    :catchall_9
    move-exception v4

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_3

    :cond_b
    move v2, v10

    goto/16 :goto_4

    nop

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0xce -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;
    .locals 7

    .prologue
    .line 103
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->e:Ljava/util/Map;

    monitor-enter v1

    .line 105
    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->k()Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lcom/google/android/gms/drive/ai;->an:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "/drive/v2internal/"

    :goto_0
    sget-object v4, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->a:Ljava/lang/String;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    aput-object v2, v5, v0

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    if-eqz v3, :cond_0

    const-string v2, "revisionId"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->an()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "fileScopeAppIds"

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->C()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    invoke-static {v0}, Lcom/google/android/gms/drive/d/a/e;->a(Landroid/net/Uri$Builder;)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 106
    sget-object v0, Lcom/google/android/gms/drive/ai;->an:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object p1

    .line 108
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->d:Lcom/google/android/gms/drive/b/f;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/b/f;->a(Lcom/google/android/gms/drive/database/model/ah;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 109
    const-string v0, "FileDownloader"

    const-string v3, "Up-to-date file is already available locally: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 111
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;

    invoke-direct {v0, p0, p1, p2, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;-><init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;Ljava/lang/String;)V

    .line 112
    iget-object v2, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->a(I)V

    .line 113
    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->a()Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;

    move-result-object v0

    monitor-exit v1

    .line 137
    :goto_1
    return-object v0

    .line 105
    :cond_3
    const-string v0, "/drive/v2beta/"

    goto :goto_0

    .line 116
    :cond_4
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->am()Z

    move-result v0

    if-nez v0, :cond_5

    .line 117
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v2, 0xa

    const-string v3, "No content is available for this file."

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 123
    :cond_5
    :try_start_1
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->ak()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 124
    new-instance v0, Ljava/lang/AssertionError;

    const-string v2, "Local document not available locally. This should not happen"

    invoke-direct {v0, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 128
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->e:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;

    .line 129
    if-nez v0, :cond_7

    .line 130
    const-string v0, "FileDownloader"

    const-string v3, "Queueing new download for: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 131
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;

    invoke-direct {v0, p0, p1, p2, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;-><init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;Ljava/lang/String;)V

    .line 132
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->e:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->i:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v2, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->b:Ljava/util/concurrent/Future;

    .line 137
    :goto_2
    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->a()Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;

    move-result-object v0

    monitor-exit v1

    goto :goto_1

    .line 135
    :cond_7
    const-string v2, "FileDownloader"

    const-string v3, "Joining existing download task for: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method
