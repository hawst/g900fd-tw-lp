.class public final enum Lcom/google/android/gms/social/location/af;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/social/location/af;

.field public static final enum b:Lcom/google/android/gms/social/location/af;

.field private static final synthetic c:[Lcom/google/android/gms/social/location/af;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, Lcom/google/android/gms/social/location/af;

    const-string v1, "BEST"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/social/location/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/social/location/af;->a:Lcom/google/android/gms/social/location/af;

    new-instance v0, Lcom/google/android/gms/social/location/af;

    const-string v1, "CITY"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/social/location/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/social/location/af;->b:Lcom/google/android/gms/social/location/af;

    .line 8
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/social/location/af;

    sget-object v1, Lcom/google/android/gms/social/location/af;->a:Lcom/google/android/gms/social/location/af;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/social/location/af;->b:Lcom/google/android/gms/social/location/af;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/gms/social/location/af;->c:[Lcom/google/android/gms/social/location/af;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/social/location/af;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/google/android/gms/social/location/af;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/social/location/af;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/social/location/af;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/google/android/gms/social/location/af;->c:[Lcom/google/android/gms/social/location/af;

    invoke-virtual {v0}, [Lcom/google/android/gms/social/location/af;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/social/location/af;

    return-object v0
.end method
