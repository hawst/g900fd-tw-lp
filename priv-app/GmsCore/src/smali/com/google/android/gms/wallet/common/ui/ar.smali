.class final Lcom/google/android/gms/wallet/common/ui/ar;
.super Lcom/google/android/gms/wallet/service/l;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/an;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/common/ui/an;)V
    .locals 0

    .prologue
    .line 878
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/ar;->a:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/l;-><init>()V

    return-void
.end method

.method private a(Lcom/google/aa/b/a/a/a/a/f;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 907
    .line 908
    if-eqz p1, :cond_3

    .line 909
    iget v0, p1, Lcom/google/aa/b/a/a/a/a/f;->a:I

    .line 911
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ar;->a:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/an;->g(Lcom/google/android/gms/wallet/common/ui/an;)Ljava/util/HashSet;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/ar;->a:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-static {v3}, Lcom/google/android/gms/wallet/common/ui/an;->f(Lcom/google/android/gms/wallet/common/ui/an;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 912
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ar;->a:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/an;->h(Lcom/google/android/gms/wallet/common/ui/an;)J

    .line 915
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ar;->a:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/an;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/ar;->a:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-static {v3}, Lcom/google/android/gms/wallet/common/ui/an;->i(Lcom/google/android/gms/wallet/common/ui/an;)[I

    move-result-object v3

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/wallet/common/w;->a(Landroid/content/res/Resources;I[I)Ljava/lang/String;

    move-result-object v0

    .line 917
    if-eqz v0, :cond_0

    .line 918
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ar;->a:Lcom/google/android/gms/wallet/common/ui/an;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/ar;->a:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-static {v3}, Lcom/google/android/gms/wallet/common/ui/an;->f(Lcom/google/android/gms/wallet/common/ui/an;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 920
    :cond_0
    if-eqz p2, :cond_2

    .line 922
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ar;->a:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/an;->j(Lcom/google/android/gms/wallet/common/ui/an;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 923
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ar;->a:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/an;->l(Lcom/google/android/gms/wallet/common/ui/an;)Lcom/google/android/gms/wallet/service/l;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/wallet/common/ui/as;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/ar;->a:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-direct {v2, v3, v1}, Lcom/google/android/gms/wallet/common/ui/as;-><init>(Lcom/google/android/gms/wallet/common/ui/an;B)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ar;->a:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/an;->k(Lcom/google/android/gms/wallet/common/ui/an;)J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, Lcom/google/android/gms/wallet/service/l;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 927
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ar;->a:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/an;->j(Lcom/google/android/gms/wallet/common/ui/an;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ar;->a:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/an;->f(Lcom/google/android/gms/wallet/common/ui/an;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 931
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ar;->a:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/an;->m(Lcom/google/android/gms/wallet/common/ui/an;)Ljava/lang/String;

    .line 933
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ar;->a:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/an;->n(Lcom/google/android/gms/wallet/common/ui/an;)V

    .line 934
    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 892
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/ar;->a(Lcom/google/aa/b/a/a/a/a/f;Z)V

    .line 893
    return-void
.end method

.method public final a(Lcom/google/aa/b/a/a/a/a/f;)V
    .locals 1

    .prologue
    .line 882
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/common/ui/ar;->a(Lcom/google/aa/b/a/a/a/a/f;Z)V

    .line 883
    return-void
.end method

.method public final a(Lcom/google/checkout/b/a/c;)V
    .locals 2

    .prologue
    .line 887
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/ar;->a(Lcom/google/aa/b/a/a/a/a/f;Z)V

    .line 888
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 902
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/ar;->a(Lcom/google/aa/b/a/a/a/a/f;Z)V

    .line 903
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 897
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/ar;->a(Lcom/google/aa/b/a/a/a/a/f;Z)V

    .line 898
    return-void
.end method
