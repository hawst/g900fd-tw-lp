.class final Lcom/google/android/gms/wallet/ow/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/FullWallet;

.field final synthetic b:Lcom/google/android/gms/wallet/ow/n;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/ow/n;Lcom/google/android/gms/wallet/FullWallet;)V
    .locals 0

    .prologue
    .line 2740
    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/o;->b:Lcom/google/android/gms/wallet/ow/n;

    iput-object p2, p0, Lcom/google/android/gms/wallet/ow/o;->a:Lcom/google/android/gms/wallet/FullWallet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 2744
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2745
    const-string v1, "com.google.android.gms.wallet.EXTRA_FULL_WALLET"

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/o;->a:Lcom/google/android/gms/wallet/FullWallet;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2747
    const-string v1, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/o;->b:Lcom/google/android/gms/wallet/ow/n;

    iget-object v2, v2, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ow/g;->x(Lcom/google/android/gms/wallet/ow/g;)Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2748
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/o;->b:Lcom/google/android/gms/wallet/ow/n;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    const/4 v2, -0x1

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;ILandroid/content/Intent;)V

    .line 2749
    return-void
.end method
