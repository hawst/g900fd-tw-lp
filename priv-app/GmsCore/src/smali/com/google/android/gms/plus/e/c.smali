.class public Lcom/google/android/gms/plus/e/c;
.super Lcom/google/android/gms/common/server/n;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct/range {p0 .. p7}, Lcom/google/android/gms/common/server/n;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    .line 49
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;
    .locals 6

    .prologue
    .line 54
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/common/server/n;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    .line 55
    invoke-static {}, Lcom/google/android/gms/common/server/aa;->a()Lcom/google/android/gms/common/server/aa;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/e/c;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/plus/e/c;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/plus/b/a;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/google/android/gms/common/server/a/b;->a()Lcom/google/android/gms/common/server/a/b;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/common/server/a/b;->a(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/google/android/gms/common/server/aa;->a(Landroid/content/Context;Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/Long;)V

    .line 59
    invoke-static {}, Lcom/google/android/gms/plus/e/e;->a()Lcom/google/android/gms/plus/e/e;

    iget-object v1, p0, Lcom/google/android/gms/plus/e/c;->a:Landroid/content/Context;

    invoke-static {v1, v0, p2}, Lcom/google/android/gms/plus/e/e;->a(Landroid/content/Context;Ljava/util/HashMap;Lcom/google/android/gms/common/server/ClientContext;)V

    .line 60
    const-string v1, "User-Agent"

    const-string v2, "G+ SDK/1.0.1"

    invoke-static {p1, v2}, Lcom/google/android/gms/common/server/ab;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    return-object v0
.end method

.method protected final a(Lcom/android/volley/p;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 73
    invoke-virtual {p1}, Lcom/android/volley/p;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/android/volley/p;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/c/a;->a(ILjava/lang/String;)I

    move-result v3

    .line 74
    new-instance v0, Lcom/google/android/gms/common/server/b;

    iget-object v1, p0, Lcom/google/android/gms/plus/e/c;->a:Landroid/content/Context;

    const/4 v4, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/server/b;-><init>(Landroid/content/Context;Ljava/lang/String;IIF)V

    invoke-virtual {p1, v0}, Lcom/android/volley/p;->a(Lcom/android/volley/z;)Lcom/android/volley/p;

    .line 76
    return-void
.end method

.method protected final c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/a/a;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Lcom/google/android/gms/common/server/a/a;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/common/server/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    return-object v0
.end method
