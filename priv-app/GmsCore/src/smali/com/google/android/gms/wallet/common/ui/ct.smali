.class public final Lcom/google/android/gms/wallet/common/ui/ct;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/widget/Filterable;


# instance fields
.field a:Ljava/util/ArrayList;

.field b:Ljava/lang/CharSequence;

.field private final c:Landroid/content/Context;

.field private final d:I

.field private final e:Ljava/util/List;

.field private final f:Landroid/view/LayoutInflater;

.field private g:Lcom/google/android/gms/wallet/common/ui/cu;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 2

    .prologue
    .line 55
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 56
    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "phoneNumberSources are required"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 59
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/ct;->c:Landroid/content/Context;

    .line 60
    iput p2, p0, Lcom/google/android/gms/wallet/common/ui/ct;->d:I

    .line 61
    iput-object p3, p0, Lcom/google/android/gms/wallet/common/ui/ct;->e:Ljava/util/List;

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ct;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ct;->f:Landroid/view/LayoutInflater;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ct;->a:Ljava/util/ArrayList;

    .line 65
    return-void

    .line 56
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 101
    if-nez p2, :cond_0

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ct;->f:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/ct;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 105
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/ct;->a(I)Lcom/google/android/gms/wallet/common/a/w;

    move-result-object v1

    .line 107
    sget v0, Lcom/google/android/gms/j;->dt:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 108
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/a/w;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 109
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ct;->c:Landroid/content/Context;

    const v3, 0x104000e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    :goto_0
    sget v0, Lcom/google/android/gms/j;->nQ:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 115
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/a/w;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    return-object p2

    .line 111
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/a/w;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(I)Lcom/google/android/gms/wallet/common/a/w;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ct;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/a/w;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/ct;)Ljava/util/List;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ct;->e:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ct;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ct;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/ct;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ct;->g:Lcom/google/android/gms/wallet/common/ui/cu;

    if-nez v0, :cond_0

    .line 85
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/cu;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/cu;-><init>(Lcom/google/android/gms/wallet/common/ui/ct;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ct;->g:Lcom/google/android/gms/wallet/common/ui/cu;

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ct;->g:Lcom/google/android/gms/wallet/common/ui/cu;

    return-object v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/ct;->a(I)Lcom/google/android/gms/wallet/common/a/w;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 79
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/ct;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
