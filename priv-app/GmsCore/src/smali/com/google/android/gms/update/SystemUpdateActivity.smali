.class public Lcom/google/android/gms/update/SystemUpdateActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/setupwizard/navigationbar/a;


# instance fields
.field private A:Ljava/lang/Runnable;

.field private final a:Ljava/lang/Object;

.field private b:Z

.field private c:I

.field private d:J

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:I

.field private l:J

.field private m:I

.field private n:Landroid/widget/ProgressBar;

.field private o:Landroid/os/Handler;

.field private p:Landroid/os/Handler;

.field private q:J

.field private r:J

.field private s:Z

.field private t:Ljava/lang/String;

.field private u:Landroid/widget/TextView;

.field private v:Z

.field private w:Landroid/content/BroadcastReceiver;

.field private x:Landroid/content/BroadcastReceiver;

.field private y:Landroid/content/BroadcastReceiver;

.field private z:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 58
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 96
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->a:Ljava/lang/Object;

    .line 98
    iput-boolean v3, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->b:Z

    .line 101
    iput v2, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->c:I

    .line 102
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->d:J

    .line 103
    iput v2, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->e:I

    .line 104
    iput-object v4, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->f:Ljava/lang/String;

    .line 110
    iput v2, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->k:I

    .line 112
    iput v2, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->m:I

    .line 136
    iput-boolean v3, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->s:Z

    .line 142
    iput-object v4, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->t:Ljava/lang/String;

    .line 151
    new-instance v0, Lcom/google/android/gms/update/i;

    invoke-direct {v0, p0}, Lcom/google/android/gms/update/i;-><init>(Lcom/google/android/gms/update/SystemUpdateActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->w:Landroid/content/BroadcastReceiver;

    .line 164
    new-instance v0, Lcom/google/android/gms/update/j;

    invoke-direct {v0, p0}, Lcom/google/android/gms/update/j;-><init>(Lcom/google/android/gms/update/SystemUpdateActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->x:Landroid/content/BroadcastReceiver;

    .line 175
    new-instance v0, Lcom/google/android/gms/update/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/update/k;-><init>(Lcom/google/android/gms/update/SystemUpdateActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->y:Landroid/content/BroadcastReceiver;

    .line 183
    new-instance v0, Lcom/google/android/gms/update/l;

    invoke-direct {v0, p0}, Lcom/google/android/gms/update/l;-><init>(Lcom/google/android/gms/update/SystemUpdateActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->z:Landroid/content/BroadcastReceiver;

    .line 439
    new-instance v0, Lcom/google/android/gms/update/m;

    invoke-direct {v0, p0}, Lcom/google/android/gms/update/m;-><init>(Lcom/google/android/gms/update/SystemUpdateActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->A:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/update/SystemUpdateActivity;)J
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->q:J

    return-wide v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 762
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->v:Z

    if-nez v0, :cond_0

    .line 763
    sget v0, Lcom/google/android/gms/j;->sQ:I

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\n"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/google/android/gms/update/SystemUpdateActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(ILjava/lang/CharSequence;)V

    .line 764
    sget v0, Lcom/google/android/gms/j;->sb:I

    const/16 v1, 0x8

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 769
    :goto_0
    return-void

    .line 766
    :cond_0
    sget v0, Lcom/google/android/gms/j;->sb:I

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 767
    sget v0, Lcom/google/android/gms/j;->sb:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    goto :goto_0
.end method

.method private a(II)V
    .locals 1

    .prologue
    .line 986
    invoke-virtual {p0, p1}, Lcom/google/android/gms/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 987
    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Landroid/view/View;->setVisibility(I)V

    .line 988
    :cond_0
    return-void
.end method

.method private a(IIZ)V
    .locals 1

    .prologue
    .line 965
    invoke-virtual {p0, p2}, Lcom/google/android/gms/update/SystemUpdateActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(ILjava/lang/CharSequence;Z)V

    .line 966
    return-void
.end method

.method private a(ILjava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 978
    invoke-virtual {p0, p1}, Lcom/google/android/gms/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 979
    if-nez v0, :cond_1

    .line 983
    :cond_0
    :goto_0
    return-void

    .line 980
    :cond_1
    instance-of v1, v0, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 981
    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(ILjava/lang/CharSequence;Z)V
    .locals 2

    .prologue
    .line 969
    invoke-virtual {p0, p1}, Lcom/google/android/gms/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 970
    if-nez v1, :cond_0

    .line 975
    :goto_0
    return-void

    .line 971
    :cond_0
    instance-of v0, v1, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 972
    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 974
    :cond_1
    invoke-virtual {v1, p3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method

.method private a(JZ)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 870
    iget-wide v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->q:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_1

    .line 905
    :cond_0
    :goto_0
    return-void

    .line 874
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->e()Ljava/lang/String;

    move-result-object v0

    .line 875
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 876
    :cond_2
    const-string v0, "SystemUpdateActivity"

    const-string v1, "URL changed during countdown; aborting"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 877
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->k()V

    .line 878
    invoke-direct {p0, v8}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(Z)V

    goto :goto_0

    .line 882
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 883
    iget-wide v2, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->q:J

    cmp-long v2, v0, v2

    if-ltz v2, :cond_4

    .line 884
    iget v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->c:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 888
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->m()V

    goto :goto_0

    .line 891
    :cond_4
    iget-wide v2, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->q:J

    sub-long/2addr v2, v0

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v2, v2

    add-int/lit8 v2, v2, 0x1

    .line 892
    sget v3, Lcom/google/android/gms/j;->sb:I

    invoke-virtual {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/gms/n;->y:I

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v2, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3, v4, v8}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(ILjava/lang/CharSequence;Z)V

    .line 896
    iget-boolean v3, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->v:Z

    if-nez v3, :cond_5

    sget v3, Lcom/google/android/gms/j;->sb:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_5

    .line 897
    sget v3, Lcom/google/android/gms/j;->sb:I

    invoke-direct {p0, v3, v9}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 899
    :cond_5
    iget-wide v4, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->q:J

    add-int/lit8 v2, v2, -0x1

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    sub-long v2, v4, v2

    sub-long v0, v2, v0

    long-to-int v0, v0

    .line 900
    if-eqz p3, :cond_0

    .line 901
    iget-object v1, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->o:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/update/n;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/google/android/gms/update/n;-><init>(Lcom/google/android/gms/update/SystemUpdateActivity;JZ)V

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/update/SystemUpdateActivity;JZ)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(JZ)V

    return-void
.end method

.method private a(Z)V
    .locals 14

    .prologue
    const/4 v2, -0x1

    const/4 v13, 0x4

    const/16 v12, 0x8

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 499
    .line 501
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->c:I

    if-eq v0, v2, :cond_1

    iget v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->c:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 506
    :goto_0
    if-ne v0, v2, :cond_15

    .line 507
    iget v1, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->k:I

    if-ne v1, v2, :cond_2

    .line 511
    const-string v1, "SystemUpdateActivity"

    const-string v3, "Status unavailable and no known last status. Displaying no update."

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    .line 520
    :goto_1
    invoke-static {p0}, Lcom/google/android/gms/update/o;->c(Landroid/content/Context;)J

    move-result-wide v4

    .line 521
    const-string v0, "SystemUpdateActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "status="

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " lastCheckinTime="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " mSetupWizard="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v6, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->v:Z

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    if-ne v1, v13, :cond_3

    iget v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->k:I

    if-ne v1, v0, :cond_3

    iget-wide v6, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->q:J

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-lez v0, :cond_3

    .line 745
    :cond_0
    :goto_2
    return-void

    .line 501
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/update/o;->c()I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_0

    .line 503
    :catch_0
    move-exception v0

    const-string v0, "SystemUpdateActivity"

    const-string v1, "Not connected to service; unable to get status."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->f()V

    move v0, v2

    goto :goto_0

    .line 515
    :cond_2
    const-string v0, "SystemUpdateActivity"

    const-string v1, "Not refreshing status, as the current status is unknown."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    iget v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->k:I

    move v1, v0

    goto :goto_1

    .line 533
    :cond_3
    iput v1, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->k:I

    .line 534
    iput-wide v4, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->l:J

    .line 541
    const/4 v0, 0x5

    if-ne v1, v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->s:Z

    if-eqz v0, :cond_4

    .line 542
    const-string v0, "SystemUpdateActivity"

    const-string v1, "skipping refresh; about to reboot"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 546
    :cond_4
    const/4 v0, 0x2

    if-eq v1, v0, :cond_5

    .line 547
    iput v2, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->m:I

    .line 551
    :cond_5
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->v:Z

    if-nez v0, :cond_6

    sget v0, Lcom/google/android/gms/j;->mo:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0, v12}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->n:Landroid/widget/ProgressBar;

    const/16 v2, 0x64

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->n:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v10}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->n:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v10}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    const-string v0, "SystemUpdateActivity"

    const-string v2, "dismissButtonQualifier"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "SystemUpdateActivity"

    const-string v2, "dismissSnackbar"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/google/android/gms/j;->rO:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/update/SystemUpdateSnackbar;

    if-eqz v0, :cond_7

    invoke-virtual {v0, v12}, Lcom/google/android/gms/update/SystemUpdateSnackbar;->setVisibility(I)V

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setVisibility(I)V

    sget v2, Lcom/google/android/gms/j;->sQ:I

    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->g:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->g:Ljava/lang/String;

    :goto_3
    invoke-direct {p0, v2, v0, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(ILjava/lang/CharSequence;Z)V

    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_b

    sget v0, Lcom/google/android/gms/j;->rL:I

    sget v2, Lcom/google/android/gms/p;->yA:I

    invoke-direct {p0, v0, v2, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    sget v0, Lcom/google/android/gms/j;->rL:I

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(ILjava/lang/CharSequence;)V

    :goto_4
    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->h:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->h:Ljava/lang/String;

    :goto_5
    sget v2, Lcom/google/android/gms/j;->eg:I

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    const-string v0, ""

    :goto_6
    invoke-direct {p0, v2, v0, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(ILjava/lang/CharSequence;Z)V

    sget v0, Lcom/google/android/gms/j;->sb:I

    invoke-direct {p0, v0, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    sget v0, Lcom/google/android/gms/j;->sc:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    sget v0, Lcom/google/android/gms/j;->rL:I

    invoke-direct {p0, v0, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    sget v0, Lcom/google/android/gms/j;->A:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 553
    iget-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->v:Z

    if-eqz v0, :cond_8

    .line 554
    if-ne v1, v13, :cond_e

    .line 555
    sget v0, Lcom/google/android/gms/j;->sQ:I

    sget v2, Lcom/google/android/gms/p;->yy:I

    invoke-direct {p0, v0, v2, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    sget v0, Lcom/google/android/gms/j;->eg:I

    sget v2, Lcom/google/android/gms/p;->yx:I

    invoke-direct {p0, v0, v2, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    sget v0, Lcom/google/android/gms/j;->sb:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_8

    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    sget v0, Lcom/google/android/gms/j;->rL:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 561
    :cond_8
    :goto_7
    sparse-switch v1, :sswitch_data_0

    .line 744
    :cond_9
    :goto_8
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->h(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 551
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/update/o;->a(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    :cond_b
    sget v0, Lcom/google/android/gms/j;->rL:I

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " \u00b7 "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(ILjava/lang/CharSequence;Z)V

    goto/16 :goto_4

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/update/o;->c(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :cond_d
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto/16 :goto_6

    .line 557
    :cond_e
    sget v0, Lcom/google/android/gms/j;->sQ:I

    sget v2, Lcom/google/android/gms/p;->yf:I

    invoke-direct {p0, v0, v2, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    sget v0, Lcom/google/android/gms/j;->eg:I

    sget v2, Lcom/google/android/gms/p;->ye:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/update/SystemUpdateActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    new-array v3, v11, [Ljava/lang/CharSequence;

    const-string v6, "5"

    aput-object v6, v3, v10

    invoke-static {v2, v3}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {p0, v0, v2, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(ILjava/lang/CharSequence;Z)V

    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    sget v0, Lcom/google/android/gms/j;->sb:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    goto :goto_7

    .line 563
    :sswitch_0
    iget-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->v:Z

    if-eqz v0, :cond_f

    .line 564
    sget v0, Lcom/google/android/gms/j;->sQ:I

    sget v1, Lcom/google/android/gms/p;->yr:I

    invoke-direct {p0, v0, v1, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 565
    sget v0, Lcom/google/android/gms/j;->eg:I

    sget v1, Lcom/google/android/gms/p;->yq:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/update/SystemUpdateActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    new-array v2, v11, [Ljava/lang/CharSequence;

    invoke-static {p0, v4, v5, v11}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(Landroid/content/Context;JZ)Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v2, v10

    invoke-static {v1, v2}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {p0, v0, v1, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(ILjava/lang/CharSequence;Z)V

    .line 570
    sget v0, Lcom/google/android/gms/j;->sb:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 571
    sget v0, Lcom/google/android/gms/j;->rL:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 572
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    goto/16 :goto_8

    .line 577
    :cond_f
    :sswitch_1
    iget-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->v:Z

    if-eqz v0, :cond_10

    .line 579
    invoke-virtual {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->finish()V

    goto/16 :goto_8

    .line 581
    :cond_10
    sget v0, Lcom/google/android/gms/j;->sQ:I

    sget v1, Lcom/google/android/gms/p;->yr:I

    invoke-direct {p0, v0, v1, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 582
    sget v0, Lcom/google/android/gms/j;->eg:I

    sget v1, Lcom/google/android/gms/p;->yq:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/update/SystemUpdateActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    new-array v2, v11, [Ljava/lang/CharSequence;

    invoke-static {p0, v4, v5, v11}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(Landroid/content/Context;JZ)Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v2, v10

    invoke-static {v1, v2}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {p0, v0, v1, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(ILjava/lang/CharSequence;Z)V

    .line 587
    sget v0, Lcom/google/android/gms/j;->sb:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 588
    sget v0, Lcom/google/android/gms/j;->rL:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 589
    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-direct {p0, v0, v13}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 590
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->xR:I

    invoke-direct {p0, v0, v1, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 591
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->g()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 592
    sget v0, Lcom/google/android/gms/j;->A:I

    invoke-direct {p0, v0, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    goto/16 :goto_8

    .line 597
    :sswitch_2
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->h()V

    .line 598
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->xW:I

    invoke-direct {p0, v0, v1, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 600
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 601
    sget v0, Lcom/google/android/gms/p;->xP:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->b(I)V

    .line 602
    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-direct {p0, v0, v13}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 603
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->g()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 604
    sget v0, Lcom/google/android/gms/j;->A:I

    invoke-direct {p0, v0, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    goto/16 :goto_8

    .line 608
    :sswitch_3
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->h()V

    .line 609
    iget-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->v:Z

    if-eqz v0, :cond_11

    .line 610
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->l()V

    goto/16 :goto_8

    .line 612
    :cond_11
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->xW:I

    invoke-direct {p0, v0, v1, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 614
    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-direct {p0, v0, v13}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 615
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->g()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 616
    sget v0, Lcom/google/android/gms/j;->A:I

    invoke-direct {p0, v0, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    goto/16 :goto_8

    .line 621
    :sswitch_4
    sget v0, Lcom/google/android/gms/p;->yd:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(I)V

    .line 622
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->xW:I

    invoke-direct {p0, v0, v1, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 624
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 625
    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-direct {p0, v0, v13}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    goto/16 :goto_8

    .line 628
    :sswitch_5
    sget v0, Lcom/google/android/gms/p;->yd:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(I)V

    .line 630
    const/16 v0, 0x15

    :try_start_2
    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_12

    sget v0, Lcom/google/android/gms/j;->eg:I

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\n\n"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/gms/p;->yh:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/update/SystemUpdateActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->c()J

    move-result-wide v6

    const/4 v5, 0x0

    invoke-static {p0, v6, v7, v5}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(Landroid/content/Context;JZ)Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(ILjava/lang/CharSequence;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    .line 635
    :goto_9
    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-direct {p0, v0, v13}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 636
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->xW:I

    invoke-direct {p0, v0, v1, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 638
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    goto/16 :goto_8

    .line 630
    :cond_12
    :try_start_3
    sget v0, Lcom/google/android/gms/j;->sc:I

    sget v1, Lcom/google/android/gms/p;->yh:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/update/SystemUpdateActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->c()J

    move-result-wide v4

    const/4 v6, 0x0

    invoke-static {p0, v4, v5, v6}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(Landroid/content/Context;JZ)Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(ILjava/lang/CharSequence;Z)V

    sget v0, Lcom/google/android/gms/j;->sc:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_9

    .line 632
    :catch_1
    move-exception v0

    const-string v0, "SystemUpdateActivity"

    const-string v1, "Not connected to service; unable to get when mobile is allowed."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->f()V

    goto :goto_9

    .line 641
    :sswitch_6
    invoke-direct {p0, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->b(Z)V

    .line 642
    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-direct {p0, v0, v13}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 643
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->xW:I

    invoke-direct {p0, v0, v1, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 645
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    goto/16 :goto_8

    .line 648
    :sswitch_7
    invoke-direct {p0, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->b(Z)V

    .line 649
    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-direct {p0, v0, v13}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 650
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->xW:I

    invoke-direct {p0, v0, v1, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 652
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    goto/16 :goto_8

    .line 655
    :sswitch_8
    sget v0, Lcom/google/android/gms/j;->eg:I

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\n\n"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/gms/p;->yc:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/update/SystemUpdateActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(ILjava/lang/CharSequence;)V

    .line 657
    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-direct {p0, v0, v13}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 658
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->xW:I

    invoke-direct {p0, v0, v1, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 660
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    goto/16 :goto_8

    .line 663
    :sswitch_9
    sget v0, Lcom/google/android/gms/p;->xY:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(I)V

    .line 664
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->xZ:I

    invoke-direct {p0, v0, v1, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 666
    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-direct {p0, v0, v13}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 667
    sget v0, Lcom/google/android/gms/j;->sb:I

    invoke-direct {p0, v0, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 668
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 669
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->g()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 670
    sget v0, Lcom/google/android/gms/j;->A:I

    invoke-direct {p0, v0, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    goto/16 :goto_8

    .line 674
    :sswitch_a
    sget v0, Lcom/google/android/gms/p;->xX:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(I)V

    .line 675
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->xZ:I

    invoke-direct {p0, v0, v1, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 677
    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-direct {p0, v0, v13}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 678
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    goto/16 :goto_8

    .line 681
    :sswitch_b
    sget v0, Lcom/google/android/gms/p;->yg:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(I)V

    .line 682
    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-direct {p0, v0, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 683
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->xW:I

    invoke-direct {p0, v0, v1, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 685
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 686
    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->n:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v10}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 687
    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->n:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 688
    iget-object v1, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 689
    :try_start_4
    iget-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->b:Z

    if-nez v0, :cond_13

    .line 690
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->b:Z

    .line 691
    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->p:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->A:Ljava/lang/Runnable;

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 693
    :cond_13
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_8

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 694
    :sswitch_c
    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-direct {p0, v0, v13}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 697
    sget v0, Lcom/google/android/gms/p;->yI:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(I)V

    .line 698
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->xW:I

    invoke-direct {p0, v0, v1, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 700
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    goto/16 :goto_8

    .line 703
    :sswitch_d
    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-direct {p0, v0, v13}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 704
    sget v0, Lcom/google/android/gms/p;->yG:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(I)V

    .line 705
    sget v0, Lcom/google/android/gms/j;->sb:I

    invoke-direct {p0, v0, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 706
    iget-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->v:Z

    if-eqz v0, :cond_14

    .line 707
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->xZ:I

    invoke-direct {p0, v0, v1, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 712
    :goto_a
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 713
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->g()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 714
    sget v0, Lcom/google/android/gms/j;->A:I

    invoke-direct {p0, v0, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    goto/16 :goto_8

    .line 710
    :cond_14
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->xR:I

    invoke-direct {p0, v0, v1, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    goto :goto_a

    .line 718
    :sswitch_e
    sget v0, Lcom/google/android/gms/p;->yH:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(I)V

    .line 719
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->yp:I

    invoke-direct {p0, v0, v1, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 720
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 721
    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-direct {p0, v0, v13}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 722
    sget v0, Lcom/google/android/gms/p;->xO:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->b(I)V

    goto/16 :goto_8

    .line 725
    :sswitch_f
    sget v0, Lcom/google/android/gms/p;->yH:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(I)V

    .line 726
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->yp:I

    invoke-direct {p0, v0, v1, v10}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 727
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-direct {p0, v0, v12}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 728
    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-direct {p0, v0, v13}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 729
    sget v0, Lcom/google/android/gms/p;->xN:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->b(I)V

    goto/16 :goto_8

    .line 732
    :sswitch_10
    sget v0, Lcom/google/android/gms/p;->yH:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(I)V

    .line 733
    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-direct {p0, v0, v13}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 734
    iget-wide v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->q:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_9

    .line 735
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->yp:I

    invoke-direct {p0, v0, v1, v11}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 736
    iget-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->v:Z

    if-eqz v0, :cond_9

    .line 739
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->i()V

    goto/16 :goto_8

    :cond_15
    move v1, v0

    goto/16 :goto_1

    .line 561
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x0 -> :sswitch_1
        0x1 -> :sswitch_3
        0x2 -> :sswitch_b
        0x3 -> :sswitch_c
        0x4 -> :sswitch_10
        0x5 -> :sswitch_1
        0x6 -> :sswitch_9
        0x7 -> :sswitch_d
        0x9 -> :sswitch_a
        0xa -> :sswitch_2
        0xb -> :sswitch_4
        0xc -> :sswitch_5
        0xd -> :sswitch_e
        0xe -> :sswitch_f
        0x10b -> :sswitch_6
        0x20b -> :sswitch_7
        0x30b -> :sswitch_8
    .end sparse-switch
.end method

.method static synthetic b(Lcom/google/android/gms/update/SystemUpdateActivity;)J
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->r:J

    return-wide v0
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 801
    const-string v0, "SystemUpdateActivity"

    const-string v1, "displayButtonQualifier"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 802
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 803
    const-string v0, "SystemUpdateActivity"

    const-string v1, "displaySnackbar"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/google/android/gms/j;->rO:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/update/SystemUpdateSnackbar;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/update/SystemUpdateSnackbar;->setText(I)V

    invoke-virtual {v0}, Lcom/google/android/gms/update/SystemUpdateSnackbar;->a()V

    .line 808
    :cond_0
    :goto_0
    return-void

    .line 805
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 806
    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->u:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 748
    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->f:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/gms/update/o;->a(Landroid/content/Context;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 751
    if-eqz v1, :cond_0

    .line 752
    if-eqz p1, :cond_1

    sget v0, Lcom/google/android/gms/p;->yb:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 755
    :goto_0
    sget v2, Lcom/google/android/gms/j;->eg:I

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\n\n"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/CharSequence;

    aget-object v5, v1, v6

    aput-object v5, v4, v6

    aget-object v1, v1, v7

    aput-object v1, v4, v7

    invoke-static {v0, v4}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v2, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(ILjava/lang/CharSequence;)V

    .line 757
    sget v0, Lcom/google/android/gms/j;->eg:I

    invoke-direct {p0, v0, v6}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 759
    :cond_0
    return-void

    .line 752
    :cond_1
    sget v0, Lcom/google/android/gms/p;->ya:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private c()J
    .locals 4

    .prologue
    .line 198
    iget-wide v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->d:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->d:J

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/google/android/gms/update/o;->d()J

    move-result-wide v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/update/SystemUpdateActivity;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->m()V

    return-void
.end method

.method private d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->i:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/update/o;->b(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/gms/update/SystemUpdateActivity;)V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(Z)V

    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/update/SystemUpdateActivity;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->a:Ljava/lang/Object;

    return-object v0
.end method

.method private e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->j:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/update/o;->d(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 425
    invoke-static {}, Lcom/google/android/gms/update/o;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 426
    const-string v0, "SystemUpdateActivity"

    const-string v1, "Reconnecting to SystemUpdateService."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    invoke-static {p0}, Lcom/google/android/gms/update/o;->a(Landroid/content/Context;)V

    .line 429
    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/update/SystemUpdateActivity;)Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->b:Z

    return v0
.end method

.method static synthetic g(Lcom/google/android/gms/update/SystemUpdateActivity;)I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->k:I

    return v0
.end method

.method private g()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 488
    iget-boolean v1, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->v:Z

    if-eqz v1, :cond_1

    .line 491
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/a/a;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "update_enable_sd_card"

    invoke-static {v1, v2, v0}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic h(Lcom/google/android/gms/update/SystemUpdateActivity;)I
    .locals 2

    .prologue
    .line 58
    iget v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->e:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->e:I

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/google/android/gms/update/o;->e()I

    move-result v0

    goto :goto_0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 772
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 773
    sget v0, Lcom/google/android/gms/j;->eg:I

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\n\n"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/gms/p;->yz:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/update/SystemUpdateActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(ILjava/lang/CharSequence;)V

    .line 778
    :goto_0
    return-void

    .line 776
    :cond_0
    sget v0, Lcom/google/android/gms/j;->sb:I

    sget v1, Lcom/google/android/gms/p;->yz:I

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    goto :goto_0
.end method

.method static synthetic i(Lcom/google/android/gms/update/SystemUpdateActivity;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->n:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private i()V
    .locals 4

    .prologue
    .line 838
    const-string v0, "SystemUpdateActivity"

    const-string v1, "startCountdown"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 839
    iget-wide v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->q:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 840
    const-string v0, "SystemUpdateActivity"

    const-string v1, "skipping, as a countdown is already in progress."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 849
    :goto_0
    return-void

    .line 843
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/update/o;->b(Landroid/content/Context;)V

    .line 844
    iget-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->v:Z

    .line 845
    const-string v0, "SystemUpdateActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "countdown delay is: 10000"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 846
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x2710

    add-long/2addr v0, v2

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->q:J

    .line 847
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->t:Ljava/lang/String;

    .line 848
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->j()V

    goto :goto_0
.end method

.method private j()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 852
    const-string v0, "SystemUpdateActivity"

    const-string v1, "resumeCountdown"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 853
    sget v0, Lcom/google/android/gms/j;->z:I

    sget v1, Lcom/google/android/gms/p;->xU:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 854
    iget-wide v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->q:J

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(JZ)V

    .line 855
    return-void
.end method

.method static synthetic j(Lcom/google/android/gms/update/SystemUpdateActivity;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->f()V

    return-void
.end method

.method static synthetic k(Lcom/google/android/gms/update/SystemUpdateActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->p:Landroid/os/Handler;

    return-object v0
.end method

.method private k()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 858
    const-string v0, "SystemUpdateActivity"

    const-string v1, "stopCountdown"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    iget-wide v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->q:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 860
    const v0, 0x3112a

    const-string v1, "activity-countdown-cancel"

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 862
    iput-wide v2, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->q:J

    .line 863
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->r:J

    .line 865
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->t:Ljava/lang/String;

    .line 866
    sget v0, Lcom/google/android/gms/j;->sb:I

    const/16 v1, 0x8

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(II)V

    .line 867
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 908
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/update/SystemUpdateService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 911
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/update/o;->f()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 916
    :goto_0
    return-void

    .line 913
    :catch_0
    move-exception v0

    const-string v0, "SystemUpdateActivity"

    const-string v1, "Not connected to service; cannot approve download."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 914
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->f()V

    goto :goto_0
.end method

.method static synthetic l(Lcom/google/android/gms/update/SystemUpdateActivity;)Z
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->b:Z

    return v0
.end method

.method private m()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 919
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/update/SystemUpdateService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 920
    sget v0, Lcom/google/android/gms/j;->sb:I

    sget v1, Lcom/google/android/gms/p;->xV:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(IIZ)V

    .line 922
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->v:Z

    invoke-static {v0}, Lcom/google/android/gms/update/o;->a(Z)V

    .line 923
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->s:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 928
    :goto_0
    return-void

    .line 925
    :catch_0
    move-exception v0

    const-string v0, "SystemUpdateActivity"

    const-string v1, "Not connected to service; cannot approve install."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 926
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->f()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 1065
    invoke-virtual {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->onBackPressed()V

    .line 1066
    return-void
.end method

.method public final a(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1048
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->b()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1049
    iget-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->v:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1052
    invoke-virtual {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "useImmersiveMode"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 1054
    if-eqz v0, :cond_0

    .line 1055
    invoke-virtual {p1, v0}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->a(Z)V

    .line 1056
    invoke-virtual {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, -0x80000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 1060
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 1072
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1000
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->z:I

    if-ne v0, v1, :cond_4

    .line 1001
    iget v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->k:I

    packed-switch v0, :pswitch_data_0

    .line 1043
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1004
    :pswitch_1
    invoke-static {p0}, Lcom/google/android/gms/update/o;->b(Landroid/content/Context;)V

    goto :goto_0

    .line 1007
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->v:Z

    if-eqz v0, :cond_1

    .line 1008
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->l()V

    goto :goto_0

    .line 1010
    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/update/o;->b(Landroid/content/Context;)V

    goto :goto_0

    .line 1016
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->l()V

    goto :goto_0

    .line 1019
    :pswitch_4
    iget-wide v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->q:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 1020
    iget-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->v:Z

    if-eqz v0, :cond_2

    .line 1023
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->m()V

    goto :goto_0

    .line 1025
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->k()V

    .line 1030
    iput-wide v2, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->r:J

    .line 1031
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(Z)V

    goto :goto_0

    .line 1034
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->i()V

    .line 1035
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/j;->cd:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/widget/ScrollView;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/widget/ScrollView;

    const/16 v1, 0x21

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->fullScroll(I)Z

    goto :goto_0

    .line 1039
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->A:I

    if-ne v0, v1, :cond_0

    .line 1040
    const-string v0, "SystemUpdateActivity"

    const-string v1, "Clicked on SD Card button."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1041
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1001
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/16 v9, 0x15

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v8, -0x1

    const-wide/16 v6, 0x0

    .line 235
    const-string v2, "SystemUpdateActivity"

    const-string v3, "onCreate"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 238
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/a/a;->b()I

    move-result v2

    if-lez v2, :cond_0

    sget-object v2, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v3, "user"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 239
    sget v1, Lcom/google/android/gms/p;->yt:I

    invoke-static {p0, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 241
    const/16 v1, 0x11

    invoke-virtual {v0}, Landroid/widget/Toast;->getXOffset()I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/Toast;->getYOffset()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 243
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 244
    invoke-virtual {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->finish()V

    .line 338
    :goto_0
    return-void

    .line 247
    :cond_0
    const-string v2, "SystemUpdateActivity"

    const-string v3, "Attempting to connect to the SystemUpdateService."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    invoke-static {p0}, Lcom/google/android/gms/update/o;->a(Landroid/content/Context;)V

    .line 250
    invoke-virtual {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 251
    invoke-virtual {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 252
    const-string v3, "force-state"

    invoke-virtual {v2, v3, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->c:I

    .line 253
    const-string v3, "force-when-mobile-allowed"

    invoke-virtual {v2, v3, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    int-to-long v4, v3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->d:J

    .line 255
    const-string v3, "force-download-progress"

    invoke-virtual {v2, v3, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->e:I

    .line 256
    const-string v3, "force-off-peak-window"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->f:Ljava/lang/String;

    .line 257
    const-string v3, "force-update-title"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->g:Ljava/lang/String;

    .line 258
    const-string v3, "force-update-description"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->h:Ljava/lang/String;

    .line 259
    const-string v3, "force-update-size"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->i:Ljava/lang/String;

    .line 260
    const-string v3, "force-update-url"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->j:Ljava/lang/String;

    .line 263
    :cond_1
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->o:Landroid/os/Handler;

    .line 264
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->p:Landroid/os/Handler;

    .line 266
    invoke-virtual {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 267
    if-eqz v2, :cond_2

    .line 268
    const-string v3, "force-setup_wizard"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_4

    move-object v1, p0

    .line 271
    :goto_1
    iput-boolean v0, v1, Lcom/google/android/gms/update/SystemUpdateActivity;->v:Z

    .line 274
    iget-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->v:Z

    if-eqz v0, :cond_2

    invoke-static {v9}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 275
    const-string v0, "theme"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 276
    const-string v1, "material"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 277
    sget v0, Lcom/google/android/gms/q;->K:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->setTheme(I)V

    .line 284
    :cond_2
    :goto_2
    sget v0, Lcom/google/android/gms/p;->xQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->setTitle(I)V

    .line 288
    invoke-static {v9}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 289
    sget v0, Lcom/google/android/gms/l;->fs:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->setContentView(I)V

    .line 296
    :goto_3
    sget v0, Lcom/google/android/gms/j;->z:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 297
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 298
    sget v0, Lcom/google/android/gms/j;->A:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 301
    :cond_3
    sget v0, Lcom/google/android/gms/j;->cm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->u:Landroid/widget/TextView;

    .line 302
    sget v0, Lcom/google/android/gms/j;->pQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->n:Landroid/widget/ProgressBar;

    .line 304
    sget v0, Lcom/google/android/gms/j;->eg:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 307
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 308
    iget-object v1, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->w:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 310
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 311
    const-string v1, "com.google.android.gms.update.STATUS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 312
    const-string v1, "com.google.android.checkin.CHECKIN_COMPLETE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 313
    iget-object v1, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->z:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 315
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 316
    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 317
    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 318
    const-string v1, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 319
    const-string v1, "android.intent.action.MEDIA_SHARED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 320
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 321
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 322
    iget-object v1, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->x:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 324
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 325
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 326
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 327
    iget-object v1, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->y:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 329
    if-eqz p1, :cond_9

    .line 330
    const-string v0, "countdown_end"

    invoke-virtual {p1, v0, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->q:J

    .line 331
    iput-wide v6, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->r:J

    .line 332
    const-string v0, "countdown_url"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->t:Ljava/lang/String;

    goto/16 :goto_0

    .line 271
    :cond_4
    const-string v3, "firstRun"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/update/o;->e(Landroid/content/ContentResolver;)Z

    move-result v3

    if-eqz v3, :cond_5

    move-object v1, p0

    goto/16 :goto_1

    :cond_5
    move v0, v1

    move-object v1, p0

    goto/16 :goto_1

    .line 279
    :cond_6
    sget v0, Lcom/google/android/gms/q;->L:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->setTheme(I)V

    goto/16 :goto_2

    .line 290
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->v:Z

    if-eqz v0, :cond_8

    .line 291
    sget v0, Lcom/google/android/gms/l;->ft:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->setContentView(I)V

    goto/16 :goto_3

    .line 293
    :cond_8
    sget v0, Lcom/google/android/gms/l;->fr:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->setContentView(I)V

    goto/16 :goto_3

    .line 334
    :cond_9
    iput-wide v6, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->q:J

    .line 335
    iput-wide v6, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->r:J

    .line 336
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->t:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 354
    const-string v0, "SystemUpdateActivity"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 356
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/a/a;->b()I

    move-result v0

    if-lez v0, :cond_0

    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v1, "user"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    :goto_0
    return-void

    .line 357
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->w:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 358
    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->z:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 359
    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->x:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 360
    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->y:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 361
    invoke-static {}, Lcom/google/android/gms/update/o;->b()V

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 366
    const-string v0, "SystemUpdateActivity"

    const-string v1, "onNewIntent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 368
    invoke-virtual {p0, p1}, Lcom/google/android/gms/update/SystemUpdateActivity;->setIntent(Landroid/content/Intent;)V

    .line 369
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->f()V

    .line 370
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 420
    const-string v0, "SystemUpdateActivity"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 422
    return-void
.end method

.method protected onRestart()V
    .locals 2

    .prologue
    .line 406
    const-string v0, "SystemUpdateActivity"

    const-string v1, "onRestart"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    .line 408
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->f()V

    .line 409
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 413
    const-string v0, "SystemUpdateActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 415
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->f()V

    .line 416
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 433
    const-string v0, "SystemUpdateActivity"

    const-string v1, "onSaveInstanceState"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 435
    const-string v0, "countdown_end"

    iget-wide v2, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->q:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 436
    const-string v0, "countdown_url"

    iget-object v1, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    return-void
.end method

.method protected onStart()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 374
    const-string v0, "SystemUpdateActivity"

    const-string v1, "onStart"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 377
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->f()V

    .line 379
    sput-boolean v2, Lcom/google/android/gms/update/SystemUpdateService;->b:Z

    .line 380
    invoke-static {p0}, Lcom/google/android/gms/update/SystemUpdateService;->a(Landroid/content/Context;)V

    .line 381
    invoke-direct {p0, v2}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(Z)V

    .line 383
    iget-wide v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->q:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 384
    const v0, 0x3112a

    const-string v1, "activity-countdown-resume"

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 385
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->j()V

    .line 387
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 391
    const-string v0, "SystemUpdateActivity"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 393
    invoke-direct {p0}, Lcom/google/android/gms/update/SystemUpdateActivity;->k()V

    .line 394
    sput-boolean v2, Lcom/google/android/gms/update/SystemUpdateService;->b:Z

    .line 395
    iget-object v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->A:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 396
    iget-object v1, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 397
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/update/SystemUpdateActivity;->b:Z

    .line 398
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 400
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/update/SystemUpdateService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "from_activity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 402
    return-void

    .line 398
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
