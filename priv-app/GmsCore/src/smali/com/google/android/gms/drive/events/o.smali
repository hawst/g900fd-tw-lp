.class final Lcom/google/android/gms/drive/events/o;
.super Lcom/google/android/gms/drive/internal/ch;
.source "SourceFile"


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcom/google/android/gms/drive/database/model/bl;

.field final synthetic c:Lcom/google/android/gms/drive/events/f;

.field final synthetic d:Lcom/google/android/gms/drive/events/l;

.field private final e:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/events/l;JLcom/google/android/gms/drive/database/model/bl;Lcom/google/android/gms/drive/events/f;)V
    .locals 2

    .prologue
    .line 196
    iput-object p1, p0, Lcom/google/android/gms/drive/events/o;->d:Lcom/google/android/gms/drive/events/l;

    iput-wide p2, p0, Lcom/google/android/gms/drive/events/o;->a:J

    iput-object p4, p0, Lcom/google/android/gms/drive/events/o;->b:Lcom/google/android/gms/drive/database/model/bl;

    iput-object p5, p0, Lcom/google/android/gms/drive/events/o;->c:Lcom/google/android/gms/drive/events/f;

    invoke-direct {p0}, Lcom/google/android/gms/drive/internal/ch;-><init>()V

    .line 197
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/drive/events/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 4

    .prologue
    .line 201
    if-eqz p1, :cond_0

    const-string v0, "snooze"

    .line 202
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/drive/events/o;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 203
    const-string v1, "EventDistributorImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Event released multiple times: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    :goto_1
    return-void

    .line 201
    :cond_0
    const-string v0, "dismiss"

    goto :goto_0

    .line 206
    :cond_1
    const-string v1, "EventDistributorImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Event released remotely: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/drive/events/o;->d:Lcom/google/android/gms/drive/events/l;

    iget-object v0, v0, Lcom/google/android/gms/drive/events/l;->g:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/gms/drive/events/p;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/drive/events/p;-><init>(Lcom/google/android/gms/drive/events/o;Z)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_1
.end method
