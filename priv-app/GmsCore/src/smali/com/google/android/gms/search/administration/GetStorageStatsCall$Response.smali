.class public Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/ap;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/administration/c;


# instance fields
.field public a:Lcom/google/android/gms/common/api/Status;

.field public b:[Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;

.field public c:J

.field public d:J

.field public e:J

.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 116
    new-instance v0, Lcom/google/android/gms/search/administration/c;

    invoke-direct {v0}, Lcom/google/android/gms/search/administration/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->CREATOR:Lcom/google/android/gms/search/administration/c;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->f:I

    .line 105
    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/common/api/Status;[Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;JJJ)V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput p1, p0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->f:I

    .line 130
    iput-object p2, p0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    .line 131
    iput-object p3, p0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->b:[Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;

    .line 132
    iput-wide p4, p0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->c:J

    .line 133
    iput-wide p6, p0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->d:J

    .line 134
    iput-wide p8, p0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->e:J

    .line 135
    return-void
.end method


# virtual methods
.method public final C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 141
    sget-object v0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->CREATOR:Lcom/google/android/gms/search/administration/c;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 147
    sget-object v0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->CREATOR:Lcom/google/android/gms/search/administration/c;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/search/administration/c;->a(Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;Landroid/os/Parcel;I)V

    .line 148
    return-void
.end method
