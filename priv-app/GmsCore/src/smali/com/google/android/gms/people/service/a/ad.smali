.class public final Lcom/google/android/gms/people/service/a/ad;
.super Lcom/google/android/gms/people/service/a/b;
.source "SourceFile"


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/util/List;

.field private final k:I

.field private final l:Z

.field private final m:J

.field private final n:Ljava/lang/String;

.field private final o:I

.field private final p:I

.field private final q:Z

.field private final r:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;III)V
    .locals 3

    .prologue
    .line 35
    invoke-direct/range {p0 .. p5}, Lcom/google/android/gms/people/service/a/b;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;)V

    .line 37
    iput-object p5, p0, Lcom/google/android/gms/people/service/a/ad;->g:Ljava/lang/String;

    .line 38
    iput-object p6, p0, Lcom/google/android/gms/people/service/a/ad;->h:Ljava/lang/String;

    .line 39
    iput-object p7, p0, Lcom/google/android/gms/people/service/a/ad;->i:Ljava/lang/String;

    .line 40
    iput-object p8, p0, Lcom/google/android/gms/people/service/a/ad;->j:Ljava/util/List;

    .line 41
    iput p9, p0, Lcom/google/android/gms/people/service/a/ad;->k:I

    .line 42
    iput-boolean p10, p0, Lcom/google/android/gms/people/service/a/ad;->l:Z

    .line 43
    iput-wide p11, p0, Lcom/google/android/gms/people/service/a/ad;->m:J

    .line 44
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/ad;->n:Ljava/lang/String;

    .line 45
    move/from16 v0, p14

    iput v0, p0, Lcom/google/android/gms/people/service/a/ad;->o:I

    .line 46
    move/from16 v0, p15

    iput v0, p0, Lcom/google/android/gms/people/service/a/ad;->p:I

    .line 47
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/people/service/a/ad;->q:Z

    .line 48
    move/from16 v0, p16

    iput v0, p0, Lcom/google/android/gms/people/service/a/ad;->r:I

    .line 50
    return-void
.end method


# virtual methods
.method public final b(Landroid/content/Context;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 15

    .prologue
    .line 54
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/ad;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/ad;->h:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/ad;->i:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/people/service/a/ad;->j:Ljava/util/List;

    iget v6, p0, Lcom/google/android/gms/people/service/a/ad;->k:I

    iget-boolean v7, p0, Lcom/google/android/gms/people/service/a/ad;->l:Z

    iget-wide v8, p0, Lcom/google/android/gms/people/service/a/ad;->m:J

    iget-object v10, p0, Lcom/google/android/gms/people/service/a/ad;->n:Ljava/lang/String;

    iget v11, p0, Lcom/google/android/gms/people/service/a/ad;->o:I

    iget v12, p0, Lcom/google/android/gms/people/service/a/ad;->r:I

    iget v13, p0, Lcom/google/android/gms/people/service/a/ad;->p:I

    iget-boolean v14, p0, Lcom/google/android/gms/people/service/a/ad;->q:Z

    move-object/from16 v1, p1

    invoke-static/range {v1 .. v14}, Lcom/google/android/gms/people/service/a/af;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;IIIZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
