.class public final Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:I

.field final b:Lcom/google/android/gms/drive/d/g;

.field final c:Ljava/lang/Exception;

.field final d:Ljava/lang/String;


# direct methods
.method constructor <init>(ILcom/google/android/gms/drive/d/g;Ljava/lang/Exception;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    if-eqz p3, :cond_0

    .line 28
    if-nez p2, :cond_2

    if-nez p4, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 30
    :cond_0
    if-eqz p2, :cond_1

    .line 31
    if-nez p3, :cond_3

    if-nez p4, :cond_3

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 34
    :cond_1
    iput p1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->a:I

    .line 35
    iput-object p2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->b:Lcom/google/android/gms/drive/d/g;

    .line 36
    iput-object p3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->c:Ljava/lang/Exception;

    .line 37
    iput-object p4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->d:Ljava/lang/String;

    .line 38
    return-void

    :cond_2
    move v0, v2

    .line 28
    goto :goto_0

    :cond_3
    move v1, v2

    .line 31
    goto :goto_1
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->b:Lcom/google/android/gms/drive/d/g;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->c:Ljava/lang/Exception;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 68
    if-ne p0, p1, :cond_1

    .line 75
    :cond_0
    :goto_0
    return v0

    .line 71
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;

    if-nez v2, :cond_2

    move v0, v1

    .line 72
    goto :goto_0

    .line 74
    :cond_2
    check-cast p1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;

    .line 75
    iget v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->a:I

    iget v3, p1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->a:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->b:Lcom/google/android/gms/drive/d/g;

    iget-object v3, p1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->b:Lcom/google/android/gms/drive/d/g;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->c:Ljava/lang/Exception;

    iget-object v3, p1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->c:Ljava/lang/Exception;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 83
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->b:Lcom/google/android/gms/drive/d/g;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->c:Ljava/lang/Exception;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 88
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ResultsQueueItem[id=%d, resultsPage=%s, e=%s, nextPageToken=%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->b:Lcom/google/android/gms/drive/d/g;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->c:Ljava/lang/Exception;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->d:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
