.class public final Lcom/google/android/gms/feedback/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/feedback/ErrorReport;

.field b:Lcom/google/android/gms/feedback/f;

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/feedback/ErrorReport;)V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/feedback/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/Screenshot;)V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/Screenshot;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object p1, p0, Lcom/google/android/gms/feedback/e;->c:Landroid/content/Context;

    .line 94
    iput-object p2, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    .line 95
    new-instance v0, Lcom/google/android/gms/feedback/f;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/feedback/f;-><init>(Lcom/google/android/gms/feedback/e;B)V

    iput-object v0, p0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    .line 97
    if-eqz p3, :cond_0

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    iput-object p3, v0, Lcom/google/android/gms/feedback/f;->e:Lcom/google/android/gms/feedback/Screenshot;

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-static {v0, v2}, Lcom/google/android/gms/feedback/e;->a(Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/Screenshot;)V

    .line 101
    :cond_0
    iget-object v0, p2, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    iget-object v1, p2, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    iput-object v1, v0, Lcom/google/android/gms/feedback/f;->f:Landroid/os/Bundle;

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iput-object v2, v0, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    .line 105
    :cond_1
    iget-object v0, p2, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    iget-object v1, p2, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/f;->g:[Ljava/lang/String;

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iput-object v2, v0, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    .line 109
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Landroid/app/ApplicationErrorReport;->time:J

    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, p0, Lcom/google/android/gms/feedback/e;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->D:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->f:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->g:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->h:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->i:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->j:Ljava/lang/String;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->l:I

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->m:Ljava/lang/String;

    sget-object v1, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->n:Ljava/lang/String;

    sget-object v1, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->o:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->BOARD:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->p:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->BRAND:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->q:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->k:Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/feedback/e;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v2, v0, Lcom/google/android/gms/feedback/ErrorReport;->d:I

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->e:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->c:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v2

    iput v2, v1, Lcom/google/android/gms/feedback/ErrorReport;->z:I

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/feedback/ErrorReport;->B:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v2

    iput v2, v1, Lcom/google/android/gms/feedback/ErrorReport;->A:I

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    :try_start_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/google/android/gms/feedback/ErrorReport;->G:I

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->H:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/e;->a(Lcom/google/android/gms/feedback/ErrorReport;)V

    .line 110
    return-void

    .line 109
    :catch_0
    move-exception v0

    const-string v1, "ErrorReportUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception while gathering network params: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/feedback/ErrorReport;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->c:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 181
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v1

    .line 182
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    .line 183
    const/4 v0, 0x0

    .line 184
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 185
    add-int/lit8 v2, v1, 0x1

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    aput-object v0, v3, v1

    move v1, v2

    .line 186
    goto :goto_0

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    iput-object v3, v0, Lcom/google/android/gms/feedback/f;->c:[Ljava/lang/String;

    .line 190
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget v0, v0, Landroid/app/ApplicationErrorReport;->type:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    invoke-static {}, Lcom/google/android/gms/feedback/e;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/feedback/f;->d:Ljava/lang/String;

    .line 193
    :cond_1
    return-void
.end method

.method public static a(Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/Screenshot;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 308
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/feedback/Screenshot;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 309
    iget v0, p1, Lcom/google/android/gms/feedback/Screenshot;->b:I

    iput v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->x:I

    .line 310
    iget v0, p1, Lcom/google/android/gms/feedback/Screenshot;->a:I

    iput v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->y:I

    .line 311
    iget-object v0, p1, Lcom/google/android/gms/feedback/Screenshot;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->v:Ljava/lang/String;

    .line 318
    :goto_0
    return-void

    .line 313
    :cond_0
    iput v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->x:I

    .line 314
    iput v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->y:I

    .line 315
    iput-object v2, p0, Lcom/google/android/gms/feedback/ErrorReport;->v:Ljava/lang/String;

    .line 316
    iput-object v2, p0, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    goto :goto_0
.end method

.method private static c()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 198
    :try_start_0
    new-instance v1, Ljava/io/FileReader;

    const-string v2, "/data/anr/traces.txt"

    invoke-direct {v1, v2}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    :try_start_1
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 200
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 202
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 203
    if-eqz v4, :cond_1

    .line 204
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 216
    :catch_0
    move-exception v2

    :goto_1
    if-eqz v1, :cond_0

    .line 218
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 224
    :cond_0
    :goto_2
    return-object v0

    .line 210
    :cond_1
    :try_start_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 216
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    move-object v1, v0

    :goto_3
    if-eqz v1, :cond_0

    .line 218
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_2

    .line 221
    :catch_3
    move-exception v1

    goto :goto_2

    .line 216
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_4
    if-eqz v1, :cond_2

    .line 218
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 221
    :cond_2
    :goto_5
    throw v0

    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_5

    .line 216
    :catchall_1
    move-exception v0

    goto :goto_4

    :catch_6
    move-exception v2

    goto :goto_3

    :catch_7
    move-exception v1

    move-object v1, v0

    goto :goto_1
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/feedback/ErrorReport;
    .locals 1

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/e;->b()V

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/feedback/ErrorReport;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iput-object p1, v0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    .line 278
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Z)Lcom/google/android/gms/feedback/ErrorReport;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iput-boolean p2, v0, Lcom/google/android/gms/feedback/ErrorReport;->X:Z

    .line 229
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iput-object p1, v0, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    .line 230
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/e;->b()V

    .line 231
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    return-object v0
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 268
    if-eqz p1, :cond_0

    .line 269
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    iput-object p1, v0, Lcom/google/android/gms/feedback/f;->a:[Ljava/lang/String;

    .line 271
    :cond_0
    if-eqz p2, :cond_1

    .line 272
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    iput-object p2, v0, Lcom/google/android/gms/feedback/f;->b:[Ljava/lang/String;

    .line 274
    :cond_1
    return-void
.end method

.method final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 246
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-boolean v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->X:Z

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->u:Ljava/lang/String;

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->s:[Ljava/lang/String;

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->t:[Ljava/lang/String;

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->r:[Ljava/lang/String;

    .line 251
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-static {v0, v1}, Lcom/google/android/gms/feedback/e;->a(Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/Screenshot;)V

    .line 262
    :goto_0
    return-void

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, p0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    iget-object v1, v1, Lcom/google/android/gms/feedback/f;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->u:Ljava/lang/String;

    .line 255
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, p0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    iget-object v1, v1, Lcom/google/android/gms/feedback/f;->a:[Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->s:[Ljava/lang/String;

    .line 256
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, p0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    iget-object v1, v1, Lcom/google/android/gms/feedback/f;->b:[Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->t:[Ljava/lang/String;

    .line 257
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, p0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    iget-object v1, v1, Lcom/google/android/gms/feedback/f;->c:[Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->r:[Ljava/lang/String;

    .line 258
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, p0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    iget-object v1, v1, Lcom/google/android/gms/feedback/f;->g:[Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    .line 259
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, p0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    iget-object v1, v1, Lcom/google/android/gms/feedback/f;->f:Landroid/os/Bundle;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    .line 260
    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, p0, Lcom/google/android/gms/feedback/e;->b:Lcom/google/android/gms/feedback/f;

    iget-object v1, v1, Lcom/google/android/gms/feedback/f;->e:Lcom/google/android/gms/feedback/Screenshot;

    invoke-static {v0, v1}, Lcom/google/android/gms/feedback/e;->a(Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/Screenshot;)V

    goto :goto_0
.end method
