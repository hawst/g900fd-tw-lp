.class final Lcom/google/android/gms/ads/internal/request/ag;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/util/s;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/google/android/gms/ads/internal/request/ae;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/request/ae;Z)V
    .locals 0

    .prologue
    .line 330
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/ag;->b:Lcom/google/android/gms/ads/internal/request/ae;

    iput-boolean p2, p0, Lcom/google/android/gms/ads/internal/request/ag;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(Ljava/io/InputStream;)Landroid/graphics/drawable/Drawable;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x0

    .line 339
    .line 341
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/InputStream;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 346
    :goto_0
    if-nez v1, :cond_0

    .line 347
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/ag;->b:Lcom/google/android/gms/ads/internal/request/ae;

    iget-boolean v2, p0, Lcom/google/android/gms/ads/internal/request/ag;->a:Z

    invoke-virtual {v1, v4, v2}, Lcom/google/android/gms/ads/internal/request/ae;->a(IZ)V

    .line 357
    :goto_1
    return-object v0

    :catch_0
    move-exception v1

    move-object v1, v0

    goto :goto_0

    .line 351
    :cond_0
    const/4 v2, 0x0

    array-length v3, v1

    invoke-static {v1, v2, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 352
    if-nez v1, :cond_1

    .line 353
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/ag;->b:Lcom/google/android/gms/ads/internal/request/ae;

    iget-boolean v2, p0, Lcom/google/android/gms/ads/internal/request/ag;->a:Z

    invoke-virtual {v1, v4, v2}, Lcom/google/android/gms/ads/internal/request/ae;->a(IZ)V

    goto :goto_1

    .line 357
    :cond_1
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/ag;->b:Lcom/google/android/gms/ads/internal/request/ae;

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/ads/internal/request/ag;->a:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/ads/internal/request/ae;->a(IZ)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic a(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 330
    invoke-direct {p0, p1}, Lcom/google/android/gms/ads/internal/request/ag;->b(Ljava/io/InputStream;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
