.class public final Lcom/google/android/gms/drive/database/model/bi;
.super Lcom/google/android/gms/drive/database/model/ad;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field private final g:Lcom/google/android/gms/drive/database/model/EntrySpec;

.field private final h:Lcom/google/android/gms/drive/auth/AppIdentity;

.field private final i:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;J)V
    .locals 11

    .prologue
    .line 58
    const/4 v10, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-wide/from16 v8, p7

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gms/drive/database/model/bi;-><init>(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;JLjava/lang/String;)V

    .line 60
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;JLjava/lang/String;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 88
    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->a()Lcom/google/android/gms/drive/database/model/bj;

    move-result-object v1

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/gms/drive/database/model/ad;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)V

    .line 89
    invoke-static {p2}, Lcom/google/android/gms/drive/g/y;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/drive/database/model/bi;->a:Ljava/lang/String;

    .line 90
    if-eqz p3, :cond_0

    invoke-static {p3}, Lcom/google/android/gms/drive/g/y;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/bi;->b:Ljava/lang/String;

    .line 92
    iput-object p4, p0, Lcom/google/android/gms/drive/database/model/bi;->c:Ljava/lang/String;

    .line 93
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/EntrySpec;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/bi;->g:Lcom/google/android/gms/drive/database/model/EntrySpec;

    .line 94
    invoke-static {p6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/AppIdentity;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/bi;->h:Lcom/google/android/gms/drive/auth/AppIdentity;

    .line 95
    iput-wide p7, p0, Lcom/google/android/gms/drive/database/model/bi;->i:J

    .line 96
    iput-object p9, p0, Lcom/google/android/gms/drive/database/model/bi;->d:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public static a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/bi;
    .locals 11

    .prologue
    .line 176
    sget-object v0, Lcom/google/android/gms/drive/database/model/bk;->a:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    .line 177
    sget-object v0, Lcom/google/android/gms/drive/database/model/bk;->h:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 179
    sget-object v0, Lcom/google/android/gms/drive/database/model/bk;->i:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    .line 181
    sget-object v0, Lcom/google/android/gms/drive/database/model/bk;->d:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 182
    sget-object v0, Lcom/google/android/gms/drive/database/model/bk;->c:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 186
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/gms/drive/auth/AppIdentity;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/auth/AppIdentity;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 190
    sget-object v0, Lcom/google/android/gms/drive/database/model/bk;->b:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 192
    sget-object v2, Lcom/google/android/gms/drive/database/model/bk;->f:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v10

    .line 193
    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v6

    .line 194
    new-instance v1, Lcom/google/android/gms/drive/database/model/bi;

    move-object v2, p0

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gms/drive/database/model/bi;-><init>(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;JLjava/lang/String;)V

    .line 197
    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->a()Lcom/google/android/gms/drive/database/model/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bj;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/drive/database/model/bi;->d(J)V

    .line 199
    return-object v1

    .line 187
    :catch_0
    move-exception v0

    .line 188
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Unable to parse creator identity"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 153
    sget-object v0, Lcom/google/android/gms/drive/database/model/bk;->a:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/bi;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    sget-object v0, Lcom/google/android/gms/drive/database/model/bk;->h:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/bi;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    sget-object v0, Lcom/google/android/gms/drive/database/model/bk;->i:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/bi;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    sget-object v0, Lcom/google/android/gms/drive/database/model/bk;->d:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/bi;->i:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 159
    :try_start_0
    sget-object v0, Lcom/google/android/gms/drive/database/model/bk;->c:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/bi;->h:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/auth/AppIdentity;->d()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    sget-object v0, Lcom/google/android/gms/drive/database/model/bk;->b:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/bi;->g:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/bi;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 166
    sget-object v0, Lcom/google/android/gms/drive/database/model/bk;->f:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/bi;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :goto_0
    return-void

    .line 161
    :catch_0
    move-exception v0

    .line 162
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unable to serialize creator identity"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 168
    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/database/model/bk;->f:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v2, 0x27

    .line 204
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PendingUpload [contentHash=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/bi;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", baseContentHash=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/bi;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", entrySpec="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/bi;->g:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", creatorIdentity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/bi;->h:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", writeOpenTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/bi;->i:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uploadUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/bi;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
