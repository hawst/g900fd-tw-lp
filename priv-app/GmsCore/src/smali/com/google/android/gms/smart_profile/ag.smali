.class final Lcom/google/android/gms/smart_profile/ag;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/ag;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;B)V
    .locals 0

    .prologue
    .line 188
    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/ag;-><init>(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 7

    .prologue
    .line 188
    check-cast p1, Lcom/google/android/gms/people/e;

    if-eqz p1, :cond_2

    invoke-interface {p1}, Lcom/google/android/gms/people/e;->c()Lcom/google/android/gms/people/model/e;

    move-result-object v1

    if-eqz v1, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/ag;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->i(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Landroid/content/Context;

    move-result-object v0

    sget v2, Lcom/google/android/gms/p;->xd:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/ag;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    new-instance v3, Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/google/android/gms/people/model/e;->c()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/HashMap;-><init>(I)V

    invoke-static {v0, v3}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;Ljava/util/Map;)Ljava/util/Map;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lcom/google/android/gms/people/model/e;->c()I

    move-result v3

    if-ge v0, v3, :cond_1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/model/e;->b(I)Lcom/google/android/gms/people/model/d;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/ag;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v4}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->j(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v3}, Lcom/google/android/gms/people/model/d;->c()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3}, Lcom/google/android/gms/people/model/d;->d()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v3}, Lcom/google/android/gms/people/model/d;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/ag;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-interface {v3}, Lcom/google/android/gms/people/model/d;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->c(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/people/model/e;->w_()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/ag;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->k(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/ag;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->l(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/ag;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->m(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V

    :cond_4
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/people/model/e;->w_()V

    throw v0
.end method
